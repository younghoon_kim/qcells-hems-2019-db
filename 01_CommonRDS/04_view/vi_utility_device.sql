CREATE OR REPLACE VIEW wems.vi_utility_device
AS SELECT bui.utility_id,
    bui.installer_id,
    bd.device_id
   FROM tb_bas_device bd
     JOIN tb_bas_utility_installer bui ON bd.instl_user_id::text = bui.installer_id::text;

-- Permissions

ALTER TABLE wems.vi_utility_device OWNER TO wems;
GRANT ALL ON TABLE wems.vi_utility_device TO wems;

CREATE OR REPLACE VIEW wems.vi_raw_log_result
AS SELECT var_sbq.req_id,
    var_sbq.device_id,
    max(var_sbq.req_tm::text) AS req_tm,
    max(
        CASE
            WHEN var_sbq.rnum = 2 THEN var_sbq.res_tm
            ELSE NULL::character varying
        END::text) AS res_tm,
        CASE max(var_sbq.rnum)
            WHEN 2 THEN 'Y'::text
            ELSE 'N'::text
        END AS cmpl_flag,
    max(var_sbq.result_cd::text) AS result_cd,
    max(var_sbq.logfilename::text) AS log_file
   FROM ( SELECT row_number() OVER (PARTITION BY a.req_id, a.device_id ORDER BY a.create_dt) AS rnum,
            a.req_id,
            a.req_type_cd,
            a.device_id,
            a.req_tm,
            a.res_tm,
            a.result_cd,
            a.cmd_type_cd,
            a.logfilename,
            a.create_dt
           FROM tb_opr_ctrl_result a,
            ( SELECT tb_opr_ctrl_result.req_id,
                    tb_opr_ctrl_result.device_id
                   FROM tb_opr_ctrl_result
                  WHERE tb_opr_ctrl_result.req_type_cd::text = 'WEB'::text AND tb_opr_ctrl_result.cmd_type_cd::text = '25'::text) b
          WHERE a.req_id = b.req_id AND a.device_id::text = b.device_id::text) var_sbq
  GROUP BY var_sbq.req_id, var_sbq.device_id;

-- Permissions

ALTER TABLE wems.vi_raw_log_result OWNER TO wems;
GRANT ALL ON TABLE wems.vi_raw_log_result TO wems;

CREATE OR REPLACE FUNCTION wems.fn_chk_pwd(i_usr_id character varying, i_pwd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
 IMMUTABLE STRICT
AS $function$
/******************************************************************************
  프로그램명   : FN_CHK_PWD
  작  성  일   : 2019-11-11
  작  성  자   : 박동환
  프로그램용도 : 암호화된 비밀번호조회함수
  참 고 사 항  :
                 < PROTOTYPE >
                   FN_CHK_PWD

                 <PARAMETER>
                   < INPUT >
                     1. i_usr_id   	사용자ID
				     2. i_pwd     	비밀번호

                   < OUTPUT >
                     1. o_result     	비번일치여부(일치:'T', 불일치:'F')

                 < MODULE DIAGRAM >
                 1.변수 및 커서 선언


  -----------------------------------------------------------------------------------------
  TABLE                                        CRUD
  ==================================================
  TB_RAW_LOGIN								 R
  ------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------
  수정일자      수정자      수정내용
  ======== ======= 	==================================
  20191111    박동환      최초작성
  -------------------------------------------------------------------------------------------
  
*. 참고(본 함수가 내부적으로 처리되는 방식에 대한 이해)
1. 비번 암호화 과정	
	select crypt('비밀번호', gen_salt('md5')); -- 결과:'$1$kxNDzbv0$UfEqWxPBViqimy0E.UC5d.' 를 DB에 저장
	
2. 검증과정(비밀번호, DB에 저장돼 있던 암호값(SALT))	
 	select crypt('비밀번호', '$1$kxNDzbv0$UfEqWxPBViqimy0E.UC5d.');	

*****************************************************************************/
BEGIN
	
	RETURN 
	(
		SELECT 	CASE WHEN USER_PWD = crypt(i_pwd, USER_PWD) THEN 'T' ELSE 'F' END
		FROM 	TB_BAS_USER
		WHERE 	USER_ID = i_usr_id
	)::character varying;

  	EXCEPTION WHEN OTHERS THEN		
		RETURN	'F'::character varying;

END;

$function$
;

CREATE OR REPLACE PROCEDURE wems.sp_add_unit_price_mm(p_prod_cd text, p_ym text, p_fromhour numeric, p_tohour numeric, p_price numeric, p_wkday text, p_io text)
 LANGUAGE plpgsql
AS $procedure$
/******************************************************************************
   NAME:       SP_ADD_UNIT_PRICE_MM
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-08-21   yngwie   Created this function.
   2.0        2019-09-25   이재형    postgresql 변환
   
   NOTES:   기준 요금제를 월 단위로 추가한다.

	Input : p_prod_cd - 요금제코드
	        p_ym - 적용년월
	        p_fromHour - 적용시작시각
	        p_toHour - 적용종료시각
	        p_price - 단가
	        p_wkday - 주중, 주말 구분(1: 주중, 0:주말)
	        p_io - 매/수가 구분(O:수가, T:매가)

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
    v_log_fn 		VARCHAR := 'SP_ADD_UNIT_PRICE_MM';
    v_log_seq 	NUMERIC := 0; 		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd	VARCHAR := 'M';							-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt		TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd		VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt		INTEGER   := 0;     						-- 적재수
    v_work_num	INTEGER   := 0;     						-- 처리건수
    v_err_msg	VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1	VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2	VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3	VARCHAR   := NULL;  					-- 메시지내용3
	
    p_dt 			VARCHAR   := ' ' ;
   
    -- (2)업무처리 변수

BEGIN

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
    
	    -- 기존에 입력된 요금제가 있으면 삭제
	    DELETE FROM TB_BAS_ELPW_UNIT_PRICE
	    WHERE 	elpw_prod_cd = p_prod_cd 
	    AND 		unit_ym = p_ym 
	    AND 		unit_hh BETWEEN TO_CHAR(p_fromHour, 'FM00') AND TO_CHAR(p_toHour, 'FM00') 
	    AND 		wkday_flag = p_wkday 
	    AND 		io_flag = p_io;
	
	    FOR y IN p_fromHour..p_toHour LOOP
	        INSERT INTO TB_BAS_ELPW_UNIT_PRICE (elpw_prod_cd, unit_ym, unit_hh, io_flag, unit_price, wkday_flag, use_flag, create_id)
	        VALUES (p_prod_cd, p_ym, TO_CHAR(y, 'FM00'), p_io, p_price, p_wkday, 'Y', 'SYSTEM');
	    END LOOP;
	    
	
	    GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	     v_rslt_cnt := v_rslt_cnt + v_work_num;

	    -- 실행성공
		v_rslt_cd := '1';
    
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
END;
$procedure$
;

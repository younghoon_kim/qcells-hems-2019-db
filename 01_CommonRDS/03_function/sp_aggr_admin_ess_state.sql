CREATE OR REPLACE PROCEDURE wems.sp_aggr_admin_ess_state()
 LANGUAGE plpgsql
AS $procedure$
/******************************************************************************
   NAME:       SP_AGGR_ADMIN_ESS_STATE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-09-04   yngwie       1. Created this function.
   1.1        2015-09-19   yngwie       1. UI 에서 SQL 로 조회하도록 변경, 사용하지 않음
   2.0        2019-09-20   박동환        Postgresql 변환

   NOTES: 전체 ESS 운용 현황 집계

    Input :

    Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_AGGR_ADMIN_ESS_STATE';
    v_log_seq 		NUMERIC := 0;	-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';							-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    p_dt				VARCHAR   := ''; 
   
BEGIN

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';

--	    RAISE NOTICE 'v_log_fn:[%] v_log_seq[%] START[%] p_dt[%]', v_log_fn, v_log_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), p_dt;

    	DELETE FROM TB_OPR_ADMIN_ESS_STATE;
        GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--     	RAISE NOTICE 'v_log_fn[%] Delete TB_OPR_ADMIN_ESS_STATE Table [%]건', v_log_fn, v_work_num;

		v_rslt_cd := 'B';
    
        INSERT INTO TB_OPR_ADMIN_ESS_STATE ( 
            CNTRY_CD, COLEC_DD
            , INSTLR_CNT, INSTLR_NA_CNT, TOT_CNT        
            , NML_CNT, NML_PER        
            , WARN_CNT, WARN_PER   
            , ERR_CNT, ERR_PER        
            , NO_SIG_CNT, NO_SIG_PER     
            , STD_CNT, STD_PER        
            , RCT_INSTL_CNT 
            , CREATE_DT 
        ) 
         WITH NU AS (
            SELECT		COUNT(*) AS CNT
            FROM (
                SELECT	A.USER_ID
                FROM    TB_BAS_USER A
                WHERE   DEC_VARCHAR_SEL(A.AUTH_TYPE_CD, 10, 'SDIENCK', 'TB_BAS_USER', 'AUTH_TYPE_CD') = '20'
                GROUP BY  A.USER_ID
                EXCEPT 	-- ORACLE에선 MINUS
                SELECT   A.USER_ID
                FROM    TB_BAS_USER A
                   		  , TB_BAS_DEVICE B
                WHERE   DEC_VARCHAR_SEL(A.AUTH_TYPE_CD, 10,  'SDIENCK', 'TB_BAS_USER', 'AUTH_TYPE_CD') = '20'
                    AND 	A.USER_ID = B.INSTL_USER_ID
                GROUP BY A.USER_ID
            ) T
         ), U AS (
            SELECT 
                CNTRY_CD
                , COUNT(*) AS CNT
            FROM (
                SELECT	CNTRY_CD
                    	,  B.INSTL_USER_ID
                FROM
                    TB_BAS_USER A
                    , TB_BAS_DEVICE B
                WHERE 
                    DEC_VARCHAR_SEL(A.AUTH_TYPE_CD, 10, 'SDIENCK', 'TB_BAS_USER', 'AUTH_TYPE_CD') = '20'
                    AND A.USER_ID = B.INSTL_USER_ID
                GROUP BY
                    B.CNTRY_CD
                    , B.INSTL_USER_ID
            ) Z
            GROUP BY   CNTRY_CD
         ), A AS (
            SELECT 
                MAX(U.CNT) AS INSTLR_CNT
                , MAX(NU.CNT) AS INSTLR_NA_CNT
                , COUNT(*) AS TOT_CNT
                , B.CNTRY_CD
                , SUM(CASE WHEN C.OPER_STUS_CD = '0' THEN 1 ELSE 0 END) AS NML_CNT 
                , SUM(CASE WHEN C.OPER_STUS_CD = '1' THEN 1 ELSE 0 END) AS WARN_CNT 
                , SUM(CASE WHEN C.OPER_STUS_CD = '2' THEN 1 ELSE 0 END) AS ERR_CNT 
                , SUM(CASE WHEN C.OPER_STUS_CD = '3' THEN 1 ELSE 0 END) AS NO_SIG_CNT 
                , SUM(CASE WHEN C.OPER_STUS_CD IS NULL OR C.OPER_STUS_CD = '4' THEN 1 ELSE 0 END) AS STD_CNT 
				/* 박동환, 2019.09.25 아래 주석처리된 원본의 조회기간이 이해가 안됨
                , SUM(CASE WHEN B.INSTL_DT BETWEEN TO_CHAR(SYS_EXTRACT_UTC(NOW()) - (7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 1), 'YYYYMMDD') 
                                                   		AND TO_CHAR(SYS_EXTRACT_UTC(NOW()) - (7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 7), 'YYYYMMDD') 
                */
                , SUM(CASE WHEN B.INSTL_DT BETWEEN TO_CHAR(SYS_EXTRACT_UTC(NOW()) - CONCAT((7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 1)::varCHAR, 'DAY' )::interval, 'YYYYMMDD')
                                                   		AND TO_CHAR(SYS_EXTRACT_UTC(NOW()) - CONCAT((7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 7)::varCHAR, 'DAY' )::interval, 'YYYYMMDD')
                      THEN 1 ELSE 0 END) AS RCT_INSTL_CNT 
            FROM 
                 TB_BAS_DEVICE B
                , TB_OPR_ESS_STATE C
                , NU
                , U
            WHERE	B.DEVICE_ID = C.DEVICE_ID	-- (+) 
                AND 	B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD	-- (+)
                AND 	B.CNTRY_CD = U.CNTRY_CD
            GROUP BY	B.CNTRY_CD
         )
         SELECT 
             A.CNTRY_CD 
            , TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'YYYYMMDD') AS COLEC_DD 
            , A.INSTLR_CNT
            , A.INSTLR_NA_CNT
            , A.TOT_CNT 
            , A.NML_CNT 
            , ROUND(A.NML_CNT / A.TOT_CNT * 100, 1) AS NML_PER 
            , A.WARN_CNT 
            , ROUND(A.WARN_CNT / A.TOT_CNT * 100, 1) AS WARN_PER 
            , A.ERR_CNT 
            , ROUND(A.ERR_CNT / A.TOT_CNT * 100, 1) AS ERR_PER 
            , A.NO_SIG_CNT 
            , TRUNC(A.NO_SIG_CNT / A.TOT_CNT * 100, 1) AS NO_SIG_PER 
            , A.STD_CNT 
            , ROUND(A.STD_CNT / A.TOT_CNT * 100, 1) AS STD_PER 
            , A.RCT_INSTL_CNT 
            , SYS_EXTRACT_UTC(NOW()) AS CREATE_DT 
         FROM 	A 
         UNION ALL
         SELECT
             'ALL' AS CNTRY_CD 
            , TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'YYYYMMDD') AS COLEC_DD 
            , SUM(A.INSTLR_CNT)
            , MAX(A.INSTLR_NA_CNT)
            , SUM(A.TOT_CNT)
            , SUM(A.NML_CNT)
            , ROUND(SUM(A.NML_CNT) / SUM(A.TOT_CNT) * 100, 1) AS NML_PER 
            , SUM(A.WARN_CNT) 
            , ROUND(SUM(A.WARN_CNT) / SUM(A.TOT_CNT) * 100, 1) AS WARN_PER 
            , SUM(A.ERR_CNT) 
            , ROUND(SUM(A.ERR_CNT) / SUM(A.TOT_CNT) * 100, 1) AS ERR_PER 
            , SUM(A.NO_SIG_CNT) 
            , TRUNC(SUM(A.NO_SIG_CNT) / SUM(A.TOT_CNT) * 100, 1) AS NO_SIG_PER 
            , SUM(A.STD_CNT) 
            , ROUND(SUM(A.STD_CNT) / SUM(A.TOT_CNT) * 100, 1) AS STD_PER 
            , SUM(A.RCT_INSTL_CNT)
            , SYS_EXTRACT_UTC(NOW()) AS CREATE_DT 
         FROM A 
        ;
        
       GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	

 --   	RAISE NOTICE 'v_log_fn[%] Insert TB_OPR_ADMIN_ESS_STATE Table [%]건', v_log_fn, v_work_num;
   	   
        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$procedure$
;

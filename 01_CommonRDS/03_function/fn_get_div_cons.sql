CREATE OR REPLACE FUNCTION wems.fn_get_div_cons(p_pv numeric, p_bt numeric, p_grid numeric, p_cons numeric)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_GET_DIV_CONS
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-28   yngwie    Created this function.
   2.0        2019-09-24   이재형     Postgresql 변환
   
   NOTES:   가정내소비전력량-TOTAL 에서 GRID, PV, 배터리가 차지하는 전력량을 구한다.

	Input : p_pv - PV전력량
	        p_bt - 배터리전력량-방전
	        p_grid - Grid 수전량
	        p_cons - 가정내소비전력량-TOTAL

    Output : VARCHAR - 결과 값, 구분자 | 를 중심으로 순서대로 pv, bt, grid

	Usage :


******************************************************************************/
DECLARE
    i_result 		VARCHAR := NULL;
    i_sql 			TEXT;
    i_ratio_pv 	numeric;
    i_ratio_bt 	numeric;
    i_ratio_grid 	numeric;
    i_pv_val 		numeric;
    i_bt_val 		numeric;
    i_grid_val 	numeric;
BEGIN

    IF p_cons IS NOT NULL THEN
	 	SELECT 
	 		 MAX( CASE WHEN TP_CD = 'PV' THEN ROUND( p_cons * RATIO, 1) ELSE NULL END ) PV_VAL 
	 		, MAX( CASE WHEN TP_CD = 'BT' THEN ROUND( p_cons * RATIO, 1) ELSE NULL END ) BT_VAL 
	 		, MAX( CASE WHEN TP_CD = 'GRID' THEN ROUND( p_cons * RATIO, 1) ELSE NULL END ) GRID_VAL 
	 	into i_pv_val, i_bt_val, i_grid_val
	 	FROM ( 
	 		SELECT 
	 		    TP_CD
	 		    , AMOUNT 
	 		    , COALESCE(1.0 * AMOUNT / NULLIF(SUM(AMOUNT) OVER(),0), 0) AS RATIO 
	 		FROM ( 
		 			SELECT  p_pv AMOUNT, 'PV' TP_CD 
		 			UNION ALL 
		 			SELECT p_bt AMOUNT, 'BT' TP_CD  
		 			UNION ALL 
		 			SELECT p_grid AMOUNT, 'GRID' TP_CD 
	 			) a
	 	) b ;

		 i_result := i_pv_val || '|' || i_bt_val || '|' || i_grid_val;
		
	--	RAISE NOTICE 'i_pv_val[%] i_bt_val[%] i_grid_val[%] >>> i_result[%]' ,i_pv_val, i_bt_val, i_grid_val, i_result;
		
	END IF;
	
	-- RAISE NOTICE 'p_cons[%], i_result[%]' ,p_cons, i_result; 

    RETURN i_result;
   

END;
$function$
;

CREATE OR REPLACE FUNCTION sp_sys_remove_partition(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       SP_SYS_REMOVE_PARTITION
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-03-17   yngwie       1. Created this function.
   2.0        2019-09-24   박동환        Postgresql 변환

   NOTES:   TB_PLF_TBL_META의 DEL_CYCLE 이전 파티션 삭제
   
    Input : 
	        
	Output : 
	
******************************************************************************/	
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_row           	record; 
   	v_loopCnt		INTEGER := 0;
   	v_nextDateStr	VARCHAR   := NULL; 
   	v_sql				VARCHAR   := NULL;
  
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

--	    RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] START[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	    FOR v_row IN ( 
			WITH PLF AS (
				SELECT M.TABLE_NM    as TABLE_NAME
				   		, MIN(CHILD.RELNAME)		OLD_PARTITION
						, M.PARTITION_TYPE
						, M.DEL_CYCLE
					FROM 	PG_INHERITS
				    JOIN 		PG_CLASS PARENT ON PG_INHERITS.INHPARENT = PARENT.OID
				    JOIN 		PG_CLASS CHILD   ON PG_INHERITS.INHRELID   = CHILD.OID
				    JOIN 		TB_PLF_TBL_META M ON (PARENT.RELNAME= LOWER(M.TABLE_NM)  )
				    WHERE	M.PARTITION_TYPE IN ('D', 'M')
				    AND		M.DEL_CYCLE > 0
				    AND     NOT M.DEL_FLAG ='M'
					GROUP BY  M.TABLE_NM, M.PARTITION_TYPE, M.DEL_CYCLE
			), BAS AS (
				SELECT 	*
							, STRING_TO_ARRAY(OLD_PARTITION, '_') RELARR
				FROM 	PLF
			), PT AS (
				SELECT *
						, RELARR[ARRAY_UPPER(RELARR,1)] 	OLD_PT_DATE_STR
						, CASE WHEN PARTITION_TYPE = 'M' THEN 
											TO_CHAR(SYS_EXTRACT_UTC(NOW())- concat(DEL_CYCLE,' MONTH')::INTERVAL, 'YYYYMM')
						       	 WHEN PARTITION_TYPE = 'D' THEN 
						       	 			TO_CHAR(SYS_EXTRACT_UTC(NOW())- concat(DEL_CYCLE,' DAY')::INTERVAL, 'YYYYMMDD')
						         ELSE NULL
						   END AS MIN_DATE_STR
				FROM BAS
			)
			SELECT 	
						TABLE_NAME
						, OLD_PARTITION
						, OLD_PT_DATE_STR
						, MIN_DATE_STR
						, PARTITION_TYPE
						, CASE WHEN PARTITION_TYPE = 'M' THEN 
										(DATE_PART('YEAR',     TO_DATE(MIN_DATE_STR, 'YYYYMM')) - DATE_PART('YEAR',     TO_DATE(OLD_PT_DATE_STR, 'YYYYMM'))
										) * 12 +
										(DATE_PART('MONTH', TO_DATE(MIN_DATE_STR, 'YYYYMM')) - DATE_PART('MONTH', TO_DATE(OLD_PT_DATE_STR, 'YYYYMM')))
						  		ELSE 	 
						  				 EXTRACT(DAY FROM TO_TIMESTAMP(MIN_DATE_STR, 'YYYYMMDD')-TO_TIMESTAMP(OLD_PT_DATE_STR, 'YYYYMMDD'))
						  END DIFF_SIZE
						, DEL_CYCLE
			FROM PT
			
		) LOOP
            IF v_row.DIFF_SIZE > 0 THEN

            	--	2015.03.31 yngwie, 파티션 보관주기 이전의 파티션들은 모두 drop 한다.
            	FOR v_loopCnt IN 0..v_row.DIFF_SIZE-1 LOOP

            		-- 다음 번 삭제할 파티션 날짜 문자열을 생성한다.
            		IF v_row.PARTITION_TYPE = 'D' THEN
            			v_nextDateStr := TO_CHAR(TO_DATE(v_row.OLD_PT_DATE_STR, 'YYYYMMDD') + concat(v_loopCnt,' DAY')::INTERVAL, 'YYYYMMDD');
            		ELSE
            			v_nextDateStr := TO_CHAR(TO_DATE(v_row.OLD_PT_DATE_STR, 'YYYYMMDD') + concat(v_loopCnt,' MONTH')::INTERVAL, 'YYYYMM');
            		END IF;
            	
                    -- ex) DROP TABLE PT_STT_ESS_DD_20170812 CASCADE;
	                v_sql := FORMAT('DROP TABLE PT_%s_%s CASCADE;' 
	                	                , REPLACE(v_row.TABLE_NAME, 'TB_', '')
	                					, v_nextDateStr
	                					) ;
	                				
					--RAISE notice 'i_sp_nm[%] REMOVE Partition :  SQL[%]', i_sp_nm, v_sql;
	                EXECUTE v_sql;		            
	                GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
				    v_rslt_cnt := v_rslt_cnt + v_work_num;	       

	            END LOOP;            
            END IF;
           
	   END LOOP;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$function$
;

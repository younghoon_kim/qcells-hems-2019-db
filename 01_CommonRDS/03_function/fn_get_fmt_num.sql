CREATE OR REPLACE FUNCTION wems.fn_get_fmt_num(p_value numeric)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
*****************************************************************************
   NAME:       FN_GET_FMT_NUM
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-04-17   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                            Postgresql 변환
   NOTES:   RAW 테이블 컬럼 중 문자열로 입력되지만 수치형 데이터로 처리해야 하는 경우 사용한다.

	Input : p_value - VARCHAR

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_FMT_NUM('123.57') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(100);
BEGIN
    v_value :=
    CASE
        WHEN p_value = 0 THEN '0'
        WHEN position('.' in TO_CHAR(p_value)) > 0 THEN TO_CHAR(p_value, 'FM999,999,990.00')
        ELSE TO_CHAR(p_value, 'FM999,999,999')
    END;
   
    RETURN v_value;

END;
$function$
;

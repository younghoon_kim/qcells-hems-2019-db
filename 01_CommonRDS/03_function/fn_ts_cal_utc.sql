CREATE OR REPLACE FUNCTION wems.fn_ts_cal_utc(i_gap numeric)
 RETURNS timestamp without time zone
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_TS_CAL_UTC
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-25   yngwie      Created this function.
   2.0        2019-09-23   박동환       Postgresql 전환

   NOTES:   입력받은 GMT 시간차에 해당하는 날짜의 Timestamp 형태 반환

	Input : i_gap - GMT 시간차

    Output : TIMESTAMP

	Usage :
	        FN_TS_CAL_UTC(9)

******************************************************************************/
DECLARE
    o_return timestamp := NULL;
BEGIN

  	SELECT SYS_EXTRACT_UTC(now()) - concat(-i_gap,' hours')::interval
	INTO o_return;

	RETURN o_return;

	EXCEPTION WHEN others THEN 
   		RETURN o_return;

END;
$function$
;

CREATE OR REPLACE FUNCTION wems.last_day(p_date date)
 RETURNS date
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       last_day
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2019-10-07   박동환       Tibero 내장함수 Postgresql 변환
   
   NOTES:   입력월의 마지막일자 조회

	Input : p_date - date

    Output : date

	Usage :
	        SELECT LAST_DAY(TO_DATE('20191007', 'YYYYMMDD'));
	        => 2019-10-31

******************************************************************************/
DECLARE
    v_result DATE;
   
BEGIN
    SELECT 	(date_trunc('MONTH', $1) + INTERVAL '1 MONTH - 1 day')::date
    INTO 		v_result;
   
   RETURN v_result;
   
END;
$function$
;

CREATE OR REPLACE FUNCTION sp_sys_remove_old_records(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
  NAME:       SP_SYS_REMOVE_OLD_RECORDS
  PURPOSE:

  REVISIONS:
  Ver        Date        Author    	Description
  ---------  ----------  --------  ------------------------------------
  1.0        2014-09-16   yngwie 	Created this function.
  2.0        2019-10-01   박동환    Postgresql 변환
  2.1		 2020-04-07   박동환    TB_RAW_LOGIN 예외 삭제 로직 추가
  NOTES:   메타테이블(TB_PLF_TBL_META) 기준으로 보관기간 경과 레코드 삭제

  Input :

  Output :

  Test : SELECT SP_SYS_REMOVE_OLD_RECORDS('SP_SYS_REMOVE_OLD_RECORDS', 0, '20200407033000', 'M');
******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); 		-- 시작일시
    v_rslt_cd		VARCHAR   := '0'; 						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt		INTEGER   := 0;    						-- 적재수
    v_work_num		INTEGER   := 0;    						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_row           record;
   	v_sql			VARCHAR   := NULL;

BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

--	    RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] START[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	    FOR v_row IN (
            (SELECT
                A.TABLE_NM
                , A.STD_COL_NM
                , A.STD_COL_TYPE
                , COALESCE(B.CHARACTER_MAXIMUM_LENGTH, 17) AS DATA_LENGTH
                , FN_GET_DATEFORMAT(COALESCE(B.CHARACTER_MAXIMUM_LENGTH, 17)) AS DATE_FMT
                , A.DEL_CYCLE
            FROM
                TB_PLF_TBL_META A
                , INFORMATION_SCHEMA.COLUMNS B
            WHERE	A.DEL_FLAG IN ('Y', 'M')
                AND 	A.DEL_CYCLE > 0
                AND 	A.TABLE_NM = UPPER(B.TABLE_NAME)
                AND 	A.STD_COL_NM = UPPER(B.COLUMN_NAME)
                AND 	PARTITION_TYPE IS NULL)
            UNION
            -- TB_OPR_ALARM_HIST는 PARTITION_TYPE이 있지만 레코드를 삭제하는 유일한 테이블임으로 따로 UNION.
            (SELECT
                A.TABLE_NM
                , A.STD_COL_NM
                , A.STD_COL_TYPE
                , COALESCE(B.CHARACTER_MAXIMUM_LENGTH, 17) AS DATA_LENGTH
                , FN_GET_DATEFORMAT(COALESCE(B.CHARACTER_MAXIMUM_LENGTH, 17)) AS DATE_FMT
                , A.DEL_CYCLE
            FROM
                TB_PLF_TBL_META A
                , INFORMATION_SCHEMA.COLUMNS B
            WHERE	A.DEL_FLAG IN ('Y', 'M')
                AND 	A.DEL_CYCLE > 0
                AND 	A.TABLE_NM = UPPER(B.TABLE_NAME)
                AND 	A.STD_COL_NM = UPPER(B.COLUMN_NAME)
                AND 	A.TABLE_NM='TB_OPR_ALARM_HIST')

		) LOOP
	        --TB_RAW_LOGIN은 일단위 주기로 삭제하기때문에 예외적으로 처리한다.
           IF v_row.TABLE_NM = 'TB_RAW_LOGIN' THEN
           		v_sql := FORMAT('
								WITH tgt AS (
									SELECT *
									FROM (
								   		SELECT  ROW_NUMBER() OVER (PARTITION BY DEVICE_ID ORDER BY CREATE_DT DESC, DATA_SEQ DESC) rn
								   				, DEVICE_ID
								   				, CREATE_DT
								   				, DATA_SEQ
										FROM   	TB_RAW_LOGIN
										WHERE 	CREATE_DT < DATE_TRUNC(''DAY'', SYS_EXTRACT_UTC(NOW()) - %s * ''1 DAY''::INTERVAL)
									) lg
									WHERE rn != 1
								)
								DELETE
								FROM 	TB_RAW_LOGIN
								WHERE  	(DEVICE_ID,	CREATE_DT, DATA_SEQ) IN (SELECT DEVICE_ID, CREATE_DT, DATA_SEQ FROM tgt);'
           						, v_row.DEL_CYCLE);

		       	EXECUTE v_sql;
	           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		       	v_rslt_cnt := v_rslt_cnt + v_work_num;
--		       	RAISE NOTICE 'i_sp_nm[%] [%] Table REMOVE old records[%] SQL[%]', i_sp_nm, v_row.TABLE_NM, v_work_num, v_sql;
--            ELSEIF v_row.table_nm = 'TB_OPR_ALARM_HIST' THEN
--                 v_sql := FORMAT(	'DELETE FROM %s WHERE %s < TO_CHAR(DATE_TRUNC(''DAY'', SYS_EXTRACT_UTC(NOW()) - %s * ''30 DAY''::INTERVAL), %s) ;'
--                 , v_row.TABLE_NM
--                 , v_row.STD_COL_NM
--                 , v_row.DEL_CYCLE
--                 , CHR(39)||v_row.DATE_FMT||CHR(39)
-- 				);
--                 EXECUTE v_sql;
--                 GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
-- 		       	v_rslt_cnt := v_rslt_cnt + v_work_num;
-- --		       	RAISE NOTICE 'i_sp_nm[%] [%] Table REMOVE old records[%] SQL[%]', i_sp_nm, v_row.TABLE_NM, v_work_num, v_sql;
--
           ELSE
	           IF v_row.STD_COL_TYPE = 'VARCHAR' then
	               -- DELETE FROM TB_SYS_EXEC_HIST WHERE CREATE_DT < TO_CHAR(DATE_TRUNC(''DAY'', SYS_EXTRACT_UTC(NOW()) - 1 * '30 DAY'::INTERVAL), 'YYYYMMDDHH24MISS') ;
	              	v_sql := FORMAT(	'DELETE FROM %s WHERE %s < TO_CHAR(DATE_TRUNC(''DAY'', SYS_EXTRACT_UTC(NOW()) - %s * ''30 DAY''::INTERVAL), %s) ;'
	              				, v_row.TABLE_NM
	              				, v_row.STD_COL_NM
	              				, v_row.DEL_CYCLE
								, CHR(39)||v_row.DATE_FMT||CHR(39)
							);

	            ELSIF v_row.STD_COL_TYPE = 'TIMESTAMP' THEN
	               -- DELETE FROM TB_SYS_EXEC_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
	              	v_sql := FORMAT(	'DELETE FROM %s WHERE %s < DATE_TRUNC(''DAY'', SYS_EXTRACT_UTC(NOW()) - %s * ''30 DAY''::INTERVAL) ;'
	              				, v_row.TABLE_NM
	              				, v_row.STD_COL_NM
	              				, v_row.DEL_CYCLE
							);
	            END IF;

		       	EXECUTE v_sql;
	           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		       	v_rslt_cnt := v_rslt_cnt + v_work_num;
--		       	RAISE NOTICE 'i_sp_nm[%] [%] Table REMOVE old records[%] SQL[%]', i_sp_nm, v_row.TABLE_NM, v_work_num, v_sql;
			END IF;


	       /*
	        SELECT * FROM TB_RAW_WEATHER WHERE TIME_FROM < TO_CHAR(DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 1 * '30 DAY'::INTERVAL), 'YYYYMMDDHH24MISS') ;
			SELECT * FROM TB_RAW_WEATHER_REQHIST WHERE CREATE_TM < TO_CHAR(DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 1 * '30 DAY'::INTERVAL), 'YYYYMMDDHH24MISS') ;
			SELECT * FROM TB_STT_ESS_YY WHERE COLEC_YY < TO_CHAR(DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 24 * '30 DAY'::INTERVAL), 'YYYY') ;
			SELECT * FROM TB_STT_QUITY_WRTY_DD WHERE COLEC_DD < TO_CHAR(DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 36 * '30 DAY'::INTERVAL), 'YYYYMMDD') ;
			SELECT * FROM TB_OPR_ESS_PREDICT_HIST WHERE COLEC_DT < TO_CHAR(DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 3 * '30 DAY'::INTERVAL), 'YYYYMMDDHH24MISS') ;
			SELECT * FROM TB_RAW_CURR_WEATHER WHERE COLEC_DT < TO_CHAR(DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 1 * '30 DAY'::INTERVAL), 'YYYYMMDDHH24MISS') ;

			SELECT * FROM TB_SYS_EXEC_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_SYS_LOGIN_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 6 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_SYS_SEC_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_SYS_SYSTEM_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 3 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_BAS_UPT_GRP_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_OPR_CTRL_RESULT WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_OPR_ESS_CONFIG_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_OPR_OPER_CTRL_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_OPR_OPER_LOGFILE_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_OPR_PVCALC_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
	        */


	   	END LOOP;

		-- 실행성공
       	v_rslt_cd := '1';

    EXCEPTION WHEN OTHERS THEN
--       	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;

   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );

   	return v_rslt_cd;

END;
$function$
;

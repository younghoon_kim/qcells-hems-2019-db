CREATE OR REPLACE FUNCTION wems.fn_get_wkday_flag(p_curymd text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_GET_WKDAY_FLAG
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-05-13   yngwie       Created this function.
   2.0        2019-09-24   이재형        Postgresql 변환
   
   NOTES:   입력받은  인자들에 해당하는 TB_BAS_ELPW_UNIT_PRICE.WKDAY_FLAG 를 반환한다.

	Input : p_curYmd - 해당 년월일

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_WKDAY_FLAG('20150701');

******************************************************************************/
BEGIN
    RETURN
	    CASE
	        WHEN TO_CHAR(TO_DATE(p_curYmd, 'YYYYMMDD'), 'd') IN ('1', '7') THEN '0'
	        ELSE '1'
	    END;
	   
    EXCEPTION WHEN OTHERS THEN
            RETURN '1';

END;
$function$
;

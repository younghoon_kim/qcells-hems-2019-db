CREATE OR REPLACE FUNCTION wems.hex2dec(hexval character)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
     /*
     *****************************************************************************
       NAME:       HEX2DEC
       PURPOSE:
    
       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
  	   2.0        2019-10-08     이재형                  postgresql 변환    
  	   
       NOTES:   HEX(16진수) to  Decimal(10진수)
    
        Input : 
    
        Output :
    
     *****************************************************************************
     */
DECLARE
    i numeric;
    digits numeric;
    result numeric := 0;
    current_digit CHARACTER(1);
    current_digit_dec numeric;
BEGIN
    digits := LENGTH(hexval);

    FOR i IN 1..digits LOOP
        current_digit := substr(hexval, i, 1);

        IF current_digit IN ('A', 'B', 'C', 'D', 'E', 'F') THEN
            current_digit_dec := ASCII(current_digit) - ASCII('A') + 10;
        ELSE
            current_digit_dec := current_digit::numeric;
        END IF;
        result := (result * 16) + current_digit_dec;
    END LOOP;
    RETURN result;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$function$
;

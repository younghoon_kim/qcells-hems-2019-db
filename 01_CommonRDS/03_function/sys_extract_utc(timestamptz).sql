CREATE OR REPLACE FUNCTION wems.sys_extract_utc(val timestamp with time zone)
 RETURNS timestamp with time zone
 LANGUAGE plpgsql
AS $function$
	DECLARE
		v_time  VARCHAR;
		v_result	timestamptz;	  
	BEGIN
		select (val at time zone 'UTC')::timestamptz into v_result ;
		return v_result;
	end;
$function$

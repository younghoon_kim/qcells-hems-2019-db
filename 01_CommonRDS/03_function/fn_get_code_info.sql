create or replace function fn_get_code_info(p_grp_cd text, p_code text, p_column text) returns text
    language plpgsql
as
$$
/*
*****************************************************************************
   NAME:       FN_GET_PRODUCT_MODEL
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2021-02-09   서정우       1. Created this function.

   NOTES:   입력받은  grp_cd와 code의 TB_PLF_CODE_INFO 테이블의 p_column 값을 반환

	Input : p_grp_cd - TB_PLF_CODE_INFO의 grp_cd_value
            p_code   - TB_PLF_CODE_INFO의 code_value
            p_column - TB_PLF_CODE_INFO 의 컬럼 명

    Output : VARCHAR

	Usage :
	        SELECT fn_get_code_info('BT_STUS_CD', '1','cd_desc');
            SELECT fn_get_code_info('BT_STUS_CD', '1','ref_4');
            SELECT fn_get_code_info('OPER_STUS_CD', '1', 'ref_4');

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(200);
    v_sql text;
BEGIN
--     v_sql := 'SELECT '|| p_column ||
    v_sql := format('SELECT %s FROM TB_PLF_CODE_INFO
              WHERE  GRP_CD = ''%s''
              AND code = ''%s'';', p_column, p_grp_cd, p_code);

--     EXECUTE  v_sql INTO v_value USING p_column, p_grp_cd, p_code;
    RAISE NOTICE 'fn_get_code_info : [%]', v_sql;
--     EXECUTE  v_sql INTO v_value USING p_grp_cd, p_grp_cd;
    EXECUTE  v_sql INTO v_value;

    RETURN v_value;

END;
$$;

alter function fn_get_code_info(text, text, text) owner to wems;

SELECT fn_get_code_info('BT_STUS_CD', '1','cd_desc');
SELECT fn_get_code_info('BT_STUS_CD', '1','ref_4');
SELECT fn_get_code_info('OPER_STUS_CD', '1', 'ref_4');


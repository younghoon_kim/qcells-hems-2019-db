CREATE OR REPLACE FUNCTION wems.dec2oct(n numeric)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
 /******************************************************************************
   NAME:       DEC2OCT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   2.0        2019-10-08     이재형                  postgresql 변환    
   
   NOTES:   Decimal(10진수) to oct(8진수)

    Input : 

    Output :

 ******************************************************************************/
DECLARE
    octval CHARACTER VARYING(64);
    n2 numeric := n;
BEGIN
    WHILE (n2 > 0) LOOP
        octval := CONCAT_WS('', MOD(n2, 8), octval);
        n2 := TRUNC((n2 / 8::NUMERIC)::NUMERIC);
    END LOOP;
    RETURN octval;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$function$
;

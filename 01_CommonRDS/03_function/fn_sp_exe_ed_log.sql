CREATE OR REPLACE FUNCTION wems.fn_sp_exe_ed_log(i_sp_nm character varying, i_job_seq numeric, i_exe_base character varying, i_st_dt timestamp without time zone, i_rslt_cd character varying, i_rslt_cnt integer, i_err_msg character varying)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
  프로그램ID   : fn_sp_exe_ed_log
  작  성  일   : 2019-09-20
  작  성  자   : 박동환
  프로그램용도 : 프로시져실행이력 종료로그 기록
  참 고 사 항  :
                 < PROTOTYPE >
                   fn_sp_exe_ed_log

                 <PARAMETER>
                   < INPUT >
                     1.i_sp_nm		프로시져명
                     2.i_job_seq	서비스대상번호
                     3.i_exe_base	실행기준값(월별    :YYYYMM 
											   일별    :YYYYMMDD 
											   시간대별:YYYYMMDDHH0000 
											   15분    :YYYYMMDDHHMI00)
                     4.i_st_dt     	시작일시
                     5.i_rslt_cd   	실행상태코드(0:실행중,1:실행성공,그외:실행오류) 
                     6.i_rslt_cnt  	실행결과건수
                     7.i_err_msg   	오류메시지
                   < OUTPUT >
                     NONE

                 < MODULE DIAGRAM >
                 1.변수 및 커서 선언
                 2.프로시져실행이력 변경

  ----------------------------------------------------------------------------
  TABLE                                                                  CRUD
  ============================================================================
  TB_SYS_SP_EXE_HST(프로시져실행이력)												U
  ----------------------------------------------------------------------------

  ----------------------------------------------------------------------------
  수정일자    수정자      수정내용
  =========== =========== ====================================================
  20190920    박동환      최초작성
  ----------------------------------------------------------------------------

*****************************************************************************/

DECLARE
  ----------------------------------------------------------------------------
	--1.변수 및 커서 선언
  ----------------------------------------------------------------------------
  v_lap_time 	NUMERIC   := 0.000;             --경과초수
  v_ed_dt		TIMESTAMP := CLOCK_TIMESTAMP(); --종료일시

BEGIN
	
  ----------------------------------------------------------------------------
	--2.프로시져실행이력 변경
  ----------------------------------------------------------------------------
	  BEGIN  
		    SELECT DATE_PART('HOUR',   v_ed_dt-i_st_dt)*3600 + 
		           DATE_PART('MINUTE', v_ed_dt-i_st_dt)*60   +
		           DATE_PART('SECOND', v_ed_dt-i_st_dt)::NUMERIC(10,2)
		    INTO v_lap_time;

--    		RAISE notice 'FN_SP_EXE_ED_LOG - SP_NAME[%] JOB_SEQ[%] EXE_BASE[%] ST_DT[%]', 	i_sp_nm,	i_job_seq, i_exe_base, i_st_dt;
		              
			UPDATE TB_SYS_SP_EXE_HST
			   SET ed_dt    = v_ed_dt
			     , lap_time =  v_lap_time
			     , rslt_cd  = i_rslt_cd
			     , rslt_cnt = i_rslt_cnt
			     , err_msg  = i_err_msg
			 WHERE sp_nm 	= i_sp_nm
			   AND exe_base = i_exe_base
			   AND job_seq = i_job_seq
			   AND st_dt    = i_st_dt;
  
    EXCEPTION WHEN OTHERS THEN
    	RAISE notice 'FN_SP_EXE_ED_LOG EXCEPTION !!!  - SP_NAME[%] JOB_SEQ[%] ', 	i_sp_nm,	i_job_seq;
    
    END;
END;
$function$
;

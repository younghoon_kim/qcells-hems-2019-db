CREATE OR REPLACE FUNCTION wems.enc_varchar_ins(cal_val character varying, int_val integer, sdienck character varying, table_name character varying, cal_name character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/***********************************
 * 사용법: SELECT ENC_VARCHAR_INS('Valent_Elio', 10, 'SDIENCK', 'TB_BAS_USER', 'user_id');
 * 
 ************************************/
DECLARE
	err_context	TEXT;
	
BEGIN		
	IF sdienck IS NULL OR sdienck = '' THEN 
		sdienck := 'SDIENCK';
	END IF;
	
	RETURN (	SELECT FN_GET_ENCRYPT(cal_val, sdienck)	);
		
	EXCEPTION WHEN OTHERS THEN 
		 GET STACKED DIAGNOSTICS err_context = MESSAGE_TEXT;
	RETURN err_context;
			 
END;

$function$
;

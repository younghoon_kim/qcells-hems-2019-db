CREATE OR REPLACE PROCEDURE wems.sp_check_lock_user(p_userid text, INOUT p_ret text)
 LANGUAGE plpgsql
AS $procedure$
/******************************************************************************
   NAME:       SP_CHECK_LOCK_USER
   PURPOSE:

   REVISIONS:
   Ver        Date            Author           Description
   ---------  ---------------  ---------------  ------------------------------------
   1.0        2015-04-27   yngwie       Created this function.
   2.0        2019-09-26   이재형        postgresql 변환
   
   NOTES:  사용자가 계정잠금 대상이면 계정을 잠그고 결과 반환

	Input : p_userId - 사용자 ID
            p_ret - 결과 (Y : 계정잠김, N : 계정 잠기지 않음)

	Usage :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
    v_log_fn 			VARCHAR := 'SP_CHECK_LOCK_USER';
    v_log_seq 		NUMERIC := 0;		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';	-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3
	
    p_dt 				VARCHAR   := ' ' ;
   
    -- (2)업무처리 변수
	v_flag 			CHARACTER VARYING(4);
    v_lockCount 	NUMERIC 	:= 5;	-- 계정잠금 최소 카운트
    v_cnt 				INTEGER 		:= 0;
BEGIN

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
    	p_ret := 'N';
   
		-- 0. 등록된 계정인지 확인한다.
	    SELECT 	COUNT(*) into v_cnt
	    FROM 	TB_BAS_USER 
	    WHERE 	USER_ID = ENC_VARCHAR_INS($1, 12, 'SDIENCK', 'TB_BAS_USER', 'USER_ID');
	
	    IF v_cnt > 0 THEN
	        -- 1. 사용자 정보에서 현재 계정잠금 상태인지 확인한다.
	    	SELECT COALESCE(DEC_VARCHAR_SEL(LOCK_FLAG, 10, 'SDIENCK', 'TB_BAS_USER', 'LOCK_FLAG'), 'N') into v_flag
	    	FROM TB_BAS_USER 
	     	WHERE USER_ID = ENC_VARCHAR_INS(p_userId, 12, 'SDIENCK', 'TB_BAS_USER', 'USER_ID');
	
	        -- 2. 현재  계정잠금 상태가 아니면 계정잠금 상황인지 체크한다. 
	        IF v_flag <> 'Y' THEN
	             SELECT COUNT(*) AS CNT 
	             into v_cnt
	             FROM TB_SYS_SEC_HIST A 
	             WHERE  A.USER_ID = ENC_VARCHAR_INS(p_userId, 12, 'SDIENCK', 'TB_SYS_SEC_HIST', 'USER_ID') 
	             AND A.REF_URL = '/login.do' 
	             AND A.CREATE_DT > ( 
				            			SELECT MAX(UNLOCK_DT) 
				            			FROM ( 
				            				SELECT LAST_UNLOCK_DT AS UNLOCK_DT 
				            			 	FROM TB_BAS_USER 
											WHERE USER_ID = ENC_VARCHAR_INS(p_userId, 12, 'SDIENCK', 'TB_BAS_USER', 'USER_ID') 			
											UNION ALL 
											SELECT  MAX(LOGIN_DT) AS UNLOCK_DT 
											FROM TB_SYS_LOGIN_HIST 
											WHERE  USER_ID = ENC_VARCHAR_INS(p_userId, 12, 'SDIENCK', 'TB_BAS_USER', 'USER_ID') 	
										) a
								)  ;
	
	           
	            -- 건수가 v_lockCount 회 이상이면 잠금 처리
	            IF v_cnt >= v_lockCount then
	                UPDATE TB_BAS_USER   SET LOCK_FLAG = ENC_VARCHAR_INS('Y', 12, 'SDIENCK', 'TB_BAS_USER', 'LOCK_FLAG') 
	               			, LOCK_START_DT = SYS_EXTRACT_UTC(now()) 
	               			, UPDATE_DT = SYS_EXTRACT_UTC(now())
	               			, UPDATE_ID =  ENC_VARCHAR_INS('SYSTEM', 12, 'SDIENCK', 'TB_BAS_USER', 'UPDATE_ID') 
	               	WHERE  USER_ID = ENC_VARCHAR_INS(p_userId, 12, 'SDIENCK', 'TB_BAS_USER', 'USER_ID');
	
	                p_ret := 'Y';
	               
	            END IF;
	
	        ELSE
	            p_ret := 'Y';
	        END IF;
	    END IF;
   
	    GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	    v_rslt_cnt := v_rslt_cnt + v_work_num;
	    
	    -- 실행성공
	    v_rslt_cd := '1';
    
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
END;
$procedure$
;

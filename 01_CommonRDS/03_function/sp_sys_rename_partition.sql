CREATE OR REPLACE PROCEDURE wems.sp_sys_rename_partition()
 LANGUAGE plpgsql
AS $procedure$
/******************************************************************************
   NAME:       SP_SYS_RENAME_PARTITION
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2019-09-30   박동환        최초작성

   NOTES:   파티션명 일괄변경
   
    Input : 
	        
	Output : 
	
******************************************************************************/	
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_SYS_RENAME_PARTITION';
    v_log_seq 		NUMERIC 	:= nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'A';							-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    p_dt				VARCHAR   := '';

   -- 미리 생성할 일수
--    v_daily_size 		INTEGER := 14;
--    v_monthly_size INTEGER := 2;
   	
    v_row           	record; 
   	v_sql				VARCHAR   := NULL;
  
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
  
		v_rslt_cd := 'A';

	    --RAISE NOTICE 'v_log_fn:[%] v_log_seq[%] START[%]', v_log_fn, v_log_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	    FOR v_row IN ( 
			SELECT 	M.TABLE_NM
						, CHILD.RELNAME as PARTION_NM
						, TO_REGCLASS( REPLACE( LOWER(CHILD.RELNAME), LOWER(TABLE_NM||'_'), '') ) as EXIST_NM
			FROM 	PG_INHERITS
		    JOIN 		PG_CLASS PARENT ON PG_INHERITS.INHPARENT = PARENT.OID
		    JOIN 		PG_CLASS CHILD   ON PG_INHERITS.INHRELID   = CHILD.OID
		    JOIN 		TB_PLF_TBL_META M ON (PARENT.RELNAME= LOWER(M.TABLE_NM)  )
		    WHERE	M.PARTITION_TYPE IS NOT null
				    
		) loop
			-- ALTER TABLE tb_sys_sp_exe_hst OWNER TO wems
--			v_sql := 'ALTER TABLE ' || v_row.PARTION_NM || ' OWNER TO wems;';
--			RAISE notice 'v_log_fn[%] Change Owner Partition Table :  SQL[%]', v_log_fn, v_sql;
--			EXECUTE v_sql;		            

			if v_row.EXIST_NM is null THEN
--				ALTER TABLE tb_stt_rank_mm_pt_stt_rank_mm_201902 RENAME TO pt_stt_rank_mm_201902;
				v_sql := 'ALTER TABLE ' || v_row.PARTION_NM || ' RENAME TO ' || REPLACE( LOWER(v_row.PARTION_NM), LOWER(v_row.TABLE_NM||'_'), '')||';';

				--RAISE notice 'v_log_fn[%] Rename Partition Table :  SQL[%]', v_log_fn, v_sql;
				EXECUTE v_sql;		            
			end if;
			
           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	       	v_rslt_cnt := v_rslt_cnt + v_work_num;	       

		END LOOP;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'v_log_fn[%] EXCEPTION !!! ', v_log_fn;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$procedure$
;

CREATE OR REPLACE FUNCTION wems.sp_stats_yy(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       SP_STATS_YY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-26   yngwie       1. Created this function.
   2.0        2019-09-20   박동환        Postgresql 변환

   NOTES:   시간별 통계 데이터를 가지고 년간 통계 데이터를 생성한다.
            PKG_OPR_ESS.SP_AGGR_STATE 가 끝난 후에 실행 (5분 주기).

            Source Table : TB_STT_ESS_TM
            Target Table : TB_STT_ESS_YY

    Input : i_exe_base VARCHAR 집계 대상 년

	Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_from			VARCHAR   := NULL; 
    v_to				VARCHAR   := NULL; 
    v_row           	record; 
   
BEGIN
	SELECT SUBSTR(i_exe_base, 1, 4) INTO i_exe_base;

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN  
		v_rslt_cd := 'A';
--	    RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] START[%] i_exe_base[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base;

    	FOR v_row IN ( 
    		-- Tibero에선 m_offset_sql
		    SELECT  UTC_OFFSET  
		    FROM (  
		    	SELECT	0 AS UTC_OFFSET  
		    	
		    	UNION ALL    
		    	SELECT  UTC_OFFSET  
		    	FROM   TB_BAS_CITY  
		    	WHERE  UTC_OFFSET IS NOT NULL  
		    	
		    	UNION ALL  
		    	SELECT  UTC_OFFSET + 1 
		    	FROM  TB_BAS_CITY  
		    	WHERE UTC_OFFSET IS NOT NULL  
		    )  T
		    GROUP by UTC_OFFSET 
		    
		) LOOP

	        -- 추가할 데이터의 시작, 종료일시를 구한다.
	        v_from := wems.fn_ts_str_to_str(CONCAT_WS('', i_exe_base, '010100'), v_row.utc_offset, 'YYYYMMDDHH24'::TEXT, 0, 'YYYYMMDDHH24'::TEXT);
	        v_to := wems.fn_ts_str_to_str(CONCAT_WS('', i_exe_base, '123123'), v_row.utc_offset, 'YYYYMMDDHH24'::TEXT, 0, 'YYYYMMDDHH24'::TEXT);
--	      	RAISE notice 'i_sp_nm[%] i_site_seq[%] Merge TB_STT_ESS_MM Table UTC_OFFSET[%] 시작[%] 종료[%]', i_sp_nm, i_site_seq, v_row.utc_offset, v_from, v_to;
	       
    		-- 시간별 통계 데이터를 SUM 해서 연간 통계 테이블에 Merge      
    		WITH AR AS ( 
				SELECT  * 
				FROM (  
					SELECT	A.*  
								, ROW_NUMBER() OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM DESC) AS RNUM  
					FROM  TB_STT_ESS_TM A 
					WHERE COLEC_TM BETWEEN v_from AND v_to
				)  z
				WHERE RNUM = 1  
			) , B AS (
				SELECT 
					  i_exe_base AS COLEC_YY    
					, v_row.utc_offset AS UTC_OFFSET   
					, A.DEVICE_ID    
					, A.DEVICE_TYPE_CD    
					, SUM(A.PV_PW_H) AS PV_PW_H     
					, TRUNC(SUM(A.PV_PW_PRICE), 2) AS PV_PW_PRICE     
					, TRUNC(SUM(A.PV_PW_CO2), 2) AS PV_PW_CO2  
					, SUM(A.CONS_PW_H) AS CONS_PW_H   
					, TRUNC(SUM(A.CONS_PW_PRICE), 2) AS CONS_PW_PRICE   
					, TRUNC(SUM(A.CONS_PW_CO2), 2) AS CONS_PW_CO2     
					, SUM(A.CONS_GRID_PW_H) AS CONS_GRID_PW_H  
					, TRUNC(SUM(A.CONS_GRID_PW_PRICE), 2) AS CONS_GRID_PW_PRICE     
					, TRUNC(SUM(A.CONS_GRID_PW_CO2), 2) AS CONS_GRID_PW_CO2   
					, SUM(A.CONS_PV_PW_H) AS CONS_PV_PW_H    
					, TRUNC(SUM(A.CONS_PV_PW_PRICE), 2) AS CONS_PV_PW_PRICE   
					, TRUNC(SUM(A.CONS_PV_PW_CO2), 2) AS CONS_PV_PW_CO2  
					, SUM(A.CONS_BT_PW_H) AS CONS_BT_PW_H    
					, TRUNC(SUM(A.CONS_BT_PW_PRICE), 2) AS CONS_BT_PW_PRICE   
					, TRUNC(SUM(A.CONS_BT_PW_CO2), 2) AS CONS_BT_PW_CO2  
					, SUM(A.BT_CHRG_PW_H) AS BT_CHRG_PW_H    
					, TRUNC(SUM(A.BT_CHRG_PW_PRICE), 2) AS BT_CHRG_PW_PRICE   
					, TRUNC(SUM(A.BT_CHRG_PW_CO2), 2) AS BT_CHRG_PW_CO2  
					, SUM(A.BT_DCHRG_PW_H) AS BT_DCHRG_PW_H   
					, TRUNC(SUM(A.BT_DCHRG_PW_PRICE), 2) AS BT_DCHRG_PW_PRICE  
					, TRUNC(SUM(A.BT_DCHRG_PW_CO2), 2) AS BT_DCHRG_PW_CO2  
					, SUM(A.GRID_OB_PW_H) AS GRID_OB_PW_H    
					, TRUNC(SUM(A.GRID_OB_PW_PRICE), 2) AS GRID_OB_PW_PRICE   
					, TRUNC(SUM(A.GRID_OB_PW_CO2), 2) AS GRID_OB_PW_CO2  
					, SUM(A.GRID_TR_PW_H) AS GRID_TR_PW_H    
					, TRUNC(SUM(A.GRID_TR_PW_PRICE), 2) AS GRID_TR_PW_PRICE   
					, TRUNC(SUM(A.GRID_TR_PW_CO2), 2) AS GRID_TR_PW_CO2  
					, SUM(A.GRID_TR_PV_PW_H) AS GRID_TR_PV_PW_H  
					, TRUNC(SUM(A.GRID_TR_PV_PW_PRICE), 2) AS GRID_TR_PV_PW_PRICE    
					, TRUNC(SUM(A.GRID_TR_PV_PW_CO2), 2) AS GRID_TR_PV_PW_CO2  
					, SUM(A.GRID_TR_BT_PW_H) AS GRID_TR_BT_PW_H  
					, TRUNC(SUM(A.GRID_TR_BT_PW_PRICE), 2) AS GRID_TR_BT_PW_PRICE    
					, TRUNC(SUM(A.GRID_TR_BT_PW_CO2), 2) AS GRID_TR_BT_PW_CO2    
				    , MAX(AR.BT_SOC) AS BT_SOC  
					, SUM(A.OUTLET_PW_H) AS OUTLET_PW_H    
					, TRUNC(SUM(A.OUTLET_PW_PRICE), 2) AS OUTLET_PW_PRICE   
					, TRUNC(SUM(A.OUTLET_PW_CO2), 2) AS OUTLET_PW_CO2 
					, SYS_EXTRACT_UTC(NOW()) AS CREATE_DT  
					, SUM(A.PCS_FD_PW_H)			AS PCS_FD_PW_H  
					, TRUNC(SUM(A.PCS_FD_PW_PRICE), 2)		AS PCS_FD_PW_PRICE  
					, TRUNC(SUM(A.PCS_FD_PW_CO2), 2)			AS PCS_FD_PW_CO2
					, SUM(A.PCS_PCH_PW_H)			AS PCS_PCH_PW_H
					, TRUNC(SUM(A.PCS_PCH_PW_PRICE), 2)		AS PCS_PCH_PW_PRICE
					, TRUNC(SUM(A.PCS_PCH_PW_CO2), 2)		AS PCS_PCH_PW_CO2
				FROM 
					 TB_STT_ESS_TM A    
					, TB_BAS_DEVICE B -- 살아있는 장비만 조회하기 위해
					, AR 
				WHERE 	A.COLEC_TM BETWEEN v_from AND v_to 
				AND 		A.DEVICE_ID = B.DEVICE_ID   
				AND 		A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD   
				AND 		A.DEVICE_ID = AR.DEVICE_ID    
				AND 		A.DEVICE_TYPE_CD = AR.DEVICE_TYPE_CD 
				GROUP BY A.DEVICE_ID, A.DEVICE_TYPE_CD   
			), U as (
				UPDATE TB_STT_ESS_YY yy
				SET  
					 PV_PW_H              = B.PV_PW_H               
					, PV_PW_PRICE          = B.PV_PW_PRICE           
					, PV_PW_CO2            = B.PV_PW_CO2             
					, CONS_PW_H            = B.CONS_PW_H             
					, CONS_PW_PRICE        = B.CONS_PW_PRICE         
					, CONS_PW_CO2          = B.CONS_PW_CO2           
					, CONS_GRID_PW_H       = B.CONS_GRID_PW_H        
					, CONS_GRID_PW_PRICE   = B.CONS_GRID_PW_PRICE    
					, CONS_GRID_PW_CO2     = B.CONS_GRID_PW_CO2      
					, CONS_PV_PW_H         = B.CONS_PV_PW_H          
					, CONS_PV_PW_PRICE     = B.CONS_PV_PW_PRICE      
					, CONS_PV_PW_CO2       = B.CONS_PV_PW_CO2        
					, CONS_BT_PW_H         = B.CONS_BT_PW_H          
					, CONS_BT_PW_PRICE     = B.CONS_BT_PW_PRICE      
					, CONS_BT_PW_CO2       = B.CONS_BT_PW_CO2        
					, BT_CHRG_PW_H         = B.BT_CHRG_PW_H          
					, BT_CHRG_PW_PRICE     = B.BT_CHRG_PW_PRICE      
					, BT_CHRG_PW_CO2       = B.BT_CHRG_PW_CO2        
					, BT_DCHRG_PW_H        = B.BT_DCHRG_PW_H         
					, BT_DCHRG_PW_PRICE    = B.BT_DCHRG_PW_PRICE     
					, BT_DCHRG_PW_CO2      = B.BT_DCHRG_PW_CO2       
					, GRID_OB_PW_H         	= B.GRID_OB_PW_H          
					, GRID_OB_PW_PRICE     	= B.GRID_OB_PW_PRICE      
					, GRID_OB_PW_CO2       	= B.GRID_OB_PW_CO2        
					, GRID_TR_PW_H         	= B.GRID_TR_PW_H          
					, GRID_TR_PW_PRICE     	= B.GRID_TR_PW_PRICE      
					, GRID_TR_PW_CO2       	= B.GRID_TR_PW_CO2        
					, GRID_TR_PV_PW_H      	= B.GRID_TR_PV_PW_H       
					, GRID_TR_PV_PW_PRICE  = B.GRID_TR_PV_PW_PRICE   
					, GRID_TR_PV_PW_CO2   = B.GRID_TR_PV_PW_CO2     
					, GRID_TR_BT_PW_H      	= B.GRID_TR_BT_PW_H       
					, GRID_TR_BT_PW_PRICE  = B.GRID_TR_BT_PW_PRICE   
					, GRID_TR_BT_PW_CO2    = B.GRID_TR_BT_PW_CO2   
				    , BT_SOC               		= B.BT_SOC  
				    , OUTLET_PW_H          	= B.OUTLET_PW_H   
				    , OUTLET_PW_PRICE      	= B.OUTLET_PW_PRICE   
				    , OUTLET_PW_CO2        	= B.OUTLET_PW_CO2   
					, CREATE_DT            		= B.CREATE_DT    
					, PCS_FD_PW_H				= B.PCS_FD_PW_H  
					, PCS_FD_PW_PRICE 		= B.PCS_FD_PW_PRICE  
					, PCS_FD_PW_CO2 			= B.PCS_FD_PW_CO2  
					, PCS_PCH_PW_H 			= B.PCS_PCH_PW_H  
					, PCS_PCH_PW_PRICE 		= B.PCS_PCH_PW_PRICE  
					, PCS_PCH_PW_CO2 		= B.PCS_PCH_PW_CO2  	
		      	FROM 	B
				WHERE 	yy.COLEC_YY 			= B.COLEC_YY  
				AND 		yy.UTC_OFFSET 		= B.UTC_OFFSET  
				AND 		yy.DEVICE_ID 			= B.DEVICE_ID  
				AND 		yy.DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
				RETURNING yy.*
			)
			INSERT into TB_STT_ESS_YY (  
				  COLEC_YY  
				, UTC_OFFSET      
				, DEVICE_ID   
				, DEVICE_TYPE_CD   
				, PV_PW_H               
				, PV_PW_PRICE           
				, PV_PW_CO2             
				, CONS_PW_H             
				, CONS_PW_PRICE         
				, CONS_PW_CO2           
				, CONS_GRID_PW_H        
				, CONS_GRID_PW_PRICE    
				, CONS_GRID_PW_CO2      
				, CONS_PV_PW_H          
				, CONS_PV_PW_PRICE      
				, CONS_PV_PW_CO2        
				, CONS_BT_PW_H          
				, CONS_BT_PW_PRICE      
				, CONS_BT_PW_CO2        
				, BT_CHRG_PW_H          
				, BT_CHRG_PW_PRICE      
				, BT_CHRG_PW_CO2        
				, BT_DCHRG_PW_H         
				, BT_DCHRG_PW_PRICE     
				, BT_DCHRG_PW_CO2       
				, GRID_OB_PW_H          
				, GRID_OB_PW_PRICE      
				, GRID_OB_PW_CO2        
				, GRID_TR_PW_H          
				, GRID_TR_PW_PRICE      
				, GRID_TR_PW_CO2        
				, GRID_TR_PV_PW_H       
				, GRID_TR_PV_PW_PRICE   
				, GRID_TR_PV_PW_CO2     
				, GRID_TR_BT_PW_H       
				, GRID_TR_BT_PW_PRICE   
				, GRID_TR_BT_PW_CO2   
			    , BT_SOC   
			    , OUTLET_PW_H   
			    , OUTLET_PW_PRICE  
			    , OUTLET_PW_CO2  
				, CREATE_DT    
				, PCS_FD_PW_H			 
				, PCS_FD_PW_PRICE 	 
				, PCS_FD_PW_CO2 		 
				, PCS_PCH_PW_H 		 
				, PCS_PCH_PW_PRICE 	 
				, PCS_PCH_PW_CO2 		 			
			) 
			SELECT   
				  B.COLEC_YY  
				, B.UTC_OFFSET      
				, B.DEVICE_ID   
				, B.DEVICE_TYPE_CD   
				, B.PV_PW_H               
				, B.PV_PW_PRICE           
				, B.PV_PW_CO2             
				, B.CONS_PW_H             
				, B.CONS_PW_PRICE         
				, B.CONS_PW_CO2           
				, B.CONS_GRID_PW_H        
				, B.CONS_GRID_PW_PRICE    
				, B.CONS_GRID_PW_CO2      
				, B.CONS_PV_PW_H          
				, B.CONS_PV_PW_PRICE      
				, B.CONS_PV_PW_CO2        
				, B.CONS_BT_PW_H          
				, B.CONS_BT_PW_PRICE      
				, B.CONS_BT_PW_CO2        
				, B.BT_CHRG_PW_H          
				, B.BT_CHRG_PW_PRICE      
				, B.BT_CHRG_PW_CO2        
				, B.BT_DCHRG_PW_H         
				, B.BT_DCHRG_PW_PRICE     
				, B.BT_DCHRG_PW_CO2       
				, B.GRID_OB_PW_H          
				, B.GRID_OB_PW_PRICE      
				, B.GRID_OB_PW_CO2        
				, B.GRID_TR_PW_H          
				, B.GRID_TR_PW_PRICE      
				, B.GRID_TR_PW_CO2        
				, B.GRID_TR_PV_PW_H       
				, B.GRID_TR_PV_PW_PRICE   
				, B.GRID_TR_PV_PW_CO2     
				, B.GRID_TR_BT_PW_H       
				, B.GRID_TR_BT_PW_PRICE   
				, B.GRID_TR_BT_PW_CO2   
			    , B.BT_SOC   
			    , B.OUTLET_PW_H   
			    , B.OUTLET_PW_PRICE  
			    , B.OUTLET_PW_CO2  
				, B.CREATE_DT  
				, B.PCS_FD_PW_H			 
				, B.PCS_FD_PW_PRICE 	 
				, B.PCS_FD_PW_CO2 		 
				, B.PCS_PCH_PW_H 		 
				, B.PCS_PCH_PW_PRICE 	 
				, B.PCS_PCH_PW_CO2	
			FROM 	B
			WHERE NOT EXISTS (  SELECT 1 
			    FROM 	U 
				WHERE	COLEC_YY 			= B.COLEC_YY  
					AND 	UTC_OFFSET 		= B.UTC_OFFSET  
					AND 	DEVICE_ID			= B.DEVICE_ID  
					AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
			  )
			 ;
    
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
           
--	      	RAISE notice 'i_sp_nm[%] Merge TB_STT_ESS_YY Table [%]건, UTC_OFFSET[%] 시작[%] 종료[%]', i_sp_nm, v_work_num, v_row.utc_offset, v_from, v_to;
    	
    	END LOOP;
   		
   		-- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS then
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$function$
;

CREATE OR REPLACE FUNCTION wems.sp_del_batch_job_data(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exec_mthd character varying)
 RETURNS void
 LANGUAGE plpgsql
AS $function$

/******************************************************************************
  프로그램ID   : SP_DEL_BATCH_JOB_DATA
  작  성  일   : 2019-10-29
  작  성  자   : 박동환
  프로그램용도 : Spring Batch 작업 테이블들에 불필요한 데이타 정리
  참 고 사 항  :
                 < PROTOTYPE >
                   sp_del_batch_job_data

                 <PARAMETER>
                     1.i_sp_nm		프로시져명
                     2.i_site_seq	사이트일련번호
                     3.i_exec_base	실행기준값(시간대별:YYYYMMDDHH0000 )
                     4.i_exec_mthd	실행사유모드 (A:자동, M:수동)


  ----------------------------------------------------------------------------
  TABLE                                                                CRUD
  ============================================================================
  BATCH_STEP_EXECUTION_CONTEXT							D
  BATCH_STEP_EXECUTION										DR
  BATCH_JOB_EXECUTION_CONTEXT							D
  BATCH_JOB_EXECUTION_PARAMS							D
  BATCH_JOB_EXECUTION										DR
  BATCH_JOB_INSTANCE                                          D            
  ----------------------------------------------------------------------------

  ----------------------------------------------------------------------------
  수정일자       수정자      수정내용
  =========== =========== ====================================================
  20191029    박동환      최초작성
  ----------------------------------------------------------------------------

*****************************************************************************/
-- 실행 스크립트
-- SELECT SP_DEL_BATCH_JOB_DATA('SP_DEL_BATCH_JOB_DATA', 0,  '20181201', 'M');

DECLARE
    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
    v_st_dt                 	TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd          		VARCHAR   	:= '0';   			-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt              	INTEGER   	:= 0;     			-- 적재수
    v_err_msg      			VARCHAR   	:= NULL;  		-- SQL오류메시지내용
    v_err_msg1              VARCHAR   	:= NULL;  		-- 메시지내용1
    v_err_msg2              VARCHAR   	:= NULL;  		-- 메시지내용2
    v_err_msg3              VARCHAR   	:= NULL;  		-- 메시지내용3

   -- (2)업무처리 변수
    v_work_cnt              INTEGER   := 0;     -- 처리건수

BEGIN
    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                                   , i_site_seq
                                   , i_exe_base
                                   , v_st_dt
                                   , i_exec_mthd
                                   );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
        v_rslt_cd := 'A';

		DELETE FROM BATCH_STEP_EXECUTION_CONTEXT WHERE STEP_EXECUTION_ID 
			IN (SELECT STEP_EXECUTION_ID FROM BATCH_STEP_EXECUTION WHERE START_TIME < TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD') );
	
		DELETE FROM BATCH_STEP_EXECUTION WHERE START_TIME < TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD');
	
		DELETE FROM BATCH_JOB_EXECUTION_CONTEXT WHERE JOB_EXECUTION_ID 
			IN (SELECT JOB_EXECUTION_ID FROM BATCH_JOB_EXECUTION WHERE CREATE_TIME < TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD') );
	
		DELETE FROM BATCH_JOB_EXECUTION_PARAMS WHERE JOB_EXECUTION_ID 
			IN (SELECT JOB_EXECUTION_ID FROM BATCH_JOB_EXECUTION WHERE CREATE_TIME < TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD') );
	
		DELETE FROM BATCH_JOB_EXECUTION WHERE CREATE_TIME < TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD');
	
		DELETE FROM BATCH_JOB_INSTANCE WHERE JOB_INSTANCE_ID NOT IN (SELECT JOB_INSTANCE_ID FROM BATCH_JOB_EXECUTION);
	
		-- [배치수행이력] 테이블 정리 
		DELETE FROM TB_SYS_SP_EXE_HST WHERE ST_DT < TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD') - INTERVAL '20 DAY';
		
		GET DIAGNOSTICS v_work_cnt = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_cnt;
        
        /* 실행성공 */
        v_rslt_cd := '1';
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;
        
        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
                               
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                                    , i_site_seq
                                    , i_exe_base
                                    , v_st_dt
                                    , v_rslt_cd
                                    , v_rslt_cnt
                                    , v_err_msg
                                    );
END;
	
$function$
;

CREATE OR REPLACE FUNCTION wems.fn_is_dst(p_srcstr text, p_srcdf text, p_start text, p_end text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_IS_DST
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-05-13   yngwie       1. Created this function.
   2.0        2019-10-07   박동환        postgres 변환

   NOTES:   해당 날짜가 DST 시즌인지 아닌지의 여부를 반환한다.


	Input : p_srcStr - 조회 년월일시
	        p_srcDf -  조회 년월일시 포맷
	        p_start - TB_BAS_TIMEZONE 테이블의  DST_START_EXP
	        p_end - TB_BAS_TIMEZONE 테이블의  DST_END_EXP

    Output : NUMBER 0 - DST 시즌이 아닐 경우, 1 - DST 시즌일 경우

	Usage :
	        SELECT
				A.*
				, FN_IS_DST('2015040100', 'YYYYMMDDHH24', DST_START_EXP, DST_END_EXP)
			FROM
				TB_BAS_TIMEZONE

********************************************************************************/
DECLARE
    v_sql			TEXT;
    v_sql_au 		TEXT;
    v_result 	NUMERIC := -1;

    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3
begin
	
    IF p_start IS NULL OR p_end IS NULL THEN
        v_result := 0;
    ELSE
    	-- DST적용국가(호주)
    	IF POSITION('{{prevYear}}' IN p_start) > 0  THEN
	    	v_sql := FORMAT(
	    			'SELECT SUM(RESULT) FROM (		' || 
						'SELECT CASE WHEN ''%s'' BETWEEN %s AND %s THEN 1 ELSE 0 END AS RESULT ' || 
						'UNION ALL '
						, p_srcStr
						, p_start
						, p_end )	;

            v_sql := REPLACE(v_sql, '{{year}}', SUBSTR(p_srcStr, 1, 4));
            v_sql := REPLACE(v_sql, '{{prevYear}}', TO_CHAR(SUBSTR(p_srcStr, 1, 4)::NUMERIC - 1, 'FM0000'));
            v_sql := REPLACE(v_sql, '{{df}}', p_srcDf);
           
   			v_sql_au   := FORMAT(
					'SELECT CASE WHEN ''%s'' BETWEEN %s AND %s THEN 1 ELSE 0 END AS RESULT) S'
					, p_srcStr
					, p_start
					, p_end )	;

            v_sql_au := REPLACE(v_sql_au, '{{prevYear}}', SUBSTR(p_srcStr, 1, 4));
            v_sql_au := REPLACE(v_sql_au, '{{year}}', TO_CHAR(SUBSTR(p_srcStr, 1, 4)::NUMERIC + 1, 'FM0000'));
            v_sql_au := REPLACE(v_sql_au, '{{df}}', p_srcDf);
            v_sql := CONCAT_WS('', v_sql, v_sql_au);
        ELSE
			v_sql   := FORMAT(
					'SELECT CASE WHEN ''%s'' BETWEEN %s AND %s THEN 1 ELSE 0 END'
					, p_srcStr
					, p_start
					, p_end );
				
            v_sql := REPLACE(v_sql, '{{year}}', SUBSTR(p_srcStr, 1, 4));
            v_sql := REPLACE(v_sql, '{{df}}', p_srcDf);
        END IF;
       
        EXECUTE  v_sql INTO v_result;
       
    END IF;
   
    RETURN v_result;
   
    EXCEPTION WHEN OTHERS then
    	/*
		GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
	    RAISE NOTICE 'FN_IS_DST >>> 오류[%] SQL[%]', v_err_msg, v_sql;
	    */
        RETURN - 1;
       
END;
$function$
;

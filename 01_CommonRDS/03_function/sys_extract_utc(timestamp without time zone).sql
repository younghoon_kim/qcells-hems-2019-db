CREATE OR REPLACE FUNCTION wems.sys_extract_utc(val time without time zone)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
	DECLARE
		v_time  VARCHAR;
		v_result	VARCHAR(10000);	  
	BEGIN
		select to_char(val, 'YYYY-MM-DD HH24:MI:SS') into v_time;
--		SELECT timestamp with time zone v_time AT TIME ZONE 'UTC'  into v_result ;
		return v_time;
	end;
$function$
;

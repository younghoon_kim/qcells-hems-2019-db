CREATE OR REPLACE FUNCTION wems.dec2bin(n numeric)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
 /******************************************************************************
   NAME:       DEC2BIN
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   2.0        2019-10-08     이재형                  postgresql 변환    
   
   NOTES:   Decimal(10진수) to Binary(2진수)

    Input : 

    Output :

 ******************************************************************************/
DECLARE
    binval CHARACTER VARYING(64);
    n2 numeric := n;
BEGIN
    IF n = 0 THEN
        binval := n::TEXT;
    ELSE
        WHILE (n2 > 0) LOOP
            binval := CONCAT_WS('', MOD(n2, 2), binval);
            n2 := TRUNC((n2 / 2::NUMERIC)::NUMERIC);
        END LOOP;
    END IF;
    RETURN binval;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$function$
;

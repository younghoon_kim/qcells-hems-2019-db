CREATE OR REPLACE FUNCTION wems.fn_get_job_hist()
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
DECLARE
    result RECORD;
/*
*****************************************************************************
   NAME:       FN_GET_JOB_HIST
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-12-05   jihoon           1. Created this function.
   2.0        2019-09-24   이재형                            Postgresql 변환

   NOTES:   TB_SYS_JOB_HIST 테이블 최신 데이터 확인

	Input : NULL

    Output : 0 또는 Exception 발생 시 -1

	Usage :
	        FN_GET_JOB_HIST('2014-12-05')

*****************************************************************************
*/
BEGIN

    FOR result IN
    SELECT
        job_seq, job_name, job_msg, exec_dt
        FROM (SELECT
            job_seq, job_name, job_msg, exec_dt
            FROM wems.tb_sys_job_hist AS a
            WHERE 1 = 1 AND job_msg LIKE 'TBR%' --'%TBR%'에서 변경, index 성능여부 추가로 확인 필요
            ORDER BY exec_dt DESC) AS var_sbq
        LIMIT 9
    LOOP
        RAISE DEBUG USING MESSAGE = result.JOB_SEQ|| '|'|| result.JOB_NAME||'|'|| result.JOB_MSG||'|'|| result.EXEC_DT;

    END LOOP;
    RETURN 0;
   
    EXCEPTION
        WHEN others THEN
            RETURN - 1;
END;
$function$
;

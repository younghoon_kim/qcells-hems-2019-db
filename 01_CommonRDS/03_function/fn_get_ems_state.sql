CREATE OR REPLACE FUNCTION wems.fn_get_ems_state(p_uid text DEFAULT 'ALL'::text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
*****************************************************************************
   NAME:       FN_GET_EMS_STATE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-04   stjoo           1. Created this function.
   2.0        2019-09-24   이재형          1. Postgresql 변환

   NOTES:   입력 받은 ESS UID 정보에 해당하는 현재 세션 상태 정보 반환

	Input : p_UID - EMS UID

	Output : session status

	Usage :
	        FN_GET_EMS_STATE('TESTBED-1000')

*****************************************************************************
*/
DECLARE
    last_create_dt CHARACTER VARYING(15);
    sqlStmt CHARACTER VARYING(300);
    isWildCh numeric;
    result RECORD;
	rsltStr TEXT := ''; 
BEGIN

    select position('%' in p_UID) INTO  isWildCh;

    IF p_UID = 'ALL' THEN
        FOR result IN
        SELECT
            device_id, TO_CHAR(create_dt, 'YYYYMMDD HH24:MI:SS') AS create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id
            FROM (SELECT device_id, create_dt, response_cd, 
							ems_ipaddress, ems_out_ipaddress, 
							connector_id, 
						ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY create_dt DESC, create_tm DESC, data_seq DESC) AS rn

                FROM wems.tb_raw_login)
            AS a
            WHERE rn = 1
            --각 디바이스별 create_dt가 가장 최신인것 1개씩만 가져옴
        LOOP
--            RAISE DEBUG USING MESSAGE = 'ESS[ '|| result.DEVICE_ID || ' ] : ';
--            CASE result.RESPONSE_CD
--                WHEN '0' THEN RAISE DEBUG USING MESSAGE = ' ON  :';
--                WHEN '1' THEN RAISE DEBUG USING MESSAGE = ' OFF : Unknown UID';
--                WHEN '2' THEN RAISE DEBUG USING MESSAGE = ' OFF : Password Err';
--                WHEN '3' THEN RAISE DEBUG USING MESSAGE = ' OFF : UID,PASSWD Err';
--                WHEN '128' THEN RAISE DEBUG USING MESSAGE = ' OFF : Session Closed';
--                ELSE RAISE DEBUG USING MESSAGE = ' OFF : Unknown Err';
--            END CASE;
--            RAISE DEBUG USING MESSAGE =' (from '|| result.create_dt|| ')';
--            RAISE DEBUG USING MESSAGE ='  IP['||result.ems_out_ipaddress||']';
--				CASE result.RESPONSE_CD
--                WHEN '0' THEN RAISE DEBUG USING MESSAGE = '  GW['|| result.connector_id|| ']';
--                ELSE RAISE DEBUG USING MESSAGE = '';
--            END CASE;

				rsltStr=rsltStr||'ESS['||result.DEVICE_ID||'] : ';
            CASE result.RESPONSE_CD
                WHEN '0' THEN rsltStr = rsltStr||' ON :';
                WHEN '1' THEN rsltStr = rsltStr||' OFF : Unknown UID';
                WHEN '2' THEN rsltStr = rsltStr||' OFF : Password Err';
                WHEN '3' THEN rsltStr = rsltStr||' OFF : UID,PASSWD Err';
                WHEN '128' THEN rsltStr = rsltStr||' OFF : Session Closed';
                ELSE rsltStr = rsltStr||' OFF : Unknown Err';
            END CASE;
            rsltStr = rsltStr||' (from '||result.create_dt||')';
            rsltStr = rsltStr||' IP['||result.ems_out_ipaddress||']';
				CASE result.RESPONSE_CD
                WHEN '0' THEN rsltStr = rsltStr||' GW['||result.connector_id||']';
                ELSE rsltStr = rsltStr||'';
            END CASE;
				rsltStr = rsltStr||chr(10);
        END LOOP;

    ELSIF p_UID = 'ONLINE' THEN
        

            FOR result IN (
                       SELECT device_id, to_char(create_dt, 'YYYYMMDD HH24:MI:SS') as create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id
                       FROM (
                                 SELECT device_id, create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id
                                 , ROW_NUMBER() OVER(PARTITION BY device_id ORDER BY create_dt DESC , create_tm DESC, data_seq DESC) rn
                                 FROM TB_RAW_LOGIN
                                 WHERE CREATE_DT BETWEEN SYS_EXTRACT_UTC(now() - interval '7 day') AND SYS_EXTRACT_UTC(now())
                             ) a
                       WHERE rn = 1 AND RESPONSE_CD = '0' 
                   )
        LOOP
        
--            RAISE DEBUG USING MESSAGE =  'ESS[ '|| result.DEVICE_ID||' ] : ';
--            RAISE DEBUG USING MESSAGE = ' ON ';
--            RAISE DEBUG USING MESSAGE = ' (from '|| result.create_dt|| ')';
--            RAISE DEBUG USING MESSAGE = '  IP['|| result.ems_out_ipaddress|| ']';
--            RAISE DEBUG USING MESSAGE = '  GW['|| result.connector_id||']';

				rsltStr = rsltStr||'ESS ['||result.DEVICE_ID||'] : ON (from '|| result.create_dt|| ') IP['|| result.ems_out_ipaddress||'] GW['|| result.connector_id||']'||chr(10);
        END LOOP;

    ELSIF p_UID = 'OFFLINE' THEN
       
		FOR result IN (
			SELECT device_id, to_char(create_dt, 'YYYYMMDD HH24:MI:SS') as create_dt, response_cd, ems_ipaddress, ems_out_ipaddress
			FROM (
				SELECT
					device_id, create_dt, response_cd, ems_ipaddress, ems_out_ipaddress
					, ROW_NUMBER() OVER(PARTITION BY device_id ORDER BY create_dt DESC, create_tm DESC, data_seq DESC) rn
				FROM TB_RAW_LOGIN
				WHERE CREATE_DT BETWEEN SYS_EXTRACT_UTC(now() - interval '7 day') AND SYS_EXTRACT_UTC(now())
        	) a
  			WHERE rn = 1 AND RESPONSE_CD <> '0'
		)
		LOOP
--            RAISE DEBUG USING MESSAGE = 'ESS[ '|| result.DEVICE_ID|| ' ] : ';
--            RAISE DEBUG USING MESSAGE = ' OFF ';
--            RAISE DEBUG USING MESSAGE = ' (from '|| result.create_dt|| ')';
--            RAISE DEBUG USING MESSAGE = '  IP['|| result.ems_out_ipaddress|| ']';
			rsltStr = rsltStr||'ESS['|| result.DEVICE_ID||'] : OFF (from '||result.create_dt||') IP['||result.ems_out_ipaddress||']'||chr(10);           
		END LOOP;
		
	ELSIF p_UID IS NOT NULL AND isWildCh = 0 THEN
        FOR result IN
        SELECT
            device_id, TO_CHAR(create_dt, 'YYYYMMDD HH24:MI:SS') AS create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id
            FROM (SELECT
                device_id, create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY create_dt DESC, create_tm DESC, data_seq DESC) AS rn
                FROM wems.tb_raw_login
                WHERE device_id = p_UID) AS a
            WHERE rn < 20
        LOOP
--            RAISE DEBUG USING MESSAGE = 'ESS[ '|| result.DEVICE_ID|| ' ] : ';
--            CASE result.RESPONSE_CD
--                WHEN '0' THEN RAISE DEBUG USING MESSAGE = ' ON  :';
--                WHEN '1' THEN RAISE DEBUG USING MESSAGE = ' OFF : Unknown UID';
--                WHEN '2' THEN RAISE DEBUG USING MESSAGE = ' OFF : Password Err';
--                WHEN '3' THEN RAISE DEBUG USING MESSAGE = ' OFF : UID,PASSWD Err';
--                WHEN '128' THEN RAISE DEBUG USING MESSAGE = ' OFF : Session Closed  ';
--                ELSE RAISE DEBUG USING MESSAGE = ' OFF LINE: Unknown Err';
--            END CASE;
--            RAISE DEBUG USING MESSAGE = ' (from '|| result.create_dt|| ')';
--            RAISE DEBUG USING MESSAGE = '  IP['|| result.ems_out_ipaddress|| ']';
--
--            IF result.RESPONSE_CD = '0' THEN RAISE DEBUG USING MESSAGE = '  GW['|| result.connector_id|| ']';
--            END IF;

            rsltStr = rsltStr||' ESS[ '||result.DEVICE_ID||' ] : ';
            CASE result.RESPONSE_CD
                WHEN '0' THEN rsltStr = rsltStr||' ON  :';
                WHEN '1' THEN rsltStr = rsltStr||' OFF : Unknown UID';
                WHEN '2' THEN rsltStr = rsltStr||' OFF : Password Err';
                WHEN '3' THEN rsltStr = rsltStr||' OFF : UID,PASSWD Err';
                WHEN '128' THEN rsltStr = rsltStr||' OFF : Session Closed  ';
                ELSE rsltStr = rsltStr||' OFF LINE: Unknown Err';
            END CASE;
            rsltStr = rsltStr||' (from '|| result.create_dt|| ')';
            rsltStr = rsltStr||' IP['|| result.ems_out_ipaddress|| ']';
            IF result.RESPONSE_CD = '0' THEN rsltStr = rsltStr||' GW ['||result.connector_id||']';
            END IF;
        		rsltStr = rsltStr||chr(10);
        END LOOP;
		
    ELSIF p_UID IS NOT NULL AND isWildCh > 0 THEN
        FOR result IN
        SELECT
            device_id, TO_CHAR(create_dt, 'YYYYMMDD HH24:MI:SS') AS create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id
            FROM (SELECT
                device_id, create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY create_dt DESC, create_tm DESC, data_seq DESC) AS rn
                FROM wems.tb_raw_login
                WHERE device_id LIKE '%'|| p_UID|| '%'
				)AS a
            WHERE rn = 1
        LOOP
--            RAISE DEBUG USING MESSAGE = 'ESS[ '|| result.DEVICE_ID|| ' ] : ';
--            CASE result.RESPONSE_CD
--                WHEN '0' THEN RAISE DEBUG USING MESSAGE = ' ON  :';
--                WHEN '1' THEN RAISE DEBUG USING MESSAGE = ' OFF : Unknown UID';
--                WHEN '2' THEN RAISE DEBUG USING MESSAGE = ' OFF : Password Err';
--                WHEN '3' THEN RAISE DEBUG USING MESSAGE = ' OFF : UID,PASSWD Err';
--                WHEN '128' THEN RAISE DEBUG USING MESSAGE = ' OFF : Session Closed';
--                ELSE RAISE DEBUG USING MESSAGE = ' OFF : Unknown Err';
--            END CASE;
--            RAISE DEBUG USING MESSAGE = ' (from '|| result.create_dt|| ')';
--            RAISE DEBUG USING MESSAGE = '  IP['|| result.ems_out_ipaddress|| ']';
--            RAISE DEBUG USING MESSAGE = '  GW['|| result.connector_id|| ']';
        
            rsltStr = rsltStr||' ESS['||result.DEVICE_ID||'] : ';
            CASE result.RESPONSE_CD
                WHEN '0' THEN rsltStr = rsltStr||' ON : ';
                WHEN '1' THEN rsltStr = rsltStr||' OFF : Unknown UID';
                WHEN '2' THEN rsltStr = rsltStr||' OFF : Password Err';
                WHEN '3' THEN rsltStr = rsltStr||' OFF : UID,PASSWD Err';
                WHEN '128' THEN rsltStr = rsltStr||' OFF : Session Closed';
                ELSE rsltStr = rsltStr||' OFF : Unknown Err';
            END CASE;
            rsltStr = rsltStr||' (from '|| result.create_dt|| ')';
            rsltStr = rsltStr||' IP['|| result.ems_out_ipaddress|| ']';
            rsltStr = rsltStr||' GW['|| result.connector_id|| ']';
				rsltStr = rsltStr||chr(10);
        END LOOP;
    END IF;

    FOR result IN
    	SELECT
        COUNT(*) AS ess_count
		FROM (
			SELECT
            device_id, create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY create_dt DESC, create_tm DESC, data_seq DESC) AS rn
			FROM wems.tb_raw_login
		) AS a
		WHERE rn = 1 AND a.response_cd = '0'
    LOOP
--        RAISE DEBUG USING MESSAGE = 'TOTAL ONLINE : ';
--        RAISE DEBUG USING MESSAGE = result.ESS_COUNT::TEXT;

        rsltStr = rsltStr||'TOTAL ONLINE : '||result.ESS_COUNT::TEXT||chr(10);
    END LOOP;
	
    RETURN rsltStr;
    EXCEPTION
        WHEN others THEN
            RAISE DEBUG USING MESSAGE = 'ERR CODE : '||TO_CHAR(SQLSTATE);
            RAISE DEBUG USING MESSAGE = 'ERR MESSAGE : '|| SQLERRM;
            RETURN - 1;
END;
/* update TB_OPR_ESS_STATE set OPER_STUS_CD='4' where DEVICE_ID = 'TESTBED-0007'; */
/* select FN_GET_EMS_STATE('TESTBED-0007') from dual; */
/* update TB_OPR_ESS_STATE set OPER_STUS_CD='1' where DEVICE_ID = 'TESTBED-0007' */
$function$
;

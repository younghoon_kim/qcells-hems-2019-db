CREATE OR REPLACE FUNCTION sp_aggr_device(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       SP_AGGR_DEVICE
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-21   yngwie       1. Created this function.
   2.0        2019-09-25   박동환        Postgresql 변환
   2.1        2020-07-24   서정우        site_id추가로 인해 tb_bas_device -> tb_bas_device_pms 를 사용하도록 수정

   NOTES: 	ESS수집데이터에서 수집된 장비상태 갱신
  				(입력시간 무관하게 호출된 시간 기준 처리)
   
    Input : 
            
    Output : 
    
	Usage : SELECT SP_AGGR_DEVICE('SP_AGGR_DEVICE', 0, '20191024', 'M')
    
******************************************************************************/ 
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
   	v_update_id		VARCHAR := NULL;
  	v_exe_tm			TIMESTAMP := SYS_EXTRACT_UTC( TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 12)||'00', 'YYYYMMDDHH24MISS'));
  
BEGIN
    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

	    -- RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] START[%] i_exe_base[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base;
        ------------------------------------------------
        -- 1. ESS 의 Login 정보를 가지고 TB_BAS_DEVICE_PMS 테이블의 Model, Firmware Version 정보 갱신
        ------------------------------------------------
        
	  	SELECT ENC_VARCHAR_INS('SYSTEM', 11, 'SDIENCK', 'UPDATE_ID', 'TB_BAS_DEVICE_PMS')
	  	INTO 	v_update_id;
	   
       	WITH R AS ( 
	        SELECT  
	            A.DEVICE_ID, B.DEVICE_TYPE_CD, A.RESPONSE_CD
	            , A.EMS_MODEL_NM, A.PCS_MODEL_NM, A.BMS_MODEL_NM
	            , A.EMS_UPDATE_DT
	            , A.EMS_VER, A.PCS_VER, A.BMS_VER
	            , A.EMS_OUT_IPADDRESS, A.EMS_IPADDRESS
	--          , CASE WHEN C.DEVICE_ID IS NULL THEN 'Y' ELSE 'N' END FW_INS_FLAG
	        FROM (  
	            SELECT   
	                ROW_NUMBER() OVER (PARTITION BY A.DEVICE_ID ORDER BY A.CREATE_DT DESC, A.CREATE_TM DESC, A.DATA_SEQ DESC) SEQ  
	                , A.DEVICE_ID, A.CREATE_TM, A.RESPONSE_CD
	                , A.EMS_MODEL_NM, A.PCS_MODEL_NM, A.BMS_MODEL_NM
	                , A.EMS_UPDATE_DT
	                , A.EMS_VER, A.PCS_VER, A.BMS_VER
	                , A.EMS_OUT_IPADDRESS, A.EMS_IPADDRESS
	            FROM    TB_RAW_LOGIN A
	            WHERE 	A.RESPONSE_CD = '0'
	                AND 	A.CREATE_DT > v_exe_tm - INTERVAL '5 MINUTE'
	                AND 	A.EMS_IPADDRESS <> 'BY SP_CHECK_ESS_LOGIN'
	            ) A  
	            , TB_BAS_DEVICE_PMS B
	        WHERE   A.SEQ = 1
	            AND 	A.DEVICE_ID = B.DEVICE_ID
		) 
        UPDATE TB_BAS_DEVICE_PMS D
        SET 
        	 EMS_MODEL_NM = R.EMS_MODEL_NM 
	        , PCS_MODEL_NM =R.PCS_MODEL_NM
	        , BMS_MODEL_NM =R.BMS_MODEL_NM 
	        , EMS_VER = R.EMS_VER 
	        , PCS_VER = R.PCS_VER 
	        , BMS_VER =R.BMS_VER
	        , IP_ADDR = R.EMS_OUT_IPADDRESS
	        , EMS_IP_ADDR = R.EMS_IPADDRESS
	        , EMS_UPDATE_DT = R.EMS_UPDATE_DT
	        , UPDATE_ID = v_update_id
	        , UPDATE_DT = SYS_EXTRACT_UTC(now()) 
	    FROM 		R
        WHERE		D.DEVICE_ID = R.DEVICE_ID
        AND 			D.DEVICE_TYPE_CD = R.DEVICE_TYPE_CD
        ;		
        
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	   	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		-- RAISE notice 'i_sp_nm[%] Update TB_BAS_DEVICE_PMS Table [%]건', i_sp_nm, v_work_num;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
   	return v_rslt_cd;

END;
$function$
;

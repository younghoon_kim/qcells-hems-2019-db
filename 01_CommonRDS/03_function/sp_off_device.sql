CREATE OR REPLACE FUNCTION wems.sp_off_device(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       SP_OFF_DEVICE
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-09-04   yngwie       1. Created this function.

   NOTES: TB_BAS_OFF_DEVICE 테이블의 WORK_FLAG 가 N 인 장비의 이력, 통계 등의 데이터를 삭제한다.
   
    Input : 
            
    Output : 
    
******************************************************************************/ 
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_row           	record; 
  
BEGIN
	-- 3시간마다 수행
	IF SUBSTR(i_exe_base, 9, 2)::NUMERIC % 3 != 0 THEN
		RETURN	'1';
	END IF ;


    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

	    --RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] START[%] i_exe_base[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base;

	   	UPDATE TB_BAS_OFF_DEVICE 
	   	SET 	UPDATE_DT = SYS_EXTRACT_UTC(NOW()) 
	  	WHERE WORK_FLAG = 'N'; 	--AND UPDATE_DT IS NULL';
	  
      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      	--RAISE notice 'i_sp_nm[%] UPDATE TB_BAS_OFF_DEVICE UNDER PROCESSING : [%]건', i_sp_nm, v_work_num;

    	FOR v_row IN ( 
			SELECT	DEVICE_ID
			          , DEVICE_TYPE_CD 
	       	FROM    	TB_BAS_OFF_DEVICE 
	       	WHERE	WORK_FLAG = 'N' 
	       	AND 		UPDATE_DT IS NOT NULL 
	  		LIMIT 100 

		) LOOP
      		--RAISE notice 'i_sp_nm[%] Delete ESS, DEVICE_ID[%] DEVICE_TYPE_CD[%]',  i_sp_nm, v_row.DEVICE_ID, v_row.DEVICE_TYPE_CD;
                
            -- 제어, 설정, 현황  이력 정보 삭제
            DELETE FROM TB_OPR_ESS_CONFIG_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_ESS_CONFIG_HIST : [%]',  i_sp_nm, v_work_num;
            
            -- 16.02.23 jihoon 추가
            DELETE FROM TB_OPR_ESS_CONFIG WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_ESS_CONFIG : [%]',  i_sp_nm, v_work_num;
                            
            -- 16.02.23 jihoon 추가
            DELETE FROM TB_BAS_ELPW_UNIT_PRICE WHERE ELPW_PROD_CD = v_row.DEVICE_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_BAS_ELPW_UNIT_PRICE : [%]',  i_sp_nm, v_work_num;

        
            DELETE FROM TB_PLF_DEVICE_FW_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_PLF_DEVICE_FW_HIST : [%]',  i_sp_nm, v_work_num;

            DELETE FROM TB_OPR_OPER_CTRL_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_OPER_CTRL_HIST : [%]',  i_sp_nm, v_work_num;
        
            DELETE FROM TB_OPR_CTRL_RESULT WHERE DEVICE_ID = v_row.DEVICE_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_CTRL_RESULT : [%]',  i_sp_nm, v_work_num;
        
            DELETE FROM TB_OPR_OPER_LOGFILE_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_OPER_LOGFILE_HIST : [%]',  i_sp_nm, v_work_num;
        
/** 2018-12-03
       임시적용 : 용량 문제로 인해 삭제로직 임시제거
                  (용량이 많아서 한시간 후 TBR-21003: Snapshot is too old. 오류 남.
                   TB_OPR_ESS_STATE_HIST테이블 용량 주기  6개월 이하 설정 후 주석 제거)
*/  
            DELETE FROM TB_OPR_ESS_STATE_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_ESS_STATE_HIST : [%]',  i_sp_nm, v_work_num;
                                              

/** 2015.11.12
        임시적용: 용량 문제로 인해 테이블 관리에서 삭제 하도록 수정.         
                v_sql := 'DELETE FROM TB_OPR_ALARM_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_OPR_ALARM_HIST : ' || v_cnt);
                
                v_sql := 'DELETE FROM TB_OPR_ESS_PREDICT_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_OPR_ESS_PREDICT_HIST : ' || v_cnt);
                
                -- 통계 정보 삭제 
                v_sql := 'DELETE FROM TB_STT_ESS_TM WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_ESS_TM : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_ESS_DD WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_ESS_DD : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_ESS_MM WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_ESS_MM : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_ESS_YY WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_ESS_YY : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_PREDICT_TM WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_PREDICT_TM : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_PREDICT_DD WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_PREDICT_DD : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_PREDICT_MM WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_PREDICT_MM : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_PREDICT_ADVICE_DD WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_PREDICT_ADVICE_DD : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_RANK_DD WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_RANK_DD : ' || v_cnt);
                
                v_sql := 'DELETE FROM TB_STT_RANK_MM WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_RANK_MM : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_QUITY_WRTY_DD WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_QUITY_WRTY_DD : ' || v_cnt);
                
                
                -- RAW 정보 삭제

                v_sql := 'DELETE FROM TB_RAW_ESS_INFO WHERE DEVICE_ID = v_row.DEVICE_ID;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_RAW_ESS_INFO : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_RAW_EMS_INFO WHERE DEVICE_ID = v_row.DEVICE_ID;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_RAW_EMS_INFO : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_RAW_PCS_INFO WHERE DEVICE_ID = v_row.DEVICE_ID;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_RAW_PCS_INFO : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_RAW_RAC_INFO WHERE DEVICE_ID = v_row.DEVICE_ID;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_RAW_RAC_INFO : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_RAW_LOGIN WHERE DEVICE_ID = v_row.DEVICE_ID;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_RAW_LOGIN : ' || v_cnt);
    **/     
        
            -- 유지보수, 기타 정보 삭제
            DELETE FROM TB_OPR_MTNC_FILE 
            WHERE MTNC_SEQ IN ( 
                SELECT 	MTNC_SEQ 
               FROM 		TB_OPR_MTNC 
              where		DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD
            )  ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_MTNC_FILE : [%]',  i_sp_nm, v_work_num;
            
            DELETE FROM TB_OPR_MTNC WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_MTNC : [%]',  i_sp_nm, v_work_num;
        
            DELETE FROM TB_OPR_VOC_FILE 
            WHERE VOC_SEQ IN ( 
                SELECT 	VOC_SEQ 
               	FROM 	TB_OPR_VOC 
              	WHERE 	DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD
            )  ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_VOC_FILE : [%]',  i_sp_nm, v_work_num;
            
            DELETE FROM TB_OPR_VOC_REPL 
            WHERE VOC_SEQ IN ( 
                	SELECT 	VOC_SEQ 
                	FROM 	TB_OPR_VOC 
                	WHERE 	DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD
            ) ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_VOC_REPL : [%]',  i_sp_nm, v_work_num;
        
            DELETE FROM TB_OPR_VOC WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_VOC : [%]',  i_sp_nm, v_work_num;
           
      	
            UPDATE TB_BAS_OFF_DEVICE 
           	SET 	WORK_FLAG = 'Y', 
           			UPDATE_DT = SYS_EXTRACT_UTC(NOW()) 
           	WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Update TB_BAS_OFF_DEVICE WORK_FLAG : [%]',  i_sp_nm, v_work_num;


            INSERT INTO TB_BAS_OFF_DEVICE_HIST 
           	SELECT * 
          	FROM TB_BAS_OFF_DEVICE  
          	WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] INSERT INTO TB_BAS_OFF_DEVICE_HIST : [%]',  i_sp_nm, v_work_num;

            DELETE FROM TB_BAS_OFF_DEVICE WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_BAS_OFF_DEVICE : [%]',  i_sp_nm, v_work_num;
	   END LOOP;
	   
 --     	RAISE notice 'i_sp_nm[%] End Loop !!! ', i_sp_nm;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$function$
;

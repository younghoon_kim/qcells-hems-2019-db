CREATE OR REPLACE FUNCTION wems.dec_varchar_sel(cal_val character varying, int_val character varying, sdienck character varying, table_name character varying, cal_name character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
 /******************************************************************************
  NAME:       DEC_VARCHAR_SEL
  PURPOSE:

  REVISIONS:
  Ver        Date           Author           Description
  ---------  ---------------  ---------------  ------------------------------------
  1.0        2019-11-22  박동환      		최초작성
  
  	NOTES:   암호해제

	USAGE:
			SELECT DEC_VARCHAR_SEL('Valent_Elio', '10', 'SDIENCK', 'TB_BAS_USER', 'user_id');

*****************************************************************************/
	DECLARE
		err_context	TEXT;
	
	BEGIN		
		IF sdienck IS NULL OR sdienck = '' THEN 
			sdienck := 'SDIENCK';
		END IF;
	
		RETURN (	SELECT FN_GET_DECRYPT(cal_val, sdienck)	);

		EXCEPTION WHEN OTHERS THEN 
			 GET STACKED DIAGNOSTICS err_context = MESSAGE_TEXT;
		RETURN err_context;			 
	END;
$function$
;

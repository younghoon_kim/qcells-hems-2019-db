CREATE OR REPLACE FUNCTION wems.fn_get_split(p_str text, p_field_number integer, p_delim text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_GET_SPLIT
   PURPOSE: 문자열을 구분자로 자른 N번째 문자 반환
                Postgresql에서 제공하는 함수를 사용 권고

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-29   yngwie      Created this function.
   2.0        2019-09-25   박동환       Postgresql 변환

   NOTES:   입력받은 문자열을 구분자를 기준으로 잘라 반환한다.

	Input : p_str - 문자열
	        p_field_number - 구분자를 기준으로 문자열의 N번째 문자
	        p_delim - 구분자

    Output : VARCHAR - 구분자를 기준으로 자른 문자열

	Usage :
	        SELECT FN_GET_SPLIT('123|45|678', 3, '|');

******************************************************************************/
DECLARE
	o_result	TEXT := null;

BEGIN
	SELECT 	SPLIT_PART(p_str, p_delim, p_field_number)
	INTO 		o_result;

    RETURN o_result;

END;
$function$
;

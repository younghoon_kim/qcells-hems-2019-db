CREATE OR REPLACE FUNCTION wems.decode(param text, con1 text, val1 text, val0 text)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
	DECLARE
		v_result	VARCHAR(10000);	
	
	BEGIN
		select CASE param WHEN  con1 THEN val1 ELSE val0 end into v_result;
		RETURN v_result;
	END;
$function$
;

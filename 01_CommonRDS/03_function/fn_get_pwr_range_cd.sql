CREATE OR REPLACE FUNCTION wems.fn_get_pwr_range_cd(p_type text, p_value numeric, p_pv_value numeric, p_pv_stus_cd text, p_grid_stus_cd text, p_bt_stus_cd text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
*****************************************************************************
   NAME:       FN_GET_PWR_RANGE_CD
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-05-21   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                            Postgresql 변환
   NOTES:   입력받은  수치에 대한 파워 범위 코드를 반환한다.

	Input : p_type - 에너지 흐름 방향
	                 GRID_LOAD :
	                 GRID_ESS :
	                 ESS_GRID :
	                 ESS_LOAD :
	                 PV_ESS :
	        p_value - 전력값
	        p_pv_value - ESS 로 in, out 되는 경우 pv_pw 값이 필요함. 그 외는  null

			- PV 발전 파워 : 해당 값 [0, 1~499, 500 ~ 3999, 4000 ~ ](W)
			- ESS-부하 파워 : 해당 값/ PV출력 [0, 1~19,20~69,70~]('%) (PV출력이 0인 경우, 큼으로 처리)
			- ESS-Grid 파워 : 해당 값/ PV출력 [0, 1~19,20~69,70~]('%) (PV출력이 0인 경우, 큼으로 처리)
			- Grid-부하 파워 : 해당 값 [0, 1~999, 1000 ~ 5000, 5000 ~ ](W)

			p_pv_stus_cd - PV_STUS_CD (0: 정지, 1:발전)
			p_grid_stus_cd - GRID_STUS_CD (0:소비, 1:매전, 2:정지)
			p_bt_stus_cd - BT_STUS_CD (0:방전, 1:충전, 2:정지)

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_PWR_RANGE_CD('ESS_GRID', 36, 2, '1', '0', '0') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_result CHARACTER VARYING(1);
    v_value numeric;
    v_pv_value numeric;
BEGIN
    v_pv_value := ABS(p_pv_value);
    v_value := ABS(p_value);

    IF p_type = 'GRID_LOAD' THEN
        IF p_grid_stus_cd = '0' THEN
            IF v_value > 0 AND v_value < 1000 THEN
                v_result := 'S';
            ELSIF v_value >= 1000 AND v_value < 5000 THEN
                v_result := 'M';
            ELSIF v_value >= 5000 THEN
                v_result := 'L';
            END IF;
        ELSE
            v_result := NULL;
        END IF;
    ELSIF p_type = 'GRID_ESS' THEN
        IF p_grid_stus_cd = '0' AND p_bt_stus_cd = '1' THEN
            IF v_value > 0 AND v_value < 1000 THEN
                v_result := 'S';
            ELSIF v_value >= 1000 AND v_value < 5000 THEN
                v_result := 'M';
            ELSIF v_value >= 5000 THEN
                v_result := 'L';
            END IF;
        ELSE
            v_result := NULL;
        END IF;
    ELSIF p_type = 'ESS_GRID' THEN
        IF p_bt_stus_cd = '0' AND p_grid_stus_cd = '1' THEN
            IF v_pv_value = 0 THEN
                v_result := 'L';
            ELSIF v_value / v_pv_value > 0 AND v_value / v_pv_value < 20 THEN
                v_result := 'S';
            ELSIF v_value / v_pv_value >= 20 AND v_value / v_pv_value < 70 THEN
                v_result := 'M';
            ELSIF v_value / v_pv_value >= 70 THEN
                v_result := 'L';
            END IF;
        ELSE
            v_result := NULL;
        END IF;
    ELSIF p_type = 'ESS_LOAD' THEN
        IF p_bt_stus_cd = '0' THEN
            IF v_pv_value = 0 THEN
                v_result := 'L';
            ELSIF v_value / v_pv_value > 0 AND v_value / v_pv_value < 20 THEN
                v_result := 'S';
            ELSIF v_value / v_pv_value >= 20 AND v_value / v_pv_value < 70 THEN
                v_result := 'M';
            ELSIF v_value / v_pv_value >= 70 THEN
                v_result := 'L';
            END IF;
        ELSE
            v_result := NULL;
        END IF;
    ELSIF p_type = 'PV_ESS' THEN
        IF p_pv_stus_cd = '1' AND p_bt_stus_cd = '1' THEN
            IF v_pv_value > 0 AND v_pv_value < 500 THEN
                v_result := 'S';
            ELSIF v_pv_value >= 500 AND v_pv_value < 4000 THEN
                v_result := 'M';
            ELSIF v_pv_value >= 4000 THEN
                v_result := 'L';
            END IF;
        ELSE
            v_result := NULL;
        END IF;
    END IF;
    RETURN v_result;
   
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$function$
;

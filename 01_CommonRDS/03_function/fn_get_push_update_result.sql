CREATE OR REPLACE FUNCTION wems.fn_get_push_update_result(p_val text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
*****************************************************************************
   NAME:       FN_GET_PUSH_UPDATE_RESULT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-09-04   yngwie       1. Created this function.

   NOTES:   RESULT_CD 를 가지고 EMS, PCS, BMS 의 Firmware Update 결과를 반환한다.

	Input : p_val - TB_OPR_CTRL_RESULT.RESULT_CD

    Output : VARCHAR - 결과 값, 구분자 | 를 기준으로 EMS|PCS|BMS 의 결과코드

	Usage :


*****************************************************************************
*/
DECLARE
    i_result CHARACTER VARYING(100) := '||';
BEGIN
    IF p_val = '0' THEN
        i_result := '0|0|0';
    ELSIF p_val = '1' THEN
        i_result := '1|1|1';
    ELSIF p_val = '2' THEN
        i_result := '2||';
    ELSIF p_val = '4' THEN
        i_result := '|4|';
    ELSIF p_val = '5' THEN
        i_result := 'NA|NA|NA';
    ELSIF p_val = '8' THEN
        i_result := '||8';
    ELSIF p_val = '16' THEN
        i_result := '16||';
    ELSIF p_val = '32' THEN
        i_result := '|32|';
    ELSIF p_val = '64' THEN
        i_result := '||64';
    ELSIF p_val = '112' THEN
        i_result := '16|32|64';
    ELSIF p_val = '255' THEN
        i_result := '255|255|255';
    ELSIF p_val = '6' THEN
        i_result := '2|4|';
    ELSIF p_val = '34' THEN
        i_result := '2|32|';
    ELSIF p_val = '10' THEN
        i_result := '2||8';
    ELSIF p_val = '66' THEN
        i_result := '2||64';
    ELSIF p_val = '20' THEN
        i_result := '16|4|';
    ELSIF p_val = '48' THEN
        i_result := '16|32|';
    ELSIF p_val = '24' THEN
        i_result := '16||8';
    ELSIF p_val = '80' THEN
        i_result := '16||64';
    ELSIF p_val = '12' THEN
        i_result := '|4|8';
    ELSIF p_val = '68' THEN
        i_result := '|4|64';
    ELSIF p_val = '40' THEN
        i_result := '|32|8';
    ELSIF p_val = '96' THEN
        i_result := '|32|64';
    ELSIF p_val = '14' THEN
        i_result := '2|4|8';
    ELSIF p_val = '70' THEN
        i_result := '2|4|64';
    ELSIF p_val = '42' THEN
        i_result := '2|32|8';
    ELSIF p_val = '98' THEN
        i_result := '2|32|64';
    ELSIF p_val = '84' THEN
        i_result := '16|4|64';
    ELSIF p_val = '28' THEN
        i_result := '16|4|8';
    ELSIF p_val = '56' THEN
        i_result := '16|32|8';
    END IF;
    RETURN i_result;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$function$
;

/******************************************************************************
  NAME:       FN_SAVE_TB_BAS_DEVICE_BATTERY_HISTORY
  PURPOSE:    베터리 정보 변경이력을 관리

  REVISIONS:
  Ver        Date        Author           Description
  ---------  ----------  ---------------  ------------------------------------
  1.0                                      Created this function.
  1.1        2020-12-30  younghoon         삽입,업데이트시 update_dt가 올바른 시간을 가지고있음으로, update_dt를 삽입


  NOTES:       tb_bas_device_battery에 update 또는 insert시 tb_bas_device_battery_hist에 insert됨


*****************************************************************************/

create or replace function fn_save_tb_bas_device_battery_history() returns trigger AS
$save_tb_bas_device_battery_history$
declare
    count int;
begin
    -- 베터리 정보 변경이력을 관리
    -- tb_bas_device_battery에 update 또는 insert시 tb_bas_device_battery_hist에 insert됨
    -- todo: implement
    if (TG_OP = 'INSERT') then
        insert into tb_bas_device_battery_hist(site_id, id, device_id, product_model_nm, module, device_type_cd,
                                               capacity, prn_device_id, create_dt)
        values (new.site_id, new.id, new.device_id, new.product_model_nm, new.module, new.device_type_cd, new.capacity,
                new.prn_device_id, new.update_dt);
        raise notice 'Insert : % : % : % : % : % : % : %: % :%'
            ,new.site_id, new.id, new.device_id, new.product_model_nm, new.module, new.device_type_cd,new.capacity,new.prn_device_id, new.update_dt;
        return new;
    elsif (TG_OP = 'UPDATE') then
        insert into tb_bas_device_battery_hist(site_id, id, device_id, product_model_nm, module, device_type_cd,
                                               capacity, prn_device_id, create_dt)
        values (new.site_id, new.id, new.device_id, new.product_model_nm, new.module, new.device_type_cd, new.capacity,
                new.prn_device_id, new.update_dt);
        raise notice 'Update : % : % : % : % : % : % : %: % :%'
            ,new.site_id, new.id, new.device_id, new.product_model_nm, new.module, new.device_type_cd,new.capacity,new.prn_device_id, new.update_dt;
        return new;
    end if;
    return null;
end;
$save_tb_bas_device_battery_history$ language plpgsql;


-- trigger 생성
create trigger save_tb_bas_battery_product_history
    after insert or update
    on tb_bas_device_battery
    for each row
execute procedure fn_save_tb_bas_device_battery_history();
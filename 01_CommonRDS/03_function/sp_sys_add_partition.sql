CREATE OR REPLACE FUNCTION wems.sp_sys_add_partition(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       SP_SYS_ADD_PARTITION
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-09-15   yngwie       1. Created this function.
   2.0        2019-09-24   박동환        Postgresql 변환

   NOTES:   일/월별 파티션 자동 생성
              매일 GMT 01시에 수행.
   
    Input : 
	        
	Output : 
	
******************************************************************************/	
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
   -- 미리 생성할 일수
--    v_daily_size 		INTEGER := 14;
--    v_monthly_size INTEGER := 2;
   	
    v_row           	record; 
   	v_len				INTEGER := 0;
   	v_tz_offset		NUMERIC := TO_CHAR(NOW() - TIMEZONE('UTC', NOW()), 'HH24')::NUMERIC;
   	v_sql				VARCHAR   := NULL;
   	v_nextDateStr	VARCHAR   := NULL; 
   	v_start_value	VARCHAR   := NULL;
   	v_end_value		VARCHAR   := NULL;
  
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

--	    RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] tz_offset[%] START[%]', i_sp_nm, i_site_seq, v_tz_offset, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	    FOR v_row IN ( 
			WITH PLF AS (
				SELECT M.TABLE_NM
						, M.PARTITION_TYPE
						, M.STD_COL_NM
						, M.STD_COL_TYPE
						, 'PK'||SUBSTRING(M.TABLE_NM, 3, LENGTH(M.TABLE_NM)-2) INDEX_NM
				   		, MAX(CHILD.RELNAME)		LAST_PARTITION
					FROM 	PG_INHERITS
				    JOIN 		PG_CLASS PARENT ON PG_INHERITS.INHPARENT = PARENT.OID
				    JOIN 		PG_CLASS CHILD   ON PG_INHERITS.INHRELID   = CHILD.OID
				    JOIN 		TB_PLF_TBL_META M ON (PARENT.RELNAME= LOWER(M.TABLE_NM)  )
				    WHERE	M.PARTITION_TYPE IS NOT NULL
				    GROUP BY  M.TABLE_NM, M.PARTITION_TYPE, M.STD_COL_NM	, M.STD_COL_TYPE
			), BAS AS (
				SELECT 	*
							, STRING_TO_ARRAY(LAST_PARTITION, '_') RELARR
				FROM 	PLF
			), PT AS (
				SELECT *
						, RELARR[ARRAY_UPPER(RELARR,1)] 					  LAST_PT_DATE_STR
						, CHAR_LENGTH(RELARR[ARRAY_UPPER(RELARR,1)]) FMNUM
						, CASE WHEN PARTITION_TYPE = 'M' THEN 
									AGE( NOW()+INTERVAL '2 MONTH', TO_TIMESTAMP(RELARR[ARRAY_UPPER(RELARR,1)] , 'YYYYMM'))
							  	 ELSE 
							  	 	AGE( NOW()+INTERVAL '14 DAY', TO_TIMESTAMP(RELARR[ARRAY_UPPER(RELARR,1)], 'YYYYMMDD'))
						  		END AGES
				FROM BAS
			)
			SELECT TABLE_NM as TABLE_NAME
					, LAST_PARTITION
					, LAST_PT_DATE_STR
					, CASE WHEN PARTITION_TYPE = 'M' THEN 
										EXTRACT(YEAR FROM AGES)*12 + EXTRACT(MONTH FROM AGES)
						  	 ELSE
						  	 			EXTRACT(DAY FROM NOW()+INTERVAL '14 DAY' - TO_TIMESTAMP(LAST_PT_DATE_STR, 'YYYYMMDD'))
							 END DIFF_SIZE
					, CASE WHEN FMNUM = 4 THEN 'YYYY' 
						  WHEN FMNUM = 6 THEN 'YYYYMM' 
						  WHEN FMNUM = 8 THEN 'YYYYMMDD' 
						  WHEN FMNUM = 10 THEN 'YYYYMMDDHH24'
						  ELSE  ''
				    END AS NEXT_DATE_FMT
					, FN_LONG2CHAR(LAST_PARTITION) as BOUND
					, PARTITION_TYPE
					, STD_COL_NM
					, STD_COL_TYPE
					, INDEX_NM
			 FROM PT
			 
		) LOOP
			v_len := LENGTH(v_row.BOUND) - 2;
			IF v_row.DIFF_SIZE > 0 THEN
	            	--  2015.03.30 yngwie
	            	-- 가장 최근에 만들어진 파티션의 날짜(LAST_PT_DATE_STR) 부터 파티션이 생성되어야 하는 날짜 (v_max...Str) 까지 loop 를 돌며 파티션을 생성한다.

	            	FOR v_loopCnt IN 1..v_row.DIFF_SIZE LOOP
--						RAISE notice 'i_sp_nm[%] v_loopCnt{%] TABLE_NAME[%] LAST_PT_DATE_STR[%] NEXT_DATE_FMT[%] tz_offset[%] DIFF_SIZE[%]', 
--											i_sp_nm, v_loopCnt, v_row.TABLE_NAME, v_row.LAST_PT_DATE_STR, v_row.NEXT_DATE_FMT, v_tz_offset, v_row.DIFF_SIZE ;

						-- 다음 번 파티션을 만들 날짜 문자열을 생성한다.
	            		IF v_row.PARTITION_TYPE = 'D' THEN
	            			v_nextDateStr := TO_CHAR(TO_DATE(v_row.LAST_PT_DATE_STR, v_row.NEXT_DATE_FMT) + concat(v_loopCnt,' DAY')::INTERVAL, v_row.NEXT_DATE_FMT);
	            		ELSE
	            			v_nextDateStr := TO_CHAR(TO_DATE(v_row.LAST_PT_DATE_STR, v_row.NEXT_DATE_FMT) + concat(v_loopCnt,' MONTH')::INTERVAL, v_row.NEXT_DATE_FMT);
	            		END IF;

		--				RAISE notice 'i_sp_nm[%] v_loopCnt{%] TABLE_NAME[%] LAST_PT_DATE_STR[%] NEXT_DATE_FMT[%] tz_offset[%]', i_sp_nm, v_loopCnt, v_row.TABLE_NAME, v_row.LAST_PT_DATE_STR, v_row.NEXT_DATE_FMT, v_tz_offset ;

		                IF v_row.STD_COL_TYPE = 'VARCHAR' THEN

	--		                RAISE notice 'i_sp_nm[%] v_loopCnt{%] STD_COL_TYPE[%]', i_sp_nm, v_loopCnt, v_row.STD_COL_TYPE ;

		                    IF v_row.PARTITION_TYPE = 'D' THEN
--				                RAISE notice 'i_sp_nm[%] v_loopCnt{%] D >>> v_nextDateStr[%]', i_sp_nm, v_loopCnt, v_nextDateStr ;
				               
		                        IF v_len = 8 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 DAY'::INTERVAL, 'YYYYMMDD');
		                        ELSIF v_len = 10 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 DAY'::INTERVAL, 'YYYYMMDDHH24');
		                        ELSIF v_len = 14 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 DAY'::INTERVAL, 'YYYYMMDDHH24MISS');
		                        END IF;
		                    ELSIF v_row.PARTITION_TYPE = 'M' THEN
	--			                RAISE notice 'i_sp_nm[%] v_loopCnt{%] M >>> v_nextDateStr[%]', i_sp_nm, v_loopCnt, v_nextDateStr ;
				               
		                        IF v_len = 6 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 MONTH'::INTERVAL, 'YYYYMM');
		                        ELSIF v_len = 8 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 MONTH'::INTERVAL, 'YYYYMMDD');
		                        ELSIF v_len = 10 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 MONTH'::INTERVAL, 'YYYYMMDDHH24');
		                        ELSIF v_len = 14 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 MONTH'::INTERVAL, 'YYYYMMDDHH24MISS');
		                        END IF;
		                    END IF;

		                ELSIF v_row.STD_COL_TYPE = 'TIMESTAMP' then
			               IF v_row.PARTITION_TYPE = 'D' THEN
   		                       v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 DAY'::INTERVAL, 'YYYY-MM-DD HH24:MI:SS');
				               v_nextDateStr := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT), 'YYYY-MM-DD');
		                    ELSIF v_row.PARTITION_TYPE = 'M' THEN
			                   --  ('2019-09-01 00:00:00') TO ('2019-10-01 00:00:00')
   		                       v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 MONTH'::INTERVAL, 'YYYY-MM-DD HH24:MI:SS');
				               v_nextDateStr := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT), 'YYYY-MM');
		                    END IF;
		                   
--			                RAISE notice 'i_sp_nm[%] TIMESTAMP >>> v_tz_offset{%] v_nextDateStr[%]  v_end_value[%]', i_sp_nm, v_tz_offset, v_nextDateStr, v_end_value ;
		                END IF;
		            
		               	v_start_value := v_nextDateStr;
		               	IF LENGTH(v_start_value) < LENGTH(v_end_value) THEN
			               v_start_value := v_start_value || SUBSTRING(v_end_value, LENGTH(v_start_value)+1);
						END IF;
					
						/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
						Oracle Style 불가 : ALTER TABLE TB_STT_ESS_TM ATTACH PARTITION PT_STT_ESS_TM_20190829 VALUES LESS THAN ('2019083000');
						
			           	CREATE TABLE tb_opr_ess_state_hist_pt_opr_ess_state_hist_20190827 PARTITION OF tb_opr_ess_state_hist FOR VALUES FROM ('20190827') TO ('20190828');							
						--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
	--	            	RAISE notice 'i_sp_nm[%] v_loopCnt{%] M >>> TABLE_NAME[%] nextDateStr[%] value_date[%]', i_sp_nm, v_loopCnt, v_row.TABLE_NAME, v_nextDateStr, v_end_value ;
						v_sql := FORMAT(	'CREATE TABLE PT_%s_%s PARTITION OF %s FOR VALUES FROM(%s) TO(%s);'
									, REPLACE(v_row.TABLE_NAME, 'TB_', '')
									, REPLACE(v_nextDateStr, '-', '')
									, v_row.TABLE_NAME
									, CHR(39)||v_start_value||CHR(39)
									, CHR(39)||v_end_value||CHR(39)
								);
						
						--RAISE notice 'i_sp_nm[%] ADD Partition :  SQL[%]', i_sp_nm, v_sql;
						EXECUTE v_sql;		            
			           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
				       	v_rslt_cnt := v_rslt_cnt + v_work_num;	
		
	            	END LOOP;
	            END IF;
	   END LOOP;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   	/***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$function$
;

CREATE OR REPLACE FUNCTION wems.sp_req_logfile_result(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       SP_REQ_LOGFILE_RESULT
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-08-26   yngwie       1. Created this function.
   2.0        2019-10-02   박동환        Postgresql 변환

   NOTES:   로그파일 요청 명령 처리
  				(입력시간 무관하게 호출된 시간 기준 처리)
   
    Input : 
	        
	Output : 

	Usage : SELECT SP_REQ_LOGFILE_RESULT('SP_REQ_LOGFILE_RESULT', 0, '20191024123500', 'M')
		
******************************************************************************/	
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
  
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

	    --RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] START[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	    -- UI 에서 로그파일 요청 명령 전송 후 M2M 에서 그 결과를 TB_OPR_CTRL_RESULT 테이블에 insert 한 건들만 가져온다.
	    -- ESS 응답 대기중인 건만 찾는다 (CMD_REQ_STATUS_CD = '50')
	    
	   	WITH R AS ( 
			SELECT	A.REQ_ID
						, A.DEVICE_ID
						, A.CREATE_DT
						, A.RESULT_CD
			FROM		TB_OPR_CTRL_RESULT A
						, TB_OPR_OPER_LOGFILE_HIST B
			WHERE	A.CMD_TYPE_CD = '25'
				AND 	A.REQ_ID = B.REQ_ID
				AND 	A.REQ_TYPE_CD = 'M2M'
				AND 	B.CMD_REQ_STATUS_CD = '50'
			
		)
		UPDATE TB_OPR_OPER_LOGFILE_HIST A
		SET 		CMD_REQ_STATUS_CD = CASE WHEN R.RESULT_CD = '0' THEN '51' ELSE '55' END
		FROM		R
		WHERE   A.REQ_ID = R.REQ_ID
		    AND 	A.DEVICE_ID = R.DEVICE_ID
		;

		GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
       	v_rslt_cnt := v_rslt_cnt + v_work_num;	
	  	--RAISE NOTICE 'i_sp_nm[%] Update TB_OPR_OPER_LOGFILE_HIST[%]건', i_sp_nm, v_work_num;

	    -- TB_OPR_CTRL_RESULT 에 같은 req_id 인 레코드가 10분 이상 존재하지 않으면
	    -- TB_OPR_OPER_LOGFILE_HIST.CMD_REQ_STATUS_CD = '55' 로 update 한다.
	   	WITH R AS ( 
			SELECT	 A.REQ_ID
						, A.DEVICE_ID
						, A.CHK_FLAG
						, A.CNT
			FROM (
				SELECT	 A.REQ_ID
							, A.DEVICE_ID
							, A.CREATE_DT
							, CASE WHEN A.CREATE_DT < ( SYS_EXTRACT_UTC(NOW()) - interval '10 MINUTE' ) THEN 'Y' ELSE 'N' END CHK_FLAG
							, (SELECT COUNT(*) FROM TB_OPR_CTRL_RESULT C WHERE C.REQ_ID = A.REQ_ID) AS CNT
				FROM		 TB_OPR_CTRL_RESULT A
							, TB_OPR_OPER_LOGFILE_HIST B
				WHERE	A.REQ_ID = B.REQ_ID
					AND 	B.CMD_REQ_STATUS_CD = '50'
			) A
			WHERE	A.CHK_FLAG = 'Y' AND 	A.CNT = 1
			GROUP BY	A.REQ_ID	, A.DEVICE_ID, A.CHK_FLAG, A.CNT
		) 
		UPDATE TB_OPR_OPER_LOGFILE_HIST A
		SET 		CMD_REQ_STATUS_CD = '55'
		FROM 	R
		WHERE   A.REQ_ID = R.REQ_ID
		    AND 	A.DEVICE_ID = R.DEVICE_ID
		;

		GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
       	v_rslt_cnt := v_rslt_cnt + v_work_num;	
	  	--RAISE NOTICE 'i_sp_nm[%] Update TB_OPR_OPER_LOGFILE_HIST for long term Fail [%]건', i_sp_nm, v_work_num;
            
        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE NOTICE 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
   	return v_rslt_cd;

END;
$function$
;

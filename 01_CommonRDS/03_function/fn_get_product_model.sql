CREATE OR REPLACE FUNCTION fn_get_product_model(p_value text, p_column text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
*****************************************************************************
   NAME$       FN_GET_PRODUCT_MODEL
   PURPOSE$

   REVISIONS$
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-03-31   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                            Postgresql 변환
   2.1        2020-07-24   서정우       site_id적용에 따른 테이블 변경 반영
   NOTES$   입력받은  DEVICE_ID 의TB_PLF_CODE_INFO 테이블의 p_column 값을 반환

	Input $ p_value - DEVICE_ID
            p_column - TB_PLF_CODE_INFO 의 컬럼 명

    Output $ VARCHAR

	Usage $
	        SELECT FN_GET_PRODUCT_MODEL('AR00460036Z114827015X', 'CODE') FROM DUAL;
	        SELECT FN_GET_PRODUCT_MODEL('AR00460036Z114827015X', 'REF_5') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(200);
    v_sql text;
BEGIN

   
  	v_sql := 'SELECT  case when ($1=''REF_5'' and SUBSTR($2, 1, 2)=''HS'')  
              then (select cntry_cd from tb_bas_site
                         where site_id = (select site_id from tb_bas_device_pms where device_id=$3) ) else '||p_column||' end
    FROM TB_PLF_CODE_INFO 
    WHERE 
     GRP_CD = ''PRODUCT_MODEL_CD'' 
     AND CODE    = CASE WHEN SUBSTR($4, 1, 2)=''HS'' THEN SUBSTR($5,1,8) WHEN SUBSTR($6, 1, 2)!=''AR'' THEN ''HHQC''||SUBSTR($7, 8, 4) ELSE CODE END 
     AND REF_2   = CASE WHEN SUBSTR($8, 1, 2)=''HS'' THEN REF_2 WHEN SUBSTR($9, 1, 2) != ''AR'' THEN (case when SUBSTR($10, 8, 4) = ''4601'' then ''0046'' when  SUBSTR($11, 8, 4) = ''5001'' then ''0050'' end ) ELSE SUBSTR($12, 3, 4)                 
	END 
     AND REF_3   = CASE WHEN SUBSTR($13, 1, 2)=''HS'' THEN REF_3 WHEN SUBSTR($14, 1, 2) != ''AR'' THEN ''0000'' ELSE FN_GET_CAPACITY($15, ''CD_DESC'') END 
    AND CD_DESC = CASE WHEN SUBSTR($16, 1, 2)=''HS'' THEN CD_DESC WHEN SUBSTR($17, 1, 2)!=''AR'' THEN CD_DESC  ELSE (CASE WHEN SUBSTR($18, LENGTH($19))= ''O'' THEN ''H'' ELSE SUBSTR($20, LENGTH($21)) END) END '
	;
	
	EXECUTE v_sql INTO v_value USING p_column, p_value, p_value, p_value, p_value,p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value;
	
    RETURN v_value;
	
END;
$function$
;

CREATE OR REPLACE FUNCTION wems.decode(param text, con1 integer, val1 text, con2 integer, val2 text)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
	DECLARE
		v_result	VARCHAR(10000);	
	
	BEGIN
		select CASE param WHEN  con1 THEN val1 WHEN  con2 THEN val2  end into v_result;
		RETURN v_result;
	END;
$function$
;

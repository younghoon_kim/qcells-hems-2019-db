CREATE OR REPLACE PROCEDURE wems.sp_log(p_jobseq double precision, p_jobname text, p_jobmsg text)
 LANGUAGE plpgsql
AS $procedure$
   
/******************************************************************************
   NAME:       SP_LOG
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-12   yngwie       Created this function.
   2.0		   2019-09-19  박동환         Postgresql 버젼적용

   NOTES:   PROCEDURE, PACKAGE 의 수행이력을 TB_SYS_JOB_HIST 테이블에 추가한다.

	Input : p_jobSeq  - 순번
	        p_jobName - Job Procedure Name
	        p_jobMsg - Job 수행 메세지

	Usage : CALL SP_LOG(nextval('wems.sq_sys_job_hist'), 'PKG_OPR_ESS.SP_AGGR_STATE', 'START');

******************************************************************************/
DECLARE
    v_err_msg         VARCHAR   := NULL;  			  -- SQL오류메시지내용
    v_err_msg1        VARCHAR   := NULL;  			  -- 메시지내용1
    v_err_msg2        VARCHAR   := NULL;  			  -- 메시지내용2
    v_err_msg3        VARCHAR   := NULL;  			  -- 메시지내용3
   
BEGIN
    
	INSERT INTO wems.tb_sys_job_hist (job_seq, job_name, job_msg)
    VALUES (p_jobSeq, p_jobName, aws_oracle_ext.substr(p_jobMsg, 1, 3200));
--    COMMIT;
   
    EXCEPTION
        WHEN others then
                GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT;

				v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
           
            INSERT INTO wems.tb_sys_job_hist (job_seq, job_name, job_msg)
            VALUES (nextval('wems.sq_sys_job_hist'), 'SP_LOG', v_err_msg);
 --           COMMIT;
END;
$procedure$
;

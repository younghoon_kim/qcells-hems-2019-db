CREATE OR REPLACE FUNCTION wems.sp_unlock_user(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       SP_UNLOCK_USER
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-08   yngwie       Created this function.
   2.0        2019-10-01   이재형        convert postgresql
   
   NOTES:   패스워드 5회 입력 오류로 잠긴계정을 일정시간 후 잠금해제
   				(입력시간 무관하게 호출된 시간 기준 처리)
	Input :

	Usage : SELECT SP_UNLOCK_USER('SP_UNLOCK_USER', 0, '20191024', 'M')

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_last_dt			TIMESTAMP := NULL;  					-- 최종작업시간
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
	v_record       	record;
	v_timeout 		numeric;

BEGIN
    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
  
		v_rslt_cd := 'A';

	    SELECT   CAST(CODE AS NUMERIC)
		INTO 		v_timeout 
		FROM 	TB_PLF_CODE_INFO     
		WHERE 	GRP_CD = 'ACC_LOCK_TIME_CD';
	
	 	FOR v_record IN
				SELECT USER_ID, LOCK_START_DT 
				FROM TB_BAS_USER 
				WHERE LOCK_FLAG = ENC_VARCHAR_INS('Y', 12, 'SDIENCK', 'TB_BAS_USER', 'LOCK_FLAG') 
		LOOP
			IF v_record.LOCK_START_DT+ (v_timeout||' MINUTE')::interval  < SYS_EXTRACT_UTC(now()) THEN
				
				UPDATE TB_BAS_USER  
	            SET  LOCK_FLAG = NULL    
	           		, LOCK_START_DT = NULL      
	           		, LAST_UNLOCK_DT = SYS_EXTRACT_UTC(now())
	           		, UPDATE_ID = ENC_VARCHAR_INS('SYSTEM', 12, 'SDIENCK', 'TB_BAS_USER', 'UPDATE_ID')      
	           		, UPDATE_DT = SYS_EXTRACT_UTC(now()) 
	           	WHERE USER_ID = v_record.USER_ID;	
			
			END IF;
		END LOOP;

     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS then
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   	/***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;


$function$
;

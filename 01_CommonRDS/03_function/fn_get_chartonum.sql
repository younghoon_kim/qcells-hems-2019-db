CREATE OR REPLACE FUNCTION wems.fn_get_chartonum(p_value text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
*****************************************************************************
   NAME:       FN_GET_CHARTONUM
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-04-17   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                  Postgresql 변환
   
   NOTES:   RAW 테이블 컬럼 중 문자열로 입력되지만 수치형 데이터로 처리해야 하는 경우 사용한다.

	Input : p_value - VARCHAR

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_CHARTONUM('NA') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(100);
   
BEGIN
    SELECT
        CASE p_value
            WHEN 'NA' THEN NULL
            ELSE p_value
        END
        INTO STRICT v_value;
    RETURN v_value;
   
END;
$function$
;

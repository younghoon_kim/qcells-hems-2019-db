CREATE OR REPLACE PROCEDURE wems.sp_rank_daily(p_dt text)
 LANGUAGE plpgsql
AS $procedure$
/******************************************************************************
   NAME:       SP_RANK_DAILY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-30   yngwie       Created this function.
   2.0        2019-09-24   박동환        Postgresql 변환

   NOTES:

            Source Table : TB_STT_ESS_DD
            Target Table : TB_STT_RANK_DD

    Input : p_dt VARCHAR 집계 대상 년월일

	Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_RANK_DAILY';
    v_log_seq 		NUMERIC := 0;		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';	-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
   
BEGIN
	SELECT SUBSTR(p_dt, 1, 8) into p_dt;

	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';

	    -- RAISE NOTICE 'v_log_fn[%] v_log_seq[%] START[%]', v_log_fn, v_log_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	   	--  일별 통계 테이블 데이터 중 p_dt 에 해당하는 레코드 Delete
       	DELETE FROM TB_STT_RANK_DD WHERE COLEC_DD = p_dt;
     
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       

--	    RAISE NOTICE 'v_log_fn[%] v_log_seq[%] DELETE TB_STT_RANK_DD Table : [%] p_dt[%]', v_log_fn, v_log_seq, v_work_num, p_dt;
	    
	    INSERT INTO TB_STT_RANK_DD ( 
		 COLEC_DD 
		 , UTC_OFFSET 
		 , RANK_TYPE_CD 
		 , DEVICE_ID 
		 , DEVICE_TYPE_CD 
		 , RANK_PER 
		 , RANK_NO 
		 , TOTAL_CNT 			
		 , CNTRY_CD 
		 , CREATE_DT 
		 ) 
/* 2015.04.09 건물 정보 폐기에 따른 수정
        SELECT '
        	A.COLEC_DD '
        	, A.UTC_OFFSET '
        	, K.RANK_TYPE_CD '
        	, A.DEVICE_ID '
        	, A.DEVICE_TYPE_CD '
        	, CASE  '
        	     WHEN K.RANK_TYPE_CD = ''RT01'' THEN TRUNC( A.RANK_RT01 / A.TOTAL_RT01 * 100 )  '
        	     WHEN K.RANK_TYPE_CD = ''RT02'' THEN TRUNC( A.RANK_RT02 / A.TOTAL_RT02 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT03'' THEN TRUNC( A.RANK_RT03 / A.TOTAL_RT03 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT04'' THEN TRUNC( A.RANK_RT04 / A.TOTAL_RT04 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT05'' THEN TRUNC( A.RANK_RT05 / A.TOTAL_RT05 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT06'' THEN TRUNC( A.RANK_RT06 / A.TOTAL_RT06 * 100 )   '
        	  ELSE NULL END AS RANK_PER '
		    , CASE '
		    	 WHEN K.RANK_TYPE_CD = ''RT01'' THEN A.RANK_RT01 '
		    	 WHEN K.RANK_TYPE_CD = ''RT02'' THEN A.RANK_RT02 '
		    	 WHEN K.RANK_TYPE_CD = ''RT03'' THEN A.RANK_RT03 '
		    	 WHEN K.RANK_TYPE_CD = ''RT04'' THEN A.RANK_RT04 '
		    	 WHEN K.RANK_TYPE_CD = ''RT05'' THEN A.RANK_RT05 '
		    	 WHEN K.RANK_TYPE_CD = ''RT06'' THEN A.RANK_RT06 '
		       ELSE NULL END AS RANK_NO '
		    , CASE '
		    	 WHEN K.RANK_TYPE_CD = ''RT01'' THEN A.TOTAL_RT01 '
		    	 WHEN K.RANK_TYPE_CD = ''RT02'' THEN A.TOTAL_RT02 '
		    	 WHEN K.RANK_TYPE_CD = ''RT03'' THEN A.TOTAL_RT03 '
		    	 WHEN K.RANK_TYPE_CD = ''RT04'' THEN A.TOTAL_RT04 '
		    	 WHEN K.RANK_TYPE_CD = ''RT05'' THEN A.TOTAL_RT05 '
		    	 WHEN K.RANK_TYPE_CD = ''RT06'' THEN A.TOTAL_RT06 '
		       ELSE NULL END AS TOTAL_CNT '
        	, A.CNTRY_CD '
        	, SYS_EXTRACT_UTC(SYSTIMESTAMP) AS CREATE_DT '
        FROM  '
        ( '
        	SELECT '
        		A.DEVICE_ID '
        		, A.DEVICE_TYPE_CD '
        		, S.COLEC_DD '
        		, S.UTC_OFFSET '
        		, C.CNTRY_CD '
        		, S.GRID_OB_PW_H '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET ORDER BY S.GRID_OB_PW_H) RANK_RT01 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT01 '
        	    , C.BLD_AREA_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.BLD_AREA_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT02 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.BLD_AREA_CD) TOTAL_RT02 '
        		, C.TOT_RSPP_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.TOT_RSPP_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT03 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.TOT_RSPP_CD) TOTAL_RT03 '
        		, C.RGR_RSPP_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.RGR_RSPP_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT04 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.RGR_RSPP_CD) TOTAL_RT04 '
        		, C.ELPW_PROD_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.ELPW_PROD_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT05 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.ELPW_PROD_CD) TOTAL_RT05 '
        	    , C.BLD_AREA_VAL '
        	    , TRUNC(S.GRID_OB_PW_H / C.BLD_AREA_VAL, 2) '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET ORDER BY TRUNC(S.GRID_OB_PW_H / C.BLD_AREA_VAL, 2) ) RANK_RT06 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT06 '
        	FROM '
        		( '
        			SELECT  '
        				ROW_NUMBER() OVER (PARTITION BY C.DEVICE_ID, C.DEVICE_TYPE_CD ORDER BY C.CREATE_DT DESC) SEQ '
        				, A.USER_ID '
        				, C.DEVICE_ID '
        				, C.DEVICE_TYPE_CD '
        			FROM '
        				TB_BAS_USER A '
        				, TB_BAS_USER_DEVICE B '
        				, TB_BAS_DEVICE C '
        			WHERE '
        				A.AGREE_FLAG = xx1.enc_varchar_ins(''Y'', 11, ''SDIENCK'', ''AGREE_FLAG'', ''TB_BAS_USER'') '
        				AND A.USER_ID = B.USER_ID '
        				AND B.DEVICE_ID = C.DEVICE_ID '
        				AND B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD '
        		) A '
        		, TB_BAS_BLD_DEVICE B '
        		, TB_BAS_BLD C '
        		, TB_STT_ESS_DD S '
        	WHERE A.SEQ = 1 '
        		AND S.COLEC_DD = :1 '
        		AND A.DEVICE_ID = B.DEVICE_ID '
        		AND A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD '
        		AND B.BLD_ID = C.BLD_ID '
        		AND A.DEVICE_ID = S.DEVICE_ID '
        		AND A.DEVICE_TYPE_CD = S.DEVICE_TYPE_CD '
        		AND C.BLD_AREA_CD IS NOT NULL '
        		AND C.TOT_RSPP_CD IS NOT NULL '
        		AND C.RGR_RSPP_CD IS NOT NULL '
        		AND C.BLD_AREA_VAL IS NOT NULL '
        ) A '
        , ( '
        	SELECT ''RT01'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT02'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT03'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT04'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT05'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT06'' RANK_TYPE_CD FROM DUAL '
        ) K '
*/
		SELECT 
			A.COLEC_DD 
			, A.UTC_OFFSET 
			, K.RANK_TYPE_CD 
			, A.DEVICE_ID 
			, A.DEVICE_TYPE_CD 
			, CASE  
			     WHEN K.RANK_TYPE_CD = 'RT01' THEN TRUNC( A.RANK_RT01 / A.TOTAL_RT01 * 100 ) 
			     WHEN K.RANK_TYPE_CD = 'RT02' THEN TRUNC( A.RANK_RT02 / A.TOTAL_RT02 * 100 ) 
			     WHEN K.RANK_TYPE_CD = 'RT05' THEN TRUNC( A.RANK_RT05 / A.TOTAL_RT05 * 100 ) 
			     WHEN K.RANK_TYPE_CD = 'RT06' THEN TRUNC( A.RANK_RT06 / A.TOTAL_RT06 * 100 ) 
			  ELSE NULL END AS RANK_PER 
		    , CASE 
		    	 WHEN K.RANK_TYPE_CD = 'RT01' THEN A.RANK_RT01 
		    	 WHEN K.RANK_TYPE_CD = 'RT02' THEN A.RANK_RT02 
		    	 WHEN K.RANK_TYPE_CD = 'RT05' THEN A.RANK_RT05 
		    	 WHEN K.RANK_TYPE_CD = 'RT06' THEN A.RANK_RT06 
		       ELSE NULL END AS RANK_NO 
		    , CASE 
		    	 WHEN K.RANK_TYPE_CD = 'RT01' THEN A.TOTAL_RT01 
		    	 WHEN K.RANK_TYPE_CD = 'RT02' THEN A.TOTAL_RT02 
		    	 WHEN K.RANK_TYPE_CD = 'RT05' THEN A.TOTAL_RT05 
		    	 WHEN K.RANK_TYPE_CD = 'RT06' THEN A.TOTAL_RT06 
		       ELSE NULL END AS TOTAL_CNT 
			, A.CNTRY_CD 
			, SYS_EXTRACT_UTC(now()) AS CREATE_DT 
		FROM  
		( 
			SELECT 
				A.DEVICE_ID 
				, A.DEVICE_TYPE_CD 
				, S.COLEC_DD 
				, S.UTC_OFFSET 
				, A.CNTRY_CD 
				, S.GRID_OB_PW_H 
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET ORDER BY S.GRID_OB_PW_H) RANK_RT01 
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT01 
			    , A.BLD_AREA_CD 
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.BLD_AREA_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT02 
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.BLD_AREA_CD) TOTAL_RT02 
				, A.ELPW_PROD_CD 
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.ELPW_PROD_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT05 
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.ELPW_PROD_CD) TOTAL_RT05 
			    , A.BLD_AREA_VAL 
			    , TRUNC(S.GRID_OB_PW_H / A.BLD_AREA_VAL, 2) 
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET ORDER BY TRUNC(S.GRID_OB_PW_H / A.BLD_AREA_VAL, 2) ) RANK_RT06 
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT06 
			FROM 
				( 
					SELECT  
						ROW_NUMBER() OVER (PARTITION BY C.DEVICE_ID, C.DEVICE_TYPE_CD ORDER BY C.CREATE_DT DESC) SEQ 
						, A.USER_ID 
						, A.BLD_AREA_VAL
						, A.BLD_AREA_CD
						, C.DEVICE_ID 
						, C.DEVICE_TYPE_CD
						, C.CNTRY_CD
						, C.ELPW_PROD_CD
					FROM 
						TB_BAS_USER A 
						, TB_BAS_USER_DEVICE B 
						, TB_BAS_DEVICE C 
					WHERE 
						A.AGREE_FLAG = ENC_VARCHAR_INS('Y', 11, 'SDIENCK', 'AGREE_FLAG', 'TB_BAS_USER')
						AND A.USER_ID = B.USER_ID
						AND B.DEVICE_ID = C.DEVICE_ID
						AND B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD 
				) A 
				, TB_STT_ESS_DD S
			WHERE A.SEQ = 1 
				AND S.COLEC_DD = p_dt 
				AND A.DEVICE_ID = S.DEVICE_ID 
				AND A.DEVICE_TYPE_CD = S.DEVICE_TYPE_CD 
				AND A.BLD_AREA_CD IS NOT NULL 
				AND A.BLD_AREA_VAL IS NOT NULL
		) A 
		, (
			SELECT 'RT01' RANK_TYPE_CD 
			UNION ALL
			SELECT 'RT02' RANK_TYPE_CD  
			UNION ALL 
			SELECT 'RT05' RANK_TYPE_CD 
			UNION ALL
			SELECT 'RT06' RANK_TYPE_CD 
		) K
	    ;
	    
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		
--		RAISE NOTICE 'v_log_fn[%] Insert TB_STT_RANK_DD Table [%]건', v_log_fn, v_work_num;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$procedure$
;

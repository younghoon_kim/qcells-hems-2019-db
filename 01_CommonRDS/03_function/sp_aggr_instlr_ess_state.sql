CREATE OR REPLACE FUNCTION wems.sp_aggr_instlr_ess_state(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$

/******************************************************************************
   NAME:       SP_AGGR_INSTLR_ESS_STATE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-07-07   yngwie       1. Created this function.
   1.1        2016-01-05   jkm1020     1. ESM (EMS Sleep Mode) 추가 - ESM_CNT, ESM_PER
   2.0        2019-09-25   박동환        Postgresql 변환

   NOTES: 	인스톨러별 ESS 운용 현황 집계
  				(입력시간 무관하게 호출된 시간 기준 처리)

    Input :

    Output :

	Usage : SELECT SP_AGGR_INSTLR_ESS_STATE('SP_AGGR_INSTLR_ESS_STATE', 0, '201910221237', 'M');

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
   
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
  
		v_rslt_cd := 'A';

	    --RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] i_exe_base[%] START[%]', i_sp_nm, i_site_seq, i_exe_base, now();

	   -- 집계 테이블 삭제
	    DELETE FROM TB_OPR_INSTLR_ESS_STATE;
	   
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       

      	--RAISE NOTICE 'i_sp_nm[%] Delete TB_OPR_INSTLR_ESS_STATE Table [%]건', i_sp_nm, v_work_num;

        
        WITH R AS ( 
            SELECT 	 A.USER_ID 
--            			, A.AUTH_TYPE_CD
						, C.OPER_STUS_CD
						, B.INSTL_DT
						, DEC_VARCHAR_SEL(AUTH_TYPE_CD, 10, 'SDIENCK', 'TB_BAS_USER', 'AUTH_TYPE_CD') as AUTH_TYPE_CD
            FROM      TB_BAS_USER A 
                		, TB_BAS_DEVICE B 
                		, TB_OPR_ESS_STATE C 
            WHERE 	A.USER_ID = B.INSTL_USER_ID 
                AND 	B.DEVICE_ID = C.DEVICE_ID 
                AND 	B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD
        ), A AS ( 
            SELECT   USER_ID 
                , COUNT(*) AS TOT_CNT 
                , SUM(CASE WHEN OPER_STUS_CD = '0' THEN 1 ELSE 0 END) AS NML_CNT 
                , SUM(CASE WHEN OPER_STUS_CD = '1' THEN 1 ELSE 0 END) AS WARN_CNT 
                , SUM(CASE WHEN OPER_STUS_CD = '2' THEN 1 ELSE 0 END) AS ERR_CNT 
                , SUM(CASE WHEN OPER_STUS_CD = '3' THEN 1 ELSE 0 END) AS NO_SIG_CNT 
                , SUM(CASE WHEN OPER_STUS_CD IS NULL OR OPER_STUS_CD = '4' THEN 1 ELSE 0 END) AS STD_CNT 
				/* 박동환, 2019.09.25 아래 주석처리된 원본의 조회기간이 이해가 안됨
                , SUM(CASE WHEN INSTL_DT BETWEEN TO_CHAR(SYS_EXTRACT_UTC(NOW()) - (7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 1), 'YYYYMMDD') 
                                                   		AND TO_CHAR(SYS_EXTRACT_UTC(NOW()) - (7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 7), 'YYYYMMDD') 
                */
                , SUM(CASE WHEN INSTL_DT BETWEEN TO_CHAR(SYS_EXTRACT_UTC(NOW()) - CONCAT((7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 1)::varCHAR, 'DAY' )::interval, 'YYYYMMDD')
                                    		AND TO_CHAR(SYS_EXTRACT_UTC(NOW()) - CONCAT((7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 7)::varCHAR, 'DAY' )::interval, 'YYYYMMDD')
                      THEN 1 ELSE 0 END) AS RCT_INSTL_CNT 
                , SUM(CASE WHEN OPER_STUS_CD = '5' THEN 1 ELSE 0 END) AS ESM_CNT                          
            FROM 	R 
            WHERE 	AUTH_TYPE_CD = '20' 
            GROUP BY USER_ID 
        ) 
       	INSERT INTO TB_OPR_INSTLR_ESS_STATE (
            USER_ID      
            , COLEC_DD   
            , TOT_CNT        
            , NML_CNT        
            , NML_PER        
            , WARN_CNT   
            , WARN_PER   
            , ERR_CNT        
            , ERR_PER        
            , NO_SIG_CNT     
            , NO_SIG_PER     
            , STD_CNT        
            , STD_PER        
            , RCT_INSTL_CNT 
            , CREATE_DT 
            , ESM_CNT 
            , ESM_PER 
        ) 
        SELECT 
            A.USER_ID 
            , TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'YYYYMMDD') AS COLEC_DD 
            , A.TOT_CNT 
            , A.NML_CNT 
            , ROUND(A.NML_CNT / A.TOT_CNT * 100, 1) AS NML_PER 
            , A.WARN_CNT 
            , ROUND(A.WARN_CNT / A.TOT_CNT * 100, 1) AS WARN_PER 
            , A.ERR_CNT 
            , ROUND(A.ERR_CNT / A.TOT_CNT * 100, 1) AS ERR_PER 
            , A.NO_SIG_CNT 
            , TRUNC(A.NO_SIG_CNT / A.TOT_CNT * 100, 1) AS NO_SIG_PER 
            , A.STD_CNT 
            , ROUND(A.STD_CNT / A.TOT_CNT * 100, 1) AS STD_PER 
            , RCT_INSTL_CNT 
            , SYS_EXTRACT_UTC(NOW()) AS CREATE_DT 
            , A.ESM_CNT        
            , ROUND(A.ESM_CNT / A.TOT_CNT * 100, 1) AS ESM_PER 
        FROM  A 
        ;
        
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       

      	--RAISE NOTICE 'i_sp_nm[%] Insert TB_OPR_INSTLR_ESS_STATE Table [%]건', i_sp_nm, v_work_num;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );

   	return v_rslt_cd;

END;
$function$
;

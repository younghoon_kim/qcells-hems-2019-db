CREATE OR REPLACE FUNCTION wems.fn_get_offset(p_srcstr text, p_srcdf text, p_start text, p_end text, p_utcoffset numeric)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_GET_OFFSET
   PURPOSE:   DST적용 OFFSET조회

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-05-13   yngwie       1. Created this function.
   2.0        2019-10-07   박동환        postgres 변환

   NOTES:   입력받은  날짜와 산식을 가지고 DST 가 적용된 날짜인지 아닌지를 계산하여 해당 OFFSET를 반환한다..


	Input : p_srcStr - 조회 년월일시
	        p_srcDf -  조회 년월일시 포맷
	        p_start - TB_BAS_TIMEZONE 테이블의  DST_START_EXP
	        p_end - TB_BAS_TIMEZONE 테이블의  DST_END_EXP
	        p_utcOffset - TB_BAS_TIMEZONE 테이블의  UTC_OFFSET

    Output : NUMBER offset - DST 시즌일 경우 p_utcOffset + 1

	Usage :
	        SELECT
				A.*
				, FN_GET_OFFSET('2015040100', 'YYYYMMDDHH24', A.DST_START_EXP, A.DST_END_EXP, A.UTC_OFFSET)
			FROM
				TB_BAS_TIMEZONE A

********************************************************************************/
DECLARE
    v_sql 		TEXT := NULL;
    v_sql_au 	TEXT := NULL;
    v_result 	NUMERIC := -99;

    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3


BEGIN

    IF p_start IS NULL OR p_end IS NULL THEN
        v_result := p_utcOffset;
    ELSE
    	-- DST적용국가(호주)
    	IF POSITION('{{prevYear}}' IN p_start) > 0 THEN

	    	v_sql := FORMAT(
	    			'SELECT CASE WHEN SUM(RESULT) > 0 THEN %s ELSE %s END FROM (	' 					||
						'SELECT CASE WHEN ''%s'' BETWEEN %s AND %s THEN 1 ELSE 0 END AS RESULT '	||
						'UNION ALL '
	    				, p_utcOffset + 1
						, p_utcOffset
						, p_srcStr
						, p_start
						, p_end )	;

			v_sql := REPLACE(v_sql, '{{year}}', SUBSTR(p_srcStr, 1, 4));
			v_sql := REPLACE(v_sql, '{{prevYear}}', TO_CHAR(SUBSTR(p_srcStr, 1, 4)::NUMERIC - 1, 'FM0000'));
			v_sql := REPLACE(v_sql, '{{df}}', p_srcDf);
			
			v_sql_au   := FORMAT(
					'SELECT 	CASE WHEN ''%s'' BETWEEN %s AND %s THEN 1 ELSE 0 END AS RESULT) S'
					, p_srcStr
					, p_start
					, p_end )	;

			v_sql_au := REPLACE(v_sql_au, '{{prevYear}}', SUBSTR(p_srcStr, 1, 4));
			v_sql_au := REPLACE(v_sql_au, '{{year}}', TO_CHAR(SUBSTR(p_srcStr, 1, 4)::NUMERIC + 1, 'FM0000'));
			v_sql_au := REPLACE(v_sql_au, '{{df}}', p_srcDf);
			
			 v_sql := CONCAT_WS('', v_sql, v_sql_au);
			
        ELSE
        
			v_sql   := FORMAT(
					'SELECT CASE WHEN ''%s'' BETWEEN %s AND %s THEN %s ELSE %s END'
					, p_srcStr
					, p_start
					, p_end
					, p_utcOffset + 1
					, p_utcOffset);
				
            v_sql := REPLACE(v_sql, '{{year}}', SUBSTR(p_srcStr, 1, 4));
            v_sql := REPLACE(v_sql, '{{df}}', p_srcDf);
           
        END IF;

        EXECUTE  v_sql INTO v_result;
--	    RAISE NOTICE 'FN_GET_OFFSET >>> SQL[%] RETURN[%]', v_sql, v_result;

    END IF;
   
    RETURN v_result;
   
    EXCEPTION WHEN OTHERS then
    	/*
		GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
	    RAISE NOTICE 'FN_GET_OFFSET >>> 오류[%] SQL[%]', v_err_msg, v_sql;
	   */
        RETURN - 99;
       
END;
$function$
;

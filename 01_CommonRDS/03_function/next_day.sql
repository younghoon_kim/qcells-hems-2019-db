CREATE OR REPLACE FUNCTION wems.next_day(p_date date, p_weekday integer)
 RETURNS date
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       next_day
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2019-10-07   박동환       Tibero 내장함수 Postgresql 변환
   
   NOTES:   입력일 초과 후 입력요일 조회

	Input : p_date - date

    Output : date

	Usage :
	        SELECT NEXT_DAY(TO_DATE('20191007', 'YYYYMMDD'), 1);		-- 1:일요일
	        ==> 2019-10-13 (기준일 이후 일요일)

******************************************************************************/
DECLARE
       v_dow				integer;
       v_add_days		integer;
      
BEGIN
	v_dow := to_number(to_char(p_date,'D'),	'9');
	
	IF v_dow < p_weekday	THEN
	     v_add_days := p_weekday - v_dow;
    ELSE
	     v_add_days := p_weekday - v_dow + 7;
	END IF;

	RETURN p_date + CONCAT(v_add_days, ' DAY')::INTERVAL;
   
END;
$function$
;

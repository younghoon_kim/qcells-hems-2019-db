CREATE OR REPLACE FUNCTION wems.fn_get_div_grid_tr(p_pv numeric, p_bt numeric, p_grid numeric)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_GET_DIV_GRID_TR
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-28   yngwie     Created this function.
   2.0        2019-09-24   이재형      Postgresql 변환
   
   NOTES:   PV전력량과 배터리전력량-방전 을 가지고 Grid 매전량 Total 중에서 PV 와 배터리가 차지하는 전력량을 구한다.

	Input : p_pv - PV전력량
	        p_bt - 배터리전력량-방전
	        p_grid - Grid 매전량 Total

    Output : VARCHAR - 결과 값, 구분자 | 를 중심으로 앞은 pv, 뒤는 bt

	Usage :

******************************************************************************/
DECLARE
    i_result 		VARCHAR := NULL;
    i_ratio_pv 	NUMERIC;
    i_ratio_bt 	NUMERIC;
    i_pv_val 		NUMERIC;
    i_bt_val 		NUMERIC;
   
BEGIN
	
     IF p_grid IS NOT NULL THEN
 
		 	SELECT MAX( CASE WHEN TP_CD = 'PV' THEN ROUND(p_grid * RATIO) ELSE NULL END ) PV_VAL 
		 			, MAX( CASE WHEN TP_CD = 'BT' THEN ROUND(p_grid * RATIO) ELSE NULL END ) BT_VAL
		 	INTO i_pv_val, i_bt_val
		 	FROM ( 
		 		SELECT 	 TP_CD 
				 		    , AMOUNT 
				 		    , COALESCE(1.0 * AMOUNT / NULLIF(SUM(AMOUNT) OVER(),0), 0) AS RATIO 
		 		FROM ( 
		 			SELECT p_pv AMOUNT, 'PV' TP_CD 
		 			UNION ALL 
		 			SELECT p_bt AMOUNT, 'BT' TP_CD  
		 		) a
		 	)b ;
		
	    i_result := i_pv_val || '|' || i_bt_val;
	    
	END IF;
    
	RETURN i_result;

END;
$function$
;

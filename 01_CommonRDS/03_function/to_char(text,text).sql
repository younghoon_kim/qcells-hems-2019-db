CREATE OR REPLACE FUNCTION wems.to_char(param character varying, fmt character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
	DECLARE
		v_result	VARCHAR(10000);	
	
	BEGIN
		select to_char(param::numeric ,fmt) into v_result;
		RETURN v_result;
	END;
$function$

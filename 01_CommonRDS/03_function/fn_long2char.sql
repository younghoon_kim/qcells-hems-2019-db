CREATE OR REPLACE FUNCTION wems.fn_long2char(p_parti_name text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_LONG2CHAR
   PURPOSE: 입력 파티션명의 마지막 경계(bound) 조회
                함수명 변경 필요(예, FN_GET_PATITION_UP_BOUND)

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-29   yngwie      Created this function.
   2.0        2019-09-25   박동환       Postgresql 변환

   NOTES:   입력받은 문자열을 구분자를 기준으로 잘라 반환한다.

	Input : p_parti_name - 파티션명

    Output : TEXT - 입력 파티션의 bound(마지막 범위)

	Usage :
	        SELECT FN_LONG2CHAR('tb_opr_alarm_hist_pt_opr_alarm_hist_201905');   
	        -- 결과 : '20190601000000'

******************************************************************************/
DECLARE
    o_result TEXT := NULL;
   
begin
	SELECT 	CHR(39) || SPLIT_PART(pg_get_expr(relpartbound, oid, false), '''', 4)  || CHR(39)
	INTO 		o_result
	FROM 	pg_class
	WHERE 	relname = p_parti_name; -- 'tb_opr_alarm_hist_pt_opr_alarm_hist_201905';	
	
    RETURN o_result;
   
END;
$function$
;

CREATE OR REPLACE FUNCTION wems.sp_crt_dmmy_data(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$

/******************************************************************************
   NAME:       SP_CRT_DMMY_DATA
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2019-10-02   이재형   Created this function.
   1.1        2019-10-11   이재형   파라미터에 따라 해당 날짜 만들도록 수정
   
   NOTES:   제공 받은 2일 raw 데이터를 매일 1회 익일 데이터로 복사해 생성한다.

	Input : i_exe_base 기준 날짜

	Usage : SELECT SP_CRT_DMMY_DATA('SP_CRT_DMMY_DATA', 0, '20191024', 'M')


******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3
	
    -- (2)업무처리 변수
	v_diff				NUMERIC   := 0;
    v_cnt           	NUMERIC   := 0;
    v_sql           	TEXT;
   
BEGIN
	-- Job에서 자동실행 시에
	IF i_exe_mthd = 'A' THEN
		i_exe_base := TO_CHAR(TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD')+INTERVAL '2 DAY', 'YYYYMMDD');
	ELSE
		i_exe_base := SUBSTR(i_exe_base, 1, 8);
	END IF;

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN  
		v_rslt_cd := 'A';
			
		-- diff day
		SELECT 	DATE_PART('day',TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000', 'YYYYMMDDHH24MISS')-TO_TIMESTAMP('20190828000000','YYYYMMDDHH24MISS')) 
		INTO		v_diff;
	   
		/*
		v_sql := FORMAT('TRUNCATE TABLE PT_RAW_BMS_INFO_%s', SUBSTR(i_exe_base, 1, 8));
		EXECUTE v_sql;
		
		v_sql := FORMAT('TRUNCATE TABLE PT_RAW_PCS_INFO_%s', SUBSTR(i_exe_base, 1, 8));
		EXECUTE v_sql;

		v_sql := FORMAT('TRUNCATE TABLE PT_RAW_EMS_INFO_%s', SUBSTR(i_exe_base, 1, 8));
		EXECUTE v_sql;
		*/

		v_rslt_cd := 'B';
		
		DELETE FROM TB_RAW_BMS_INFO
		WHERE 	CREATE_DT >=TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000 09', 'YYYYMMDDHH24MISS TZH')
		AND 		CREATE_DT <  TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000 09', 'YYYYMMDDHH24MISS TZH')+INTERVAL '1 DAY';
	
		INSERT INTO TB_RAW_BMS_INFO
		SELECT DISTINCT device_id,
				TO_CHAR(TO_TIMESTAMP(colec_dt,'YYYYMMDDHH24MISS')+(v_diff||'day')::INTERVAL,'YYYYMMDDHH24MISS') as colec_dt, 
				NULL as done_flag, 
				col0, col1, col2, col3, col4, col5, col6, col7, 
				col8, col9, col10, col11, col12, col13, col14, 
				col15, col16, col17, col18, col19, col20, col21, 
				col22, col23, col24, col25, col26, col27, col28, col29, 
				col30, col31, col32, col33, col34, col35, col36, col37, 
				col38, col39, col40, 
				create_dt+(v_diff||'day')::INTERVAL as create_dt, NULL as handle_flag
		FROM 	TB_RAW_BMS_INFO
		WHERE 	CREATE_DT >=TO_TIMESTAMP('20190828000000 09', 'YYYYMMDDHH24MISS TZH')
		AND 		CREATE_DT <  TO_TIMESTAMP('20190829000000 09', 'YYYYMMDDHH24MISS TZH');

		
		GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	    v_rslt_cnt := v_rslt_cnt + v_work_num;

	    v_rslt_cd := 'C';

	   	DELETE FROM TB_RAW_PCS_INFO
		WHERE 	CREATE_DT >=TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000 09', 'YYYYMMDDHH24MISS TZH')
		AND 		CREATE_DT <  TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000 09', 'YYYYMMDDHH24MISS TZH')+INTERVAL '1 DAY';
		
		INSERT INTO TB_RAW_PCS_INFO
		SELECT 	DISTINCT device_id, 
					to_char(TO_TIMESTAMP(colec_dt,'YYYYMMDDHH24MISS')+(v_diff||'day')::INTERVAL,'YYYYMMDDHH24MISS') as COLEC_DT,
					NULL as done_flag, 
					col0, col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, 
					col15, col16, col17, col18, col19, col20, col21, col22, col23, col24, col25, col26, col27, col28, col29, col30, col31, col32, 
					col33, col34, col35, col36, col37, col38, col39, col40, col41, col42, col43, col44, col45, col46, col47, col48, col49, col50, 
					col51, col52, col53, col54, col55, col56, col57, col58, col59, col60, col61, col62, col63, col64, col65, col66, col67, col68, 
					col69, col70, col71, col72, col73, col74, col75, col76, col77, col78, col79, col80, col81, col82, col83, col84, col85, col86, 
					col87, col88, col89, col90, col91, col92, col93, col94, col95, col96, col97, col98, col99, col100, col101, col102, col103, col104, 
					col105, col106, col107, col108, col109, col110, col111, col112, col113, col114, col115, col116, col117, col118, col119, col120, 
					col121, col122, col123, col124, col125, col126, col127, col128, col129, col130, col131, col132, col133, col134, col135, col136, 
					col137, col138, col139, col140, col141, col142, col143, col144, col145, col146, col147, col148, col149, col150, col151, col152, 
					col153, col154, col155, col156, col157, col158, col159, col160, col161, col162, col163, col164, col165, col166, col167, col168, 
					col169, col170, col171, col172, col173, col174, col175, col176, col177, col178, col179, col180, col181, col182, col183, col184, 
					col185, col186, col187, col188, col189, col190, col191, col192, col193, col194, col195, col196, col197, col198, col199, col200, 
					col201, col202, col203, col204, col205, col206, col207, col208, col209, col210, col211, col212, col213, col214, col215, col216, 
					col217, col218, col219, col220, col221, col222, col223, col224, col225, col226, 
					create_dt+(v_diff||'day')::INTERVAL as create_dt, NULL as handle_flag
			FROM TB_RAW_PCS_INFO
		WHERE 	CREATE_DT >=TO_TIMESTAMP('20190828000000 09', 'YYYYMMDDHH24MISS TZH')
		AND 		CREATE_DT <  TO_TIMESTAMP('20190829000000 09', 'YYYYMMDDHH24MISS TZH');
		
		GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	     v_rslt_cnt := v_rslt_cnt + v_work_num;

	    v_rslt_cd := 'D';

   	   	DELETE FROM TB_RAW_EMS_INFO
		WHERE 	CREATE_DT >=TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000 09', 'YYYYMMDDHH24MISS TZH')
		AND 		CREATE_DT <  TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000 09', 'YYYYMMDDHH24MISS TZH')+INTERVAL '1 DAY';


		INSERT INTO TB_RAW_EMS_INFO
		SELECT DISTINCT device_id, 
				TO_CHAR(TO_TIMESTAMP(colec_dt,'YYYYMMDDHH24MISS')+(v_diff||'day')::INTERVAL,'YYYYMMDDHH24MISS') as colec_dt, 
				NULL as done_flag, 
				col0, col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16, col17, col18, col19, col20, 
				col21, col22, col23, col24, col25, col26, col27, col28, col29, col30, col31, col32, col33, col34, col35, col36, col37, col38, col39, col40, 
				col41, col42, col43, col44, col45, col46, col47, col48, col49, col50, col51, col52, col53, col54, col55, col56, col57, col58, col59, col60, 
				col61, col62, col63, col64, col65, col66, col67, col68, col69, col70, col71, col72, col73, col74, col75, col76, col77, col78, col79, col80,
				col81, col82, col83, col84, col85, col86, col87, col88, col89, col90, col91, col92, col93, col94, col95, col96, col97, col98, col99, col100,
				col101, col102, col103, col104, col105, col106, col107, col108, col109, col110, col111, col112, col113, col114, col115, col116, col117, col118,
				col119, col120, col121, col122, col123, col124, col125, col126, col127, col128, col129, col130, col131, col132, col133, col134, col135, col136,
				col137, col138, col139, col140, col141, col142, col143, col144, col145, col146, col147, col148, col149, col150, col151, col152, col153, col154,
				col155, col156, col157, col158, col159, col160, col161, col162, col163, col164, col165, col166, col167, col168, col169, col170, col171, col172,
				col173, col174, col175, col176, col177, col178, col179, col180, col181, col182, col183, col184, col185, col186, col187, col188, col189, col190,
				col191, col192, col193, col194, col195, col196, col197, col198, col199, col200, col201, col202, col203, col204, col205, col206, col207, col208,
				col209, col210, col211, col212, col213, col214, col215, col216, col217, col218, col219, col220, col221, col222, col223, col224, col225, col226,
				col227, col228, col229, col230, col231, col232, col233, col234, col235, col236, col237, col238, col239, col240, col241, col242, col243, col244,
				col245, col246, col247, col248, col249, col250, col251, col252, col253, col254, col255, col256, col257, col258, col259, col260, col261, col262,
				col263, col264, col265, col266, col267, col268, col269, col270, col271, col272, col273, col274, col275, col276, col277, col278, col279, col280,
				col281, col282, col283, col284, col285, col286, col287, col288, col289, col290, col291, col292, col293, col294, col295, col296, col297, col298,
				col299, col300, col301, col302, col303, col304, col305, col306, col307, col308, col309, col310, col311, col312, col313, col314, col315, col316,
				col317, col318, col319, col320, col321, col322, col323, col324, col325, col326, col327, col328, col329, col330, col331, col332, col333, col334,
				col335, col336, col337, col338, col339, col340, col341, 
				create_dt+(v_diff||'day')::INTERVAL as create_dt, NULL as handle_flag
		FROM 	TB_RAW_EMS_INFO
		WHERE 	CREATE_DT >=TO_TIMESTAMP('20190828000000 09', 'YYYYMMDDHH24MISS TZH')
		AND 		CREATE_DT <  TO_TIMESTAMP('20190829000000 09', 'YYYYMMDDHH24MISS TZH');

		GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	     v_rslt_cnt := v_rslt_cnt + v_work_num;
	    
	    
	    -- TB_RAW_LOGIN 월단위처리
	    
	      -- 실행성공
	     v_rslt_cd := '1';
    
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$function$
;

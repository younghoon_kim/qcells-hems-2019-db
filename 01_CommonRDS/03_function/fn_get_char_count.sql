CREATE OR REPLACE FUNCTION wems.fn_get_char_count(p_str text, p_chr text)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
/*
*****************************************************************************
   NAME:       FN_GET_CHAR_COUNT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-09-16   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                  Postgresql 변환
   NOTES:   입력받은 문자열에 포함된 문자의 갯수를  반환한다.

	Input : p_str - 문자열
	        p_chr - 찾을 문자

    Output : NUMBER - 문자 갯수

	Usage :
	        SELECT FN_GET_CHAR_COUNT('123|23|123', '|') FROM DUAL;

*****************************************************************************
*/
DECLARE
    i_result numeric := 0;
BEGIN
    SELECT
        LENGTH(p_str) - LENGTH(REPLACE(p_str, p_chr, ''))
        INTO STRICT i_result;
    RETURN i_result;
END;
$function$
;

CREATE OR REPLACE FUNCTION sp_off_user_v2(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       SP_OFF_USER
   PURPOSE:    
a
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-09-22   yngwie       1. Created this function.
   2.0        2019-09-25   박동환        Postgresql 변환
   2.1        2020-10-26   서정우        1. site_id 적용에 따른 수

   NOTES: TB_BAS_USER 테이블의 LEAVE_DT(탈퇴일시) 컬럼을 일 2회 체크하여 다음과 같이 처리한다.
          1. 탈퇴일시가 24시간 이내이면 사용자가 남긴 흔적을 모두 삭제한다.
          2. 탈퇴일시가 24시간 이후이면 TB_BAS_USER 에서 사용자 정보를 삭제한다.
   
    Input : 
            
    Output : 
    
******************************************************************************/ 
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_row           	record; 
   
BEGIN
		
	-- 12시간마다 수행
	IF SUBSTR(i_exe_base, 9, 2) NOT IN ('10', '22') THEN
		RETURN	'1';
	END IF ;


	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

	    --RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] START[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');
--	    RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] START[%] i_exe_base[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base;

    	FOR v_row IN ( 
			SELECT 
			            USER_ID
			            , LEAVE_DT
			            , CASE WHEN EXTRACT(DAY FROM SYS_EXTRACT_UTC(NOW()) - LEAVE_DT) < 1 THEN 'N' ELSE 'Y' END OFF_FLAG
	        FROM    TB_BAS_USER 
	        WHERE 	LEAVE_DT IS NOT null
	        LIMIT     100

	    ) LOOP
--             CALL wems.SP_LOG(i_site_seq, i_sp_nm, 'Delete USER INFO Start : ' || v_row.USER_ID);
			 --RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete USER INFO Start : USER_ID[%]', i_sp_nm, i_site_seq, v_row.USER_ID;
        
            -- 1. 24시간이 지났으면 사용자 테이블에서 삭제한다.
            IF v_row.OFF_FLAG = 'Y' THEN
                DELETE FROM TB_BAS_USER WHERE USER_ID = v_row.USER_ID;
		      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
			 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_BAS_USER :[%]', i_sp_nm, i_site_seq, v_work_num;

                DELETE FROM TB_OPR_TERMS_HIST WHERE USER_ID = v_row.USER_ID;
		      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
			 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_TERMS_HIST :[%]', i_sp_nm, i_site_seq, v_work_num;

                DELETE FROM tb_bas_user_site WHERE USER_ID = v_row.USER_ID;
		      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
			 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_BAS_USER_DEVICE :[%]', i_sp_nm, i_site_seq, v_work_num;
            END IF;
            
            -- 2. 사용자가 남긴 흔적 삭제
            
            -- TB_OPR_CUST_QNA
            DELETE FROM TB_OPR_CUST_QNA_REPL 
            WHERE CUST_QNA_SEQ IN ( 
	                SELECT CUST_QNA_SEQ FROM TB_OPR_CUST_QNA WHERE CREATE_ID = v_row.USER_ID
            )    ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_CUST_QNA_REPL :[%]', i_sp_nm, i_site_seq, v_work_num;

            DELETE FROM TB_OPR_CUST_QNA_FILE 
            WHERE CUST_QNA_SEQ IN ( 
                SELECT CUST_QNA_SEQ FROM TB_OPR_CUST_QNA WHERE CREATE_ID = v_row.USER_ID
            )  ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_CUST_QNA_FILE :[%]', i_sp_nm, i_site_seq, v_work_num;
        
            DELETE FROM TB_OPR_CUST_QNA WHERE CREATE_ID = v_row.USER_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_CUST_QNA :[%]', i_sp_nm, i_site_seq, v_work_num;
            
            -- TB_OPR_PVCALC_HIST
            DELETE FROM TB_OPR_PVCALC_HIST WHERE USER_ID = v_row.USER_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_PVCALC_HIST :[%]', i_sp_nm, i_site_seq, v_work_num;
            
            -- TB_OPR_VOC
            DELETE FROM TB_OPR_VOC_FILE 
            WHERE VOC_SEQ IN ( 
                SELECT VOC_SEQ FROM TB_OPR_VOC WHERE CREATE_ID = v_row.USER_ID
            )  ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_VOC_FILE :[%]', i_sp_nm, i_site_seq, v_work_num;
            
            DELETE FROM TB_OPR_VOC_REPL 
            WHERE VOC_SEQ IN ( 
                SELECT VOC_SEQ FROM TB_OPR_VOC WHERE CREATE_ID = v_row.USER_ID
            )  ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_VOC_REPL :[%]', i_sp_nm, i_site_seq, v_work_num;
        
           	DELETE FROM TB_OPR_VOC WHERE CREATE_ID = v_row.USER_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_VOC :[%]', i_sp_nm, i_site_seq, v_work_num;
            
            -- 3. 이력 정보 삭제
            
            -- TB_SYS_LOGIN_HIST
            DELETE FROM TB_SYS_LOGIN_HIST WHERE USER_ID = v_row.USER_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_SYS_LOGIN_HIST :[%]', i_sp_nm, i_site_seq, v_work_num;
            
            -- TB_SYS_EXEC_HIST
            DELETE FROM TB_SYS_EXEC_HIST WHERE USER_ID =v_row.USER_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_SYS_EXEC_HIST :[%]', i_sp_nm, i_site_seq, v_work_num;
            
            -- TB_SYS_SEC_HIST
           	DELETE FROM TB_SYS_SEC_HIST WHERE USER_ID =v_row.USER_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_SYS_SEC_HIST :[%]', i_sp_nm, i_site_seq, v_work_num;

		END LOOP;
    	--RAISE NOTICE 'i_sp_nm[%] End Loop !!! ', i_sp_nm;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$function$
;

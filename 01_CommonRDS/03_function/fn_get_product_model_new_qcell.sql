CREATE OR REPLACE FUNCTION wems.fn_get_product_model_new_qcell(p_value text, p_column text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
*****************************************************************************
   NAME:       FN_GET_PRODUCT_MODEL
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-03-31   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                            Postgresql 변환
   
   NOTES:   입력받은  DEVICE_ID 의TB_PLF_CODE_INFO 테이블의 p_column 값을 반환

	Input : p_value - DEVICE_ID
            p_column - TB_PLF_CODE_INFO 의 컬럼 명

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_PRODUCT_MODEL('AR00460036Z114827015X', 'CODE') FROM DUAL;
	        SELECT FN_GET_PRODUCT_MODEL('AR00460036Z114827015X', 'REF_5') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(200);
    v_sql text;
BEGIN
    v_sql := '	SELECT '|| p_column || ' 
				FROM TB_PLF_CODE_INFO 
				WHERE GRP_CD = ''PRODUCT_MODEL_CD''
				AND code = ''HHQC''||SUBSTR($1, 8, 4) ';
    
    EXECUTE  v_sql INTO v_value USING p_value;

    RETURN v_value;

END;
$function$
;

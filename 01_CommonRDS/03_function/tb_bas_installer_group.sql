drop table if exists tb_bas_parent_installer_and_installer;
drop table if exists tb_bas_installer_group;
create table tb_bas_installer_group
(
    parent_id       varchar(64)                                                not null,
    installer_id     varchar(64)                                                not null,
    create_id        varchar(64)                                                not null,
    create_dt        timestamp(6) with time zone default sys_extract_utc(now()) not null,
    update_id        varchar(64),
    update_dt        timestamp(6) with time zone,
    constraint pk_bas_parent_installer_and_installer
        primary key (parent_id, installer_id)
);

comment on table tb_bas_installer_group is '유틸리티계정 또는 인스톨러그룹과 인스톨러 매핑테이블';

comment on column tb_bas_installer_group.parent_id is '부모 인스톨러ID';

comment on column tb_bas_installer_group.installer_id is '인스톨러ID';

comment on column tb_bas_installer_group.create_id is '등록자ID';

comment on column tb_bas_installer_group.create_dt is '등록일시';

comment on column tb_bas_installer_group.update_id is '수정자ID';

comment on column tb_bas_installer_group.update_dt is '수정일시';

CREATE OR REPLACE PROCEDURE wems.sp_get_err_list()
 LANGUAGE plpgsql
AS $procedure$
/******************************************************************************
  NAME:       SP_GET_ERR_LIST
  PURPOSE:

  REVISIONS:
  Ver        Date        Author           Description
  ---------  ----------  ---------------  ------------------------------------
  1.0        2014-07-25   yngwie       1. Created this function.
  2.0		 2019-10-07		박동환			postgres변환

  NOTES:  ESS 장애상황을 출력한다.

Input :

Usage :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_GET_ERR_LIST';
    v_log_seq 		NUMERIC := 0;		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';							-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    p_dt    			VARCHAR	:= ''; 
    v_row           	RECORD; 
   	v_sql				VARCHAR;
   
BEGIN

	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt 
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';

--	    RAISE NOTICE 'v_log_fn[%] v_log_seq[%] START[%]', v_log_fn, v_log_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

    	FOR v_row IN ( 
			SELECT	A.DEVICE_ID
						, A.DEVICE_TYPE_CD
						, DEC_VARCHAR_SEL(C.INSTL_ADDR , 10, 'SDIENCK', 'TB_BAS_DEVICE', 'INSTL_ADDR') AS ADDRESS
						, B.ALARM_TYPE_CD
						, B.ALARM_CD
						, B.ALARM_DESC
						, A.EVENT_DT
						, FN_TS_STR_TO_STR(A.EVENT_DT, 0, 'YYYYMMDDHH24MISS', 0, 'YYYY-MM-DD HH24:MI:SS') EVENT_DT_STR
						, ALARM_ID
						, ALARM_ID_SEQ
			FROM	 	TB_OPR_ALARM A
						, TB_PLF_ALARM_CODE B
						, TB_BAS_DEVICE C
			WHERE	A.NOTI_FLAG IS NULL
			   AND 	A.ALARM_TYPE_CD = B.ALARM_TYPE_CD
			   AND 	A.ALARM_CD = B.ALARM_CD
			   AND 	A.DEVICE_ID = C.DEVICE_ID
			   AND 	A.ALARM_CD in (
					   		'E601', 'E603', 
					   		'E901', 'E902', 'E903', 'E904', 'E905', 'E906',	'E907'
				 		)        
			ORDER BY A.DEVICE_ID, A.EVENT_DT

		) LOOP
--      		RAISE notice 'v_log_fn[%] Insert TB_OPR_ALARM_ERR_LIST : ALARM_ID[%] ALARM_ID_SEQ[%]',  v_log_fn, v_row.ALARM_ID, v_row.ALARM_ID_SEQ;
		
	   		INSERT INTO TB_OPR_ALARM_ERR_LIST(ALARM_ID, ALARM_ID_SEQ) 
	   		VALUES( v_row.ALARM_ID, v_row.ALARM_ID_SEQ);
	   	
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	
	   END LOOP;

	  /* 로그
	  IF v_rslt_cnt > 0 THEN
	  	RAISE notice 'v_log_fn[%] Insert TB_OPR_ALARM_ERR_LIST : [%]건',  v_log_fn, v_rslt_cnt;
	  ELSE
	  	RAISE notice 'v_log_fn[%] No New Alarm Records',  v_log_fn;
	  END IF;
	 */

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'v_log_fn[%] EXCEPTION !!! ', v_log_fn;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$procedure$
;

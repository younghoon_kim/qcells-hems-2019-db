create or replace procedure sp_off_ess_v2(p_deviceid text, p_devicetypecd text, p_userid text)
	language plpgsql
as $$
/******************************************************************************
   NAME:       SP_OFF_ESS_V2
   PURPOSE:

   REVISIONS:
   Ver        Date           Author           Description
   ---------  ----------------  ---------------  ------------------------------------
   1.0        2015-08-11   yngwie         Created this function.
   1.1        2019-09-30   이재형          convert postgresql
   1.2        2020-07-22    서정우     modify      SP_OFF_ESS를 기준으로 추가/변경된 테이블 적용.
   
   NOTES:   장비 삭제시 관련 정보 (통계, 이력, 장애 등)를 모두 삭제한다.

	Input : p_deviceId VARCHAR TB_BAS_DEVICE.DEVICE_ID
	        p_deviceTypeCd VARCHAR TB_BAS_DEVICE.DEVICE_TYPE_CD
	        p_userId VARCHAR TB_BAS_USER.USER_ID

	Usage : exec SP_OFF_ESS_V2('AR00460036Z199999999X', 'RES', 'installer1');

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
    v_log_fn   VARCHAR   := 'SP_OFF_ESS_V2';
    v_log_seq  NUMERIC   := 0; -- nextval('wems.sq_sys_job_hist');
    v_exe_mthd VARCHAR   := 'M'; -- 실행방식( A:자동(job수행), M:수동 )

    v_st_dt    TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd  VARCHAR   := '0'; -- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt INTEGER   := 0; -- 적재수
    v_work_num INTEGER   := 0; -- 처리건수
    v_err_msg  VARCHAR   := NULL; -- SQL오류메시지내용
    v_err_msg1 VARCHAR   := NULL; -- 메시지내용1
    v_err_msg2 VARCHAR   := NULL; -- 메시지내용2
    v_err_msg3 VARCHAR   := NULL; -- 메시지내용3

    p_dt       VARCHAR   := ' ' ;

    -- (2)업무처리 변수
    v_cnt      INTEGER   := 0; -- 적재수
BEGIN

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG(v_log_fn
        , v_log_seq
        , p_dt
        , v_st_dt
        , v_exe_mthd
        );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
    BEGIN
        v_rslt_cd := 'A';

        -- 형상, 요금 정보 삭제
-- 	    DELETE FROM TB_BAS_DEVICE WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
        DELETE FROM TB_BAS_DEVICE_PMS WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
        DELETE FROM tb_bas_super_device WHERE prn_device_id = p_deviceId;


        -- 	    DELETE FROM TB_BAS_USER_DEVICE WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
-- 	    DELETE FROM TB_BAS_API_USER_DEVICE WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;

        DELETE FROM TB_BAS_ELPW_UNIT_PRICE WHERE elpw_prod_cd = p_deviceId;

        DELETE FROM TB_BAS_CTL_GRP_MGT WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;

        DELETE FROM TB_BAS_UPT_GRP_MGT WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;

        --	    DELETE FROM TB_OPR_PRE_DIAG_EX_DEVICE WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
-- 	    DELETE FROM TB_OPR_ESS_CONFIG WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
        DELETE FROM tb_opr_ess_config_set_v2 WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
        DELETE FROM tb_opr_ess_config_mon_v2 WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;

        -- 현황 정보 삭제
        DELETE FROM TB_OPR_ESS_STATE WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;


        DELETE FROM TB_OPR_ESS_STATE_TL WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;

        DELETE FROM TB_OPR_ALARM WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;


        v_rslt_cd := 'B';

        -- 삭제 대상 장비 정보에 추가
        INSERT INTO TB_BAS_OFF_DEVICE (device_id, device_type_cd) VALUES (p_deviceId, p_deviceTypeCd);

        GET DIAGNOSTICS v_work_num = ROW_COUNT; -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;

        -- 실행성공
        v_rslt_cd := '1';

    EXCEPTION
        WHEN OTHERS THEN
            /***************************************************************************
              오류메세지 등에 대한 처리
            ***************************************************************************/
            IF v_rslt_cd = '0' THEN
                v_rslt_cd := '2';
            END IF;

            v_rslt_cnt := 0; --적재수
            GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                ,v_err_msg2 = PG_EXCEPTION_DETAIL
                ,v_err_msg3 = PG_EXCEPTION_HINT;
            v_err_msg = v_err_msg1 || '_|~' || v_err_msg2 || '_|~' || v_err_msg3; --SQL오류메시지내용
    END;

    /***************************************************************************
       4.프로시져 종료로그 등록
     ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG(v_log_fn
        , v_log_seq
        , p_dt
        , v_st_dt
        , v_rslt_cd
        , v_rslt_cnt
        , v_err_msg
        );

END;
$$;

alter procedure sp_off_ess_v2(text, text, text) owner to wems;


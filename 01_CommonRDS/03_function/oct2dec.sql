CREATE OR REPLACE FUNCTION wems.oct2dec(octval character)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
     /*
     *****************************************************************************
       NAME:       OCT2DEC
       PURPOSE:
    
       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
  	   2.0        2019-10-08     이재형                  postgresql 변환    
  	   
       NOTES:   oct(8진수) to  Decimal(10진수)
    
        Input : 
    
        Output :
    
     *****************************************************************************
     */
DECLARE
    i numeric;
    digits numeric;
    result numeric := 0;
    current_digit CHARACTER(1);
    current_digit_dec numeric;
BEGIN
    digits := LENGTH(octval);

    FOR i IN 1..digits LOOP
        current_digit := substr(octval, i, 1);
        current_digit_dec := current_digit::numeric;
        result := (result * 8) + current_digit_dec;
    END LOOP;
    RETURN result;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$function$
;

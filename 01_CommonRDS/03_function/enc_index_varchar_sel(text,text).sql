CREATE OR REPLACE FUNCTION wems.enc_index_varchar_sel(cal_val character varying, sdienck character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/***********************************
 * 사용법: SELECT ENC_INDEX_VARCHAR_SEL('userA', 'SDIENCK');
 * 
 ************************************/
DECLARE
	err_context	VARCHAR;

BEGIN
	IF sdienck IS NULL OR sdienck = '' THEN 
		sdienck := 'SDIENCK';
	END IF;
	
	RETURN (	SELECT FN_GET_ENCRYPT(cal_val, sdienck)	);
	
	EXCEPTION WHEN OTHERS THEN 
		 GET STACKED DIAGNOSTICS err_context = MESSAGE_TEXT;
	RETURN err_context;
			 
END;

$function$
;

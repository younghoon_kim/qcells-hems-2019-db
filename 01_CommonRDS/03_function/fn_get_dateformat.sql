CREATE OR REPLACE FUNCTION wems.fn_get_dateformat(p_val numeric)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_GET_DATEFORMAT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   2.0        2019-09-24   박동환       Postgresql 변환

   NOTES:   일자포맷 구성

	Input   : 일자포맷 스트링 문자수

    Output : 일자포맷

	Usage :
	        FN_GET_DATEFORMAT(14);

******************************************************************************/
DECLARE
    v_result TEXT;
BEGIN
    IF p_val = 4 THEN
        v_result := 'YYYY';
    ELSIF p_val = 6 THEN
        v_result := 'YYYYMM';
    ELSIF p_val = 8 THEN
        v_result := 'YYYYMMDD';
    ELSIF p_val = 10 THEN
        v_result := 'YYYYMMDDHH24';
    ELSIF p_val = 14 THEN
        v_result := 'YYYYMMDDHH24MISS';
    ELSE
        v_result := 'YYYYMMDDHH24MISS';
    END IF;
    RETURN v_result;
END;
$function$
;

CREATE OR REPLACE FUNCTION wems.fn_get_encrypt(i_text character varying, i_key character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
  프로그램ID   : fn_get_encrypt
  작  성  일   : 2019-11-07
  작  성  자   : 박동환
  프로그램용도 : 문자열암호화
  참 고 사 항  :
                 < PROTOTYPE >
                   fn_get_encrypt

                 <PARAMETER>
                   < INPUT >
                     1.i_text	입력문자열
                     2.i_key	입력키

                   < OUTPUT >
                     1.o_text	출력문자열

                 < MODULE DIAGRAM >
                 1.변수 및 커서 선언


  ----------------------------------------------------------------------------
  TABLE                                                                  CRUD
  ============================================================================

  ----------------------------------------------------------------------------

  ----------------------------------------------------------------------------
  수정일자    수정자      수정내용
  =========== =========== ====================================================
  20191107    박동환      최초작성
  ----------------------------------------------------------------------------

*****************************************************************************/

DECLARE
	o_text	character varying:= '';

BEGIN
	
	SELECT ENCODE(ENCRYPT(CONVERT_TO(i_text,'utf8'),CONVERT_TO(i_key,'utf8'),'aes'),'hex')
	INTO	o_text;

	RETURN	o_text::character varying;

  	EXCEPTION WHEN OTHERS THEN		
		RETURN	''::character varying;
			
END;
$function$
;

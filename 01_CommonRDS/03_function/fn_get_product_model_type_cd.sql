CREATE OR REPLACE FUNCTION wems.fn_get_product_model_type_cd(p_value text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
*****************************************************************************
   NAME:       FN_GET_PRODUCT_MODEL
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-10-28   stjoo       1. Created this function.
   2.0        2019-09-24   이재형                            Postgresql 변환
   
   NOTES:   입력받은  DEVICE_ID 의  TB_BAS_DEVICE.PRODUCT_MODEL_TYPE_CD 를 반환

	Input : p_value - DEVICE_ID

    Output : VARCHAR

	Usage : SELECT FN_GET_PRODUCT_MODEL_TYPE_CD('AR00460036Z114827015X') FROM DUAL;


*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(200);
BEGIN
    
	SELECT SUBSTR(PRODUCT_MODEL_NM, 1, 7) 
	into v_value
	FROM TB_BAS_DEVICE 
	WHERE  DEVICE_ID = p_value;

    RETURN v_value;

END;
$function$
;

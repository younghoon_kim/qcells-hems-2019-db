CREATE OR REPLACE FUNCTION wems.fn_get_capacity(p_value text, p_column text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
*****************************************************************************
   NAME:       FN_GET_CAPACITY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-03-31   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                  Postgresql 변환
   
   NOTES:   입력받은  DEVICE_ID 의 TB_PLF_CODE_INFO 테이블의 p_column 값을 반환

	Input : p_value - DEVICE_ID
            p_column - TB_PLF_CODE_INFO 의 컬럼 명

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_CAPACITY('AR00460036Z114827015X', 'CODE') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(200);
    v_sql text;
BEGIN
   v_sql := 'SELECT  '||p_column||' 
			FROM    wems.tb_plf_code_info 
			WHERE grp_cd = ''CAPACITY_CD''   
   			AND cd_desc =     
			  		CASE         
			  			WHEN substr($2, 1, 2) = ''HS'' THEN wems.fn_get_product_model($3, ''REF_3''::TEXT)         
			  		    else substr($4, 7, 4)     
			  		END';
	
  	execute v_sql into v_value using p_column,p_value,p_value,p_value;
    RETURN v_value;

END;
$function$
;

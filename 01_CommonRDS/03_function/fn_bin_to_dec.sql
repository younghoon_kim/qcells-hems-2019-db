CREATE OR REPLACE FUNCTION wems.fn_bin_to_dec(p_binary text)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_GET_CONV_NA
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-11-16   stjoo       1. Created this function.
   2.0        2019-09-24   이재형                  Postgresql 변환
   NOTES:   문자열로 입력된 이진수를 10진수의 숫자형으로 반환한다.

    Input : p_value - VARCHAR

    Output : NUMBER

    Usage :
            SELECT FN_BIN_TO_DEC('01010100') FROM DUAL;

******************************************************************************/
DECLARE
    v_number numeric := 0;
    i numeric := 0;
BEGIN
    WHILE LENGTH(p_binary) > 0 LOOP
        v_number := v_number + (right(p_binary, 1)::NUMERIC * POWER(2, i));
        p_binary := substr(p_binary, 1, LENGTH(p_binary) - 1);
        i := i + 1;
    END LOOP;
    RETURN v_number;
END;
$function$
;

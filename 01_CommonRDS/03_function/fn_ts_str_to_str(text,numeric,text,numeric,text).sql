CREATE OR REPLACE FUNCTION wems.fn_ts_str_to_str(p_srcstr text, p_srctz numeric, p_srcdf text, p_targettz numeric, p_targetdf text)
 RETURNS text
 LANGUAGE plpgsql
 IMMUTABLE
AS $function$
/******************************************************************************
   NAME:       FN_TS_STR_TO_STR
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-11   yngwie      Created this function.
   2.0        2019-09-23   박동환       Postgresql 전환

   NOTES:   입력받은 날짜형식의 문자열을 TIMESTAMP WITH TIME ZONE 형식으로 변환하여
            GMT 0 의 날짜형식의 문자열로 반환한다.
            2020-11-09 김영훈 코멘트 추가 : 날짜,SRCTZ,TARGETTZ를 입력받아 원래 TIME ZONE에서 목표로하는 TIME ZONE
            시간으로 바꾼 문자열을 반환한다. TARGETTZ가 0일시 GMT 0(=UTC 0) 시간으로 바꿀 수 있다.

   TEST :
			SELECT
				TO_TIMESTAMP_TZ('20140612005800 +01', 'YYYYMMDDHH24MISS TZH') a -- SRC TS 를 TS WITH TIMEZONE 으로 변경
				, TO_CHAR( TO_TIMESTAMP_TZ('20140612005800 +01', 'YYYYMMDDHH24MISS TZH') ) b -- a 컬럼 TO_CHAR 로 변환
				, TO_CHAR( SYS_EXTRACT_UTC(TO_TIMESTAMP_TZ('20140612005800 +01', 'YYYYMMDDHH24MISS TZH')) ) c -- a 를 GMT 0 으로 변환
				, TO_CHAR( SYS_EXTRACT_UTC(TO_TIMESTAMP_TZ('20140612005800 +01', 'YYYYMMDDHH24MISS TZH')) + NUMTODSINTERVAL(9, 'HOUR') ) d -- c컬럼 TO_CHAR 로 변환
				, FN_TS_STR_TO_STR('20140612005800', 1, 'YYYYMMDDHH24MISS', 9, 'YYYYMMDDHH24MISS') e -- 함수 테스트
			FROM DUAL;


	Input : p_srcStr - 날짜형식의 문자열 (ex : 2014-04-21 01:00:00)
	        p_srcTz - p_dateStr 의 GMT Offset (-11 ~ 12)
	        p_srcDf - p_dateStr 의 날짜포맷(ex : YYYY-MM-DD HH24:MI:SS)
	        p_targetTz - 반환값의 GMT Offset (-11 ~ 12)
			p_targetDf - 반환값의 날짜포맷(ex : YYYY-MM-DD HH24:MI:SS)

	Usage :
	        FN_TS_STR_TO_STR('20140612005800', 1, 'YYYYMMDDHH24MISS', 9, 'YYYYMMDDHH24MISS')

******************************************************************************/
DECLARE
    o_return VARCHAR   := NULL;
BEGIN
	
	select CASE p_srcstr WHEN '' THEN '' ELSE to_char(to_timestamp(p_srcstr, p_srcdf) - concat(p_srctz,' hours')::interval - concat(-p_targettz,' hours')::interval, p_targetdf) END
	into o_return;

	return o_return;
   
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$function$
;

CREATE OR REPLACE PROCEDURE wems.sp_init_tb_raw_login(p_connectorid text DEFAULT NULL::text)
 LANGUAGE plpgsql
AS $procedure$
DECLARE
/******************************************************************************
   NAME:       SP_INIT_TB_RAW_LOGIN
   PURPOSE:

   REVISIONS:
   Ver        Date            Author           Description
   ---------  ----------------  ---------------  ------------------------------------
   2.0        2019-09-26   이재형         postgresql 변환
   
   NOTES:  입력받은 p_connectorid와 연결되어있는 장비들의 최근 로그인 정보를 초기화함.

	Input : 

	Usage :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
    v_log_fn 			VARCHAR := 'SP_INIT_TB_RAW_LOGIN';
    v_log_seq 		NUMERIC := 0;		
    v_exe_mthd		VARCHAR := 'M';	-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3
	
    p_dt 				VARCHAR   := ' ' ;
   
    -- (2)업무처리 변수
    v_row 			RECORD;
BEGIN

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
	
--	    sqlCmd := CONCAT_WS('', 'INSERT INTO TB_RAW_LOGIN (DEVICE_ID, CREATE_TM, PRODUCT_MODEL_NM, RESPONSE_CD, CREATE_DT, DATA_SEQ, EMS_OUT_IPADDRESS, EMS_TM) VALUES ( :1, ', CHR(10), ' TO_CHAR( SYS_EXTRACT_UTC(systimestamp), ''YYYYMMDDHH24MISS''), ', CHR(10), ' ''INIT'', ''128'', SYS_EXTRACT_UTC(systimestamp), SQ_RAW_LOGIN.nextval , ''127.0.0.1'', ', CHR(10), ' TO_CHAR(SYS_EXTRACT_UTC(systimestamp), ''YYYYMMDDHH24MISS'') )');
	
	    FOR v_row IN
		    SELECT  device_id, create_dt, response_cd, connector_id
	        FROM 	(	SELECT 	device_id, create_dt, response_cd, connector_id
	        							, ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY create_dt DESC, create_tm DESC) AS rn
	              			FROM 	TB_RAW_LOGIN
	              		) AS a
	        WHERE 	rn = 1 
	        AND 		a.response_cd = '0' 
	        AND 		a.connector_id = CASE WHEN p_connectorid IS NOT NULL THEN p_connectorid ELSE a.connector_id END
	    LOOP
			INSERT INTO TB_RAW_LOGIN (DEVICE_ID, CREATE_TM, PRODUCT_MODEL_NM, RESPONSE_CD, CREATE_DT, DATA_SEQ, EMS_OUT_IPADDRESS, EMS_TM)
			VALUES ( v_row.DEVICE_ID, TO_CHAR( SYS_EXTRACT_UTC(now()), 'YYYYMMDDHH24MISS'),  'INIT', '128', SYS_EXTRACT_UTC(now()), nextval('wems.sq_raw_login') , '127.0.0.1',  TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYYMMDDHH24MISS') );
	
           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	       	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	    END LOOP;
	   
	     -- 실행성공 
	     v_rslt_cd := '1';
    
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = 'p_connectorid['||p_connectorid||'] '||v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
END;
$procedure$
;

CREATE OR REPLACE FUNCTION wems.sys_extract_utcnow()
 RETURNS timestamp with time zone
 LANGUAGE plpgsql
 IMMUTABLE
AS $function$
	DECLARE
		v_result	timestamptz;	  
	BEGIN
		select SYS_EXTRACT_UTC(NOW()) into v_result ;
		return v_result;
	end;
$function$
;

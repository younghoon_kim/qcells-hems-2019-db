CREATE OR REPLACE PROCEDURE sp_sumup_stat(p_dt text)
 LANGUAGE plpgsql
AS $procedure$
/******************************************************************************
   NAME:       SP_SUMUP_STAT
   PURPOSE:

   REVISIONS:
   Ver        Date            Author           Description
   ---------  ----------------  ---------------  ------------------------------------
   1.0        2014-07-08   yngwie        Created this function.
   1.1        2019-10-01   이재형         convert postgresql
   1.2        2020-07-24   서정우          sp_rank_daily,SP_RANK_MONTHLY 주석 처리


   NOTES:   통계 재집계용

	Input : p_dt VARCHAR 재집계 시작일

	Usage : exec SP_SUMUP_STAT('2014/08/20');

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_SUMUP_STAT';
    v_log_seq 		NUMERIC := 0;		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';	-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_last_dt			TIMESTAMP := NULL;  					-- 최종작업시간
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_date 			RECORD;
    v_startdt 			CHARACTER VARYING(20);  
   
BEGIN
    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';

	    --시작일자
	    v_startdt := p_dt;
	
	   -- 시간별 통계 생성
		for v_date in 
	   		select to_char(generate_series(v_startdt, now()- interval '1 day', '1 hour'),'YYYYMMDDHH24') tm
	   	loop
	   		PERFORM SP_STATS_TM(v_date.tm);
	     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   	end loop;
	   
	   	-- 일별 통계 생성
	   	for v_date in 
	   		select to_char(generate_series(v_startdt, now()- interval '1 day', '1 day'),'YYYYMMDD') tm
	   	loop
	   		PERFORM  SP_STATS_DD(v_date.tm);
	     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   	end loop;  
	  
	   -- 월간 통계 생성
	   	for v_date in 
	   		select to_char(generate_series(v_startdt, now(), '1 month'),'YYYYMM') tm
	   	loop
	   		PERFORM  SP_STATS_MM(v_date.tm);
	     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   	end loop; 
	  
	   	for v_date in 
	   		select to_char(generate_series(v_startdt, now(), '1 year'),'YYYY') tm
	   	loop
	   		PERFORM  SP_STATS_YY(v_date.tm);
	     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   	end loop; 
	
	  /* 예측 업무 배제 - QCELLS 
	    -- 시간별 예측통계 생성
	   for v_date in select to_char(generate_series(v_startdt, now()- interval '1 day', '1 hour'),'YYYYMMDDHH24') tm
	   loop
	   		perform  wems.sp_predict_tm(v_date.tm);
	   end loop;
	
	   -- 일별 예측통계 생성
	   for v_date in select to_char(generate_series(v_startdt, now()- interval '1 day', '1 day'),'YYYYMMDD') tm
	   loop
	   		perform  wems.sp_predict_dd(v_date.tm);
	   end loop;  
	
	   -- 월간 예측통계 생성
	   for v_date in select to_char(generate_series(v_startdt, now(), '1 month'),'YYYYMM') tm
	   loop
	   		perform  wems.sp_predict_mm(v_date.tm);
	   end loop; 
	   */
	
	   	-- 일별 순위통계 생성 
-- 	   	for v_date in
-- 	   		select to_char(generate_series(v_startdt, now()- interval '1 day', '1 day'),'YYYYMMDD') tm
-- 	   	loop
-- 	   		PERFORM SP_RANK_DAILY(v_date.tm);
-- 	     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
-- 	        v_rslt_cnt := v_rslt_cnt + v_work_num;
-- 	   	end loop;
	
	   	-- 월별 순위통계 생성
-- 	   	for v_date in
-- 	   		select to_char(generate_series(v_startdt, now(), '1 month'),'YYYYMM') tm
-- 	   	loop
-- 	   		PERFORM SP_RANK_MONTHLY('SP_RANK_MONTHLY', 0, v_date.tm, 'M');
-- 	     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
-- 	        v_rslt_cnt := v_rslt_cnt + v_work_num;
-- 	   	end loop;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS then
--          	RAISE notice 'v_log_fn[%] EXCEPTION !!! ', v_log_fn;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/


    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$procedure$
;

CREATE OR REPLACE FUNCTION wems.nvl(expr1 double precision, expr2 integer)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
	DECLARE
		v_result	VARCHAR(10000);	  
	BEGIN
		SELECT coalesce($1, $2) into v_result ;
		return v_result;
	end;
$function$
;

CREATE OR REPLACE FUNCTION wems.fn_get_std_col_name(p_tbl text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_GET_STD_COL_NAME
   PURPOSE:   파티션테이블의 기준컬럼명 조회

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2019-10-02   박동환        Postgresql 변환

   NOTES:   입력테이블에 대한 파티션 기준컬럼명 조회
   
    Input :  테이블
	Output : 입력 테이블의 파티션 기준컬럼명 
	
	TEST : SELECT FN_GET_STD_COL_NAME('TB_OPR_ALARM_HIST');   
	         --> 'EVENT_DT'
******************************************************************************/	
DECLARE
    v_ret TEXT := '';
BEGIN
    SELECT   STD_COL_NM     
    INTO		v_ret
   	FROM 	TB_PLF_TBL_META
   	WHERE 	TABLE_NM = p_tbl
    ;
   
    RETURN v_ret;
END;
$function$
;

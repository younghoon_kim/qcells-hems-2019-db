--trigger 생성
--tb_opr_voc 에서 생성하는 트리거
--1종류 tgr_act_voc$tb_opr_voc

CREATE OR REPLACE FUNCTION "tgr_act_voc$tb_opr_voc"() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE

BEGIN
    INSERT INTO tb_opr_act (act_seq, act_type_cd, device_id, device_type_cd, ref_seq, ref_type_cd, ref_desc, ref_user_id, create_dt)
    VALUES (nextval('sq_opr_act'), '10', new.device_id, new.device_type_cd, new.voc_seq, new.voc_type_cd, new.subject
    		, (SELECT instl_user_id FROM tb_bas_device_pms WHERE device_id = new.device_id)
    		, new.create_dt);

    RETURN NULL;

    EXCEPTION WHEN OTHERS THEN
        RETURN NULL;
END;
$$;



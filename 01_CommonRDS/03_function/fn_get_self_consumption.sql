CREATE OR REPLACE FUNCTION wems.fn_get_self_consumption(p_device_id text, p_colec_dd text, p_whbt numeric, p_whday numeric)
 RETURNS TABLE(panel_pw numeric, self_cons_wo_bt numeric, self_suff_wo_bt numeric, self_cons_wt_bt numeric, self_suff_wt_bt numeric)
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_GET_SELF_CONSUMPTION
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-05-20   yngwie     Created this function.
   2.0        2019-09-24   이재형      Postgresql 변환
   
   NOTES:   Self Consumption & Self Sufficiency 개선 예측 데이터를 구한다.

    Input : p_device_id VARCHAR 장비 ID, NULL 이면 장비 데이터 조회 없이 계산
            p_colec_dd VARCHAR  일통계 조회 날짜 (YYYYMMDD)
            p_whbt NUMBER 배터리 용량
            p_whday NUMBER 일 부하 용량

	Usage : SELECT * FROM TABLE(FN_GET_SELF_CONSUMPTION(NULL, NULL, 5, 12));

******************************************************************************/
DECLARE
    v_whbt numeric := p_whbt;
    v_whday numeric := p_whday;
BEGIN

    IF p_device_id IS NOT NULL AND (OCTET_LENGTH(p_device_id) = 21 OR OCTET_LENGTH(p_device_id) = 18) then
    
        SELECT (B.CONS_GRID_PW_H + B.CONS_PV_PW_H + B.CONS_BT_PW_H) * 0.001 AS CONS_PW_H , A.CAPACITY::numeric 
        INTO v_whday, v_whbt
        FROM TB_BAS_DEVICE A , TB_BAS_CITY C , TB_STT_ESS_DD B 
        WHERE  A.DEVICE_ID = p_device_id
        AND A.CITY_CD = C.CITY_CD 
        AND A.DEVICE_ID = B.DEVICE_ID 
        AND A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD  
        AND B.COLEC_DD = p_colec_dd 
        AND C.UTC_OFFSET = B.UTC_OFFSET;

    END IF;

    IF v_whday > 0 then
    	  
    	  RETURN QUERY
          WITH SELFCSM_RATE AS ( 	
         		SELECT '0600' AS FROM_TM, 0.25 AS RATE 
         	    UNION ALL 
         	    SELECT '0630' AS FROM_TM, 0.75 AS RATE 
         	   	UNION ALL 
         	    SELECT '0700' AS FROM_TM,  1.25 AS RATE 
         	   	UNION ALL 
         	    SELECT '0730' AS FROM_TM, 1.75 AS RATE 
         	   	UNION ALL 
         	    SELECT '0800' AS FROM_TM, 2.25 AS RATE 
         	   	UNION ALL 
         	    SELECT '0830' AS FROM_TM, 2.75 AS RATE 
         	   	UNION ALL 
         	    SELECT '0900' AS FROM_TM, 3.25 AS RATE 
         	   	UNION ALL 
         	    SELECT '0930' AS FROM_TM, 3.75 AS RATE 
         	   	UNION ALL 
         	    SELECT '1000' AS FROM_TM, 4.25 AS RATE 
         	   	UNION ALL 
         	    SELECT '1030' AS FROM_TM, 4.75 AS RATE
         	   	UNION ALL 
         	    SELECT '1100' AS FROM_TM, 5.25 AS RATE 
         	   	UNION ALL 
         	    SELECT '1130' AS FROM_TM, 5.75 AS RATE  
         	    UNION ALL 
         	    SELECT '1200' AS FROM_TM,  6.25 AS RATE
         	    UNION ALL 
         	    SELECT '1230' AS FROM_TM,  6.75 AS RATE 
         	    UNION ALL 
         	    SELECT '1300' AS FROM_TM,  7.25 AS RATE 
         	    UNION ALL 
         	    SELECT '1330' AS FROM_TM,  7.75 AS RATE
         	    UNION ALL 
         	    SELECT '1400' AS FROM_TM,  8.25 AS RATE 
         	    UNION ALL 
         	    SELECT '1430' AS FROM_TM,  8.75 AS RATE 
         	    UNION ALL  	
         	    SELECT '1500' AS FROM_TM,  9.25 AS RATE  
         	    UNION ALL  	
         	    SELECT '1530' AS FROM_TM,  9.75 AS RATE  
         	    UNION ALL  	
         	    SELECT '1600' AS FROM_TM, 10.25 AS RATE  
         	    UNION ALL  	
         	    SELECT '1630' AS FROM_TM, 10.75 AS RATE  
         	    UNION ALL  	
         	    SELECT '1700' AS FROM_TM, 11.25 AS RATE  
         	    UNION ALL  	
         	    SELECT '1730' AS FROM_TM, 11.75 AS RATE   )  
         SELECT  	PANEL_PW::float8  	, 
                    ROUND( ((SUM(PV_PW_PER_HALFHOUR) - SUM(PV_CONS_PER_HALFHOUR)) / SUM(PV_PW_PER_HALFHOUR))::numeric, 9)::float8 AS SELF_CONS_WO_BT, 
                    ROUND( ((SUM(PV_PW_PER_HALFHOUR) - SUM(PV_CONS_PER_HALFHOUR)) / v_whday)::numeric, 9)::float8 AS SELF_SUFF_WO_BT , 
                    ROUND( (((SUM(PV_PW_PER_HALFHOUR) - SUM(PV_CONS_PER_HALFHOUR)) + 
                   			CASE 
                   				WHEN SUM(PV_CONS_PER_HALFHOUR) > v_whbt THEN v_whbt 
                   				ELSE SUM(PV_CONS_PER_HALFHOUR) 
                   			END)  / SUM(PV_PW_PER_HALFHOUR))::numeric, 9)::float8 AS SELF_CONS_WT_BT ,
                   	ROUND( (((SUM(PV_PW_PER_HALFHOUR) - SUM(PV_CONS_PER_HALFHOUR)) + 
                   			CASE 
                   				WHEN SUM(PV_CONS_PER_HALFHOUR) > v_whbt THEN v_whbt 
                   			ELSE SUM(PV_CONS_PER_HALFHOUR) 
                   			END)  / v_whday)::numeric, 9)::float8 AS SELF_SUFF_WT_BT  
        FROM (  	
       			select	A.RATE, 
       					A.FROM_TM, 
       					A.CONS_DAY, 
       					0.5 * SIN(A.RATE / 12 * (asin(1)*2)::numeric(14,13)) * B.PANEL_PW AS PV_PW_PER_HALFHOUR, 
       					CASE 
       						WHEN 0.5 * SIN(A.RATE / 12 * (asin(1)*2)::numeric(14,13)) * B.PANEL_PW - A.CONS_DAY < 0 THEN 0  	   	  		
       						ELSE 0.5 * SIN(A.RATE / 12 * (asin(1)*2)::numeric(14,13)) * B.PANEL_PW - A.CONS_DAY   	   	 
       					END AS PV_CONS_PER_HALFHOUR , 
       					B.PANEL_PW  	
       			FROM  		( 
       							select	FROM_TM, 
       									RATE, 
       									CASE 
       										WHEN FROM_TM IN ('0700', '0730') THEN v_whday / 48*2  		  		
       										ELSE v_whday / 56  		  	  
       									END AS CONS_DAY  		  
       							FROM   SELFCSM_RATE  ) A  , 
       						( 
       							select generate_series(1,6,1) AS PANEL_PW  ) B  
       			) C  
       	GROUP BY  	PANEL_PW  
        ORDER BY  	PANEL_PW ;

    END IF;
END;
$function$
;

CREATE OR REPLACE FUNCTION wems.decode(param numeric, con1 character varying, val1 character varying, val0 numeric)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
	DECLARE
		v_result	numeric;	
	
	BEGIN
		select CASE param WHEN  con1::numeric THEN val1::numeric ELSE val0 end into v_result;
		RETURN v_result;
	END;
$function$
;

CREATE OR REPLACE FUNCTION wems.to_char(param numeric)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
	DECLARE
		v_result	VARCHAR(10000);	
	
	BEGIN
		select param::VARCHAR into v_result;
		RETURN v_result;
	END;
$function$

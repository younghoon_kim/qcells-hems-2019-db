CREATE OR REPLACE FUNCTION wems.dec2hex(n numeric)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
 /******************************************************************************
   NAME:       DEC2HEX
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   2.0        2019-10-08     이재형                  postgresql 변환    
   
   NOTES:   Decimal(10진수) to hex(16진수)

    Input : 

    Output :

 ******************************************************************************/
DECLARE
    hexval CHARACTER VARYING(64);
    n2 numeric := n;
    digit numeric;
    hexdigit CHARACTER(1);
BEGIN
    WHILE (n2 > 0) LOOP
        digit := MOD(n2, 16);

        IF digit > 9 THEN
            hexdigit := CHR(TRUNC((ASCII('A') + digit - 10)::NUMERIC)::INTEGER);
        ELSE
            hexdigit := digit::char;
        END IF;
        hexval := CONCAT_WS('', hexdigit, hexval);
        n2 := TRUNC((n2 / 16::NUMERIC)::NUMERIC);
    END LOOP;
    RETURN hexval;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$function$
;

CREATE OR REPLACE FUNCTION wems.fn_get_conv_na(p_value text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
*****************************************************************************
   NAME:       FN_GET_CONV_NA
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-04-17   yngwie       1. Created this function.


   NOTES:   RAW 테이블 컬럼 중 문자열로 입력되지만 수치형 데이터로 처리해야 하는 경우 사용한다.
            2020.11.10 younghoon NOTES 수정
            'NA'문자열을 NULL값으로 변환하여준다.



	Input : p_value - VARCHAR

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_CONV_NA('NA') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(100);
BEGIN
    v_value := (CASE
        WHEN p_value = 'NA' THEN NULL
        ELSE p_value
    END)::TEXT;
    RETURN v_value;
    EXCEPTION
        WHEN others THEN
            RETURN SQLERRM;
END;
$function$
;

CREATE OR REPLACE PROCEDURE wems.sp_set_err_list_flag()
 LANGUAGE plpgsql
AS $procedure$
/******************************************************************************
   NAME:       SP_SET_ERR_LIST_FLAG
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-29   yngwie       Created this function.
   1.1        2019-10-01   이재형        Convert postgresql   

   NOTES:  SP_GET_ERRLIST 로 출력한 장애상황 목록을 임시테이블에서 삭제한다.

	Input :

	Usage :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_SET_ERR_LIST_FLAG';
    v_log_seq 		NUMERIC := 0;		--	nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';	-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_last_dt			TIMESTAMP := NULL;  					-- 최종작업시간
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3

    p_dt				VARCHAR   	:= ' ';	
   
    -- (2)업무처리 변수
   
BEGIN
    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
	
	    UPDATE TB_OPR_ALARM 
	    SET 		NOTI_FLAG = 'Y' 
	    WHERE 	( ALARM_ID, ALARM_ID_SEQ ) 
	    			IN (	
	    					SELECT 	ALARM_ID, ALARM_ID_SEQ 
	   			    		FROM 	TB_OPR_ALARM_ERR_LIST 
	   			    	) ;
	
	    TRUNCATE TABLE TB_OPR_ALARM_ERR_LIST;
   
     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
       
--      	RAISE notice 'v_log_fn[%] v_log_seq[%] Insert TB_STT_RANK_MM Table [%]건, p_dt[%]', v_log_fn, v_log_seq, v_work_num, p_dt;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS then
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$procedure$
;

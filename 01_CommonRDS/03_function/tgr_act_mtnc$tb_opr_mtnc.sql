--trigger 생성
--tb_opr_mtnc 에서 생성하는 트리거
--1종류 tgr_act_mtnc$tb_opr_mtnc


CREATE OR REPLACE FUNCTION "tgr_act_mtnc$tb_opr_mtnc"() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE

BEGIN
    INSERT INTO tb_opr_act (act_seq, act_type_cd, device_id, device_type_cd, ref_seq, ref_type_cd, ref_desc, ref_user_id, create_dt)
    VALUES (nextval('sq_opr_act'), '20', new.device_id, new.device_type_cd, new.mtnc_seq, new.mtnc_type_cd, new.subject
    		, ( SELECT instl_user_id FROM tb_bas_device_pms WHERE device_id = new.device_id)
    		, new.create_dt);

    RETURN NULL;

    EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END;
$$;


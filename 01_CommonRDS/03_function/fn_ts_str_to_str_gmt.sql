CREATE OR REPLACE FUNCTION wems.fn_ts_str_to_str_gmt(p_datestr text, p_tz numeric, p_srcdf text, p_targetdf text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_TS_STR_TO_STR_GMT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-11   yngwie       1. Created this function.
   2.0        2019-09-23   박동환       Postgresql 전환

   NOTES:   입력받은 날짜형식의 문자열을 TIMESTAMP WITH TIME ZONE 형식으로 변환하여
            GMT 0 의 날짜형식의 문자열로 반환한다.
            => 결과적으로 입력된 시간에  OFFSET만큼 마이너스한 값과 동일(박동환)

	Input : p_dateStr - 날짜형식의 문자열 (ex : 2014-04-21 01:00:00)
	        p_tz - p_dateStr 의 GMT Offset (-11 ~ 12)
	        p_srcDf - p_dateStr 의 날짜포맷(ex : YYYY-MM-DD HH24:MI:SS)
	        p_targetDf - 반환값의 날짜포맷 (ex : YYYYMMDDHH24MISS)


	Usage : SELECT 절에서만 사용하고, 조건절에서는 사용하지 마세요. 알수 없는 오류 발생함.
	        SELECT FN_TS_STR_TO_STR_GMT('2014-06-11 08:12:00', 1, 'YYYY-MM-DD HH24:MI:SS', 'YYYYMMDD HH24MISS')  FROM DUAL;


******************************************************************************/
DECLARE
    o_return text := NULL;
BEGIN
 	select to_char(to_timestamp(p_datestr, p_srcdf) - concat(p_tz,' hours')::interval, p_targetdf)
	into o_return;

	return o_return;

     EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$function$
;

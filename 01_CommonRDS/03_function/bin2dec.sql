CREATE OR REPLACE FUNCTION wems.bin2dec(binval character)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
 /******************************************************************************
   NAME:       BIN2DEC
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   2.0        2019-10-08     이재형                  postgresql 변환    
   
   NOTES:   Binary(2진수) to Decimal(10진수)

    Input : 

    Output :

 ******************************************************************************/
DECLARE
    i numeric;
    digits numeric;
    result numeric := 0;
    current_digit CHARACTER(1);
    current_digit_dec numeric;
BEGIN
    digits := LENGTH(binval);

    FOR i IN 1..digits LOOP
        current_digit := substr(binval, i, 1);
        current_digit_dec := current_digit::numeric;
        result := (result * 2) + current_digit_dec;
    END LOOP;
    RETURN result;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$function$
;

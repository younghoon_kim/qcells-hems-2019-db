CREATE OR REPLACE FUNCTION wems.fn_get_partition_name(p_tbl text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       FN_GET_PARTITION_NAME
   PURPOSE:   파티션테이블명 조회

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2019-10-02   박동환        Postgresql 변환

   NOTES:   입력테이블에 대한 최신 파티션 테이블명 조회
   
    Input :  테이블명	        
	Output : 입력 테이블의 한 시간 전 시간을 반영하는 파티션 테이블명 
	
	TEST : SELECT FN_GET_PARTITION_NAME('TB_OPR_ALARM_HIST');   
	         --> 'pt_opr_alarm_hist_201910'
******************************************************************************/	
DECLARE
    v_ret TEXT := '';
BEGIN

	SELECT 	CHILD.RELNAME
	INTO		v_ret
	FROM 	PG_INHERITS
    JOIN 		PG_CLASS PARENT ON PG_INHERITS.INHPARENT = PARENT.OID
    JOIN 		PG_CLASS CHILD   ON PG_INHERITS.INHRELID   = CHILD.OID
    JOIN 		TB_PLF_TBL_META M ON (PARENT.RELNAME= LOWER(M.TABLE_NM)  )
    WHERE	M.PARTITION_TYPE IS NOT null
    AND		M.TABLE_NM = UPPER(p_tbl)
    AND		( CHILD.RELNAME like 'pt_'||SUBSTRING(LOWER(p_tbl), 4)||'%'||TO_CHAR(SYS_EXTRACT_UTC(NOW() -  interval '1 HOUR'), 'YYYYMMDD') || '%'
    				OR
				 CHILD.RELNAME like 'pt_'||SUBSTRING(LOWER(p_tbl), 4)||'%'||TO_CHAR(SYS_EXTRACT_UTC(NOW() -  interval '1 HOUR'), 'YYYYMM') || '%'
				)
	;

    RETURN v_ret;
   
END;
$function$
;

CREATE OR REPLACE FUNCTION wems.fn_get_device_type(p_value text, p_column text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
*****************************************************************************
   NAME:       FN_GET_DEVICE_TYPE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-03-31   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                  Postgresql 변환
   NOTES:   입력받은  DEVICE_ID 의 TB_PLF_CODE_INFO 테이블의 p_column 값을 반환

	Input : p_value - DEVICE_ID
	        p_column - TB_PLF_CODE_INFO 의 컬럼 명

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_DEVICE_TYPE('AR00460036Z114827015X', 'CODE');

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(200);
    v_sql text;
begin
	
	
	v_sql := 'SELECT '||p_column||' 
    FROM wems.tb_plf_code_info     
    WHERE grp_cd = ''DEVICE_TYPE_CD'' 
    AND cd_desc =     
   				CASE       
   					WHEN substr($1, 1, 2) = ''HS'' THEN ''AR''         
   				    WHEN substr($2, 8, 4) = ''4601'' THEN ''AR''         
   				    WHEN substr($3, 8, 4) = ''5001'' THEN ''AR''       
   				    ELSE substr($4, 1, 2)     
   				end';
   			
   	execute v_sql into v_value using p_value, p_value, p_value,p_value;
   	return v_value;
END;
$function$
;

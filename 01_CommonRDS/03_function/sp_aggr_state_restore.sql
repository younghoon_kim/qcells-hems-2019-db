CREATE OR REPLACE PROCEDURE wems.sp_aggr_state_restore(p_device_id text, p_from_datetime text, p_to_datetime text)
 LANGUAGE plpgsql
AS $procedure$
 /******************************************************************************
   NAME:       SP_AGGR_STATE_RESTORE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-16   yngwie       Created this function.
   1.1        2016-01-05   jkm1020     ESM (EMS Sleep Mode) 추가
   2.0        2019-10-08     이재형      postgresql 변환    
   
   NOTES:   1분주기 수집데이터를 가지고 상태/예측/장애 데이터를 생성한다.

    Input :

    Output :

 ******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_AGGR_STATE_RESTORE';
    v_log_seq 		NUMERIC := 0;		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';							-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    p_dt    			VARCHAR	:= ''; 
    v_curTm     		VARCHAR(4);
    v_handleFlag 	VARCHAR;
    v_selectDt   		VARCHAR; --TIMESTAMP WITH TIME ZONE;
    v_row           	record; 
	
   	v_sql				VARCHAR;
    v_tbl				VARCHAR;
   	v_ExeCnt1 		INTEGER 		:= 0;
   	v_ExeCnt2 		INTEGER 		:= 0;
   	v_ExeCnt3 		INTEGER 		:= 0;
 
    arrayTbl 			VARCHAR ARRAY := ARRAY['TB_RAW_EMS_INFO', 'TB_RAW_PCS_INFO', 'TB_RAW_BMS_INFO'];
    arrayRawTbl 	VARCHAR ARRAY := ARRAY['TB_RAW_EMS_INFO', 'TB_RAW_PCS_INFO', 'TB_RAW_BMS_INFO'];
   
BEGIN

	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt 
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
  
		v_rslt_cd := 'A';
	
		/*
        2015.04.16 yngwie
        DONE_FLAG 에 임시로 마킹을 할 Y, N 을 제외한 문자를 임의 생성한다.
        
        2015.04.17 개발/운영DB 에서 DBMS_RANDOM 패키지 사용못함
        2015.07.03 raw 데이터 처리여부는 handleFlag 를 사용하는 것으로 변경
        */

        v_handleFlag := TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'YYYYMMDDHH24MISS');   
        v_selectDt := 'SYS_EXTRACT_UTC(NOW()) - INTERVAL ''5 MINUTE'' ';
       
        INSERT INTO TB_RAW_EMS_INFO_TMP 
        -- modified by stjoo 20151229 : add hint statement for performance improvement
        SELECT /*+ index(a PK_RAW_EMS_INFO_1) */ 
            DEVICE_ID  
            , COLEC_DT  
            , DONE_FLAG  
            , COL0, COL1, COL2, COL3, COL4, COL5, COL6, COL7, COL8, COL9, COL10  
            , COL11, COL12, COL13, COL14, COL15, COL16, COL17, COL18, COL19, COL20  
            , COL21, COL22, COL23, COL24, COL25, COL26, COL27, COL28, COL29, COL30 
            , COL31, COL32, COL33, COL34, COL35, COL36, COL37, COL38, COL39, COL40 
            , COL41, COL42, COL43, COL44, COL45, COL46, COL47, COL48, COL49, COL50
            , COL51, COL52, COL53, COL54, COL55, COL56, COL57, COL58, COL59, COL60 
            , COL61, COL62, COL63, COL64, COL65, COL66, COL67, COL68, COL69, COL70 
            , COL71, COL72, COL73, COL74, COL75, COL76, COL77, COL78, COL79, COL80 
            , COL81, COL82, COL83, COL84, COL85, COL86, COL87 
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR( COL88, 3))) ), 'FM00000000') AS COL88  
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR( COL89, 3))) ), 'FM00000000') AS COL89  
            , COL90, COL91, COL92, COL93, COL94, COL95, COL96, COL97, COL98, COL99, COL100
			, COL101, COL102, COL103, COL104, COL105, COL106, COL107, COL108, COL109, COL110 
			, COL111, COL112, COL113, COL114, COL115, COL116, COL117, COL118, COL119, COL120 
			, COL121, COL122, COL123, COL124, COL125, COL126, COL127, COL128, COL129, COL130
			, COL131, COL132, COL133, COL134, COL135, COL136, COL137, COL138, COL139, COL140
			, COL141, COL142, COL143, COL144, COL145, COL146, COL147, COL148, COL149, COL150
			, COL151, COL152, COL153, COL154, COL155, COL156, COL157, COL158, COL159, COL160
			, COL161, COL162, COL163, COL164, COL165, COL166, COL167, COL168, COL169, COL170
			, COL171, COL172, COL173, COL174, COL175, COL176, COL177, COL178, COL179, COL180
			, COL181, COL182, COL183, COL184, COL185, COL186, COL187, COL188, COL189, COL190
			, COL191, COL192, COL193, COL194, COL195, COL196, COL197, COL198, COL199, COL200
			, COL201, COL202, COL203, COL204, COL205, COL206, COL207, COL208, COL209, COL210 
			, COL211, COL212, COL213, COL214, COL215, COL216, COL217, COL218, COL219, COL220 
			, COL221, COL222, COL223, COL224, COL225, COL226, COL227, COL228, COL229, COL230 
			, COL231, COL232, COL233, COL234, COL235, COL236, COL237, COL238, COL239, COL240 
			, COL241, COL242, COL243, COL244, COL245, COL246, COL247, COL248, COL249, COL250 
			, COL251, COL252, COL253, COL254, COL255, COL256, COL257, COL258, COL259, COL260 
			, COL261, COL262, COL263, COL264, COL265, COL266, COL267, COL268, COL269, COL270
			, COL271, COL272, COL273, COL274, COL275, COL276, COL277, COL278, COL279, COL280
			, COL281, COL282, COL283, COL284, COL285, COL286, COL287, COL288, COL289, COL290
			, COL291, COL292, COL293, COL294, COL295, COL296, COL297, COL298, COL299, COL300
			, COL301, COL302, COL303, COL304, COL305, COL306, COL307, COL308, COL309, COL310
			, COL311, COL312, COL313, COL314, COL315, COL316, COL317, COL318, COL319, COL320
			, COL321, COL322, COL323, COL324, COL325, COL326, COL327, COL328, COL329, COL330
			, COL331, COL332, COL333, COL334, COL335, COL336, COL337, COL338, COL339, COL340
			, COL341            
			, CREATE_DT 
            , HANDLE_FLAG 
        FROM  TB_RAW_EMS_INFO --  arrayRawTbl[0]
         WHERE COLEC_DT BETWEEN  p_from_datetime  AND p_to_datetime
         AND DEVICE_ID = case p_device_id 
         					when '' then DEVICE_ID
         					when null  then DEVICE_ID
         					else p_device_id
         				  end 
         AND COALESCE(DONE_FLAG, 'N') = 'N' 
         AND COALESCE(HANDLE_FLAG, 'N') = 'N';

        
        GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	    v_ExeCnt1 := v_work_num;
        
	   	-- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Insert TB_RAW_EMS_INFO_TMP Table : ', v_work_num));
		RAISE NOTICE 'v_log_fn[%] Insert TB_RAW_EMS_INFO_TMP Table [%]건 ', v_log_fn, v_work_num;
	   
		INSERT INTO TB_RAW_PCS_INFO_TMP 
        -- modified by stjoo 20151229 : add hint statement for performance improvement
        SELECT /*+ index(a PK_RAW_PCS_INFO) */ 
            A.DEVICE_ID  
            , A.COLEC_DT     
            , A.DONE_FLAG    
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL0, 3))) ), 'FM00000000') AS COL0  
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL1, 3))) ), 'FM00000000') AS COL1  
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL2, 3))) ), 'FM00000000') AS COL2  
    		 , A.COL3, A.COL4, A.COL5, A.COL6, A.COL7, A.COL8, A.COL9, A.COL10
    		 , A.COL11, A.COL12, A.COL13, A.COL14, A.COL15, A.COL16, A.COL17, A.COL18, A.COL19, A.COL20
    		 , A.COL21, A.COL22, A.COL23, A.COL24, A.COL25, A.COL26, A.COL27, A.COL28, A.COL29, A.COL30
    		 , A.COL31, A.COL32, A.COL33, A.COL34, A.COL35, A.COL36, A.COL37, A.COL38, A.COL39, A.COL40
    		 , A.COL41, A.COL42, A.COL43, A.COL44, A.COL45, A.COL46, A.COL47, A.COL48, A.COL49, A.COL50
    		 , A.COL51, A.COL52, A.COL53, A.COL54, A.COL55, A.COL56, A.COL57, A.COL58, A.COL59, A.COL60
    		 , A.COL61, A.COL62, A.COL63, A.COL64, A.COL65, A.COL66, A.COL67, A.COL68, A.COL69, A.COL70
    		 , A.COL71, A.COL72, A.COL73, A.COL74, A.COL75, A.COL76, A.COL77, A.COL78, A.COL79, A.COL80
    		 , A.COL81, A.COL82, A.COL83, A.COL84, A.COL85, A.COL86, A.COL87, A.COL88, A.COL89, A.COL90
    		 , A.COL91, A.COL92, A.COL93, A.COL94, A.COL95, A.COL96, A.COL97, A.COL98, A.COL99, A.COL100
    		 , A.COL101, A.COL102, A.COL103, A.COL104, A.COL105, A.COL106, A.COL107, A.COL108, A.COL109, A.COL110
    		 , A.COL111, A.COL112, A.COL113, A.COL114, A.COL115, A.COL116, A.COL117, A.COL118, A.COL119, A.COL120
    		 , A.COL121, A.COL122, A.COL123, A.COL124, A.COL125, A.COL126, A.COL127, A.COL128, A.COL129, A.COL130
    		 , A.COL131, A.COL132, A.COL133, A.COL134, A.COL135, A.COL136, A.COL137, A.COL138, A.COL139, A.COL140
    		 , A.COL141, A.COL142, A.COL143, A.COL144, A.COL145, A.COL146, A.COL147, A.COL148, A.COL149, A.COL150
    		 , A.COL151, A.COL152, A.COL153, A.COL154, A.COL155, A.COL156, A.COL157, A.COL158, A.COL159, A.COL160
    		 , A.COL161, A.COL162, A.COL163, A.COL164, A.COL165, A.COL166, A.COL167, A.COL168, A.COL169, A.COL170
    		 , A.COL171, A.COL172, A.COL173, A.COL174, A.COL175, A.COL176, A.COL177, A.COL178, A.COL179, A.COL180
    		 , A.COL181, A.COL182, A.COL183, A.COL184, A.COL185, A.COL186, A.COL187, A.COL188, A.COL189, A.COL190
    		 , A.COL191, A.COL192, A.COL193, A.COL194, A.COL195, A.COL196, A.COL197, A.COL198, A.COL199, A.COL200
    		 , A.COL201, A.COL202, A.COL203, A.COL204, A.COL205, A.COL206, A.COL207, A.COL208, A.COL209, A.COL210
    		 , A.COL211, A.COL212, A.COL213, A.COL214, A.COL215, A.COL216, A.COL217, A.COL218, A.COL219, A.COL220
    		 , A.COL221, A.COL222, A.COL223, A.COL224, A.COL225, A.COL226 
            , A.CREATE_DT 
            , A.HANDLE_FLAG 
         FROM TB_RAW_EMS_INFO_TMP B , 
         		TB_RAW_PCS_INFO  A
         WHERE  B.COLEC_DT = A.COLEC_DT 
         AND B.DEVICE_ID = A.DEVICE_ID;
        
        GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   
	    -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Insert TB_RAW_PCS_INFO_TMP Table : ', v_work_num));
		RAISE NOTICE 'v_log_fn[%] Insert TB_RAW_PCS_INFO_TMP Table [%]건', v_log_fn, v_work_num;


        INSERT INTO TB_RAW_BMS_INFO_TMP 
        /* modified by stjoo 20151229 : add hint statement for performance improvement */
        SELECT /*+ index(a PK_RAW_BMS_INFO) */ 
             A.DEVICE_ID 
            , A.COLEC_DT    
            , A.DONE_FLAG  
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL0, 3))) ), 'FM00000000') AS COL0
            , A.COL1, A.COL2, A.COL3, A.COL4, A.COL5, A.COL6, A.COL7, A.COL8, A.COL9, A.COL10    
            , A.COL11, A.COL12, A.COL13, A.COL14, A.COL15, A.COL16, A.COL17, A.COL18, A.COL19    
--          , A.COL20   
            , case when COALESCE(A.COL20,0)::numeric > 4 then '0' else A.COL20 end COL20  
            , A.COL21, A.COL22, A.COL23    
--          , A.COL24    
            , CASE WHEN position('0x' in A.COL24) > 0 THEN TO_CHAR(HEX2DEC(UPPER(SUBSTR(A.COL24, 3)))) ELSE A.COL24 END COL24
            , A.COL25, A.COL26, A.COL27, A.COL28, A.COL29, A.COL30    
            , A.COL31, A.COL32, A.COL33, A.COL34, A.COL35, A.COL36, A.COL37, A.COL38, A.COL39, A.COL40    
            , A.CREATE_DT 
            , A.HANDLE_FLAG 
        FROM 
            TB_RAW_EMS_INFO_TMP B , 
        	TB_RAW_BMS_INFO A -- arrayRawTbl[2]
        WHERE  	B.COLEC_DT = A.COLEC_DT 
           AND 	B.DEVICE_ID = A.DEVICE_ID
		;
	
      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   
	    -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Insert TB_RAW_BMS_INFO_TMP Table : ', v_work_num));
		RAISE NOTICE 'v_log_fn[%] Insert TB_RAW_BMS_INFO_TMP Table [%]건', v_log_fn, v_work_num;
 	
        IF v_ExeCnt1 > 0 THEN
            /* ---------------------- */
            /* 0. 다른 세션에서 처리되지 않도록 플래그 설정 */
            /* ---------------------- */

              FOREACH v_tbl SLICE 1 IN ARRAY arrayTbl
			LOOP
                v_sql := FORMAT(
                	'UPDATE %s B '	||
                	'SET 		B.HANDLE_FLAG = %s ' 	|| 
                	'WHERE 	COALESCE(B.DONE_FLAG, ''N'') = ''N'' '	||
	                'AND 		COALESCE(B.HANDLE_FLAG, ''N'') = ''N'' '	||
	                'AND 		(B.DEVICE_ID, B.COLEC_DT) IN ( SELECT   DEVICE_ID, COLEC_DT FROM %s_TMP ) ',
	                v_tbl, v_handleFlag, v_tbl
	               );
			     
	            EXECUTE v_sql;

	           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--				CALL wems.SP_LOG(v_log_seq, v_log_fn, 'Update ' || v_tbl || ' Records Temporary flag : ' || v_work_num || ', ' || v_handleFlag);
				RAISE NOTICE 'v_log_fn[%] Update % [%]건 v_handleFlag[%]', v_log_fn, v_tbl, v_work_num, v_handleFlag;
			END LOOP;
				

          
		  
            /* ---------------------- */
            /* 2. 상태정보 데이터 처리 */
            /* ---------------------- */
            /* 대상 레코드들의 CO2와 금액을 계산한 결과를 TEMP 테이블에 Insert */
            /* 2014.08.12 E700번대 장애는 사용자에게 보여주지 않도록 수정 */
            /* 2014.08.20 E900번대와 C999 장애만 사용자에게 보여주도록 수정 => 장애 발생된 상태 그대로 보여주도록 원복함 */
            
			 INSERT INTO TB_OPR_ESS_STATE_TMP (
	              DEVICE_ID 
	            , DEVICE_TYPE_CD 
	            , COLEC_DT 
	            , OPER_STUS_CD 
	            , PV_PW 
	            , PV_PW_H 
	            , PV_PW_PRICE 
	            , PV_PW_CO2 
	            , PV_STUS_CD 
	            , CONS_PW 
	            , CONS_PW_H 
	            , CONS_PW_PRICE 
	            , CONS_PW_CO2 
	            , BT_PW 
	            , BT_CHRG_PW_H 
	            , BT_CHRG_PW_PRICE 
	            , BT_CHRG_PW_CO2 
	            , BT_DCHRG_PW_H 
	            , BT_DCHRG_PW_PRICE 
	            , BT_DCHRG_PW_CO2 
	            , BT_SOC 
	            , BT_SOH 
	            , BT_STUS_CD 
	            , GRID_PW 
	            , GRID_OB_PW_H 
	            , GRID_OB_PW_PRICE 
	            , GRID_OB_PW_CO2 
	            , GRID_TR_PW_H 
	            , GRID_TR_PW_PRICE 
	            , GRID_TR_PW_CO2 
	            , GRID_STUS_CD 
	            , PCS_PW 
	            , PCS_FD_PW_H 
	            , PCS_PCH_PW_H 
	            , EMS_OPMODE 
	            , PCS_OPMODE1 
	            , PCS_OPMODE2 
	            , PCS_TGT_PW 
	            , FEED_IN_LIMIT 
	            , MAX_INVERTER_PW_CD 
	            , OUTLET_PW 
	            , OUTLET_PW_H 
	            , OUTLET_PW_PRICE 
	            , OUTLET_PW_CO2 
	            , CREATE_DT 
	            -- [ 2015.07.06 yngwie 수집 컬럼 추가
	            , PCS_COMM_ERR_RATE 
	            , SMTR_COMM_ERR_RATE 
	            , M2M_COMM_ERR_RATE            
	            , SMTR_TP_CD 
	            , SMTR_PULSE_CNT 
	            , SMTR_MODL_CD 
	            , PV_MAX_PWR1 
	            , PV_MAX_PWR2 
	            , INSTL_RGN 
	            , MMTR_SLV_ADDR 
	            , BASICMODE_CD 
	            , PCS_FLAG0 
	            , PCS_FLAG1 
	            , PCS_FLAG2 
	            , PCS_OPMODE_CMD1 
	            , PCS_OPMODE_CMD2 
	            , PV_V1 
	            , PV_I1 
	            , PV_PW1 
	            , PV_V2 
	            , PV_I2 
	            , PV_PW2 
	            , INVERTER_V 
	            , INVERTER_I 
	            , INVERTER_PW 
	            , DC_LINK_V 
	            , G_RLY_CNT 
	            , BAT_RLY_CNT 
	            , GRID_TO_GRID_RLY_CNT 
	            , BAT_PCHRG_RLY_CNT 
	            , BMS_FLAG0 
	            , BMS_DIAG0 
	            , RACK_V 
	            , RACK_I 
	            , CELL_MAX_V 
	            , CELL_MIN_V 
	            , CELL_AVG_V 
	            , CELL_MAX_T 
	            , CELL_MIN_T 
	            , CELL_AVG_T 
	            , TRAY_CNT 
	            , SYS_ST_CD 
	            , BT_CHRG_ST_CD 
	            , BT_DCHRG_ST_CD 
	            , PV_ST_CD 
	            , GRID_ST_CD 
	            , SMTR_ST_CD 
	            , SMTR_TR_CNTER 
	            , SMTR_OB_CNTER        
	            , DN_ENABLE    
	            , DC_START     
	            , DC_END   
	            , NC_START                 
	            , NC_END       
	            -- ]            
	           --[신규모델에 대한 모니터링 추가
	            , GRID_CODE0 
	            , GRID_CODE1 
	            , GRID_CODE2 
	            , PCON_BAT_TARGETPOWER 
	            , GRID_CODE3 
	            , BDC_MODULE_CODE0 
	            , BDC_MODULE_CODE1 
	            , BDC_MODULE_CODE2 
	            , BDC_MODULE_CODE3 
	            , FAULT5_REALTIME_YEAR 
	            , FAULT5_REALTIME_MONTH 
	            , FAULT5_DAY 
	            , FAULT5_HOUR 
	            , FAULT5_MINUTE 
	            , FAULT5_SECOND 
	            , FAULT5_DATAHIGH 
	            , FAULT5_DATALOW 
	            , FAULT6_REALTIME_YEAR 
	            , FAULT6_REALTIME_MONTH 
	            , FAULT6_REALTIME_DAY 
	            , FAULT6_REALTIME_HOUR 
	            , FAULT6_REALTIME_MINUTE 
	            , FAULT6_REALTIME_SECOND 
	            , FAULT6_DATAHIGH 
	            , FAULT6_DATALOW 
	            , FAULT7_REALTIME_YEAR 
	            , FAULT7_REALTIME_MONTH 
	            , FAULT7_REALTIME_DAY 
	            , FAULT7_REALTIME_HOUR 
	            , FAULT7_REALTIME_MINUTE 
	            , FAULT7_REALTIME_SECOND 
	            , FAULT7_DATAHIGH 
	            , FAULT7_DATALOW 
	            , FAULT8_REALTIME_YEAR 
	            , FAULT8_REALTIME_MONTH 
	            , FAULT8_REALTIME_DAY 
	            , FAULT8_REALTIME_HOUR 
	            , FAULT8_REALTIME_MINUTE 
	            , FAULT8_REALTIME_SECOND 
	            , FAULT8_DATAHIGH 
	            , FAULT8_DATALOW 
	            , FAULT9_REALTIME_YEAR 
	            , FAULT9_REALTIME_MONTH 
	            , FAULT9_REALTIME_DAY 
	            , FAULT9_REALTIME_HOUR 
	            , FAULT9_REALTIME_MINUTE 
	            , FAULT9_REALTIME_SECOND 
	            , FAULT9_DATAHIGH 
	            , FAULT9_DATALOW 
	            --]
            ) 
            SELECT   
                A.DEVICE_ID AS DEVICE_ID   
                , B.DEVICE_TYPE_CD AS DEVICE_TYPE_CD   
                , A.COLEC_DT AS COLEC_DT   
                , 0 AS OPER_STUS_CD 
                , A.COL8::numeric AS PV_PW   
                , A.COL17::numeric AS PV_PW_H   
                , TRUNC(ABS(A.COL17)::numeric * ET.UNIT_PRICE * .001, 2) AS PV_PW_PRICE   
                , ABS(A.COL17)::numeric * D.CARB_EMI_QUITY * .001 AS PV_PW_CO2   
                , SUBSTR(A.COL88, 3, 1) AS PV_STUS_CD   
                , A.COL7::numeric AS CONS_PW   
                , A.COL16::numeric AS CONS_PW_H   
                , TRUNC(ABS(A.COL16::numeric) * E.UNIT_PRICE * .001, 2) AS CONS_PW_PRICE   
                , ABS(A.COL16::numeric) * D.CARB_EMI_QUITY * .001 AS CONS_PW_CO2   
                , A.COL10::numeric AS BT_PW   
                , A.COL20::numeric AS BT_CHRG_PW_H   
                , TRUNC(ABS(A.COL20::numeric) * E.UNIT_PRICE * .001, 2) AS BT_CHRG_PW_PRICE   
                , ABS(A.COL20::numeric) * D.CARB_EMI_QUITY * .001 AS BT_CHRG_PW_CO2   
                , A.COL21::numeric AS BT_DCHRG_PW_H   
                , TRUNC(ABS(A.COL21::numeric) * E.UNIT_PRICE * .001, 2) AS BT_DCHRG_PW_PRICE   
                , ABS(A.COL21::numeric) * D.CARB_EMI_QUITY * .001 AS BT_DCHRG_PW_CO2   
                , case when (A.COL11::numeric >= 1000 OR A.COL11::numeric < -999) then -999 else A.COL11::numeric end AS BT_SOC   
                , COALESCE(FN_GET_CONV_NA(A.COL13), 0)::numeric AS BT_SOH   
                , CASE   
                     WHEN SUBSTR(A.COL88, 1, 1) = '1' THEN '0'  
                     WHEN SUBSTR(A.COL88, 2, 1) = '1' THEN '1'  
                     ELSE '2'   
                  END AS BT_STUS_CD   
                , A.COL6::numeric AS GRID_PW   
                , A.COL15::numeric AS GRID_OB_PW_H   
                , TRUNC(ABS(A.COL15::numeric) * E.UNIT_PRICE * .001, 2) AS GRID_OB_PW_PRICE   
                , ABS(A.COL15::numeric) * D.CARB_EMI_QUITY * .001 AS GRID_OB_PW_CO2   
                , A.COL14::numeric AS GRID_TR_PW_H   
                , TRUNC(ABS(A.COL14::numeric) * ET.UNIT_PRICE * .001, 2) AS GRID_TR_PW_PRICE   
                , ABS(A.COL14::numeric) * D.CARB_EMI_QUITY * .001 AS GRID_TR_PW_CO2   
                , CASE   
                     WHEN COALESCE(A.COL6::numeric, 0) > 0 THEN '0'
                     WHEN COALESCE(A.COL6::numeric, 0) = 0 THEN '2'
                     ELSE '1'  
                  END GRID_STUS_CD   
                , A.COL9::numeric AS PCS_PW   
                , A.COL18::numeric AS PCS_FD_PW_H   
                , A.COL19::numeric AS PCS_PCH_PW_H  
                , A.COL0 AS EMS_OPMODE    
                , A.COL1 AS PCS_OPMODE1   
                , A.COL2 AS PCS_OPMODE2   
                , A.COL3 AS PCS_TGT_PW   
                , A.COL331 AS FEED_IN_LIMIT   
                , A.COL332 AS MAX_INVERTER_PW_CD 
            -- [ 2015.05.13 yngwie OUTLET_PW 추가
               , FN_GET_CONV_NA(A.COL5)::numeric AS OUTLET_PW   
               , FN_GET_CONV_NA(A.COL22)::numeric AS OUTLET_PW_H   
               , TRUNC(ABS(FN_GET_CONV_NA(A.COL22)::numeric) * E.UNIT_PRICE * .001, 2) AS OUTLET_PW_PRICE   
               , ABS(FN_GET_CONV_NA(A.COL22)::numeric) * D.CARB_EMI_QUITY * .001 AS OUTLET_PW_CO2   
            -- ]
               , SYS_EXTRACT_UTC(NOW()) AS CREATE_DT    
            -- [ 2015.07.06 yngwie 수집 컬럼 추가
				, FN_GET_CONV_NA(A.COL34)::numeric AS PCS_COMM_ERR_RATE 
				, FN_GET_CONV_NA(A.COL35)::numeric AS SMTR_COMM_ERR_RATE 
				, FN_GET_CONV_NA(A.COL36)::numeric AS M2M_COMM_ERR_RATE          
				, A.COL324 AS SMTR_TP_CD
				, FN_GET_CONV_NA(A.COL325)::numeric AS SMTR_PULSE_CNT 
				, A.COL326 AS SMTR_MODL_CD
				, FN_GET_CONV_NA(A.COL328)::numeric AS PV_MAX_PWR1 
				, FN_GET_CONV_NA(A.COL329)::numeric AS PV_MAX_PWR2 
				, A.COL330 AS INSTL_RGN
				, FN_GET_CONV_NA(A.COL334)::numeric AS MMTR_SLV_ADDR 
				, A.COL335 AS BASICMODE_CD 
	            , P.COL0                                                       AS PCS_FLAG0 
	            , P.COL1                                                       AS PCS_FLAG1 
	            , P.COL2                                                       AS PCS_FLAG2 
	            -- ]
	            -- [ 2015.07.13 modified by stjoo : P.COL20s value was NA on test ems.
              , FN_GET_CONV_NA(P.COL20)                                      AS PCS_OPMODE_CMD1 
              , P.COL21                                                      AS PCS_OPMODE_CMD2 
              , FN_GET_CONV_NA(P.COL38)::numeric   AS PV_V1 
              , FN_GET_CONV_NA(P.COL39)::numeric   AS PV_I1 
              , CASE WHEN FN_GET_CONV_NA(P.COL40)::numeric >= 100000 OR FN_GET_CONV_NA(P.COL40)::numeric <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL40)::numeric END AS PV_PW1  
              , FN_GET_CONV_NA(P.COL42)::numeric   AS PV_V2 
              , FN_GET_CONV_NA(P.COL43)::numeric   AS PV_I2 
              , CASE WHEN FN_GET_CONV_NA(P.COL44)::numeric >= 100000 OR FN_GET_CONV_NA(P.COL44)::numeric <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL44)::numeric END AS PV_PW2  
              , CASE WHEN FN_GET_CONV_NA(P.COL53)::numeric >= 100000 OR FN_GET_CONV_NA(P.COL53)::numeric <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL53)::numeric END AS INVERTER_V 
              , CASE WHEN FN_GET_CONV_NA(P.COL54)::numeric >= 10000 OR FN_GET_CONV_NA(P.COL54)::numeric <= -10000 THEN 0 ELSE FN_GET_CONV_NA(P.COL54)::numeric END AS INVERTER_I 
              , FN_GET_CONV_NA(P.COL55)::numeric   AS INVERTER_PW 
              , CASE WHEN FN_GET_CONV_NA(P.COL59)::numeric >= 100000 OR FN_GET_CONV_NA(P.COL59)::numeric <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL59)::numeric END AS DC_LINK_V 
              , FN_GET_CONV_NA(P.COL61)::numeric   AS G_RLY_CNT 
              , FN_GET_CONV_NA(P.COL62)::numeric   AS BAT_RLY_CNT 
              , FN_GET_CONV_NA(P.COL63)::numeric   AS GRID_TO_GRID_RLY_CNT 
              , FN_GET_CONV_NA(P.COL64)::numeric   AS BAT_PCHRG_RLY_CNT   
              , M.COL0                               AS BMS_FLAG0 
              , M.COL1                               AS BMS_DIAG0 
              , FN_GET_CONV_NA(M.COL16)::numeric   AS RACK_V 
              , FN_GET_CONV_NA(M.COL17)::numeric   AS RACK_I 
              , FN_GET_CONV_NA(M.COL18)::numeric   AS CELL_MAX_V 
              , FN_GET_CONV_NA(M.COL19)::numeric   AS CELL_MIN_V 
              , FN_GET_CONV_NA(M.COL20)::numeric   AS CELL_AVG_V 
              , FN_GET_CONV_NA(M.COL21)::numeric   AS CELL_MAX_T 
              , FN_GET_CONV_NA(M.COL22)::numeric   AS CELL_MIN_T 
              , FN_GET_CONV_NA(M.COL23)::numeric   AS CELL_AVG_T 
              , FN_GET_CONV_NA(M.COL24)::numeric   AS TRAY_CNT 
              , CASE WHEN SUBSTR(A.COL88, 4, 1) = '0' AND SUBSTR(A.COL88, 8, 1) = '1' THEN '1' ELSE '0' END AS SYS_ST_CD
              , SUBSTR(A.COL89, 7, 1) AS BT_CHRG_ST_CD 
              , SUBSTR(A.COL89, 6, 1) AS BT_DCHRG_ST_CD 
              , SUBSTR(A.COL89, 8, 1) AS PV_ST_CD 
              , SUBSTR(A.COL89, 5, 1) AS GRID_ST_CD 
              , SUBSTR(A.COL89, 4, 1) AS SMTR_ST_CD 
              , A.COL127 AS SMTR_TR_CNTER 
              , A.COL128 AS SMTR_OB_CNTER 
              , A.COL337 AS DN_ENABLE 
              , A.COL338 AS DC_START 
              , A.COL339 AS DC_END 
              , A.COL340 AS NC_START 
              , A.COL341 AS NC_END 
            -- ]     
			--[신규모델로 모니터링 추가
              , CASE WHEN P.COL3  ='NA' THEN NULL ELSE P.COL3   END as GRID_CODE0 
              , CASE WHEN P.COL4  ='NA' THEN NULL ELSE P.COL4   END as GRID_CODE1 
              , CASE WHEN P.COL5  ='NA' THEN NULL ELSE P.COL5   END as GRID_CODE2 
              , CASE WHEN P.COL19 ='NA' THEN NULL ELSE P.COL19  END as PCON_BAT_TARGETPOWER 
              , CASE WHEN P.COL31 ='NA' THEN NULL ELSE P.COL31  END as GRID_CODE3 
              , CASE WHEN P.COL32 ='NA' THEN NULL ELSE P.COL32  END as BDC_MODULE_CODE0 
              , CASE WHEN P.COL33 ='NA' THEN NULL ELSE P.COL33  END as BDC_MODULE_CODE1 
              , CASE WHEN P.COL34 ='NA' THEN NULL ELSE P.COL34  END as BDC_MODULE_CODE2 
              , CASE WHEN P.COL35 ='NA' THEN NULL ELSE P.COL35  END as BDC_MODULE_CODE3 
              , CASE WHEN P.COL36 ='NA' THEN NULL ELSE P.COL36  END as FAULT5_REALTIME_YEAR 
              , CASE WHEN P.COL37 ='NA' THEN NULL ELSE P.COL37  END as FAULT5_REALTIME_MONTH 
              , CASE WHEN P.COL87 ='NA' THEN NULL ELSE P.COL87  END as FAULT5_DAY 
              , CASE WHEN P.COL88 ='NA' THEN NULL ELSE P.COL88  END as FAULT5_HOUR 
              , CASE WHEN P.COL89 ='NA' THEN NULL ELSE P.COL89  END as FAULT5_MINUTE 
              , CASE WHEN P.COL90 ='NA' THEN NULL ELSE P.COL90  END as FAULT5_SECOND 
              , CASE WHEN P.COL91 ='NA' THEN NULL ELSE P.COL91  END as FAULT5_DATAHIGH 
              , CASE WHEN P.COL162='NA' THEN NULL ELSE P.COL162 END as FAULT5_DATALOW 
              , CASE WHEN P.COL163='NA' THEN NULL ELSE P.COL163 END as FAULT6_REALTIME_YEAR 
              , CASE WHEN P.COL164='NA' THEN NULL ELSE P.COL164 END as FAULT6_REALTIME_MONTH 
              , CASE WHEN P.COL165='NA' THEN NULL ELSE P.COL165 END as FAULT6_REALTIME_DAY 
              , CASE WHEN P.COL166='NA' THEN NULL ELSE P.COL166 END as FAULT6_REALTIME_HOUR 
              , CASE WHEN P.COL167='NA' THEN NULL ELSE P.COL167 END as FAULT6_REALTIME_MINUTE 
              , CASE WHEN P.COL168='NA' THEN NULL ELSE P.COL168 END as FAULT6_REALTIME_SECOND 
              , CASE WHEN P.COL169='NA' THEN NULL ELSE P.COL169 END as FAULT6_DATAHIGH 
              , CASE WHEN P.COL170='NA' THEN NULL ELSE P.COL170 END as FAULT6_DATALOW 
              , CASE WHEN P.COL171='NA' THEN NULL ELSE P.COL171 END as FAULT7_REALTIME_YEAR 
              , CASE WHEN P.COL177='NA' THEN NULL ELSE P.COL177 END as FAULT7_REALTIME_MONTH 
              , CASE WHEN P.COL178='NA' THEN NULL ELSE P.COL178 END as FAULT7_REALTIME_DAY 
              , CASE WHEN P.COL179='NA' THEN NULL ELSE P.COL179 END as FAULT7_REALTIME_HOUR 
              , CASE WHEN P.COL187='NA' THEN NULL ELSE P.COL187 END as FAULT7_REALTIME_MINUTE 
              , CASE WHEN P.COL188='NA' THEN NULL ELSE P.COL188 END as FAULT7_REALTIME_SECOND 
              , CASE WHEN P.COL189='NA' THEN NULL ELSE P.COL189 END as FAULT7_DATAHIGH 
              , CASE WHEN P.COL191='NA' THEN NULL ELSE P.COL191 END as FAULT7_DATALOW 
              , CASE WHEN P.COL192='NA' THEN NULL ELSE P.COL192 END as FAULT8_REALTIME_YEAR 
              , CASE WHEN P.COL193='NA' THEN NULL ELSE P.COL193 END as FAULT8_REALTIME_MONTH 
              , CASE WHEN P.COL194='NA' THEN NULL ELSE P.COL194 END as FAULT8_REALTIME_DAY 
              , CASE WHEN P.COL195='NA' THEN NULL ELSE P.COL195 END as FAULT8_REALTIME_HOUR 
              , CASE WHEN P.COL196='NA' THEN NULL ELSE P.COL196 END as FAULT8_REALTIME_MINUTE 
              , CASE WHEN P.COL197='NA' THEN NULL ELSE P.COL197 END as FAULT8_REALTIME_SECOND 
              , CASE WHEN P.COL198='NA' THEN NULL ELSE P.COL198 END as FAULT8_DATAHIGH 
              , CASE WHEN P.COL199='NA' THEN NULL ELSE P.COL199 END as FAULT8_DATALOW 
              , CASE WHEN P.COL200='NA' THEN NULL ELSE P.COL200 END as FAULT9_REALTIME_YEAR 
              , CASE WHEN P.COL201='NA' THEN NULL ELSE P.COL201 END as FAULT9_REALTIME_MONTH 
              , CASE WHEN P.COL202='NA' THEN NULL ELSE P.COL202 END as FAULT9_REALTIME_DAY 
              , CASE WHEN P.COL203='NA' THEN NULL ELSE P.COL203 END as FAULT9_REALTIME_HOUR 
              , CASE WHEN P.COL204='NA' THEN NULL ELSE P.COL204 END as FAULT9_REALTIME_MINUTE 
              , CASE WHEN P.COL205='NA' THEN NULL ELSE P.COL205 END as FAULT9_REALTIME_SECOND 
              , CASE WHEN P.COL206='NA' THEN NULL ELSE P.COL206 END as FAULT9_DATAHIGH 
              , CASE WHEN P.COL207='NA' THEN NULL ELSE P.COL207 END as FAULT9_DATALOW 
            --]
            FROM   
                TB_RAW_EMS_INFO_TMP A   
                , TB_RAW_PCS_INFO_TMP P   
                , TB_RAW_BMS_INFO_TMP M   
                , TB_BAS_DEVICE B   
                , TB_PLF_CARB_EMI_QUITY D   
                , TB_BAS_ELPW_UNIT_PRICE E 
                , TB_BAS_ELPW_UNIT_PRICE ET 
            WHERE   
                A.DEVICE_ID = P.DEVICE_ID
                AND A.COLEC_DT = P.COLEC_DT   
                AND A.DEVICE_ID = M.DEVICE_ID   
                AND A.COLEC_DT = M.COLEC_DT   
                AND A.DEVICE_ID = B.DEVICE_ID   
                AND B.CNTRY_CD = D.CNTRY_CD   
                AND D.USE_FLAG = 'Y'
                AND B.ELPW_PROD_CD = E.ELPW_PROD_CD   
                AND E.IO_FLAG = 'O'
                AND E.USE_FLAG = 'Y'
            -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가
                AND E.WKDAY_FLAG = FN_GET_WKDAY_FLAG(SUBSTR(A.COLEC_DT, 1, 8)) 
            -- 2015.07.31 요금제 년월 변경
            --  AND SUBSTR(A.COLEC_DT, 1, 6) = E.UNIT_YM   
                AND E.UNIT_YM = '000000'
                AND SUBSTR(A.COLEC_DT, 9, 2) = E.UNIT_HH 
                AND B.ELPW_PROD_CD = ET.ELPW_PROD_CD  
                AND ET.IO_FLAG = 'T'   
                AND ET.USE_FLAG = 'Y'  
            -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가, T 는  1 만 있음
            -- 2015.08.06 modified by yngwie T 도 주말 0 추가
            --  AND ET.WKDAY_FLAG = '1'   
                AND ET.WKDAY_FLAG = FN_GET_WKDAY_FLAG(SUBSTR(A.COLEC_DT, 1, 8))
            -- 2015.07.31 요금제 년월 변경
            --  AND SUBSTR(A.COLEC_DT, 1, 6) = ET.UNIT_YM  
                AND ET.UNIT_YM = '000000'   
                AND SUBSTR(A.COLEC_DT, 9, 2) = ET.UNIT_HH 
            ;

	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	       -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Insert TB_OPR_ESS_STATE_TMP Table :  ', v_work_num));
	      	RAISE notice 'v_log_fn[%] Insert TB_OPR_ESS_STATE_TMP Table :  [%]건', v_log_fn, v_work_num;
            

	      	v_ExeCnt1 := 0;
			v_ExeCnt2 := 0;
			v_ExeCnt3 := 0;

            FOR v_row IN ( 
		      
	            -- 2015.01.07 stjoo,  아래 코드만 오류로 처리 E601 ~ 603, E901 ~ 907, C128
	            -- 2015.07.14 yngwie, 호주 장비인 경우 E706, E708 장애로 처리
	            -- 2015.09.03 yngwie, 호주 장비인 경우 E706, E708 장애로 처리 => 모델별 에러코드 정의에 등록되어 있으므로 구분할 필요 없음 
	            WITH ERR_INFO AS (  
	                SELECT  
	                    DEVICE_ID   
	                    , DEVICE_TYPE_CD   
	                    , CASE    
	                        WHEN MAX(ALARM_TYPE_CD::INTEGER) = 1 AND ALARM_CD = 'C128' THEN '3'
	                        WHEN MAX(ALARM_TYPE_CD::INTEGER) = 3 THEN '2' 
	--                      WHEN MAX(ALARM_TYPE_CD::INTEGER) IN (1, 2) AND ALARM_CD <> 'C128' THEN '1'
	                        WHEN MAX(ALARM_TYPE_CD::INTEGER) = 2 THEN '1'
	                        ELSE '0'
	                      END AS OPER_STUS_CD
	                    , MAX(EVENT_DT) AS EVENT_DT
	                FROM TB_OPR_ALARM 
	            --E202오류의 경우 사용자에게 보여주지 않도록 한다.
	                WHERE ALARM_CD NOT IN ('E202')  
	                GROUP BY   DEVICE_ID, DEVICE_TYPE_CD, ALARM_CD
	             ) 
	            SELECT 
	                DEVICE_ID 
	                , DEVICE_TYPE_CD 
	                , MAX(OPER_STUS_CD) AS OPER_STUS_CD 
	                , MAX(EVENT_DT) AS EVENT_DT 
	            FROM        ERR_INFO 
	            GROUP BY  DEVICE_ID, DEVICE_TYPE_CD
	            
			) LOOP
	      		RAISE NOTICE 'v_log_fn[%] v_log_seq[%] 알람 OPER_STUS_CD 변경 DEVICE_ID[%] OPER_STUS_CD[%] DEVICE_TYPE_CD[%] COLEC_DT[%]', 
	      							v_log_fn, v_log_seq, v_row.DEVICE_ID, v_row.OPER_STUS_CD,  v_row.DEVICE_TYPE_CD,  v_row.EVENT_DT;
            
	            -- 통신 단절인 장비의 경우 1분 데이터가 수집되지 않으므로 상태를 현황 임시 테이블에도 Update 한다.
	            UPDATE TB_OPR_ESS_STATE_TMP
	            SET OPER_STUS_CD = v_row.OPER_STUS_CD 
	            WHERE DEVICE_ID = v_row.DEVICE_ID
	            AND DEVICE_TYPE_CD =v_row.DEVICE_TYPE_CD;
	           
       	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        	v_rslt_cnt := v_rslt_cnt + v_work_num;	
				v_ExeCnt1 := v_ExeCnt1 + v_work_num;
			
	            -- 통신 단절인 장비는 현황 임시 테이블에 데이터가 수집되지 않을 수 있으니 현황 테이블에도 Update 한다.
	            UPDATE TB_OPR_ESS_STATE 
	            SET OPER_STUS_CD = v_row.OPER_STUS_CD
	            WHERE DEVICE_ID = v_row.DEVICE_ID
	            AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;

       	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
				v_ExeCnt2 := v_ExeCnt2 + v_work_num;

	        
	            UPDATE TB_OPR_ESS_STATE_HIST
	            SET OPER_STUS_CD = v_row.OPER_STUS_CD
	            WHERE DEVICE_ID = v_row.DEVICE_ID
	            AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD
	            AND COLEC_DT = v_row.EVENT_DT;

       	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
				v_ExeCnt3 := v_ExeCnt3 + v_work_num;
	        
			END LOOP;

           -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Update TB_OPR_ESS_STATE_TMP Table For OPER_STUS_CD :  ', v_ExeCnt1));
           -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Update TB_OPR_ESS_STATE Table For OPER_STUS_CD :  ', v_ExeCnt2));
           -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Update TB_OPR_ESS_STATE_HIST Table For OPER_STUS_CD :  ', v_ExeCnt3));
	      	RAISE NOTICE 'v_log_fn[%] Update TB_OPR_ESS_STATE_TMP Table For OPER_STUS_CD : [%]', v_log_fn, v_ExeCnt1;
	      	RAISE NOTICE 'v_log_fn[%] Update TB_OPR_ESS_STATE Table For OPER_STUS_CD : [%]', v_log_fn, v_ExeCnt2;
	      	RAISE NOTICE 'v_log_fn[%] Update TB_OPR_ESS_STATE_HIST Table For OPER_STUS_CD : [%]', v_log_fn, v_ExeCnt3;
   
       
            -- #411 Sleep 모드의 상태의 장비를 절전 상태로 상태 변경을 한다. 20160105 jkm1020
	      	v_ExeCnt1 := 0;
			v_ExeCnt2 := 0;
			v_ExeCnt3 := 0;

            FOR v_row IN ( 
	            SELECT  DEVICE_ID  
	                 	, DEVICE_TYPE_CD   
	                 	, '5' AS OPER_STUS_CD  
	                 	, COLEC_DT
	            FROM   TB_OPR_ESS_STATE_TMP 
	            WHERE  EMS_OPMODE = '9'
	            
 			) LOOP
	      		RAISE NOTICE 'v_log_fn[%] v_log_seq[%]  Sleep상태의 장비를 절전상태로 변경 DEVICE_ID[%] OPER_STUS_CD[%] DEVICE_TYPE_CD[%] COLEC_DT[%]', 
	      							v_log_fn, v_log_seq, v_row.DEVICE_ID, v_row.OPER_STUS_CD,  v_row.DEVICE_TYPE_CD,  v_row.COLEC_DT;

	      						
	            -- 통신 단절인 장비의 경우 1분 데이터가 수집되지 않으므로 상태를 현황 임시 테이블에도 Update 한다.
	            UPDATE 	TB_OPR_ESS_STATE_TMP 
	            SET 		OPER_STUS_CD =v_row.OPER_STUS_CD
	            WHERE 	DEVICE_ID = v_row.DEVICE_ID
	            AND 		DEVICE_TYPE_CD =v_row.DEVICE_TYPE_CD;
	           
       	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        	v_rslt_cnt := v_rslt_cnt + v_work_num;	
				v_ExeCnt1 := v_ExeCnt1 + v_work_num;
			
	            
	            -- 통신 단절인 장비는 현황 임시 테이블에 데이터가 수집되지 않을 수 있으니 현황 테이블에도 Update 한다.
	            UPDATE 	TB_OPR_ESS_STATE 
	            SET 		OPER_STUS_CD = v_row.OPER_STUS_CD
	            WHERE 	DEVICE_ID = v_row.DEVICE_ID
	            AND 		DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;

       	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        	v_rslt_cnt := v_rslt_cnt + v_work_num;	
				v_ExeCnt2 := v_ExeCnt2 + v_work_num;

			
	            UPDATE TB_OPR_ESS_STATE_HIST 
	            SET 		OPER_STUS_CD = v_row.OPER_STUS_CD
	            WHERE 	DEVICE_ID =v_row.DEVICE_ID
	            AND 		DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD
	            AND 		COLEC_DT = v_row.COLEC_DT;

       	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        	v_rslt_cnt := v_rslt_cnt + v_work_num;	
				v_ExeCnt3 := v_ExeCnt3 + v_work_num;

            END LOOP;

           -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Update TB_OPR_ESS_STATE_TMP Table For OPER_STUS_CD Sleep Mode :  ', v_ExeCnt1));
           -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Update TB_OPR_ESS_STATE Table For OPER_STUS_CD Sleep Mode  :  ', v_ExeCnt2));
           -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Update TB_OPR_ESS_STATE_HIST Table For OPER_STUS_CD Sleep Mode :  ', v_ExeCnt3));
	      	RAISE notice 'v_log_fn[%] Update TB_OPR_ESS_STATE_TMP Table For OPER_STUS_CD Sleep Mode : [%]', v_log_fn, v_ExeCnt1;
	      	RAISE notice 'v_log_fn[%] Update TB_OPR_ESS_STATE Table For OPER_STUS_CD Sleep Mode : [%]', v_log_fn, v_ExeCnt2;
	      	RAISE notice 'v_log_fn[%] Update TB_OPR_ESS_STATE_HIST Table For OPER_STUS_CD Sleep Mode : [%]', v_log_fn, v_ExeCnt3;

            
            --- 	2015.07.07 yngwie 장비별 상태 타임라인 데이터 처리
            WITH B  AS ( 
                WITH PRIORITY AS ( 
                    SELECT 1 AS PRIORITY, '2' AS OPER_STUS_CD FROM DUAL UNION ALL 
                    SELECT 2 AS PRIORITY, '1' AS OPER_STUS_CD FROM DUAL UNION ALL 
                    SELECT 3 AS PRIORITY, '3' AS OPER_STUS_CD FROM DUAL 
                ), UALL AS ( 
                    SELECT 
                        TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'YYYYMMDD') AS COLEC_DD 
                        , A.DEVICE_ID 
                        , A.DEVICE_TYPE_CD 
                        , A.OPER_STUS_CD 
                        , B.PRIORITY 
                    FROM 
                        TB_OPR_ESS_STATE A 
                        , PRIORITY B 
                    WHERE 
                        A.OPER_STUS_CD = B.OPER_STUS_CD 
                    UNION ALL 
                    SELECT 
                        A.COLEC_DD 
                        , A.DEVICE_ID 
                        , A.DEVICE_TYPE_CD 
                        , A.OPER_STUS_CD 
                        , B.PRIORITY 
                    FROM 
                        TB_OPR_ESS_STATE_TL A 
                        , PRIORITY B 
                    WHERE 
                        A.OPER_STUS_CD = B.OPER_STUS_CD 
                        AND A.COLEC_DD = TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'YYYYMMDD') 
                ) 
                SELECT    * 
                FROM ( 
                    SELECT 
                        COLEC_DD 
                        , DEVICE_ID 
                        , DEVICE_TYPE_CD 
                        , OPER_STUS_CD 
                        , PRIORITY 
                        , ROW_NUMBER() OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY PRIORITY) AS RK 
                    FROM 
                        UALL 
                ) T
                WHERE RK = 1 
            ), U as (
                UPDATE TB_OPR_ESS_STATE_TL A
                SET        A.OPER_STUS_CD = B.OPER_STUS_CD 
                FROM    B
				WHERE   A.DEVICE_ID = B.DEVICE_ID 
                AND 		A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD 
                AND 		A.COLEC_DD = B.COLEC_DD 
				
                RETURNING U.*
            )
            INSERT INTO TB_OPR_ESS_STATE_TL (  
                 DEVICE_ID 
                , DEVICE_TYPE_CD 
                , COLEC_DD 
                , OPER_STUS_CD 
            ) 
            SELECT 
                B.DEVICE_ID 
                , B.DEVICE_TYPE_CD 
                , B.COLEC_DD 
                , B.OPER_STUS_CD 
           	FROM B
			WHERE NOT EXISTS (  
				SELECT 	1 
			    FROM 	U 
				WHERE	COLEC_DD 			= B.COLEC_DD  
					AND 	DEVICE_ID			= B.DEVICE_ID  
					AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
			  )
            ;            
           
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	       -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Merge TB_OPR_ESS_STATE_TL :  : ', v_work_num));
	      	RAISE notice 'v_log_fn[%] Merge TB_OPR_ESS_STATE_TL :  [%]건', v_log_fn, v_work_num;


            -- TEMP 테이블의 레코드 중 가장 최근 데이터를 상태정보 테이블에 Merge
            WITH B AS (
				SELECT	* 
				FROM (   
				        SELECT	ROW_NUMBER() OVER (PARTITION BY A.DEVICE_ID ORDER BY A.COLEC_DT DESC ) SEQ   
				            		, A.*   
				        FROM   TB_OPR_ESS_STATE_TMP A   
				    ) A 
				    WHERE A.SEQ = 1 
            ), U as ( 
	            UPDATE TB_OPR_ESS_STATE A
	            SET
		              A.COLEC_DT         = B.COLEC_DT           
		            , A.OPER_STUS_CD     = B.OPER_STUS_CD       
		            , A.PV_PW            = B.PV_PW              
		            , A.PV_PW_H          = B.PV_PW_H            
		            , A.PV_PW_PRICE      = B.PV_PW_PRICE        
		            , A.PV_PW_CO2        = B.PV_PW_CO2          
		            , A.PV_STUS_CD       = B.PV_STUS_CD         
		            , A.CONS_PW          = B.CONS_PW            
		            , A.CONS_PW_H        = B.CONS_PW_H          
		            , A.CONS_PW_PRICE    = B.CONS_PW_PRICE      
		            , A.CONS_PW_CO2      = B.CONS_PW_CO2        
		            , A.BT_PW            = B.BT_PW              
		            , A.BT_CHRG_PW_H     = B.BT_CHRG_PW_H       
		            , A.BT_CHRG_PW_PRICE = B.BT_CHRG_PW_PRICE   
		            , A.BT_CHRG_PW_CO2   = B.BT_CHRG_PW_CO2     
		            , A.BT_DCHRG_PW_H    = B.BT_DCHRG_PW_H      
		            , A.BT_DCHRG_PW_PRICE= B.BT_DCHRG_PW_PRICE  
		            , A.BT_DCHRG_PW_CO2  = B.BT_DCHRG_PW_CO2    
		            , A.BT_SOC           = B.BT_SOC             
		            , A.BT_SOH           = B.BT_SOH             
		            , A.BT_STUS_CD       = B.BT_STUS_CD         
		            , A.GRID_PW          = B.GRID_PW            
		            , A.GRID_OB_PW_H     = B.GRID_OB_PW_H          
		            , A.GRID_OB_PW_PRICE = B.GRID_OB_PW_PRICE      
		            , A.GRID_OB_PW_CO2   = B.GRID_OB_PW_CO2        
		            , A.GRID_TR_PW_H     = B.GRID_TR_PW_H       
		            , A.GRID_TR_PW_PRICE = B.GRID_TR_PW_PRICE   
		            , A.GRID_TR_PW_CO2   = B.GRID_TR_PW_CO2     
		            , A.GRID_STUS_CD     = B.GRID_STUS_CD       
		            , A.PCS_PW           = B.PCS_PW             
		            , A.PCS_FD_PW_H      = B.PCS_FD_PW_H        
		            , A.PCS_PCH_PW_H     = B.PCS_PCH_PW_H       
		            , A.CREATE_DT        = COALESCE(B.CREATE_DT, SYS_EXTRACT_UTC(NOW()))           
		            , A.EMS_OPMODE       = B.EMS_OPMODE       
		            , A.PCS_OPMODE1      = B.PCS_OPMODE1       
		            , A.PCS_OPMODE2      = B.PCS_OPMODE2       
		            , A.PCS_TGT_PW       = B.PCS_TGT_PW       
		            , A.FEED_IN_LIMIT    = B.FEED_IN_LIMIT       
		            , A.MAX_INVERTER_PW_CD    = B.MAX_INVERTER_PW_CD       
		            --, A.RFSH_PRD = ROUND((TO_DATE(B.COLEC_DT, 'YYYYMMDDHH24MISS') - TO_DATE(A.COLEC_DT, 'YYYYMMDDHH24MISS')) * 60 * 60 * 24) 
		            , A.RFSH_PRD = 60 
		            -- [ 2015.05.13 yngwie OUTLET_PW 추가
		            , A.OUTLET_PW        = B.OUTLET_PW       
		            , A.OUTLET_PW_H      = B.OUTLET_PW_H       
		            , A.OUTLET_PW_PRICE  = B.OUTLET_PW_PRICE       
		            , A.OUTLET_PW_CO2    = B.OUTLET_PW_CO2       
		            , A.PWR_RANGE_CD_ESS_GRID    = FN_GET_PWR_RANGE_CD('ESS_GRID',  B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , A.PWR_RANGE_CD_GRID_ESS    = FN_GET_PWR_RANGE_CD('GRID_ESS',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , A.PWR_RANGE_CD_GRID_LOAD   = FN_GET_PWR_RANGE_CD('GRID_LOAD', B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , A.PWR_RANGE_CD_ESS_LOAD    = FN_GET_PWR_RANGE_CD('ESS_LOAD',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , A.PWR_RANGE_CD_PV_ESS      = FN_GET_PWR_RANGE_CD('PV_ESS',    B.PV_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            -- ]
		            -- [ 2015.07.06 yngwie 수집 컬럼 추가
		            , A.PCS_COMM_ERR_RATE   = B.PCS_COMM_ERR_RATE 
		            , A.SMTR_COMM_ERR_RATE  = B.SMTR_COMM_ERR_RATE 
		            , A.M2M_COMM_ERR_RATE   = B.M2M_COMM_ERR_RATE       
		            , A.SMTR_TP_CD          = B.SMTR_TP_CD 
		            , A.SMTR_PULSE_CNT      = B.SMTR_PULSE_CNT 
		            , A.SMTR_MODL_CD        = B.SMTR_MODL_CD 
		            , A.PV_MAX_PWR1         = B.PV_MAX_PWR1 
		            , A.PV_MAX_PWR2         = B.PV_MAX_PWR2 
		            , A.INSTL_RGN           = B.INSTL_RGN 
		            , A.MMTR_SLV_ADDR       = B.MMTR_SLV_ADDR 
		            , A.BASICMODE_CD        = B.BASICMODE_CD 
		            , A.PCS_FLAG0       = B.PCS_FLAG0  
		            , A.PCS_FLAG1       = B.PCS_FLAG1  
		            , A.PCS_FLAG2       = B.PCS_FLAG2  
		            , A.PCS_OPMODE_CMD1         = B.PCS_OPMODE_CMD1  
		            , A.PCS_OPMODE_CMD2         = B.PCS_OPMODE_CMD2  
		            , A.PV_V1       = B.PV_V1  
		            , A.PV_I1       = B.PV_I1  
		            , A.PV_PW1      = B.PV_PW1  
		            , A.PV_V2       = B.PV_V2  
		            , A.PV_I2       = B.PV_I2  
		            , A.PV_PW2      = B.PV_PW2  
		            , A.INVERTER_V      = B.INVERTER_V  
		            , A.INVERTER_I      = B.INVERTER_I  
		            , A.INVERTER_PW         = B.INVERTER_PW  
		            , A.DC_LINK_V       = B.DC_LINK_V  
		            , A.G_RLY_CNT       = B.G_RLY_CNT  
		            , A.BAT_RLY_CNT         = B.BAT_RLY_CNT  
		            , A.GRID_TO_GRID_RLY_CNT        = B.GRID_TO_GRID_RLY_CNT  
		            , A.BAT_PCHRG_RLY_CNT           = B.BAT_PCHRG_RLY_CNT    
		            , A.BMS_FLAG0       = B.BMS_FLAG0  
		            , A.BMS_DIAG0       = B.BMS_DIAG0  
		            , A.RACK_V      = B.RACK_V  
		            , A.RACK_I      = B.RACK_I  
		            , A.CELL_MAX_V      = B.CELL_MAX_V  
		            , A.CELL_MIN_V      = B.CELL_MIN_V  
		            , A.CELL_AVG_V      = B.CELL_AVG_V  
		            , A.CELL_MAX_T      = B.CELL_MAX_T  
		            , A.CELL_MIN_T      = B.CELL_MIN_T  
		            , A.CELL_AVG_T      = B.CELL_AVG_T  
		            , A.TRAY_CNT        = B.TRAY_CNT 
		            , A.SYS_ST_CD       = B.SYS_ST_CD 
		            , A.BT_CHRG_ST_CD   = B.BT_CHRG_ST_CD 
		            , A.BT_DCHRG_ST_CD  = B.BT_DCHRG_ST_CD 
		            , A.PV_ST_CD        = B.PV_ST_CD 
		            , A.GRID_ST_CD      = B.GRID_ST_CD 
		            , A.SMTR_ST_CD      = B.SMTR_ST_CD 
		            , A.SMTR_TR_CNTER   = B.SMTR_TR_CNTER 
		            , A.SMTR_OB_CNTER   = B.SMTR_OB_CNTER 
		            , A.DN_ENABLE       = B.DN_ENABLE         
		            , A.DC_START        = B.DC_START         
		            , A.DC_END          = B.DC_END         
		            , A.NC_START        = B.NC_START         
		            , A.NC_END          = B.NC_END                             
		            -- ]
		            --[신규모델로 모니터링 추가
		            , A.GRID_CODE0 =             B.GRID_CODE0 
		            , A.GRID_CODE1 =             B.GRID_CODE1 
		            , A.GRID_CODE2 =             B.GRID_CODE2 
		            , A.PCON_BAT_TARGETPOWER =   B.PCON_BAT_TARGETPOWER 
		            , A.GRID_CODE3 =             B.GRID_CODE3 
		            , A.BDC_MODULE_CODE0 =       B.BDC_MODULE_CODE0 
		            , A.BDC_MODULE_CODE1 =       B.BDC_MODULE_CODE1 
		            , A.BDC_MODULE_CODE2 =       B.BDC_MODULE_CODE2 
		            , A.BDC_MODULE_CODE3 =       B.BDC_MODULE_CODE3 
		            , A.FAULT5_REALTIME_YEAR =   B.FAULT5_REALTIME_YEAR 
		            , A.FAULT5_REALTIME_MONTH =  B.FAULT5_REALTIME_MONTH 
		            , A.FAULT5_DAY =             B.FAULT5_DAY 
		            , A.FAULT5_HOUR =            B.FAULT5_HOUR 
		            , A.FAULT5_MINUTE =          B.FAULT5_MINUTE 
		            , A.FAULT5_SECOND =          B.FAULT5_SECOND 
		            , A.FAULT5_DATAHIGH =        B.FAULT5_DATAHIGH 
		            , A.FAULT5_DATALOW =         B.FAULT5_DATALOW 
		            , A.FAULT6_REALTIME_YEAR =   B.FAULT6_REALTIME_YEAR 
		            , A.FAULT6_REALTIME_MONTH =  B.FAULT6_REALTIME_MONTH 
		            , A.FAULT6_REALTIME_DAY =    B.FAULT6_REALTIME_DAY 
		            , A.FAULT6_REALTIME_HOUR =   B.FAULT6_REALTIME_HOUR 
		            , A.FAULT6_REALTIME_MINUTE = B.FAULT6_REALTIME_MINUTE 
		            , A.FAULT6_REALTIME_SECOND = B.FAULT6_REALTIME_SECOND 
		            , A.FAULT6_DATAHIGH =        B.FAULT6_DATAHIGH 
		            , A.FAULT6_DATALOW =         B.FAULT6_DATALOW 
		            , A.FAULT7_REALTIME_YEAR =   B.FAULT7_REALTIME_YEAR 
		            , A.FAULT7_REALTIME_MONTH =  B.FAULT7_REALTIME_MONTH 
		            , A.FAULT7_REALTIME_DAY =    B.FAULT7_REALTIME_DAY 
		            , A.FAULT7_REALTIME_HOUR =   B.FAULT7_REALTIME_HOUR 
		            , A.FAULT7_REALTIME_MINUTE = B.FAULT7_REALTIME_MINUTE 
		            , A.FAULT7_REALTIME_SECOND = B.FAULT7_REALTIME_SECOND 
		            , A.FAULT7_DATAHIGH =        B.FAULT7_DATAHIGH 
		            , A.FAULT7_DATALOW =         B.FAULT7_DATALOW 
		            , A.FAULT8_REALTIME_YEAR =   B.FAULT8_REALTIME_YEAR 
		            , A.FAULT8_REALTIME_MONTH =  B.FAULT8_REALTIME_MONTH 
		            , A.FAULT8_REALTIME_DAY =    B.FAULT8_REALTIME_DAY 
		            , A.FAULT8_REALTIME_HOUR =   B.FAULT8_REALTIME_HOUR 
		            , A.FAULT8_REALTIME_MINUTE = B.FAULT8_REALTIME_MINUTE 
		            , A.FAULT8_REALTIME_SECOND = B.FAULT8_REALTIME_SECOND 
		            , A.FAULT8_DATAHIGH =        B.FAULT8_DATAHIGH 
		            , A.FAULT8_DATALOW =         B.FAULT8_DATALOW 
		            , A.FAULT9_REALTIME_YEAR =   B.FAULT9_REALTIME_YEAR 
		            , A.FAULT9_REALTIME_MONTH =  B.FAULT9_REALTIME_MONTH 
		            , A.FAULT9_REALTIME_DAY =    B.FAULT9_REALTIME_DAY 
		            , A.FAULT9_REALTIME_HOUR =   B.FAULT9_REALTIME_HOUR 
		            , A.FAULT9_REALTIME_MINUTE = B.FAULT9_REALTIME_MINUTE 
		            , A.FAULT9_REALTIME_SECOND = B.FAULT9_REALTIME_SECOND 
		            , A.FAULT9_DATAHIGH =        B.FAULT9_DATAHIGH 
		            , A.FAULT9_DATALOW =         B.FAULT9_DATALOW 
		            --]        
	            WHERE 	A.DEVICE_ID = B.DEVICE_ID             
	            AND 		A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD   
	            
	            RETURNING A.*
	        )                           
            INSERT INTO TB_OPR_ESS_STATE ( 
	               DEVICE_ID                               
	              , DEVICE_TYPE_CD                          
	              , COLEC_DT                                
	              , OPER_STUS_CD                            
	              , PV_PW                                   
	              , PV_PW_H                                 
	              , PV_PW_PRICE                             
	              , PV_PW_CO2                               
	              , PV_STUS_CD                              
	              , CONS_PW                                 
	              , CONS_PW_H                               
	              , CONS_PW_PRICE                           
	              , CONS_PW_CO2                             
	              , BT_PW                                   
	              , BT_CHRG_PW_H                            
	              , BT_CHRG_PW_PRICE                        
	              , BT_CHRG_PW_CO2                          
	              , BT_DCHRG_PW_H                           
	              , BT_DCHRG_PW_PRICE                       
	              , BT_DCHRG_PW_CO2                         
	              , BT_SOC                                  
	              , BT_SOH                                  
	              , BT_STUS_CD                              
	              , GRID_PW                                 
	              , GRID_OB_PW_H                               
	              , GRID_OB_PW_PRICE                           
	              , GRID_OB_PW_CO2                             
	              , GRID_TR_PW_H                            
	              , GRID_TR_PW_PRICE                        
	              , GRID_TR_PW_CO2                          
	              , GRID_STUS_CD                            
	              , PCS_PW                                 
	              , PCS_FD_PW_H                                 
	              , PCS_PCH_PW_H                                 
	              , CREATE_DT                               
	              , EMS_OPMODE                               
	              , PCS_OPMODE1                               
	              , PCS_OPMODE2                               
	              , PCS_TGT_PW                               
	              , FEED_IN_LIMIT                               
	              , MAX_INVERTER_PW_CD                               
	            -- [ 2015.05.13 yngwie OUTLET_PW 추가
	              , OUTLET_PW                               
	              , OUTLET_PW_H                               
	              , OUTLET_PW_PRICE                               
	              , OUTLET_PW_CO2                               
	              , PWR_RANGE_CD_ESS_GRID 
	              , PWR_RANGE_CD_GRID_ESS 
	              , PWR_RANGE_CD_GRID_LOAD 
	              , PWR_RANGE_CD_ESS_LOAD 
	              , PWR_RANGE_CD_PV_ESS             
	            -- ]
	            -- [ 2015.07.06 yngwie 수집 컬럼 추가
	              , PCS_COMM_ERR_RATE  
	              , SMTR_COMM_ERR_RATE 
	              , M2M_COMM_ERR_RATE  
	              , SMTR_TP_CD          
	              , SMTR_PULSE_CNT      
	              , SMTR_MODL_CD    
	              , PV_MAX_PWR1     
	              , PV_MAX_PWR2     
	              , INSTL_RGN       
	              , MMTR_SLV_ADDR   
	              , BASICMODE_CD    
	              , PCS_FLAG0  
	              , PCS_FLAG1  
	              , PCS_FLAG2  
	              , PCS_OPMODE_CMD1  
	              , PCS_OPMODE_CMD2  
	              , PV_V1  
	              , PV_I1  
	              , PV_PW1  
	              , PV_V2  
	              , PV_I2  
	              , PV_PW2  
	              , INVERTER_V  
	              , INVERTER_I  
	              , INVERTER_PW  
	              , DC_LINK_V  
	              , G_RLY_CNT  
	              , BAT_RLY_CNT  
	              , GRID_TO_GRID_RLY_CNT  
	              , BAT_PCHRG_RLY_CNT   
	              , BMS_FLAG0  
	              , BMS_DIAG0  
	              , RACK_V  
	              , RACK_I  
	              , CELL_MAX_V  
	              , CELL_MIN_V  
	              , CELL_AVG_V  
	              , CELL_MAX_T  
	              , CELL_MIN_T  
	              , CELL_AVG_T  
	              , TRAY_CNT 
	              , SYS_ST_CD 
	              , BT_CHRG_ST_CD 
	              , BT_DCHRG_ST_CD 
	              , PV_ST_CD 
	              , GRID_ST_CD 
	              , SMTR_ST_CD 
	              , SMTR_TR_CNTER 
	              , SMTR_OB_CNTER 
	              , DN_ENABLE     
	              , DC_START     
	              , DC_END     
	              , NC_START     
	              , NC_END                                
	            -- ]            
	         --[신규모델로 모니터링 추가
	              , GRID_CODE0 
	              , GRID_CODE1 
	              , GRID_CODE2 
	              , PCON_BAT_TARGETPOWER 
	              , GRID_CODE3 
	              , BDC_MODULE_CODE0 
	              , BDC_MODULE_CODE1 
	              , BDC_MODULE_CODE2 
	              , BDC_MODULE_CODE3 
	              , FAULT5_REALTIME_YEAR 
	              , FAULT5_REALTIME_MONTH 
	              , FAULT5_DAY 
	              , FAULT5_HOUR 
	              , FAULT5_MINUTE 
	              , FAULT5_SECOND 
	              , FAULT5_DATAHIGH 
	              , FAULT5_DATALOW 
	              , FAULT6_REALTIME_YEAR 
	              , FAULT6_REALTIME_MONTH 
	              , FAULT6_REALTIME_DAY 
	              , FAULT6_REALTIME_HOUR 
	              , FAULT6_REALTIME_MINUTE 
	              , FAULT6_REALTIME_SECOND 
	              , FAULT6_DATAHIGH 
	              , FAULT6_DATALOW 
	              , FAULT7_REALTIME_YEAR 
	              , FAULT7_REALTIME_MONTH 
	              , FAULT7_REALTIME_DAY 
	              , FAULT7_REALTIME_HOUR 
	              , FAULT7_REALTIME_MINUTE 
	              , FAULT7_REALTIME_SECOND 
	              , FAULT7_DATAHIGH 
	              , FAULT7_DATALOW 
	              , FAULT8_REALTIME_YEAR 
	              , FAULT8_REALTIME_MONTH 
	              , FAULT8_REALTIME_DAY 
	              , FAULT8_REALTIME_HOUR 
	              , FAULT8_REALTIME_MINUTE 
	              , FAULT8_REALTIME_SECOND 
	              , FAULT8_DATAHIGH 
	              , FAULT8_DATALOW 
	              , FAULT9_REALTIME_YEAR 
	              , FAULT9_REALTIME_MONTH 
	              , FAULT9_REALTIME_DAY 
	              , FAULT9_REALTIME_HOUR 
	              , FAULT9_REALTIME_MINUTE 
	              , FAULT9_REALTIME_SECOND 
	              , FAULT9_DATAHIGH 
	              , FAULT9_DATALOW 
	            --]  
	            )
				SELECT                              
	               B.DEVICE_ID                               
	              , B.DEVICE_TYPE_CD                          
	              , B.COLEC_DT                                
	              , B.OPER_STUS_CD                            
	              , B.PV_PW                                   
	              , B.PV_PW_H                                 
	              , B.PV_PW_PRICE                             
	              , B.PV_PW_CO2                               
	              , B.PV_STUS_CD                              
	              , B.CONS_PW                                 
	              , B.CONS_PW_H                               
	              , B.CONS_PW_PRICE                           
	              , B.CONS_PW_CO2                             
	              , B.BT_PW                                   
	              , B.BT_CHRG_PW_H                            
	              , B.BT_CHRG_PW_PRICE                        
	              , B.BT_CHRG_PW_CO2                          
	              , B.BT_DCHRG_PW_H                           
	              , B.BT_DCHRG_PW_PRICE                       
	              , B.BT_DCHRG_PW_CO2                         
	              , B.BT_SOC                                  
	              , B.BT_SOH                                  
	              , B.BT_STUS_CD                              
	              , B.GRID_PW                                 
	              , B.GRID_OB_PW_H                               
	              , B.GRID_OB_PW_PRICE                           
	              , B.GRID_OB_PW_CO2                             
	              , B.GRID_TR_PW_H                            
	              , B.GRID_TR_PW_PRICE                        
	              , B.GRID_TR_PW_CO2                          
	              , B.GRID_STUS_CD                            
	              , B.PCS_PW                                 
	              , B.PCS_FD_PW_H                                 
	              , B.PCS_PCH_PW_H                                 
	              , COALESCE(B.CREATE_DT, SYS_EXTRACT_UTC(NOW()))                               
	              , B.EMS_OPMODE                               
	              , B.PCS_OPMODE1                               
	              , B.PCS_OPMODE2                               
	              , B.PCS_TGT_PW                               
	              , B.FEED_IN_LIMIT                               
	              , B.MAX_INVERTER_PW_CD                               
	            -- [ 2015.05.13 yngwie OUTLET_PW 추가
	              , B.OUTLET_PW                               
	              , B.OUTLET_PW_H                               
	              , B.OUTLET_PW_PRICE                               
	              , B.OUTLET_PW_CO2                               
	              , FN_GET_PWR_RANGE_CD('ESS_GRID',  B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('GRID_ESS',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('GRID_LOAD', B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('ESS_LOAD',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('PV_ESS',    B.PV_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD)
	            -- ]
	            -- [ 2015.07.06 yngwie 수집 컬럼 추가
	              , B.PCS_COMM_ERR_RATE 
	              , B.SMTR_COMM_ERR_RATE 
	              , B.M2M_COMM_ERR_RATE      
	              , B.SMTR_TP_CD 
	              , B.SMTR_PULSE_CNT 
	              , B.SMTR_MODL_CD 
	              , B.PV_MAX_PWR1 
	              , B.PV_MAX_PWR2 
	              , B.INSTL_RGN 
	              , B.MMTR_SLV_ADDR 
	              , B.BASICMODE_CD 
	              , B.PCS_FLAG0  
	              , B.PCS_FLAG1  
	              , B.PCS_FLAG2  
	              , B.PCS_OPMODE_CMD1  
	              , B.PCS_OPMODE_CMD2  
	              , B.PV_V1  
	              , B.PV_I1  
	              , B.PV_PW1  
	              , B.PV_V2  
	              , B.PV_I2  
	              , B.PV_PW2  
	              , B.INVERTER_V  
	              , B.INVERTER_I  
	              , B.INVERTER_PW  
	              , B.DC_LINK_V  
	              , B.G_RLY_CNT  
	              , B.BAT_RLY_CNT  
	              , B.GRID_TO_GRID_RLY_CNT  
	              , B.BAT_PCHRG_RLY_CNT   
	              , B.BMS_FLAG0  
	              , B.BMS_DIAG0  
	              , B.RACK_V  
	              , B.RACK_I  
	              , B.CELL_MAX_V  
	              , B.CELL_MIN_V  
	              , B.CELL_AVG_V  
	              , B.CELL_MAX_T  
	              , B.CELL_MIN_T  
	              , B.CELL_AVG_T  
	              , B.TRAY_CNT          
	              , B.SYS_ST_CD 
	              , B.BT_CHRG_ST_CD 
	              , B.BT_DCHRG_ST_CD 
	              , B.PV_ST_CD 
	              , B.GRID_ST_CD 
	              , B.SMTR_ST_CD 
	              , B.SMTR_TR_CNTER 
	              , B.SMTR_OB_CNTER 
	              , B.DN_ENABLE 
	              , B.DC_START 
	              , B.DC_END 
	              , B.NC_START 
	              , B.NC_END                        
	            -- ]
	        --[신규모델로 모니터링 추가
	              , B.GRID_CODE0 
	              , B.GRID_CODE1 
	              , B.GRID_CODE2 
	              , B.PCON_BAT_TARGETPOWER 
	              , B.GRID_CODE3 
	              , B.BDC_MODULE_CODE0 
	              , B.BDC_MODULE_CODE1 
	              , B.BDC_MODULE_CODE2 
	              , B.BDC_MODULE_CODE3 
	              , B.FAULT5_REALTIME_YEAR 
	              , B.FAULT5_REALTIME_MONTH 
	              , B.FAULT5_DAY 
	              , B.FAULT5_HOUR 
	              , B.FAULT5_MINUTE 
	              , B.FAULT5_SECOND 
	              , B.FAULT5_DATAHIGH 
	              , B.FAULT5_DATALOW 
	              , B.FAULT6_REALTIME_YEAR 
	              , B.FAULT6_REALTIME_MONTH 
	              , B.FAULT6_REALTIME_DAY 
	              , B.FAULT6_REALTIME_HOUR 
	              , B.FAULT6_REALTIME_MINUTE 
	              , B.FAULT6_REALTIME_SECOND 
	              , B.FAULT6_DATAHIGH 
	              , B.FAULT6_DATALOW 
	              , B.FAULT7_REALTIME_YEAR 
	              , B.FAULT7_REALTIME_MONTH 
	              , B.FAULT7_REALTIME_DAY 
	              , B.FAULT7_REALTIME_HOUR 
	              , B.FAULT7_REALTIME_MINUTE 
	              , B.FAULT7_REALTIME_SECOND 
	              , B.FAULT7_DATAHIGH 
	              , B.FAULT7_DATALOW 
	              , B.FAULT8_REALTIME_YEAR 
	              , B.FAULT8_REALTIME_MONTH 
	              , B.FAULT8_REALTIME_DAY 
	              , B.FAULT8_REALTIME_HOUR 
	              , B.FAULT8_REALTIME_MINUTE 
	              , B.FAULT8_REALTIME_SECOND 
	              , B.FAULT8_DATAHIGH 
	              , B.FAULT8_DATALOW 
	              , B.FAULT9_REALTIME_YEAR 
	              , B.FAULT9_REALTIME_MONTH 
	              , B.FAULT9_REALTIME_DAY 
	              , B.FAULT9_REALTIME_HOUR 
	              , B.FAULT9_REALTIME_MINUTE 
	              , B.FAULT9_REALTIME_SECOND 
	              , B.FAULT9_DATAHIGH 
	              , B.FAULT9_DATALOW 
	            --]           
			FROM B
			WHERE NOT EXISTS (  
				SELECT 	1 
			    FROM 	U 
				WHERE	DEVICE_ID			= B.DEVICE_ID  
					AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
			  )
			 ;                 

           GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	       -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'STATE New Records ', v_work_num));
	      	RAISE notice 'v_log_fn[%] STATE New Records :  [%]건', v_log_fn, v_work_num;
           
            -- TEMP 테이블의 레코드를 이력테이블에 Insert
            INSERT INTO TB_OPR_ESS_STATE_HIST (
                 DEVICE_ID 
                , DEVICE_TYPE_CD 
                , COLEC_DT 
                , OPER_STUS_CD 
                , PV_PW 
                , PV_PW_H 
                , PV_PW_PRICE 
                , PV_PW_CO2 
                , PV_STUS_CD 
                , CONS_PW 
                , CONS_PW_H 
                , CONS_PW_PRICE 
                , CONS_PW_CO2 
                , BT_PW 
                , BT_CHRG_PW_H 
                , BT_CHRG_PW_PRICE 
                , BT_CHRG_PW_CO2 
                , BT_DCHRG_PW_H 
                , BT_DCHRG_PW_PRICE 
                , BT_DCHRG_PW_CO2 
                , BT_SOC 
                , BT_SOH 
                , BT_STUS_CD 
                , GRID_PW 
                , GRID_OB_PW_H 
                , GRID_OB_PW_PRICE 
                , GRID_OB_PW_CO2 
                , GRID_TR_PW_H 
                , GRID_TR_PW_PRICE 
                , GRID_TR_PW_CO2 
                , GRID_STUS_CD 
                , PCS_PW 
                , PCS_FD_PW_H 
                , PCS_PCH_PW_H 
                , EMS_OPMODE 
                , PCS_OPMODE1 
                , PCS_OPMODE2 
                , PCS_TGT_PW 
                , FEED_IN_LIMIT 
                , CREATE_DT 
                , MAX_INVERTER_PW_CD 
            -- [ 2015.05.13 yngwie OUTLET_PW 추가
                , OUTLET_PW 
                , OUTLET_PW_H 
                , OUTLET_PW_PRICE 
                , OUTLET_PW_CO2 
            -- ]
            -- [ 2015.07.06 yngwie 수집 컬럼 추가
                , PCS_COMM_ERR_RATE  
                , SMTR_COMM_ERR_RATE 
                , M2M_COMM_ERR_RATE  
                , SMTR_TP_CD 
                , SMTR_PULSE_CNT 
                , SMTR_MODL_CD 
                , PV_MAX_PWR1 
                , PV_MAX_PWR2 
                , INSTL_RGN 
                , MMTR_SLV_ADDR 
                , BASICMODE_CD 
              , PCS_FLAG0  
              , PCS_FLAG1  
              , PCS_FLAG2  
              , PCS_OPMODE_CMD1  
              , PCS_OPMODE_CMD2  
              , PV_V1  
              , PV_I1  
              , PV_PW1  
              , PV_V2  
              , PV_I2  
              , PV_PW2  
              , INVERTER_V  
              , INVERTER_I  
              , INVERTER_PW  
              , DC_LINK_V  
              , G_RLY_CNT  
              , BAT_RLY_CNT  
              , GRID_TO_GRID_RLY_CNT  
              , BAT_PCHRG_RLY_CNT    
              , BMS_FLAG0  
              , BMS_DIAG0  
              , RACK_V  
              , RACK_I  
              , CELL_MAX_V  
              , CELL_MIN_V  
              , CELL_AVG_V  
              , CELL_MAX_T  
              , CELL_MIN_T  
              , CELL_AVG_T  
              , TRAY_CNT   
              , SYS_ST_CD 
              , BT_CHRG_ST_CD 
              , BT_DCHRG_ST_CD 
              , PV_ST_CD 
              , GRID_ST_CD 
              , SMTR_ST_CD 
              , SMTR_TR_CNTER   
              , SMTR_OB_CNTER                                  
            -- ]         
  			--[신규모델로 모니터링 추가
	          , GRID_CODE0 
	          , GRID_CODE1 
	          , GRID_CODE2 
	          , PCON_BAT_TARGETPOWER 
	          , GRID_CODE3 
	          , BDC_MODULE_CODE0 
	          , BDC_MODULE_CODE1 
	          , BDC_MODULE_CODE2 
	          , BDC_MODULE_CODE3 
	          , FAULT5_REALTIME_YEAR 
	          , FAULT5_REALTIME_MONTH 
	          , FAULT5_DAY 
	          , FAULT5_HOUR 
	          , FAULT5_MINUTE 
	          , FAULT5_SECOND 
	          , FAULT5_DATAHIGH 
	          , FAULT5_DATALOW 
	          , FAULT6_REALTIME_YEAR 
	          , FAULT6_REALTIME_MONTH 
	          , FAULT6_REALTIME_DAY 
	          , FAULT6_REALTIME_HOUR 
	          , FAULT6_REALTIME_MINUTE 
	          , FAULT6_REALTIME_SECOND 
	          , FAULT6_DATAHIGH 
	          , FAULT6_DATALOW 
	          , FAULT7_REALTIME_YEAR 
	          , FAULT7_REALTIME_MONTH 
	          , FAULT7_REALTIME_DAY 
	          , FAULT7_REALTIME_HOUR 
	          , FAULT7_REALTIME_MINUTE 
	          , FAULT7_REALTIME_SECOND 
	          , FAULT7_DATAHIGH 
	          , FAULT7_DATALOW 
	          , FAULT8_REALTIME_YEAR 
	          , FAULT8_REALTIME_MONTH 
	          , FAULT8_REALTIME_DAY 
	          , FAULT8_REALTIME_HOUR 
	          , FAULT8_REALTIME_MINUTE 
	          , FAULT8_REALTIME_SECOND 
	          , FAULT8_DATAHIGH 
	          , FAULT8_DATALOW 
	          , FAULT9_REALTIME_YEAR 
	          , FAULT9_REALTIME_MONTH 
	          , FAULT9_REALTIME_DAY 
	          , FAULT9_REALTIME_HOUR 
	          , FAULT9_REALTIME_MINUTE 
	          , FAULT9_REALTIME_SECOND 
	          , FAULT9_DATAHIGH 
	          , FAULT9_DATALOW             
	          --]
            ) 
            SELECT 
                DEVICE_ID 
                , DEVICE_TYPE_CD 
                , COLEC_DT 
                , OPER_STUS_CD 
                , PV_PW 
                , PV_PW_H 
                , PV_PW_PRICE 
                , PV_PW_CO2 
                , PV_STUS_CD 
                , CONS_PW 
                , CONS_PW_H 
                , CONS_PW_PRICE 
                , CONS_PW_CO2 
                , BT_PW 
                , BT_CHRG_PW_H 
                , BT_CHRG_PW_PRICE 
                , BT_CHRG_PW_CO2 
                , BT_DCHRG_PW_H 
                , BT_DCHRG_PW_PRICE 
                , BT_DCHRG_PW_CO2 
                , BT_SOC 
                , BT_SOH 
                , BT_STUS_CD 
                , GRID_PW 
                , GRID_OB_PW_H 
                , GRID_OB_PW_PRICE 
                , GRID_OB_PW_CO2 
                , GRID_TR_PW_H 
                , GRID_TR_PW_PRICE 
                , GRID_TR_PW_CO2 
                , GRID_STUS_CD 
                , PCS_PW 
                , PCS_FD_PW_H 
                , PCS_PCH_PW_H 
                , EMS_OPMODE 
                , PCS_OPMODE1 
                , PCS_OPMODE2 
                , PCS_TGT_PW 
                , FEED_IN_LIMIT 
                , CREATE_DT 
                , MAX_INVERTER_PW_CD 
            -- [ 2015.05.13 yngwie OUTLET_PW 추가
                , OUTLET_PW 
                , OUTLET_PW_H 
                , OUTLET_PW_PRICE 
                , OUTLET_PW_CO2            
            -- ]
            -- [ 2015.07.06 yngwie 수집 컬럼 추가
                , PCS_COMM_ERR_RATE  
                , SMTR_COMM_ERR_RATE 
                , M2M_COMM_ERR_RATE  
                , SMTR_TP_CD         
                , SMTR_PULSE_CNT     
                , SMTR_MODL_CD   
                , PV_MAX_PWR1    
                , PV_MAX_PWR2    
                , INSTL_RGN          
                , MMTR_SLV_ADDR  
                , BASICMODE_CD
	            , PCS_FLAG0  
	            , PCS_FLAG1  
	            , PCS_FLAG2  
	            , PCS_OPMODE_CMD1  
	            , PCS_OPMODE_CMD2  
	            , PV_V1  
	            , PV_I1  
	            , PV_PW1  
	            , PV_V2  
	            , PV_I2  
	            , PV_PW2  
	            , INVERTER_V  
	            , INVERTER_I  
	            , INVERTER_PW  
	            , DC_LINK_V  
	            , G_RLY_CNT  
	            , BAT_RLY_CNT  
	            , GRID_TO_GRID_RLY_CNT  
	            , BAT_PCHRG_RLY_CNT    
	            , BMS_FLAG0  
	            , BMS_DIAG0  
	            , RACK_V  
	            , RACK_I  
	            , CELL_MAX_V  
	            , CELL_MIN_V  
	            , CELL_AVG_V  
	            , CELL_MAX_T  
	            , CELL_MIN_T  
	            , CELL_AVG_T  
	            , TRAY_CNT       
	            , SYS_ST_CD 
	            , BT_CHRG_ST_CD 
	            , BT_DCHRG_ST_CD 
	            , PV_ST_CD 
	            , GRID_ST_CD 
	            , SMTR_ST_CD  
	            , SMTR_TR_CNTER 
	            , SMTR_OB_CNTER                      
	            -- ]            
	 			--[신규모델로 모니터링 추가
	            , GRID_CODE0 
	            , GRID_CODE1 
	            , GRID_CODE2 
	            , PCON_BAT_TARGETPOWER 
	            , GRID_CODE3 
	            , BDC_MODULE_CODE0 
	            , BDC_MODULE_CODE1 
	            , BDC_MODULE_CODE2 
	            , BDC_MODULE_CODE3 
	            , FAULT5_REALTIME_YEAR 
	            , FAULT5_REALTIME_MONTH 
	            , FAULT5_DAY 
	            , FAULT5_HOUR 
	            , FAULT5_MINUTE 
	            , FAULT5_SECOND 
	            , FAULT5_DATAHIGH 
	            , FAULT5_DATALOW 
	            , FAULT6_REALTIME_YEAR 
	            , FAULT6_REALTIME_MONTH 
	            , FAULT6_REALTIME_DAY 
	            , FAULT6_REALTIME_HOUR 
	            , FAULT6_REALTIME_MINUTE 
	            , FAULT6_REALTIME_SECOND 
	            , FAULT6_DATAHIGH 
	            , FAULT6_DATALOW 
	            , FAULT7_REALTIME_YEAR 
	            , FAULT7_REALTIME_MONTH 
	            , FAULT7_REALTIME_DAY 
	            , FAULT7_REALTIME_HOUR 
	            , FAULT7_REALTIME_MINUTE 
	            , FAULT7_REALTIME_SECOND 
	            , FAULT7_DATAHIGH 
	            , FAULT7_DATALOW 
	            , FAULT8_REALTIME_YEAR 
	            , FAULT8_REALTIME_MONTH 
	            , FAULT8_REALTIME_DAY 
	            , FAULT8_REALTIME_HOUR 
	            , FAULT8_REALTIME_MINUTE 
	            , FAULT8_REALTIME_SECOND 
	            , FAULT8_DATAHIGH 
	            , FAULT8_DATALOW 
	            , FAULT9_REALTIME_YEAR 
	            , FAULT9_REALTIME_MONTH 
	            , FAULT9_REALTIME_DAY 
	            , FAULT9_REALTIME_HOUR 
	            , FAULT9_REALTIME_MINUTE 
	            , FAULT9_REALTIME_SECOND 
	            , FAULT9_DATAHIGH 
	            , FAULT9_DATALOW             
	            --]
            FROM 	TB_OPR_ESS_STATE_TMP 
            ;
            
           GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	       -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Insert TB_OPR_ESS_STATE_HIST Table :  ', v_work_num));
	      	RAISE notice 'v_log_fn[%] Insert TB_OPR_ESS_STATE_HIST Table :  [%]건', v_log_fn, v_work_num;

        END IF;
       
    	RAISE NOTICE 'v_log_fn[%] SP End !!! ', v_log_fn;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$procedure$
;

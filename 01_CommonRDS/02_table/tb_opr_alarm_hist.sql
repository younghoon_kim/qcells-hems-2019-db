-- Drop table

-- DROP TABLE wems.tb_opr_alarm_hist;

CREATE TABLE wems.tb_opr_alarm_hist (
	alarm_id varchar(50) NOT NULL, -- 장애 ID
	alarm_id_seq numeric(3) NOT NULL, -- 장애 ID 일련번호
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	alarm_type_cd varchar(4) NOT NULL, -- 장애유형코드
	alarm_cd varchar(4) NOT NULL, -- 장애코드
	event_dt varchar(14) NOT NULL, -- 발생일시
	clear_dt varchar(14) NOT NULL, -- 해제일시
	manu_recv_flag varchar(1) NULL, -- 수동복구여부
	manu_recv_user_id varchar(64) NULL, -- 수동복구자
	manu_recv_desc varchar(4000) NULL, -- 수동복구내용
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	CONSTRAINT pk_opr_alarm_hist PRIMARY KEY (alarm_id, alarm_id_seq, alarm_type_cd, alarm_cd, event_dt)
)
PARTITION BY RANGE (event_dt);
COMMENT ON TABLE wems.tb_opr_alarm_hist IS '장비에 발생한 장애이력 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_alarm_hist.alarm_id IS '장애 ID';
COMMENT ON COLUMN wems.tb_opr_alarm_hist.alarm_id_seq IS '장애 ID 일련번호';
COMMENT ON COLUMN wems.tb_opr_alarm_hist.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_opr_alarm_hist.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_opr_alarm_hist.alarm_type_cd IS '장애유형코드';
COMMENT ON COLUMN wems.tb_opr_alarm_hist.alarm_cd IS '장애코드';
COMMENT ON COLUMN wems.tb_opr_alarm_hist.event_dt IS '발생일시';
COMMENT ON COLUMN wems.tb_opr_alarm_hist.clear_dt IS '해제일시';
COMMENT ON COLUMN wems.tb_opr_alarm_hist.manu_recv_flag IS '수동복구여부';
COMMENT ON COLUMN wems.tb_opr_alarm_hist.manu_recv_user_id IS '수동복구자';
COMMENT ON COLUMN wems.tb_opr_alarm_hist.manu_recv_desc IS '수동복구내용';
COMMENT ON COLUMN wems.tb_opr_alarm_hist.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_opr_alarm_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_alarm_hist TO wems;

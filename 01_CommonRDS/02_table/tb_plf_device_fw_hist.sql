-- Drop table

-- DROP TABLE wems.tb_plf_device_fw_hist;

CREATE TABLE wems.tb_plf_device_fw_hist (
	req_id numeric(18) NOT NULL, -- 버전갱신이력일련번호
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	grp_id varchar(50) NOT NULL, -- 그룹ID
	ems_update_err_cd varchar(4) NULL, -- EMS 업데이트 결과 
	pcs_update_err_cd varchar(4) NULL, -- PCS 업데이트 결과
	bms_update_err_cd varchar(4) NULL, -- BMS 업데이트 결과
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	grp_upt_seq float8 NOT NULL, -- 그룹 변경 이력 순번
	fwi_upt_seq float8 NOT NULL, -- 펌웨어 관리정보 변경이력 순번
	cmd_req_status_cd varchar(4) NULL, -- 제어명령 진행상태 코드
	CONSTRAINT pk_plf_device_fw_hist PRIMARY KEY (req_id, device_id, device_type_cd, grp_id, grp_upt_seq, fwi_upt_seq)
);
COMMENT ON TABLE wems.tb_plf_device_fw_hist IS '장비의 펌웨어 갱신이력정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_device_fw_hist.req_id IS '버전갱신이력일련번호';
COMMENT ON COLUMN wems.tb_plf_device_fw_hist.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_plf_device_fw_hist.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_plf_device_fw_hist.grp_id IS '그룹ID';
COMMENT ON COLUMN wems.tb_plf_device_fw_hist.ems_update_err_cd IS 'EMS 업데이트 결과 ';
COMMENT ON COLUMN wems.tb_plf_device_fw_hist.pcs_update_err_cd IS 'PCS 업데이트 결과';
COMMENT ON COLUMN wems.tb_plf_device_fw_hist.bms_update_err_cd IS 'BMS 업데이트 결과';
COMMENT ON COLUMN wems.tb_plf_device_fw_hist.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_plf_device_fw_hist.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_plf_device_fw_hist.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_plf_device_fw_hist.update_dt IS '수정일시';
COMMENT ON COLUMN wems.tb_plf_device_fw_hist.grp_upt_seq IS '그룹 변경 이력 순번';
COMMENT ON COLUMN wems.tb_plf_device_fw_hist.fwi_upt_seq IS '펌웨어 관리정보 변경이력 순번';
COMMENT ON COLUMN wems.tb_plf_device_fw_hist.cmd_req_status_cd IS '제어명령 진행상태 코드';

-- Permissions

ALTER TABLE wems.tb_plf_device_fw_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_device_fw_hist TO wems;

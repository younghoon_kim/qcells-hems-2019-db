-- Drop table

-- DROP TABLE wems.tb_opr_terms_hist;

CREATE TABLE wems.tb_opr_terms_hist (
	user_id varchar(64) NOT NULL, -- 사용자ID (암호화)
	terms_ver varchar(100) NULL, -- 약관 버전
	terms_info varchar(4000) NULL, -- 약관 동의 정보
	bld_area_val numeric(18,2) NULL, -- 건물면적(m2)
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()) -- 등록일시
);
COMMENT ON TABLE wems.tb_opr_terms_hist IS '약관 동의 이력을 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_terms_hist.user_id IS '사용자ID (암호화)';
COMMENT ON COLUMN wems.tb_opr_terms_hist.terms_ver IS '약관 버전';
COMMENT ON COLUMN wems.tb_opr_terms_hist.terms_info IS '약관 동의 정보';
COMMENT ON COLUMN wems.tb_opr_terms_hist.bld_area_val IS '건물면적(m2)';
COMMENT ON COLUMN wems.tb_opr_terms_hist.create_dt IS '등록일시';

-- Permissions

ALTER TABLE wems.tb_opr_terms_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_terms_hist TO wems;

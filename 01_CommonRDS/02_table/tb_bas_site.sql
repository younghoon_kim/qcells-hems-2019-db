-- auto-generated definition
-- DROP table IF exists tb_bas_site;
create table tb_bas_site
(
    site_id   integer GENERATED ALWAYS AS IDENTITY (START WITH 100000000 INCREMENT BY 1),
    site_name varchar(128),
    owner_id            varchar(64),
    pin_code            varchar(4),
    instl_addr          varchar(4000),
    cntry_cd            varchar(4),
    city_cd             varchar(50),
    latitude            double precision,
    longitude           double precision,
    timezone_id         varchar(100),
    create_id varchar(64)                                                not null,
    create_dt timestamp(6) with time zone default sys_extract_utc(now()) not null,

    constraint pk_bas_site
        primary key (site_id)
);

comment on table tb_bas_site is 'site의 정보를 관리한다.';

comment on column tb_bas_site.site_id is 'site_id';
comment on column tb_bas_site.site_name is 'site_name';
comment on column tb_bas_site.owner_id is '소유자 id (암호화)';
comment on column tb_bas_site.pin_code is '사용자 가입 시 필요';
comment on column tb_bas_site.instl_addr is '설치주소 (암호화)';
comment on column tb_bas_site.cntry_cd is '국가코드';
comment on column tb_bas_site.city_cd is '도시코드';
comment on column tb_bas_site.latitude is '설치 위치 위도';
comment on column tb_bas_site.longitude is '설치 위치 경도';
comment on column tb_bas_site.timezone_id is '타임존 ID';
comment on column tb_bas_site.create_id is '등록자ID';
comment on column tb_bas_site.create_dt is '등록일시';

-- Drop table

-- DROP TABLE wems.tb_sys_sec_hist;

CREATE TABLE wems.tb_sys_sec_hist (
	login_seq numeric(18) NOT NULL, -- 접속SEQ
	user_id varchar(64) NOT NULL, -- 사용자ID
	client_type_cd varchar(4) NULL, -- 접속유형
	sec_type_cd varchar(4) NULL, -- 보안유형코드
	client_ip varchar(15) NULL, -- 접속IP
	ref_url varchar(255) NULL, -- 참조URL
	req_url varchar(255) NULL, -- 요청URL
	create_dt timestamptz NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	sec_desc varchar(4000) NULL,
	CONSTRAINT pk_sys_sec_hist PRIMARY KEY (login_seq)
);
COMMENT ON TABLE wems.tb_sys_sec_hist IS '시스템에 보안접속한 이력정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_sys_sec_hist.login_seq IS '접속SEQ';
COMMENT ON COLUMN wems.tb_sys_sec_hist.user_id IS '사용자ID';
COMMENT ON COLUMN wems.tb_sys_sec_hist.client_type_cd IS '접속유형';
COMMENT ON COLUMN wems.tb_sys_sec_hist.sec_type_cd IS '보안유형코드';
COMMENT ON COLUMN wems.tb_sys_sec_hist.client_ip IS '접속IP';
COMMENT ON COLUMN wems.tb_sys_sec_hist.ref_url IS '참조URL';
COMMENT ON COLUMN wems.tb_sys_sec_hist.req_url IS '요청URL';
COMMENT ON COLUMN wems.tb_sys_sec_hist.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_sys_sec_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_sys_sec_hist TO wems;

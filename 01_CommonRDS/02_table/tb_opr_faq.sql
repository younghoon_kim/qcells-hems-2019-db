-- Drop table

-- DROP TABLE wems.tb_opr_faq;

CREATE TABLE wems.tb_opr_faq (
	faq_seq float8 NOT NULL, -- 일련번호
	cntry_cd varchar(4) NOT NULL, -- 국가코드
	faq_type_cd varchar(4) NULL, -- FAQ 유형 코드
	subject varchar(4000) NOT NULL, -- 제목
	contents1 varchar(4000) NOT NULL, -- 내용1
	contents2 varchar(4000) NULL, -- 내용2
	create_id varchar(64) NULL, -- 등록자ID (암호화)
	create_dt timestamptz NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_opr_faq PRIMARY KEY (faq_seq, cntry_cd)
);
COMMENT ON TABLE wems.tb_opr_faq IS '서비스센터 관리';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_faq.faq_seq IS '일련번호';
COMMENT ON COLUMN wems.tb_opr_faq.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_opr_faq.faq_type_cd IS 'FAQ 유형 코드';
COMMENT ON COLUMN wems.tb_opr_faq.subject IS '제목';
COMMENT ON COLUMN wems.tb_opr_faq.contents1 IS '내용1';
COMMENT ON COLUMN wems.tb_opr_faq.contents2 IS '내용2';
COMMENT ON COLUMN wems.tb_opr_faq.create_id IS '등록자ID (암호화)';
COMMENT ON COLUMN wems.tb_opr_faq.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_opr_faq.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_opr_faq.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_opr_faq OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_faq TO wems;

-- Drop table

-- DROP TABLE wems.tb_sys_system_hist;

CREATE TABLE wems.tb_sys_system_hist (
	hist_seq numeric(18) NOT NULL, -- 발생 순번
	sys_type_cd varchar(10) NOT NULL, -- 시스템 구분 코드
	src_loc varchar(255) NULL, -- 소스위치
	err_cd varchar(50) NULL, -- 오류코드
	create_dt timestamptz NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	err_desc varchar(4000) NULL,
	CONSTRAINT pk_sys_system_hist PRIMARY KEY (hist_seq)
);
COMMENT ON TABLE wems.tb_sys_system_hist IS '시스템에서 발생한 오류 이력정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_sys_system_hist.hist_seq IS '발생 순번';
COMMENT ON COLUMN wems.tb_sys_system_hist.sys_type_cd IS '시스템 구분 코드';
COMMENT ON COLUMN wems.tb_sys_system_hist.src_loc IS '소스위치';
COMMENT ON COLUMN wems.tb_sys_system_hist.err_cd IS '오류코드';
COMMENT ON COLUMN wems.tb_sys_system_hist.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_sys_system_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_sys_system_hist TO wems;

-- Drop table

-- DROP TABLE wems.tb_opr_pvcalc_pv_info;

CREATE TABLE wems.tb_opr_pvcalc_pv_info (
	pv_brand_nm varchar(128) NOT NULL, -- 패널 브랜드명
	pv_prod_model_nm varchar(100) NOT NULL, -- PV 패널 모델명
	pv_pmax numeric(18,2) NOT NULL, -- PV 패널 정격 파워
	pv_voc numeric(18,2) NOT NULL, -- PV 패널 Over Circuit 전압
	pv_vmpp numeric(18,2) NULL, -- PV 패널 최대전력의 전압
	pv_isc numeric(18,2) NOT NULL, -- PV 패널 short 전력
	use_flag varchar(4) NULL DEFAULT 'Y'::character varying, --  USE_FLAG
	create_id varchar(64) NULL, -- 등록자ID (암호화)
	create_dt timestamptz NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	CONSTRAINT pk_opr_pvcalc_pv_info PRIMARY KEY (pv_brand_nm, pv_prod_model_nm)
);
COMMENT ON TABLE wems.tb_opr_pvcalc_pv_info IS 'PV 계산기를 위한 PV 패널 모델 정보';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_pvcalc_pv_info.pv_brand_nm IS '패널 브랜드명';
COMMENT ON COLUMN wems.tb_opr_pvcalc_pv_info.pv_prod_model_nm IS 'PV 패널 모델명';
COMMENT ON COLUMN wems.tb_opr_pvcalc_pv_info.pv_pmax IS 'PV 패널 정격 파워';
COMMENT ON COLUMN wems.tb_opr_pvcalc_pv_info.pv_voc IS 'PV 패널 Over Circuit 전압';
COMMENT ON COLUMN wems.tb_opr_pvcalc_pv_info.pv_vmpp IS 'PV 패널 최대전력의 전압';
COMMENT ON COLUMN wems.tb_opr_pvcalc_pv_info.pv_isc IS 'PV 패널 short 전력';
COMMENT ON COLUMN wems.tb_opr_pvcalc_pv_info.use_flag IS ' USE_FLAG';
COMMENT ON COLUMN wems.tb_opr_pvcalc_pv_info.create_id IS '등록자ID (암호화)';
COMMENT ON COLUMN wems.tb_opr_pvcalc_pv_info.create_dt IS '등록일시';

-- Permissions

ALTER TABLE wems.tb_opr_pvcalc_pv_info OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_pvcalc_pv_info TO wems;

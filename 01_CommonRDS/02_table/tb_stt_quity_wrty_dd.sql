-- Drop table

-- DROP TABLE wems.tb_stt_quity_wrty_dd;

CREATE TABLE wems.tb_stt_quity_wrty_dd (
	colec_dd varchar(8) NOT NULL, -- 수집일자
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	cell_max_vol_avg numeric(4,3) NULL, -- CELL_MAX_VOLTAGE의 평균값
	cell_min_vol_avg numeric(4,3) NULL, -- CELL_MIN_VOLTAGE의 평균값 
	rack_cur_avg numeric(4,1) NULL, -- RACK_CURRENT 의 평균값
	cell_max_temp_avg numeric(4,1) NULL, -- CELL_MAX_TEMPERATURE 의 평균값
	cell_min_temp_avg numeric(4,1) NULL, -- CELL_MIN_TEMPERATURE 의 평균값
	emsboard_temp_max_avg numeric(4,1) NULL, -- EMS 보드 온도의 최대값
	emsboard_temp_min_avg numeric(4,1) NULL, -- EMS 보드 온도의 최소값
	last_soh numeric(4,1) NULL, -- SOH 당일 마지막 값
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	CONSTRAINT pk_stt_quity_wrty_dd PRIMARY KEY (colec_dd, device_id, device_type_cd)
);
COMMENT ON TABLE wems.tb_stt_quity_wrty_dd IS '품질 보증을 위해 특정 수집 항목들의 일 평균, 최소, 최대값 등을 저장한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_stt_quity_wrty_dd.colec_dd IS '수집일자';
COMMENT ON COLUMN wems.tb_stt_quity_wrty_dd.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_stt_quity_wrty_dd.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_stt_quity_wrty_dd.cell_max_vol_avg IS 'CELL_MAX_VOLTAGE의 평균값';
COMMENT ON COLUMN wems.tb_stt_quity_wrty_dd.cell_min_vol_avg IS 'CELL_MIN_VOLTAGE의 평균값 ';
COMMENT ON COLUMN wems.tb_stt_quity_wrty_dd.rack_cur_avg IS 'RACK_CURRENT 의 평균값';
COMMENT ON COLUMN wems.tb_stt_quity_wrty_dd.cell_max_temp_avg IS 'CELL_MAX_TEMPERATURE 의 평균값';
COMMENT ON COLUMN wems.tb_stt_quity_wrty_dd.cell_min_temp_avg IS 'CELL_MIN_TEMPERATURE 의 평균값';
COMMENT ON COLUMN wems.tb_stt_quity_wrty_dd.emsboard_temp_max_avg IS 'EMS 보드 온도의 최대값';
COMMENT ON COLUMN wems.tb_stt_quity_wrty_dd.emsboard_temp_min_avg IS 'EMS 보드 온도의 최소값';
COMMENT ON COLUMN wems.tb_stt_quity_wrty_dd.last_soh IS 'SOH 당일 마지막 값';
COMMENT ON COLUMN wems.tb_stt_quity_wrty_dd.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_stt_quity_wrty_dd OWNER TO wems;
GRANT ALL ON TABLE wems.tb_stt_quity_wrty_dd TO wems;

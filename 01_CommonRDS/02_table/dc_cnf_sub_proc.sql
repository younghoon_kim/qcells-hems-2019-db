-- Drop table

-- DROP TABLE wems.dc_cnf_sub_proc;

CREATE TABLE wems.dc_cnf_sub_proc (
	id numeric(1) NOT NULL,
	id_str varchar(40) NOT NULL,
	parent numeric(1) NOT NULL,
	parentid varchar(40) NOT NULL,
	ipaddress varchar(20) NOT NULL,
	hostname varchar(40) NOT NULL,
	status numeric(1) NOT NULL,
	logcycle numeric(1) NOT NULL,
	description varchar(500) NULL,
	bin_name varchar(40) NOT NULL,
	args varchar(3000) NULL
);

-- Permissions

ALTER TABLE wems.dc_cnf_sub_proc OWNER TO wems;
GRANT ALL ON TABLE wems.dc_cnf_sub_proc TO wems;

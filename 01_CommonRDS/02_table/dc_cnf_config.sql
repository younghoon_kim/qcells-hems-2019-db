-- Drop table
DROP TABLE IF EXISTS dc_cnf_config;

-- Create table
create table dc_cnf_config
(
    seq      serial                             not null
        constraint dc_cnf_config_pkey
            primary key,
    cust_cd  varchar(30)                        not null,
    cfg_grp  varchar(30)                        not null,
    cfg_key  varchar(100)                       not null,
    cfg_val  varchar(1000)                      not null,
    cfg_flag numeric(1)               default 1 not null,
    cfg_desc varchar(1024),
    lst_dt   timestamp with time zone default sys_extract_utc(now()),
    constraint dc_cnf_config_cust_cd_cfg_flag_uindex
        unique (cust_cd, cfg_key)
);

comment on table dc_cnf_config is 'DAQ 설정 정보';

comment on column dc_cnf_config.seq is '일련번호';

comment on column dc_cnf_config.cust_cd is '고객코드';

comment on column dc_cnf_config.cfg_grp is '환경설정 Key Grouping';

comment on column dc_cnf_config.cfg_key is '환경설정 Key';

comment on column dc_cnf_config.cfg_val is '환경설정 value';

comment on column dc_cnf_config.cfg_flag is '0:미사용  1:사용(active)';

comment on column dc_cnf_config.cfg_desc is '설명';

comment on column dc_cnf_config.lst_dt is '최종수정일시';

alter table dc_cnf_config
    owner to wems;

-- sequence 생성
-- create sequence dc_cnf_config_seq_seq
--     as integer;

-- Insert data
INSERT INTO wems.dc_cnf_config ( cust_cd, cfg_grp, cfg_key, cfg_val, cfg_flag, cfg_desc, lst_dt) VALUES ('default', 'customer', 'customer', 'qcell', 1, '대상이 되는 고객코드', '2020-03-13 01:33:15.057759');
INSERT INTO wems.dc_cnf_config ( cust_cd, cfg_grp, cfg_key, cfg_val, cfg_flag, cfg_desc, lst_dt) VALUES ('qcell', 'healthcheck', 'socket-health', 'request:health!check#request,response:health!check#response', 1, 'health 체크시 handshake 정보', '2020-03-13 01:33:15.057759');
INSERT INTO wems.dc_cnf_config ( cust_cd, cfg_grp, cfg_key, cfg_val, cfg_flag, cfg_desc, lst_dt) VALUES ('qcell', 'healthcheck', 'master-health-url', '/daqmaster/healthcheck', 1, 'master health URL {dc_cnf_server.ip}:{dc_cnf_server.guiport}', '2020-03-13 01:33:15.057759');
INSERT INTO wems.dc_cnf_config ( cust_cd, cfg_grp, cfg_key, cfg_val, cfg_flag, cfg_desc, lst_dt) VALUES ('qcell', 'healthcheck', 'api-health-url', '/api/daqapi/healthcheck', 1, 'api server health URL', '2020-03-13 01:33:15.057759');
INSERT INTO wems.dc_cnf_config ( cust_cd, cfg_grp, cfg_key, cfg_val, cfg_flag, cfg_desc, lst_dt) VALUES ('qcell', 'event', 'event_email_title', 'daq event 알림', 1, '이벤트 이메일 제목', '2020-03-13 01:33:15.057759');
INSERT INTO wems.dc_cnf_config ( cust_cd, cfg_grp, cfg_key, cfg_val, cfg_flag, cfg_desc, lst_dt) VALUES ('qcell', 'event', 'event_email_contents', '<h2>daq 에서 발생한 이벤트 </h2>', 1, '이벤트 이메일 내용 중 상위내용 (상세내용은 프로그램에서)', '2020-03-13 01:33:15.057759');
INSERT INTO wems.dc_cnf_config ( cust_cd, cfg_grp, cfg_key, cfg_val, cfg_flag, cfg_desc, lst_dt) VALUES ('qcell', 'ess', 'network_disconnect_ftp_basepath', '/home/bems/project/BEMS_GW/ASCA/Raw/UPLOAD', 1, '네트워크 단절시 ftp로 업로드되는 파일경로', '2020-03-13 01:33:15.057759');
INSERT INTO wems.dc_cnf_config ( cust_cd, cfg_grp, cfg_key, cfg_val, cfg_flag, cfg_desc, lst_dt) VALUES ('qcell', 'event', 'event_email_cc', 'sangwon.yun@hanwha-qcells.com,myungkyoon.kim@hanwha-qcells.com,insung.seok@hanwha-qcells.com', 1, '이벤트 참조 수신자', '2020-03-13 01:33:15.057759');
INSERT INTO wems.dc_cnf_config ( cust_cd, cfg_grp, cfg_key, cfg_val, cfg_flag, cfg_desc, lst_dt) VALUES ('qcell', 'event', 'event_email_to', 'sangwon.yun@hanwha-qcells.com,myungkyoon.kim@hanwha-qcells.com,insung.seok@hanwha-qcells.com', 1, '이벤트 수신자', '2020-03-13 01:33:15.057759');




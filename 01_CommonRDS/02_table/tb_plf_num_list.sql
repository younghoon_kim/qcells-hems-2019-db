-- Drop table

-- DROP TABLE wems.tb_plf_num_list;

CREATE TABLE wems.tb_plf_num_list (
	num float8 NOT NULL, -- 숫자번호
	char_num varchar(2) NULL, -- 문자번호
	lpad_num varchar(2) NULL, -- 왼쪽정렬번호
	CONSTRAINT pk_plf_num_list PRIMARY KEY (num)
);
COMMENT ON TABLE wems.tb_plf_num_list IS '시스템 관리에 필요한 일련번호를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_num_list.num IS '숫자번호';
COMMENT ON COLUMN wems.tb_plf_num_list.char_num IS '문자번호';
COMMENT ON COLUMN wems.tb_plf_num_list.lpad_num IS '왼쪽정렬번호';

-- Permissions

ALTER TABLE wems.tb_plf_num_list OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_num_list TO wems;

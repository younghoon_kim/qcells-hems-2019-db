-- Drop table

-- DROP TABLE wems.tb_opr_event;

CREATE TABLE wems.tb_opr_event (
	event_seq float8 NOT NULL, -- 일련번호
	cntry_cd varchar(4) NOT NULL, -- 국가코드
	start_tm varchar(10) NOT NULL, -- 시작일시
	end_tm varchar(10) NOT NULL, -- 종료일시
	event_type_cd varchar(4) NOT NULL, -- 이벤트 상태 코드
	subject varchar(4000) NOT NULL, -- 제목
	contents1 varchar(4000) NOT NULL, -- 내용1
	contents2 varchar(4000) NULL, -- 내용2
	pctr_file varchar(4000) NULL, -- 이미지 파일(암호화)
	pctr_file_path varchar(4000) NULL, -- 파일 경로(암호화)
	pctr_file_disp_name varchar(4000) NULL, -- 화면에 표시되는 명칭
	create_id varchar(64) NULL, -- 등록자ID (암호화)
	create_dt timestamptz NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_opr_event PRIMARY KEY (event_seq, cntry_cd)
);
COMMENT ON TABLE wems.tb_opr_event IS '이벤트 관리';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_event.event_seq IS '일련번호';
COMMENT ON COLUMN wems.tb_opr_event.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_opr_event.start_tm IS '시작일시';
COMMENT ON COLUMN wems.tb_opr_event.end_tm IS '종료일시';
COMMENT ON COLUMN wems.tb_opr_event.event_type_cd IS '이벤트 상태 코드';
COMMENT ON COLUMN wems.tb_opr_event.subject IS '제목';
COMMENT ON COLUMN wems.tb_opr_event.contents1 IS '내용1';
COMMENT ON COLUMN wems.tb_opr_event.contents2 IS '내용2';
COMMENT ON COLUMN wems.tb_opr_event.pctr_file IS '이미지 파일(암호화)';
COMMENT ON COLUMN wems.tb_opr_event.pctr_file_path IS '파일 경로(암호화)';
COMMENT ON COLUMN wems.tb_opr_event.pctr_file_disp_name IS '화면에 표시되는 명칭';
COMMENT ON COLUMN wems.tb_opr_event.create_id IS '등록자ID (암호화)';
COMMENT ON COLUMN wems.tb_opr_event.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_opr_event.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_opr_event.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_opr_event OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_event TO wems;

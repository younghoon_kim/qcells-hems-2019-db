-- Drop table

-- DROP TABLE wems.tb_bas_advice_tmpl;

CREATE TABLE wems.tb_bas_advice_tmpl (
	tmpl_id varchar(50) NOT NULL, -- 템플릿 ID
	lang_cd varchar(4) NOT NULL, -- 언어 코드
	dsp_seq numeric(4) NOT NULL, -- 표시 순서
	msg_txt varchar(4000) NULL, -- 메세지
	ref_cols varchar(4000) NULL, -- 참조 컬럼 순서
	CONSTRAINT pk_bas_advice_tmpl PRIMARY KEY (tmpl_id, lang_cd, dsp_seq)
);
COMMENT ON TABLE wems.tb_bas_advice_tmpl IS '예측 데이터에 대한 어드바이스 문구 템플릿을 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_advice_tmpl.tmpl_id IS '템플릿 ID';
COMMENT ON COLUMN wems.tb_bas_advice_tmpl.lang_cd IS '언어 코드';
COMMENT ON COLUMN wems.tb_bas_advice_tmpl.dsp_seq IS '표시 순서';
COMMENT ON COLUMN wems.tb_bas_advice_tmpl.msg_txt IS '메세지';
COMMENT ON COLUMN wems.tb_bas_advice_tmpl.ref_cols IS '참조 컬럼 순서';

-- Permissions

ALTER TABLE wems.tb_bas_advice_tmpl OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_advice_tmpl TO wems;

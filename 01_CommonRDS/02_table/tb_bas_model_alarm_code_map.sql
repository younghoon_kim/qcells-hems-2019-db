-- Drop table

-- DROP TABLE wems.tb_bas_model_alarm_code_map;

CREATE TABLE wems.tb_bas_model_alarm_code_map (
	product_model_type_cd varchar(10) NOT NULL, -- 제품모델 유형
	alarm_type_cd varchar(4) NOT NULL, -- 장애유형코드
	alarm_cd varchar(4) NOT NULL, -- 장애코드
	apply_user_flag varchar(1) NULL, -- 고객 적용 여부
	apply_instlr_flag varchar(1) NULL, -- 인스톨러 적용 여부
	apply_oprtr_flag varchar(1) NULL, -- 오퍼레이터 적용 여부
	apply_admin_flag varchar(1) NULL, -- 어드민 적용 여부
	noti_user_flag varchar(1) NULL, -- 고객 알림 여부
	noti_instlr_flag varchar(1) NULL, -- 인스톨러 알림 여부
	noti_oprtr_flag varchar(1) NULL, -- 오퍼레이터 알림 여부
	noti_admin_flag varchar(1) NULL, -- 어드민 알림 여부
	create_id varchar(64) NOT NULL, -- 등록자ID (암호화)
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID (암호화)
	update_dt timestamptz NULL, -- 수정일시
	alarm_title varchar(225) NULL, -- 장애 제목 Properties Key
	alarm_desc varchar(225) NULL, -- 장애 설명 Properties Key
	repeat_flag varchar(1) NULL, --알람 메일 반복 발송 여부
	repeat_interval varchar(6) NULL, -- 알람 메일 반복 주기
	CONSTRAINT pk_bas_model_alarm_code_map PRIMARY KEY (product_model_type_cd, alarm_type_cd, alarm_cd)
);
COMMENT ON TABLE wems.tb_bas_model_alarm_code_map IS '모델별 장애 맵핑 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.product_model_type_cd IS '제품모델 유형';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.alarm_type_cd IS '장애유형코드';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.alarm_cd IS '장애코드';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.apply_user_flag IS '고객 적용 여부';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.apply_instlr_flag IS '인스톨러 적용 여부';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.apply_oprtr_flag IS '오퍼레이터 적용 여부';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.apply_admin_flag IS '어드민 적용 여부';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.noti_user_flag IS '고객 알림 여부';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.noti_instlr_flag IS '인스톨러 알림 여부';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.noti_oprtr_flag IS '오퍼레이터 알림 여부';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.noti_admin_flag IS '어드민 알림 여부';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.create_id IS '등록자ID (암호화)';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.update_id IS '수정자ID (암호화)';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.update_dt IS '수정일시';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.alarm_title IS '장애 제목 Properties Key';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.alarm_desc IS '장애 설명 Properties Key';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.repeat_flag IS '알람 메일 반복 발송 여부';
COMMENT ON COLUMN wems.tb_bas_model_alarm_code_map.repeat_interval IS '알람 메일 반복 주기';

-- Permissions

ALTER TABLE wems.tb_bas_model_alarm_code_map OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_model_alarm_code_map TO wems;

-- auto-generated definition
create table tb_opr_offline_info
(
    device_id varchar(50) not null,
    start_tm  varchar(14) not null,
    end_tm    varchar(14),
    type      varchar(4),
    constraint tb_opr_offline_info_pk
        primary key (device_id, start_tm)
);

comment on table tb_opr_offline_info is '네트워크 단절 정보 저장';

alter table tb_opr_offline_info
    owner to wems;


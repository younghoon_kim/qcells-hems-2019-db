-- Drop table

-- DROP TABLE wems.tb_plf_menu_auth;

CREATE TABLE wems.tb_plf_menu_auth (
	auth_type_cd varchar(4) NOT NULL, -- 권한유형코드
	menu_id varchar(15) NOT NULL, -- 메뉴ID
	use_flag varchar(1) NULL, -- 사용여부
	CONSTRAINT pk_plf_menu_auth PRIMARY KEY (auth_type_cd, menu_id)
);
COMMENT ON TABLE wems.tb_plf_menu_auth IS '메뉴 사용에 대한 권한 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_menu_auth.auth_type_cd IS '권한유형코드';
COMMENT ON COLUMN wems.tb_plf_menu_auth.menu_id IS '메뉴ID';
COMMENT ON COLUMN wems.tb_plf_menu_auth.use_flag IS '사용여부';

-- Permissions

ALTER TABLE wems.tb_plf_menu_auth OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_menu_auth TO wems;

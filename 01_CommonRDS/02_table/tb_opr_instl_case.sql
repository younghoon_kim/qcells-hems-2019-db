-- Drop table

-- DROP TABLE wems.tb_opr_instl_case;

CREATE TABLE wems.tb_opr_instl_case (
	instl_case_seq float8 NOT NULL, -- 일련번호
	cntry_cd varchar(4) NOT NULL, -- 국가코드
	apply_txt varchar(1000) NOT NULL, -- 적용
	capacity_txt varchar(1000) NOT NULL, -- 용량명
	oper_start_ym varchar(6) NOT NULL, -- 가동년월
	subject varchar(4000) NOT NULL, -- 제목
	pctr_file varchar(4000) NULL, -- 이미지 파일(암호화)
	pctr_file_path varchar(4000) NULL, -- 파일 경로(암호화)
	pctr_file_disp_name varchar(4000) NULL, -- 화면에 표시되는 명칭
	create_id varchar(64) NULL, -- 등록자ID (암호화)
	create_dt timestamptz NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	done_flag varchar(1) NULL,
	CONSTRAINT pk_opr_instl_case PRIMARY KEY (instl_case_seq, cntry_cd)
);
COMMENT ON TABLE wems.tb_opr_instl_case IS '설치사례 관리';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_instl_case.instl_case_seq IS '일련번호';
COMMENT ON COLUMN wems.tb_opr_instl_case.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_opr_instl_case.apply_txt IS '적용';
COMMENT ON COLUMN wems.tb_opr_instl_case.capacity_txt IS '용량명';
COMMENT ON COLUMN wems.tb_opr_instl_case.oper_start_ym IS '가동년월';
COMMENT ON COLUMN wems.tb_opr_instl_case.subject IS '제목';
COMMENT ON COLUMN wems.tb_opr_instl_case.pctr_file IS '이미지 파일(암호화)';
COMMENT ON COLUMN wems.tb_opr_instl_case.pctr_file_path IS '파일 경로(암호화)';
COMMENT ON COLUMN wems.tb_opr_instl_case.pctr_file_disp_name IS '화면에 표시되는 명칭';
COMMENT ON COLUMN wems.tb_opr_instl_case.create_id IS '등록자ID (암호화)';
COMMENT ON COLUMN wems.tb_opr_instl_case.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_opr_instl_case.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_opr_instl_case.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_opr_instl_case OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_instl_case TO wems;

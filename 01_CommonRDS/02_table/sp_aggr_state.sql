CREATE OR REPLACE FUNCTION sp_aggr_state(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
  NAME:       SP_AGGR_STATE
  PURPOSE:

  REVISIONS:
  Ver        Date        Author           Description
  ---------  ----------  ---------------  ------------------------------------
  1.0        2014-06-16   yngwie      Created this function.a
  1.1        2016-01-05   jkm1020    ESM (EMS Sleep Mode) 추가
  2.0        2019-09-24   박동환       Postgresql 변환
  2.1        2020-01-22   박동환       오류수정(A.COL126<>'NA' => A.COL126 IS NULL OR A.COL126<>'NA')  
  2.2        2020-04-24   이재형       미처리 데이터 보정을 위한 raw_ems_info_tmp 저장 데이터 쿼리 수정
  2.3        2020-05-28   서정우       오류수정(TB_RAW_EMS_INFO_TMP에 col88, col89 데이터가 3자리까지 수신됨, 4자리까지 고려하여 수정)
  2.4        2020-06-02   서정우       OPER_STUS_CD의 연결/미연결 정보를 분리
  2.6        2020-06-05   윤상원       네트워크 단절 처리(상태 업데이트 조건 추가, 알람 필터링)
  2.7        2020-07-24   서정우       site_id 적용에 따른 수정
  2.8        2020-08-10   윤상원       BT_REAL_SOC 컬럼 추가 (UI에서 미사용중인 tb_raw_bms_info의 col14 이용)
  2.9        2020-09-23   서정우       load_main_pw, load_sub_pw 컬럼 추가 (tb_raw_ems_info의 col37, col38)

  
  NOTES:   1분주기 수집데이터를 가지고 상태/예측/장애 데이터를 생성한다.
                   < INPUT >
                     1.i_sp_nm        프로시져명
                     2.i_site_seq	     서비스대상번호
                     3.i_exe_base     실행기준값(월별    :YYYYMM 
                                                일별    :YYYYMMDD 
                                                시간대별:YYYYMMDDHH0000 
                                                15분    :YYYYMMDDHHMI00)
                     4.i_exe_mthd     실행방법모드(A:자동, M:수동)

                   < OUTPUT >
                     1.v_rslt_cd     실행상태코드 (0:실행중,1:실행성공,그외:실행오류)  

*****************************************************************************/
-- 수동 실행 스크립트
-- SELECT SP_AGGR_STATE('SP_AGGR_STATE', 0, '20191024102000', 'M');

DECLARE
    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
    v_st_dt                 	TIMESTAMP := CLOCK_TIMESTAMP(); 	-- 시작일시
    v_rslt_cd          		VARCHAR  	:= '0';   				-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt              	INTEGER   	:= 0;     				-- 적재수
    v_err_msg      			VARCHAR   	:= NULL;  			-- 오류메시지
    v_err_msg1              VARCHAR  	:= NULL;  			-- 메시지내용1
    v_err_msg2              VARCHAR   	:= NULL;  			-- 메시지내용2
    v_err_msg3              VARCHAR   	:= NULL;  			-- 메시지내용3

    -- (2)업무처리 변수
    v_work_num    INTEGER   := 0;     -- 처리건수
    v_handleFlag 	VARCHAR;
    v_preHandleFlag VARCHAR;
    v_selectDt   		VARCHAR; --TIMESTAMP WITH TIME ZONE;
    v_baseTm   		TIMESTAMPTZ;
    v_row           	record; 
	
   	v_sql				VARCHAR;
    v_tbl				VARCHAR;
   	v_ExeCnt1 		INTEGER 		:= 0;
   	v_ExeCnt2 		INTEGER 		:= 0;
   	v_ExeCnt3 		INTEGER 		:= 0;
  
BEGIN
   	v_baseTm 		:= SYS_EXTRACT_UTC(TO_TIMESTAMP(i_exe_base, 'YYYYMMDDHH24MISS'));
    v_preHandleFlag := TO_CHAR(v_baseTm - interval '1 day', 'YYYYMMDDHH24MISS');
   	v_handleFlag 	:= TO_CHAR(v_baseTm, 'YYYYMMDDHH24MISS');

	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base 
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
--	    RAISE NOTICE 'i_sp_nm[%] i_exe_base[%] v_handleFlag[%] v_baseTm[%] START[%]', i_sp_nm, i_exe_base, v_handleFlag, v_baseTm, TO_CHAR(now(), 'YYYY-MM-DD HH24:MI:SS');
       	
       	-- EMS 처리
		TRUNCATE TABLE TB_RAW_EMS_INFO_TMP;

        -- 미처리 데이터 확인을 위한 쿼리 추가 ess_state_hist와 비교로 미처리 데이터 조회 , 20200424 이재형
        INSERT INTO TB_RAW_EMS_INFO_TMP 
        SELECT   DEVICE_ID, COLEC_DT, DONE_FLAG  
            , COL0, COL1, COL2, COL3, COL4, COL5, COL6, COL7, COL8, COL9, COL10  
            , COL11, COL12, COL13, COL14, COL15, COL16, COL17, COL18, COL19, COL20  
            , COL21, COL22, COL23, COL24, COL25, COL26, COL27, COL28, COL29, COL30 
            , COL31, COL32, COL33, COL34, COL35, COL36, COL37, COL38, COL39, COL40 
            , COL41, COL42, COL43, COL44, COL45, COL46, COL47, COL48, COL49, COL50
            , COL51, COL52, COL53, COL54, COL55, COL56, COL57, COL58, COL59, COL60 
            , COL61, COL62, COL63, COL64, COL65, COL66, COL67, COL68, COL69, COL70 
            , COL71, COL72, COL73, COL74, COL75, COL76, COL77, COL78, COL79, COL80 
            , COL81, COL82, COL83, COL84, COL85, COL86, COL87 
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR( COL88, 3))) )::NUMERIC, 'FM0000000000000000') AS COL88  
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR( COL89, 3))) )::NUMERIC, 'FM0000000000000000') AS COL89  
            , COL90, COL91, COL92, COL93, COL94, COL95, COL96, COL97, COL98, COL99, COL100
			, COL101, COL102, COL103, COL104, COL105, COL106, COL107, COL108, COL109, COL110 
			, COL111, COL112, COL113, COL114, COL115, COL116, COL117, COL118, COL119, COL120 
			, COL121, COL122, COL123, COL124, COL125, COL126, COL127, COL128, COL129, COL130 
			, COL131, COL132, COL133, COL134, COL135, COL136, COL137, COL138, COL139, COL140 
			, COL141, COL142, COL143, COL144, COL145, COL146, COL147, COL148, COL149, COL150 
			, COL151, COL152, COL153, COL154, COL155, COL156, COL157, COL158, COL159, COL160 
			, COL161, COL162, COL163, COL164, COL165, COL166, COL167, COL168, COL169, COL170 
			, COL171, COL172, COL173, COL174, COL175, COL176, COL177, COL178, COL179, COL180 
			, COL181, COL182, COL183, COL184, COL185, COL186, COL187, COL188, COL189, COL190 
			, COL191, COL192, COL193, COL194, COL195, COL196, COL197, COL198, COL199, COL200 
			, COL201, COL202, COL203, COL204, COL205, COL206, COL207, COL208, COL209, COL210 
			, COL211, COL212, COL213, COL214, COL215, COL216, COL217, COL218, COL219, COL220 
			, COL221, COL222, COL223, COL224, COL225, COL226, COL227, COL228, COL229, COL230 
			, COL231, COL232, COL233, COL234, COL235, COL236, COL237, COL238, COL239, COL240 
			, COL241, COL242, COL243, COL244, COL245, COL246, COL247, COL248, COL249, COL250 
			, COL251, COL252, COL253, COL254, COL255, COL256, COL257, COL258, COL259, COL260 
			, COL261, COL262, COL263, COL264, COL265, COL266, COL267, COL268, COL269, COL270
			, COL271, COL272, COL273, COL274, COL275, COL276, COL277, COL278, COL279, COL280
			, COL281, COL282, COL283, COL284, COL285, COL286, COL287, COL288, COL289, COL290
			, COL291, COL292, COL293, COL294, COL295, COL296, COL297, COL298, COL299, COL300
			, COL301, COL302, COL303, COL304, COL305, COL306, COL307, COL308, COL309, COL310
			, COL311, COL312, COL313, COL314, COL315, COL316, COL317, COL318, COL319, COL320
			, COL321, COL322, COL323, COL324, COL325, COL326, COL327, COL328, COL329, COL330
			, COL331, COL332, COL333, COL334, COL335, COL336, COL337, COL338, COL339, COL340
			, COL341            
			, CREATE_DT 
            , HANDLE_FLAG  
        FROM   	TB_RAW_EMS_INFO
		WHERE   CREATE_DT >= TO_TIMESTAMP(SUBSTR(v_handleFlag, 1, 12)||'00', 'YYYYMMDDHH24MISS') - interval '2 minute'
		AND CREATE_DT <= TO_TIMESTAMP(SUBSTR(v_handleFlag, 1, 12)||'59', 'YYYYMMDDHH24MISS');



   	
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	    v_ExeCnt1 := v_work_num;
--		RAISE NOTICE 'i_sp_nm[%] Insert TB_RAW_EMS_INFO_TMP Table [%]건', i_sp_nm, v_work_num;

                  
   		v_rslt_cd := 'B';
		TRUNCATE TABLE TB_RAW_PCS_INFO_TMP;

       	INSERT INTO TB_RAW_PCS_INFO_TMP 
        SELECT 
            A.DEVICE_ID, A.COLEC_DT, A.DONE_FLAG    
			, TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL0, 3))) )::NUMERIC, 'FM00000000') AS COL0  
			, TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL1, 3))) )::NUMERIC, 'FM00000000') AS COL1  
			, TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL2, 3))) )::NUMERIC, 'FM00000000') AS COL2  
			, A.COL3, A.COL4, A.COL5, A.COL6, A.COL7, A.COL8, A.COL9, A.COL10
			, A.COL11, A.COL12, A.COL13, A.COL14, A.COL15, A.COL16, A.COL17, A.COL18, A.COL19, A.COL20
			, A.COL21, A.COL22, A.COL23, A.COL24, A.COL25, A.COL26, A.COL27, A.COL28, A.COL29, A.COL30
			, A.COL31, A.COL32, A.COL33, A.COL34, A.COL35, A.COL36, A.COL37, A.COL38, A.COL39, A.COL40
			, A.COL41, A.COL42, A.COL43, A.COL44, A.COL45, A.COL46, A.COL47, A.COL48, A.COL49, A.COL50
			, A.COL51, A.COL52, A.COL53, A.COL54, A.COL55, A.COL56, A.COL57, A.COL58, A.COL59, A.COL60
			, A.COL61, A.COL62, A.COL63, A.COL64, A.COL65, A.COL66, A.COL67, A.COL68, A.COL69, A.COL70
			, A.COL71, A.COL72, A.COL73, A.COL74, A.COL75, A.COL76, A.COL77, A.COL78, A.COL79, A.COL80
			, A.COL81, A.COL82, A.COL83, A.COL84, A.COL85, A.COL86, A.COL87, A.COL88, A.COL89, A.COL90
			, A.COL91, A.COL92, A.COL93, A.COL94, A.COL95, A.COL96, A.COL97, A.COL98, A.COL99, A.COL100
			, A.COL101, A.COL102, A.COL103, A.COL104, A.COL105, A.COL106, A.COL107, A.COL108, A.COL109, A.COL110
			, A.COL111, A.COL112, A.COL113, A.COL114, A.COL115, A.COL116, A.COL117, A.COL118, A.COL119, A.COL120
			, A.COL121, A.COL122, A.COL123, A.COL124, A.COL125, A.COL126, A.COL127, A.COL128, A.COL129, A.COL130
			, A.COL131, A.COL132, A.COL133, A.COL134, A.COL135, A.COL136, A.COL137, A.COL138, A.COL139, A.COL140
			, A.COL141, A.COL142, A.COL143, A.COL144, A.COL145, A.COL146, A.COL147, A.COL148, A.COL149, A.COL150
			, A.COL151, A.COL152, A.COL153, A.COL154, A.COL155, A.COL156, A.COL157, A.COL158, A.COL159, A.COL160
			, A.COL161, A.COL162, A.COL163, A.COL164, A.COL165, A.COL166, A.COL167, A.COL168, A.COL169, A.COL170
			, A.COL171, A.COL172, A.COL173, A.COL174, A.COL175, A.COL176, A.COL177, A.COL178, A.COL179, A.COL180
			, A.COL181, A.COL182, A.COL183, A.COL184, A.COL185, A.COL186, A.COL187, A.COL188, A.COL189, A.COL190
			, A.COL191, A.COL192, A.COL193, A.COL194, A.COL195, A.COL196, A.COL197, A.COL198, A.COL199, A.COL200
			, A.COL201, A.COL202, A.COL203, A.COL204, A.COL205, A.COL206, A.COL207, A.COL208, A.COL209, A.COL210
			, A.COL211, A.COL212, A.COL213, A.COL214, A.COL215, A.COL216, A.COL217, A.COL218, A.COL219, A.COL220
			, A.COL221, A.COL222, A.COL223, A.COL224, A.COL225, A.COL226 
			, A.CREATE_DT 
			, A.HANDLE_FLAG 
        FROM TB_RAW_EMS_INFO_TMP B, 
        		 TB_RAW_PCS_INFO A		
        WHERE  B.COLEC_DT = A.COLEC_DT 
            AND B.DEVICE_ID = A.DEVICE_ID  ;
       
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--		RAISE NOTICE 'i_sp_nm[%] Insert TB_RAW_PCS_INFO_TMP Table [%]건', i_sp_nm, v_work_num;

	
		v_rslt_cd := 'C';
        TRUNCATE TABLE TB_RAW_BMS_INFO_TMP;

        INSERT INTO TB_RAW_BMS_INFO_TMP 
        SELECT 
             A.DEVICE_ID, A.COLEC_DT, A.DONE_FLAG  
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL0, 3))) )::NUMERIC, 'FM00000000') AS COL0
            , A.COL1, A.COL2, A.COL3, A.COL4, A.COL5, A.COL6, A.COL7, A.COL8, A.COL9, A.COL10    
            , A.COL11, A.COL12, A.COL13, A.COL14, A.COL15, A.COL16, A.COL17, A.COL18, A.COL19    
            , CASE WHEN A.COL20 IS NOT NULL AND A.COL20::NUMERIC> 4 THEN '0' ELSE A.COL20 end COL20  
            , A.COL21, A.COL22, A.COL23    
--          , A.COL24    
            , (CASE WHEN POSITION('0x' in A.COL24) > 0 THEN HEX2DEC(UPPER(SUBSTR(A.COL24, 3))) ELSE A.COL24::NUMERIC END)::VARCHAR COL24
            , A.COL25, A.COL26, A.COL27, A.COL28, A.COL29, A.COL30    
            , A.COL31, A.COL32, A.COL33, A.COL34, A.COL35, A.COL36, A.COL37, A.COL38, A.COL39, A.COL40    
            , A.CREATE_DT 
            , A.HANDLE_FLAG 
        FROM 	TB_RAW_EMS_INFO_TMP B , 
        			TB_RAW_BMS_INFO A 
        WHERE  	B.COLEC_DT = A.COLEC_DT 
           AND 	B.DEVICE_ID = A.DEVICE_ID	;
                
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;
--		RAISE NOTICE 'i_sp_nm[%] Insert TB_RAW_BMS_INFO_TMP Table [%]건', i_sp_nm, v_work_num;
   	   

		IF v_ExeCnt1 > 0 THEN
       
            ------------------------
            -- 1. 장애 데이터 처리
            ------------------------
	 		v_rslt_cd := 'D';

 	        TRUNCATE TABLE TB_OPR_ALARM_TMP;
	        TRUNCATE TABLE TB_OPR_ALARM_TMP_RAW;

            -- TEMP 테이블에 장애 발생/미발생 데이터 생성
            -- 2015.09.03 yngwie 모델별 에러코드 적용 관련 으로 TB_OPR_ALARM_TMP_RAW 테이블에 먼저 insert 한 후
            --                   필터링한 데이터를 TB_OPR_ALARM_TMP 로 옮긴 다음에 처리하는 것으로 변경
            -- 2015.09.16 yngwie 장애 스트링 처리로 변경
	 		-- 2020-06-10 YUNSANG tb_opr_offline_info 정보를 이용하여 offline data는 알람에서 처리하지 않도록 함
            v_sql := FORMAT('
            WITH OFF AS (
                SELECT *
                FROM tb_opr_offline_info
                WHERE type = ''TM''
            ), A AS (
                SELECT  	A.DEVICE_ID   
		                    , B.DEVICE_TYPE_CD  
		                    , A.COLEC_DT 
		                    , A.COL126 
		                    , FN_GET_CHAR_COUNT(A.COL126, ''$'') AS POS
                FROM 	(   
                        SELECT     ROW_NUMBER() OVER (PARTITION BY A.DEVICE_ID ORDER BY A.COLEC_DT DESC ) SEQ   
		                            , A.DEVICE_ID 
		                            , A.COLEC_DT 
		                            , A.COL126 
                        FROM		TB_RAW_EMS_INFO_TMP A  
                        WHERE   (A.COL126 IS NULL OR A.COL126 <> ''NA'')  
                     ) A, TB_BAS_DEVICE_PMS B
                WHERE   A.SEQ = 1    
                    AND 	A.DEVICE_ID = B.DEVICE_ID  
                       AND NOT EXISTS (SELECT 1 FROM OFF WHERE A.DEVICE_ID = OFF.DEVICE_ID)
                     UNION
                     SELECT A.DEVICE_ID
                          , B.DEVICE_TYPE_CD
                          , A.COLEC_DT
                          , A.COL126
                          , FN_GET_CHAR_COUNT(A.COL126, ''$'') AS POS
                     FROM (
                         SELECT ROW_NUMBER() OVER (PARTITION BY A.DEVICE_ID ORDER BY A.COLEC_DT DESC ) SEQ
                              , A.DEVICE_ID
                              , A.COLEC_DT
                              , A.COL126
                         FROM TB_RAW_EMS_INFO_TMP A
                         WHERE (A.COL126 IS NULL OR A.COL126 <> ''NA'')
                     ) A, TB_BAS_DEVICE_PMS B, OFF
                     WHERE A.SEQ = 1
                       AND A.DEVICE_ID = B.DEVICE_ID
                       AND (A.device_id = OFF.device_id AND A.colec_dt > OFF.end_tm)
            ) 
            INSERT INTO TB_OPR_ALARM_TMP_RAW (
                ALARM_ID 
           --     , ALARM_ID_SEQ 
                , DEVICE_ID 
                , DEVICE_TYPE_CD 
           --     , ALARM_TYPE_CD 
                , ALARM_CD 
                , EVENT_DT 
                , ERR_OCUR 
                , POS 
           --     , INS_FLAG 
                , CREATE_DT 
			)  
            SELECT	*
            FROM (  
	            SELECT  
		                 CONCAT( CONCAT( CONCAT(''ALM_'' , A.DEVICE_ID) , ''_'' ), A.COLEC_DT) AS ALARM_ID  
		         --       , NULL AS ALARM_ID_SEQ  
		                , A.DEVICE_ID  
		                , A.DEVICE_TYPE_CD  
		        --        , NULL AS ALARM_TYPE_CD  
		                , (CASE WHEN LENGTH(FN_GET_SPLIT(A.COL126::TEXT, B.LVL::INT4, ''$''::TEXT)) > 4 THEN NULL ELSE FN_GET_SPLIT(A.COL126::TEXT, B.LVL::INT4, ''$''::TEXT) END) AS ALARM_CD 
		                , A.COLEC_DT AS EVENT_DT  
		                , 1 AS ERR_OCUR  
		                , A.POS  
		         --       , NULL AS INS_FLAG  
		                , TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'') AS CREATE_DT  
	            FROM  	A 
			                , (	SELECT LEVEL AS LVL  
			                	FROM	GENERATE_SERIES(1, ( SELECT MAX(POS) FROM A ) ) level 
			                ) B
	            WHERE 	LENGTH(A.COL126) > 0 
            ) T
            WHERE ALARM_CD IS NOT NULL ;'
            , v_handleFlag);
           
            EXECUTE v_sql;
           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;
--			RAISE NOTICE 'i_sp_nm[%] Insert TB_OPR_ALARM_TMP_RAW ALARM [%]건 SQL[%]', i_sp_nm, v_work_num, v_sql;
                        
 	 		v_rslt_cd := 'E';
             
            -- 2015.09.03 yngwie 모델별 에러코드 적용 관련
            -- TB_BAS_MODEL_ALARM_CODE_MAP 에 등록되어 있는 장애들만 임시 테이블로 옮긴다.
            WITH ESS AS ( 
                SELECT   A.* 
                    		, SUBSTR(B.PRODUCT_MODEL_NM, 1, 7) AS PRODUCT_MODEL_TYPE_CD 
                FROM 	TB_OPR_ALARM_TMP_RAW A 
                    		, TB_BAS_DEVICE_PMS B
                WHERE   A.DEVICE_ID = B.DEVICE_ID 
                    AND 	A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD 
            ) 
            INSERT INTO TB_OPR_ALARM_TMP ( 
                 ALARM_ID  
                , ALARM_ID_SEQ  
                , DEVICE_ID  
                , DEVICE_TYPE_CD  
                , ALARM_TYPE_CD  
                , ALARM_CD  
                , EVENT_DT  
                , ERR_OCUR  
                , POS  
                , INS_FLAG  
                , CREATE_DT 
            ) 
            SELECT 
                 B.ALARM_ID 
                , B.ALARM_ID_SEQ 
                , B.DEVICE_ID 
                , B.DEVICE_TYPE_CD 
                , A.ALARM_TYPE_CD 
                , (CASE WHEN LENGTH(B.ALARM_CD) > 4 THEN NULL ELSE B.ALARM_CD END) AS ALARM_CD 
                , B.EVENT_DT 
                , B.ERR_OCUR 
                , B.POS 
                , B.INS_FLAG 
                , B.CREATE_DT 
            FROM		TB_BAS_MODEL_ALARM_CODE_MAP A 
                		, ESS B 
            WHERE	A.PRODUCT_MODEL_TYPE_CD like concat(B.PRODUCT_MODEL_TYPE_CD, '%')
            AND 		A.ALARM_CD = B.ALARM_CD ;

           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--			RAISE NOTICE 'i_sp_nm[%] Insert TB_OPR_ALARM_TMP ALARM [%]건', i_sp_nm, v_work_num;
                                
		
            -- TEMP 테이블에 TB_RAW_LOGIN 테이블의 레코드를 확인해서 연결장애 발생/미발생 데이터도 추가한다
            -- 2014-08-01  연결장애 발생 후 한시간 이상 지난 장비만 연결장애로 처리하도록 변경
            -- 2015. ?? 연결장애 발생 후 7분이 지난 장비만 연결장애로 처리하도록 변경
            -- sleep 모드는 장애로 넣으면 안된다. 20160105
            
   	 		v_rslt_cd := 'F';
   
   	 		v_sql := FORMAT('
            INSERT INTO TB_OPR_ALARM_TMP (
                 ALARM_ID 
--                , ALARM_ID_SEQ 
                , DEVICE_ID 
                , DEVICE_TYPE_CD 
                , ALARM_TYPE_CD 
                , ALARM_CD 
                , EVENT_DT 
                , ERR_OCUR 
                , POS 
--                , INS_FLAG 
                , CREATE_DT 
            )          
            SELECT 
                CONCAT( CONCAT( CONCAT(''ALM_'' , A.DEVICE_ID) , ''_'' ), TO_CHAR(A.CREATE_DT, ''YYYYMMDDHH24MISS'')) AS ALARM_ID  
 --               , NULL AS ALARM_ID_SEQ 
                , A.DEVICE_ID    
                , B.DEVICE_TYPE_CD    
                , ''1'' AS ALARM_TYPE_CD 	-- 2015.09.03 yngwie, 연결장애는 빈번히 발생하므로 ALARM_TYPE_CD 를 1 (Notification) 으로 변경
                , ''C128'' AS ALARM_CD  		-- 2015.09.03 yngwie, 연결장애는 C128 로 통일 (모델별 에러코드 적용 관련)
                , TO_CHAR(A.CREATE_DT, ''YYYYMMDDHH24MISS'') AS EVENT_DT 
                , CASE WHEN A.RESPONSE_CD <> ''0'' AND A.GAP > 7 THEN 1 ELSE 0 END AS ERR_OCUR 
                , 0 AS POS 
--                , NULL AS INS_FLAG 
                , TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'')  AS CREATE_DT 
            FROM (    
                SELECT     
                    ROW_NUMBER() OVER (PARTITION BY A.DEVICE_ID ORDER BY A.CREATE_DT DESC, A.CREATE_TM DESC, A.DATA_SEQ DESC) SEQ    
                    , A.DEVICE_ID    
                    , A.CREATE_DT    
                    , A.RESPONSE_CD    
                    , EXTRACT(DAY FROM TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'') - CREATE_DT) * 24 * 60 +   
                     EXTRACT(HOUR FROM TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'') - CREATE_DT) * 60 +    
                     EXTRACT(MINUTE FROM TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'') - CREATE_DT)  GAP    
                FROM 	TB_RAW_LOGIN A 
                WHERE 	A.CREATE_DT BETWEEN TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'')- INTERVAL ''1 DAY'' AND TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'')  
            ) A 
            , TB_BAS_DEVICE_PMS B
            WHERE  	A.SEQ = 1		-- 2016.01.11 Slepp 모드는 연결 장애에서 제외 시킨다. jkm1020 => NOT IN 추가  
                AND 	A.DEVICE_ID = B.DEVICE_ID 
                AND 	A.RESPONSE_CD <> ''0'' AND A.GAP > 7 
                AND 	A.DEVICE_ID NOT IN (SELECT DEVICE_ID FROM TB_OPR_ESS_STATE WHERE OPER_STUS_CD =''5'') ; ',
                v_handleFlag, v_handleFlag, v_handleFlag, v_handleFlag, v_handleFlag, v_handleFlag
            );

           	EXECUTE v_sql;
           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--			RAISE NOTICE 'i_sp_nm[%] Insert TB_OPR_ALARM_TMP ALARM [%]건 SQL[%]', i_sp_nm, v_work_num, v_sql;
            
   	 		v_rslt_cd := 'G';

   	 		v_ExeCnt1 := 0;
   	 	
   	 		SELECT 	COUNT(*) 
   	 		INTO 		v_ExeCnt1
   	 		WHERE EXISTS (SELECT * FROM TB_OPR_ALARM_TMP WHERE ERR_OCUR = 1);
   	 	
           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--			RAISE NOTICE 'i_sp_nm[%] Check ALARM Records Exists [%]건', i_sp_nm, v_work_num;
            
            IF v_ExeCnt1 > 0 then
      	 		v_rslt_cd := 'J';

                -- 2015.06.24 연결장애는 EVENT_DT 를 TB_RAW_EMS_INFO 레코드 중 가장 최근의 COLEC_DT로  처리하도록 변경
                v_sql := FORMAT('
                WITH B AS ( 
                    SELECT 	MAX(B.COLEC_DT) AS COLEC_DT 
		                        , B.DEVICE_ID 
		                        , B.DEVICE_TYPE_CD 
                    FROM 	TB_OPR_ALARM_TMP A,
								TB_OPR_ESS_STATE_HIST B 
					WHERE	A.ALARM_CD = ''C128''  -- 2015.09.03 yngwie, 연결장애는 C128 로 통일 (모델별 에러코드 적용 관련)
                    AND 		A.ERR_OCUR = 1 
                    AND 		A.DEVICE_ID = B.DEVICE_ID 
                    AND 		A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD
                    AND 		B.COLEC_DT BETWEEN TO_CHAR(TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'') - INTERVAL ''1 DAY'', ''YYYYMMDDHH24MISS'') AND ''%s'' 
                    GROUP BY B.DEVICE_ID, B.DEVICE_TYPE_CD 
                ) 
                UPDATE  TB_OPR_ALARM_TMP A
                    SET 	EVENT_DT = B.COLEC_DT 
		      	FROM B
				WHERE	A.DEVICE_ID = B.DEVICE_ID 
                    AND 	A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD  ;	'
                ,	v_handleFlag, v_handleFlag);
                
            	EXECUTE v_sql;
	           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--				RAISE NOTICE 'i_sp_nm[%] Merge TB_OPR_ALARM_TMP from TB_OPR_ESS_STATE_HIST Records : [%]건 SQL[%]', i_sp_nm, v_work_num, v_sql;

	   	 		v_rslt_cd := 'H';
   	 			v_ExeCnt2 := 0;
   	 		
	            -- 새로운 장애를 장애 테이블에 추가한다.
	            WITH S AS (
	   				SELECT 	  ALARM_ID 
					            , ROW_NUMBER() OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY ALARM_CD) AS ALARM_ID_SEQ 
					            , DEVICE_ID 
					            , DEVICE_TYPE_CD 
					            , ALARM_TYPE_CD 
					            , ALARM_CD 
					            , EVENT_DT 
					FROM 	TB_OPR_ALARM_TMP 
					WHERE 	ERR_OCUR = 1
					    AND 	(DEVICE_ID, DEVICE_TYPE_CD, ALARM_TYPE_CD, ALARM_CD) NOT IN ( 
					        SELECT DEVICE_ID  
					            	, DEVICE_TYPE_CD  
					            	, ALARM_TYPE_CD  
					            	, ALARM_CD  
					        FROM TB_OPR_ALARM ) 
				)
	            INSERT INTO TB_OPR_ALARM (ALARM_ID, ALARM_ID_SEQ, DEVICE_ID, DEVICE_TYPE_CD, ALARM_TYPE_CD, ALARM_CD, EVENT_DT)
				SELECT 	  ALARM_ID, ALARM_ID_SEQ, DEVICE_ID, DEVICE_TYPE_CD, ALARM_TYPE_CD
							, (CASE WHEN LENGTH(ALARM_CD) > 4 THEN NULL ELSE ALARM_CD END) AS ALARM_CD, EVENT_DT 
				FROM S   ; 
               
		      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		      	v_ExeCnt2 := v_ExeCnt2+v_work_num;
		        v_rslt_cnt := v_rslt_cnt + v_work_num;

--			  RAISE notice 'i_sp_nm[%] Add New ALARM Records :[%]건', i_sp_nm, v_ExeCnt2;
            
            END IF;
            
           	-- modified by stjoo(20160805) : 주석처리
			--  데이타 이전작업 완료로 PK충돌이 발생할 소지가 없으므로 속도향상을 위해 INSERT 구문으로 변경
           	v_rslt_cd := 'I';

           	v_ExeCnt1 := 0;
			v_ExeCnt2 := 0;     

	        -- 2015.09.19 yngwie 해지된 알람 조회
	        WITH B AS (
	            SELECT	 DEVICE_ID 
			                , DEVICE_TYPE_CD 
			                , ALARM_TYPE_CD 
			                , ALARM_CD 
	            FROM   TB_OPR_ALARM
	            EXCEPT
	            SELECT
			                DEVICE_ID 
			                , DEVICE_TYPE_CD 
			                , ALARM_TYPE_CD 
			                , ALARM_CD 
	            FROM 	TB_OPR_ALARM_TMP
	            WHERE	ERR_OCUR = 1
	        ), EMS AS (
	            SELECT        DEVICE_ID
				                , MAX(COLEC_DT) AS COLEC_DT
	            FROM         TB_RAW_EMS_INFO_TMP
	            GROUP BY    DEVICE_ID
	        ), R as (
		        SELECT 
		            A.ALARM_ID 
		            , A.ALARM_ID_SEQ 
		            , A.DEVICE_ID 
		            , A.DEVICE_TYPE_CD 
		            , A.ALARM_TYPE_CD 
		            , A.ALARM_CD 
		            , A.EVENT_DT 
		            , EMS.COLEC_DT AS CLEAR_DT 
		            , 'N' AS MANU_RECV_FLAG 
		            , NULL AS MANU_RECV_USER_ID 
		            , NULL AS MANU_RECV_DESC 
		            , v_baseTm AS CREATE_DT 
		        FROM 	TB_OPR_ALARM A
				            , B
				            , EMS
		        WHERE
		            A.DEVICE_ID = B.DEVICE_ID 
		            AND A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD 
		            AND A.ALARM_TYPE_CD = B.ALARM_TYPE_CD 
		            AND A.ALARM_CD = B.ALARM_CD 
		            AND EMS.DEVICE_ID = A.DEVICE_ID
	    	
			), ins as (
--				RAISE notice 'i_sp_nm[%] Clear ALARM Records, DEVICE_ID[%] ALARM_CD[%] EVENT_DT[%] CLEAR_DT[%] ALARM_ID[%] ALARM_ID_SEQT[%]',
--										i_sp_nm, v_row.DEVICE_ID, v_row.ALARM_CD, v_row.EVENT_DT, v_row.CLEAR_DT, v_row.ALARM_ID, v_row.ALARM_ID_SEQ;
                
                -- 해지된 장애는 이력테이블로 옮긴다.
               	INSERT INTO TB_OPR_ALARM_HIST (ALARM_ID, ALARM_ID_SEQ, DEVICE_ID, DEVICE_TYPE_CD, ALARM_TYPE_CD, ALARM_CD, EVENT_DT, CLEAR_DT, MANU_RECV_FLAG)
            	SELECT ALARM_ID
                        , ALARM_ID_SEQ
                        , DEVICE_ID
                        , DEVICE_TYPE_CD
                        , ALARM_TYPE_CD
                        , (CASE WHEN LENGTH(ALARM_CD) > 4 THEN NULL ELSE ALARM_CD END)
                        , EVENT_DT
                        , CLEAR_DT
                        , MANU_RECV_FLAG
                FROM R
                  
               RETURNING *
			), del as (
                -- 해지된 장애는 현황테이블에서 삭제한다
                DELETE FROM TB_OPR_ALARM
                WHERE 	(DEVICE_ID, DEVICE_TYPE_CD, ALARM_TYPE_CD, ALARM_CD) 
                			IN (
		                			SELECT DEVICE_ID, DEVICE_TYPE_CD, ALARM_TYPE_CD, ALARM_CD 
		                			FROM R 
		                		)
		                		
              	RETURNING *
			)
			SELECT SUM(cnt1), SUM(cnt2)
			INTO 	v_ExeCnt1, v_ExeCnt2
			FROM (
			   SELECT 	COUNT(ins.*)::INTEGER cnt1, 0 cnt2
			   FROM 	ins
			UNION ALL
			   SELECT 	0 cnt1, COUNT(del.*)::INTEGER cnt2
			   FROM 	del
			) Z;
			
--			RAISE notice 'i_sp_nm[%] insert TB_OPR_ALARM_HIST : [%]건', i_sp_nm, v_ExeCnt1;
--			RAISE notice 'i_sp_nm[%] delete TB_OPR_ALARM : [%]건', i_sp_nm, v_ExeCnt2;
           
            ------------------------    
            -- 2. 상태정보 데이터 처리
            ------------------------
            v_rslt_cd := 'J';
	        TRUNCATE TABLE TB_OPR_ESS_STATE_TMP;

            -- 대상 레코드들의 CO2와 금액을 계산한 결과를 TEMP 테이블에 Insert
            -- 2014.08.12 E700번대 장애는 사용자에게 보여주지 않도록 수정
            -- 2014.08.20 E900번대와 C999 장애만 사용자에게 보여주도록 수정 => 장애 발생된 상태 그대로 보여주도록 원복함
            INSERT INTO TB_OPR_ESS_STATE_TMP (
	              DEVICE_ID, DEVICE_TYPE_CD, COLEC_DT, OPER_STUS_CD 
	            , PV_PW, PV_PW_H, PV_PW_PRICE, PV_PW_CO2, PV_STUS_CD 
	            , CONS_PW, CONS_PW_H, CONS_PW_PRICE 
	            , CONS_PW_CO2
	            , LOAD_MAIN_PW, LOAD_SUB_PW
	            , BT_PW, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2, BT_DCHRG_PW_H, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2, BT_SOC, BT_REAL_SOC, BT_SOH, BT_STUS_CD
	            , GRID_PW, GRID_OB_PW_H, GRID_OB_PW_PRICE, GRID_OB_PW_CO2, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2, GRID_STUS_CD
	            , PCS_PW, PCS_FD_PW_H, PCS_PCH_PW_H, EMS_OPMODE, PCS_OPMODE1, PCS_OPMODE2, PCS_TGT_PW, FEED_IN_LIMIT, MAX_INVERTER_PW_CD
	            , OUTLET_PW, OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2
	            , CREATE_DT
	            -- [ 2015.07.06 yngwie 수집 컬럼 추가 
	            , PCS_COMM_ERR_RATE, SMTR_COMM_ERR_RATE, M2M_COMM_ERR_RATE, SMTR_TP_CD, SMTR_PULSE_CNT, SMTR_MODL_CD
	            , PV_MAX_PWR1, PV_MAX_PWR2, INSTL_RGN, MMTR_SLV_ADDR, BASICMODE_CD
	            , PCS_FLAG0, PCS_FLAG1, PCS_FLAG2, PCS_OPMODE_CMD1, PCS_OPMODE_CMD2, PV_V1, PV_I1, PV_PW1, PV_V2, PV_I2, PV_PW2
	            , INVERTER_V, INVERTER_I, INVERTER_PW, DC_LINK_V
	            , G_RLY_CNT, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT
	            , BMS_FLAG0, BMS_DIAG0, RACK_V, RACK_I
	            , CELL_MAX_V, CELL_MIN_V, CELL_AVG_V, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T, TRAY_CNT
	            , SYS_ST_CD, BT_CHRG_ST_CD, BT_DCHRG_ST_CD, PV_ST_CD, GRID_ST_CD, SMTR_ST_CD, SMTR_TR_CNTER, SMTR_OB_CNTER
	            , DN_ENABLE, DC_START, DC_END, NC_START, NC_END
	            -- ]            
	           	--[신규모델에 대한 모니터링 추가
	           	, GRID_CODE0, GRID_CODE1, GRID_CODE2, PCON_BAT_TARGETPOWER, GRID_CODE3
	           	, BDC_MODULE_CODE0, BDC_MODULE_CODE1, BDC_MODULE_CODE2, BDC_MODULE_CODE3
	           	, FAULT5_REALTIME_YEAR, FAULT5_REALTIME_MONTH, FAULT5_DAY, FAULT5_HOUR, FAULT5_MINUTE, FAULT5_SECOND, FAULT5_DATAHIGH, FAULT5_DATALOW
	           	, FAULT6_REALTIME_YEAR, FAULT6_REALTIME_MONTH, FAULT6_REALTIME_DAY, FAULT6_REALTIME_HOUR, FAULT6_REALTIME_MINUTE, FAULT6_REALTIME_SECOND, FAULT6_DATAHIGH, FAULT6_DATALOW
	           	, FAULT7_REALTIME_YEAR, FAULT7_REALTIME_MONTH, FAULT7_REALTIME_DAY, FAULT7_REALTIME_HOUR, FAULT7_REALTIME_MINUTE, FAULT7_REALTIME_SECOND, FAULT7_DATAHIGH, FAULT7_DATALOW
	           	, FAULT8_REALTIME_YEAR, FAULT8_REALTIME_MONTH, FAULT8_REALTIME_DAY, FAULT8_REALTIME_HOUR, FAULT8_REALTIME_MINUTE, FAULT8_REALTIME_SECOND, FAULT8_DATAHIGH, FAULT8_DATALOW 
	           	, FAULT9_REALTIME_YEAR, FAULT9_REALTIME_MONTH, FAULT9_REALTIME_DAY, FAULT9_REALTIME_HOUR, FAULT9_REALTIME_MINUTE, FAULT9_REALTIME_SECOND, FAULT9_DATAHIGH, FAULT9_DATALOW
	           	-- 추가 elanth
	           	, IS_CONNECT
	           	--] 
            ) 
            SELECT  
                 A.DEVICE_ID AS DEVICE_ID   
                , B.DEVICE_TYPE_CD AS DEVICE_TYPE_CD   
                , A.COLEC_DT AS COLEC_DT   
                , 0 AS OPER_STUS_CD 
                , A.COL8::NUMERIC AS PV_PW   
                , A.COL17::NUMERIC AS PV_PW_H   
                , TRUNC(ABS(A.COL17::NUMERIC) * ET.UNIT_PRICE * .001, 2) AS PV_PW_PRICE   
                , ABS(A.COL17::NUMERIC) * D.CARB_EMI_QUITY * .001 AS PV_PW_CO2   
                , SUBSTR(A.COL88, 3+8, 1) AS PV_STUS_CD   
                , A.COL7::NUMERIC AS CONS_PW   
                , A.COL16::NUMERIC AS CONS_PW_H   
                , TRUNC(ABS(A.COL16::NUMERIC) * E.UNIT_PRICE * .001, 2) AS CONS_PW_PRICE
                , ABS(A.COL16::NUMERIC) * D.CARB_EMI_QUITY * .001 AS CONS_PW_CO2
                , (COALESCE(FN_GET_CONV_NA(A.COL37), '0'))::NUMERIC AS LOAD_MAIN_PW
                , (COALESCE(FN_GET_CONV_NA(A.COL38), '0'))::NUMERIC AS LOAD_SUB_PW

                , A.COL10::NUMERIC AS BT_PW   
                , A.COL20::NUMERIC AS BT_CHRG_PW_H   
                , TRUNC(ABS(A.COL20::NUMERIC) * E.UNIT_PRICE * .001, 2) AS BT_CHRG_PW_PRICE   
                , ABS(A.COL20::NUMERIC) * D.CARB_EMI_QUITY * .001 AS BT_CHRG_PW_CO2   
                , A.COL21::NUMERIC AS BT_DCHRG_PW_H   
                , TRUNC(ABS(A.COL21::NUMERIC) * E.UNIT_PRICE * .001, 2) AS BT_DCHRG_PW_PRICE   
                , ABS(A.COL21::NUMERIC) * D.CARB_EMI_QUITY * .001 AS BT_DCHRG_PW_CO2   
                , CASE WHEN (A.COL11::NUMERIC >= 1000 OR A.COL11::NUMERIC < -999) THEN -999 ELSE A.COL11::NUMERIC END AS BT_SOC
                , CASE WHEN (M.COL14::NUMERIC >= 1000 OR M.COL14::NUMERIC < -999) THEN -999 ELSE M.COL14::NUMERIC END AS BT_REAL_SOC
                , (COALESCE(FN_GET_CONV_NA(A.COL13), '0'))::NUMERIC AS BT_SOH   
                , CASE   
                     WHEN SUBSTR(A.COL88, 1+8, 1) = '1' THEN '0'  
                     WHEN SUBSTR(A.COL88, 2+8, 1) = '1' THEN '1'  
                     ELSE '2'   
                  END AS BT_STUS_CD   
                , A.COL6::NUMERIC AS GRID_PW   
                , A.COL15::NUMERIC AS GRID_OB_PW_H   
                , TRUNC(ABS(A.COL15::NUMERIC) * E.UNIT_PRICE * .001, 2) AS GRID_OB_PW_PRICE   
                , ABS(A.COL15::NUMERIC) * D.CARB_EMI_QUITY * .001 AS GRID_OB_PW_CO2   
                , A.COL14::NUMERIC AS GRID_TR_PW_H   
                , TRUNC(ABS(A.COL14::NUMERIC) * ET.UNIT_PRICE * .001, 2) AS GRID_TR_PW_PRICE   
                , ABS(A.COL14::NUMERIC) * D.CARB_EMI_QUITY * .001 AS GRID_TR_PW_CO2   
                , CASE   
                     WHEN COALESCE(A.COL6, '0')::NUMERIC > 0 THEN '0'
                     WHEN COALESCE(A.COL6, '0')::NUMERIC = 0 THEN '2'
                     ELSE '1'  
                  END GRID_STUS_CD   
                , A.COL9::NUMERIC AS PCS_PW   
                , A.COL18::NUMERIC AS PCS_FD_PW_H   
                , A.COL19::NUMERIC AS PCS_PCH_PW_H  
                , A.COL0 AS EMS_OPMODE    
                , A.COL1 AS PCS_OPMODE1   
                , A.COL2 AS PCS_OPMODE2   
                , A.COL3::NUMERIC AS PCS_TGT_PW   
                , A.COL331 AS FEED_IN_LIMIT   
                , A.COL332 AS MAX_INVERTER_PW_CD 
            	-- [ 2015.05.13 yngwie OUTLET_PW 추가
               	, FN_GET_CONV_NA(A.COL5)::NUMERIC AS OUTLET_PW   
               	, FN_GET_CONV_NA(A.COL22)::NUMERIC AS OUTLET_PW_H   
               	, TRUNC(ABS(FN_GET_CONV_NA(A.COL22)::NUMERIC) * E.UNIT_PRICE * .001, 2) AS OUTLET_PW_PRICE   
               	, ABS(FN_GET_CONV_NA(A.COL22)::NUMERIC) * D.CARB_EMI_QUITY * .001 AS OUTLET_PW_CO2   
            	-- ]
               	, v_baseTm AS CREATE_DT    
            	-- [ 2015.07.06 yngwie 수집 컬럼 추가
				, FN_GET_CONV_NA(A.COL34)::NUMERIC AS PCS_COMM_ERR_RATE 
				, FN_GET_CONV_NA(A.COL35)::NUMERIC AS SMTR_COMM_ERR_RATE 
				, FN_GET_CONV_NA(A.COL36)::NUMERIC AS M2M_COMM_ERR_RATE          
				, A.COL324 AS SMTR_TP_CD
				, FN_GET_CONV_NA(A.COL325)::NUMERIC AS SMTR_PULSE_CNT 
				, A.COL326 AS SMTR_MODL_CD
				, FN_GET_CONV_NA(A.COL328)::NUMERIC AS PV_MAX_PWR1 
				, FN_GET_CONV_NA(A.COL329)::NUMERIC AS PV_MAX_PWR2 
				, A.COL330 AS INSTL_RGN
				, FN_GET_CONV_NA(A.COL334)::NUMERIC AS MMTR_SLV_ADDR 
				, A.COL335 AS BASICMODE_CD 
	            , P.COL0                                                       AS PCS_FLAG0 
	            , P.COL1                                                       AS PCS_FLAG1 
	            , P.COL2                                                       AS PCS_FLAG2 
	            -- ]
	            -- [ 2015.07.13 modified by stjoo : P.COL20s value was NA on test ems.
              	, FN_GET_CONV_NA(P.COL20)                             AS PCS_OPMODE_CMD1 
              	, P.COL21                                                      AS PCS_OPMODE_CMD2 
              	, FN_GET_CONV_NA(P.COL38)::NUMERIC   AS PV_V1 
              	, FN_GET_CONV_NA(P.COL39)::NUMERIC   AS PV_I1 
				, CASE WHEN FN_GET_CONV_NA(P.COL40)::NUMERIC >= 100000 OR FN_GET_CONV_NA(P.COL40)::NUMERIC <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL40)::NUMERIC END AS PV_PW1  
				, FN_GET_CONV_NA(P.COL42)::NUMERIC   AS PV_V2 
				, FN_GET_CONV_NA(P.COL43)::NUMERIC   AS PV_I2 
				, CASE WHEN FN_GET_CONV_NA(P.COL44)::NUMERIC >= 100000 OR FN_GET_CONV_NA(P.COL44)::NUMERIC <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL44)::NUMERIC END AS PV_PW2  
				, CASE WHEN FN_GET_CONV_NA(P.COL53)::NUMERIC >= 100000 OR FN_GET_CONV_NA(P.COL53)::NUMERIC <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL53)::NUMERIC END AS INVERTER_V 
				, CASE WHEN FN_GET_CONV_NA(P.COL54)::NUMERIC >= 10000 OR FN_GET_CONV_NA(P.COL54)::NUMERIC <= -10000 THEN 0 ELSE FN_GET_CONV_NA(P.COL54)::NUMERIC END AS INVERTER_I 
				, FN_GET_CONV_NA(P.COL55)::NUMERIC   AS INVERTER_PW 
				, CASE WHEN FN_GET_CONV_NA(P.COL59)::NUMERIC >= 100000 OR FN_GET_CONV_NA(P.COL59)::NUMERIC <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL59)::NUMERIC END AS DC_LINK_V 
				, FN_GET_CONV_NA(P.COL61)::NUMERIC   AS G_RLY_CNT 
				, FN_GET_CONV_NA(P.COL62)::NUMERIC   AS BAT_RLY_CNT 
				, FN_GET_CONV_NA(P.COL63)::NUMERIC   AS GRID_TO_GRID_RLY_CNT 
				, FN_GET_CONV_NA(P.COL64)::NUMERIC   AS BAT_PCHRG_RLY_CNT   
				, M.COL0                               AS BMS_FLAG0 
				, M.COL1                               AS BMS_DIAG0 
				, FN_GET_CONV_NA(M.COL16)::NUMERIC   AS RACK_V 
				, FN_GET_CONV_NA(M.COL17)::NUMERIC   AS RACK_I 
				, FN_GET_CONV_NA(M.COL18)::NUMERIC   AS CELL_MAX_V 
				, FN_GET_CONV_NA(M.COL19)::NUMERIC   AS CELL_MIN_V 
				, FN_GET_CONV_NA(M.COL20)::NUMERIC   AS CELL_AVG_V 
				, FN_GET_CONV_NA(M.COL21)::NUMERIC   AS CELL_MAX_T 
				, FN_GET_CONV_NA(M.COL22)::NUMERIC   AS CELL_MIN_T 
				, FN_GET_CONV_NA(M.COL23)::NUMERIC   AS CELL_AVG_T 
				, FN_GET_CONV_NA(M.COL24)::NUMERIC   AS TRAY_CNT 
				, CASE WHEN SUBSTR(A.COL88, 4+8, 1) = '0' AND SUBSTR(A.COL88, 8+8, 1) = '1' THEN '1' ELSE '0' END AS SYS_ST_CD
				, SUBSTR(A.COL89, 7+8, 1) AS BT_CHRG_ST_CD 
				, SUBSTR(A.COL89, 6+8, 1) AS BT_DCHRG_ST_CD 
				, SUBSTR(A.COL89, 8+8, 1) AS PV_ST_CD 
				, SUBSTR(A.COL89, 5+8, 1) AS GRID_ST_CD 
				, SUBSTR(A.COL89, 4+8, 1) AS SMTR_ST_CD 
				, A.COL127::NUMERIC AS SMTR_TR_CNTER 
				, A.COL128::NUMERIC AS SMTR_OB_CNTER 
				, A.COL337 AS DN_ENABLE 
				, A.COL338 AS DC_START 
				, A.COL339 AS DC_END 
				, A.COL340 AS NC_START 
				, A.COL341 AS NC_END 
            	-- ]     
				--[신규모델로 모니터링 추가
	            , CASE WHEN P.COL3  ='NA' THEN NULL ELSE P.COL3   END as GRID_CODE0 
	            , CASE WHEN P.COL4  ='NA' THEN NULL ELSE P.COL4   END as GRID_CODE1 
	            , CASE WHEN P.COL5  ='NA' THEN NULL ELSE P.COL5   END as GRID_CODE2 
	            , CASE WHEN P.COL19 ='NA' THEN NULL ELSE P.COL19  END as PCON_BAT_TARGETPOWER 
	            , CASE WHEN P.COL31 ='NA' THEN NULL ELSE P.COL31  END as GRID_CODE3 
	            , CASE WHEN P.COL32 ='NA' THEN NULL ELSE P.COL32  END as BDC_MODULE_CODE0 
	            , CASE WHEN P.COL33 ='NA' THEN NULL ELSE P.COL33  END as BDC_MODULE_CODE1 
	            , CASE WHEN P.COL34 ='NA' THEN NULL ELSE P.COL34  END as BDC_MODULE_CODE2 
	            , CASE WHEN P.COL35 ='NA' THEN NULL ELSE P.COL35  END as BDC_MODULE_CODE3 
	            , CASE WHEN P.COL36 ='NA' THEN NULL ELSE P.COL36  END as FAULT5_REALTIME_YEAR 
	            , CASE WHEN P.COL37 ='NA' THEN NULL ELSE P.COL37  END as FAULT5_REALTIME_MONTH 
	            , CASE WHEN P.COL87 ='NA' THEN NULL ELSE P.COL87  END as FAULT5_DAY 
	            , CASE WHEN P.COL88 ='NA' THEN NULL ELSE P.COL88  END as FAULT5_HOUR 
	            , CASE WHEN P.COL89 ='NA' THEN NULL ELSE P.COL89  END as FAULT5_MINUTE 
	            , CASE WHEN P.COL90 ='NA' THEN NULL ELSE P.COL90  END as FAULT5_SECOND 
	            , CASE WHEN P.COL91 ='NA' THEN NULL ELSE P.COL91  END as FAULT5_DATAHIGH 
	            , CASE WHEN P.COL162='NA' THEN NULL ELSE P.COL162 END as FAULT5_DATALOW 
	            , CASE WHEN P.COL163='NA' THEN NULL ELSE P.COL163 END as FAULT6_REALTIME_YEAR 
	            , CASE WHEN P.COL164='NA' THEN NULL ELSE P.COL164 END as FAULT6_REALTIME_MONTH 
	            , CASE WHEN P.COL165='NA' THEN NULL ELSE P.COL165 END as FAULT6_REALTIME_DAY 
	            , CASE WHEN P.COL166='NA' THEN NULL ELSE P.COL166 END as FAULT6_REALTIME_HOUR 
	            , CASE WHEN P.COL167='NA' THEN NULL ELSE P.COL167 END as FAULT6_REALTIME_MINUTE 
	            , CASE WHEN P.COL168='NA' THEN NULL ELSE P.COL168 END as FAULT6_REALTIME_SECOND 
	            , CASE WHEN P.COL169='NA' THEN NULL ELSE P.COL169 END as FAULT6_DATAHIGH 
	            , CASE WHEN P.COL170='NA' THEN NULL ELSE P.COL170 END as FAULT6_DATALOW 
	            , CASE WHEN P.COL171='NA' THEN NULL ELSE P.COL171 END as FAULT7_REALTIME_YEAR 
	            , CASE WHEN P.COL177='NA' THEN NULL ELSE P.COL177 END as FAULT7_REALTIME_MONTH 
	            , CASE WHEN P.COL178='NA' THEN NULL ELSE P.COL178 END as FAULT7_REALTIME_DAY 
	            , CASE WHEN P.COL179='NA' THEN NULL ELSE P.COL179 END as FAULT7_REALTIME_HOUR 
	            , CASE WHEN P.COL187='NA' THEN NULL ELSE P.COL187 END as FAULT7_REALTIME_MINUTE 
	            , CASE WHEN P.COL188='NA' THEN NULL ELSE P.COL188 END as FAULT7_REALTIME_SECOND 
	            , CASE WHEN P.COL189='NA' THEN NULL ELSE P.COL189 END as FAULT7_DATAHIGH 
	            , CASE WHEN P.COL191='NA' THEN NULL ELSE P.COL191 END as FAULT7_DATALOW 
	            , CASE WHEN P.COL192='NA' THEN NULL ELSE P.COL192 END as FAULT8_REALTIME_YEAR 
	            , CASE WHEN P.COL193='NA' THEN NULL ELSE P.COL193 END as FAULT8_REALTIME_MONTH 
	            , CASE WHEN P.COL194='NA' THEN NULL ELSE P.COL194 END as FAULT8_REALTIME_DAY 
	            , CASE WHEN P.COL195='NA' THEN NULL ELSE P.COL195 END as FAULT8_REALTIME_HOUR 
	            , CASE WHEN P.COL196='NA' THEN NULL ELSE P.COL196 END as FAULT8_REALTIME_MINUTE 
	            , CASE WHEN P.COL197='NA' THEN NULL ELSE P.COL197 END as FAULT8_REALTIME_SECOND 
	            , CASE WHEN P.COL198='NA' THEN NULL ELSE P.COL198 END as FAULT8_DATAHIGH 
	            , CASE WHEN P.COL199='NA' THEN NULL ELSE P.COL199 END as FAULT8_DATALOW 
	            , CASE WHEN P.COL200='NA' THEN NULL ELSE P.COL200 END as FAULT9_REALTIME_YEAR 
	            , CASE WHEN P.COL201='NA' THEN NULL ELSE P.COL201 END as FAULT9_REALTIME_MONTH 
	            , CASE WHEN P.COL202='NA' THEN NULL ELSE P.COL202 END as FAULT9_REALTIME_DAY 
	            , CASE WHEN P.COL203='NA' THEN NULL ELSE P.COL203 END as FAULT9_REALTIME_HOUR 
	            , CASE WHEN P.COL204='NA' THEN NULL ELSE P.COL204 END as FAULT9_REALTIME_MINUTE 
	            , CASE WHEN P.COL205='NA' THEN NULL ELSE P.COL205 END as FAULT9_REALTIME_SECOND 
	            , CASE WHEN P.COL206='NA' THEN NULL ELSE P.COL206 END as FAULT9_DATAHIGH 
	            , CASE WHEN P.COL207='NA' THEN NULL ELSE P.COL207 END as FAULT9_DATALOW 
	            , true AS IS_CONNECT
	            --]
            FROM  	 TB_RAW_EMS_INFO_TMP A
		                , TB_RAW_PCS_INFO_TMP P   
		                , TB_RAW_BMS_INFO_TMP M   
		                , TB_BAS_DEVICE_PMS B
		                , TB_PLF_CARB_EMI_QUITY D   
		                , TB_BAS_ELPW_UNIT_PRICE E 
		                , TB_BAS_ELPW_UNIT_PRICE ET 
                        , TB_BAS_SITE S
            WHERE  A.DEVICE_ID = P.DEVICE_ID
                AND A.COLEC_DT = P.COLEC_DT   
                AND A.DEVICE_ID = M.DEVICE_ID   
                AND A.COLEC_DT = M.COLEC_DT   
                AND A.DEVICE_ID = B.DEVICE_ID   
                AND B.site_id = S.site_id
                AND S.cntry_cd = D.cntry_cd
--                 AND B.CNTRY_CD = D.CNTRY_CD
                AND D.USE_FLAG = 'Y'
                AND B.ELPW_PROD_CD = E.ELPW_PROD_CD   
                AND E.IO_FLAG = 'O'
                AND E.USE_FLAG = 'Y'
            -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가
                AND E.WKDAY_FLAG = FN_GET_WKDAY_FLAG(SUBSTR(A.COLEC_DT, 1, 8)) 
            -- 2015.07.31 요금제 년월 변경
            --  AND SUBSTR(A.COLEC_DT, 1, 6) = E.UNIT_YM   
                AND E.UNIT_YM = '000000'
                AND SUBSTR(A.COLEC_DT, 9, 2) = E.UNIT_HH 
                AND B.ELPW_PROD_CD = ET.ELPW_PROD_CD  
                AND ET.IO_FLAG = 'T'   
                AND ET.USE_FLAG = 'Y'  
            -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가, T 는  1 만 있음
            -- 2015.08.06 modified by yngwie T 도 주말 0 추가
            --  AND ET.WKDAY_FLAG = '1'   
                AND ET.WKDAY_FLAG = FN_GET_WKDAY_FLAG(SUBSTR(A.COLEC_DT, 1, 8))
            -- 2015.07.31 요금제 년월 변경
            --  AND SUBSTR(A.COLEC_DT, 1, 6) = ET.UNIT_YM  
                AND ET.UNIT_YM = '000000'   
                AND SUBSTR(A.COLEC_DT, 9, 2) = ET.UNIT_HH
            ;

	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--	      	RAISE notice 'i_sp_nm[%] Insert TB_OPR_ESS_STATE_TMP Table :  [%]건', i_sp_nm, v_work_num;
            
            -- 발생한 ALARM_TYPE_CD 를  가지고 OPER_STUS_CD 를 업데이트 한다.
            -- 2014.08.12 E700번대 장애는 사용자에게 보여주지 않도록 수정
            -- 2014.08.20 E900번대와 C999 장애만 사용자에게 보여주도록 수정 => 장애 발생된 상태 그대로 보여주도록 원복함
            -- [ 2015.01.07 
--                SELECT * FROM TB_PLF_CODE_INFO WHERE GRP_CD = 'OPER_STUS_CD';
--                OPER_STUS_CD = 0 : 운전중
--                OPER_STUS_CD = 1 : 경고
--                OPER_STUS_CD = 2 : 오류
--                OPER_STUS_CD = 3 : 통신불가
--                OPER_STUS_CD = 4 : 미인증
--               ] 
	      
           	v_rslt_cd := 'K';

	      	v_ExeCnt1 := 0;
			v_ExeCnt2 := 0;
			v_ExeCnt3 := 0;

		
            WITH ERR_INFO AS (  
                SELECT  
                    DEVICE_ID   
                    , DEVICE_TYPE_CD   
                    , CASE    
                        --주석처리 elnath 임시 해재
--                         WHEN MAX(ALARM_TYPE_CD::INTEGER) = 1 AND ALARM_CD = 'C128' THEN '3'
                        WHEN MAX(ALARM_TYPE_CD::INTEGER) = 3 THEN '2' 
--                      WHEN MAX(ALARM_TYPE_CD::INTEGER) IN (1, 2) AND ALARM_CD <> 'C128' THEN '1'
                        WHEN MAX(ALARM_TYPE_CD::INTEGER) = 2 THEN '1'
                        ELSE '0'
                      END AS OPER_STUS_CD
                     --추가 elnath
                    , CASE
                        WHEN MAX(ALARM_TYPE_CD::INTEGER) = 1 AND ALARM_CD = 'C128' THEN false ELSE true
                      END AS IS_CONNECT
                    , MAX(EVENT_DT) AS EVENT_DT
                FROM TB_OPR_ALARM 
            --E202오류의 경우 사용자에게 보여주지 않도록 한다.
                WHERE ALARM_CD NOT IN ('E202')  
                GROUP BY   DEVICE_ID, DEVICE_TYPE_CD, ALARM_CD
             ), DEV as (
	            SELECT 	 DEVICE_ID 
			                , DEVICE_TYPE_CD 
			                , MAX(OPER_STUS_CD) AS OPER_STUS_CD 
			                , MAX(EVENT_DT) AS EVENT_DT 
	                        , bool_and(IS_CONNECT) AS IS_CONNECT
	            FROM 	ERR_INFO 
	            GROUP BY  DEVICE_ID, DEVICE_TYPE_CD
             ), U1 as (
				-- 통신 단절인 장비의 경우 1분 데이터가 수집되지 않으므로 상태를 현황 임시 테이블에도 Update 한다.
				UPDATE TB_OPR_ESS_STATE_TMP U
				SET 		OPER_STUS_CD = DEV.OPER_STUS_CD 
                          , IS_CONNECT = DEV.IS_CONNECT
				FROM 	DEV
				WHERE 	U.DEVICE_ID = DEV.DEVICE_ID
				AND 		U.DEVICE_TYPE_CD =DEV.DEVICE_TYPE_CD
				
				returning *
             ), U2 as (
	            -- 통신 단절인 장비는 현황 임시 테이블에 데이터가 수집되지 않을 수 있으니 현황 테이블에도 Update 한다.
	            UPDATE 	TB_OPR_ESS_STATE U
	            SET 		OPER_STUS_CD = DEV.OPER_STUS_CD
	                       , IS_CONNECT = DEV.IS_CONNECT
				FROM 	DEV
	            WHERE 	U.DEVICE_ID = DEV.DEVICE_ID
	            AND 		U.DEVICE_TYPE_CD = DEV.DEVICE_TYPE_CD

				returning *
             ), U3 as (
	            UPDATE 	TB_OPR_ESS_STATE_HIST U
	            SET 		OPER_STUS_CD = DEV.OPER_STUS_CD
	                        , IS_CONNECT = DEV.IS_CONNECT
				FROM 	DEV
	            WHERE 	U.DEVICE_ID = DEV.DEVICE_ID
	            AND 		U.DEVICE_TYPE_CD = DEV.DEVICE_TYPE_CD
	            AND 		U.COLEC_DT = DEV.EVENT_DT

				returning *
             )
             SELECT SUM(cnt1), SUM(cnt2), SUM(cnt3)
             INTO 	v_ExeCnt1, v_ExeCnt2, v_ExeCnt3
             FROM (
	            SELECT 	COUNT(U1.*)::INTEGER cnt1, 0 cnt2, 0 cnt3
	            FROM 	U1
				UNION ALL
	            SELECT 	0 cnt1, COUNT(U2.*)::INTEGER cnt2, 0 cnt3
	            FROM 	U2
				UNION 	ALL
	            SELECT 	0 cnt1, 0 cnt2, COUNT(U3.*)::INTEGER cnt3
	            FROM 	U3
			) Z;
            
            GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		
--	      	RAISE NOTICE 'i_sp_nm[%] Update TB_OPR_ESS_STATE_TMP Table For OPER_STUS_CD : [%]', i_sp_nm, v_ExeCnt1;
--	      	RAISE NOTICE 'i_sp_nm[%] Update TB_OPR_ESS_STATE Table For OPER_STUS_CD : [%]', i_sp_nm, v_ExeCnt2;
--	      	RAISE NOTICE 'i_sp_nm[%] Update TB_OPR_ESS_STATE_HIST Table For OPER_STUS_CD : [%]', i_sp_nm, v_ExeCnt3;
   
       
            -- #411 Sleep 모드의 상태의 장비를 절전 상태로 상태 변경을 한다. 20160105 jkm1020
          	v_rslt_cd := 'L';

	      	v_ExeCnt1 := 0;
			v_ExeCnt2 := 0;
			v_ExeCnt3 := 0;

            WITH ess_st AS (
	            SELECT  DEVICE_ID  
	                 	, DEVICE_TYPE_CD   
	                 	, '5' AS OPER_STUS_CD  
	                 	, COLEC_DT
	            FROM   TB_OPR_ESS_STATE_TMP 
	            WHERE  EMS_OPMODE = '9'
	        ), u1 as (
	            -- 통신 단절인 장비의 경우 1분 데이터가 수집되지 않으므로 상태를 현황 임시 테이블에도 Update 한다.
	            UPDATE 	TB_OPR_ESS_STATE_TMP B
	            SET 		OPER_STUS_CD =A.OPER_STUS_CD
	            FROM 	ess_st A 
	            WHERE 	B.DEVICE_ID = A.DEVICE_ID
	            AND 		B.DEVICE_TYPE_CD = A.DEVICE_TYPE_CD
	            returning *
			), u2 as (
	            -- 통신 단절인 장비는 현황 임시 테이블에 데이터가 수집되지 않을 수 있으니 현황 테이블에도 Update 한다.
	            UPDATE 	TB_OPR_ESS_STATE B
	            SET 		OPER_STUS_CD = A.OPER_STUS_CD
	            FROM		ess_st A
	            WHERE 	B.DEVICE_ID = A.DEVICE_ID
	            AND 		B.DEVICE_TYPE_CD = A.DEVICE_TYPE_CD
	            returning *
			), u3 as (
	            UPDATE 	TB_OPR_ESS_STATE_HIST B
	            SET 		OPER_STUS_CD = A.OPER_STUS_CD
	            FROM 	ess_st A
	            WHERE 	B.DEVICE_ID =A.DEVICE_ID
	            AND 		B.DEVICE_TYPE_CD = A.DEVICE_TYPE_CD
	            AND 		B.COLEC_DT = A.COLEC_DT
	            returning *
			)
             SELECT 	SUM(cnt1), SUM(cnt2), SUM(cnt3)
             INTO 	v_ExeCnt1, v_ExeCnt2, v_ExeCnt3
             FROM (
	            SELECT 	COUNT(u1.*)::INTEGER cnt1, 0 cnt2, 0 cnt3
	            FROM 	u1
				UNION ALL
	            SELECT 	0 cnt1, COUNT(u2.*)::INTEGER cnt2, 0 cnt3
	            FROM 	u2
				UNION ALL
	            SELECT 	0 cnt1, 0 cnt2, COUNT(u3.*)::INTEGER cnt3
	            FROM 	u3
			) Z;

--	      	RAISE notice 'i_sp_nm[%] Update TB_OPR_ESS_STATE_TMP Table For OPER_STUS_CD Sleep Mode : [%]', i_sp_nm, v_ExeCnt1;
--	      	RAISE notice 'i_sp_nm[%] Update TB_OPR_ESS_STATE Table For OPER_STUS_CD Sleep Mode : [%]', i_sp_nm, v_ExeCnt2;
--	      	RAISE notice 'i_sp_nm[%] Update TB_OPR_ESS_STATE_HIST Table For OPER_STUS_CD Sleep Mode : [%]', i_sp_nm, v_ExeCnt3;

          	v_rslt_cd := 'M';

            --- 	2015.07.07 yngwie 장비별 상태 타임라인 데이터 처리
            v_sql := FORMAT('
	            WITH PRIORITY AS ( 
	                SELECT 1 AS PRIORITY, ''2'' AS OPER_STUS_CD UNION ALL 
	                SELECT 2 AS PRIORITY, ''1'' AS OPER_STUS_CD UNION ALL 
	                SELECT 3 AS PRIORITY, ''3'' AS OPER_STUS_CD 
	            ), UALL AS ( 
	                SELECT 	''%s'' AS COLEC_DD 
			                    , A.DEVICE_ID 
			                    , A.DEVICE_TYPE_CD 
			                    , A.OPER_STUS_CD 
			                    , B.PRIORITY 
	                FROM 	TB_OPR_ESS_STATE A
			                    , PRIORITY B 
	                WHERE 	A.OPER_STUS_CD = B.OPER_STUS_CD 
	                UNION ALL 
	                SELECT   A.COLEC_DD 
			                    , A.DEVICE_ID 
			                    , A.DEVICE_TYPE_CD 
			                    , A.OPER_STUS_CD 
			                    , B.PRIORITY 
	                FROM 	TB_OPR_ESS_STATE_TL A 
			                    , PRIORITY B 
	                WHERE   A.OPER_STUS_CD = B.OPER_STUS_CD 
	        	    AND 		A.COLEC_DD = ''%s''
	            ), B  AS ( 
	                SELECT    * 
	                FROM ( 
	                    SELECT 	COLEC_DD 
			                        , DEVICE_ID 
			                        , DEVICE_TYPE_CD 
			                        , OPER_STUS_CD 
			                        , PRIORITY 
			                        , ROW_NUMBER() OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY PRIORITY) AS RK 
	                    FROM 	UALL 
	                ) T
	                WHERE RK = 1 
	            ), U AS (
	                UPDATE TB_OPR_ESS_STATE_TL A
	                SET        OPER_STUS_CD = B.OPER_STUS_CD 
	                FROM    B
					WHERE   A.DEVICE_ID = B.DEVICE_ID 
	                AND 		A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD 
	                AND 		A.COLEC_DD = B.COLEC_DD 
					
	                RETURNING A.*
	            )
	            INSERT INTO TB_OPR_ESS_STATE_TL ( DEVICE_ID, DEVICE_TYPE_CD, COLEC_DD, OPER_STUS_CD ) 
	            SELECT 	B.DEVICE_ID, B.DEVICE_TYPE_CD, B.COLEC_DD, B.OPER_STUS_CD 
	           	FROM 	B
				WHERE NOT EXISTS (  
					SELECT 	1 
				    FROM 	U 
					WHERE	COLEC_DD 			= B.COLEC_DD  
						AND 	DEVICE_ID			= B.DEVICE_ID  
						AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
				  ) ; '
	              , SUBSTR(v_handleFlag, 1, 8), SUBSTR(v_handleFlag, 1, 8) );
             
          	EXECUTE v_sql;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--	      	RAISE notice 'i_sp_nm[%] Merge TB_OPR_ESS_STATE_TL :  [%]건 SQL[%]', i_sp_nm, v_work_num, v_sql;

           	v_rslt_cd := 'N';

            -- TEMP 테이블의 레코드 중 가장 최근 데이터를 상태정보 테이블에 Merge
            WITH B AS (
				SELECT	* 
				FROM (   
				        SELECT	ROW_NUMBER() OVER (PARTITION BY A.DEVICE_ID ORDER BY A.COLEC_DT DESC ) SEQ   
				            		, A.*   
				        FROM   TB_OPR_ESS_STATE_TMP A   
				    ) A 
				    WHERE A.SEQ = 1 
            ), U as ( 
	            UPDATE TB_OPR_ESS_STATE A
	            SET
		              COLEC_DT         		= B.COLEC_DT           
		            , OPER_STUS_CD     	= B.OPER_STUS_CD       
		            , PV_PW            		= B.PV_PW              
		            , PV_PW_H          		= B.PV_PW_H            
		            , PV_PW_PRICE      	= B.PV_PW_PRICE        
		            , PV_PW_CO2        	= B.PV_PW_CO2          
		            , PV_STUS_CD       	= B.PV_STUS_CD         
		            , CONS_PW          	= B.CONS_PW            
		            , CONS_PW_H        	= B.CONS_PW_H          
		            , CONS_PW_PRICE    	= B.CONS_PW_PRICE      
		            , CONS_PW_CO2      	= B.CONS_PW_CO2
	                , LOAD_MAIN_PW      = B.LOAD_MAIN_PW
                    , LOAD_SUB_PW      = B.LOAD_SUB_PW
		            , BT_PW            		= B.BT_PW              
		            , BT_CHRG_PW_H     	= B.BT_CHRG_PW_H       
		            , BT_CHRG_PW_PRICE = B.BT_CHRG_PW_PRICE   
		            , BT_CHRG_PW_CO2   = B.BT_CHRG_PW_CO2     
		            , BT_DCHRG_PW_H    	= B.BT_DCHRG_PW_H      
		            , BT_DCHRG_PW_PRICE= B.BT_DCHRG_PW_PRICE  
		            , BT_DCHRG_PW_CO2 	= B.BT_DCHRG_PW_CO2    
		            , BT_SOC           		= B.BT_SOC
		            , BT_REAL_SOC           = B.BT_REAL_SOC
		            , BT_SOH           		= B.BT_SOH             
		            , BT_STUS_CD       	= B.BT_STUS_CD         
		            , GRID_PW          		= B.GRID_PW            
		            , GRID_OB_PW_H     	= B.GRID_OB_PW_H          
		            , GRID_OB_PW_PRICE 	= B.GRID_OB_PW_PRICE      
		            , GRID_OB_PW_CO2   	= B.GRID_OB_PW_CO2        
		            , GRID_TR_PW_H     	= B.GRID_TR_PW_H       
		            , GRID_TR_PW_PRICE 	= B.GRID_TR_PW_PRICE   
		            , GRID_TR_PW_CO2   	= B.GRID_TR_PW_CO2     
		            , GRID_STUS_CD     	= B.GRID_STUS_CD       
		            , PCS_PW           		= B.PCS_PW             
		            , PCS_FD_PW_H      	= B.PCS_FD_PW_H        
		            , PCS_PCH_PW_H     	= B.PCS_PCH_PW_H       
		            , CREATE_DT        		= COALESCE(B.CREATE_DT, v_baseTm)           
		            , EMS_OPMODE       	= B.EMS_OPMODE       
		            , PCS_OPMODE1      	= B.PCS_OPMODE1       
		            , PCS_OPMODE2      	= B.PCS_OPMODE2       
		            , PCS_TGT_PW       	= B.PCS_TGT_PW       
		            , FEED_IN_LIMIT    	= B.FEED_IN_LIMIT       
		            , MAX_INVERTER_PW_CD    = B.MAX_INVERTER_PW_CD       
		            --, RFSH_PRD = ROUND((TO_DATE(B.COLEC_DT, 'YYYYMMDDHH24MISS') - TO_DATE(A.COLEC_DT, 'YYYYMMDDHH24MISS')) * 60 * 60 * 24) 
		            , RFSH_PRD = 60 
		            -- [ 2015.05.13 yngwie OUTLET_PW 추가
		            , OUTLET_PW        	= B.OUTLET_PW       
		            , OUTLET_PW_H      	= B.OUTLET_PW_H       
		            , OUTLET_PW_PRICE  	= B.OUTLET_PW_PRICE       
		            , OUTLET_PW_CO2    	= B.OUTLET_PW_CO2       
		            , PWR_RANGE_CD_ESS_GRID    	= FN_GET_PWR_RANGE_CD('ESS_GRID',  B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , PWR_RANGE_CD_GRID_ESS    	= FN_GET_PWR_RANGE_CD('GRID_ESS',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , PWR_RANGE_CD_GRID_LOAD  = FN_GET_PWR_RANGE_CD('GRID_LOAD', B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , PWR_RANGE_CD_ESS_LOAD    = FN_GET_PWR_RANGE_CD('ESS_LOAD',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , PWR_RANGE_CD_PV_ESS      	= FN_GET_PWR_RANGE_CD('PV_ESS',    B.PV_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            -- ]
		            -- [ 2015.07.06 yngwie 수집 컬럼 추가
		            , PCS_COMM_ERR_RATE   = B.PCS_COMM_ERR_RATE 
		            , SMTR_COMM_ERR_RATE  = B.SMTR_COMM_ERR_RATE 
		            , M2M_COMM_ERR_RATE   = B.M2M_COMM_ERR_RATE       
		            , SMTR_TP_CD          = B.SMTR_TP_CD 
		            , SMTR_PULSE_CNT      = B.SMTR_PULSE_CNT 
		            , SMTR_MODL_CD        = B.SMTR_MODL_CD 
		            , PV_MAX_PWR1         = B.PV_MAX_PWR1 
		            , PV_MAX_PWR2         = B.PV_MAX_PWR2 
		            , INSTL_RGN           = B.INSTL_RGN 
		            , MMTR_SLV_ADDR       = B.MMTR_SLV_ADDR 
		            , BASICMODE_CD        = B.BASICMODE_CD 
		            , PCS_FLAG0       = B.PCS_FLAG0  
		            , PCS_FLAG1       = B.PCS_FLAG1  
		            , PCS_FLAG2       = B.PCS_FLAG2  
		            , PCS_OPMODE_CMD1         = B.PCS_OPMODE_CMD1  
		            , PCS_OPMODE_CMD2         = B.PCS_OPMODE_CMD2  
		            , PV_V1       	= B.PV_V1  
		            , PV_I1       		= B.PV_I1  
		            , PV_PW1      	= B.PV_PW1  
		            , PV_V2       	= B.PV_V2  
		            , PV_I2       		= B.PV_I2  
		            , PV_PW2      	= B.PV_PW2  
		            , INVERTER_V   = B.INVERTER_V  
		            , INVERTER_I    = B.INVERTER_I  
		            , INVERTER_PW = B.INVERTER_PW  
		            , DC_LINK_V     = B.DC_LINK_V  
		            , G_RLY_CNT     = B.G_RLY_CNT  
		            , BAT_RLY_CNT  = B.BAT_RLY_CNT  
		            , GRID_TO_GRID_RLY_CNT	= B.GRID_TO_GRID_RLY_CNT  
		            , BAT_PCHRG_RLY_CNT       = B.BAT_PCHRG_RLY_CNT    
		            , BMS_FLAG0       = B.BMS_FLAG0  
		            , BMS_DIAG0       = B.BMS_DIAG0  
		            , RACK_V      		= B.RACK_V  
		            , RACK_I      		= B.RACK_I  
		            , CELL_MAX_V      	= B.CELL_MAX_V  
		            , CELL_MIN_V      	= B.CELL_MIN_V  
		            , CELL_AVG_V      	= B.CELL_AVG_V  
		            , CELL_MAX_T     	= B.CELL_MAX_T  
		            , CELL_MIN_T      	= B.CELL_MIN_T  
		            , CELL_AVG_T      	= B.CELL_AVG_T  
		            , TRAY_CNT        	= B.TRAY_CNT 
		            , SYS_ST_CD       	= B.SYS_ST_CD 
		            , BT_CHRG_ST_CD  = B.BT_CHRG_ST_CD 
		            , BT_DCHRG_ST_CD= B.BT_DCHRG_ST_CD 
		            , PV_ST_CD        	= B.PV_ST_CD 
		            , GRID_ST_CD      	= B.GRID_ST_CD 
		            , SMTR_ST_CD      	= B.SMTR_ST_CD 
		            , SMTR_TR_CNTER 	= B.SMTR_TR_CNTER 
		            , SMTR_OB_CNTER	= B.SMTR_OB_CNTER 
		            , DN_ENABLE       	= B.DN_ENABLE         
		            , DC_START        	= B.DC_START         
		            , DC_END          	= B.DC_END         
		            , NC_START        	= B.NC_START         
		            , NC_END          	= B.NC_END                             
		            -- ]
		            --[신규모델로 모니터링 추가
		            , GRID_CODE0 		= B.GRID_CODE0 
		            , GRID_CODE1 		= B.GRID_CODE1 
		            , GRID_CODE2 		= B.GRID_CODE2 
		            , PCON_BAT_TARGETPOWER =   B.PCON_BAT_TARGETPOWER 
		            , GRID_CODE3 =             B.GRID_CODE3 
		            , BDC_MODULE_CODE0 =       B.BDC_MODULE_CODE0 
		            , BDC_MODULE_CODE1 =       B.BDC_MODULE_CODE1 
		            , BDC_MODULE_CODE2 =       B.BDC_MODULE_CODE2 
		            , BDC_MODULE_CODE3 =       B.BDC_MODULE_CODE3 
		            , FAULT5_REALTIME_YEAR =   B.FAULT5_REALTIME_YEAR 
		            , FAULT5_REALTIME_MONTH =  B.FAULT5_REALTIME_MONTH 
		            , FAULT5_DAY =             B.FAULT5_DAY 
		            , FAULT5_HOUR =            B.FAULT5_HOUR 
		            , FAULT5_MINUTE =          B.FAULT5_MINUTE 
		            , FAULT5_SECOND =          B.FAULT5_SECOND 
		            , FAULT5_DATAHIGH =        B.FAULT5_DATAHIGH 
		            , FAULT5_DATALOW =         B.FAULT5_DATALOW 
		            , FAULT6_REALTIME_YEAR =   B.FAULT6_REALTIME_YEAR 
		            , FAULT6_REALTIME_MONTH =  B.FAULT6_REALTIME_MONTH 
		            , FAULT6_REALTIME_DAY =    B.FAULT6_REALTIME_DAY 
		            , FAULT6_REALTIME_HOUR =   B.FAULT6_REALTIME_HOUR 
		            , FAULT6_REALTIME_MINUTE = B.FAULT6_REALTIME_MINUTE 
		            , FAULT6_REALTIME_SECOND = B.FAULT6_REALTIME_SECOND 
		            , FAULT6_DATAHIGH =        B.FAULT6_DATAHIGH 
		            , FAULT6_DATALOW =         B.FAULT6_DATALOW 
		            , FAULT7_REALTIME_YEAR =   B.FAULT7_REALTIME_YEAR 
		            , FAULT7_REALTIME_MONTH =  B.FAULT7_REALTIME_MONTH 
		            , FAULT7_REALTIME_DAY =    B.FAULT7_REALTIME_DAY 
		            , FAULT7_REALTIME_HOUR =   B.FAULT7_REALTIME_HOUR 
		            , FAULT7_REALTIME_MINUTE = B.FAULT7_REALTIME_MINUTE 
		            , FAULT7_REALTIME_SECOND = B.FAULT7_REALTIME_SECOND 
		            , FAULT7_DATAHIGH =        B.FAULT7_DATAHIGH 
		            , FAULT7_DATALOW =         B.FAULT7_DATALOW 
		            , FAULT8_REALTIME_YEAR =   B.FAULT8_REALTIME_YEAR 
		            , FAULT8_REALTIME_MONTH =  B.FAULT8_REALTIME_MONTH 
		            , FAULT8_REALTIME_DAY =    B.FAULT8_REALTIME_DAY 
		            , FAULT8_REALTIME_HOUR =   B.FAULT8_REALTIME_HOUR 
		            , FAULT8_REALTIME_MINUTE = B.FAULT8_REALTIME_MINUTE 
		            , FAULT8_REALTIME_SECOND = B.FAULT8_REALTIME_SECOND 
		            , FAULT8_DATAHIGH =        B.FAULT8_DATAHIGH 
		            , FAULT8_DATALOW =         B.FAULT8_DATALOW 
		            , FAULT9_REALTIME_YEAR =   B.FAULT9_REALTIME_YEAR 
		            , FAULT9_REALTIME_MONTH =  B.FAULT9_REALTIME_MONTH 
		            , FAULT9_REALTIME_DAY =    B.FAULT9_REALTIME_DAY 
		            , FAULT9_REALTIME_HOUR =   B.FAULT9_REALTIME_HOUR 
		            , FAULT9_REALTIME_MINUTE = B.FAULT9_REALTIME_MINUTE 
		            , FAULT9_REALTIME_SECOND = B.FAULT9_REALTIME_SECOND 
		            , FAULT9_DATAHIGH =        B.FAULT9_DATAHIGH 
		            , FAULT9_DATALOW =         B.FAULT9_DATALOW 
                        -- 추가 elanth
                        , IS_CONNECT =             B.IS_CONNECT
		            --]        
		        FROM	B
	            WHERE 	A.DEVICE_ID = B.DEVICE_ID             
	            AND 		A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD   
                        AND         A.colec_dt <= B.colec_dt -- YUNSANG 유입 데이터가 더 최근 때에만 update (A: 기존, B: 유입)
	            
	            RETURNING A.*
	        )                           
            INSERT INTO TB_OPR_ESS_STATE ( 
				DEVICE_ID, DEVICE_TYPE_CD, COLEC_DT, OPER_STUS_CD                            
	            , PV_PW, PV_PW_H, PV_PW_PRICE, PV_PW_CO2, PV_STUS_CD                              
	            , CONS_PW, CONS_PW_H, CONS_PW_PRICE, CONS_PW_CO2
	            , LOAD_MAIN_PW, LOAD_SUB_PW
	            , BT_PW, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2, BT_DCHRG_PW_H, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2, BT_SOC, BT_REAL_SOC, BT_SOH, BT_STUS_CD
	            , GRID_PW, GRID_OB_PW_H, GRID_OB_PW_PRICE, GRID_OB_PW_CO2, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2, GRID_STUS_CD                            
	            , PCS_PW, PCS_FD_PW_H, PCS_PCH_PW_H
	            , CREATE_DT                               
	            , EMS_OPMODE, PCS_OPMODE1, PCS_OPMODE2                               
	            , PCS_TGT_PW, FEED_IN_LIMIT, MAX_INVERTER_PW_CD                               
	            -- [ 2015.05.13 yngwie OUTLET_PW 추가
	            , OUTLET_PW, OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2                               
	            , PWR_RANGE_CD_ESS_GRID, PWR_RANGE_CD_GRID_ESS, PWR_RANGE_CD_GRID_LOAD, PWR_RANGE_CD_ESS_LOAD, PWR_RANGE_CD_PV_ESS             
	            -- ]
	            -- [ 2015.07.06 yngwie 수집 컬럼 추가
	            , PCS_COMM_ERR_RATE, SMTR_COMM_ERR_RATE, M2M_COMM_ERR_RATE  
	            , SMTR_TP_CD, SMTR_PULSE_CNT, SMTR_MODL_CD, PV_MAX_PWR1, PV_MAX_PWR2, INSTL_RGN, MMTR_SLV_ADDR, BASICMODE_CD    
	            , PCS_FLAG0, PCS_FLAG1, PCS_FLAG2, PCS_OPMODE_CMD1, PCS_OPMODE_CMD2  
	            , PV_V1, PV_I1, PV_PW1, PV_V2, PV_I2, PV_PW2, INVERTER_V, INVERTER_I, INVERTER_PW, DC_LINK_V  
	            , G_RLY_CNT, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT   
	            , BMS_FLAG0, BMS_DIAG0, RACK_V, RACK_I  
	            , CELL_MAX_V, CELL_MIN_V, CELL_AVG_V, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T  
	            , TRAY_CNT, SYS_ST_CD, BT_CHRG_ST_CD, BT_DCHRG_ST_CD, PV_ST_CD, GRID_ST_CD, SMTR_ST_CD 
	            , SMTR_TR_CNTER, SMTR_OB_CNTER, DN_ENABLE, DC_START, DC_END, NC_START, NC_END                                
	            -- ]            
	         	--[신규모델로 모니터링 추가
				, GRID_CODE0, GRID_CODE1, GRID_CODE2 
				, PCON_BAT_TARGETPOWER, GRID_CODE3 
				, BDC_MODULE_CODE0, BDC_MODULE_CODE1, BDC_MODULE_CODE2, BDC_MODULE_CODE3 
				, FAULT5_REALTIME_YEAR, FAULT5_REALTIME_MONTH, FAULT5_DAY, FAULT5_HOUR, FAULT5_MINUTE, FAULT5_SECOND, FAULT5_DATAHIGH, FAULT5_DATALOW 
				, FAULT6_REALTIME_YEAR, FAULT6_REALTIME_MONTH, FAULT6_REALTIME_DAY, FAULT6_REALTIME_HOUR, FAULT6_REALTIME_MINUTE, FAULT6_REALTIME_SECOND, FAULT6_DATAHIGH, FAULT6_DATALOW 
				, FAULT7_REALTIME_YEAR, FAULT7_REALTIME_MONTH, FAULT7_REALTIME_DAY, FAULT7_REALTIME_HOUR, FAULT7_REALTIME_MINUTE, FAULT7_REALTIME_SECOND, FAULT7_DATAHIGH, FAULT7_DATALOW 
				, FAULT8_REALTIME_YEAR, FAULT8_REALTIME_MONTH, FAULT8_REALTIME_DAY, FAULT8_REALTIME_HOUR, FAULT8_REALTIME_MINUTE, FAULT8_REALTIME_SECOND, FAULT8_DATAHIGH, FAULT8_DATALOW 
				, FAULT9_REALTIME_YEAR, FAULT9_REALTIME_MONTH, FAULT9_REALTIME_DAY, FAULT9_REALTIME_HOUR, FAULT9_REALTIME_MINUTE, FAULT9_REALTIME_SECOND, FAULT9_DATAHIGH, FAULT9_DATALOW             
                                         -- 추가 elanth
                                         , IS_CONNECT
	            --]  
	        )
			SELECT                              
	               B.DEVICE_ID, B.DEVICE_TYPE_CD, B.COLEC_DT, B.OPER_STUS_CD                            
	              , B.PV_PW, B.PV_PW_H, B.PV_PW_PRICE, B.PV_PW_CO2, B.PV_STUS_CD                              
	              , B.CONS_PW, B.CONS_PW_H, B.CONS_PW_PRICE, B.CONS_PW_CO2
			      , B.LOAD_MAIN_PW, B.LOAD_SUB_PW
	              , B.BT_PW, B.BT_CHRG_PW_H, B.BT_CHRG_PW_PRICE, B.BT_CHRG_PW_CO2, B.BT_DCHRG_PW_H, B.BT_DCHRG_PW_PRICE, B.BT_DCHRG_PW_CO2, B.BT_SOC, B.BT_REAL_SOC, B.BT_SOH, B.BT_STUS_CD
	              , B.GRID_PW, B.GRID_OB_PW_H, B.GRID_OB_PW_PRICE, B.GRID_OB_PW_CO2, B.GRID_TR_PW_H, B.GRID_TR_PW_PRICE, B.GRID_TR_PW_CO2, B.GRID_STUS_CD
	              , B.PCS_PW, B.PCS_FD_PW_H, B.PCS_PCH_PW_H
	              , COALESCE(B.CREATE_DT, v_baseTm)                               
	              , B.EMS_OPMODE, B.PCS_OPMODE1, B.PCS_OPMODE2, B.PCS_TGT_PW, B.FEED_IN_LIMIT, B.MAX_INVERTER_PW_CD                               
	            -- [ 2015.05.13 yngwie OUTLET_PW 추가
	              , B.OUTLET_PW, B.OUTLET_PW_H, B.OUTLET_PW_PRICE, B.OUTLET_PW_CO2                               
	              , FN_GET_PWR_RANGE_CD('ESS_GRID',  B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('GRID_ESS',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('GRID_LOAD', B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('ESS_LOAD',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('PV_ESS',    B.PV_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD)
	            -- ]
	            -- [ 2015.07.06 yngwie 수집 컬럼 추가
	              , B.PCS_COMM_ERR_RATE, B.SMTR_COMM_ERR_RATE, B.M2M_COMM_ERR_RATE      
	              , B.SMTR_TP_CD, B.SMTR_PULSE_CNT, B.SMTR_MODL_CD, B.PV_MAX_PWR1, B.PV_MAX_PWR2, B.INSTL_RGN, B.MMTR_SLV_ADDR, B.BASICMODE_CD 
	              , B.PCS_FLAG0, B.PCS_FLAG1, B.PCS_FLAG2, B.PCS_OPMODE_CMD1, B.PCS_OPMODE_CMD2  
	              , B.PV_V1, B.PV_I1, B.PV_PW1, B.PV_V2, B.PV_I2, B.PV_PW2  
	              , B.INVERTER_V, B.INVERTER_I, B.INVERTER_PW, B.DC_LINK_V  
	              , B.G_RLY_CNT, B.BAT_RLY_CNT, B.GRID_TO_GRID_RLY_CNT, B.BAT_PCHRG_RLY_CNT   
	              , B.BMS_FLAG0, B.BMS_DIAG0, B.RACK_V, B.RACK_I  
	              , B.CELL_MAX_V, B.CELL_MIN_V, B.CELL_AVG_V, B.CELL_MAX_T, B.CELL_MIN_T, B.CELL_AVG_T  
	              , B.TRAY_CNT, B.SYS_ST_CD, B.BT_CHRG_ST_CD, B.BT_DCHRG_ST_CD, B.PV_ST_CD, B.GRID_ST_CD, B.SMTR_ST_CD 
	              , B.SMTR_TR_CNTER, B.SMTR_OB_CNTER, B.DN_ENABLE 
	              , B.DC_START, B.DC_END, B.NC_START, B.NC_END                        
	           		-- ]
	        		--[신규모델로 모니터링 추가
	              , B.GRID_CODE0, B.GRID_CODE1, B.GRID_CODE2, B.PCON_BAT_TARGETPOWER, B.GRID_CODE3 
	              , B.BDC_MODULE_CODE0, B.BDC_MODULE_CODE1, B.BDC_MODULE_CODE2, B.BDC_MODULE_CODE3 
	              , B.FAULT5_REALTIME_YEAR, B.FAULT5_REALTIME_MONTH, B.FAULT5_DAY, B.FAULT5_HOUR, B.FAULT5_MINUTE, B.FAULT5_SECOND, B.FAULT5_DATAHIGH, B.FAULT5_DATALOW 
	              , B.FAULT6_REALTIME_YEAR, B.FAULT6_REALTIME_MONTH, B.FAULT6_REALTIME_DAY, B.FAULT6_REALTIME_HOUR, B.FAULT6_REALTIME_MINUTE, B.FAULT6_REALTIME_SECOND, B.FAULT6_DATAHIGH, B.FAULT6_DATALOW 
	              , B.FAULT7_REALTIME_YEAR, B.FAULT7_REALTIME_MONTH, B.FAULT7_REALTIME_DAY, B.FAULT7_REALTIME_HOUR, B.FAULT7_REALTIME_MINUTE, B.FAULT7_REALTIME_SECOND, B.FAULT7_DATAHIGH, B.FAULT7_DATALOW 
	              , B.FAULT8_REALTIME_YEAR, B.FAULT8_REALTIME_MONTH, B.FAULT8_REALTIME_DAY, B.FAULT8_REALTIME_HOUR, B.FAULT8_REALTIME_MINUTE, B.FAULT8_REALTIME_SECOND, B.FAULT8_DATAHIGH, B.FAULT8_DATALOW 
	              , B.FAULT9_REALTIME_YEAR, B.FAULT9_REALTIME_MONTH, B.FAULT9_REALTIME_DAY, B.FAULT9_REALTIME_HOUR, B.FAULT9_REALTIME_MINUTE, B.FAULT9_REALTIME_SECOND, B.FAULT9_DATAHIGH, B.FAULT9_DATALOW 
                 -- 추가 elanth
                 , B.IS_CONNECT
	            	--]           
			FROM B
                     LEFT JOIN tb_opr_ess_state AS current ON B.device_id = current.device_id
			WHERE NOT EXISTS (  
				SELECT 	1 
			    FROM 	U 
				WHERE	DEVICE_ID			= B.DEVICE_ID  
					AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
			  )
            AND (current.colec_dt <= B.colec_dt or current.colec_dt IS NULL)
			 ;                 

           GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--	      	RAISE notice 'i_sp_nm[%] STATE New Records :  [%]건', i_sp_nm, v_work_num;
            
			v_rslt_cd := 'O';
 
            -- TEMP 테이블의 레코드를 이력테이블에 Insert
			INSERT INTO TB_OPR_ESS_STATE_HIST (
				 DEVICE_ID, DEVICE_TYPE_CD, COLEC_DT, OPER_STUS_CD 
                , PV_PW, PV_PW_H, PV_PW_PRICE, PV_PW_CO2, PV_STUS_CD 
                , CONS_PW, CONS_PW_H, CONS_PW_PRICE, CONS_PW_CO2
                , LOAD_MAIN_PW, LOAD_SUB_PW
                , BT_PW, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2, BT_DCHRG_PW_H, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2, BT_SOC, BT_REAL_SOC, BT_SOH, BT_STUS_CD
                , GRID_PW, GRID_OB_PW_H, GRID_OB_PW_PRICE, GRID_OB_PW_CO2, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2, GRID_STUS_CD 
                , PCS_PW, PCS_FD_PW_H, PCS_PCH_PW_H, EMS_OPMODE, PCS_OPMODE1, PCS_OPMODE2, PCS_TGT_PW, FEED_IN_LIMIT
                , CREATE_DT
                , MAX_INVERTER_PW_CD 
                , OUTLET_PW, OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2         -- [ 2015.05.13 yngwie OUTLET_PW 추가   
            -- [ 2015.07.06 yngwie 수집 컬럼 추가
                , PCS_COMM_ERR_RATE, SMTR_COMM_ERR_RATE, M2M_COMM_ERR_RATE, SMTR_TP_CD, SMTR_PULSE_CNT, SMTR_MODL_CD, PV_MAX_PWR1, PV_MAX_PWR2    
                , INSTL_RGN, MMTR_SLV_ADDR, BASICMODE_CD
	            , PCS_FLAG0, PCS_FLAG1, PCS_FLAG2, PCS_OPMODE_CMD1, PCS_OPMODE_CMD2  
	            , PV_V1, PV_I1, PV_PW1, PV_V2, PV_I2, PV_PW2  
	            , INVERTER_V, INVERTER_I, INVERTER_PW, DC_LINK_V  
	            , G_RLY_CNT, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT    
	            , BMS_FLAG0, BMS_DIAG0, RACK_V, RACK_I  
	            , CELL_MAX_V, CELL_MIN_V, CELL_AVG_V, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T  
	            , TRAY_CNT, SYS_ST_CD 
	            , BT_CHRG_ST_CD, BT_DCHRG_ST_CD, PV_ST_CD, GRID_ST_CD 
	            , SMTR_ST_CD, SMTR_TR_CNTER, SMTR_OB_CNTER             
            -- ]         
				--[신규모델로 모니터링 추가
				, GRID_CODE0, GRID_CODE1, GRID_CODE2 
				, PCON_BAT_TARGETPOWER, GRID_CODE3 
				, BDC_MODULE_CODE0, BDC_MODULE_CODE1, BDC_MODULE_CODE2, BDC_MODULE_CODE3 
				, FAULT5_REALTIME_YEAR, FAULT5_REALTIME_MONTH, FAULT5_DAY, FAULT5_HOUR, FAULT5_MINUTE, FAULT5_SECOND, FAULT5_DATAHIGH, FAULT5_DATALOW 
				, FAULT6_REALTIME_YEAR, FAULT6_REALTIME_MONTH, FAULT6_REALTIME_DAY, FAULT6_REALTIME_HOUR, FAULT6_REALTIME_MINUTE, FAULT6_REALTIME_SECOND, FAULT6_DATAHIGH, FAULT6_DATALOW 
				, FAULT7_REALTIME_YEAR, FAULT7_REALTIME_MONTH, FAULT7_REALTIME_DAY, FAULT7_REALTIME_HOUR, FAULT7_REALTIME_MINUTE, FAULT7_REALTIME_SECOND, FAULT7_DATAHIGH, FAULT7_DATALOW 
				, FAULT8_REALTIME_YEAR, FAULT8_REALTIME_MONTH, FAULT8_REALTIME_DAY, FAULT8_REALTIME_HOUR, FAULT8_REALTIME_MINUTE, FAULT8_REALTIME_SECOND, FAULT8_DATAHIGH, FAULT8_DATALOW 
				, FAULT9_REALTIME_YEAR, FAULT9_REALTIME_MONTH, FAULT9_REALTIME_DAY, FAULT9_REALTIME_HOUR, FAULT9_REALTIME_MINUTE, FAULT9_REALTIME_SECOND, FAULT9_DATAHIGH, FAULT9_DATALOW             
				-- 추가 elanth
				, IS_CONNECT
				  --]
			)
            SELECT DEVICE_ID, DEVICE_TYPE_CD, COLEC_DT, OPER_STUS_CD 
	                , PV_PW, PV_PW_H, PV_PW_PRICE, PV_PW_CO2, PV_STUS_CD 
	                , CONS_PW, CONS_PW_H, CONS_PW_PRICE, CONS_PW_CO2
                    , LOAD_MAIN_PW, LOAD_SUB_PW
	                , BT_PW, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2, BT_DCHRG_PW_H, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2, BT_SOC, BT_REAL_SOC, BT_SOH, BT_STUS_CD
	                , GRID_PW, GRID_OB_PW_H, GRID_OB_PW_PRICE, GRID_OB_PW_CO2, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2, GRID_STUS_CD 
	                , PCS_PW, PCS_FD_PW_H, PCS_PCH_PW_H, EMS_OPMODE, PCS_OPMODE1, PCS_OPMODE2, PCS_TGT_PW, FEED_IN_LIMIT
	                , CREATE_DT
	                , MAX_INVERTER_PW_CD 
	                , OUTLET_PW, OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2
	                , PCS_COMM_ERR_RATE, SMTR_COMM_ERR_RATE, M2M_COMM_ERR_RATE, SMTR_TP_CD, SMTR_PULSE_CNT, SMTR_MODL_CD, PV_MAX_PWR1, PV_MAX_PWR2    
	                , INSTL_RGN, MMTR_SLV_ADDR, BASICMODE_CD
		            , PCS_FLAG0, PCS_FLAG1, PCS_FLAG2, PCS_OPMODE_CMD1, PCS_OPMODE_CMD2  
		            , PV_V1, PV_I1, PV_PW1, PV_V2, PV_I2, PV_PW2  
		            , INVERTER_V, INVERTER_I, INVERTER_PW, DC_LINK_V  
		            , G_RLY_CNT, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT    
		            , BMS_FLAG0, BMS_DIAG0, RACK_V, RACK_I  
		            , CELL_MAX_V, CELL_MIN_V, CELL_AVG_V, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T  
		            , TRAY_CNT, SYS_ST_CD 
		            , BT_CHRG_ST_CD, BT_DCHRG_ST_CD, PV_ST_CD, GRID_ST_CD 
		            , SMTR_ST_CD, SMTR_TR_CNTER, SMTR_OB_CNTER                      
		            , GRID_CODE0, GRID_CODE1, GRID_CODE2 
		            , PCON_BAT_TARGETPOWER, GRID_CODE3 
		            , BDC_MODULE_CODE0, BDC_MODULE_CODE1, BDC_MODULE_CODE2, BDC_MODULE_CODE3 
		            , FAULT5_REALTIME_YEAR, FAULT5_REALTIME_MONTH, FAULT5_DAY, FAULT5_HOUR, FAULT5_MINUTE, FAULT5_SECOND, FAULT5_DATAHIGH, FAULT5_DATALOW 
		            , FAULT6_REALTIME_YEAR, FAULT6_REALTIME_MONTH, FAULT6_REALTIME_DAY, FAULT6_REALTIME_HOUR, FAULT6_REALTIME_MINUTE, FAULT6_REALTIME_SECOND, FAULT6_DATAHIGH, FAULT6_DATALOW 
		            , FAULT7_REALTIME_YEAR, FAULT7_REALTIME_MONTH, FAULT7_REALTIME_DAY, FAULT7_REALTIME_HOUR, FAULT7_REALTIME_MINUTE, FAULT7_REALTIME_SECOND, FAULT7_DATAHIGH, FAULT7_DATALOW 
		            , FAULT8_REALTIME_YEAR, FAULT8_REALTIME_MONTH, FAULT8_REALTIME_DAY, FAULT8_REALTIME_HOUR, FAULT8_REALTIME_MINUTE, FAULT8_REALTIME_SECOND, FAULT8_DATAHIGH, FAULT8_DATALOW 
		            , FAULT9_REALTIME_YEAR, FAULT9_REALTIME_MONTH, FAULT9_REALTIME_DAY, FAULT9_REALTIME_HOUR, FAULT9_REALTIME_MINUTE, FAULT9_REALTIME_SECOND, FAULT9_DATAHIGH, FAULT9_DATALOW             
                    -- 추가 elanth
			    	, IS_CONNECT
            FROM 	TB_OPR_ESS_STATE_TMP
            ON conflict(DEVICE_ID, DEVICE_TYPE_CD, COLEC_DT)
            DO nothing;
           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--	      	RAISE notice 'i_sp_nm[%] Insert TB_OPR_ESS_STATE_HIST Table :  [%]건', i_sp_nm, v_work_num;
	      
--	      	v_err_msg ='Insert TB_OPR_ESS_STATE_HIST Table : '||v_work_num::TEXT;

	      
	      /*--------------------------------------------------------------- 예측관련 업무 배제결정 ( QCells ) -----------------------------------------------

--                2015.09.02 yngwie 장비별 CONFIG 데이터 처리.
--                TB_OPR_ESS_CONFIG 테이블에 정보가 없는 장비는
--                TB_RAW_EMS_INFO 에 올라온 정보로 추가하고 _HIST 에도 INSERT 한다.

--			 2015.11.12 stjoo 장비별 CONFIG 데이터 처리 삭제
--			 CONFIG 정보가 없는 기존에 운영하던 장비들을 위해 
--			 임시로 적용하던 로직이였습니다.			
            
            ------------------------
            -- 3.1 예측정보 데이터 처리
            ------------------------
			v_rslt_cd := 'P';

			TRUNCATE TABLE TB_OPR_ESS_PREDICT_TMP;

            -- 대상 레코드들의 CO2와 금액을 계산한 결과를 예측정보 TMP 테이블에 Insert
            INSERT INTO TB_OPR_ESS_PREDICT_TMP 
            SELECT 
                A.COLEC_DT  
                , A.DEVICE_ID  
                , A.DEVICE_TYPE_CD  
                , A.PREDICT_DD  
                , A.PREDICT_HH  
                , A.CONS_VAL AS CONS_PREDICT_PW_H  
                , TRUNC(ABS(A.CONS_VAL) * D.CARB_EMI_QUITY * .001, 2) AS CONS_PREDICT_CO2  
                , TRUNC(ABS(A.CONS_VAL) * E.UNIT_PRICE * .001, 2) AS CONS_PREDICT_PRICE  
                , A.PV_VAL AS PV_PREDICT_PW_H  
                , TRUNC(A.PV_VAL * D.CARB_EMI_QUITY * .001, 2) AS PV_PREDICT_CO2  
                , TRUNC(A.PV_VAL * ET.UNIT_PRICE * .001, 2) AS PV_PREDICT_PRICE  
                , v_baseTm AS CREATE_DT  
            FROM  
                ( 
                    SELECT  
                        A.COLEC_DT  
                        , A.DEVICE_ID  
                        , A.DEVICE_TYPE_CD  
                        , A.CNTRY_CD 
                        , A.ELPW_PROD_CD 
                        , FN_TS_STR_TO_STR(A.UTC_COLEC_DT || H.HH, A.UTC_OFFSET, 'YYYYMMDDHH24', 0, 'YYYYMMDD') AS PREDICT_DD  
                        , FN_TS_STR_TO_STR(A.UTC_COLEC_DT || H.HH, A.UTC_OFFSET, 'YYYYMMDDHH24', 0, 'HH24') AS PREDICT_HH 
                        , MAX(CASE  
                            WHEN H.HH = '00' THEN A.COL193  
                            WHEN H.HH = '01' THEN A.COL194  
                            WHEN H.HH = '02' THEN A.COL195  
                            WHEN H.HH = '03' THEN A.COL196  
                            WHEN H.HH = '04' THEN A.COL197  
                            WHEN H.HH = '05' THEN A.COL198  
                            WHEN H.HH = '06' THEN A.COL199  
                            WHEN H.HH = '07' THEN A.COL200  
                            WHEN H.HH = '08' THEN A.COL201  
                            WHEN H.HH = '09' THEN A.COL202  
                            WHEN H.HH = '10' THEN A.COL203  
                            WHEN H.HH = '11' THEN A.COL204  
                            WHEN H.HH = '12' THEN A.COL205  
                            WHEN H.HH = '13' THEN A.COL206  
                            WHEN H.HH = '14' THEN A.COL207  
                            WHEN H.HH = '15' THEN A.COL208  
                            WHEN H.HH = '16' THEN A.COL209  
                            WHEN H.HH = '17' THEN A.COL210  
                            WHEN H.HH = '18' THEN A.COL211  
                            WHEN H.HH = '19' THEN A.COL212  
                            WHEN H.HH = '20' THEN A.COL213  
                            WHEN H.HH = '21' THEN A.COL214  
                            WHEN H.HH = '22' THEN A.COL215  
                            WHEN H.HH = '23' THEN A.COL216  
                            ELSE NULL  
                        END)::NUMERIC PV_VAL  
                        , MAX(CASE  
                            WHEN H.HH = '00' THEN A.COL217
                            WHEN H.HH = '01' THEN A.COL218
                            WHEN H.HH = '02' THEN A.COL219
                            WHEN H.HH = '03' THEN A.COL220
                            WHEN H.HH = '04' THEN A.COL221
                            WHEN H.HH = '05' THEN A.COL222
                            WHEN H.HH = '06' THEN A.COL223
                            WHEN H.HH = '07' THEN A.COL224
                            WHEN H.HH = '08' THEN A.COL225
                            WHEN H.HH = '09' THEN A.COL226
                            WHEN H.HH = '10' THEN A.COL227
                            WHEN H.HH = '11' THEN A.COL228
                            WHEN H.HH = '12' THEN A.COL229
                            WHEN H.HH = '13' THEN A.COL230
                            WHEN H.HH = '14' THEN A.COL231
                            WHEN H.HH = '15' THEN A.COL232
                            WHEN H.HH = '16' THEN A.COL233
                            WHEN H.HH = '17' THEN A.COL234
                            WHEN H.HH = '18' THEN A.COL235
                            WHEN H.HH = '19' THEN A.COL236
                            WHEN H.HH = '20' THEN A.COL237
                            WHEN H.HH = '21' THEN A.COL238
                            WHEN H.HH = '22' THEN A.COL239
                            WHEN H.HH = '23' THEN A.COL240
                            ELSE NULL  
                        END)::NUMERIC CONS_VAL 
                    FROM  
                        (  
                            SELECT  
                                FN_TS_STR_TO_STR(A.COLEC_DT, 0, 'YYYYMMDDHH24MISS', CT.UTC_OFFSET, 'YYYYMMDD') AS UTC_COLEC_DT 
                                , CT.UTC_OFFSET  
                                , B.CITY_CD  
                                , B.CNTRY_CD  
                                , B.ELPW_PROD_CD  
                                , B.DEVICE_TYPE_CD  
                                , A.*  
                            FROM   
                                (  
                                    SELECT   
                                        A.COLEC_DT 
                                        , A.DEVICE_ID 
                                        , FN_GET_CONV_NA(A.COL193) AS COL193
                                        , FN_GET_CONV_NA(A.COL194) AS COL194
                                        , FN_GET_CONV_NA(A.COL195) AS COL195
                                        , FN_GET_CONV_NA(A.COL196) AS COL196
                                        , FN_GET_CONV_NA(A.COL197) AS COL197
                                        , FN_GET_CONV_NA(A.COL198) AS COL198
                                        , FN_GET_CONV_NA(A.COL199) AS COL199
                                        , FN_GET_CONV_NA(A.COL200) AS COL200
                                        , FN_GET_CONV_NA(A.COL201) AS COL201
                                        , FN_GET_CONV_NA(A.COL202) AS COL202
                                        , FN_GET_CONV_NA(A.COL203) AS COL203
                                        , FN_GET_CONV_NA(A.COL204) AS COL204
                                        , FN_GET_CONV_NA(A.COL205) AS COL205
                                        , FN_GET_CONV_NA(A.COL206) AS COL206
                                        , FN_GET_CONV_NA(A.COL207) AS COL207
                                        , FN_GET_CONV_NA(A.COL208) AS COL208
                                        , FN_GET_CONV_NA(A.COL209) AS COL209
                                        , FN_GET_CONV_NA(A.COL210) AS COL210
                                        , FN_GET_CONV_NA(A.COL211) AS COL211
                                        , FN_GET_CONV_NA(A.COL212) AS COL212
                                        , FN_GET_CONV_NA(A.COL213) AS COL213
                                        , FN_GET_CONV_NA(A.COL214) AS COL214
                                        , FN_GET_CONV_NA(A.COL215) AS COL215
                                        , FN_GET_CONV_NA(A.COL216) AS COL216
                                        , FN_GET_CONV_NA(A.COL217) AS COL217 
                                        , FN_GET_CONV_NA(A.COL218) AS COL218 
                                        , FN_GET_CONV_NA(A.COL219) AS COL219 
                                        , FN_GET_CONV_NA(A.COL220) AS COL220 
                                        , FN_GET_CONV_NA(A.COL221) AS COL221 
                                        , FN_GET_CONV_NA(A.COL222) AS COL222 
                                        , FN_GET_CONV_NA(A.COL223) AS COL223 
                                        , FN_GET_CONV_NA(A.COL224) AS COL224 
                                        , FN_GET_CONV_NA(A.COL225) AS COL225 
                                        , FN_GET_CONV_NA(A.COL226) AS COL226 
                                        , FN_GET_CONV_NA(A.COL227) AS COL227 
                                        , FN_GET_CONV_NA(A.COL228) AS COL228 
                                        , FN_GET_CONV_NA(A.COL229) AS COL229 
                                        , FN_GET_CONV_NA(A.COL230) AS COL230 
                                        , FN_GET_CONV_NA(A.COL231) AS COL231 
                                        , FN_GET_CONV_NA(A.COL232) AS COL232 
                                        , FN_GET_CONV_NA(A.COL233) AS COL233 
                                        , FN_GET_CONV_NA(A.COL234) AS COL234 
                                        , FN_GET_CONV_NA(A.COL235) AS COL235 
                                        , FN_GET_CONV_NA(A.COL236) AS COL236 
                                        , FN_GET_CONV_NA(A.COL237) AS COL237 
                                        , FN_GET_CONV_NA(A.COL238) AS COL238 
                                        , FN_GET_CONV_NA(A.COL239) AS COL239 
                                        , FN_GET_CONV_NA(A.COL240) AS COL240    
                                    FROM TB_RAW_EMS_INFO_TMP A   
                                ) A  
                                , TB_BAS_DEVICE B  
                                , TB_BAS_CITY CT  
                            WHERE 	A.DEVICE_ID = B.DEVICE_ID   
                                AND 	B.CITY_CD = CT.CITY_CD   
                        ) A  
                        , (SELECT TO_CHAR(TM, '09') HH FROM GENERATE_SERIES(0, 23) TM) H 
                    GROUP BY  
                        A.COLEC_DT 
                        , A.DEVICE_ID 
                        , A.DEVICE_TYPE_CD 
                        , A.CNTRY_CD 
                        , A.ELPW_PROD_CD 
                        , A.UTC_COLEC_DT 
                        , A.UTC_OFFSET 
                        , H.HH 
                ) A 
                , TB_PLF_CARB_EMI_QUITY D  
                , TB_BAS_ELPW_UNIT_PRICE E  
                , TB_BAS_ELPW_UNIT_PRICE ET 
            WHERE  
                A.CNTRY_CD = D.CNTRY_CD  
                AND D.USE_FLAG = 'Y'  
                AND A.ELPW_PROD_CD = E.ELPW_PROD_CD 
                AND E.IO_FLAG = 'O' 
                AND E.USE_FLAG = 'Y'  
            -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가
                AND E.WKDAY_FLAG = FN_GET_WKDAY_FLAG(PREDICT_DD) 
            -- 2015.07.31 요금제 년월 변경
            --  AND SUBSTR(PREDICT_DD, 1, 6) = E.UNIT_YM  
                AND E.UNIT_YM = '000000' 
                AND PREDICT_HH = E.UNIT_HH  
                AND A.ELPW_PROD_CD = ET.ELPW_PROD_CD 
                AND ET.IO_FLAG = 'T' 
                AND ET.USE_FLAG = 'Y'  
            -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가, T 는  1 만 있음
            -- 2015.08.06 modified by yngwie T 도 주말 0 추가
            --  AND ET.WKDAY_FLAG = '1'  
                AND ET.WKDAY_FLAG = FN_GET_WKDAY_FLAG(PREDICT_DD) 

            -- 2015.07.31 요금제 년월 변경
            --  AND SUBSTR(PREDICT_DD, 1, 6) = ET.UNIT_YM 
                AND ET.UNIT_YM ='000000'
                AND PREDICT_HH = ET.UNIT_HH 
            ;
            
           GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	      	RAISE notice 'i_sp_nm[%] Insert PREDICT Tmp  Table :  [%]건', i_sp_nm, v_work_num;
			
   			v_rslt_cd := 'Q';

            -- 예측정보 Tmp 테이블의 레코드를 예측정보이력 테이블에 Insert 
            DELETE FROM TB_OPR_ESS_PREDICT_HIST
            WHERE (COLEC_DT, DEVICE_ID, DEVICE_TYPE_CD) IN ( 
                SELECT COLEC_DT, DEVICE_ID, DEVICE_TYPE_CD FROM TB_OPR_ESS_PREDICT_TMP 
            ) ;
           
           GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;
	      	RAISE notice 'i_sp_nm[%] Delete TB_OPR_ESS_PREDICT_HIST Old Records : [%]건', i_sp_nm, v_work_num;
            
   			v_rslt_cd := 'R';

            INSERT INTO TB_OPR_ESS_PREDICT_HIST ( 
                COLEC_DT 
                , DEVICE_ID 
                , DEVICE_TYPE_CD 
                , PREDICT_DD 
                , PREDICT_HH 
                , CONS_PREDICT_PW_H 
                , CONS_PREDICT_CO2   
                , CONS_PREDICT_PRICE 
                , PV_PREDICT_PW_H    
                , PV_PREDICT_CO2     
                , PV_PREDICT_PRICE   
            ) 
            SELECT 
                COLEC_DT 
                , DEVICE_ID 
                , DEVICE_TYPE_CD 
                , PREDICT_DD 
                , PREDICT_HH 
                , CONS_PREDICT_PW_H 
                , CONS_PREDICT_CO2   
                , CONS_PREDICT_PRICE 
                , PV_PREDICT_PW_H    
                , PV_PREDICT_CO2     
                , PV_PREDICT_PRICE 
            FROM 
                TB_OPR_ESS_PREDICT_TMP 
            ;
           
           GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;
	      	RAISE NOTICE 'i_sp_nm[%] TB_OPR_ESS_PREDICT_HIST New Records : [%]건', i_sp_nm, v_work_num;
            
            -- 2015.06.23 yngwie
            -- 일자별로 가장 최근 데이터만 유지하도록 변경, 관련 일감 #129 
            SELECT 	SUBSTR(MAX(COLEC_DT), 1, 8) 
            INTO 		v_selectDt
            FROM 	TB_OPR_ESS_PREDICT_TMP;
            
            --2016.02.17 하루 이상 데이터가 적재되지 않은 경우 v_selectDt가 NULL이 되면서 아래 DELETE문 수행속도에 지장이 발생하여 오늘날짜로 입력 이지훈
            IF v_selectDt IS NULL THEN
                SELECT TO_CHAR(NOW(), 'YYYYMMDD') INTO v_selectDt;
            END IF; 
            
	      	RAISE notice 'i_sp_nm[%] TB_OPR_ESS_PREDICT_TMP 최근일 : [%]', i_sp_nm, v_selectDt;

  			v_rslt_cd := 'S';

            DELETE FROM TB_OPR_ESS_PREDICT_HIST 
--            WHERE 	COLEC_DT LIKE (v_selectDt || '%')
            WHERE 	(COLEC_DT >= v_selectDt||'000000' AND COLEC_DT <= v_selectDt||'235959')
            AND 		(COLEC_DT, DEVICE_ID) NOT IN ( 
				            SELECT 	MAX(COLEC_DT) 
				                		, DEVICE_ID 
				            FROM		TB_OPR_ESS_PREDICT_HIST
				            WHERE 	(COLEC_DT >= v_selectDt||'000000' AND COLEC_DT <= v_selectDt||'235959')
--				            WHERE 	SUBSTR(COLEC_DT, 1, 8) = v_selectDt
				            GROUP BY DEVICE_ID 
				            ) 
            ;

            GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	      	RAISE notice 'i_sp_nm[%] Delete TB_OPR_ESS_PREDICT_HIST : [%], v_selectDt[%]', i_sp_nm, v_work_num, v_selectDt;
        

            ------------------------
            -- 3.2 예측정보 ADVICE 데이터 처리
            ------------------------
            -- 우선 장애 대응을 위해 기능 Disable 이지훈
           SELECT COUNT(*)  
           INTO v_ExeCnt1
           FROM TB_RAW_EMS_INFO_TMP;

	      RAISE notice 'i_sp_nm[%] SELECT COUNT(*) FROM TB_RAW_EMS_INFO_TMP :  [%]', i_sp_nm, v_ExeCnt1;

	      
           	SELECT 	COUNT(COLEC_DT) 
            INTO 		v_ExeCnt1
           	FROM 	TB_RAW_EMS_INFO_TMP 
           	WHERE 	COLEC_DT IS NULL;
           
	      	RAISE notice 'i_sp_nm[%] SELECT COUNT(COLEC_DT) FROM TB_RAW_EMS_INFO_TMP WHERE COLEC_DT IS NULL : [%]', i_sp_nm, v_ExeCnt1;
            
  			v_rslt_cd := 'T';
  
            WITH B AS ( 
                SELECT 
                    FN_TS_STR_TO_STR(A.COLEC_DT, 0, 'YYYYMMDDHH24MISS', CT.UTC_OFFSET, 'YYYYMMDD') AS COLEC_DD 
                    , A.DEVICE_ID 
                    , B.DEVICE_TYPE_CD 
                    , CASE WHEN COL254 IS NULL THEN NULL ELSE 
                      FN_GET_FMT_NUM( COL254 ) 
                      || '|' || FN_GET_FMT_NUM( COL253 )
                      || '|' || FN_GET_FMT_NUM( COL257 )
                      || '|' || FN_GET_FMT_NUM( COL258 )
                      || '|' || FN_GET_FMT_NUM( COL251 )
                      || '|' || FN_GET_FMT_NUM( COL252 )
                      || '|' || COL259 * 100
                      || '|' || COL260 * 100 END AS DFLT_REF 
                    , 1 AS CASE_CNT 
                    , CASE WHEN (COL251 > B.CAPACITY::NUMERIC / 12) AND COL252 > B.CAPACITY::NUMERIC / 12 THEN 'Y' ELSE 'N' END CASE_YN 
                    , FN_GET_FMT_NUM( B.CAPACITY::NUMERIC / 12 )
                      || '|' || COL261 * 10
                      || '|' || E.REF_1
                      || '|' || FN_GET_FMT_NUM( COL262 ) AS CASE_REF 
                FROM 
                    ( 
                        SELECT 
                            ROW_NUMBER() OVER (PARTITION BY DEVICE_ID ORDER BY COLEC_DT DESC ) SEQ 
                            , COLEC_DT 
                            , DEVICE_ID 
                            , CASE WHEN COL251 = 'NA' THEN NULL ELSE (COL251)::NUMERIC END AS COL251 
                            , CASE WHEN COL252 = 'NA' THEN NULL ELSE (COL252)::NUMERIC END AS COL252 
                            , CASE WHEN COL253 = 'NA' THEN NULL ELSE (COL253)::NUMERIC END AS COL253 
                            , CASE WHEN COL254 = 'NA' THEN NULL ELSE (COL254)::NUMERIC END AS COL254 
                            , CASE WHEN COL255 = 'NA' THEN NULL ELSE (COL255)::NUMERIC END AS COL255 
                            , CASE WHEN COL256 = 'NA' THEN NULL ELSE (COL256)::NUMERIC END AS COL256 
                            , CASE WHEN COL257 = 'NA' THEN NULL ELSE (COL257)::NUMERIC END AS COL257 
                            , CASE WHEN COL258 = 'NA' THEN NULL ELSE (COL258)::NUMERIC END AS COL258 
                            , CASE WHEN COL259 = 'NA' THEN NULL ELSE (COL259)::NUMERIC END AS COL259 
                            , CASE WHEN COL260 = 'NA' THEN NULL ELSE (COL260)::NUMERIC END AS COL260 
                            , CASE WHEN COL261 = 'NA' THEN NULL ELSE (COL261)::NUMERIC END AS COL261 
                            , CASE WHEN COL262 = 'NA' THEN NULL ELSE (COL262)::NUMERIC END AS COL262 
                        FROM    TB_RAW_EMS_INFO_TMP 
                     ) A 
                     , TB_BAS_DEVICE B   
                     , TB_BAS_CITY CT 
                     , TB_BAS_CNTRY D 
                     , (SELECT CODE, REF_1 FROM TB_PLF_CODE_INFO WHERE GRP_CD = 'MONE_UNIT_CD') E 
                WHERE  
                    A.SEQ = 1 
                    AND A.DEVICE_ID = B.DEVICE_ID    
                    AND B.CITY_CD = CT.CITY_CD   
                    AND B.CNTRY_CD = D.CNTRY_CD 
                    AND D.MONE_UNIT_CD = E.CODE 
            ), U AS (
	            UPDATE  TB_STT_PREDICT_ADVICE_DD A  
	            SET 
	                 DFLT_REF = B.DFLT_REF 
	                , CASE_CNT = B.CASE_CNT 
	                , CASE_YN  = B.CASE_YN 
	                , CASE_REF = B.CASE_REF 
	                , CREATE_DT = SYS_EXTRACT_UTC(NOW()) 
	            FROM B
	            WHERE
	                A.COLEC_DD = B.COLEC_DD 
	                AND A.DEVICE_ID = B.DEVICE_ID 
	                AND A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD 
	                
	           RETURNING A.*
            ) 
            INSERT INTO TB_STT_PREDICT_ADVICE_DD ( 
                  COLEC_DD 
                , DEVICE_ID 
                , DEVICE_TYPE_CD 
                , DFLT_REF 
                , CASE_CNT 
                , CASE_YN 
                , CASE_REF 
            ) 
            SELECT
                  B.COLEC_DD 
                , B.DEVICE_ID 
                , B.DEVICE_TYPE_CD 
                , B.DFLT_REF 
                , B.CASE_CNT 
                , B.CASE_YN 
                , B.CASE_REF 
			FROM B
			WHERE NOT EXISTS (  
				SELECT 	1 
			    FROM 	U 
				WHERE	COLEC_DD 			= B.COLEC_DD  
					AND 	DEVICE_ID			= B.DEVICE_ID  
					AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
			  )
			 ;

            
            GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	      	RAISE notice 'i_sp_nm[%] Merge TB_STT_PREDICT_ADVICE_DD Records : [%]', i_sp_nm, v_work_num;
            */
           
--		ELSE 
--    		RAISE NOTICE 'i_sp_nm[%] No New Record', i_sp_nm;

        END IF;
       
--    	RAISE NOTICE 'i_sp_nm[%] SP End !!! ', i_sp_nm;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
        
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
        
--       	RAISE NOTICE 'i_sp_nm[%] SP EXCEPTION ERR[%]', i_sp_nm, v_err_msg;

    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
	return v_rslt_cd;

END;


$function$
;

-- Drop table

-- DROP TABLE wems.tb_opr_ctrl_result;

CREATE TABLE wems.tb_opr_ctrl_result (
	req_id numeric(18) NOT NULL, -- RequestId
	req_type_cd varchar(4) NOT NULL, -- Request Type 
	device_id varchar(50) NOT NULL, -- 장비ID
	req_tm varchar(14) NULL, -- 명령전송시간
	res_tm varchar(14) NULL, -- 응답수신시간
	result_cd varchar(4) NULL, -- 결과코드
	cmd_type_cd varchar(4) NULL, -- 명령종류
	logfilename varchar(255) NULL, -- 로그파일명
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일자
	CONSTRAINT pk_opr_ctrl_result PRIMARY KEY (req_id, req_type_cd, device_id)
);
COMMENT ON TABLE wems.tb_opr_ctrl_result IS 'ESS 제어결과 내역을 보관한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_ctrl_result.req_id IS 'RequestId';
COMMENT ON COLUMN wems.tb_opr_ctrl_result.req_type_cd IS 'Request Type ';
COMMENT ON COLUMN wems.tb_opr_ctrl_result.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_opr_ctrl_result.req_tm IS '명령전송시간';
COMMENT ON COLUMN wems.tb_opr_ctrl_result.res_tm IS '응답수신시간';
COMMENT ON COLUMN wems.tb_opr_ctrl_result.result_cd IS '결과코드';
COMMENT ON COLUMN wems.tb_opr_ctrl_result.cmd_type_cd IS '명령종류';
COMMENT ON COLUMN wems.tb_opr_ctrl_result.logfilename IS '로그파일명';
COMMENT ON COLUMN wems.tb_opr_ctrl_result.create_dt IS '생성일자';

-- Permissions

ALTER TABLE wems.tb_opr_ctrl_result OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_ctrl_result TO wems;

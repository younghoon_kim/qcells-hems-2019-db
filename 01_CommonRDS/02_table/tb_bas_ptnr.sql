-- Drop table

-- DROP TABLE wems.tb_bas_ptnr;

CREATE TABLE wems.tb_bas_ptnr (
	ptnr_id varchar(10) NOT NULL, -- 판매처 코드
	ptnr_nm varchar(1000) NOT NULL, -- 판매처 명
	cntry_cd varchar(4) NOT NULL, -- 국가코드
	homepage varchar(1000) NULL, -- 홈페이지 URL (암호화)
	addr varchar(4000) NULL, -- 주소 (암호화)
	latitude float8 NULL, -- 위도
	longitude float8 NULL, -- 경도
	pctr_file varchar(4000) NULL, -- 이미지 파일(암호화)
	pctr_file_path varchar(4000) NULL, -- 파일 경로(암호화)
	pctr_file_disp_name varchar(4000) NULL, -- 화면에 표시되는 명칭
	create_id varchar(64) NULL, -- 등록자ID (암호화)
	create_dt timestamptz NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_bas_ptnr PRIMARY KEY (ptnr_id)
);
COMMENT ON TABLE wems.tb_bas_ptnr IS '판매처 관리';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_ptnr.ptnr_id IS '판매처 코드';
COMMENT ON COLUMN wems.tb_bas_ptnr.ptnr_nm IS '판매처 명';
COMMENT ON COLUMN wems.tb_bas_ptnr.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_bas_ptnr.homepage IS '홈페이지 URL (암호화)';
COMMENT ON COLUMN wems.tb_bas_ptnr.addr IS '주소 (암호화)';
COMMENT ON COLUMN wems.tb_bas_ptnr.latitude IS '위도';
COMMENT ON COLUMN wems.tb_bas_ptnr.longitude IS '경도';
COMMENT ON COLUMN wems.tb_bas_ptnr.pctr_file IS '이미지 파일(암호화)';
COMMENT ON COLUMN wems.tb_bas_ptnr.pctr_file_path IS '파일 경로(암호화)';
COMMENT ON COLUMN wems.tb_bas_ptnr.pctr_file_disp_name IS '화면에 표시되는 명칭';
COMMENT ON COLUMN wems.tb_bas_ptnr.create_id IS '등록자ID (암호화)';
COMMENT ON COLUMN wems.tb_bas_ptnr.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_ptnr.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_bas_ptnr.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_bas_ptnr OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_ptnr TO wems;

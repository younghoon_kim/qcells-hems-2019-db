-- Drop table

-- DROP TABLE wems.tb_plf_daq_type;

CREATE TABLE wems.tb_plf_daq_type (
	daq_type_cd varchar(3) NOT NULL, -- 수집유형코드
	value_ver varchar(10) NOT NULL, -- 수집유형버전
	daq_type_nm varchar(50) NULL, -- 수집유형명
	daq_type_desc varchar(255) NULL, -- 수집유형상세
	min_value numeric(20,2) NULL, -- 최저값
	max_value numeric(20,2) NULL, -- 최고값
	value_desc varchar(255) NULL, -- 값설명
	unit_type varchar(4) NULL, -- 단위유형
	value_nm varchar(30) NULL, -- 값이름
	value_ord numeric(4) NULL, -- 값순서
	device_type_cd varchar(4) NULL, -- 장비타입코드
	CONSTRAINT pk_plf_daq_type PRIMARY KEY (daq_type_cd, value_ver)
);
COMMENT ON TABLE wems.tb_plf_daq_type IS 'ESS 연동시 필요한 수집유형코드를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_daq_type.daq_type_cd IS '수집유형코드';
COMMENT ON COLUMN wems.tb_plf_daq_type.value_ver IS '수집유형버전';
COMMENT ON COLUMN wems.tb_plf_daq_type.daq_type_nm IS '수집유형명';
COMMENT ON COLUMN wems.tb_plf_daq_type.daq_type_desc IS '수집유형상세';
COMMENT ON COLUMN wems.tb_plf_daq_type.min_value IS '최저값';
COMMENT ON COLUMN wems.tb_plf_daq_type.max_value IS '최고값';
COMMENT ON COLUMN wems.tb_plf_daq_type.value_desc IS '값설명';
COMMENT ON COLUMN wems.tb_plf_daq_type.unit_type IS '단위유형';
COMMENT ON COLUMN wems.tb_plf_daq_type.value_nm IS '값이름';
COMMENT ON COLUMN wems.tb_plf_daq_type.value_ord IS '값순서';
COMMENT ON COLUMN wems.tb_plf_daq_type.device_type_cd IS '장비타입코드';

-- Permissions

ALTER TABLE wems.tb_plf_daq_type OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_daq_type TO wems;

-- Drop table

-- DROP TABLE wems.tbd_eqp_hosthw;

CREATE TABLE wems.tbd_eqp_hosthw (
	"name" varchar(50) NULL,
	ip varchar(50) NOT NULL,
	agentequipid varchar(40) NOT NULL,
	description varchar(500) NULL
);

-- Permissions

ALTER TABLE wems.tbd_eqp_hosthw OWNER TO wems;
GRANT ALL ON TABLE wems.tbd_eqp_hosthw TO wems;

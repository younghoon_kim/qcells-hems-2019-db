-- Drop table

-- DROP TABLE wems.tb_opr_ess_state_tl;

CREATE TABLE wems.tb_opr_ess_state_tl (
	colec_dd varchar(8) NOT NULL, -- 수집일
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	oper_stus_cd varchar(4) NULL, -- 운전상태코드(0:운전중,1:경고,2:오류,3:통신불가, 4:미인증)
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()) -- 등록일시
)
PARTITION BY RANGE (colec_dd);
CREATE UNIQUE INDEX pk_opr_ess_state_tl ON ONLY wems.tb_opr_ess_state_tl USING btree (colec_dd, device_id, device_type_cd);
COMMENT ON TABLE wems.tb_opr_ess_state_tl IS 'ESS 일별 상태 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_ess_state_tl.colec_dd IS '수집일';
COMMENT ON COLUMN wems.tb_opr_ess_state_tl.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_opr_ess_state_tl.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_tl.oper_stus_cd IS '운전상태코드(0:운전중,1:경고,2:오류,3:통신불가, 4:미인증)';
COMMENT ON COLUMN wems.tb_opr_ess_state_tl.create_dt IS '등록일시';

-- Permissions

ALTER TABLE wems.tb_opr_ess_state_tl OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_ess_state_tl TO wems;

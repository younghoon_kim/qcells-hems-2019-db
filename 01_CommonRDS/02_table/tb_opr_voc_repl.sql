-- Drop table

-- DROP TABLE wems.tb_opr_voc_repl;

CREATE TABLE wems.tb_opr_voc_repl (
	voc_seq float8 NOT NULL, --  VOC 일련번호
	ord_seq float8 NOT NULL DEFAULT 1, -- 순번
	contents1 varchar(4000) NOT NULL, -- 내용1
	contents2 varchar(4000) NULL, -- 내용2
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_opr_voc_repl PRIMARY KEY (voc_seq, ord_seq)
);
COMMENT ON TABLE wems.tb_opr_voc_repl IS 'VOC에 등록되는 댓글 정보';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_voc_repl.voc_seq IS ' VOC 일련번호';
COMMENT ON COLUMN wems.tb_opr_voc_repl.ord_seq IS '순번';
COMMENT ON COLUMN wems.tb_opr_voc_repl.contents1 IS '내용1';
COMMENT ON COLUMN wems.tb_opr_voc_repl.contents2 IS '내용2';
COMMENT ON COLUMN wems.tb_opr_voc_repl.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_opr_voc_repl.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_opr_voc_repl.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_opr_voc_repl OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_voc_repl TO wems;

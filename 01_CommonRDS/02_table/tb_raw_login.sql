-- Drop table

-- DROP TABLE wems.tb_raw_login;

CREATE TABLE wems.tb_raw_login (
	device_id varchar(50) NOT NULL, -- 장비ID
	create_tm varchar(14) NOT NULL, -- 생성시간
	product_model_nm varchar(30) NULL, -- 제품모델명
	"password" varchar(255) NULL, -- 패스워드
	ems_model_nm varchar(30) NULL, -- EMS모델명
	pcs_model_nm varchar(30) NULL, -- PCS모델명
	bms_model_nm varchar(30) NULL, -- BMS모델명
	ems_ver varchar(30) NULL, -- EMSVersion
	pcs_ver varchar(30) NULL, -- PCSVersion
	bms_ver varchar(30) NULL, -- BMSVersion
	ems_update_dt varchar(14) NULL, -- RecentEMSUpdateDate
	ems_update_err_cd varchar(4) NULL, -- RecentEMSUpdateErrorCode
	pcs_update_dt varchar(14) NULL, -- RecentPCSUpdateDate
	pcs_update_err_cd varchar(4) NULL, -- RecentPCSUpdateErrorCode
	bms_update_dt varchar(14) NULL, -- RecentBMSUpdateDate
	bms_update_err_cd varchar(4) NULL, -- RecentBMSUpdateErrorCode
	response_cd varchar(4) NULL, -- ESS로전송한 결과코드
	ems_ipaddress varchar(30) NULL, -- EMS 로컬 IPADDRESS
	ems_out_ipaddress varchar(30) NULL, -- EMS PUBLIC IPADDRESS
	connector_id varchar(30) NULL, -- CONNECTOR ID
	ems_tm varchar(14) NOT NULL, -- EMS시간
	data_seq float8 NOT NULL DEFAULT nextval('wems.sq_raw_login'::regclass), -- 순차번호
	create_dt timestamp NULL DEFAULT wems.sys_extract_utc(now()) -- 생성일시
)
PARTITION BY RANGE (create_dt);
CREATE UNIQUE INDEX pk_raw_login ON ONLY wems.tb_raw_login USING btree (device_id, create_dt, create_tm, response_cd, ems_out_ipaddress, ems_tm, data_seq);
COMMENT ON TABLE wems.tb_raw_login IS 'EMS 가 M2M 서버로 접속한 이력을 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_raw_login.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_raw_login.create_tm IS '생성시간';
COMMENT ON COLUMN wems.tb_raw_login.product_model_nm IS '제품모델명';
COMMENT ON COLUMN wems.tb_raw_login."password" IS '패스워드';
COMMENT ON COLUMN wems.tb_raw_login.ems_model_nm IS 'EMS모델명';
COMMENT ON COLUMN wems.tb_raw_login.pcs_model_nm IS 'PCS모델명';
COMMENT ON COLUMN wems.tb_raw_login.bms_model_nm IS 'BMS모델명';
COMMENT ON COLUMN wems.tb_raw_login.ems_ver IS 'EMSVersion';
COMMENT ON COLUMN wems.tb_raw_login.pcs_ver IS 'PCSVersion';
COMMENT ON COLUMN wems.tb_raw_login.bms_ver IS 'BMSVersion';
COMMENT ON COLUMN wems.tb_raw_login.ems_update_dt IS 'RecentEMSUpdateDate';
COMMENT ON COLUMN wems.tb_raw_login.ems_update_err_cd IS 'RecentEMSUpdateErrorCode';
COMMENT ON COLUMN wems.tb_raw_login.pcs_update_dt IS 'RecentPCSUpdateDate';
COMMENT ON COLUMN wems.tb_raw_login.pcs_update_err_cd IS 'RecentPCSUpdateErrorCode';
COMMENT ON COLUMN wems.tb_raw_login.bms_update_dt IS 'RecentBMSUpdateDate';
COMMENT ON COLUMN wems.tb_raw_login.bms_update_err_cd IS 'RecentBMSUpdateErrorCode';
COMMENT ON COLUMN wems.tb_raw_login.response_cd IS 'ESS로전송한 결과코드';
COMMENT ON COLUMN wems.tb_raw_login.ems_ipaddress IS 'EMS 로컬 IPADDRESS';
COMMENT ON COLUMN wems.tb_raw_login.ems_out_ipaddress IS 'EMS PUBLIC IPADDRESS';
COMMENT ON COLUMN wems.tb_raw_login.connector_id IS 'CONNECTOR ID';
COMMENT ON COLUMN wems.tb_raw_login.ems_tm IS 'EMS시간';
COMMENT ON COLUMN wems.tb_raw_login.data_seq IS '순차번호';
COMMENT ON COLUMN wems.tb_raw_login.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_raw_login OWNER TO wems;
GRANT ALL ON TABLE wems.tb_raw_login TO wems;

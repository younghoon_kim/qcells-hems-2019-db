-- Drop table

-- DROP TABLE wems.dc_rul_rule;

CREATE TABLE wems.dc_rul_rule (
	id varchar(80) NOT NULL,
	generationcode varchar(10) NULL,
	vendercode varchar(10) NULL,
	"version" varchar(20) NULL,
	identificationtype numeric(3) NULL,
	linedecimal numeric(18) NULL,
	update_user varchar(50) NULL,
	update_date timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now())
);

-- Permissions

ALTER TABLE wems.dc_rul_rule OWNER TO wems;
GRANT ALL ON TABLE wems.dc_rul_rule TO wems;

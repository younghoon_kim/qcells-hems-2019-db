-- Drop table

-- DROP TABLE wems.batch_job_instance;

CREATE TABLE wems.batch_job_instance (
	job_instance_id int8 NOT NULL,
	"version" int8 NULL,
	job_name varchar(100) NOT NULL,
	job_key varchar(32) NOT NULL,
	CONSTRAINT batch_job_instance_pkey PRIMARY KEY (job_instance_id)
);

-- Permissions

ALTER TABLE wems.batch_job_instance OWNER TO wems;
GRANT ALL ON TABLE wems.batch_job_instance TO wems;

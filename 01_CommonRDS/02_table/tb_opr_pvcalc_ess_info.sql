-- Drop table

-- DROP TABLE wems.tb_opr_pvcalc_ess_info;

CREATE TABLE wems.tb_opr_pvcalc_ess_info (
	ess_model_nm varchar(128) NOT NULL, -- ESS 모델명
	ess_img_file_nm varchar(256) NOT NULL, -- ESS 모델 이미지 파일명
	ess_pv_r_v numeric(18,2) NOT NULL, -- ESS MPPT 최대효율
	ess_power numeric(18,2) NOT NULL, -- ESS 정격 파워
	ess_voc numeric(18,2) NOT NULL, -- ESS Over Circuit 전압
	ess_vmppt numeric(18,2) NULL, -- ESS MPPT 전압
	ess_imp numeric(18,2) NOT NULL, -- PV 최대 전력
	use_flag varchar(4) NULL DEFAULT 'Y'::character varying, --  USE_FLAG
	create_id varchar(64) NULL, -- 등록자ID (암호화)
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	CONSTRAINT pk_opr_pvcalc_ess_info PRIMARY KEY (ess_model_nm)
);
COMMENT ON TABLE wems.tb_opr_pvcalc_ess_info IS 'PV 계산기를 위한 ESS 모델 정보';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_pvcalc_ess_info.ess_model_nm IS 'ESS 모델명';
COMMENT ON COLUMN wems.tb_opr_pvcalc_ess_info.ess_img_file_nm IS 'ESS 모델 이미지 파일명';
COMMENT ON COLUMN wems.tb_opr_pvcalc_ess_info.ess_pv_r_v IS 'ESS MPPT 최대효율';
COMMENT ON COLUMN wems.tb_opr_pvcalc_ess_info.ess_power IS 'ESS 정격 파워';
COMMENT ON COLUMN wems.tb_opr_pvcalc_ess_info.ess_voc IS 'ESS Over Circuit 전압';
COMMENT ON COLUMN wems.tb_opr_pvcalc_ess_info.ess_vmppt IS 'ESS MPPT 전압';
COMMENT ON COLUMN wems.tb_opr_pvcalc_ess_info.ess_imp IS 'PV 최대 전력';
COMMENT ON COLUMN wems.tb_opr_pvcalc_ess_info.use_flag IS ' USE_FLAG';
COMMENT ON COLUMN wems.tb_opr_pvcalc_ess_info.create_id IS '등록자ID (암호화)';
COMMENT ON COLUMN wems.tb_opr_pvcalc_ess_info.create_dt IS '등록일시';

-- Permissions

ALTER TABLE wems.tb_opr_pvcalc_ess_info OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_pvcalc_ess_info TO wems;

-- Drop table

-- DROP TABLE wems.tb_opr_cust_qna_file;

CREATE TABLE wems.tb_opr_cust_qna_file (
	cust_qna_seq float8 NOT NULL, -- 일련번호
	ord_seq float8 NOT NULL DEFAULT 1, -- 순번
	file_name varchar(4000) NOT NULL, -- 파일명(암호화)
	file_path varchar(4000) NOT NULL, -- 파일 경로(암호화)
	disp_name varchar(4000) NOT NULL, -- 화면에 표시되는 명칭
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	CONSTRAINT pk_opr_cust_qna_file PRIMARY KEY (cust_qna_seq, ord_seq)
);
COMMENT ON TABLE wems.tb_opr_cust_qna_file IS '고객문의 첨부파일 목록';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_cust_qna_file.cust_qna_seq IS '일련번호';
COMMENT ON COLUMN wems.tb_opr_cust_qna_file.ord_seq IS '순번';
COMMENT ON COLUMN wems.tb_opr_cust_qna_file.file_name IS '파일명(암호화)';
COMMENT ON COLUMN wems.tb_opr_cust_qna_file.file_path IS '파일 경로(암호화)';
COMMENT ON COLUMN wems.tb_opr_cust_qna_file.disp_name IS '화면에 표시되는 명칭';
COMMENT ON COLUMN wems.tb_opr_cust_qna_file.create_dt IS '등록일시';

-- Permissions

ALTER TABLE wems.tb_opr_cust_qna_file OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_cust_qna_file TO wems;

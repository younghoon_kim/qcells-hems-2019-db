-- Drop table

-- DROP TABLE wems.dc_cnf_event_consumer;

CREATE TABLE wems.dc_cnf_event_consumer (
	id varchar(80) NOT NULL,
	dbuserid varchar(16) NULL,
	dbpassword varchar(16) NULL,
	dbtns varchar(16) NULL,
	hostname varchar(40) NULL,
	timemode numeric(1) NULL,
	listenport numeric(8) NULL,
	status numeric(1) NULL,
	logmode numeric(1) NULL,
	ipaddress varchar(20) NULL,
	bypasslistenport numeric(8) NULL,
	loadinginterval numeric(18) NULL,
	handlermode numeric(1) NULL,
	targetinfo varchar(200) NULL,
	logcycle numeric(1) NULL,
	runmode numeric(1) NULL,
	managerid varchar(40) NULL,
	subopermode float8 NULL
);

-- Permissions

ALTER TABLE wems.dc_cnf_event_consumer OWNER TO wems;
GRANT ALL ON TABLE wems.dc_cnf_event_consumer TO wems;

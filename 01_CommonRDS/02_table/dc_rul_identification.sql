-- Drop table

-- DROP TABLE wems.dc_rul_identification;

CREATE TABLE wems.dc_rul_identification (
	id varchar(80) NOT NULL,
	ruleid varchar(80) NOT NULL,
	parentid varchar(80) NULL,
	parsingruleid numeric(18) NULL,
	idstring varchar(80) NOT NULL,
	outputflag numeric(1) NULL,
	parsingflag numeric(1) NULL,
	rawdataflag numeric(1) NULL,
	update_user varchar(50) NULL,
	update_date timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()),
	xmlflag float8 NULL,
	editoruseflag float8 NULL,
	default_ident_flag float8 NULL
);

-- Permissions

ALTER TABLE wems.dc_rul_identification OWNER TO wems;
GRANT ALL ON TABLE wems.dc_rul_identification TO wems;

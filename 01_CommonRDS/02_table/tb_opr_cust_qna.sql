-- Drop table

-- DROP TABLE wems.tb_opr_cust_qna;

CREATE TABLE wems.tb_opr_cust_qna (
	cust_qna_seq float8 NOT NULL, -- 일련번호
	subject varchar(4000) NOT NULL, -- 제목
	contents1 varchar(4000) NOT NULL, -- 내용1
	contents2 varchar(4000) NULL, -- 내용2
	repl_flag varchar(1) NOT NULL DEFAULT 'N'::character varying, -- 댓글 등록 여부
	pwd varchar(4000) NOT NULL, -- 비밀번호 (암호화)
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_dt timestamptz NULL, -- 수정일시
	cust_nm varchar(4000) NULL, -- 고객명(암호화)
	cust_tel_no varchar(4000) NULL, -- 연락처(암호화)
	cust_email varchar(4000) NULL, -- 이메일(암호화)
	cust_addr varchar(4000) NULL, -- 주소(암호화)
	CONSTRAINT pk_opr_cust_qna PRIMARY KEY (cust_qna_seq)
);
COMMENT ON TABLE wems.tb_opr_cust_qna IS '고객문의';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_cust_qna.cust_qna_seq IS '일련번호';
COMMENT ON COLUMN wems.tb_opr_cust_qna.subject IS '제목';
COMMENT ON COLUMN wems.tb_opr_cust_qna.contents1 IS '내용1';
COMMENT ON COLUMN wems.tb_opr_cust_qna.contents2 IS '내용2';
COMMENT ON COLUMN wems.tb_opr_cust_qna.repl_flag IS '댓글 등록 여부';
COMMENT ON COLUMN wems.tb_opr_cust_qna.pwd IS '비밀번호 (암호화)';
COMMENT ON COLUMN wems.tb_opr_cust_qna.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_opr_cust_qna.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_opr_cust_qna.update_dt IS '수정일시';
COMMENT ON COLUMN wems.tb_opr_cust_qna.cust_nm IS '고객명(암호화)';
COMMENT ON COLUMN wems.tb_opr_cust_qna.cust_tel_no IS '연락처(암호화)';
COMMENT ON COLUMN wems.tb_opr_cust_qna.cust_email IS '이메일(암호화)';
COMMENT ON COLUMN wems.tb_opr_cust_qna.cust_addr IS '주소(암호화)';

-- Permissions

ALTER TABLE wems.tb_opr_cust_qna OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_cust_qna TO wems;

-- Drop table

-- DROP TABLE wems.tb_raw_curr_weather;

CREATE TABLE wems.tb_raw_curr_weather (
	city_cd varchar(20) NOT NULL, -- 도시코드
	colec_dt varchar(14) NOT NULL, -- 수집일시
	latitude varchar(13) NOT NULL, -- 위도
	longitude varchar(13) NOT NULL, -- 경도
	location_nm varchar(50) NULL, -- 지역명
	country varchar(50) NULL, -- 국가명
	day_type varchar(1) NOT NULL, -- 요일구분
	temperature varchar(50) NULL, -- 온도
	temperature_max varchar(50) NULL, -- 최고온도
	temperature_min varchar(50) NULL, -- 최저온도
	symbol_var varchar(50) NULL, -- 날씨코드
	symbol_cd varchar(50) NULL, -- 날씨요약코드
	symbol_name varchar(50) NULL, -- 날씨명
	symbol_num varchar(50) NULL, -- 날씨수치
	clouds_all varchar(50) NULL, -- 구름량
	clouds_value varchar(50) NULL, -- 구름상태설명
	precipication_mode varchar(50) NULL, -- 강수상태
	precipication varchar(50) NULL, -- 강수량
	wind_speed_mps varchar(50) NULL, -- 풍속
	wind_speed_name varchar(50) NULL, -- 풍속명
	wind_direction_cd varchar(50) NULL, -- 풍향
	wind_direction_deg varchar(50) NULL, -- 풍향각
	wind_direction_name varchar(50) NULL, -- 풍향명
	humidity varchar(50) NULL, -- 습도
	pressure varchar(50) NULL, -- 기압
	sunrise_dt varchar(50) NULL, -- 일출시간
	sunset_dt varchar(50) NULL, -- 일몰시간
	sunrise_idx varchar(4) NULL, -- 일출시간 15분 단위 인덱스
	sunset_idx varchar(4) NULL, -- 일몰시간 15분 단위 인덱스
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	CONSTRAINT pk_raw_curr_weather PRIMARY KEY (city_cd, colec_dt)
);
COMMENT ON TABLE wems.tb_raw_curr_weather IS '1시간 단위 현재 날씨 수집 데이터를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_raw_curr_weather.city_cd IS '도시코드';
COMMENT ON COLUMN wems.tb_raw_curr_weather.colec_dt IS '수집일시';
COMMENT ON COLUMN wems.tb_raw_curr_weather.latitude IS '위도';
COMMENT ON COLUMN wems.tb_raw_curr_weather.longitude IS '경도';
COMMENT ON COLUMN wems.tb_raw_curr_weather.location_nm IS '지역명';
COMMENT ON COLUMN wems.tb_raw_curr_weather.country IS '국가명';
COMMENT ON COLUMN wems.tb_raw_curr_weather.day_type IS '요일구분';
COMMENT ON COLUMN wems.tb_raw_curr_weather.temperature IS '온도';
COMMENT ON COLUMN wems.tb_raw_curr_weather.temperature_max IS '최고온도';
COMMENT ON COLUMN wems.tb_raw_curr_weather.temperature_min IS '최저온도';
COMMENT ON COLUMN wems.tb_raw_curr_weather.symbol_var IS '날씨코드';
COMMENT ON COLUMN wems.tb_raw_curr_weather.symbol_cd IS '날씨요약코드';
COMMENT ON COLUMN wems.tb_raw_curr_weather.symbol_name IS '날씨명';
COMMENT ON COLUMN wems.tb_raw_curr_weather.symbol_num IS '날씨수치';
COMMENT ON COLUMN wems.tb_raw_curr_weather.clouds_all IS '구름량';
COMMENT ON COLUMN wems.tb_raw_curr_weather.clouds_value IS '구름상태설명';
COMMENT ON COLUMN wems.tb_raw_curr_weather.precipication_mode IS '강수상태';
COMMENT ON COLUMN wems.tb_raw_curr_weather.precipication IS '강수량';
COMMENT ON COLUMN wems.tb_raw_curr_weather.wind_speed_mps IS '풍속';
COMMENT ON COLUMN wems.tb_raw_curr_weather.wind_speed_name IS '풍속명';
COMMENT ON COLUMN wems.tb_raw_curr_weather.wind_direction_cd IS '풍향';
COMMENT ON COLUMN wems.tb_raw_curr_weather.wind_direction_deg IS '풍향각';
COMMENT ON COLUMN wems.tb_raw_curr_weather.wind_direction_name IS '풍향명';
COMMENT ON COLUMN wems.tb_raw_curr_weather.humidity IS '습도';
COMMENT ON COLUMN wems.tb_raw_curr_weather.pressure IS '기압';
COMMENT ON COLUMN wems.tb_raw_curr_weather.sunrise_dt IS '일출시간';
COMMENT ON COLUMN wems.tb_raw_curr_weather.sunset_dt IS '일몰시간';
COMMENT ON COLUMN wems.tb_raw_curr_weather.sunrise_idx IS '일출시간 15분 단위 인덱스';
COMMENT ON COLUMN wems.tb_raw_curr_weather.sunset_idx IS '일몰시간 15분 단위 인덱스';
COMMENT ON COLUMN wems.tb_raw_curr_weather.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_raw_curr_weather OWNER TO wems;
GRANT ALL ON TABLE wems.tb_raw_curr_weather TO wems;

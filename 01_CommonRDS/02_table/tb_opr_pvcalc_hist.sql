-- Drop table

-- DROP TABLE wems.tb_opr_pvcalc_hist;

CREATE TABLE wems.tb_opr_pvcalc_hist (
	seq float8 NOT NULL, -- PC계산이력ID
	user_id varchar(64) NULL, -- 사용자ID (암호화)
	ess_model_nm varchar(128) NOT NULL, -- ESS 모델명
	pv_brand_nm varchar(128) NOT NULL, -- 패널 브랜드명
	pv_prod_model_nm varchar(128) NOT NULL, -- PV 패널 모델명
	pv_pmax numeric(18,2) NOT NULL, -- PV 패널 정격 파워
	pv_voc numeric(18,2) NOT NULL, -- PV 패널 Over Circuit 전압
	pv_vmpp numeric(18,2) NULL, -- PV 패널 최대전력의 전압
	pv_isc numeric(18,2) NOT NULL, -- PV 패널 short 전력
	min_series_mp numeric(18,2) NOT NULL, -- 중간값 직렬수 MAX POWER
	min_series_voc numeric(18,2) NOT NULL, -- 중간값 직렬수 VOC
	min_series_vmppt numeric(18,2) NOT NULL, -- 중간값 직렬수 VMPPT
	max_parallel_d numeric(18,2) NOT NULL, -- 최대값 병렬수 CURRENT
	max_series_a numeric(18,2) NOT NULL, -- 최대값 직렬수 중간값중 최소값
	max_c numeric(18,2) NOT NULL, -- 최대값 임시값
	basic_series_aa numeric(18,2) NOT NULL, -- 기본값 직렬수
	user_parallel numeric(18) NOT NULL, -- 사용자 병렬 입력값
	user_series numeric(18) NOT NULL, -- 사용자 직렬 입력값
	pmax_value numeric(18,2) NOT NULL, -- 최대전력값
	pmax_result numeric(18,2) NOT NULL, -- 최대전력결과
	voc_value numeric(18,2) NOT NULL, -- 개방전압값
	voc_result numeric(18,2) NOT NULL, -- 개방전압결과
	vmppt_value numeric(18,2) NOT NULL, -- MPPT전압값
	vmppt_result numeric(18,2) NOT NULL, -- MPPT전압결과
	isc_value numeric(18,2) NOT NULL, -- 단락전류값
	isc_result numeric(18,2) NOT NULL, -- 단락전류결과
	done_flag varchar(4) NULL, -- 데이터 처리여부
	use_flag varchar(4) NULL, -- 사용여부
	create_id varchar(64) NULL, -- 등록자ID (암호화)
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()) -- 등록일시
);
COMMENT ON TABLE wems.tb_opr_pvcalc_hist IS 'PV 계산기 운용 정보';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.seq IS 'PC계산이력ID';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.user_id IS '사용자ID (암호화)';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.ess_model_nm IS 'ESS 모델명';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.pv_brand_nm IS '패널 브랜드명';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.pv_prod_model_nm IS 'PV 패널 모델명';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.pv_pmax IS 'PV 패널 정격 파워';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.pv_voc IS 'PV 패널 Over Circuit 전압';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.pv_vmpp IS 'PV 패널 최대전력의 전압';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.pv_isc IS 'PV 패널 short 전력';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.min_series_mp IS '중간값 직렬수 MAX POWER';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.min_series_voc IS '중간값 직렬수 VOC';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.min_series_vmppt IS '중간값 직렬수 VMPPT';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.max_parallel_d IS '최대값 병렬수 CURRENT';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.max_series_a IS '최대값 직렬수 중간값중 최소값';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.max_c IS '최대값 임시값';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.basic_series_aa IS '기본값 직렬수';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.user_parallel IS '사용자 병렬 입력값';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.user_series IS '사용자 직렬 입력값';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.pmax_value IS '최대전력값';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.pmax_result IS '최대전력결과';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.voc_value IS '개방전압값';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.voc_result IS '개방전압결과';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.vmppt_value IS 'MPPT전압값';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.vmppt_result IS 'MPPT전압결과';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.isc_value IS '단락전류값';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.isc_result IS '단락전류결과';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.done_flag IS '데이터 처리여부';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.use_flag IS '사용여부';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.create_id IS '등록자ID (암호화)';
COMMENT ON COLUMN wems.tb_opr_pvcalc_hist.create_dt IS '등록일시';

-- Permissions

ALTER TABLE wems.tb_opr_pvcalc_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_pvcalc_hist TO wems;

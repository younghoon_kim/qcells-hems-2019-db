-- Drop table

-- DROP TABLE wems.tb_opr_ctrl_result_m2m;

CREATE TABLE wems.tb_opr_ctrl_result_m2m (
	req_id numeric(18) NULL,
	req_type_cd varchar(4) NULL,
	device_id varchar(50) NULL,
	req_tm varchar(14) NULL,
	res_tm varchar(14) NULL,
	result_cd varchar(4) NULL,
	cmd_type_cd varchar(4) NULL,
	logfilename varchar(255) NULL
);

-- Permissions

ALTER TABLE wems.tb_opr_ctrl_result_m2m OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_ctrl_result_m2m TO wems;
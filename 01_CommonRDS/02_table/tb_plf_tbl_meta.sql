-- Drop table

-- DROP TABLE wems.tb_plf_tbl_meta;

CREATE TABLE wems.tb_plf_tbl_meta (
	sub_area varchar(4) NOT NULL, -- 주제영역
	table_nm varchar(50) NOT NULL, -- 테이블 명
	table_type varchar(4) NULL, -- 테이블 유형
	arcv_flag varchar(1) NULL, -- 백업 여부
	arcv_period float8 NULL, -- 보존기간(개월)
	fst_load_dt varchar(8) NULL, -- 최초 적재 시점
	load_type varchar(10) NULL, -- 적재 형태
	partition_type varchar(4) NULL, -- 파티션 유형
	std_col_nm varchar(50) NULL, -- 기준 컬럼명
	std_col_type varchar(50) NULL, -- 기준 컬럼 타입
	del_flag varchar(1) NULL DEFAULT 'N'::character varying, -- 일정 기간 후 삭제 여부
	del_cycle float8 NULL, -- 삭제 주기(일)
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	CONSTRAINT pk_plf_tbl_meta PRIMARY KEY (sub_area, table_nm)
);
COMMENT ON TABLE wems.tb_plf_tbl_meta IS '테이블 메타정보';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_tbl_meta.sub_area IS '주제영역';
COMMENT ON COLUMN wems.tb_plf_tbl_meta.table_nm IS '테이블 명';
COMMENT ON COLUMN wems.tb_plf_tbl_meta.table_type IS '테이블 유형';
COMMENT ON COLUMN wems.tb_plf_tbl_meta.arcv_flag IS '백업 여부';
COMMENT ON COLUMN wems.tb_plf_tbl_meta.arcv_period IS '보존기간(개월)';
COMMENT ON COLUMN wems.tb_plf_tbl_meta.fst_load_dt IS '최초 적재 시점';
COMMENT ON COLUMN wems.tb_plf_tbl_meta.load_type IS '적재 형태';
COMMENT ON COLUMN wems.tb_plf_tbl_meta.partition_type IS '파티션 유형';
COMMENT ON COLUMN wems.tb_plf_tbl_meta.std_col_nm IS '기준 컬럼명';
COMMENT ON COLUMN wems.tb_plf_tbl_meta.std_col_type IS '기준 컬럼 타입';
COMMENT ON COLUMN tb_plf_tbl_meta.del_flag IS '일정 기간 후 삭제/유지/보관 여부를 Y/M/N로 나타낸다.Y=데이터삭제&파티션삭제 M=데이터삭제&파티션남김 N=데이터남김';
COMMENT ON COLUMN wems.tb_plf_tbl_meta.del_cycle IS '삭제 주기(일)';
COMMENT ON COLUMN wems.tb_plf_tbl_meta.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_plf_tbl_meta OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_tbl_meta TO wems;

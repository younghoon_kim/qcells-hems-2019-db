-- Drop table

-- DROP TABLE wems.tb_opr_ess_config_hist;

CREATE TABLE wems.tb_opr_ess_config_hist (
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	pv_max_pwr1 float8 NULL, -- 1번패널최대발전파워(W)
	pv_max_pwr2 float8 NULL, -- 2번패널최대발전파워(W)
	feed_in_limit varchar(4) NULL, -- 매전량(%)
	max_inverter_pw_cd varchar(4) NULL, -- 인버터최대출력(W)
	basicmode_cd varchar(4) NULL, -- 기본모드
	smtr_tp_cd varchar(4) NULL, -- 에너지미터기종류
	smtr_modl_cd varchar(4) NULL, -- 디지털미터기모델 코드
	smtr_pulse_cnt float8 NULL, -- 아날로그미터기1펄스(pulse/kwh)
	create_id varchar(64) NOT NULL, -- 생성자ID
	create_dt timestamptz NOT NULL, -- 생성일시
	grid_max_volt numeric(4,1) NULL, -- GRID MAX VOLTAGE
	grid_min_volt numeric(4,1) NULL, -- GRID MIN VOLTAGE
	grid_max_freq numeric(4,2) NULL, -- GRID 최대 주파수
	grid_min_freq numeric(4,2) NULL, -- GRID 최소 주파수
	rcp_p_enable varchar(1) NULL, -- Reactive Power Control Cos Pi(P) Enable 코드
	rcp_q_fix_enable varchar(1) NULL, -- Reactive Power Control Q fix Enable 코드
	rcp_p_excited varchar(1) NULL, -- Reactive Power Control Cos Pi(P) Excited 코드
	acp_pf_enable_cd varchar(1) NULL, -- Active Power Control P(F) Enable 코드
	acp_pu_enable_cd varchar(1) NULL, -- Active Power Control P(U) Enable 코드
	rcp_q_fix_excited varchar(1) NULL, -- Reactive Power Control Q fix Excited 코드
	rcp_f_enable_cd varchar(1) NULL, -- Reactive Power Control Cos Pi fest Enable 코드
	rcp_f_excited_cd varchar(1) NULL, -- Reactive Power Control Cos Pi fest Excited 코드
	acp_set_point numeric(3) NULL, -- Active Power Control Set Point
	acp_pe_max float8 NULL, -- 예비
	rcp_set_cos_pi numeric(3,2) NULL, -- Reactive Power Control Cos Pi fest Value
	dci_variable float8 NULL, -- 예비
	dci_trip_level float8 NULL, -- 예비
	rcp_set_x1 numeric(2,1) NULL, -- Reactive Power Control X1
	rcp_set_y1 numeric(3,2) NULL, -- Reactive Power Control Y1
	rcp_set_x2 numeric(2,1) NULL, -- Reactive Power Control X2
	rcp_set_y2 numeric(3,2) NULL, -- Reactive Power Control Y2
	rcp_set_x3 numeric(2,1) NULL, -- Reactive Power Control X3
	rcp_set_y3 numeric(3,1) NULL, -- Reactive Power Control Y3
	rcp_set_x4 numeric(2,1) NULL, -- Reactive Power Control X4
	rcp_set_y4 numeric(3,2) NULL, -- Reactive Power Control Y4
	rcp_q_fix_data numeric(4,3) NULL, -- Reactive Power Control Q fix Value
	algo_flag numeric(3) NULL, -- 비트연산(10진수)
	algo_flag2 numeric(3) NULL, -- 비트연산(10진수)
	pemmode numeric(1) NULL, -- 3rd Party Control (0=Disable, 1=Enable)
	backupmode numeric(1) NULL, -- Backup Mode (0=Disable, 1=Enable)
	islandingmode numeric(1) NULL, -- Islanding Mode (0=Disable, 1=Enable)
	feedinmode numeric(1) NULL, -- Feed In Relay (0=Disable, 1=Enable)
	relay1attachlevel numeric(4) NULL, -- Relay1 Attach Level (0~6000[W])
	relay1detachlevel numeric(4) NULL, -- Relay1 Detach Level (0~6000[W])
	relay2attachlevel numeric(4) NULL, -- Relay2 Attach Level (0~6000[W])
	relay2detachlevel numeric(4) NULL, -- Relay2 Detach Level (0~6000[W])
	relay3attachlevel numeric(4) NULL, -- Relay3 Attach Level (0~6000[W])
	relay3detachlevel numeric(4) NULL, -- Relay3 Detach Level (0~6000[W])
	relay4attachlevel numeric(4) NULL, -- Relay4 Attach Level (0~6000[W])
	relay4detachlevel numeric(4) NULL, -- Relay4 Detach Level (0~6000[W])
	faultlockmode numeric(1) NULL, -- Fault Lock (0=Disable, 1=Enable)
	faultlockvalue numeric(3) NULL, -- Fault Lock Vaue (0~100[count])
	faultlocktimelevel numeric(4) NULL, -- Fault Lock Time Level (1~1000[minute])
	smetertype numeric(1) NULL, -- Smart Meter Type (0=None, 2=RS485)
	smeterd0id numeric(1) NULL, -- Smart Meter D0 ID1 (0=None, 4=EM112, 5=EM24)
	apsetpointmode numeric(1) NULL, -- Active Power Setpoint Mode (0=Disable, 1=Enable)
	apsetpointvalue numeric(4) NULL, -- Active Power Setpoint Value (-6000~6000[W])
	apfreqmode numeric(1) NULL, -- Active Power Frequency Mode (0=Disable, 1=Enable)
	apfreqx1 numeric(4,2) NULL, -- Active Power Frequency X1 (40.00~60.00[Hz])
	apfreqy1 numeric(3,2) NULL, -- Active Power Frequency Y1 (0.00~2.00[%])
	apfreqx2 numeric(4,2) NULL, -- Active Power Frequency X2 (40.00~60.00[Hz])
	apfreqy2 numeric(3,2) NULL, -- Active Power Frequency Y2 (0.00~2.00[%])
	apfreqx3 numeric(4,2) NULL, -- Active Power Frequency X3 (50.00~70.00[Hz])
	apfreqy3 numeric(3,2) NULL, -- Active Power Frequency Y3 (-1.00~0.00[%])
	apfreqx4 numeric(4,2) NULL, -- Active Power Frequency X4 (50.00~70.00[Hz])
	apfreqy4 numeric(3,2) NULL, -- Active Power Frequency Y4 (-1.00~0.00[%])
	apvoltagemode numeric(1) NULL, -- Active Power Voltage Mode (0=Disable, 1=Enable)
	apvoltagex1 numeric(3,2) NULL, -- Active Power Voltage X1 (0.80~1.00[%])
	apvoltagey1 numeric(3,2) NULL, -- Active Power Voltage Y1 (0.00~2.00[%])
	apvoltagex2 numeric(3,2) NULL, -- Active Power Voltage X2 (0.80~1.00[%])
	apvoltagey2 numeric(3,2) NULL, -- Active Power Voltage Y2 (0.00~2.00[%])
	apvoltagex3 numeric(3,2) NULL, -- Active Power Voltage X3 (1.00~1.20[%])
	apvoltagey3 numeric(3,2) NULL, -- Active Power Voltage Y3 (-1.00~0.00[%])
	apvoltagex4 numeric(3,2) NULL, -- Active Power Voltage X4 (1.00~1.20[%])
	apvoltagey4 numeric(3,2) NULL, -- Active Power Voltage Y4 (-1.00~0.00[%])
	rpsetpointmode numeric(1) NULL, -- Reactive Power SetPoint Mode (0=Disable, 1=Enable)
	rpsetpointexcited numeric(1) NULL, -- Reactive Power SetPoint Excited (0=Over, 1=Under)
	rpsetpointvalue numeric(3,2) NULL, -- Reactive Power Setpoint Value (0.80~1.00[Power Factor])
	rpcospipmode numeric(1) NULL, -- Reactive Power Cospi P Mode (0=Disable, 1=Enable)
	rpcospipexcited numeric(1) NULL, -- Reactive Power Cospi P Excited (0=Over, 1=Under)
	rpcospipx1 numeric(3,2) NULL, -- Reactive Power Cospi P X1 (0.00~1.00)
	rpcospipy1 numeric(3,2) NULL, -- Reactive Power Cospi P Y1 (0.80~1.00)
	rpcospipx2 numeric(3,2) NULL, -- Reactive Power Cospi P X2 (0.00~1.00)
	rpcospipy2 numeric(3,2) NULL, -- Reactive Power Cospi P Y2 (0.80~1.00)
	rpcospipx3 numeric(3,2) NULL, -- Reactive Power Cospi P X3 (0.00~1.00)
	rpcospipy3 numeric(3,2) NULL, -- Reactive Power Cospi P Y3 (0.80~1.00)
	rpqsetpointmode numeric(1) NULL, -- Reactive Power Q SetPoint Mode (0=Disable, 1=Enable)
	rpqsetpointvalue numeric(4) NULL, -- Reactive Power Q SetPoint Value (-3000~3000[var,Q/S])
	rpqumode numeric(1) NULL, -- Reactive Power Q U Mode (0=Disable, 1=Enable)
	rpqux1 numeric(3,2) NULL, -- Reactive Power Q U X1 (0.80~1.00)
	rpquy1 numeric(4) NULL, -- Reactive Power Q U Y1 (-3000~0)
	rpqux2 numeric(3,2) NULL, -- Reactive Power Q U X2 (0.80~1.00)
	rpquy2 numeric(4) NULL, -- Reactive Power Q U Y2 (-3000~0)
	rpqux3 numeric(3,2) NULL, -- Reactive Power Q U X3 (1.00~1.20)
	rpquy3 numeric(4) NULL, -- Reactive Power Q U Y3 (0~3000)
	rpqux4 numeric(3,2) NULL, -- Reactive Power Q U X4 (1.00~1.20)
	rpquy4 numeric(4) NULL, -- Reactive Power Q U Y4 (0~3000)
	reserved1 numeric(4) NULL, -- reserved1 (0~9999)
	reserved2 numeric(4) NULL, -- reserved2 (0~9999)
	reserved3 numeric(4) NULL, -- reserved3 (0~9999)
	reserved4 numeric(4) NULL, -- reserved4 (0~9999)
	reserved5 numeric(4) NULL, -- reserved5 (0~9999)
	reserved6 numeric(4) NULL, -- reserved6 (0~9999)
	reserved7 numeric(4) NULL, -- reserved7 (0~9999)
	reserved8 numeric(4) NULL, -- reserved8 (0~9999)
	reserved9 numeric(4) NULL, -- reserved9 (0~9999)
	reserved10 numeric(4) NULL, -- reserved10 (0~9999)
	reserved11 numeric(4) NULL, -- reserved11 (0~9999)
	reserved12 numeric(4) NULL, -- reserved12 (0~9999)
	reserved13 numeric(4) NULL, -- reserved13 (0~9999)
	reserved14 numeric(4) NULL, -- reserved14 (0~9999)
	reserved15 numeric(4) NULL, -- reserved15 (0~9999)
	reserved16 numeric(4) NULL, -- reserved16 (0~9999)
	reserved17 numeric(4) NULL, -- reserved17 (0~9999)
	reserved18 numeric(4) NULL, -- reserved18 (0~9999)
	reserved19 numeric(4) NULL, -- reserved19 (0~9999)
	reserved20 numeric(4) NULL, -- reserved20 (0~9999)
	reserved21 numeric(4) NULL, -- reserved21 (0~9999)
	reserved22 numeric(4) NULL, -- reserved22 (0~9999)
	reserved23 numeric(4) NULL, -- reserved23 (0~9999)
	reserved24 numeric(4) NULL, -- reserved24 (0~9999)
	reserved25 numeric(4) NULL, -- reserved25 (0~9999)
	reserved26 numeric(4,2) NULL, -- reserved26 (-99.99~99.99)
	reserved27 numeric(4,2) NULL, -- reserved27 (-99.99~99.99)
	reserved28 numeric(4,2) NULL, -- reserved28 (-99.99~99.99)
	reserved29 numeric(4,2) NULL, -- reserved29 (-99.99~99.99)
	reserved30 numeric(4,2) NULL, -- reserved30 (-99.99~99.99)
	reserved31 numeric(4,2) NULL, -- reserved31 (-99.99~99.99)
	reserved32 numeric(4,2) NULL, -- reserved32 (-99.99~99.99)
	reserved33 numeric(4,2) NULL, -- reserved33 (-99.99~99.99)
	reserved34 numeric(4,2) NULL, -- reserved34 (-99.99~99.99)
	reserved35 numeric(4,2) NULL, -- reserved35 (-99.99~99.99)
	reserved36 numeric(4,2) NULL, -- reserved36 (-99.99~99.99)
	reserved37 numeric(4,2) NULL, -- reserved37 (-99.99~99.99)
	reserved38 numeric(4,2) NULL, -- reserved38 (-99.99~99.99)
	reserved39 numeric(4,2) NULL, -- reserved39 (-99.99~99.99)
	reserved40 numeric(4,2) NULL, -- reserved40 (-99.99~99.99)
	reserved41 numeric(4,2) NULL, -- reserved41 (-99.99~99.99)
	reserved42 numeric(4,2) NULL, -- reserved42 (-99.99~99.99)
	reserved43 numeric(4,2) NULL, -- reserved43 (-99.99~99.99)
	reserved44 numeric(4,2) NULL, -- reserved44 (-99.99~99.99)
	reserved45 numeric(4,2) NULL, -- reserved45 (-99.99~99.99)
	reserved46 numeric(4,2) NULL, -- reserved46 (-99.99~99.99)
	reserved47 numeric(4,2) NULL, -- reserved47 (-99.99~99.99)
	reserved48 numeric(4,2) NULL, -- reserved48 (-99.99~99.99)
	reserved49 numeric(4,2) NULL, -- reserved49 (-99.99~99.99)
	reserved50 numeric(4,2) NULL -- reserved50 (-99.99~99.99)
);
COMMENT ON TABLE wems.tb_opr_ess_config_hist IS ' ESS 의 기본설정 정보 갱신 이력 테이블';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_ess_config_hist.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.pv_max_pwr1 IS '1번패널최대발전파워(W)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.pv_max_pwr2 IS '2번패널최대발전파워(W)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.feed_in_limit IS '매전량(%)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.max_inverter_pw_cd IS '인버터최대출력(W)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.basicmode_cd IS '기본모드';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.smtr_tp_cd IS '에너지미터기종류';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.smtr_modl_cd IS '디지털미터기모델 코드';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.smtr_pulse_cnt IS '아날로그미터기1펄스(pulse/kwh)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.create_id IS '생성자ID';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.create_dt IS '생성일시';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.grid_max_volt IS 'GRID MAX VOLTAGE';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.grid_min_volt IS 'GRID MIN VOLTAGE';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.grid_max_freq IS 'GRID 최대 주파수';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.grid_min_freq IS 'GRID 최소 주파수';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_p_enable IS 'Reactive Power Control Cos Pi(P) Enable 코드';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_q_fix_enable IS 'Reactive Power Control Q fix Enable 코드';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_p_excited IS 'Reactive Power Control Cos Pi(P) Excited 코드';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.acp_pf_enable_cd IS 'Active Power Control P(F) Enable 코드';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.acp_pu_enable_cd IS 'Active Power Control P(U) Enable 코드';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_q_fix_excited IS 'Reactive Power Control Q fix Excited 코드';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_f_enable_cd IS 'Reactive Power Control Cos Pi fest Enable 코드';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_f_excited_cd IS 'Reactive Power Control Cos Pi fest Excited 코드';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.acp_set_point IS 'Active Power Control Set Point';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.acp_pe_max IS '예비';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_set_cos_pi IS 'Reactive Power Control Cos Pi fest Value';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.dci_variable IS '예비';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.dci_trip_level IS '예비';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_set_x1 IS 'Reactive Power Control X1';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_set_y1 IS 'Reactive Power Control Y1';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_set_x2 IS 'Reactive Power Control X2';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_set_y2 IS 'Reactive Power Control Y2';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_set_x3 IS 'Reactive Power Control X3';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_set_y3 IS 'Reactive Power Control Y3';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_set_x4 IS 'Reactive Power Control X4';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_set_y4 IS 'Reactive Power Control Y4';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rcp_q_fix_data IS 'Reactive Power Control Q fix Value';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.algo_flag IS '비트연산(10진수)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.algo_flag2 IS '비트연산(10진수)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.pemmode IS '3rd Party Control (0=Disable, 1=Enable)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.backupmode IS 'Backup Mode (0=Disable, 1=Enable)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.islandingmode IS 'Islanding Mode (0=Disable, 1=Enable)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.feedinmode IS 'Feed In Relay (0=Disable, 1=Enable)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.relay1attachlevel IS 'Relay1 Attach Level (0~6000[W])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.relay1detachlevel IS 'Relay1 Detach Level (0~6000[W])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.relay2attachlevel IS 'Relay2 Attach Level (0~6000[W])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.relay2detachlevel IS 'Relay2 Detach Level (0~6000[W])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.relay3attachlevel IS 'Relay3 Attach Level (0~6000[W])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.relay3detachlevel IS 'Relay3 Detach Level (0~6000[W])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.relay4attachlevel IS 'Relay4 Attach Level (0~6000[W])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.relay4detachlevel IS 'Relay4 Detach Level (0~6000[W])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.faultlockmode IS 'Fault Lock (0=Disable, 1=Enable)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.faultlockvalue IS 'Fault Lock Vaue (0~100[count])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.faultlocktimelevel IS 'Fault Lock Time Level (1~1000[minute])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.smetertype IS 'Smart Meter Type (0=None, 2=RS485)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.smeterd0id IS 'Smart Meter D0 ID1 (0=None, 4=EM112, 5=EM24)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apsetpointmode IS 'Active Power Setpoint Mode (0=Disable, 1=Enable)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apsetpointvalue IS 'Active Power Setpoint Value (-6000~6000[W])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apfreqmode IS 'Active Power Frequency Mode (0=Disable, 1=Enable)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apfreqx1 IS 'Active Power Frequency X1 (40.00~60.00[Hz])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apfreqy1 IS 'Active Power Frequency Y1 (0.00~2.00[%])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apfreqx2 IS 'Active Power Frequency X2 (40.00~60.00[Hz])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apfreqy2 IS 'Active Power Frequency Y2 (0.00~2.00[%])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apfreqx3 IS 'Active Power Frequency X3 (50.00~70.00[Hz])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apfreqy3 IS 'Active Power Frequency Y3 (-1.00~0.00[%])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apfreqx4 IS 'Active Power Frequency X4 (50.00~70.00[Hz])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apfreqy4 IS 'Active Power Frequency Y4 (-1.00~0.00[%])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apvoltagemode IS 'Active Power Voltage Mode (0=Disable, 1=Enable)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apvoltagex1 IS 'Active Power Voltage X1 (0.80~1.00[%])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apvoltagey1 IS 'Active Power Voltage Y1 (0.00~2.00[%])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apvoltagex2 IS 'Active Power Voltage X2 (0.80~1.00[%])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apvoltagey2 IS 'Active Power Voltage Y2 (0.00~2.00[%])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apvoltagex3 IS 'Active Power Voltage X3 (1.00~1.20[%])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apvoltagey3 IS 'Active Power Voltage Y3 (-1.00~0.00[%])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apvoltagex4 IS 'Active Power Voltage X4 (1.00~1.20[%])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.apvoltagey4 IS 'Active Power Voltage Y4 (-1.00~0.00[%])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpsetpointmode IS 'Reactive Power SetPoint Mode (0=Disable, 1=Enable)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpsetpointexcited IS 'Reactive Power SetPoint Excited (0=Over, 1=Under)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpsetpointvalue IS 'Reactive Power Setpoint Value (0.80~1.00[Power Factor])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpcospipmode IS 'Reactive Power Cospi P Mode (0=Disable, 1=Enable)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpcospipexcited IS 'Reactive Power Cospi P Excited (0=Over, 1=Under)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpcospipx1 IS 'Reactive Power Cospi P X1 (0.00~1.00)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpcospipy1 IS 'Reactive Power Cospi P Y1 (0.80~1.00)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpcospipx2 IS 'Reactive Power Cospi P X2 (0.00~1.00)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpcospipy2 IS 'Reactive Power Cospi P Y2 (0.80~1.00)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpcospipx3 IS 'Reactive Power Cospi P X3 (0.00~1.00)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpcospipy3 IS 'Reactive Power Cospi P Y3 (0.80~1.00)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpqsetpointmode IS 'Reactive Power Q SetPoint Mode (0=Disable, 1=Enable)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpqsetpointvalue IS 'Reactive Power Q SetPoint Value (-3000~3000[var,Q/S])';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpqumode IS 'Reactive Power Q U Mode (0=Disable, 1=Enable)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpqux1 IS 'Reactive Power Q U X1 (0.80~1.00)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpquy1 IS 'Reactive Power Q U Y1 (-3000~0)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpqux2 IS 'Reactive Power Q U X2 (0.80~1.00)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpquy2 IS 'Reactive Power Q U Y2 (-3000~0)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpqux3 IS 'Reactive Power Q U X3 (1.00~1.20)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpquy3 IS 'Reactive Power Q U Y3 (0~3000)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpqux4 IS 'Reactive Power Q U X4 (1.00~1.20)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.rpquy4 IS 'Reactive Power Q U Y4 (0~3000)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved1 IS 'reserved1 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved2 IS 'reserved2 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved3 IS 'reserved3 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved4 IS 'reserved4 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved5 IS 'reserved5 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved6 IS 'reserved6 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved7 IS 'reserved7 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved8 IS 'reserved8 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved9 IS 'reserved9 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved10 IS 'reserved10 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved11 IS 'reserved11 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved12 IS 'reserved12 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved13 IS 'reserved13 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved14 IS 'reserved14 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved15 IS 'reserved15 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved16 IS 'reserved16 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved17 IS 'reserved17 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved18 IS 'reserved18 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved19 IS 'reserved19 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved20 IS 'reserved20 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved21 IS 'reserved21 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved22 IS 'reserved22 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved23 IS 'reserved23 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved24 IS 'reserved24 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved25 IS 'reserved25 (0~9999)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved26 IS 'reserved26 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved27 IS 'reserved27 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved28 IS 'reserved28 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved29 IS 'reserved29 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved30 IS 'reserved30 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved31 IS 'reserved31 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved32 IS 'reserved32 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved33 IS 'reserved33 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved34 IS 'reserved34 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved35 IS 'reserved35 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved36 IS 'reserved36 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved37 IS 'reserved37 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved38 IS 'reserved38 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved39 IS 'reserved39 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved40 IS 'reserved40 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved41 IS 'reserved41 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved42 IS 'reserved42 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved43 IS 'reserved43 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved44 IS 'reserved44 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved45 IS 'reserved45 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved46 IS 'reserved46 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved47 IS 'reserved47 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved48 IS 'reserved48 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved49 IS 'reserved49 (-99.99~99.99)';
COMMENT ON COLUMN wems.tb_opr_ess_config_hist.reserved50 IS 'reserved50 (-99.99~99.99)';

-- Permissions

ALTER TABLE wems.tb_opr_ess_config_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_ess_config_hist TO wems;

-- Drop table

-- DROP TABLE wems.dc_code_porttype;

CREATE TABLE wems.dc_code_porttype (
	porttype numeric(8) NOT NULL,
	description varchar(40) NOT NULL,
	extflag numeric(1) NULL,
	"section" numeric(10) NULL,
	cmdflag numeric(1) NULL
);

-- Permissions

ALTER TABLE wems.dc_code_porttype OWNER TO wems;
GRANT ALL ON TABLE wems.dc_code_porttype TO wems;

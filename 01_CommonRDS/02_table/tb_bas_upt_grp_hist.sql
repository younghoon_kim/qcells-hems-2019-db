-- Drop table

-- DROP TABLE wems.tb_bas_upt_grp_hist;

CREATE TABLE wems.tb_bas_upt_grp_hist (
	grp_id varchar(50) NOT NULL, -- 그룹ID
	grp_upt_seq float8 NOT NULL, -- 변경 이력 순번
	grp_nm varchar(200) NULL, -- 그룹명
	grp_desc varchar(255) NULL, -- 그룹상세
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL, -- 등록일시
	update_id varchar(64) NOT NULL, -- 수정자ID
	update_dt timestamptz NOT NULL, -- 수정일시
	CONSTRAINT pk_bas_upt_grp_hist PRIMARY KEY (grp_id, grp_upt_seq)
);
COMMENT ON TABLE wems.tb_bas_upt_grp_hist IS '업데이트그룹정보 변경 이력을 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_upt_grp_hist.grp_id IS '그룹ID';
COMMENT ON COLUMN wems.tb_bas_upt_grp_hist.grp_upt_seq IS '변경 이력 순번';
COMMENT ON COLUMN wems.tb_bas_upt_grp_hist.grp_nm IS '그룹명';
COMMENT ON COLUMN wems.tb_bas_upt_grp_hist.grp_desc IS '그룹상세';
COMMENT ON COLUMN wems.tb_bas_upt_grp_hist.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_bas_upt_grp_hist.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_upt_grp_hist.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_bas_upt_grp_hist.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_bas_upt_grp_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_upt_grp_hist TO wems;

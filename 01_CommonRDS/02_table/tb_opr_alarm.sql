-- Drop table

-- DROP TABLE wems.tb_opr_alarm;

CREATE TABLE wems.tb_opr_alarm (
	alarm_id varchar(50) NOT NULL, -- 장애 ID
	alarm_id_seq numeric(3) NOT NULL, -- 장애 ID 일련번호
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	alarm_type_cd varchar(4) NOT NULL, -- 장애유형코드
	alarm_cd varchar(4) NOT NULL, -- 장애코드
	event_dt varchar(14) NOT NULL, -- 발생일시
	noti_flag varchar(1) NULL, -- 통지여부
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	send_dt timestamp NULL,	-- noti mail 발송시간
	CONSTRAINT pk_opr_alarm PRIMARY KEY (alarm_id, alarm_id_seq, alarm_type_cd, alarm_cd)
);
COMMENT ON TABLE wems.tb_opr_alarm IS '장비에 발생한 장애이력 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_alarm.alarm_id IS '장애 ID';
COMMENT ON COLUMN wems.tb_opr_alarm.alarm_id_seq IS '장애 ID 일련번호';
COMMENT ON COLUMN wems.tb_opr_alarm.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_opr_alarm.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_opr_alarm.alarm_type_cd IS '장애유형코드';
COMMENT ON COLUMN wems.tb_opr_alarm.alarm_cd IS '장애코드';
COMMENT ON COLUMN wems.tb_opr_alarm.event_dt IS '발생일시';
COMMENT ON COLUMN wems.tb_opr_alarm.noti_flag IS '통지여부';
COMMENT ON COLUMN wems.tb_opr_alarm.create_dt IS '생성일시';
COMMENT ON COLUMN wems.tb_opr_alarm.send_dt IS 'noti mail 발송시간';

-- Permissions

ALTER TABLE wems.tb_opr_alarm OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_alarm TO wems;

-- Drop table

-- DROP TABLE wems.dc_rul_parsing_template_grp;

CREATE TABLE wems.dc_rul_parsing_template_grp (
	id varchar(80) NOT NULL,
	identificationid varchar(80) NOT NULL,
	ruleid varchar(80) NULL,
	update_user varchar(50) NULL,
	update_date timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now())
);

-- Permissions

ALTER TABLE wems.dc_rul_parsing_template_grp OWNER TO wems;
GRANT ALL ON TABLE wems.dc_rul_parsing_template_grp TO wems;

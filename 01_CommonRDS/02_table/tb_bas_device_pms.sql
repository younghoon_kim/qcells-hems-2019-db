create table tb_bas_device_pms
(
    activation          varchar,
    ems_model_nm        varchar(64),
    pcs_model_nm        varchar(64),
    bms_model_nm        varchar(64),
    ems_ver             varchar(64),
    pcs_ver             varchar(64),
    bms_ver             varchar(64),
    ems_update_dt       varchar(14),
    upgrade_state       varchar(5),
    upgrade_dt          timestamp(6) with time zone,
    create_id           varchar(160) not null,
    update_id           varchar(160),
    instl_dt            varchar(14),
    instl_user_id       varchar(64),
    oper_test_dt        timestamp(6) with time zone,
    agree_sign          text,
    remark              varchar(4000),
    elpw_prod_cd        varchar(50),
    wrty_dt             varchar(8),
    pctr_file           varchar(4000),
    pctr_file_path      varchar(4000),
    pctr_file_disp_name varchar(4000),
    ems_ip_addr         varchar(15),
    ip_addr             varchar(32),
    connector_id        varchar(30) default NULL::character varying,

    constraint pk_bas_device_tmp
        primary key (site_id, device_id, device_type_cd)
) INHERITS (tb_bas_super_device);

comment on table tb_bas_device_pms is 'PMS 정보를 저장';
comment on column tb_bas_device_pms.activation is '활성화 비활성화 여부';
comment on column tb_bas_device_pms.ems_model_nm is 'EMS모델명';
comment on column tb_bas_device_pms.pcs_model_nm is 'PCS모델명';
comment on column tb_bas_device_pms.bms_model_nm is 'BMS모델명';
comment on column tb_bas_device_pms.ems_ver is 'EMSVersion';
comment on column tb_bas_device_pms.pcs_ver is 'PCSVersion';
comment on column tb_bas_device_pms.bms_ver is 'BMSVersion';

comment on column tb_bas_device_pms.upgrade_state is '원격업그레이드상태';
comment on column tb_bas_device_pms.upgrade_dt is '원격업그레이드수신시간';
comment on column tb_bas_device_pms.create_id is '등록자ID (암호화)';
comment on column tb_bas_device_pms.update_id is '수정자ID (암호화)';
comment on column tb_bas_device_pms.instl_dt is '설치일자';
comment on column tb_bas_device_pms.instl_user_id is '설치자 ID (암호화)';


comment on column tb_bas_device_pms.remark is '비고';

comment on column tb_bas_device_pms.wrty_dt is 'Warranty 만료일자';
comment on column tb_bas_device_pms.pctr_file is '사진파일';


comment on column tb_bas_device_pms.ems_ip_addr is '설치제품 IP주소';
comment on column tb_bas_device_pms.ip_addr is 'IP주소';
comment on column tb_bas_device_pms.connector_id is 'connector_id'
-- Drop table

-- DROP TABLE wems.tb_opr_dnld;

CREATE TABLE wems.tb_opr_dnld (
	dnld_seq float8 NOT NULL, -- 다운로드 순번
	product_cd varchar(4) NULL, -- 제품군 코드
	product_model_cd varchar(20) NULL, -- 제품모델 코드
	cntry_cd varchar(4) NOT NULL, -- 국가코드
	dnld_type_cd varchar(4) NOT NULL, -- 다운로드 유형 코드
	file_name varchar(4000) NOT NULL, -- 파일명(암호화)
	file_path varchar(4000) NOT NULL, -- 파일 경로(암호화)
	disp_name varchar(4000) NOT NULL, -- 화면에 표시되는 명칭
	create_id varchar(64) NOT NULL, -- 생성자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	update_id varchar(64) NULL, -- 생성자ID
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_opr_dnld PRIMARY KEY (dnld_seq)
);
COMMENT ON TABLE wems.tb_opr_dnld IS '다운로드 관리';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_dnld.dnld_seq IS '다운로드 순번';
COMMENT ON COLUMN wems.tb_opr_dnld.product_cd IS '제품군 코드';
COMMENT ON COLUMN wems.tb_opr_dnld.product_model_cd IS '제품모델 코드';
COMMENT ON COLUMN wems.tb_opr_dnld.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_opr_dnld.dnld_type_cd IS '다운로드 유형 코드';
COMMENT ON COLUMN wems.tb_opr_dnld.file_name IS '파일명(암호화)';
COMMENT ON COLUMN wems.tb_opr_dnld.file_path IS '파일 경로(암호화)';
COMMENT ON COLUMN wems.tb_opr_dnld.disp_name IS '화면에 표시되는 명칭';
COMMENT ON COLUMN wems.tb_opr_dnld.create_id IS '생성자ID';
COMMENT ON COLUMN wems.tb_opr_dnld.create_dt IS '생성일시';
COMMENT ON COLUMN wems.tb_opr_dnld.update_id IS '생성자ID';
COMMENT ON COLUMN wems.tb_opr_dnld.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_opr_dnld OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_dnld TO wems;

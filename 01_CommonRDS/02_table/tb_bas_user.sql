-- Drop table

-- DROP TABLE wems.tb_bas_user;

CREATE TABLE wems.tb_bas_user (
	user_id varchar(64) NOT NULL, -- 사용자ID
	user_nm varchar(128) NULL, -- 사용자명
	user_pwd varchar(128) NOT NULL, -- 사용자비밀번호
	email varchar(192) NULL, -- 이메일
	mpn_no varchar(64) NULL, -- 핸드폰번호
	auth_type_cd varchar(32) NOT NULL, -- 권한유형코드
	lang_cd varchar(32) NOT NULL, -- 표시언어코드
	join_dt timestamptz NULL, -- 가입일시
	leave_dt timestamptz NULL, -- 탈퇴일시
	agree_flag varchar(32) NULL, -- 정보제공 동의여부
	lock_flag varchar(32) NULL, -- 계정잠김여부
	lock_start_dt timestamptz NULL, -- 계정잠김시작시간
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	comn_nm varchar(200) NULL, -- 회사명
	bld_area_val numeric(18,2) NULL,
	bld_area_cd varchar(4) NULL,
	last_unlock_dt timestamptz NULL, -- 계정잠금이 마지막으로 풀린 시간
	alarm_mail_recv_flag varchar(1) NULL, -- 장애메일 수신여부
	m_rpt_recv_flag varchar(1) NULL, -- 월간 리포트 수신여부
	m_rpt_send_dt varchar(14) NULL, -- 리포트 메일발송 일자
	terms_dt timestamptz NULL, -- 약관동의일시
	migration_agree_too varchar(10) NULL, -- 이관동의재확인여부(기준일20150930)
	migration_agree_dt timestamp NULL, -- 이관동의일시(기준일20150930)
	user_edit varchar(1) NULL DEFAULT 'Y'::character varying, -- 인스톨러 사용자정보 변경여부(N:불가)
	gdpr_agree_flag varchar(32) NULL, -- GDPR 동의여부(기준일20180423)
	gdpr_agree_dt timestamp NULL, -- GDPR 동의일시(기준일20180423)
	CONSTRAINT pk_bas_user PRIMARY KEY (user_id)
);
COMMENT ON TABLE wems.tb_bas_user IS '시스템을 사용하는 사용자 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_user.user_id IS '사용자ID';
COMMENT ON COLUMN wems.tb_bas_user.user_nm IS '사용자명';
COMMENT ON COLUMN wems.tb_bas_user.user_pwd IS '사용자비밀번호';
COMMENT ON COLUMN wems.tb_bas_user.email IS '이메일';
COMMENT ON COLUMN wems.tb_bas_user.mpn_no IS '핸드폰번호';
COMMENT ON COLUMN wems.tb_bas_user.auth_type_cd IS '권한유형코드';
COMMENT ON COLUMN wems.tb_bas_user.lang_cd IS '표시언어코드';
COMMENT ON COLUMN wems.tb_bas_user.join_dt IS '가입일시';
COMMENT ON COLUMN wems.tb_bas_user.leave_dt IS '탈퇴일시';
COMMENT ON COLUMN wems.tb_bas_user.agree_flag IS '정보제공 동의여부';
COMMENT ON COLUMN wems.tb_bas_user.lock_flag IS '계정잠김여부';
COMMENT ON COLUMN wems.tb_bas_user.lock_start_dt IS '계정잠김시작시간';
COMMENT ON COLUMN wems.tb_bas_user.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_bas_user.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_user.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_bas_user.update_dt IS '수정일시';
COMMENT ON COLUMN wems.tb_bas_user.comn_nm IS '회사명';
COMMENT ON COLUMN wems.tb_bas_user.last_unlock_dt IS '계정잠금이 마지막으로 풀린 시간';
COMMENT ON COLUMN wems.tb_bas_user.alarm_mail_recv_flag IS '장애메일 수신여부';
COMMENT ON COLUMN wems.tb_bas_user.m_rpt_recv_flag IS '월간 리포트 수신여부';
COMMENT ON COLUMN wems.tb_bas_user.m_rpt_send_dt IS '리포트 메일발송 일자';
COMMENT ON COLUMN wems.tb_bas_user.terms_dt IS '약관동의일시';
COMMENT ON COLUMN wems.tb_bas_user.migration_agree_too IS '이관동의재확인여부(기준일20150930)';
COMMENT ON COLUMN wems.tb_bas_user.migration_agree_dt IS '이관동의일시(기준일20150930)';
COMMENT ON COLUMN wems.tb_bas_user.user_edit IS '인스톨러 사용자정보 변경여부(N:불가)';
COMMENT ON COLUMN wems.tb_bas_user.gdpr_agree_flag IS 'GDPR 동의여부(기준일20180423)';
COMMENT ON COLUMN wems.tb_bas_user.gdpr_agree_dt IS 'GDPR 동의일시(기준일20180423)';

-- Permissions

ALTER TABLE wems.tb_bas_user OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_user TO wems;

-- Drop table

-- DROP TABLE wems.tb_bas_api_user;

CREATE TABLE wems.tb_bas_api_user (
	user_id varchar(64) NOT NULL, -- 사용자ID (암호화)
	user_pwd varchar(128) NOT NULL, -- 사용자비밀번호 (암호화)
	auth_type_cd varchar(32) NOT NULL, -- 권한유형코드 (암호화)
	comn_nm varchar(200) NULL, -- 회사명 (암호화)
	create_id varchar(64) NOT NULL, -- 등록자ID (암호화)
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	CONSTRAINT pk_bas_api_user PRIMARY KEY (user_id)
);
COMMENT ON TABLE wems.tb_bas_api_user IS '시스템을 사용하는 사용자 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_api_user.user_id IS '사용자ID (암호화)';
COMMENT ON COLUMN wems.tb_bas_api_user.user_pwd IS '사용자비밀번호 (암호화)';
COMMENT ON COLUMN wems.tb_bas_api_user.auth_type_cd IS '권한유형코드 (암호화)';
COMMENT ON COLUMN wems.tb_bas_api_user.comn_nm IS '회사명 (암호화)';
COMMENT ON COLUMN wems.tb_bas_api_user.create_id IS '등록자ID (암호화)';
COMMENT ON COLUMN wems.tb_bas_api_user.create_dt IS '등록일시';

-- Permissions

ALTER TABLE wems.tb_bas_api_user OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_api_user TO wems;

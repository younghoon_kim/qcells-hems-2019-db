-- Drop table

-- DROP TABLE wems.tb_sys_sp_exe_hst;

CREATE TABLE wems.tb_sys_sp_exe_hst (
	sp_nm varchar(100) NOT NULL, -- 프로시져명
	job_seq numeric(15) NOT NULL, -- 실행일련번호
	exe_base varchar(20) NOT NULL, -- 실행기준값
	st_dt timestamp NOT NULL, -- 시작일시
	ed_dt timestamp NULL, -- 종료일시
	lap_time numeric(15,2) NULL, -- 경과초수
	exe_mthd varchar(1) NULL, -- 실행사유코드
	rslt_cd varchar(1) NULL, -- 처리결과코드
	rslt_cnt numeric(10) NULL, -- 처리결과수
	err_msg varchar(500) NULL, -- SQL오류메시지내용
	CONSTRAINT pk_tb_sys_sp_exe_hst PRIMARY KEY (st_dt, sp_nm, job_seq, exe_base)
);
COMMENT ON TABLE wems.tb_sys_sp_exe_hst IS '프로시져실행이력';

-- Column comments

COMMENT ON COLUMN wems.tb_sys_sp_exe_hst.sp_nm IS '프로시져명';
COMMENT ON COLUMN wems.tb_sys_sp_exe_hst.job_seq IS '실행일련번호';
COMMENT ON COLUMN wems.tb_sys_sp_exe_hst.exe_base IS '실행기준값';
COMMENT ON COLUMN wems.tb_sys_sp_exe_hst.st_dt IS '시작일시';
COMMENT ON COLUMN wems.tb_sys_sp_exe_hst.ed_dt IS '종료일시';
COMMENT ON COLUMN wems.tb_sys_sp_exe_hst.lap_time IS '경과초수';
COMMENT ON COLUMN wems.tb_sys_sp_exe_hst.exe_mthd IS '실행사유코드';
COMMENT ON COLUMN wems.tb_sys_sp_exe_hst.rslt_cd IS '처리결과코드';
COMMENT ON COLUMN wems.tb_sys_sp_exe_hst.rslt_cnt IS '처리결과수';
COMMENT ON COLUMN wems.tb_sys_sp_exe_hst.err_msg IS 'SQL오류메시지내용';

-- Permissions

ALTER TABLE wems.tb_sys_sp_exe_hst OWNER TO wems;
GRANT ALL ON TABLE wems.tb_sys_sp_exe_hst TO wems;

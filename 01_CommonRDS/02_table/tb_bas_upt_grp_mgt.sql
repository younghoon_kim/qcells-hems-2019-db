-- Drop table

-- DROP TABLE wems.tb_bas_upt_grp_mgt;

CREATE TABLE wems.tb_bas_upt_grp_mgt (
	grp_id varchar(50) NOT NULL, -- 그룹ID
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	firmware_exception varchar(1) NULL DEFAULT '0'::character varying, -- 펌웨어 제외:1
	CONSTRAINT pk_bas_upt_grp_mgt PRIMARY KEY (grp_id, device_id, device_type_cd)
);
COMMENT ON TABLE wems.tb_bas_upt_grp_mgt IS '업데이트그룹정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_upt_grp_mgt.grp_id IS '그룹ID';
COMMENT ON COLUMN wems.tb_bas_upt_grp_mgt.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_bas_upt_grp_mgt.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_bas_upt_grp_mgt.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_bas_upt_grp_mgt.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_upt_grp_mgt.firmware_exception IS '펌웨어 제외:1';

-- Permissions

ALTER TABLE wems.tb_bas_upt_grp_mgt OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_upt_grp_mgt TO wems;

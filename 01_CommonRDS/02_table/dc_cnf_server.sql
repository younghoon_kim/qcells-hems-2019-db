-- Drop table

-- DROP TABLE wems.dc_cnf_server;

CREATE TABLE wems.dc_cnf_server (
	ip varchar(20) NOT NULL,
	guiport numeric(8) NOT NULL,
	cmdport numeric(8) NOT NULL,
	netfinderport numeric(8) NOT NULL,
	logport numeric(8) NOT NULL,
	netfinderip varchar(20) NOT NULL,
	sockmgrport numeric(8) NULL
);

-- Permissions

ALTER TABLE wems.dc_cnf_server OWNER TO wems;
GRANT ALL ON TABLE wems.dc_cnf_server TO wems;

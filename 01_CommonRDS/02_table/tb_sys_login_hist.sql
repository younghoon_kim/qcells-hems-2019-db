-- Drop table

-- DROP TABLE wems.tb_sys_login_hist;

CREATE TABLE wems.tb_sys_login_hist (
	login_seq numeric(18) NOT NULL, -- 접속SEQ
	user_id varchar(64) NOT NULL, -- 사용자ID
	client_type_cd varchar(4) NULL, -- 접속유형
	login_dt timestamptz NULL, -- 로그인일시
	logout_dt timestamptz NULL, -- 로그아웃일시
	client_ip varchar(15) NULL, -- 접속IP
	create_dt timestamptz NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	CONSTRAINT pk_sys_login_hist PRIMARY KEY (login_seq, user_id)
);
COMMENT ON TABLE wems.tb_sys_login_hist IS '시스템에 접속한 사용자의 접속이력 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_sys_login_hist.login_seq IS '접속SEQ';
COMMENT ON COLUMN wems.tb_sys_login_hist.user_id IS '사용자ID';
COMMENT ON COLUMN wems.tb_sys_login_hist.client_type_cd IS '접속유형';
COMMENT ON COLUMN wems.tb_sys_login_hist.login_dt IS '로그인일시';
COMMENT ON COLUMN wems.tb_sys_login_hist.logout_dt IS '로그아웃일시';
COMMENT ON COLUMN wems.tb_sys_login_hist.client_ip IS '접속IP';
COMMENT ON COLUMN wems.tb_sys_login_hist.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_sys_login_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_sys_login_hist TO wems;

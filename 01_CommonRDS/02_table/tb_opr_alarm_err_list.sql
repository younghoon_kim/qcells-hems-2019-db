-- Drop table

-- DROP TABLE wems.tb_opr_alarm_err_list;

CREATE TABLE wems.tb_opr_alarm_err_list (
	alarm_id varchar(50) NOT NULL,
	alarm_id_seq numeric(3) NOT NULL
);

-- Permissions

ALTER TABLE wems.tb_opr_alarm_err_list OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_alarm_err_list TO wems;

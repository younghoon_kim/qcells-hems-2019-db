-- Drop table

-- DROP TABLE wems.dc_code_protocoltype;

CREATE TABLE wems.dc_code_protocoltype (
	protocoltype numeric(8) NOT NULL,
	description varchar(40) NOT NULL,
	need_portno numeric(1) NOT NULL,
	need_password numeric(1) NOT NULL,
	need_userid numeric(1) NOT NULL,
	extflag numeric(1) NULL,
	porttype_able varchar(100) NULL,
	need_ip varchar(40) NULL
);

-- Permissions

ALTER TABLE wems.dc_code_protocoltype OWNER TO wems;
GRANT ALL ON TABLE wems.dc_code_protocoltype TO wems;

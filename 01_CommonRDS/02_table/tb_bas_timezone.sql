-- Drop table

-- DROP TABLE wems.tb_bas_timezone;

CREATE TABLE wems.tb_bas_timezone (
	timezone_id varchar(100) NOT NULL, -- 타임존 ID
	utc_offset numeric(4,2) NULL, -- Standard Offset
	dst_start_exp varchar(4000) NULL,
	dst_end_exp varchar(4000) NULL,
	dst_use_flag varchar(1) NULL DEFAULT 'N'::character varying,
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	CONSTRAINT pk_bas_timezone PRIMARY KEY (timezone_id)
);
COMMENT ON TABLE wems.tb_bas_timezone IS '타임존 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_timezone.timezone_id IS '타임존 ID';
COMMENT ON COLUMN wems.tb_bas_timezone.utc_offset IS 'Standard Offset';
COMMENT ON COLUMN wems.tb_bas_timezone.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_bas_timezone OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_timezone TO wems;

-- Drop table

-- DROP TABLE wems.batch_job_execution;

CREATE TABLE wems.batch_job_execution (
	job_execution_id int8 NOT NULL,
	"version" int8 NULL,
	job_instance_id int8 NOT NULL,
	create_time timestamp NOT NULL,
	start_time timestamp NULL,
	end_time timestamp NULL,
	status varchar(10) NULL,
	exit_code varchar(2500) NULL,
	exit_message varchar(2500) NULL,
	last_updated timestamp NULL,
	job_configuration_location varchar(2500) NULL,
	CONSTRAINT batch_job_execution_pkey PRIMARY KEY (job_execution_id)
);

-- Permissions

ALTER TABLE wems.batch_job_execution OWNER TO wems;
GRANT ALL ON TABLE wems.batch_job_execution TO wems;

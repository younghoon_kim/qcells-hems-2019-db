-- Drop table

-- DROP TABLE wems.tb_plf_elpw_prod;

CREATE TABLE wems.tb_plf_elpw_prod (
	cntry_cd varchar(4) NOT NULL, -- 국가코드
	elpw_conm varchar(30) NOT NULL, -- 전력회사명
	elpw_prod_cd varchar(50) NOT NULL, -- 요금제코드
	elpw_prod_nm varchar(30) NULL, -- 요금제이름
	remark varchar(255) NULL, -- 비고
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	create_id varchar(64) NOT NULL, -- 등록자ID
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_plf_elpw_prod PRIMARY KEY (cntry_cd, elpw_conm, elpw_prod_cd)
);
COMMENT ON TABLE wems.tb_plf_elpw_prod IS '시스템 관리 및 운영에 필요한 국가/전력회사별 전기요금 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_elpw_prod.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_plf_elpw_prod.elpw_conm IS '전력회사명';
COMMENT ON COLUMN wems.tb_plf_elpw_prod.elpw_prod_cd IS '요금제코드';
COMMENT ON COLUMN wems.tb_plf_elpw_prod.elpw_prod_nm IS '요금제이름';
COMMENT ON COLUMN wems.tb_plf_elpw_prod.remark IS '비고';
COMMENT ON COLUMN wems.tb_plf_elpw_prod.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_plf_elpw_prod.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_plf_elpw_prod.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_plf_elpw_prod.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_plf_elpw_prod OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_elpw_prod TO wems;

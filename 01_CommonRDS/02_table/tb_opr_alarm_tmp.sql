-- Drop table

-- DROP TABLE wems.tb_opr_alarm_tmp;

CREATE TABLE wems.tb_opr_alarm_tmp (
	alarm_id varchar(50) NULL,
	alarm_id_seq numeric(3) NULL,
	device_id varchar(50) NULL,
	device_type_cd varchar(4) NULL,
	alarm_type_cd varchar(4) NULL,
	alarm_cd varchar(4) NULL,
	event_dt varchar(14) NULL,
	err_ocur numeric(1) NULL,
	pos numeric(3) NULL,
	ins_flag varchar(1) NULL,
	create_dt timestamptz NULL
);

-- Permissions

ALTER TABLE wems.tb_opr_alarm_tmp OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_alarm_tmp TO wems;

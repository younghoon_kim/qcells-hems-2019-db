-- Drop table

-- DROP TABLE wems.tb_raw_ess_info;

CREATE TABLE wems.tb_raw_ess_info (
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	device_id varchar(50) NOT NULL, -- ESS ID
	colec_dt varchar(14) NOT NULL, -- 수집시간
	"delimiter" varchar(1) NULL, -- 문자열구분자
	ems_ver varchar(30) NULL, -- EMS Version
	pcs_ver varchar(30) NULL, -- PCS Version
	rack_ver varchar(30) NULL, -- RACK Version
	rackhist_ver varchar(30) NULL, -- RACKHIST Version
	tray_ver varchar(30) NULL, -- Tray Version
	done_flag varchar(1) NULL, -- 이관여부
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()) -- 생성일시
)
PARTITION BY RANGE (colec_dt);
CREATE UNIQUE INDEX pk_raw_ess_info ON ONLY wems.tb_raw_ess_info USING btree (device_type_cd, device_id, colec_dt);
COMMENT ON TABLE wems.tb_raw_ess_info IS '5분단위 ESS 수집 데이터 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_raw_ess_info.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_raw_ess_info.device_id IS 'ESS ID';
COMMENT ON COLUMN wems.tb_raw_ess_info.colec_dt IS '수집시간';
COMMENT ON COLUMN wems.tb_raw_ess_info."delimiter" IS '문자열구분자';
COMMENT ON COLUMN wems.tb_raw_ess_info.ems_ver IS 'EMS Version';
COMMENT ON COLUMN wems.tb_raw_ess_info.pcs_ver IS 'PCS Version';
COMMENT ON COLUMN wems.tb_raw_ess_info.rack_ver IS 'RACK Version';
COMMENT ON COLUMN wems.tb_raw_ess_info.rackhist_ver IS 'RACKHIST Version';
COMMENT ON COLUMN wems.tb_raw_ess_info.tray_ver IS 'Tray Version';
COMMENT ON COLUMN wems.tb_raw_ess_info.done_flag IS '이관여부';
COMMENT ON COLUMN wems.tb_raw_ess_info.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_raw_ess_info OWNER TO wems;
GRANT ALL ON TABLE wems.tb_raw_ess_info TO wems;

-- Drop table

-- DROP TABLE wems.tb_raw_bms_info;

CREATE TABLE wems.tb_raw_bms_info (
	device_id varchar(50) NOT NULL, -- ESS ID
	colec_dt varchar(14) NOT NULL, -- 수집시간
	done_flag varchar(1) NULL, -- 이관여부
	col0 varchar(50) NULL, -- G_PCS_INFO_BMSFLAG0
	col1 varchar(50) NULL, -- G_PCS_INFO_BMSFLAG_RESERVED1
	col2 varchar(50) NULL, -- G_PCS_INFO_BMSFLAG_RESERVED2
	col3 varchar(50) NULL, -- G_PCS_INFO_BMSFLAG_RESERVED3
	col4 varchar(50) NULL, -- G_PCS_INFO_BMSDIAG_DIAG0
	col5 varchar(50) NULL, -- G_PCS_INFO_BMSDIAG_DIAG1
	col6 varchar(50) NULL, -- G_PCS_INFO_BMSDIAG_DIAG2
	col7 varchar(50) NULL, -- G_PCS_INFO_BMSDIAG_DIAG3
	col8 varchar(50) NULL, -- G_PCS_INFO_BMSDIAG_DIAG4
	col9 varchar(50) NULL, -- G_PCS_INFO_BMSDIAG_DIAG_RESERVED1
	col10 varchar(50) NULL, -- G_PCS_INFO_BMSDIAG_DIAG_RESERVED2
	col11 varchar(50) NULL, -- G_PCS_INFO_BMSDIAG_DIAG_RESERVED3
	col12 varchar(50) NULL, -- G_PCS_INFO_BMSDIAG_DIAG_RESERVED4
	col13 varchar(50) NULL, -- G_PCS_INFO_BMSDIAG_DIAG_RESERVED5
	col14 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_SOC
	col15 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_SOH
	col16 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_RACKVOLTAGE
	col17 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_RACKCURRENT
	col18 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_CELLV_MAX
	col19 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_CELLV_MIN
	col20 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_CELLV_AVE
	col21 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_CELLT_MAX
	col22 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_CELLT_MIN
	col23 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_CELLT_AVE
	col24 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_NUMOFTRAY
	col25 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_RACK_RESERVED1
	col26 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_RACK_RESERVED2
	col27 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_RACK_RESERVED3
	col28 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_RACK_RESERVED4
	col29 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_TRAY1_CELLBALANCING
	col30 varchar(100) NULL, -- G_PCS_INFO_BMSDATA_TRAY1_CELL_V
	col31 varchar(100) NULL, -- G_PCS_INFO_BMSDATA_TRAY1_CELL_T
	col32 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_TRAY1_RESERVED
	col33 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_TRAY2_CELLBALANCING
	col34 varchar(100) NULL, -- G_PCS_INFO_BMSDATA_TRAY2_CELL_V
	col35 varchar(100) NULL, -- G_PCS_INFO_BMSDATA_TRAY2_CELL_T
	col36 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_TRAY2_RESERVED
	col37 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_TRAY3_CELLBALANCING
	col38 varchar(100) NULL, -- G_PCS_INFO_BMSDATA_TRAY3_CELL_V
	col39 varchar(100) NULL, -- G_PCS_INFO_BMSDATA_TRAY3_CELL_T
	col40 varchar(50) NULL, -- G_PCS_INFO_BMSDATA_TRAY3_RESERVED
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	handle_flag varchar(14) NULL, -- RAW 데이터 처리여부
	CONSTRAINT pk_raw_bms_info PRIMARY KEY (colec_dt, device_id)
)
PARTITION BY RANGE (colec_dt);
CREATE INDEX idx_raw_bms_info_01 ON ONLY wems.tb_raw_bms_info USING btree (create_dt);
COMMENT ON TABLE wems.tb_raw_bms_info IS '5분단위 BMS 데이터 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_raw_bms_info.device_id IS 'ESS ID';
COMMENT ON COLUMN wems.tb_raw_bms_info.colec_dt IS '수집시간';
COMMENT ON COLUMN wems.tb_raw_bms_info.done_flag IS '이관여부';
COMMENT ON COLUMN wems.tb_raw_bms_info.col0 IS 'G_PCS_INFO_BMSFLAG0';
COMMENT ON COLUMN wems.tb_raw_bms_info.col1 IS 'G_PCS_INFO_BMSFLAG_RESERVED1';
COMMENT ON COLUMN wems.tb_raw_bms_info.col2 IS 'G_PCS_INFO_BMSFLAG_RESERVED2';
COMMENT ON COLUMN wems.tb_raw_bms_info.col3 IS 'G_PCS_INFO_BMSFLAG_RESERVED3';
COMMENT ON COLUMN wems.tb_raw_bms_info.col4 IS 'G_PCS_INFO_BMSDIAG_DIAG0';
COMMENT ON COLUMN wems.tb_raw_bms_info.col5 IS 'G_PCS_INFO_BMSDIAG_DIAG1';
COMMENT ON COLUMN wems.tb_raw_bms_info.col6 IS 'G_PCS_INFO_BMSDIAG_DIAG2';
COMMENT ON COLUMN wems.tb_raw_bms_info.col7 IS 'G_PCS_INFO_BMSDIAG_DIAG3';
COMMENT ON COLUMN wems.tb_raw_bms_info.col8 IS 'G_PCS_INFO_BMSDIAG_DIAG4';
COMMENT ON COLUMN wems.tb_raw_bms_info.col9 IS 'G_PCS_INFO_BMSDIAG_DIAG_RESERVED1';
COMMENT ON COLUMN wems.tb_raw_bms_info.col10 IS 'G_PCS_INFO_BMSDIAG_DIAG_RESERVED2';
COMMENT ON COLUMN wems.tb_raw_bms_info.col11 IS 'G_PCS_INFO_BMSDIAG_DIAG_RESERVED3';
COMMENT ON COLUMN wems.tb_raw_bms_info.col12 IS 'G_PCS_INFO_BMSDIAG_DIAG_RESERVED4';
COMMENT ON COLUMN wems.tb_raw_bms_info.col13 IS 'G_PCS_INFO_BMSDIAG_DIAG_RESERVED5';
COMMENT ON COLUMN wems.tb_raw_bms_info.col14 IS 'G_PCS_INFO_BMSDATA_SOC';
COMMENT ON COLUMN wems.tb_raw_bms_info.col15 IS 'G_PCS_INFO_BMSDATA_SOH';
COMMENT ON COLUMN wems.tb_raw_bms_info.col16 IS 'G_PCS_INFO_BMSDATA_RACKVOLTAGE';
COMMENT ON COLUMN wems.tb_raw_bms_info.col17 IS 'G_PCS_INFO_BMSDATA_RACKCURRENT';
COMMENT ON COLUMN wems.tb_raw_bms_info.col18 IS 'G_PCS_INFO_BMSDATA_CELLV_MAX';
COMMENT ON COLUMN wems.tb_raw_bms_info.col19 IS 'G_PCS_INFO_BMSDATA_CELLV_MIN';
COMMENT ON COLUMN wems.tb_raw_bms_info.col20 IS 'G_PCS_INFO_BMSDATA_CELLV_AVE';
COMMENT ON COLUMN wems.tb_raw_bms_info.col21 IS 'G_PCS_INFO_BMSDATA_CELLT_MAX';
COMMENT ON COLUMN wems.tb_raw_bms_info.col22 IS 'G_PCS_INFO_BMSDATA_CELLT_MIN';
COMMENT ON COLUMN wems.tb_raw_bms_info.col23 IS 'G_PCS_INFO_BMSDATA_CELLT_AVE';
COMMENT ON COLUMN wems.tb_raw_bms_info.col24 IS 'G_PCS_INFO_BMSDATA_NUMOFTRAY';
COMMENT ON COLUMN wems.tb_raw_bms_info.col25 IS 'G_PCS_INFO_BMSDATA_RACK_RESERVED1';
COMMENT ON COLUMN wems.tb_raw_bms_info.col26 IS 'G_PCS_INFO_BMSDATA_RACK_RESERVED2';
COMMENT ON COLUMN wems.tb_raw_bms_info.col27 IS 'G_PCS_INFO_BMSDATA_RACK_RESERVED3';
COMMENT ON COLUMN wems.tb_raw_bms_info.col28 IS 'G_PCS_INFO_BMSDATA_RACK_RESERVED4';
COMMENT ON COLUMN wems.tb_raw_bms_info.col29 IS 'G_PCS_INFO_BMSDATA_TRAY1_CELLBALANCING';
COMMENT ON COLUMN wems.tb_raw_bms_info.col30 IS 'G_PCS_INFO_BMSDATA_TRAY1_CELL_V';
COMMENT ON COLUMN wems.tb_raw_bms_info.col31 IS 'G_PCS_INFO_BMSDATA_TRAY1_CELL_T';
COMMENT ON COLUMN wems.tb_raw_bms_info.col32 IS 'G_PCS_INFO_BMSDATA_TRAY1_RESERVED';
COMMENT ON COLUMN wems.tb_raw_bms_info.col33 IS 'G_PCS_INFO_BMSDATA_TRAY2_CELLBALANCING';
COMMENT ON COLUMN wems.tb_raw_bms_info.col34 IS 'G_PCS_INFO_BMSDATA_TRAY2_CELL_V';
COMMENT ON COLUMN wems.tb_raw_bms_info.col35 IS 'G_PCS_INFO_BMSDATA_TRAY2_CELL_T';
COMMENT ON COLUMN wems.tb_raw_bms_info.col36 IS 'G_PCS_INFO_BMSDATA_TRAY2_RESERVED';
COMMENT ON COLUMN wems.tb_raw_bms_info.col37 IS 'G_PCS_INFO_BMSDATA_TRAY3_CELLBALANCING';
COMMENT ON COLUMN wems.tb_raw_bms_info.col38 IS 'G_PCS_INFO_BMSDATA_TRAY3_CELL_V';
COMMENT ON COLUMN wems.tb_raw_bms_info.col39 IS 'G_PCS_INFO_BMSDATA_TRAY3_CELL_T';
COMMENT ON COLUMN wems.tb_raw_bms_info.col40 IS 'G_PCS_INFO_BMSDATA_TRAY3_RESERVED';
COMMENT ON COLUMN wems.tb_raw_bms_info.create_dt IS '생성일시';
COMMENT ON COLUMN wems.tb_raw_bms_info.handle_flag IS 'RAW 데이터 처리여부';

-- Permissions

ALTER TABLE wems.tb_raw_bms_info OWNER TO wems;
GRANT ALL ON TABLE wems.tb_raw_bms_info TO wems;

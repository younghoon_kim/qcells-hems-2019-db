-- Drop table

-- DROP TABLE wems.tb_stt_rank_mm;

CREATE TABLE wems.tb_stt_rank_mm (
	colec_mm varchar(6) NOT NULL, -- 집계년월
	utc_offset numeric(4,2) NOT NULL, -- UTC Offset
	rank_type_cd varchar(4) NOT NULL, -- 순위유형코드
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	rank_per numeric(3) NULL, -- 상위퍼센트
	rank_no numeric(10) NULL, -- 상위 등수
	total_cnt numeric(10) NULL, -- 전체 건수
	cntry_cd varchar(4) NULL, -- 국가코드
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()) -- 생성일시
)
PARTITION BY RANGE (colec_mm);
CREATE UNIQUE INDEX pk_stt_rank_mm ON ONLY wems.tb_stt_rank_mm USING btree (colec_mm, utc_offset, rank_type_cd, device_id, device_type_cd);
COMMENT ON TABLE wems.tb_stt_rank_mm IS '월별 유형별 순위 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_stt_rank_mm.colec_mm IS '집계년월';
COMMENT ON COLUMN wems.tb_stt_rank_mm.utc_offset IS 'UTC Offset';
COMMENT ON COLUMN wems.tb_stt_rank_mm.rank_type_cd IS '순위유형코드';
COMMENT ON COLUMN wems.tb_stt_rank_mm.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_stt_rank_mm.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_stt_rank_mm.rank_per IS '상위퍼센트';
COMMENT ON COLUMN wems.tb_stt_rank_mm.rank_no IS '상위 등수';
COMMENT ON COLUMN wems.tb_stt_rank_mm.total_cnt IS '전체 건수';
COMMENT ON COLUMN wems.tb_stt_rank_mm.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_stt_rank_mm.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_stt_rank_mm OWNER TO wems;
GRANT ALL ON TABLE wems.tb_stt_rank_mm TO wems;

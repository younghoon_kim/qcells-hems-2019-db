-- Drop table

-- DROP TABLE wems.tb_bas_city;

CREATE TABLE wems.tb_bas_city (
	city_cd varchar(50) NOT NULL, -- 도시코드
	latitude numeric(12,6) NULL, -- 위도
	longitude numeric(12,6) NULL, -- 경도
	utc_offset numeric(4,2) NULL, -- UTC Offset
	city_nm varchar(50) NULL, -- 도시명
	cntry_cd varchar(4) NULL, -- 국가코드
	cntry_nm varchar(50) NULL, -- 국가명
	colec_flg varchar(1) NULL, -- 날씨정보 수집여부
	pop_cnt numeric(18) NULL, -- 인구수
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	timezone_id varchar(100) NULL, -- 타임존 ID
	CONSTRAINT pk_bas_city PRIMARY KEY (city_cd)
);
COMMENT ON TABLE wems.tb_bas_city IS '날씨연동대상 도시정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_city.city_cd IS '도시코드';
COMMENT ON COLUMN wems.tb_bas_city.latitude IS '위도';
COMMENT ON COLUMN wems.tb_bas_city.longitude IS '경도';
COMMENT ON COLUMN wems.tb_bas_city.utc_offset IS 'UTC Offset';
COMMENT ON COLUMN wems.tb_bas_city.city_nm IS '도시명';
COMMENT ON COLUMN wems.tb_bas_city.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_bas_city.cntry_nm IS '국가명';
COMMENT ON COLUMN wems.tb_bas_city.colec_flg IS '날씨정보 수집여부';
COMMENT ON COLUMN wems.tb_bas_city.pop_cnt IS '인구수';
COMMENT ON COLUMN wems.tb_bas_city.create_dt IS '생성일시';
COMMENT ON COLUMN wems.tb_bas_city.timezone_id IS '타임존 ID';

-- Permissions

ALTER TABLE wems.tb_bas_city OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_city TO wems;

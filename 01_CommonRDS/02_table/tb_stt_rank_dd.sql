-- Drop table

-- DROP TABLE wems.tb_stt_rank_dd;

CREATE TABLE wems.tb_stt_rank_dd (
	colec_dd varchar(8) NOT NULL, -- 집계일자
	utc_offset numeric(4,2) NOT NULL, -- UTC Offset
	rank_type_cd varchar(4) NOT NULL, -- 순위유형코드
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	rank_per numeric(3) NULL, -- 상위퍼센트
	rank_no numeric(10) NULL,
	total_cnt numeric(10) NULL,
	cntry_cd varchar(4) NULL, -- 국가코드
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()) -- 생성일시
)
PARTITION BY RANGE (colec_dd);
CREATE UNIQUE INDEX pk_stt_rank_dd ON ONLY wems.tb_stt_rank_dd USING btree (colec_dd, utc_offset, rank_type_cd, device_id, device_type_cd);
COMMENT ON TABLE wems.tb_stt_rank_dd IS '일별 유형별 순위 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_stt_rank_dd.colec_dd IS '집계일자';
COMMENT ON COLUMN wems.tb_stt_rank_dd.utc_offset IS 'UTC Offset';
COMMENT ON COLUMN wems.tb_stt_rank_dd.rank_type_cd IS '순위유형코드';
COMMENT ON COLUMN wems.tb_stt_rank_dd.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_stt_rank_dd.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_stt_rank_dd.rank_per IS '상위퍼센트';
COMMENT ON COLUMN wems.tb_stt_rank_dd.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_stt_rank_dd.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_stt_rank_dd OWNER TO wems;
GRANT ALL ON TABLE wems.tb_stt_rank_dd TO wems;

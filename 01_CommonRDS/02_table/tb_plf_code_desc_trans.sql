-- Drop table

-- DROP TABLE wems.tb_plf_code_desc_trans;

CREATE TABLE wems.tb_plf_code_desc_trans (
	grp_cd varchar(30) NOT NULL, -- 장애유형코드
	code varchar(4) NOT NULL, -- 장애코드
	lang_cd varchar(4) NOT NULL, -- 장비유형코드
	desc_trans varchar(4000) NULL, -- 설명
	CONSTRAINT pk_plf_code_desc_trans PRIMARY KEY (grp_cd, code, lang_cd)
);
COMMENT ON TABLE wems.tb_plf_code_desc_trans IS '코드의 언어별 상세 설명';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_code_desc_trans.grp_cd IS '장애유형코드';
COMMENT ON COLUMN wems.tb_plf_code_desc_trans.code IS '장애코드';
COMMENT ON COLUMN wems.tb_plf_code_desc_trans.lang_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_plf_code_desc_trans.desc_trans IS '설명';

-- Permissions

ALTER TABLE wems.tb_plf_code_desc_trans OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_code_desc_trans TO wems;

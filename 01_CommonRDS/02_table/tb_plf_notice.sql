-- Drop table

-- DROP TABLE wems.tb_plf_notice;

CREATE TABLE wems.tb_plf_notice (
	noti_seq numeric(18) NOT NULL, -- 순번
	cntry_cd varchar(4) NOT NULL, -- 국가코드
	noti_type_cd varchar(4) NOT NULL, -- 공지유형코드
	txt_title varchar(255) NOT NULL, -- 제목
	txt_desc text NOT NULL, -- 내용
	txt_file varchar(255) NULL, -- 첨부파일명
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	popup_flag varchar(1) NULL DEFAULT 'N'::character varying, -- 팝업여부
	popup_start_tm varchar(10) NULL, -- 팝업시작일시
	popup_end_tm varchar(10) NULL, -- 팝업종료일시
	popup_tp_cd varchar(1) NULL, -- 팝업 유형(긴급, 일반)
	CONSTRAINT pk_plf_notice PRIMARY KEY (noti_seq, cntry_cd)
);
COMMENT ON TABLE wems.tb_plf_notice IS '시스템 운영에 필요한 공지사항정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_notice.noti_seq IS '순번';
COMMENT ON COLUMN wems.tb_plf_notice.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_plf_notice.noti_type_cd IS '공지유형코드';
COMMENT ON COLUMN wems.tb_plf_notice.txt_title IS '제목';
COMMENT ON COLUMN wems.tb_plf_notice.txt_desc IS '내용';
COMMENT ON COLUMN wems.tb_plf_notice.txt_file IS '첨부파일명';
COMMENT ON COLUMN wems.tb_plf_notice.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_plf_notice.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_plf_notice.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_plf_notice.update_dt IS '수정일시';
COMMENT ON COLUMN wems.tb_plf_notice.popup_flag IS '팝업여부';
COMMENT ON COLUMN wems.tb_plf_notice.popup_start_tm IS '팝업시작일시';
COMMENT ON COLUMN wems.tb_plf_notice.popup_end_tm IS '팝업종료일시';
COMMENT ON COLUMN wems.tb_plf_notice.popup_tp_cd IS '팝업 유형(긴급, 일반)';

-- Permissions

ALTER TABLE wems.tb_plf_notice OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_notice TO wems;

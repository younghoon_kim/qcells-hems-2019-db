-- Drop table

-- DROP TABLE wems.tb_plf_popup;

CREATE TABLE wems.tb_plf_popup (
	popup_seq float8 NOT NULL, -- 순번
	cntry_cd varchar(4) NOT NULL, -- 국가코드
	txt_title varchar(4000) NOT NULL, -- 제목
	file_name varchar(4000) NOT NULL, -- 파일명(암호화)
	file_path varchar(4000) NOT NULL, -- 파일 경로(암호화)
	disp_name varchar(4000) NOT NULL, -- 화면에 표시되는 명칭
	popup_start_tm varchar(10) NOT NULL, -- 노출시작일시
	popup_end_tm varchar(10) NOT NULL, -- 노출종료일시
	active_flag varchar(1) NOT NULL, -- 활성 여부
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_plf_popup PRIMARY KEY (popup_seq, cntry_cd)
);
COMMENT ON TABLE wems.tb_plf_popup IS '시스템 운영에 필요한 팝업공지사항정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_popup.popup_seq IS '순번';
COMMENT ON COLUMN wems.tb_plf_popup.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_plf_popup.txt_title IS '제목';
COMMENT ON COLUMN wems.tb_plf_popup.file_name IS '파일명(암호화)';
COMMENT ON COLUMN wems.tb_plf_popup.file_path IS '파일 경로(암호화)';
COMMENT ON COLUMN wems.tb_plf_popup.disp_name IS '화면에 표시되는 명칭';
COMMENT ON COLUMN wems.tb_plf_popup.popup_start_tm IS '노출시작일시';
COMMENT ON COLUMN wems.tb_plf_popup.popup_end_tm IS '노출종료일시';
COMMENT ON COLUMN wems.tb_plf_popup.active_flag IS '활성 여부';
COMMENT ON COLUMN wems.tb_plf_popup.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_plf_popup.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_plf_popup.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_plf_popup.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_plf_popup OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_popup TO wems;

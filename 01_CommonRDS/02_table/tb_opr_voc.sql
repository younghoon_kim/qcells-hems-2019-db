-- Drop table

-- DROP TABLE wems.tb_opr_voc;

CREATE TABLE wems.tb_opr_voc (
	voc_seq float8 NOT NULL, -- 일련번호
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	voc_type_cd varchar(4) NOT NULL, -- 작업유형코드
	subject varchar(4000) NULL, -- 제목
	contents1 varchar(4000) NOT NULL, -- 내용1
	contents2 varchar(4000) NULL, -- 내용2
	repl_flag varchar(1) NOT NULL DEFAULT 'N'::character varying, -- 댓글 등록 여부
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_opr_voc PRIMARY KEY (voc_seq)
);
COMMENT ON TABLE wems.tb_opr_voc IS 'VOC 내역을 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_voc.voc_seq IS '일련번호';
COMMENT ON COLUMN wems.tb_opr_voc.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_opr_voc.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_opr_voc.voc_type_cd IS '작업유형코드';
COMMENT ON COLUMN wems.tb_opr_voc.subject IS '제목';
COMMENT ON COLUMN wems.tb_opr_voc.contents1 IS '내용1';
COMMENT ON COLUMN wems.tb_opr_voc.contents2 IS '내용2';
COMMENT ON COLUMN wems.tb_opr_voc.repl_flag IS '댓글 등록 여부';
COMMENT ON COLUMN wems.tb_opr_voc.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_opr_voc.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_opr_voc.update_dt IS '수정일시';

-- Table Triggers

-- DROP TRIGGER tgr_act_voc ON wems.tb_opr_voc;

create trigger tgr_act_voc after
insert
    on
    wems.tb_opr_voc for each row execute procedure wems."tgr_act_voc$tb_opr_voc"();

-- Permissions

ALTER TABLE wems.tb_opr_voc OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_voc TO wems;

-- Drop table

-- DROP TABLE wems.tb_opr_mtnc;

CREATE TABLE wems.tb_opr_mtnc (
	mtnc_seq float8 NOT NULL, -- 유지보수이력 일련번호
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	mtnc_type_cd varchar(4) NOT NULL, -- 유지보수 유형코드
	mtnc_dt varchar(12) NOT NULL, -- 유지보수 일시
	subject varchar(4000) NOT NULL, -- 제목
	contents1 varchar(4000) NOT NULL, -- 내용1
	contents2 varchar(4000) NULL, -- 내용2
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_dt timestamptz NULL, -- 수정일시
	mtnc_stus_cd varchar(4) NOT NULL, -- 유지보수상태코드
	CONSTRAINT pk_opr_mtnc PRIMARY KEY (mtnc_seq)
);
COMMENT ON TABLE wems.tb_opr_mtnc IS '장비의 유지보수이력 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_mtnc.mtnc_seq IS '유지보수이력 일련번호';
COMMENT ON COLUMN wems.tb_opr_mtnc.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_opr_mtnc.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_opr_mtnc.mtnc_type_cd IS '유지보수 유형코드';
COMMENT ON COLUMN wems.tb_opr_mtnc.mtnc_dt IS '유지보수 일시';
COMMENT ON COLUMN wems.tb_opr_mtnc.subject IS '제목';
COMMENT ON COLUMN wems.tb_opr_mtnc.contents1 IS '내용1';
COMMENT ON COLUMN wems.tb_opr_mtnc.contents2 IS '내용2';
COMMENT ON COLUMN wems.tb_opr_mtnc.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_opr_mtnc.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_opr_mtnc.update_dt IS '수정일시';
COMMENT ON COLUMN wems.tb_opr_mtnc.mtnc_stus_cd IS '유지보수상태코드';

-- Table Triggers

-- DROP TRIGGER tgr_act_mtnc ON wems.tb_opr_mtnc;

create trigger tgr_act_mtnc after
insert
    on
    wems.tb_opr_mtnc for each row execute procedure wems."tgr_act_mtnc$tb_opr_mtnc"();

-- Permissions

ALTER TABLE wems.tb_opr_mtnc OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_mtnc TO wems;

-- Drop table

-- DROP TABLE wems.tb_raw_bms_info_tmp;

CREATE TABLE wems.tb_raw_bms_info_tmp (
	device_id varchar(50) NOT NULL,
	colec_dt varchar(14) NOT NULL,
	done_flag varchar(1) NULL,
	col0 varchar(50) NULL,
	col1 varchar(50) NULL,
	col2 varchar(50) NULL,
	col3 varchar(50) NULL,
	col4 varchar(50) NULL,
	col5 varchar(50) NULL,
	col6 varchar(50) NULL,
	col7 varchar(50) NULL,
	col8 varchar(50) NULL,
	col9 varchar(50) NULL,
	col10 varchar(50) NULL,
	col11 varchar(50) NULL,
	col12 varchar(50) NULL,
	col13 varchar(50) NULL,
	col14 varchar(50) NULL,
	col15 varchar(50) NULL,
	col16 varchar(50) NULL,
	col17 varchar(50) NULL,
	col18 varchar(50) NULL,
	col19 varchar(50) NULL,
	col20 varchar(50) NULL,
	col21 varchar(50) NULL,
	col22 varchar(50) NULL,
	col23 varchar(50) NULL,
	col24 varchar(50) NULL,
	col25 varchar(50) NULL,
	col26 varchar(50) NULL,
	col27 varchar(50) NULL,
	col28 varchar(50) NULL,
	col29 varchar(50) NULL,
	col30 varchar(100) NULL,
	col31 varchar(100) NULL,
	col32 varchar(50) NULL,
	col33 varchar(50) NULL,
	col34 varchar(100) NULL,
	col35 varchar(100) NULL,
	col36 varchar(50) NULL,
	col37 varchar(50) NULL,
	col38 varchar(100) NULL,
	col39 varchar(100) NULL,
	col40 varchar(50) NULL,
	create_dt timestamptz NULL,
	handle_flag varchar(14) NULL
);

-- Permissions

ALTER TABLE wems.tb_raw_bms_info_tmp OWNER TO wems;
GRANT ALL ON TABLE wems.tb_raw_bms_info_tmp TO wems;

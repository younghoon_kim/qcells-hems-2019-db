-- Drop table

-- DROP TABLE wems.tb_plf_firmware_info;

CREATE TABLE wems.tb_plf_firmware_info (
	grp_id varchar(50) NOT NULL, -- 펌웨어 관리 ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	product_model_nm varchar(30) NOT NULL, -- 장비모델명
	ems_model_nm varchar(30) NOT NULL, -- EMS모델명
	pcs_model_nm varchar(30) NOT NULL, -- PCS 모델명
	bms_model_nm varchar(30) NOT NULL, -- BMS 모델명
	ems_ver varchar(30) NOT NULL, -- EMS 버젼
	pcs_ver varchar(30) NOT NULL, -- PCS 버젼
	bms_ver varchar(30) NOT NULL, -- BMS 버젼
	ems_fw_url varchar(255) NOT NULL, -- EMS펌웨어 URL
	pcs_fw_url varchar(255) NOT NULL, -- PCS펌웨어 URL
	bms_fw_url varchar(255) NOT NULL, -- BMS 펌웨어 URL
	ems_fw_size numeric(16) NOT NULL, -- EMS 펌웨어 파일 크기
	pcs_fw_size numeric(16) NOT NULL, -- PCS 펌웨어 파일 크기
	bms_fw_size numeric(16) NOT NULL, -- BMS 펌웨어 파일 크기
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL,
	grp_upt_seq float8 NOT NULL, -- 그룹 변경 이력 순번
	fwi_upt_seq float8 NOT NULL DEFAULT 1, -- 펌웨어 관리정보 변경이력 순번
	stable_ver_flag varchar(1) NULL, -- Stable 버전 여부
	CONSTRAINT pk_plf_firmware_info PRIMARY KEY (grp_id, grp_upt_seq)
);
COMMENT ON TABLE wems.tb_plf_firmware_info IS '장비의 펌웨어 관리정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_firmware_info.grp_id IS '펌웨어 관리 ID';
COMMENT ON COLUMN wems.tb_plf_firmware_info.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_plf_firmware_info.product_model_nm IS '장비모델명';
COMMENT ON COLUMN wems.tb_plf_firmware_info.ems_model_nm IS 'EMS모델명';
COMMENT ON COLUMN wems.tb_plf_firmware_info.pcs_model_nm IS 'PCS 모델명';
COMMENT ON COLUMN wems.tb_plf_firmware_info.bms_model_nm IS 'BMS 모델명';
COMMENT ON COLUMN wems.tb_plf_firmware_info.ems_ver IS 'EMS 버젼';
COMMENT ON COLUMN wems.tb_plf_firmware_info.pcs_ver IS 'PCS 버젼';
COMMENT ON COLUMN wems.tb_plf_firmware_info.bms_ver IS 'BMS 버젼';
COMMENT ON COLUMN wems.tb_plf_firmware_info.ems_fw_url IS 'EMS펌웨어 URL';
COMMENT ON COLUMN wems.tb_plf_firmware_info.pcs_fw_url IS 'PCS펌웨어 URL';
COMMENT ON COLUMN wems.tb_plf_firmware_info.bms_fw_url IS 'BMS 펌웨어 URL';
COMMENT ON COLUMN wems.tb_plf_firmware_info.ems_fw_size IS 'EMS 펌웨어 파일 크기';
COMMENT ON COLUMN wems.tb_plf_firmware_info.pcs_fw_size IS 'PCS 펌웨어 파일 크기';
COMMENT ON COLUMN wems.tb_plf_firmware_info.bms_fw_size IS 'BMS 펌웨어 파일 크기';
COMMENT ON COLUMN wems.tb_plf_firmware_info.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_plf_firmware_info.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_plf_firmware_info.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_plf_firmware_info.grp_upt_seq IS '그룹 변경 이력 순번';
COMMENT ON COLUMN wems.tb_plf_firmware_info.fwi_upt_seq IS '펌웨어 관리정보 변경이력 순번';
COMMENT ON COLUMN wems.tb_plf_firmware_info.stable_ver_flag IS 'Stable 버전 여부';

-- Permissions

ALTER TABLE wems.tb_plf_firmware_info OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_firmware_info TO wems;

create table tb_bas_super_device
(
    site_id       integer not null,
    device_id varchar,
    device_type_cd   varchar, --ex) PMS, Battery 등등
    prn_device_id  varchar, --상위 디바이스 S/N
    product_model_nm varchar,
    update_dt     timestamp with time zone,
    create_dt     timestamp with time zone
);


comment on table tb_bas_super_device is '디바이스 정보를 저장하는 부모 테이블';
comment on column tb_bas_super_device.site_id is 'site_id';
comment on column tb_bas_super_device.device_id is '장비ID';
comment on column tb_bas_super_device.device_type_cd is '장비유형코드';
comment on column tb_bas_super_device.prn_device_id is '상위장비ID';
comment on column tb_bas_super_device.product_model_nm is '제품모델명';
comment on column tb_bas_super_device.update_dt is '수정일시';
comment on column tb_bas_super_device.create_dt is '등록일시';
-- Drop table

-- DROP TABLE wems.tb_opr_instlr_ess_state;

CREATE TABLE wems.tb_opr_instlr_ess_state (
	user_id varchar(64) NOT NULL, -- 사용자ID (암호화)
	colec_dd varchar(8) NOT NULL, -- 수집일
	tot_cnt float8 NULL, -- 전체 ESS 수
	nml_cnt float8 NULL, -- 정상 ESS 수
	nml_per numeric(4,1) NULL, -- 정상 ESS %
	warn_cnt float8 NULL, -- 경고 ESS 수
	warn_per numeric(4,1) NULL, -- 경고 ESS %
	err_cnt float8 NULL, -- 에러 ESS 수
	err_per numeric(4,1) NULL, -- 에러 ESS %
	no_sig_cnt float8 NULL, -- 신호없음 ESS 수
	no_sig_per numeric(4,1) NULL, -- 신호없음 ESS %
	std_cnt float8 NULL, -- 독립운전 ESS 수
	std_per numeric(4,1) NULL, -- 독립운전 ESS %
	rct_instl_cnt float8 NULL, -- 최근 설치 ESS 수
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	update_dt timestamptz NULL, -- 수정일시
	esm_cnt float8 NULL, -- EMS Sleep 모드 수
	esm_per numeric(4,1) NULL, -- EMS Sleep 모드 %
	CONSTRAINT pk_opr_instlr_ess_state PRIMARY KEY (user_id)
);
COMMENT ON TABLE wems.tb_opr_instlr_ess_state IS '인스톨러별 ESS 운용 건수 현황';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.user_id IS '사용자ID (암호화)';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.colec_dd IS '수집일';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.tot_cnt IS '전체 ESS 수';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.nml_cnt IS '정상 ESS 수';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.nml_per IS '정상 ESS %';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.warn_cnt IS '경고 ESS 수';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.warn_per IS '경고 ESS %';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.err_cnt IS '에러 ESS 수';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.err_per IS '에러 ESS %';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.no_sig_cnt IS '신호없음 ESS 수';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.no_sig_per IS '신호없음 ESS %';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.std_cnt IS '독립운전 ESS 수';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.std_per IS '독립운전 ESS %';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.rct_instl_cnt IS '최근 설치 ESS 수';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.create_dt IS '생성일시';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.update_dt IS '수정일시';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.esm_cnt IS 'EMS Sleep 모드 수';
COMMENT ON COLUMN wems.tb_opr_instlr_ess_state.esm_per IS 'EMS Sleep 모드 %';

-- Permissions

ALTER TABLE wems.tb_opr_instlr_ess_state OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_instlr_ess_state TO wems;

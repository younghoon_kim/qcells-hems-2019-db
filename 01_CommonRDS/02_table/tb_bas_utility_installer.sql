-- Drop table

-- DROP TABLE wems.tb_bas_utility_installer;

CREATE TABLE wems.tb_bas_utility_installer (
	utility_id varchar(64) NOT NULL, -- 유틸리티ID
	installer_id varchar(64) NOT NULL, -- 인스톨러ID
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_bas_utility_installer PRIMARY KEY (utility_id, installer_id)
);
COMMENT ON TABLE wems.tb_bas_utility_installer IS '유틸리티, 인스톨러 매핑테이블';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_utility_installer.utility_id IS '유틸리티ID';
COMMENT ON COLUMN wems.tb_bas_utility_installer.installer_id IS '인스톨러ID';
COMMENT ON COLUMN wems.tb_bas_utility_installer.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_bas_utility_installer.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_utility_installer.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_bas_utility_installer.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_bas_utility_installer OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_utility_installer TO wems;

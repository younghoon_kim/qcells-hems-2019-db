
-- Drop table

-- DROP TABLE wems.tb_raw_login_m2m;

CREATE TABLE wems.tb_raw_login_m2m (
	device_id varchar(50) NULL,
	create_tm varchar(14) NULL,
	product_model_nm varchar(30) NULL,
	"password" varchar(255) NULL,
	ems_model_nm varchar(30) NULL,
	pcs_model_nm varchar(30) NULL,
	bms_model_nm varchar(30) NULL,
	ems_ver varchar(30) NULL,
	pcs_ver varchar(30) NULL,
	bms_ver varchar(30) NULL,
	ems_update_dt varchar(14) NULL,
	ems_update_err_cd varchar(4) NULL,
	pcs_update_dt varchar(14) NULL,
	pcs_update_err_cd varchar(4) NULL,
	bms_update_dt varchar(14) NULL,
	bms_update_err_cd varchar(4) NULL,
	response_cd varchar(4) NOT NULL,
	ems_ipaddress varchar(30) NULL,
	ems_out_ipaddress varchar(30) NULL,
	connector_id varchar(30) NULL,
	ems_tm varchar(14) NOT NULL
);

-- Permissions

ALTER TABLE wems.tb_raw_login_m2m OWNER TO wems;
GRANT ALL ON TABLE wems.tb_raw_login_m2m TO wems;
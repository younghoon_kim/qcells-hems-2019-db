-- Drop table

-- DROP TABLE tb_stt_ess_tm;

CREATE TABLE tb_stt_ess_tm (
	colec_tm varchar(10) NOT NULL, -- 집계일시
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	pv_pw_h numeric(18,2) NULL, -- PV전력량
	pv_pw_price numeric(22,4) NULL, -- PV전력요금
	pv_pw_co2 numeric(20,6) NULL, -- PV탄소배출량
	cons_pw_h numeric(18,2) NULL, -- 가정내소비전력량-TOTAL
	cons_pw_price numeric(22,4) NULL, -- 가정내소비전력요금-TOTAL
	cons_pw_co2 numeric(20,6) NULL, -- 가정내소비전력탄소배출량-TOTAL
	cons_grid_pw_h numeric(18,2) NULL, -- 가정내소비전력량-GRID
	cons_grid_pw_price numeric(22,4) NULL, -- 가정내소비전력요금-GRID
	cons_grid_pw_co2 numeric(20,6) NULL, -- 가정내소비전력탄소배출량-GRID
	cons_pv_pw_h numeric(18,2) NULL, -- 가정내소비전력량-PV
	cons_pv_pw_price numeric(22,4) NULL, -- 가정내소비전력요금-PV
	cons_pv_pw_co2 numeric(20,6) NULL, -- 가정내소비전력탄소배출량-PV
	cons_bt_pw_h numeric(18,2) NULL, -- 가정내소비전력량-배터리
	cons_bt_pw_price numeric(22,4) NULL, -- 가정내소비전력요금-배터리
	cons_bt_pw_co2 numeric(20,6) NULL, -- 가정내소비전력탄소배출량-배터리
	bt_chrg_pw_h numeric(18,2) NULL, -- 배터리전력량-충전
	bt_chrg_pw_price numeric(22,4) NULL, -- 배터리전력요금-충전
	bt_chrg_pw_co2 numeric(20,6) NULL, -- 배터리탄소배출량-충전
	bt_dchrg_pw_h numeric(18,2) NULL, -- 배터리전력량-방전
	bt_dchrg_pw_price numeric(22,4) NULL, -- 배터리전력요금-방전
	bt_dchrg_pw_co2 numeric(20,6) NULL, -- 배터리탄소배출량-방전
	grid_ob_pw_h numeric(18,2) NULL, -- Grid전력량-수전
	grid_ob_pw_price numeric(22,4) NULL, -- Grid전력요금-수전
	grid_ob_pw_co2 numeric(20,6) NULL, -- Grid탄소배출량-수전
	grid_tr_pw_h numeric(18,2) NULL, -- Grid 전력량 - 매전 Total
	grid_tr_pw_price numeric(22,4) NULL, -- Grid 전력요금 - 매전 Total
	grid_tr_pw_co2 numeric(20,6) NULL, -- Grid 탄소배출량 - 매전 Total
	grid_tr_pv_pw_h numeric(18,2) NULL, -- Grid 전력량 - 매전 중 PV
	grid_tr_pv_pw_price numeric(22,4) NULL, -- Grid 전력요금 - 매전 중 PV
	grid_tr_pv_pw_co2 numeric(20,6) NULL, -- Grid 탄소배출량 - 매전 중 PV
	grid_tr_bt_pw_h numeric(18,2) NULL, -- Grid 전력량 - 매전 중 배터리
	grid_tr_bt_pw_price numeric(22,4) NULL, -- Grid 전력요금 - 매전 중 배터리
	grid_tr_bt_pw_co2 numeric(20,6) NULL, -- Grid 탄소배출량 - 매전 중 배터리
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	bt_soc numeric(5,1) NULL, -- 배터리충전량백분율(SOC) 평균
	outlet_pw_h numeric(18,2) NULL, -- Power Outlet 전력량
	outlet_pw_price numeric(18,2) NULL, -- Power Outlet 전력요금
	outlet_pw_co2 numeric(18,2) NULL, -- Power Outlet 탄소배출량
	pv_pw numeric(18,2) NULL, -- PV현재 발전 전력
	cons_pw numeric(18,2) NULL, -- 부하현재전력
	bt_pw numeric(18,2) NULL, -- 배터리현재전력
	bt_soh numeric(10) NULL, -- 배터리잔존수명(SOH)
	grid_pw numeric(18,2) NULL, -- GRID현재전력
	pcs_pw numeric(18,2) NULL, -- PCS 인버터 파워
	pcs_tgt_pw numeric(18,2) NULL, -- PCS 타겟파워
	pcs_fd_pw_h numeric(18,2) NULL, -- PCS 인버터 전력 발전량
	pcs_fd_pw_price numeric(18,2) NULL, -- PCS 인버터 전력 발전요금
	pcs_fd_pw_co2 numeric(18,2) NULL, -- PCS 인버터 전력 발전 탄소배출량
	pcs_pch_pw_h numeric(18,2) NULL, -- PCS 인버터 전력 사용량
	pcs_pch_pw_price numeric(18,2) NULL, -- PCS 인버터 전력 사용요금
	pcs_pch_pw_co2 numeric(18,2) NULL, -- PCS 인버터 전력 사용 탄소배출량
	outlet_pw numeric(18,2) NULL, -- Power Outlet 전력
	pv_v1 numeric(5,1) NULL, -- PV 전압1
	pv_i1 numeric(4,1) NULL, -- PV 전류1
	pv_pw1 numeric(5) NULL, -- PV 파워1
	pv_v2 numeric(5,1) NULL, -- PV 전압2
	pv_i2 numeric(4,1) NULL, -- PV 전류2
	pv_pw2 numeric(5) NULL, -- PV 파워2
	inverter_v numeric(5,1) NULL, -- 인버터 전압
	inverter_i numeric(4,1) NULL, -- 인버터 전류
	inverter_pw numeric(5) NULL, -- 인버터 파워
	dc_link_v numeric(5,1) NULL, -- DC Link 전압
	g_rly_cnt float8 NULL, -- Relay Count (G-relay)
	bat_rly_cnt float8 NULL, -- Relay Count (BAT-relay)
	grid_to_grid_rly_cnt float8 NULL, -- Relay Count (Grid to Grid relay)
	bat_pchrg_rly_cnt float8 NULL, -- Relay Count (BAT-Precharge-relay)
	rack_v numeric(5,1) NULL, -- 랙 전압
	rack_i numeric(4,1) NULL, -- 랙 전류
	cell_max_v numeric(4,3) NULL, -- 셀 최대 전압
	cell_min_v numeric(4,3) NULL, -- 셀 최소 전압
	cell_avg_v numeric(4,3) NULL, -- 셀 평균 전압
	cell_max_t numeric(4,1) NULL, -- 셀 최대 온도
	cell_min_t numeric(4,1) NULL, -- 셀 최소 온도
	cell_avg_t numeric(4,1) NULL, -- 셀 평균 온도
	smtr_tr_cnter float8 NULL, -- 스마트미터 총 사용량
	smtr_ob_cnter float8 NULL, -- 스마트미터 총 구매량
    bt_real_soc numeric(5,1) NULL, -- real 배터리충전량백분율(SOC) 평균
	load_main_pw numeric(18,2) NULL,
    load_sub_pw numeric(18,2) NULL,
    load_main_pw_h numeric(18,2) NULL,
	load_sub_pw_h numeric(18,2) NULL
)
PARTITION BY RANGE (colec_tm);
CREATE UNIQUE INDEX pk_stt_ess_tm ON ONLY tb_stt_ess_tm USING btree (colec_tm, device_id, device_type_cd);
COMMENT ON TABLE tb_stt_ess_tm IS '시간별 ESS 통계데이터 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN tb_stt_ess_tm.colec_tm IS '집계일시';
COMMENT ON COLUMN tb_stt_ess_tm.device_id IS '장비ID';
COMMENT ON COLUMN tb_stt_ess_tm.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN tb_stt_ess_tm.pv_pw_h IS 'PV전력량';
COMMENT ON COLUMN tb_stt_ess_tm.pv_pw_price IS 'PV전력요금';
COMMENT ON COLUMN tb_stt_ess_tm.pv_pw_co2 IS 'PV탄소배출량';
COMMENT ON COLUMN tb_stt_ess_tm.cons_pw_h IS '가정내소비전력량-TOTAL';
COMMENT ON COLUMN tb_stt_ess_tm.cons_pw_price IS '가정내소비전력요금-TOTAL';
COMMENT ON COLUMN tb_stt_ess_tm.cons_pw_co2 IS '가정내소비전력탄소배출량-TOTAL';
COMMENT ON COLUMN tb_stt_ess_tm.cons_grid_pw_h IS '가정내소비전력량-GRID';
COMMENT ON COLUMN tb_stt_ess_tm.cons_grid_pw_price IS '가정내소비전력요금-GRID';
COMMENT ON COLUMN tb_stt_ess_tm.cons_grid_pw_co2 IS '가정내소비전력탄소배출량-GRID';
COMMENT ON COLUMN tb_stt_ess_tm.cons_pv_pw_h IS '가정내소비전력량-PV';
COMMENT ON COLUMN tb_stt_ess_tm.cons_pv_pw_price IS '가정내소비전력요금-PV';
COMMENT ON COLUMN tb_stt_ess_tm.cons_pv_pw_co2 IS '가정내소비전력탄소배출량-PV';
COMMENT ON COLUMN tb_stt_ess_tm.cons_bt_pw_h IS '가정내소비전력량-배터리';
COMMENT ON COLUMN tb_stt_ess_tm.cons_bt_pw_price IS '가정내소비전력요금-배터리';
COMMENT ON COLUMN tb_stt_ess_tm.cons_bt_pw_co2 IS '가정내소비전력탄소배출량-배터리';
COMMENT ON COLUMN tb_stt_ess_tm.bt_chrg_pw_h IS '배터리전력량-충전';
COMMENT ON COLUMN tb_stt_ess_tm.bt_chrg_pw_price IS '배터리전력요금-충전';
COMMENT ON COLUMN tb_stt_ess_tm.bt_chrg_pw_co2 IS '배터리탄소배출량-충전';
COMMENT ON COLUMN tb_stt_ess_tm.bt_dchrg_pw_h IS '배터리전력량-방전';
COMMENT ON COLUMN tb_stt_ess_tm.bt_dchrg_pw_price IS '배터리전력요금-방전';
COMMENT ON COLUMN tb_stt_ess_tm.bt_dchrg_pw_co2 IS '배터리탄소배출량-방전';
COMMENT ON COLUMN tb_stt_ess_tm.grid_ob_pw_h IS 'Grid전력량-수전';
COMMENT ON COLUMN tb_stt_ess_tm.grid_ob_pw_price IS 'Grid전력요금-수전';
COMMENT ON COLUMN tb_stt_ess_tm.grid_ob_pw_co2 IS 'Grid탄소배출량-수전';
COMMENT ON COLUMN tb_stt_ess_tm.grid_tr_pw_h IS 'Grid 전력량 - 매전 Total';
COMMENT ON COLUMN tb_stt_ess_tm.grid_tr_pw_price IS 'Grid 전력요금 - 매전 Total';
COMMENT ON COLUMN tb_stt_ess_tm.grid_tr_pw_co2 IS 'Grid 탄소배출량 - 매전 Total';
COMMENT ON COLUMN tb_stt_ess_tm.grid_tr_pv_pw_h IS 'Grid 전력량 - 매전 중 PV';
COMMENT ON COLUMN tb_stt_ess_tm.grid_tr_pv_pw_price IS 'Grid 전력요금 - 매전 중 PV';
COMMENT ON COLUMN tb_stt_ess_tm.grid_tr_pv_pw_co2 IS 'Grid 탄소배출량 - 매전 중 PV';
COMMENT ON COLUMN tb_stt_ess_tm.grid_tr_bt_pw_h IS 'Grid 전력량 - 매전 중 배터리';
COMMENT ON COLUMN tb_stt_ess_tm.grid_tr_bt_pw_price IS 'Grid 전력요금 - 매전 중 배터리';
COMMENT ON COLUMN tb_stt_ess_tm.grid_tr_bt_pw_co2 IS 'Grid 탄소배출량 - 매전 중 배터리';
COMMENT ON COLUMN tb_stt_ess_tm.create_dt IS '생성일시';
COMMENT ON COLUMN tb_stt_ess_tm.bt_soc IS '배터리충전량백분율(SOC) 평균';
COMMENT ON COLUMN tb_stt_ess_tm.outlet_pw_h IS 'Power Outlet 전력량';
COMMENT ON COLUMN tb_stt_ess_tm.outlet_pw_price IS 'Power Outlet 전력요금';
COMMENT ON COLUMN tb_stt_ess_tm.outlet_pw_co2 IS 'Power Outlet 탄소배출량';
COMMENT ON COLUMN tb_stt_ess_tm.pv_pw IS 'PV현재 발전 전력';
COMMENT ON COLUMN tb_stt_ess_tm.cons_pw IS '부하현재전력';
COMMENT ON COLUMN tb_stt_ess_tm.bt_pw IS '배터리현재전력';
COMMENT ON COLUMN tb_stt_ess_tm.bt_soh IS '배터리잔존수명(SOH)';
COMMENT ON COLUMN tb_stt_ess_tm.grid_pw IS 'GRID현재전력';
COMMENT ON COLUMN tb_stt_ess_tm.pcs_pw IS 'PCS 인버터 파워';
COMMENT ON COLUMN tb_stt_ess_tm.pcs_tgt_pw IS 'PCS 타겟파워';
COMMENT ON COLUMN tb_stt_ess_tm.pcs_fd_pw_h IS 'PCS 인버터 전력 발전량';
COMMENT ON COLUMN tb_stt_ess_tm.pcs_fd_pw_price IS 'PCS 인버터 전력 발전요금';
COMMENT ON COLUMN tb_stt_ess_tm.pcs_fd_pw_co2 IS 'PCS 인버터 전력 발전 탄소배출량';
COMMENT ON COLUMN tb_stt_ess_tm.pcs_pch_pw_h IS 'PCS 인버터 전력 사용량';
COMMENT ON COLUMN tb_stt_ess_tm.pcs_pch_pw_price IS 'PCS 인버터 전력 사용요금';
COMMENT ON COLUMN tb_stt_ess_tm.pcs_pch_pw_co2 IS 'PCS 인버터 전력 사용 탄소배출량';
COMMENT ON COLUMN tb_stt_ess_tm.outlet_pw IS 'Power Outlet 전력';
COMMENT ON COLUMN tb_stt_ess_tm.pv_v1 IS 'PV 전압1';
COMMENT ON COLUMN tb_stt_ess_tm.pv_i1 IS 'PV 전류1';
COMMENT ON COLUMN tb_stt_ess_tm.pv_pw1 IS 'PV 파워1';
COMMENT ON COLUMN tb_stt_ess_tm.pv_v2 IS 'PV 전압2';
COMMENT ON COLUMN tb_stt_ess_tm.pv_i2 IS 'PV 전류2';
COMMENT ON COLUMN tb_stt_ess_tm.pv_pw2 IS 'PV 파워2';
COMMENT ON COLUMN tb_stt_ess_tm.inverter_v IS '인버터 전압';
COMMENT ON COLUMN tb_stt_ess_tm.inverter_i IS '인버터 전류';
COMMENT ON COLUMN tb_stt_ess_tm.inverter_pw IS '인버터 파워';
COMMENT ON COLUMN tb_stt_ess_tm.dc_link_v IS 'DC Link 전압';
COMMENT ON COLUMN tb_stt_ess_tm.g_rly_cnt IS 'Relay Count (G-relay)';
COMMENT ON COLUMN tb_stt_ess_tm.bat_rly_cnt IS 'Relay Count (BAT-relay)';
COMMENT ON COLUMN tb_stt_ess_tm.grid_to_grid_rly_cnt IS 'Relay Count (Grid to Grid relay)';
COMMENT ON COLUMN tb_stt_ess_tm.bat_pchrg_rly_cnt IS 'Relay Count (BAT-Precharge-relay)';
COMMENT ON COLUMN tb_stt_ess_tm.rack_v IS '랙 전압';
COMMENT ON COLUMN tb_stt_ess_tm.rack_i IS '랙 전류';
COMMENT ON COLUMN tb_stt_ess_tm.cell_max_v IS '셀 최대 전압';
COMMENT ON COLUMN tb_stt_ess_tm.cell_min_v IS '셀 최소 전압';
COMMENT ON COLUMN tb_stt_ess_tm.cell_avg_v IS '셀 평균 전압';
COMMENT ON COLUMN tb_stt_ess_tm.cell_max_t IS '셀 최대 온도';
COMMENT ON COLUMN tb_stt_ess_tm.cell_min_t IS '셀 최소 온도';
COMMENT ON COLUMN tb_stt_ess_tm.cell_avg_t IS '셀 평균 온도';
COMMENT ON COLUMN tb_stt_ess_tm.smtr_tr_cnter IS '스마트미터 총 사용량';
COMMENT ON COLUMN tb_stt_ess_tm.smtr_ob_cnter IS '스마트미터 총 구매량';
COMMENT ON COLUMN tb_stt_ess_tm.bt_real_soc IS 'real 배터리충전량백분율(SOC) 평균';
COMMENT ON COLUMN tb_stt_ess_tm.load_main_pw is 'Load 중 main_load의 power';
COMMENT ON COLUMN tb_stt_ess_tm.load_sub_pw is 'Load 중 sub_load의 power';
COMMENT ON COLUMN tb_stt_ess_tm.load_main_pw_h IS 'Load 중 main_load의 Electrical energy';
COMMENT ON COLUMN tb_stt_ess_tm.load_sub_pw_h IS 'Load 중 sub_load의 Electrical energy';

-- Permissions

ALTER TABLE tb_stt_ess_tm OWNER TO wems;
GRANT ALL ON TABLE tb_stt_ess_tm TO wems;

-- Drop table

-- DROP TABLE wems.tb_opr_mv;

CREATE TABLE wems.tb_opr_mv (
	mv_seq float8 NOT NULL, -- 일련번호
	mv_url varchar(1000) NOT NULL, -- 동영상 URL
	pv_img_url varchar(1000) NULL, -- 미리보기 이미지 URL
	subject varchar(4000) NOT NULL, -- 제목
	create_id varchar(64) NOT NULL, -- 생성자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	update_id varchar(64) NULL, -- 생성자ID
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_opr_mv PRIMARY KEY (mv_seq)
);
COMMENT ON TABLE wems.tb_opr_mv IS '동영상 관리';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_mv.mv_seq IS '일련번호';
COMMENT ON COLUMN wems.tb_opr_mv.mv_url IS '동영상 URL';
COMMENT ON COLUMN wems.tb_opr_mv.pv_img_url IS '미리보기 이미지 URL';
COMMENT ON COLUMN wems.tb_opr_mv.subject IS '제목';
COMMENT ON COLUMN wems.tb_opr_mv.create_id IS '생성자ID';
COMMENT ON COLUMN wems.tb_opr_mv.create_dt IS '생성일시';
COMMENT ON COLUMN wems.tb_opr_mv.update_id IS '생성자ID';
COMMENT ON COLUMN wems.tb_opr_mv.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_opr_mv OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_mv TO wems;

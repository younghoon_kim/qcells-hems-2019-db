-- Drop table

-- DROP TABLE wems.tb_opr_oper_ctrl_hist;

CREATE TABLE wems.tb_opr_oper_ctrl_hist (
	req_id numeric(18) NOT NULL, -- 제어요청 일련번호
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	grp_id varchar(50) NOT NULL, -- 제어그룹ID
	grp_nm varchar(200) NULL, -- 제어그룹명
	ems_oper_mode_cd varchar(4) NULL, -- EMS운용모드
	era_mode_cd varchar(4) NULL, -- 원격자동모드
	erm_mode_1_cd varchar(4) NULL, -- 원격수동모드1
	erm_mode_2_cd varchar(4) NULL, -- 원격수동모드2
	erm_mode_3_cd varchar(4) NULL, -- 원격수동모드3
	feedin float8 NULL, -- FeedIn
	target_soc float8 NULL, -- 목표SOC
	target_pw float8 NULL, -- 목표입출력PW
	algo_flag float8 NULL, -- ALGO_FLAG
	acpsetpoint float8 NULL, -- ACPSetPoint
	acppemax float8 NULL, -- ACPPeMax
	rcpsetcospi float8 NULL, -- RCPSetCospi
	dcivariable float8 NULL, -- DCIVariable
	dcitriplevel float8 NULL, -- DCITripLevel
	rcpsetx1 float8 NULL, -- RCPSetX1
	rcpsety1 float8 NULL, -- RCPSetY1
	rcpsetx2 float8 NULL, -- RCPSetX2
	rcpsety2 float8 NULL, -- RCPSetY2
	rcpsetx3 float8 NULL, -- RCPSetX3
	rcpsety3 float8 NULL, -- RCPSetY3
	rcpsetx4 float8 NULL, -- RCPSetX4
	rcpsety4 float8 NULL, -- RCPSetY4
	response_err_cd varchar(4) NULL, -- 응답코드
	create_id varchar(64) NOT NULL,
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일자
	inverter_pw float8 NULL,
	basicmode_cd float8 NULL DEFAULT 0,
	cmd_req_status_cd varchar(4) NULL, -- 제어명령 진행상태 코드
	reversesshport float8 NULL, -- ReverseSSH용 포트번호
	apcdrop numeric(6,1) NULL, -- APCDROP
	lockin numeric(6,1) NULL, -- LOCKIN
	lockout numeric(6,1) NULL, -- LOCKOUT
	batteryapc numeric(6,1) NULL, -- BATTERYAPC
	algo_flag2 float8 NULL,
	rcpqfixdata float8 NULL,
	CONSTRAINT pk_opr_oper_ctrl_hist PRIMARY KEY (req_id, device_id, device_type_cd, grp_id)
);
COMMENT ON TABLE wems.tb_opr_oper_ctrl_hist IS 'ESS 제어요청정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.req_id IS '제어요청 일련번호';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.grp_id IS '제어그룹ID';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.grp_nm IS '제어그룹명';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.ems_oper_mode_cd IS 'EMS운용모드';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.era_mode_cd IS '원격자동모드';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.erm_mode_1_cd IS '원격수동모드1';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.erm_mode_2_cd IS '원격수동모드2';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.erm_mode_3_cd IS '원격수동모드3';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.feedin IS 'FeedIn';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.target_soc IS '목표SOC';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.target_pw IS '목표입출력PW';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.algo_flag IS 'ALGO_FLAG';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.acpsetpoint IS 'ACPSetPoint';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.acppemax IS 'ACPPeMax';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.rcpsetcospi IS 'RCPSetCospi';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.dcivariable IS 'DCIVariable';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.dcitriplevel IS 'DCITripLevel';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.rcpsetx1 IS 'RCPSetX1';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.rcpsety1 IS 'RCPSetY1';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.rcpsetx2 IS 'RCPSetX2';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.rcpsety2 IS 'RCPSetY2';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.rcpsetx3 IS 'RCPSetX3';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.rcpsety3 IS 'RCPSetY3';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.rcpsetx4 IS 'RCPSetX4';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.rcpsety4 IS 'RCPSetY4';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.response_err_cd IS '응답코드';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.create_dt IS '생성일자';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.cmd_req_status_cd IS '제어명령 진행상태 코드';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.reversesshport IS 'ReverseSSH용 포트번호';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.apcdrop IS 'APCDROP';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.lockin IS 'LOCKIN';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.lockout IS 'LOCKOUT';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl_hist.batteryapc IS 'BATTERYAPC';

-- Permissions

ALTER TABLE wems.tb_opr_oper_ctrl_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_oper_ctrl_hist TO wems;

-- Drop table

-- DROP TABLE wems.tb_bas_user_hash;

CREATE TABLE wems.tb_bas_user_hash (
	user_id varchar(64) NOT NULL,
	user_id_hash varchar(200) NULL,
	user_pwd_hash varchar(200) NOT NULL,
	user_email_hash varchar(200) NOT NULL,
	salt varchar(200) NOT NULL,
	create_dt timestamptz NOT NULL
);

-- Permissions

ALTER TABLE wems.tb_bas_user_hash OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_user_hash TO wems;

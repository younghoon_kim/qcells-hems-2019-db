-- Drop table

-- DROP TABLE wems.dc_cnf_mapping;

CREATE TABLE wems.dc_cnf_mapping (
	agentequipid varchar(40) NULL,
	agentportno numeric(18) NULL,
	bscno numeric(18) NULL,
	aciiheader varchar(83) NULL
);

-- Permissions

ALTER TABLE wems.dc_cnf_mapping OWNER TO wems;
GRANT ALL ON TABLE wems.dc_cnf_mapping TO wems;

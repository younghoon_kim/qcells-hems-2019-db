-- Drop table

-- DROP TABLE wems.tb_bas_user_device;

CREATE TABLE wems.tb_bas_user_device (
	user_id varchar(64) NOT NULL, -- 사용자ID
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_bas_user_device PRIMARY KEY (user_id, device_id, device_type_cd)
);
CREATE INDEX idx_bas_user_device_01 ON wems.tb_bas_user_device USING btree (device_id, user_id);
COMMENT ON TABLE wems.tb_bas_user_device IS '사용자가 소유하고 있는 ESS 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_user_device.user_id IS '사용자ID';
COMMENT ON COLUMN wems.tb_bas_user_device.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_bas_user_device.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_bas_user_device.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_bas_user_device.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_user_device.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_bas_user_device.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_bas_user_device OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_user_device TO wems;

-- Drop table

-- DROP TABLE wems.tb_opr_act;

CREATE TABLE wems.tb_opr_act (
	act_seq float8 NOT NULL, -- 일련번호
	act_type_cd varchar(4) NOT NULL, -- 활동유형코드
	device_id varchar(50) NULL, -- 장비ID
	device_type_cd varchar(4) NULL, -- 장비유형코드
	ref_seq float8 NULL, -- 참조 대상 테이블의 일련번호
	ref_type_cd varchar(4) NULL, -- 참조 대상의 유형 코드
	ref_desc varchar(4000) NULL, -- 부가 설명
	ref_user_id varchar(64) NULL, -- 관련 사용자ID (암호화)
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	CONSTRAINT pk_opr_act PRIMARY KEY (act_seq)
);
COMMENT ON TABLE wems.tb_opr_act IS '활동목록을 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_act.act_seq IS '일련번호';
COMMENT ON COLUMN wems.tb_opr_act.act_type_cd IS '활동유형코드';
COMMENT ON COLUMN wems.tb_opr_act.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_opr_act.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_opr_act.ref_seq IS '참조 대상 테이블의 일련번호';
COMMENT ON COLUMN wems.tb_opr_act.ref_type_cd IS '참조 대상의 유형 코드';
COMMENT ON COLUMN wems.tb_opr_act.ref_desc IS '부가 설명';
COMMENT ON COLUMN wems.tb_opr_act.ref_user_id IS '관련 사용자ID (암호화)';
COMMENT ON COLUMN wems.tb_opr_act.create_dt IS '등록일시';

-- Permissions

ALTER TABLE wems.tb_opr_act OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_act TO wems;

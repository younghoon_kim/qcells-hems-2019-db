-- Drop table

-- DROP TABLE wems.tb_bas_cntry;

CREATE TABLE wems.tb_bas_cntry (
	cntry_cd varchar(4) NOT NULL, -- 국가코드
	cntry_nm varchar(50) NULL, -- 국가명
	mone_unit_cd varchar(4) NULL, -- 화폐단위코드
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	lang_cd varchar(4) NULL,
	basic_latitude numeric(12,6) NULL,
	basic_longitude numeric(12,6) NULL,
	CONSTRAINT pk_bas_cntry PRIMARY KEY (cntry_cd)
);
COMMENT ON TABLE wems.tb_bas_cntry IS '국가 코드정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_cntry.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_bas_cntry.cntry_nm IS '국가명';
COMMENT ON COLUMN wems.tb_bas_cntry.mone_unit_cd IS '화폐단위코드';
COMMENT ON COLUMN wems.tb_bas_cntry.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_bas_cntry OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_cntry TO wems;

-- Drop table

-- DROP TABLE wems.tb_bas_svc_cntr;

CREATE TABLE wems.tb_bas_svc_cntr (
	svc_cntr_id varchar(10) NOT NULL, -- 센터 코드
	svc_cntr_nm varchar(1000) NOT NULL, -- 판매처 명
	cntry_cd varchar(4) NOT NULL, -- 국가코드
	addr varchar(4000) NULL, -- 주소 (암호화)
	email varchar(192) NULL, -- 이메일 (암호화)
	mpn_no varchar(64) NULL, -- 연락처 (암호화)
	create_id varchar(64) NULL, -- 등록자ID (암호화)
	create_dt timestamptz NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	staff_nm varchar(160) NULL, -- 담당자 명
	ord_seq float8 NULL, -- 정렬 순번
	CONSTRAINT pk_bas_svc_cntr PRIMARY KEY (svc_cntr_id)
);
COMMENT ON TABLE wems.tb_bas_svc_cntr IS '서비스센터 관리';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_svc_cntr.svc_cntr_id IS '센터 코드';
COMMENT ON COLUMN wems.tb_bas_svc_cntr.svc_cntr_nm IS '판매처 명';
COMMENT ON COLUMN wems.tb_bas_svc_cntr.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_bas_svc_cntr.addr IS '주소 (암호화)';
COMMENT ON COLUMN wems.tb_bas_svc_cntr.email IS '이메일 (암호화)';
COMMENT ON COLUMN wems.tb_bas_svc_cntr.mpn_no IS '연락처 (암호화)';
COMMENT ON COLUMN wems.tb_bas_svc_cntr.create_id IS '등록자ID (암호화)';
COMMENT ON COLUMN wems.tb_bas_svc_cntr.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_svc_cntr.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_bas_svc_cntr.update_dt IS '수정일시';
COMMENT ON COLUMN wems.tb_bas_svc_cntr.staff_nm IS '담당자 명';
COMMENT ON COLUMN wems.tb_bas_svc_cntr.ord_seq IS '정렬 순번';

-- Permissions

ALTER TABLE wems.tb_bas_svc_cntr OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_svc_cntr TO wems;

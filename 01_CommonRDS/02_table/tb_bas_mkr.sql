-- Drop table

-- DROP TABLE wems.tb_bas_mkr;

CREATE TABLE wems.tb_bas_mkr (
	mkr_id varchar(10) NOT NULL, -- 제조사ID
	mkr_nm varchar(30) NULL, -- 제조사명
	mkr_nm_s varchar(10) NULL, -- 제조사명(약식)
	mkr_addr varchar(4000) NULL, -- 제조사주소
	mkr_sales_email varchar(40) NULL, -- 판매자E-MAIL
	mkr_tel_no varchar(30) NULL, -- 제조사전화번호
	mkr_prms_no varchar(10) NULL, -- 제조사승인번호
	create_id varchar(64) NOT NULL, -- 생성자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_bas_mkr PRIMARY KEY (mkr_id)
);
COMMENT ON TABLE wems.tb_bas_mkr IS '관리대상(ESS, 스마트미터, PV, PCS 등) 장비의 제조사 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_mkr.mkr_id IS '제조사ID';
COMMENT ON COLUMN wems.tb_bas_mkr.mkr_nm IS '제조사명';
COMMENT ON COLUMN wems.tb_bas_mkr.mkr_nm_s IS '제조사명(약식)';
COMMENT ON COLUMN wems.tb_bas_mkr.mkr_addr IS '제조사주소';
COMMENT ON COLUMN wems.tb_bas_mkr.mkr_sales_email IS '판매자E-MAIL';
COMMENT ON COLUMN wems.tb_bas_mkr.mkr_tel_no IS '제조사전화번호';
COMMENT ON COLUMN wems.tb_bas_mkr.mkr_prms_no IS '제조사승인번호';
COMMENT ON COLUMN wems.tb_bas_mkr.create_id IS '생성자ID';
COMMENT ON COLUMN wems.tb_bas_mkr.create_dt IS '생성일시';
COMMENT ON COLUMN wems.tb_bas_mkr.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_bas_mkr.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_bas_mkr OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_mkr TO wems;

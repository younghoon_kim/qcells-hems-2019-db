-- Drop table

-- DROP TABLE wems.tb_raw_weather_reqhist;

CREATE TABLE wems.tb_raw_weather_reqhist (
	device_id varchar(50) NOT NULL, -- 날씨요청접수시간
	create_tm varchar(14) NOT NULL, -- 날씨요청응답시간
	response_tm varchar(14) NULL, -- 에러코드
	err_cd varchar(4) NULL, -- ERR_CD
	CONSTRAINT pk_raw_weather_reqhist PRIMARY KEY (device_id, create_tm)
);
COMMENT ON TABLE wems.tb_raw_weather_reqhist IS 'ESS 의 날씨정보처리내역을 보관한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_raw_weather_reqhist.device_id IS '날씨요청접수시간';
COMMENT ON COLUMN wems.tb_raw_weather_reqhist.create_tm IS '날씨요청응답시간';
COMMENT ON COLUMN wems.tb_raw_weather_reqhist.response_tm IS '에러코드';
COMMENT ON COLUMN wems.tb_raw_weather_reqhist.err_cd IS 'ERR_CD';

-- Permissions

ALTER TABLE wems.tb_raw_weather_reqhist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_raw_weather_reqhist TO wems;

-- Drop table

-- DROP TABLE wems.tb_opr_mng_file;

CREATE TABLE wems.tb_opr_mng_file (
	file_type_cd varchar(4) NOT NULL, -- 파일 유형 코드
	lang_cd varchar(4) NOT NULL, -- 언어 코드
	file_name varchar(4000) NOT NULL, -- 파일명(암호화)
	file_path varchar(4000) NOT NULL, -- 파일 경로(암호화)
	create_id varchar(64) NOT NULL, -- 생성자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	update_id varchar(64) NULL, -- 생성자ID
	update_dt timestamptz NULL, -- 생성일시
	ord_seq float8 NULL, -- 순번
	use_flag varchar(1) NOT NULL DEFAULT 'N'::character varying, -- 사용 여부
	disp_name varchar(4000) NOT NULL, -- 화면에 표시되는 파일명
	CONSTRAINT pk_opr_mng_file PRIMARY KEY (file_type_cd, lang_cd, file_name)
);
COMMENT ON TABLE wems.tb_opr_mng_file IS '매뉴얼, 동영상 등 웹페이지에서 제공하는 파일 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_mng_file.file_type_cd IS '파일 유형 코드';
COMMENT ON COLUMN wems.tb_opr_mng_file.lang_cd IS '언어 코드';
COMMENT ON COLUMN wems.tb_opr_mng_file.file_name IS '파일명(암호화)';
COMMENT ON COLUMN wems.tb_opr_mng_file.file_path IS '파일 경로(암호화)';
COMMENT ON COLUMN wems.tb_opr_mng_file.create_id IS '생성자ID';
COMMENT ON COLUMN wems.tb_opr_mng_file.create_dt IS '생성일시';
COMMENT ON COLUMN wems.tb_opr_mng_file.update_id IS '생성자ID';
COMMENT ON COLUMN wems.tb_opr_mng_file.update_dt IS '생성일시';
COMMENT ON COLUMN wems.tb_opr_mng_file.ord_seq IS '순번';
COMMENT ON COLUMN wems.tb_opr_mng_file.use_flag IS '사용 여부';
COMMENT ON COLUMN wems.tb_opr_mng_file.disp_name IS '화면에 표시되는 파일명';

-- Permissions

ALTER TABLE wems.tb_opr_mng_file OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_mng_file TO wems;

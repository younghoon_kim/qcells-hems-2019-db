-- Drop table

-- DROP TABLE wems.batch_job_execution_params;

CREATE TABLE wems.batch_job_execution_params (
	job_execution_id int8 NOT NULL,
	type_cd varchar(6) NOT NULL,
	key_name varchar(100) NOT NULL,
	string_val varchar(250) NULL,
	date_val timestamp NULL,
	long_val int8 NULL,
	double_val float8 NULL,
	identifying bpchar(1) NOT NULL
);

-- Permissions

ALTER TABLE wems.batch_job_execution_params OWNER TO wems;
GRANT ALL ON TABLE wems.batch_job_execution_params TO wems;

-- Drop table

-- DROP TABLE wems.tb_bas_ctl_grp;

CREATE TABLE wems.tb_bas_ctl_grp (
	grp_id varchar(50) NOT NULL, -- 제어그룹ID
	grp_type_cd varchar(4) NOT NULL, -- 제어그룹 유형코드
	grp_nm varchar(200) NULL, -- 제어그룹명
	ctl_type_cd varchar(4) NULL, -- 제어유형코드
	ctl_start_dt varchar(14) NULL, -- 제어시작일시
	ctl_end_dt varchar(14) NULL, -- 제어종료일시
	ctl_stus_cd varchar(4) NULL, -- 제어상태코드
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_bas_ctl_grp PRIMARY KEY (grp_id, grp_type_cd)
);
COMMENT ON TABLE wems.tb_bas_ctl_grp IS '제어그룹정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_ctl_grp.grp_id IS '제어그룹ID';
COMMENT ON COLUMN wems.tb_bas_ctl_grp.grp_type_cd IS '제어그룹 유형코드';
COMMENT ON COLUMN wems.tb_bas_ctl_grp.grp_nm IS '제어그룹명';
COMMENT ON COLUMN wems.tb_bas_ctl_grp.ctl_type_cd IS '제어유형코드';
COMMENT ON COLUMN wems.tb_bas_ctl_grp.ctl_start_dt IS '제어시작일시';
COMMENT ON COLUMN wems.tb_bas_ctl_grp.ctl_end_dt IS '제어종료일시';
COMMENT ON COLUMN wems.tb_bas_ctl_grp.ctl_stus_cd IS '제어상태코드';
COMMENT ON COLUMN wems.tb_bas_ctl_grp.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_bas_ctl_grp.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_ctl_grp.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_bas_ctl_grp.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_bas_ctl_grp OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_ctl_grp TO wems;

-- Drop table
DROP TABLE IF EXISTS dc_cnf_net_conn_file;

-- Create table
create table dc_cnf_net_conn_file
(
	seq bigserial not null
		constraint dc_cnf_net_conn_file_pkey
			primary key,
	dev_id varchar(100) not null,
	file_date varchar(14),
	file_nm varchar(100) not null,
	row_cnt numeric(8) default 0,
	proc_date varchar(8),
	proc_flag numeric(1) default 0,
	lst_dt timestamp with time zone default sys_extract_utc(now())
);

comment on table dc_cnf_net_conn_file is 'ESS 장비 네트워크 단절시 ftp로 업로드된 파일';
comment on column dc_cnf_net_conn_file.seq is '일련번호';
comment on column dc_cnf_net_conn_file.dev_id is '디바이스 아이디';
comment on column dc_cnf_net_conn_file.file_date is '파일명에 있는 일자';
comment on column dc_cnf_net_conn_file.file_nm is '파일명';
comment on column dc_cnf_net_conn_file.row_cnt is '파일 row 건수';
comment on column dc_cnf_net_conn_file.proc_date is '처리일자 YYYYMMDD';
comment on column dc_cnf_net_conn_file.proc_flag is '처리상태 0:미처리 1:처리';
comment on column dc_cnf_net_conn_file.lst_dt is '최종수정일시';

alter table dc_cnf_net_conn_file owner to wems;

-- sequence 생성
-- create sequence dc_cnf_net_conn_file_seq_seq;



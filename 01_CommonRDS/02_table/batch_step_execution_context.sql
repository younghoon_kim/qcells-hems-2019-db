-- Drop table

-- DROP TABLE wems.batch_step_execution_context;

CREATE TABLE wems.batch_step_execution_context (
	step_execution_id int8 NOT NULL,
	short_context varchar(2500) NOT NULL,
	serialized_context text NULL,
	CONSTRAINT batch_step_execution_context_pkey PRIMARY KEY (step_execution_id)
);

-- Permissions

ALTER TABLE wems.batch_step_execution_context OWNER TO wems;
GRANT ALL ON TABLE wems.batch_step_execution_context TO wems;

-- Drop table

-- DROP TABLE wems.tb_sys_exec_hist;

CREATE TABLE wems.tb_sys_exec_hist (
	exec_seq numeric(18) NOT NULL, -- 실행SEQ
	menu_id varchar(15) NULL, -- 메뉴ID
	func_id varchar(20) NULL, -- 기능ID
	exec_type_cd varchar(4) NULL, -- 실행구분코드
	login_seq numeric(18) NULL, -- 접속SEQ
	user_id varchar(64) NULL, -- 사용자ID
	create_dt timestamptz NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	exec_desc varchar(4000) NULL,
	CONSTRAINT pk_sys_exec_hist PRIMARY KEY (exec_seq)
);
COMMENT ON TABLE wems.tb_sys_exec_hist IS '시스템에 접속한 사용자의 실행이력 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_sys_exec_hist.exec_seq IS '실행SEQ';
COMMENT ON COLUMN wems.tb_sys_exec_hist.menu_id IS '메뉴ID';
COMMENT ON COLUMN wems.tb_sys_exec_hist.func_id IS '기능ID';
COMMENT ON COLUMN wems.tb_sys_exec_hist.exec_type_cd IS '실행구분코드';
COMMENT ON COLUMN wems.tb_sys_exec_hist.login_seq IS '접속SEQ';
COMMENT ON COLUMN wems.tb_sys_exec_hist.user_id IS '사용자ID';
COMMENT ON COLUMN wems.tb_sys_exec_hist.create_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.tb_sys_exec_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_sys_exec_hist TO wems;

-- Drop table

-- DROP TABLE wems.tb_plf_alarm_code;

CREATE TABLE wems.tb_plf_alarm_code (
	alarm_type_cd varchar(4) NOT NULL, -- 장애유형코드
	alarm_cd varchar(4) NOT NULL, -- 장애코드
	device_type_cd varchar(4) NULL, -- 장비유형코드
	alarm_desc varchar(4000) NULL, -- 설명
	mapp_seq numeric(3) NULL, -- 장애 맵핑 순서
	CONSTRAINT pk_plf_alarm_code PRIMARY KEY (alarm_type_cd, alarm_cd)
);
COMMENT ON TABLE wems.tb_plf_alarm_code IS '장애 유형별 코드와 대처방안을 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_alarm_code.alarm_type_cd IS '장애유형코드';
COMMENT ON COLUMN wems.tb_plf_alarm_code.alarm_cd IS '장애코드';
COMMENT ON COLUMN wems.tb_plf_alarm_code.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_plf_alarm_code.alarm_desc IS '설명';
COMMENT ON COLUMN wems.tb_plf_alarm_code.mapp_seq IS '장애 맵핑 순서';

-- Permissions

ALTER TABLE wems.tb_plf_alarm_code OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_alarm_code TO wems;

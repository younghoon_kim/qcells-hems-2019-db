-- Drop table

-- DROP TABLE wems.dc_cmd_session_ident;

CREATE TABLE wems.dc_cmd_session_ident (
	id varchar(80) NOT NULL,
	maxcmdqueue numeric(18) NULL,
	description varchar(200) NULL,
	priority numeric(2) NULL,
	logmode numeric(1) NOT NULL,
	ackmode numeric(1) NULL,
	max_session_cnt numeric(5) NULL
);

-- Permissions

ALTER TABLE wems.dc_cmd_session_ident OWNER TO wems;
GRANT ALL ON TABLE wems.dc_cmd_session_ident TO wems;

-- Drop table

-- DROP TABLE wems.tb_bas_oprtr_device;

CREATE TABLE wems.tb_bas_oprtr_device (
	user_id varchar(64) NOT NULL, -- 사용자ID (암호화)
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	create_id varchar(64) NOT NULL, -- 등록자ID (암호화)
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID (암호화)
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_bas_oprtr_device PRIMARY KEY (user_id, device_id, device_type_cd)
);
COMMENT ON TABLE wems.tb_bas_oprtr_device IS 'Operator 별 접근가능 장비 정보';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_oprtr_device.user_id IS '사용자ID (암호화)';
COMMENT ON COLUMN wems.tb_bas_oprtr_device.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_bas_oprtr_device.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_bas_oprtr_device.create_id IS '등록자ID (암호화)';
COMMENT ON COLUMN wems.tb_bas_oprtr_device.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_oprtr_device.update_id IS '수정자ID (암호화)';
COMMENT ON COLUMN wems.tb_bas_oprtr_device.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_bas_oprtr_device OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_oprtr_device TO wems;

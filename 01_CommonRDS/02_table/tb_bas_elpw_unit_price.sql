-- Drop table

-- DROP TABLE wems.tb_bas_elpw_unit_price;

CREATE TABLE wems.tb_bas_elpw_unit_price (
	elpw_prod_cd varchar(50) NOT NULL, -- 요금제코드
	unit_ym varchar(6) NOT NULL, -- 단위년월
	unit_hh varchar(2) NOT NULL, -- 단위시각
	io_flag varchar(1) NOT NULL, -- 매/수가 구분(O:수가, T:매가)
	unit_price numeric(10,4) NULL, -- 단가
	use_flag varchar(1) NULL, -- 사용여부
	create_id varchar(64) NOT NULL, -- 생성자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	wkday_flag varchar(1) NOT NULL DEFAULT '1'::character varying, -- 주중, 주말 구분(1: 주중, 0:주말)
	unit_mi varchar(2) NOT NULL DEFAULT '00'::character varying, -- 단위 분
	CONSTRAINT pk_bas_elpw_unit_price PRIMARY KEY (elpw_prod_cd, unit_ym, unit_hh, io_flag, wkday_flag)
);
COMMENT ON TABLE wems.tb_bas_elpw_unit_price IS '국가별 전력단가정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_elpw_unit_price.elpw_prod_cd IS '요금제코드';
COMMENT ON COLUMN wems.tb_bas_elpw_unit_price.unit_ym IS '단위년월';
COMMENT ON COLUMN wems.tb_bas_elpw_unit_price.unit_hh IS '단위시각';
COMMENT ON COLUMN wems.tb_bas_elpw_unit_price.io_flag IS '매/수가 구분(O:수가, T:매가)';
COMMENT ON COLUMN wems.tb_bas_elpw_unit_price.unit_price IS '단가';
COMMENT ON COLUMN wems.tb_bas_elpw_unit_price.use_flag IS '사용여부';
COMMENT ON COLUMN wems.tb_bas_elpw_unit_price.create_id IS '생성자ID';
COMMENT ON COLUMN wems.tb_bas_elpw_unit_price.create_dt IS '생성일시';
COMMENT ON COLUMN wems.tb_bas_elpw_unit_price.wkday_flag IS '주중, 주말 구분(1: 주중, 0:주말)';
COMMENT ON COLUMN wems.tb_bas_elpw_unit_price.unit_mi IS '단위 분';

-- Permissions

ALTER TABLE wems.tb_bas_elpw_unit_price OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_elpw_unit_price TO wems;

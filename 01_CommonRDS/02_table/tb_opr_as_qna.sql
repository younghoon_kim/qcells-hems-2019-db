-- Drop table

-- DROP TABLE wems.tb_opr_as_qna;

CREATE TABLE wems.tb_opr_as_qna (
	as_qna_seq float8 NOT NULL, -- 일련번호
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	as_qna_type_cd varchar(4) NOT NULL, -- AS QnA 유형코드
	subject varchar(4000) NOT NULL, -- 제목
	contents1 varchar(4000) NOT NULL, -- 내용1
	contents2 varchar(4000) NULL, -- 내용2
	repl_flag varchar(1) NOT NULL DEFAULT 'N'::character varying, -- 댓글 등록 여부
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_opr_as_qna PRIMARY KEY (as_qna_seq)
);
COMMENT ON TABLE wems.tb_opr_as_qna IS 'AS문의';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_as_qna.as_qna_seq IS '일련번호';
COMMENT ON COLUMN wems.tb_opr_as_qna.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_opr_as_qna.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_opr_as_qna.as_qna_type_cd IS 'AS QnA 유형코드';
COMMENT ON COLUMN wems.tb_opr_as_qna.subject IS '제목';
COMMENT ON COLUMN wems.tb_opr_as_qna.contents1 IS '내용1';
COMMENT ON COLUMN wems.tb_opr_as_qna.contents2 IS '내용2';
COMMENT ON COLUMN wems.tb_opr_as_qna.repl_flag IS '댓글 등록 여부';
COMMENT ON COLUMN wems.tb_opr_as_qna.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_opr_as_qna.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_opr_as_qna.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_opr_as_qna OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_as_qna TO wems;

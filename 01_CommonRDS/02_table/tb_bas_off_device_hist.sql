-- Drop table

-- DROP TABLE wems.tb_bas_off_device_hist;

CREATE TABLE wems.tb_bas_off_device_hist (
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	work_flag varchar(1) NULL, -- 처리여부
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_dt timestamptz NULL -- 수정일시
);
COMMENT ON TABLE wems.tb_bas_off_device_hist IS 'deleted device info history';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_off_device_hist.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_bas_off_device_hist.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_bas_off_device_hist.work_flag IS '처리여부';
COMMENT ON COLUMN wems.tb_bas_off_device_hist.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_off_device_hist.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_bas_off_device_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_off_device_hist TO wems;

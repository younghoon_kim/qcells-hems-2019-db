-- Drop table

-- DROP TABLE wems.tb_plf_menu;

CREATE TABLE wems.tb_plf_menu (
	menu_id varchar(15) NOT NULL, -- 메뉴ID
	prn_menu_id varchar(15) NOT NULL, -- 상위메뉴ID
	menu_nm varchar(30) NOT NULL, -- 메뉴명
	menu_type_cd varchar(4) NOT NULL, -- 메뉴유형코드
	menu_desc varchar(255) NULL, -- 메뉴설명
	menu_url varchar(255) NULL, -- 메뉴URL
	menu_param varchar(255) NULL, -- 메뉴파라메터
	use_flag varchar(1) NULL, -- 사용여부
	popup_flag varchar(1) NULL, -- 팝업여부
	ord_seq float8 NULL, -- 정렬순서
	CONSTRAINT pk_plf_menu PRIMARY KEY (menu_id)
);
COMMENT ON TABLE wems.tb_plf_menu IS '시스템 관리 및 운영에 필요한 메뉴 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_menu.menu_id IS '메뉴ID';
COMMENT ON COLUMN wems.tb_plf_menu.prn_menu_id IS '상위메뉴ID';
COMMENT ON COLUMN wems.tb_plf_menu.menu_nm IS '메뉴명';
COMMENT ON COLUMN wems.tb_plf_menu.menu_type_cd IS '메뉴유형코드';
COMMENT ON COLUMN wems.tb_plf_menu.menu_desc IS '메뉴설명';
COMMENT ON COLUMN wems.tb_plf_menu.menu_url IS '메뉴URL';
COMMENT ON COLUMN wems.tb_plf_menu.menu_param IS '메뉴파라메터';
COMMENT ON COLUMN wems.tb_plf_menu.use_flag IS '사용여부';
COMMENT ON COLUMN wems.tb_plf_menu.popup_flag IS '팝업여부';
COMMENT ON COLUMN wems.tb_plf_menu.ord_seq IS '정렬순서';

-- Permissions

ALTER TABLE wems.tb_plf_menu OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_menu TO wems;

-- Drop table

-- DROP TABLE wems.dc_cnf_connection;

CREATE TABLE wems.dc_cnf_connection (
	"sequence" numeric(8) NOT NULL,
	connectorid varchar(40) NOT NULL,
	agentequipid varchar(40) NULL,
	protocoltype numeric(8) NULL,
	status numeric(1) NULL,
	commandflag numeric(1) NULL,
	description varchar(500) NULL,
	"name" varchar(30) NULL,
	agentportno numeric(8) NULL,
	porttype numeric(8) NULL,
	userid varchar(40) NULL,
	"password" varchar(40) NULL
);

-- Permissions

ALTER TABLE wems.dc_cnf_connection OWNER TO wems;
GRANT ALL ON TABLE wems.dc_cnf_connection TO wems;

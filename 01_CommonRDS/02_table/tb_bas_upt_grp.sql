-- Drop table

-- DROP TABLE wems.tb_bas_upt_grp;

CREATE TABLE wems.tb_bas_upt_grp (
	grp_id varchar(50) NOT NULL, -- 그룹ID
	grp_nm varchar(200) NULL, -- 그룹명
	grp_desc varchar(255) NULL, -- 그룹상세
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	grp_upt_seq float8 NOT NULL DEFAULT 1, -- 변경 이력 순번
	CONSTRAINT pk_bas_upt_grp PRIMARY KEY (grp_id)
);
COMMENT ON TABLE wems.tb_bas_upt_grp IS '업데이트그룹정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_upt_grp.grp_id IS '그룹ID';
COMMENT ON COLUMN wems.tb_bas_upt_grp.grp_nm IS '그룹명';
COMMENT ON COLUMN wems.tb_bas_upt_grp.grp_desc IS '그룹상세';
COMMENT ON COLUMN wems.tb_bas_upt_grp.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_bas_upt_grp.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_upt_grp.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_bas_upt_grp.update_dt IS '수정일시';
COMMENT ON COLUMN wems.tb_bas_upt_grp.grp_upt_seq IS '변경 이력 순번';

-- Permissions

ALTER TABLE wems.tb_bas_upt_grp OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_upt_grp TO wems;

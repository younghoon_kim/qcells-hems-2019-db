-- Drop table
DROP TABLE IF EXISTS dc_cnf_evt;

-- Create table
create table dc_cnf_evt
(
	evt_seq bigserial not null
		constraint dc_cnf_evt_pk
			primary key,
	reg_date varchar(8) not null,
	evt_type_cd varchar(5) not null,
	evt_cd varchar(10) not null,
	worker_id varchar(80),
	device_id varchar(50),
	ref_msg varchar(256),
	noti_flag numeric(2) default 0 not null,
	noti_err_cd varchar(5),
	noti_err_msg varchar(256),
	noti_dt timestamp with time zone,
	reg_dt timestamp with time zone default sys_extract_utc(now()) not null
);

comment on table dc_cnf_evt is 'DAQ 이벤트';

comment on column dc_cnf_evt.evt_seq is 'DAQ 이벤트';

comment on column dc_cnf_evt.reg_date is '등록일자';

comment on column dc_cnf_evt.evt_type_cd is '이벤트 타입 코드';

comment on column dc_cnf_evt.evt_cd is '이벤트 코드: dc_code_bas.cd_grp=4000';

comment on column dc_cnf_evt.worker_id is 'worker 아이디';

comment on column dc_cnf_evt.device_id is '이벤트 참고 메시지';

comment on column dc_cnf_evt.noti_flag is '알림 플래그: dc_code_bas.cd_grp=4010';

comment on column dc_cnf_evt.noti_err_cd is '알림 에러 코드: dc_code_bas.cd_grp=4011';

comment on column dc_cnf_evt.noti_err_msg is '알림 에러 메시지 default: success';

--index생성
create index dc_cnf_evt_reg_date_evt_cd_evt_type_cd_noti_flag_index
	on dc_cnf_evt (reg_date, evt_cd, evt_type_cd, noti_flag);

-- sequence 생성
-- create sequence dc_cnf_evt_evt_seq_seq;

alter table dc_cnf_evt owner to wems;

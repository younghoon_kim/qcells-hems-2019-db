-- Drop table
-- DROP TABLE tb_opr_ess_config_mon_hist_v2;

CREATE TABLE tb_opr_ess_config_mon_hist_v2
(
    device_id                                       varchar(50)      NOT NULL, -- 장비ID
    device_type_cd                                  varchar(4)       NOT NULL, -- 장비유형코드
    pv_max_pwr1                                     double precision NULL,     -- 1번패널최대발전파워(W)
    pv_max_pwr2                                     double precision NULL,     -- 2번패널최대발전파워(W)
    feed_in_limit                                   varchar(4)       NULL,     -- 매전량(%)
    pem_mode                                        numeric(1)       NULL,     -- 3rd Party Control (0=Disable, 1=Enable)
    smeterd0id                                      numeric(1)       NULL,     -- Smart Meter D0 ID1 (0=None, 4=EM112, 5=EM24)
    max_inverter_pw_cd                              varchar(4)       NULL,     -- InverterLimit, 인버터최대출력(W)
    basicmode_cd                                    varchar(4),                -- 우선충전모드 0:Off, 1:On, NA(변경없음)
    backup_soc                                      numeric,
    hysteresis_low                                  numeric,
    hysteresis_high                                 numeric,
    --profile_id integer ,
    update_id                                       varchar(64)      NOT NULL, -- 생성자ID
    update_dt                                       timestamptz      NOT NULL, -- 생성일시
    base_profile_cd                                 integer,
    GC_over_voltage_level1                          numeric,                   -- (1.00 ~ 1.35)[Un]
    GC_over_voltage_time1                           numeric,                   -- (0 ~ 65535)[mesc]
    GC_over_voltage_level2                          numeric,                   -- (1.00 ~ 1.35)[Un]
    GC_over_voltage_time2                           numeric,                   -- (0 ~ 65535)[msec]
    GC_over_voltage_level3                          numeric,                   -- (1.00 ~ 1.35)[Un]
    GC_over_voltage_time3                           numeric,                   -- (0 ~ 65535)[msec]
    GC_over_voltage_level4                          numeric,                   -- (1.00 ~ 1.35)[Un]
    GC_over_voltage_time4                           numeric,                   -- (0 ~ 65535)[msec]
    GC_under_voltage_level1                         numeric,                   -- (0.01 ~ 1.00)[Un]
    GC_under_voltage_time1                          numeric,                   -- (0 ~ 65535)[msec]
    GC_under_voltage_level2                         numeric,                   -- (0.01 ~ 1.00)[Un]
    GC_under_voltage_time2                          numeric,                   -- (0 ~ 65535)[msec]
    GC_10min_voltage_in                             numeric,                   -- (1.00 ~ 1.35)[Un]
    GC_10min_voltage_out                            numeric,                   -- (1.00 ~ 1.35)[Un]
    GC_over_frequency_in                            numeric,                   -- (50.00 ~ 70.00)[Hz]
    GC_over_frequency_out                           numeric,                   -- (50.00 ~ 70.00)[Hz]
    GC_under_frequency_in                           numeric,                   -- (40.00 ~ 60.00)[Hz]
    GC_under_frequency_out                          numeric,                   -- (40.00 ~ 60.00)[Hz]
    GC_active_power_setpoint_enable                 numeric,                   -- Enable     Disable
    GC_active_power_setpoint_value                  numeric,                   --(* Range : 0 ~ 100)
    GC_active_power_frequency_enable                numeric,                   -- Enable     Disable
    GC_active_power_frequency_battery_frequency     numeric,                   -- Enable     Disable
    GC_active_power_frequency_SL                    numeric,                   --(* Range : 2 ~ 15)
    GC_active_power_frequency_X1                    numeric,                   --(* Range : 40.00 ~ 60.00)
    GC_active_power_frequency_X2                    numeric,                   --(* Range : 40.00 ~ 60.00)
    GC_active_power_frequency_SH                    numeric,                   --(* Range : 2 ~ 15)
    GC_active_power_frequency_X3                    numeric,                   --(* Range : 50.00 ~ 70.00)
    GC_active_power_frequency_X4                    numeric,                   --(* Range : 50.00 ~ 70.00)
    GC_active_power_voltage_enable                  numeric,                   -- Enable     Disable
    GC_active_power_voltage_battery_voltage         numeric,                   -- Enable     Disable
    GC_active_power_voltage_X1                      numeric,                   --(* Range : 0.80 ~ 1.25)
    GC_active_power_voltage_Y1                      numeric,                   --(* Range : 0.00 ~ 1.00)
    GC_active_power_voltage_X2                      numeric,                   --(* Range : 0.80 ~ 1.25)
    GC_active_power_voltage_Y2                      numeric,                   --(* Range : 0.00 ~ 1.00)
    GC_active_power_voltage_X3                      numeric,                   --(* Range : 1.00 ~ 1.25)
    GC_active_power_voltage_Y3                      numeric,                   --(* Range : 0.00 ~ 1.00)
    GC_active_power_voltage_X4                      numeric,                   --(* Range : 1.00 ~ 1.25)
    GC_active_power_voltage_Y4                      numeric,                   --(* Range : 0.00 ~ 1.00)
    GC_reactive_PT1_filter_Tau                      numeric,                   --(*Range : 6 ~ 60)
    GC_reactive_power_setpoint_enable               numeric,                   -- Enable     Disable
    GC_reactive_power_setpoint_excited              numeric,                   -- Over     Under
    GC_reactive_power_setpoint_value                numeric,                   --(*Range : 0.80 ~ 1.00)
    GC_reactive_power_cospi_enable                  numeric,                   -- Enable     Disable
    GC_reactive_power_cospi_excited                 numeric,                   -- Over     Under
    GC_reactive_power_cospi_X1                      numeric,                   --(* Range : 0.00 ~ 1.00)
    GC_reactive_power_cospi_Y1                      numeric,                   --(* Range : 0.80 ~ 1.00)
    GC_reactive_power_cospi_X2                      numeric,                   --(* Range : 0.00 ~ 1.00)
    GC_reactive_power_cospi_Y2                      numeric,                   --(* Range : 0.80 ~ 1.00)
    GC_reactive_power_cospi_X3                      numeric,                   --(* Range : 0.00 ~ 1.00)
    GC_reactive_power_cospi_Y3                      numeric,                   --(* Range : 0.80 ~ 1.00)
    GC_reactive_power_Q_setpoint_enable             numeric,                   -- Enable     Disable
    GC_reactive_power_Q_setpoint_excited            numeric,                   -- Over     Under
    GC_reactive_power_Q_setpoint_value              numeric,                   --(* Range : 0 ~ 0.600)
    GC_reactive_power_Q_U_enable                    numeric,                   -- Enable     Disable
    GC_reactive_power_Q_U_X1                        numeric,                   --(* Range : 0 ~ 1.9999)
    GC_reactive_power_Q_U_Y1                        numeric,                   --(* Range : -60.00 ~ 60.00)
    GC_reactive_power_Q_U_X2                        numeric,                   --(* Range : 0 ~ 1.9999)
    GC_reactive_power_Q_U_Y2                        numeric,                   --(* Range : -60.00 ~ 60.00)
    GC_reactive_power_Q_U_X3                        numeric,                   --(* Range : 0 ~ 1.9999)
    GC_reactive_power_Q_U_Y3                        numeric,                   --(* Range : -60.00 ~ 60.00)
    GC_reactive_power_Q_U_X4                        numeric,                   --(* Range : 0 ~ 1.9999)
    GC_reactive_power_Q_U_Y4                        numeric,                   --(* Range : -60.00 ~ 60.00)
    GC_DC_injection_control_enable                  numeric,                   -- (0=Diable,1=Enable)
    GC_DC_injection_control_faultlevel1             numeric,                   -- (0.00 ~ 1.00)[A]
    GC_DC_injection_control_faulttime1              numeric,                   -- (0.000 ~ 2.00)[sec]
    GC_DC_injection_control_faultlevel2             numeric,                   -- (0.00 ~ 1.00)[A]
    GC_DC_injection_control_faulttime2              numeric,                   -- (0.00 ~ 2.0)[sec]

    GC_RCMU_control_sudden_enable                   numeric,                   -- (0=Diable,1=Enable)
    GC_RCMU_control_sudden_level1                   numeric,                   -- (0.000 ~ 0.500)[A]
    GC_RCMU_control_sudden_time1                    numeric,                   -- (0.000 ~ 0.500)[sec]
    GC_RCMU_control_sudden_level2                   numeric,                   -- (0.000 ~ 0.500)[A]
    GC_RCMU_control_sudden_time2                    numeric,                   -- (0.000 ~ 0.500)[sec]
    GC_RCMU_control_sudden_level3                   numeric,                   -- (0.000 ~ 0.500)[A]
    GC_RCMU_control_sudden_time3                    numeric,                   -- (0.000 ~ 0.500)[sec]

    GC_RCMU_control_continuous_enable               numeric,                   -- (0=Diable,1=Enable)
    GC_RCMU_control_continuous_level1               numeric,                   -- (0.000 ~ 0.500)[A]
    GC_RCMU_control_continuous_time1                numeric,                   -- (0.000 ~ 0.500)[sec]
    GC_PV_insulation_control_enable                 numeric,                   -- (0=Diable,1=Enable)
    GC_PV_insulation_control_faultlevel             numeric,                   -- (0 ~ 50000)[R]
    GC_PV_insulation_control_checkcnt               numeric,                   -- (1 ~ 100)[Cnt]
    GC_antiislanding_control_enable                 numeric,                   -- (0=Diable,1=Enable)
    GC_antiislanding_control_time                   numeric,                   -- (0 ~ 10000)[msec]
    GC_gradient_control_enable                      numeric,                   -- (0=Diable,1=Enable)
    GC_gradient_control_energy_source_change_enable numeric,                   -- Enable     Disable
    GC_gradient_control_timelevel                   numeric,                   -- (60 ~ 1200)[sec]
    GC_derating_control_enable                      numeric,                   -- (0=Diable,1=Enable)
    GC_derating_control_start_temprature            numeric,                   -- (0 ~ 100)[C]
    GC_derating_control_value                       numeric,                   -- (0.00 ~ 0.60)[%]
    GC_battery_option_frequency                      numeric,                   -- Enable     Disable   
    GC_battery_option_voltage                       numeric,                   -- Enable     Disable
    GC_battery_option_derating                      numeric                    -- Enable     Disable
);
COMMENT ON TABLE tb_opr_ess_config_mon_hist_v2 IS 'ESS 의 설정 정보 모니터링 이력 테이블';

-- Column comments
COMMENT ON COLUMN tb_opr_ess_config_mon_hist_v2.device_id IS '장비ID';
COMMENT ON COLUMN tb_opr_ess_config_mon_hist_v2.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN tb_opr_ess_config_mon_hist_v2.pv_max_pwr1 IS '1번패널최대발전파워(W)';
COMMENT ON COLUMN tb_opr_ess_config_mon_hist_v2.pv_max_pwr2 IS '2번패널최대발전파워(W)';
COMMENT ON COLUMN tb_opr_ess_config_mon_hist_v2.feed_in_limit IS '매전량(%)';
COMMENT ON COLUMN tb_opr_ess_config_mon_hist_v2.max_inverter_pw_cd IS 'InverterLimit, 인버터최대출력(W)';
COMMENT ON COLUMN tb_opr_ess_config_mon_hist_v2.basicmode_cd IS '우선충전모드 0:Off, 1:On, NA(변경없음)';
COMMENT ON COLUMN tb_opr_ess_config_mon_hist_v2.update_id IS '생성자ID';
COMMENT ON COLUMN tb_opr_ess_config_mon_hist_v2.update_dt IS '생성일시';
COMMENT ON COLUMN tb_opr_ess_config_mon_hist_v2.pem_mode IS '3rd Party Control (0=Disable, 1=Enable)';
COMMENT ON COLUMN tb_opr_ess_config_mon_hist_v2.smeterd0id IS 'Smart Meter D0 ID1 (0=None, 4=EM112, 5=EM24)';
COMMENT ON COLUMN tb_opr_ess_config_mon_hist_v2.base_profile_cd IS 'base가 되는 profile의 country_code';


-- Permissions

ALTER TABLE tb_opr_ess_config_mon_hist_v2
    OWNER TO wems;
GRANT ALL ON TABLE tb_opr_ess_config_mon_hist_v2 TO wems;

-- Drop table

-- DROP TABLE wems.tb_bas_city_not_in_tb_bas_device;

CREATE TABLE wems.tb_bas_city_not_in_tb_bas_device (
	city_cd varchar(50) NOT NULL,
	latitude numeric(12,6) NULL,
	longitude numeric(12,6) NULL,
	utc_offset numeric(2) NULL,
	city_nm varchar(50) NULL,
	cntry_cd varchar(4) NULL,
	cntry_nm varchar(50) NULL,
	colec_flg varchar(1) NULL,
	pop_cnt numeric(18) NULL,
	create_dt timestamptz NOT NULL,
	timezone_id varchar(100) NULL
);

-- Permissions

ALTER TABLE wems.tb_bas_city_not_in_tb_bas_device OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_city_not_in_tb_bas_device TO wems;

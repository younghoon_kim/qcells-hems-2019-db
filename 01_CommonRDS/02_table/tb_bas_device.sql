-- Drop table

-- DROP TABLE wems.tb_bas_device;

CREATE TABLE wems.tb_bas_device (
	device_id varchar(128) NOT NULL, -- 장비ID
	device_type_cd varchar(32) NOT NULL, -- 장비유형코드
	prn_device_id varchar(128) NULL, -- 상위장비ID
	mkr_id varchar(32) NULL, -- 제조사ID
	device_nm varchar(64) NULL, -- 장비명
	modl_nm varchar(64) NULL, -- 모델명
	made_dt varchar(32) NULL, -- 제조일
	standard varchar(64) NULL, -- 규격
	capacity varchar(64) NOT NULL, -- 용량
	instl_floor numeric(4) NULL, -- 설치층
	instl_place varchar(512) NULL, -- 설치장소
	instl_dt varchar(14) NULL, -- 설치일자
	pctr_file varchar(4000) NULL, -- 사진파일
	draw_file varchar(512) NULL, -- 도면파일
	ip_addr varchar(32) NULL, -- IP주소
	unit_id float8 NULL, -- 유닛ID
	port_no numeric(18) NULL, -- 포트번호
	product_model_nm varchar(64) NULL, -- 제품모델명
	ems_model_nm varchar(64) NULL, -- EMS모델명
	pcs_model_nm varchar(64) NULL, -- PCS모델명
	bms_model_nm varchar(64) NULL, -- BMS모델명
	ems_ver varchar(64) NULL, -- EMSVersion
	pcs_ver varchar(64) NULL, -- PCSVersion
	bms_ver varchar(64) NULL, -- BMSVersion
	com_prot varchar(32) NULL, -- 프로토콜
	remark varchar(4000) NULL, -- 비고
	owner_nm varchar(160) NULL, -- 소유자 명 (암호화)
	owner_addr varchar(4000) NULL, -- 소유자 주소 (암호화)
	owner_tel_no varchar(96) NULL, -- 소유자 전화번호 (암호화)
	create_id varchar(160) NOT NULL, -- 등록자ID (암호화)
	create_dt timestamptz NOT NULL, -- 등록일시
	update_id varchar(160) NULL, -- 수정자ID (암호화)
	update_dt timestamptz NULL, -- 수정일시
	owner_email varchar(192) NULL, -- 소유자 이메일주소 (암호화)
	instl_user_id varchar(64) NULL, -- 설치자 ID (암호화)
	instl_addr varchar(4000) NULL, -- 설치주소 (암호화)
	cntry_cd varchar(4) NULL, -- 국가코드
	city_cd varchar(50) NULL, -- 도시코드
	elpw_prod_cd varchar(50) NULL,
	wrty_dt varchar(8) NULL, -- Warranty 만료일자
	ems_ip_addr varchar(15) NULL, -- 설치제품 IP주소
	latitude float8 NULL, -- 설치 위치 위도
	longitude float8 NULL, -- 설치 위치 경도
	agree_sign text NULL,
	oper_test_dt timestamptz NULL,
	pctr_file_path varchar(4000) NULL,
	pctr_file_disp_name varchar(4000) NULL,
	pin_code varchar(4) NULL, -- 사용자 가입 시 필요
	timezone_id varchar(100) NULL, -- 타임존 ID
	ems_update_dt varchar(14) NULL,
	serial_no varchar(21) NULL, -- 시리얼번호(DEVICE_ID가 21자리라 18자리 처리를 위해 사용)
	battery1 varchar(128) NULL, -- 배터리1 시리얼번호
	battery2 varchar(128) NULL, -- 배터리2 시리얼번호
	battery3 varchar(128) NULL, -- 배터리3 시리얼번호
	battery4 varchar(128) NULL, -- 배터리4 시리얼번호
	battery5 varchar(128) NULL, -- 배터리5 시리얼번호
	upgrade_state varchar(5) NULL, -- 원격업그레이드상태
	upgrade_dt timestamptz NULL, -- 원격업그레이드수신시간
	CONSTRAINT pk_bas_device PRIMARY KEY (device_id, device_type_cd)
);

-- Column comments

COMMENT ON COLUMN wems.tb_bas_device.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_bas_device.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_bas_device.prn_device_id IS '상위장비ID';
COMMENT ON COLUMN wems.tb_bas_device.mkr_id IS '제조사ID';
COMMENT ON COLUMN wems.tb_bas_device.device_nm IS '장비명';
COMMENT ON COLUMN wems.tb_bas_device.modl_nm IS '모델명';
COMMENT ON COLUMN wems.tb_bas_device.made_dt IS '제조일';
COMMENT ON COLUMN wems.tb_bas_device.standard IS '규격';
COMMENT ON COLUMN wems.tb_bas_device.capacity IS '용량';
COMMENT ON COLUMN wems.tb_bas_device.instl_floor IS '설치층';
COMMENT ON COLUMN wems.tb_bas_device.instl_place IS '설치장소';
COMMENT ON COLUMN wems.tb_bas_device.instl_dt IS '설치일자';
COMMENT ON COLUMN wems.tb_bas_device.pctr_file IS '사진파일';
COMMENT ON COLUMN wems.tb_bas_device.draw_file IS '도면파일';
COMMENT ON COLUMN wems.tb_bas_device.ip_addr IS 'IP주소';
COMMENT ON COLUMN wems.tb_bas_device.unit_id IS '유닛ID';
COMMENT ON COLUMN wems.tb_bas_device.port_no IS '포트번호';
COMMENT ON COLUMN wems.tb_bas_device.product_model_nm IS '제품모델명';
COMMENT ON COLUMN wems.tb_bas_device.ems_model_nm IS 'EMS모델명';
COMMENT ON COLUMN wems.tb_bas_device.pcs_model_nm IS 'PCS모델명';
COMMENT ON COLUMN wems.tb_bas_device.bms_model_nm IS 'BMS모델명';
COMMENT ON COLUMN wems.tb_bas_device.ems_ver IS 'EMSVersion';
COMMENT ON COLUMN wems.tb_bas_device.pcs_ver IS 'PCSVersion';
COMMENT ON COLUMN wems.tb_bas_device.bms_ver IS 'BMSVersion';
COMMENT ON COLUMN wems.tb_bas_device.com_prot IS '프로토콜';
COMMENT ON COLUMN wems.tb_bas_device.remark IS '비고';
COMMENT ON COLUMN wems.tb_bas_device.owner_nm IS '소유자 명 (암호화)';
COMMENT ON COLUMN wems.tb_bas_device.owner_addr IS '소유자 주소 (암호화)';
COMMENT ON COLUMN wems.tb_bas_device.owner_tel_no IS '소유자 전화번호 (암호화)';
COMMENT ON COLUMN wems.tb_bas_device.create_id IS '등록자ID (암호화)';
COMMENT ON COLUMN wems.tb_bas_device.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_device.update_id IS '수정자ID (암호화)';
COMMENT ON COLUMN wems.tb_bas_device.update_dt IS '수정일시';
COMMENT ON COLUMN wems.tb_bas_device.owner_email IS '소유자 이메일주소 (암호화)';
COMMENT ON COLUMN wems.tb_bas_device.instl_user_id IS '설치자 ID (암호화)';
COMMENT ON COLUMN wems.tb_bas_device.instl_addr IS '설치주소 (암호화)';
COMMENT ON COLUMN wems.tb_bas_device.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_bas_device.city_cd IS '도시코드';
COMMENT ON COLUMN wems.tb_bas_device.wrty_dt IS 'Warranty 만료일자';
COMMENT ON COLUMN wems.tb_bas_device.ems_ip_addr IS '설치제품 IP주소';
COMMENT ON COLUMN wems.tb_bas_device.latitude IS '설치 위치 위도';
COMMENT ON COLUMN wems.tb_bas_device.longitude IS '설치 위치 경도';
COMMENT ON COLUMN wems.tb_bas_device.pin_code IS '사용자 가입 시 필요';
COMMENT ON COLUMN wems.tb_bas_device.timezone_id IS '타임존 ID';
COMMENT ON COLUMN wems.tb_bas_device.serial_no IS '시리얼번호(DEVICE_ID가 21자리라 18자리 처리를 위해 사용)';
COMMENT ON COLUMN wems.tb_bas_device.battery1 IS '배터리1 시리얼번호';
COMMENT ON COLUMN wems.tb_bas_device.battery2 IS '배터리2 시리얼번호';
COMMENT ON COLUMN wems.tb_bas_device.battery3 IS '배터리3 시리얼번호';
COMMENT ON COLUMN wems.tb_bas_device.battery4 IS '배터리4 시리얼번호';
COMMENT ON COLUMN wems.tb_bas_device.battery5 IS '배터리5 시리얼번호';
COMMENT ON COLUMN wems.tb_bas_device.upgrade_state IS '원격업그레이드상태';
COMMENT ON COLUMN wems.tb_bas_device.upgrade_dt IS '원격업그레이드수신시간';

-- Table Triggers

-- DROP TRIGGER tgr_act_instl ON wems.tb_bas_device;

create trigger tgr_act_instl after
insert
    on
    wems.tb_bas_device for each row execute procedure wems."tgr_act_instl$tb_bas_device"();
-- DROP TRIGGER tgr_device_warranty ON wems.tb_bas_device;

create trigger tgr_device_warranty before
insert
    on
    wems.tb_bas_device for each row execute procedure wems."tgr_device_warranty$tb_bas_device"();

-- Permissions

ALTER TABLE wems.tb_bas_device OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_device TO wems;

-- Drop table

-- DROP TABLE wems.tb_plf_code_grp;

CREATE TABLE wems.tb_plf_code_grp (
	grp_cd varchar(30) NOT NULL, -- 그룹코드
	grp_nm varchar(50) NOT NULL, -- 그룹명
	remark varchar(255) NULL, -- 비고
	table_nm varchar(30) NULL, -- 테이블명
	CONSTRAINT pk_plf_code_grp PRIMARY KEY (grp_cd)
);
COMMENT ON TABLE wems.tb_plf_code_grp IS '시스템 관리 및 운영에 필요한 코드 그룹 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_code_grp.grp_cd IS '그룹코드';
COMMENT ON COLUMN wems.tb_plf_code_grp.grp_nm IS '그룹명';
COMMENT ON COLUMN wems.tb_plf_code_grp.remark IS '비고';
COMMENT ON COLUMN wems.tb_plf_code_grp.table_nm IS '테이블명';

-- Permissions

ALTER TABLE wems.tb_plf_code_grp OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_code_grp TO wems;


-- Drop table

-- DROP TABLE wems.tb_raw_ess_info_m2m;

CREATE TABLE wems.tb_raw_ess_info_m2m (
	device_type_cd varchar(4) NULL,
	device_id varchar(50) NULL,
	colec_dt varchar(14) NULL,
	"delimiter" varchar(1) NULL,
	ems_ver varchar(30) NULL,
	pcs_ver varchar(30) NULL,
	rack_ver varchar(30) NULL,
	rackhist_ver varchar(30) NULL,
	tray_ver varchar(30) NULL
);

-- Permissions

ALTER TABLE wems.tb_raw_ess_info_m2m OWNER TO wems;
GRANT ALL ON TABLE wems.tb_raw_ess_info_m2m TO wems;
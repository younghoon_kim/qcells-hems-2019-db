-- Drop table

-- DROP TABLE wems.tb_raw_weather_hist;

CREATE TABLE wems.tb_raw_weather_hist (
	city_cd varchar(20) NOT NULL, -- 도시코드
	latitude varchar(13) NOT NULL, -- 위도
	longitude varchar(13) NOT NULL, -- 경도
	location_nm varchar(50) NULL, -- 지역명
	location_type varchar(50) NULL, -- 지역유형
	country varchar(50) NULL, -- 국가명
	timezone varchar(10) NULL, -- Timezone
	time_to varchar(24) NOT NULL, -- 예보시작일시
	time_from varchar(24) NOT NULL, -- 예보종료일시
	day_type varchar(1) NOT NULL, -- 요일구분
	temperature varchar(50) NULL, -- 온도
	temperature_max varchar(50) NULL, -- 최고온도
	temperature_min varchar(50) NULL, -- 최저온도
	symbol_var varchar(50) NULL, -- 날씨코드
	symbol_cd varchar(50) NULL, -- 날씨요약코드
	symbol_name varchar(50) NULL, -- 날씨명
	symbol_num varchar(50) NULL, -- 날씨수치
	clouds_all varchar(50) NULL, -- 구름량
	clouds_value varchar(50) NULL, -- 구름상태설명
	precipication_prob varchar(50) NULL, -- 강수확률
	precipication varchar(50) NULL, -- 강수량
	wind_speed_mps varchar(50) NULL, -- 풍속
	wind_speed_name varchar(50) NULL, -- 풍속명
	wind_direction_cd varchar(50) NULL, -- 풍향
	wind_direction_deg varchar(50) NULL, -- 풍향각
	wind_direction_name varchar(50) NULL, -- 풍향명
	humidity varchar(50) NULL, -- 습도
	pressure varchar(50) NULL, -- 기압
	sunrise_dt varchar(50) NULL, -- 일출시간
	sunset_dt varchar(50) NULL, -- 일몰시간
	sunrise_idx varchar(4) NULL, -- 일출시간 15분 단위 인덱스
	sunset_idx varchar(4) NULL, -- 일몰시간 15분 단위 인덱스
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()),
	CONSTRAINT pk_raw_weather_hist PRIMARY KEY (city_cd, time_from, time_to)
)
PARTITION BY RANGE (time_from);
COMMENT ON TABLE wems.tb_raw_weather_hist IS '3시간 단위 날씨예보 수집 데이터 이력을 보관 한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_raw_weather_hist.city_cd IS '도시코드';
COMMENT ON COLUMN wems.tb_raw_weather_hist.latitude IS '위도';
COMMENT ON COLUMN wems.tb_raw_weather_hist.longitude IS '경도';
COMMENT ON COLUMN wems.tb_raw_weather_hist.location_nm IS '지역명';
COMMENT ON COLUMN wems.tb_raw_weather_hist.location_type IS '지역유형';
COMMENT ON COLUMN wems.tb_raw_weather_hist.country IS '국가명';
COMMENT ON COLUMN wems.tb_raw_weather_hist.timezone IS 'Timezone';
COMMENT ON COLUMN wems.tb_raw_weather_hist.time_to IS '예보시작일시';
COMMENT ON COLUMN wems.tb_raw_weather_hist.time_from IS '예보종료일시';
COMMENT ON COLUMN wems.tb_raw_weather_hist.day_type IS '요일구분';
COMMENT ON COLUMN wems.tb_raw_weather_hist.temperature IS '온도';
COMMENT ON COLUMN wems.tb_raw_weather_hist.temperature_max IS '최고온도';
COMMENT ON COLUMN wems.tb_raw_weather_hist.temperature_min IS '최저온도';
COMMENT ON COLUMN wems.tb_raw_weather_hist.symbol_var IS '날씨코드';
COMMENT ON COLUMN wems.tb_raw_weather_hist.symbol_cd IS '날씨요약코드';
COMMENT ON COLUMN wems.tb_raw_weather_hist.symbol_name IS '날씨명';
COMMENT ON COLUMN wems.tb_raw_weather_hist.symbol_num IS '날씨수치';
COMMENT ON COLUMN wems.tb_raw_weather_hist.clouds_all IS '구름량';
COMMENT ON COLUMN wems.tb_raw_weather_hist.clouds_value IS '구름상태설명';
COMMENT ON COLUMN wems.tb_raw_weather_hist.precipication_prob IS '강수확률';
COMMENT ON COLUMN wems.tb_raw_weather_hist.precipication IS '강수량';
COMMENT ON COLUMN wems.tb_raw_weather_hist.wind_speed_mps IS '풍속';
COMMENT ON COLUMN wems.tb_raw_weather_hist.wind_speed_name IS '풍속명';
COMMENT ON COLUMN wems.tb_raw_weather_hist.wind_direction_cd IS '풍향';
COMMENT ON COLUMN wems.tb_raw_weather_hist.wind_direction_deg IS '풍향각';
COMMENT ON COLUMN wems.tb_raw_weather_hist.wind_direction_name IS '풍향명';
COMMENT ON COLUMN wems.tb_raw_weather_hist.humidity IS '습도';
COMMENT ON COLUMN wems.tb_raw_weather_hist.pressure IS '기압';
COMMENT ON COLUMN wems.tb_raw_weather_hist.sunrise_dt IS '일출시간';
COMMENT ON COLUMN wems.tb_raw_weather_hist.sunset_dt IS '일몰시간';
COMMENT ON COLUMN wems.tb_raw_weather_hist.sunrise_idx IS '일출시간 15분 단위 인덱스';
COMMENT ON COLUMN wems.tb_raw_weather_hist.sunset_idx IS '일몰시간 15분 단위 인덱스';

-- Permissions

ALTER TABLE wems.tb_raw_weather_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_raw_weather_hist TO wems;

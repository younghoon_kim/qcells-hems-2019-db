-- Drop table

-- DROP TABLE wems.batch_inf;

CREATE TABLE wems.batch_inf (
	sys_nm varchar(10) NOT NULL, -- 시스템명¶'WEMS'¶
	sp_nm varchar(100) NOT NULL, -- 프로시져명
	cycl_cd varchar(1) NOT NULL, -- 주기코드
	cycl_time varchar(2) NOT NULL, -- 주기시간
	exe_scdul varchar(4) NULL, -- 실행스케쥴¶cycl_cd 값에 따라 ¶'D':실행시간('HH')¶'M':실행일자 ('DD')¶'Y':실행월일('MMDD')
	ord numeric(5) NULL,
	use_yn varchar(1) NOT NULL DEFAULT 'Y'::character varying, -- 사용여부
	sp_desc varchar(100) NULL, -- 프로시져설명
	cretr varchar(20) NULL, -- 생성자
	cret_dt timestamp NOT NULL DEFAULT now(), -- 생성일시
	CONSTRAINT batch_inf_pk PRIMARY KEY (sys_nm, sp_nm)
);
COMMENT ON TABLE wems.batch_inf IS '배치기본';

-- Column comments

COMMENT ON COLUMN wems.batch_inf.sys_nm IS '시스템명
''WEMS''
';
COMMENT ON COLUMN wems.batch_inf.sp_nm IS '프로시져명';
COMMENT ON COLUMN wems.batch_inf.cycl_cd IS '주기코드';
COMMENT ON COLUMN wems.batch_inf.cycl_time IS '주기시간';
COMMENT ON COLUMN wems.batch_inf.exe_scdul IS '실행스케쥴
cycl_cd 값에 따라 
''D'':실행시간(''HH'')
''M'':실행일자 (''DD'')
''Y'':실행월일(''MMDD'')';
COMMENT ON COLUMN wems.batch_inf.use_yn IS '사용여부';
COMMENT ON COLUMN wems.batch_inf.sp_desc IS '프로시져설명';
COMMENT ON COLUMN wems.batch_inf.cretr IS '생성자';
COMMENT ON COLUMN wems.batch_inf.cret_dt IS '생성일시';

-- Permissions

ALTER TABLE wems.batch_inf OWNER TO wems;
GRANT ALL ON TABLE wems.batch_inf TO wems;

-- Drop table

-- DROP TABLE wems.tb_opr_was_state;

CREATE TABLE wems.tb_opr_was_state (
	id varchar(80) NOT NULL,
	service_name varchar(100) NOT NULL,
	ipaddr varchar(18) NOT NULL,
	port varchar(6) NOT NULL,
	url varchar(100) NOT NULL,
	status varchar(10) NOT NULL,
	modify_date timestamptz NULL,
	create_date timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now())
);

-- Permissions

ALTER TABLE wems.tb_opr_was_state OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_was_state TO wems;

-- Drop table

-- DROP TABLE wems.tb_opr_oper_ctrl;

CREATE TABLE wems.tb_opr_oper_ctrl (
	grp_id varchar(50) NOT NULL, -- 제어그룹 ID
	ems_oper_mode_cd varchar(4) NULL, -- EMS운용모드
	era_mode_cd varchar(4) NULL, -- 원격자동모드
	erm_mode_1_cd varchar(4) NULL, -- 원격수동모드1
	erm_mode_2_cd varchar(4) NULL, -- 원격수동모드2
	erm_mode_3_cd varchar(4) NULL, -- 원격수동모드3
	feedin float8 NULL, -- FeedIn
	target_soc float8 NULL, -- 목표SOC
	target_pw float8 NULL, -- 목표입출력PW
	algo_flag float8 NULL, -- ALGO_FLAG
	acpsetpoint float8 NULL, -- ACPSetPoint
	acppemax float8 NULL, -- ACPPeMax
	rcpsetcospi float8 NULL, -- RCPSetCospi
	dcivariable float8 NULL, -- DCIVariable
	dcitriplevel float8 NULL, -- DCITripLevel
	rcpsetx1 float8 NULL, -- RCPSetX1
	rcpsety1 float8 NULL, -- RCPSetY1
	rcpsetx2 float8 NULL, -- RCPSetX2
	rcpsety2 float8 NULL, -- RCPSetY2
	rcpsetx3 float8 NULL, -- RCPSetX3
	rcpsety3 float8 NULL, -- RCPSetY3
	rcpsetx4 float8 NULL, -- RCPSetX4
	rcpsety4 float8 NULL, -- RCPSetY4
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	inverter_pw float8 NULL,
	basicmode_cd float8 NULL DEFAULT 0,
	reversesshport float8 NULL, -- ReverseSSH용 포트번호
	apcdrop numeric(6,1) NULL, -- APCDROP
	lockin numeric(6,1) NULL, -- LOCKIN
	lockout numeric(6,1) NULL, -- LOCKOUT
	batteryapc numeric(6,1) NULL, -- BATTERYAPC
	algo_flag2 float8 NULL,
	rcpqfixdata float8 NULL,
	CONSTRAINT pk_opr_oper_ctrl PRIMARY KEY (grp_id)
);
COMMENT ON TABLE wems.tb_opr_oper_ctrl IS 'ESS 제어요청정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_oper_ctrl.grp_id IS '제어그룹 ID';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.ems_oper_mode_cd IS 'EMS운용모드';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.era_mode_cd IS '원격자동모드';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.erm_mode_1_cd IS '원격수동모드1';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.erm_mode_2_cd IS '원격수동모드2';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.erm_mode_3_cd IS '원격수동모드3';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.feedin IS 'FeedIn';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.target_soc IS '목표SOC';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.target_pw IS '목표입출력PW';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.algo_flag IS 'ALGO_FLAG';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.acpsetpoint IS 'ACPSetPoint';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.acppemax IS 'ACPPeMax';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.rcpsetcospi IS 'RCPSetCospi';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.dcivariable IS 'DCIVariable';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.dcitriplevel IS 'DCITripLevel';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.rcpsetx1 IS 'RCPSetX1';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.rcpsety1 IS 'RCPSetY1';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.rcpsetx2 IS 'RCPSetX2';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.rcpsety2 IS 'RCPSetY2';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.rcpsetx3 IS 'RCPSetX3';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.rcpsety3 IS 'RCPSetY3';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.rcpsetx4 IS 'RCPSetX4';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.rcpsety4 IS 'RCPSetY4';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.update_dt IS '수정일시';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.reversesshport IS 'ReverseSSH용 포트번호';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.apcdrop IS 'APCDROP';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.lockin IS 'LOCKIN';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.lockout IS 'LOCKOUT';
COMMENT ON COLUMN wems.tb_opr_oper_ctrl.batteryapc IS 'BATTERYAPC';

-- Permissions

ALTER TABLE wems.tb_opr_oper_ctrl OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_oper_ctrl TO wems;

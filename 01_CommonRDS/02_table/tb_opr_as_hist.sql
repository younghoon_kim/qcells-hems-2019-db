-- Drop table

-- DROP TABLE wems.tb_opr_as_hist;

CREATE TABLE wems.tb_opr_as_hist (
	as_hist_seq numeric(18) NOT NULL, -- 유지보수이력일련번호
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	work_type_cd varchar(4) NOT NULL, -- 작업유형코드
	alarm_event_dt varchar(12) NULL,
	as_start_dt varchar(12) NULL, -- 작업시작일시
	as_end_dt varchar(12) NULL, -- 작업종료일시
	engr_nm varchar(30) NULL, -- 작업자 명
	engr_tel_no varchar(30) NULL, -- 작업자 전화번호
	as_smry varchar(255) NULL, -- 보수내역 요약
	as_desc varchar(4000) NULL, -- 보수내역 상세
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_id varchar(64) NULL, -- 수정자ID
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_opr_as_hist PRIMARY KEY (as_hist_seq)
);
COMMENT ON TABLE wems.tb_opr_as_hist IS '장비의 유지보수이력 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_as_hist.as_hist_seq IS '유지보수이력일련번호';
COMMENT ON COLUMN wems.tb_opr_as_hist.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_opr_as_hist.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_opr_as_hist.work_type_cd IS '작업유형코드';
COMMENT ON COLUMN wems.tb_opr_as_hist.as_start_dt IS '작업시작일시';
COMMENT ON COLUMN wems.tb_opr_as_hist.as_end_dt IS '작업종료일시';
COMMENT ON COLUMN wems.tb_opr_as_hist.engr_nm IS '작업자 명';
COMMENT ON COLUMN wems.tb_opr_as_hist.engr_tel_no IS '작업자 전화번호';
COMMENT ON COLUMN wems.tb_opr_as_hist.as_smry IS '보수내역 요약';
COMMENT ON COLUMN wems.tb_opr_as_hist.as_desc IS '보수내역 상세';
COMMENT ON COLUMN wems.tb_opr_as_hist.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_opr_as_hist.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_opr_as_hist.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_opr_as_hist.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_opr_as_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_as_hist TO wems;

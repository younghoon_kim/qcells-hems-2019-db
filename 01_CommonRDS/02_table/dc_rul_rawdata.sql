-- Drop table

-- DROP TABLE wems.dc_rul_rawdata;

CREATE TABLE wems.dc_rul_rawdata (
	ruleid varchar(80) NOT NULL,
	id varchar(80) NOT NULL,
	rawdata text NULL
);

-- Permissions

ALTER TABLE wems.dc_rul_rawdata OWNER TO wems;
GRANT ALL ON TABLE wems.dc_rul_rawdata TO wems;

-- Drop table

-- DROP TABLE wems.tb_plf_carb_emi_quity;

CREATE TABLE wems.tb_plf_carb_emi_quity (
	cntry_cd varchar(4) NOT NULL, -- 국가코드
	apply_year varchar(4) NOT NULL, -- 적용년도
	carb_emi_quity numeric(5,2) NOT NULL, -- 탄소배출량 계수
	use_flag varchar(1) NOT NULL, -- 사용여부
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	CONSTRAINT pk_plf_carb_emi_quity PRIMARY KEY (cntry_cd, apply_year, use_flag)
);
COMMENT ON TABLE wems.tb_plf_carb_emi_quity IS '국가별 탄소배출량 계수 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_carb_emi_quity.cntry_cd IS '국가코드';
COMMENT ON COLUMN wems.tb_plf_carb_emi_quity.apply_year IS '적용년도';
COMMENT ON COLUMN wems.tb_plf_carb_emi_quity.carb_emi_quity IS '탄소배출량 계수';
COMMENT ON COLUMN wems.tb_plf_carb_emi_quity.use_flag IS '사용여부';
COMMENT ON COLUMN wems.tb_plf_carb_emi_quity.create_dt IS '등록일시';

-- Permissions

ALTER TABLE wems.tb_plf_carb_emi_quity OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_carb_emi_quity TO wems;

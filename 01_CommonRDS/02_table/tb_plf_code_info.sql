-- Drop table

-- DROP TABLE wems.tb_plf_code_info;

CREATE TABLE wems.tb_plf_code_info (
	grp_cd varchar(30) NOT NULL, -- 그룹코드
	code varchar(20) NOT NULL, -- 코드
	code_nm varchar(255) NOT NULL, -- 코드명
	ref_1 varchar(255) NULL, -- 참조_1
	ref_2 varchar(255) NULL, -- 참조_2
	ref_3 varchar(255) NULL, -- 참조_3
	ref_4 varchar(255) NULL, -- 참조_4
	ref_5 varchar(255) NULL, -- 참조_5
	cd_desc varchar(255) NULL, -- 코드상세
	ord_seq float8 NULL, -- 정렬순서
	ref_6 varchar(255) NULL, -- 참조_6
	CONSTRAINT pk_plf_code_info PRIMARY KEY (grp_cd, code)
);
COMMENT ON TABLE wems.tb_plf_code_info IS '시스템 관리 및 운영에 필요한 코드 그룹 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_code_info.grp_cd IS '그룹코드';
COMMENT ON COLUMN wems.tb_plf_code_info.code IS '코드';
COMMENT ON COLUMN wems.tb_plf_code_info.code_nm IS '코드명';
COMMENT ON COLUMN wems.tb_plf_code_info.ref_1 IS '참조_1';
COMMENT ON COLUMN wems.tb_plf_code_info.ref_2 IS '참조_2';
COMMENT ON COLUMN wems.tb_plf_code_info.ref_3 IS '참조_3';
COMMENT ON COLUMN wems.tb_plf_code_info.ref_4 IS '참조_4';
COMMENT ON COLUMN wems.tb_plf_code_info.ref_5 IS '참조_5';
COMMENT ON COLUMN wems.tb_plf_code_info.cd_desc IS '코드상세';
COMMENT ON COLUMN wems.tb_plf_code_info.ord_seq IS '정렬순서';
COMMENT ON COLUMN wems.tb_plf_code_info.ref_6 IS '참조_6';

-- Permissions

ALTER TABLE wems.tb_plf_code_info OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_code_info TO wems;

-- Drop table

-- DROP TABLE wems.tb_opr_oper_logfile_hist;

CREATE TABLE wems.tb_opr_oper_logfile_hist (
	req_id numeric(18) NOT NULL, -- RequestId
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	logfile_type_cd varchar(4) NOT NULL, -- 로그분석 유형 코드
	req_tm varchar(14) NULL, -- 명령전송시각
	res_tm varchar(14) NULL, -- 응답수신시각
	cmd_req_status_cd varchar(4) NULL, -- 제어명령 진행상태 코드
	logfile_nm varchar(255) NULL, -- 로그파일명
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일자
	ip varchar(20) NULL, -- 업로드 서버 IP
	CONSTRAINT pk_opr_oper_logfile_hist PRIMARY KEY (req_id, device_id, device_type_cd)
);
COMMENT ON TABLE wems.tb_opr_oper_logfile_hist IS 'Logfile 요청 결과 내역을 보관한다';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_oper_logfile_hist.req_id IS 'RequestId';
COMMENT ON COLUMN wems.tb_opr_oper_logfile_hist.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_opr_oper_logfile_hist.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_opr_oper_logfile_hist.logfile_type_cd IS '로그분석 유형 코드';
COMMENT ON COLUMN wems.tb_opr_oper_logfile_hist.req_tm IS '명령전송시각';
COMMENT ON COLUMN wems.tb_opr_oper_logfile_hist.res_tm IS '응답수신시각';
COMMENT ON COLUMN wems.tb_opr_oper_logfile_hist.cmd_req_status_cd IS '제어명령 진행상태 코드';
COMMENT ON COLUMN wems.tb_opr_oper_logfile_hist.logfile_nm IS '로그파일명';
COMMENT ON COLUMN wems.tb_opr_oper_logfile_hist.create_dt IS '생성일자';
COMMENT ON COLUMN wems.tb_opr_oper_logfile_hist.ip IS '업로드 서버 IP';

-- Table Triggers

-- DROP TRIGGER tgr_act_req_log ON wems.tb_opr_oper_logfile_hist;

create trigger tgr_act_req_log after
update
    on
    wems.tb_opr_oper_logfile_hist for each row execute procedure wems."tgr_act_req_log$tb_opr_oper_logfile_hist"();

-- Permissions

ALTER TABLE wems.tb_opr_oper_logfile_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_oper_logfile_hist TO wems;

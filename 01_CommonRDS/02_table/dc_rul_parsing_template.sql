-- Drop table

-- DROP TABLE wems.dc_rul_parsing_template;

CREATE TABLE wems.dc_rul_parsing_template (
	id varchar(80) NOT NULL,
	parsingtemplategrpid varchar(80) NOT NULL,
	eventid varchar(32) NOT NULL,
	"sequence" numeric(18) NOT NULL,
	parsettype numeric(1) NOT NULL,
	headersize numeric(18) NULL,
	datasize numeric(18) NULL,
	skipline numeric(18) NULL,
	nextflag numeric(1) NULL,
	equipflag numeric(1) NULL,
	consumers numeric(18) NULL,
	ruleid varchar(80) NULL,
	udpate_user varchar(50) NULL,
	update_date timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()),
	xmlkeyelementtag varchar(1000) NULL,
	xmlrootelementtag varchar(1000) NULL,
	xmlrootelementcdatatag varchar(1000) NULL,
	xmlrootelementpcdata varchar(1000) NULL,
	xmlrootpcdatakind numeric(1) NULL,
	atomresulthideflag numeric(1) NULL,
	eofhideflag numeric(1) NULL
);

-- Permissions

ALTER TABLE wems.dc_rul_parsing_template OWNER TO wems;
GRANT ALL ON TABLE wems.dc_rul_parsing_template TO wems;

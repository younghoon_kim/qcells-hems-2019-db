-- Drop table

-- DROP TABLE wems.tb_bas_off_device;

CREATE TABLE wems.tb_bas_off_device (
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	work_flag varchar(1) NULL DEFAULT 'N'::character varying, -- 처리여부
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	update_dt timestamptz NULL, -- 수정일시
	CONSTRAINT pk_bas_off_device PRIMARY KEY (device_id, device_type_cd)
);
COMMENT ON TABLE wems.tb_bas_off_device IS '장비를 삭제하면 기준 정보 몇가지만 삭제 후 이 테이블에 insert 하면,  DB job 에서 주기적으로 남은 대량 데이터(통계, 이력 등)를 삭제한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_bas_off_device.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_bas_off_device.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_bas_off_device.work_flag IS '처리여부';
COMMENT ON COLUMN wems.tb_bas_off_device.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_bas_off_device.update_dt IS '수정일시';

-- Permissions

ALTER TABLE wems.tb_bas_off_device OWNER TO wems;
GRANT ALL ON TABLE wems.tb_bas_off_device TO wems;

-- Drop table

-- DROP TABLE wems.tb_plf_firmware_info_hist;

CREATE TABLE wems.tb_plf_firmware_info_hist (
	grp_id varchar(50) NOT NULL, -- 그룹ID
	grp_upt_seq float8 NOT NULL, -- 그룹 변경 이력 순번
	fwi_upt_seq float8 NOT NULL, -- 펌웨어 관리정보 변경이력 순번
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	product_model_nm varchar(30) NOT NULL, -- 장비모델명
	ems_model_nm varchar(30) NOT NULL, -- EMS모델명
	pcs_model_nm varchar(30) NOT NULL, -- PCS 모델명
	bms_model_nm varchar(30) NOT NULL, -- BMS 모델명
	ems_ver varchar(30) NOT NULL, -- EMS 버젼
	pcs_ver varchar(30) NOT NULL, -- PCS 버젼
	bms_ver varchar(30) NOT NULL, -- BMS 버젼
	ems_fw_url varchar(255) NOT NULL, -- EMS펌웨어 URL
	pcs_fw_url varchar(255) NOT NULL, -- PCS펌웨어 URL
	bms_fw_url varchar(255) NOT NULL, -- BMS 펌웨어 URL
	ems_fw_size numeric(16) NOT NULL, -- EMS 펌웨어 파일 크기
	pcs_fw_size numeric(16) NOT NULL, -- PCS 펌웨어 파일 크기
	bms_fw_size numeric(16) NOT NULL, -- BMS 펌웨어 파일 크기
	create_id varchar(64) NOT NULL, -- 등록자ID
	create_dt timestamptz NOT NULL, -- 등록일시
	update_id varchar(64) NOT NULL, -- 수정자ID
	update_dt timestamptz NOT NULL, -- 수정일시
	stable_ver_flag varchar(1) NULL, -- Stable 버전 여부
	CONSTRAINT pk_plf_firmware_info_hist PRIMARY KEY (grp_id, grp_upt_seq, fwi_upt_seq)
);
COMMENT ON TABLE wems.tb_plf_firmware_info_hist IS '장비의 펌웨어 관리정보가 변경된 이력을 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.grp_id IS '그룹ID';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.grp_upt_seq IS '그룹 변경 이력 순번';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.fwi_upt_seq IS '펌웨어 관리정보 변경이력 순번';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.product_model_nm IS '장비모델명';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.ems_model_nm IS 'EMS모델명';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.pcs_model_nm IS 'PCS 모델명';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.bms_model_nm IS 'BMS 모델명';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.ems_ver IS 'EMS 버젼';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.pcs_ver IS 'PCS 버젼';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.bms_ver IS 'BMS 버젼';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.ems_fw_url IS 'EMS펌웨어 URL';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.pcs_fw_url IS 'PCS펌웨어 URL';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.bms_fw_url IS 'BMS 펌웨어 URL';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.ems_fw_size IS 'EMS 펌웨어 파일 크기';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.pcs_fw_size IS 'PCS 펌웨어 파일 크기';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.bms_fw_size IS 'BMS 펌웨어 파일 크기';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.create_id IS '등록자ID';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.update_id IS '수정자ID';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.update_dt IS '수정일시';
COMMENT ON COLUMN wems.tb_plf_firmware_info_hist.stable_ver_flag IS 'Stable 버전 여부';

-- Permissions

ALTER TABLE wems.tb_plf_firmware_info_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_plf_firmware_info_hist TO wems;



CREATE FUNCTION wems."tgr_act_instl$tb_bas_device"() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE

BEGIN
   
    INSERT INTO wems.tb_opr_act (act_seq, act_type_cd, device_id, device_type_cd
    -- , REF_DESC
    	, ref_user_id, create_dt)
    VALUES (nextval('wems.sq_opr_act'), '30', new.device_id, new.device_type_cd
    -- , :NEW.INSTL_DT 
    	, new.instl_user_id, new.create_dt);
    
    RETURN NULL;
   
    EXCEPTION WHEN others THEN
            RETURN NULL;
END;
$$;

ALTER FUNCTION wems."tgr_act_instl$tb_bas_device"() OWNER TO wems;



CREATE FUNCTION wems."tgr_act_mtnc$tb_opr_mtnc"() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE

BEGIN
    INSERT INTO wems.tb_opr_act (act_seq, act_type_cd, device_id, device_type_cd, ref_seq, ref_type_cd, ref_desc, ref_user_id, create_dt)
    VALUES (nextval('wems.sq_opr_act'), '20', new.device_id, new.device_type_cd, new.mtnc_seq, new.mtnc_type_cd, new.subject
    		, ( SELECT instl_user_id FROM wems.tb_bas_device WHERE device_id = new.device_id)
    		, new.create_dt);

    RETURN NULL;
   
    EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END;
$$;

ALTER FUNCTION wems."tgr_act_mtnc$tb_opr_mtnc"() OWNER TO wems;


CREATE FUNCTION wems."tgr_act_req_log$tb_opr_oper_logfile_hist"() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE

BEGIN
    --  M2M 에서 ESS 으로 요청이 처리 된 후에 기록한다.
    IF new.cmd_req_status_cd IN ('51', '55') THEN
        INSERT INTO wems.tb_opr_act (act_seq, act_type_cd, device_id, device_type_cd, ref_seq, ref_type_cd, ref_desc, ref_user_id, create_dt)
        VALUES (nextval('wems.sq_opr_act'), '50', new.device_id, new.device_type_cd, new.req_id, new.logfile_type_cd, new.logfile_nm
        			, (select instl_user_id FROM wems.tb_bas_device WHERE device_id = new.device_id)
        			, new.create_dt);
    END IF;
   
    RETURN NULL;
   
    EXCEPTION WHEN OTHERS THEN
        RETURN NULL;
       
END;
$$;

ALTER FUNCTION wems."tgr_act_req_log$tb_opr_oper_logfile_hist"() OWNER TO wems;


CREATE FUNCTION wems."tgr_act_voc$tb_opr_voc"() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE

BEGIN
    INSERT INTO wems.tb_opr_act (act_seq, act_type_cd, device_id, device_type_cd, ref_seq, ref_type_cd, ref_desc, ref_user_id, create_dt)
    VALUES (nextval('wems.sq_opr_act'), '10', new.device_id, new.device_type_cd, new.voc_seq, new.voc_type_cd, new.subject
    		, (SELECT instl_user_id FROM wems.tb_bas_device WHERE device_id = new.device_id)
    		, new.create_dt);
       
    RETURN NULL;
   
    EXCEPTION WHEN OTHERS THEN
        RETURN NULL;
END;
$$;

ALTER FUNCTION wems."tgr_act_voc$tb_opr_voc"() OWNER TO wems;


CREATE FUNCTION wems."tgr_device_warranty$tb_bas_device"() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

DECLARE
    v_wrty_dt CHARACTER VARYING(8);
BEGIN
    IF new.instl_dt IS NOT NULL AND LENGTH(new.instl_dt) = 8 then
    	SELECT 	TO_CHAR( TO_TIMESTAMP(new.instl_dt, 'YYYYMMDD') + INTERVAL '60 MONTH'- INTERVAL '1 DAY', 'YYYYMMDD')	-- 60개월 -1일
    	INTO 	new.wrty_dt ;
    END IF;

   	RETURN NEW;
    
   	EXCEPTION WHEN OTHERS THEN
	    RETURN NEW;
END;
$$;

ALTER FUNCTION wems."tgr_device_warranty$tb_bas_device"() OWNER TO wems;




CREATE TRIGGER tgr_act_instl AFTER INSERT ON wems.tb_bas_device FOR EACH ROW EXECUTE PROCEDURE wems."tgr_act_instl$tb_bas_device"();

CREATE TRIGGER tgr_act_mtnc AFTER INSERT ON wems.tb_opr_mtnc FOR EACH ROW EXECUTE PROCEDURE wems."tgr_act_mtnc$tb_opr_mtnc"();

CREATE TRIGGER tgr_act_req_log AFTER UPDATE ON wems.tb_opr_oper_logfile_hist FOR EACH ROW EXECUTE PROCEDURE wems."tgr_act_req_log$tb_opr_oper_logfile_hist"();

CREATE TRIGGER tgr_act_voc AFTER INSERT ON wems.tb_opr_voc FOR EACH ROW EXECUTE PROCEDURE wems."tgr_act_voc$tb_opr_voc"();

CREATE TRIGGER tgr_device_warranty BEFORE INSERT ON wems.tb_bas_device FOR EACH ROW EXECUTE PROCEDURE wems."tgr_device_warranty$tb_bas_device"();


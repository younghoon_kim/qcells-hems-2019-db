--1.TB_BAS_API_USER_ORI
--------------------------------------------------
select * from TB_BAS_API_USER;

-- 데이터 백업
create table TB_BAS_API_USER_ORI AS
    select * from tb_bas_api_user;

select * from tb_bas_api_user
except
select * from tb_bas_api_user_ori;

-- 임시테이블 생성
create table tb_bas_api_user_han as
    select * from tb_bas_api_user
    where 0 = 1;

select * from tb_bas_api_user_han;
-- 기존 테이블의 데이터 삭제
truncate table tb_bas_api_user;



-- 데이터 추가
insert into tb_bas_api_user
select fn_get_encrypt(user_id, 'SDIENCK')      as user_id,
       fn_get_encrypt(user_pwd, 'SDIENCK')      as user_pwd,
       fn_get_encrypt(auth_type_cd, 'SDIENCK') as auth_type_cd,
       fn_get_encrypt(comn_nm, 'SDIENCK')      as comn_nm,
       fn_get_encrypt(create_id, 'SDIENCK')    as create_id,
       create_dt
from tb_bas_api_user_han;
--데이터 확인
select user_id, user_pwd, auth_type_cd, comn_nm, create_id from tb_bas_api_user
except
select user_id, user_pwd, auth_type_cd, comn_nm, create_id from tb_bas_api_user_ori;

--------------------------------------------------
--2. TB_BAS_API_USER_DEVICE
select *
from TB_BAS_API_USER_DEVICE;

-- 데이터 백업
create table TB_BAS_API_USER_DEVICE_ORI AS
select *
from TB_BAS_API_USER_DEVICE;

-- 임시테이블 생성
create table TB_BAS_API_USER_DEVICE_HAN as
select *
from TB_BAS_API_USER_DEVICE
where 0 = 1;

-- 기존 테이블의 데이터 삭제
select *
from TB_BAS_API_USER_DEVICE_HAN;
select *
from TB_BAS_API_USER_DEVICE;
truncate table TB_BAS_API_USER_DEVICE;

insert into tb_bas_api_user_device
select fn_get_encrypt(user_id, 'SDIENCK')   as user_id,
       device_id,
       device_type_cd,
       fn_get_encrypt(create_id, 'SDIENCK') as create_id,
       create_dt,
       update_id,
       update_dt
from TB_BAS_API_USER_DEVICE_HAN;

--데이터 확인
select * from tb_bas_api_user_device;


--------------------------------------------------
--3. TB_BAS_CITY

select * from TB_BAS_CITY;

-- 데이터 백업
create table TB_BAS_CITY_ORI AS
select *
from TB_BAS_CITY;

-- 임시테이블 생성
create table TB_BAS_CITY_HAN as
select *
from TB_BAS_CITY
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제
select * from TB_BAS_CITY_HAN;
select * from TB_BAS_CITY;
truncate table TB_BAS_CITY;


-- data insert
insert into tb_bas_city
select city_cd,
       latitude,
       longitude,
       utc_offset,
       city_nm,
       cntry_cd,
       cntry_nm,
       colec_flg,
       pop_cnt,
       create_dt,
       timezone_id
from tb_bas_city_han;

--데이터 확인
select * from tb_bas_city;

--------------------------------------------------
--4.TB_BAS_CNTRY

select * from TB_BAS_CNTRY;

-- 데이터 백업
create table TB_BAS_CNTRY_ORI AS
select *
from TB_BAS_CNTRY;

-- 임시테이블 생성
create table TB_BAS_CNTRY_HAN as
select *
from TB_BAS_CNTRY
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제
select COUNT(*) from TB_BAS_CNTRY_HAN;
select COUNT(*) from TB_BAS_CNTRY;
truncate table TB_BAS_CNTRY;


-- data insert
insert into TB_BAS_CNTRY
select
    CNTRY_CD, CNTRY_NM, MONE_UNIT_CD, CREATE_DT, LANG_CD, BASIC_LATITUDE, BASIC_LONGITUDE
from TB_BAS_CNTRY_HAN;

--데이터 확인
select * from TB_BAS_CNTRY;

--------------------------------------------------
--5.TB_BAS_CTL_GRP

select * from TB_BAS_CTL_GRP;
-- create_id는 암호화

-- 데이터 백업
create table TB_BAS_CTL_GRP_ORI AS
select *
from TB_BAS_CTL_GRP;

-- 임시테이블 생성
create table TB_BAS_CTL_GRP_HAN AS
select *
from TB_BAS_CTL_GRP
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제
select COUNT(*) from TB_BAS_CTL_GRP_HAN;
select COUNT(*) from TB_BAS_CTL_GRP;
truncate table TB_BAS_CTL_GRP;


-- data insert
insert into TB_BAS_CTL_GRP
select grp_id,
       grp_type_cd,
       grp_nm,
       ctl_type_cd,
       ctl_start_dt,
       ctl_end_dt,
       ctl_stus_cd,
       fn_get_encrypt(create_id, 'SDIENCK'),
       create_dt,
       update_id,
       update_dt
from TB_BAS_CTL_GRP_HAN;

--데이터 확인
select * from TB_BAS_CTL_GRP;

--------------------------------------------------
--5.TB_BAS_CTL_GRP_MGT

select * from TB_BAS_CTL_GRP_MGT;
-- create_id 암호화

-- 데이터 백업
create table TB_BAS_CTL_GRP_MGT_ORI AS
select *
from TB_BAS_CTL_GRP_MGT;

-- 임시테이블 생성
create table TB_BAS_CTL_GRP_MGT_HAN AS
select *
from TB_BAS_CTL_GRP_MGT
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제
select COUNT(*) from TB_BAS_CTL_GRP_MGT_HAN;
select COUNT(*) from TB_BAS_CTL_GRP_MGT;
truncate table TB_BAS_CTL_GRP_MGT;


-- data insert
insert into TB_BAS_CTL_GRP_MGT
select grp_id,
       device_id,
       device_type_cd,
       fn_get_encrypt(create_id, 'SDIENCK'),
       create_dt
from TB_BAS_CTL_GRP_MGT_HAN;

--데이터 확인
select * from TB_BAS_CTL_GRP_MGT;
--------------------------------------------------
--6.TB_BAS_DEVICE

select * from TB_BAS_DEVICE;
-- create_id 암호화

-- 데이터 백업
create table TB_BAS_DEVICE_ORI AS
select *
from TB_BAS_DEVICE;

-- 임시테이블 생성
create table TB_BAS_DEVICE_HAN AS
select *
from TB_BAS_DEVICE
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제
select COUNT(*) from TB_BAS_DEVICE_HAN;
select COUNT(*) from TB_BAS_DEVICE;
truncate table TB_BAS_DEVICE;

-- data insert
insert into TB_BAS_DEVICE
select device_id,
       device_type_cd,
       prn_device_id,
       mkr_id,
       device_nm,
       modl_nm,
       made_dt,
       standard,
       capacity,
       instl_floor,
       instl_place,
       instl_dt,
       fn_get_encrypt(pctr_file, 'SDIENCK'),
       draw_file,
       ip_addr,
       unit_id,
       port_no,
       product_model_nm,
       ems_model_nm,
       pcs_model_nm,
       bms_model_nm,
       ems_ver,
       pcs_ver,
       bms_ver,
       com_prot,
       remark,
       fn_get_encrypt(owner_nm, 'SDIENCK'),
       fn_get_encrypt(owner_addr, 'SDIENCK'),
       fn_get_encrypt(owner_tel_no, 'SDIENCK'),
       fn_get_encrypt(create_id, 'SDIENCK'),
       create_dt,
       fn_get_encrypt(update_id, 'SDIENCK'),
       update_dt,
       fn_get_encrypt(owner_email, 'SDIENCK'),
       fn_get_encrypt(instl_user_id, 'SDIENCK'),
       fn_get_encrypt(instl_addr, 'SDIENCK'),
       cntry_cd,
       city_cd,
       elpw_prod_cd,
       wrty_dt,
       ems_ip_addr,
       latitude,
       longitude,
       agree_sign,
       oper_test_dt,
       pctr_file_path,
       pctr_file_disp_name,
       pin_code,
       timezone_id,
       ems_update_dt,
       serial_no,
       battery1,
       battery2,
       battery3,
       battery4,
       battery5,
       upgrade_state,
       upgrade_dt,
       connector_id
from TB_BAS_DEVICE_HAN;

--데이터 확인
select * from TB_BAS_DEVICE;
--------------------------------------------------
--7.TB_BAS_ELPW_UNIT_PRICE

select * from TB_BAS_ELPW_UNIT_PRICE;


-- 데이터 백업
create table TB_BAS_ELPW_UNIT_PRICE_ORI AS
select *
from TB_BAS_ELPW_UNIT_PRICE;

-- 임시테이블 생성
create table TB_BAS_ELPW_UNIT_PRICE_HAN AS
select *
from TB_BAS_ELPW_UNIT_PRICE
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제
select COUNT(*) from TB_BAS_ELPW_UNIT_PRICE_HAN;
select COUNT(*) from TB_BAS_ELPW_UNIT_PRICE;
truncate table TB_BAS_ELPW_UNIT_PRICE;

-- data insert
insert into TB_BAS_ELPW_UNIT_PRICE
select elpw_prod_cd,
       unit_ym,
       unit_hh,
       io_flag,
       unit_price,
       use_flag,
       create_id,
       create_dt,
       wkday_flag,
       unit_mi
from TB_BAS_ELPW_UNIT_PRICE_HAN;

--데이터 확인
select * from TB_BAS_ELPW_UNIT_PRICE;

--------------------------------------------------
--7.TB_BAS_MODEL_ALARM_CODE_MAP

select * from TB_BAS_MODEL_ALARM_CODE_MAP;
--여긴 넘어감

--------------------------------------------------
--8.TB_BAS_OFF_DEVICE_HIST

select * from TB_BAS_OFF_DEVICE_HIST;


-- 데이터 백업
create table TB_BAS_OFF_DEVICE_HIST_ORI AS
select *
from TB_BAS_OFF_DEVICE_HIST;

-- 임시테이블 생성
create table TB_BAS_OFF_DEVICE_HIST_HAN AS
select *
from TB_BAS_OFF_DEVICE_HIST
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제
select * from TB_BAS_OFF_DEVICE_HIST_HAN;
select COUNT(*) from TB_BAS_OFF_DEVICE_HIST_HAN;
select COUNT(*) from TB_BAS_OFF_DEVICE_HIST;
truncate table TB_BAS_OFF_DEVICE_HIST;


-- data insert
insert into TB_BAS_OFF_DEVICE_HIST
select device_id, device_type_cd, work_flag, create_dt, update_dt
from TB_BAS_OFF_DEVICE_HIST_HAN;

--데이터 확인
select * from TB_BAS_OFF_DEVICE_HIST;
--------------------------------------------------
--9.TB_BAS_OPRTR_DEVICE

select * from TB_BAS_OPRTR_DEVICE;
-- user_id, create_id 암호화

-- 데이터 백업
create table TB_BAS_OPRTR_DEVICE_ORI AS
select *
from TB_BAS_OPRTR_DEVICE;

-- 임시테이블 생성
create table TB_BAS_OPRTR_DEVICE_HAN AS
select *
from TB_BAS_OPRTR_DEVICE
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제
select * from TB_BAS_OPRTR_DEVICE_HAN;;
select COUNT(*) from TB_BAS_OPRTR_DEVICE_HAN;
select COUNT(*) from TB_BAS_OPRTR_DEVICE;
truncate table TB_BAS_OPRTR_DEVICE;


-- data insert
insert into TB_BAS_OPRTR_DEVICE
select
       fn_get_encrypt(user_id, 'SDIENCK'),
       device_id,
       device_type_cd,
       fn_get_encrypt(create_id, 'SDIENCK'),
       create_dt,
       update_id,
       update_dt
from TB_BAS_OPRTR_DEVICE_HAN;

--데이터 확인
select * from TB_BAS_OPRTR_DEVICE;
--------------------------------------------------
--10.TB_BAS_PTNR

select * from TB_BAS_PTNR;
-- 기존데이터랑 같은거 같아 패스
--------------------------------------------------
--11.TB_BAS_SVC_CNTR

select *
from TB_BAS_SVC_CNTR;
-- create_id, update_id 암호


-- 데이터 백업
create table TB_BAS_SVC_CNTR_ORI AS
select *
from TB_BAS_SVC_CNTR;

-- 임시테이블 생성
create table TB_BAS_SVC_CNTR_HAN AS
select *
from TB_BAS_SVC_CNTR
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제
select *
from TB_BAS_SVC_CNTR_HAN;;
select COUNT(*)
from TB_BAS_SVC_CNTR_HAN;
select COUNT(*)
from TB_BAS_SVC_CNTR;
truncate table TB_BAS_SVC_CNTR;


-- data insert
insert into tb_bas_svc_cntr
select svc_cntr_id,
       svc_cntr_nm,
       cntry_cd,
       addr,
       email,
       mpn_no,
       fn_get_encrypt(create_id, 'SDIENCK'),
       create_dt,
       fn_get_encrypt(update_id, 'SDIENCK'),
       update_dt,
       staff_nm,
       ord_seq
from tb_bas_svc_cntr_han;

--데이터 확인
select *
from tb_bas_svc_cntr;

--------------------------------------------------
--12.TB_BAS_TIMEZONE

select *
from TB_BAS_TIMEZONE
order by create_dt DESC;
-- 수정사항이 있어 EU데이터로 import


-- 데이터 백업
create table TB_BAS_TIMEZONE_ORI AS
select *
from TB_BAS_TIMEZONE;

-- 임시테이블 생성
create table TB_BAS_TIMEZONE_HAN AS
select *
from TB_BAS_TIMEZONE
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제
select COUNT(*)
from TB_BAS_TIMEZONE_HAN;
select COUNT(*)
from TB_BAS_TIMEZONE;
truncate table TB_BAS_TIMEZONE;


-- data insert
insert into TB_BAS_TIMEZONE
select timezone_id,
       utc_offset,
       dst_start_exp,
       dst_end_exp,
       dst_use_flag,
       create_dt
from TB_BAS_TIMEZONE_HAN;

--데이터 확인
select *
from TB_BAS_TIMEZONE;
--------------------------------------------------
--13.TB_BAS_UPT_GRP

select *
from TB_BAS_UPT_GRP;
-- create_id, update_id

-- 데이터 백업
create table TB_BAS_UPT_GRP_ORI AS
select *
from TB_BAS_UPT_GRP;

-- 임시테이블 생성
create table TB_BAS_UPT_GRP_HAN AS
select *
from TB_BAS_UPT_GRP
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제
select * from TB_BAS_UPT_GRP_HAN;
select COUNT(*) from TB_BAS_UPT_GRP_HAN;
select COUNT(*) from TB_BAS_UPT_GRP;
truncate table TB_BAS_UPT_GRP;


-- data insert
insert into TB_BAS_UPT_GRP
select grp_id,
       grp_nm,
       grp_desc,
       fn_get_encrypt(create_id, 'SDIENCK'),
       create_dt,
       fn_get_encrypt(update_id, 'SDIENCK'),
       update_dt,
       grp_upt_seq
from TB_BAS_UPT_GRP_HAN;

--데이터 확인
select *
from TB_BAS_UPT_GRP;
--------------------------------------------------
--14.TB_BAS_UPT_GRP_HIST

select *
from TB_BAS_UPT_GRP_HIST;
-- create_id, update_id

-- 데이터 백업
create table TB_BAS_UPT_GRP_HIST_ORI AS
select *
from TB_BAS_UPT_GRP_HIST;

-- 임시테이블 생성
create table TB_BAS_UPT_GRP_HIST_HAN AS
select *
from TB_BAS_UPT_GRP_HIST
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제
select * from TB_BAS_UPT_GRP_HIST_HAN;
select COUNT(*) from TB_BAS_UPT_GRP_HIST_HAN;
select COUNT(*) from TB_BAS_UPT_GRP_HIST;
truncate table TB_BAS_UPT_GRP_HIST;


-- data insert
insert into TB_BAS_UPT_GRP_HIST
select
grp_id,
       grp_upt_seq,
       grp_nm,
       grp_desc,
       fn_get_encrypt(create_id, 'SDIENCK'),
       create_dt,
       fn_get_encrypt(update_id, 'SDIENCK'),
       update_dt
from TB_BAS_UPT_GRP_HIST_HAN;

--데이터 확인
select * from TB_BAS_UPT_GRP_HIST;

--------------------------------------------------
--15.TB_BAS_USER_DEVICE

select *
from TB_BAS_USER_DEVICE;
-- user_id, create_id

-- 데이터 백업
create table TB_BAS_USER_DEVICE_ORI AS
select *
from TB_BAS_USER_DEVICE;

-- 임시테이블 생성
create table TB_BAS_USER_DEVICE_HAN AS
select *
from TB_BAS_USER_DEVICE
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제
truncate table TB_BAS_USER_DEVICE_HAN;
select * from TB_BAS_USER_DEVICE_HAN;
select COUNT(*) from TB_BAS_USER_DEVICE_HAN;
select COUNT(*) from TB_BAS_USER_DEVICE;
truncate table TB_BAS_USER_DEVICE;


-- data insert
insert into TB_BAS_USER_DEVICE
select fn_get_encrypt(user_id, 'SDIENCK'),
       device_id,
       device_type_cd,
       fn_get_encrypt(create_id, 'SDIENCK'),
       create_dt,
       update_id,
       update_dt
from TB_BAS_USER_DEVICE_HAN;

--데이터 확인
select * from TB_BAS_USER_DEVICE;
--------------------------------------------------
--16.TB_BAS_UTILITY_INSTALLER

select *
from TB_BAS_UTILITY_INSTALLER;
-- 데이터가 같으므로 업데이트 하지 않음
--------------------------------------------------
--17.TB_OPR_ALARM_HIST

select *
from TB_OPR_ALARM_HIST;
-- 일단 나중에 작업

--------------------------------------------------
--18.TB_BAS_USER

select * from TB_BAS_USER;
--        암호화 컬럼
--        user_id,
--        user_nm,
--        user_pwd,
--        email,
--        mpn_no,
--        auth_type_cd,
--        lang_cd,
--        agree_flag,
--        lock_flag,
--        create_id,
--        update_id,
--        comn_nm,

-- 데이터 백업
create table TB_BAS_USER_ORI AS
select *
from TB_BAS_USER;

select * from tb_bas_user_ori;
-- 임시테이블 생성
create table TB_BAS_USER_HAN AS
select *
from TB_BAS_USER
where 0 = 1;

-- 데이터 import


-- 기존 테이블의 데이터 삭제

select * from TB_BAS_USER_HAN;
select COUNT(*) from TB_BAS_USER_HAN;
select COUNT(*) from TB_BAS_USER;
truncate table TB_BAS_USER;


-- data insert
insert into TB_BAS_USER
select
       fn_get_encrypt(user_id, 'SDIENCK'),
       fn_get_encrypt(user_nm, 'SDIENCK'),
       crypt(user_pwd, gen_salt('md5')),
       fn_get_encrypt(email, 'SDIENCK'),
       fn_get_encrypt(mpn_no, 'SDIENCK'),
       fn_get_encrypt(auth_type_cd, 'SDIENCK'),
       fn_get_encrypt(lang_cd, 'SDIENCK'),
       join_dt,
       leave_dt,
       fn_get_encrypt(agree_flag, 'SDIENCK'),
       fn_get_encrypt(lock_flag, 'SDIENCK'),
       lock_start_dt,
       fn_get_encrypt(create_id, 'SDIENCK'),
       create_dt,
       fn_get_encrypt(update_id, 'SDIENCK'),
       update_dt,
       fn_get_encrypt(comn_nm, 'SDIENCK'),
       bld_area_val,
       bld_area_cd,
       last_unlock_dt,
       alarm_mail_recv_flag,
       m_rpt_recv_flag,
       m_rpt_send_dt,
       terms_dt,
       migration_agree_too,
       migration_agree_dt,
       user_edit,
       gdpr_agree_flag,
       gdpr_agree_dt

from TB_BAS_USER_HAN;

--데이터 확인
select * from TB_BAS_USER;


TRUNCATE TABLE wems.TB_STT_ESS_DD;
SELECT aws_s3.table_import_from_s3(
   	'wems.TB_STT_ESS_DD',
	'colec_dd, utc_offset, device_id, device_type_cd, pv_pw_h, pv_pw_price, pv_pw_co2, cons_pw_h, cons_pw_price, cons_pw_co2, cons_grid_pw_h, cons_grid_pw_price, cons_grid_pw_co2, cons_pv_pw_h, cons_pv_pw_price, cons_pv_pw_co2, cons_bt_pw_h, cons_bt_pw_price, cons_bt_pw_co2, bt_chrg_pw_h, bt_chrg_pw_price, bt_chrg_pw_co2, bt_dchrg_pw_h, bt_dchrg_pw_price, bt_dchrg_pw_co2, grid_ob_pw_h, grid_ob_pw_price, grid_ob_pw_co2, grid_tr_pw_h, grid_tr_pw_price, grid_tr_pw_co2, grid_tr_pv_pw_h, grid_tr_pv_pw_price, grid_tr_pv_pw_co2, grid_tr_bt_pw_h, grid_tr_bt_pw_price, grid_tr_bt_pw_co2, create_dt, bt_soc, outlet_pw_h, outlet_pw_price, outlet_pw_co2, pv_pw, cons_pw, bt_pw, bt_soh, grid_pw, pcs_pw, pcs_tgt_pw, pcs_fd_pw_h, pcs_fd_pw_price, pcs_fd_pw_co2, pcs_pch_pw_h, pcs_pch_pw_price, pcs_pch_pw_co2, outlet_pw, pv_v1, pv_i1, pv_pw1, pv_v2, pv_i2, pv_pw2, inverter_v, inverter_i, inverter_pw, dc_link_v, g_rly_cnt, bat_rly_cnt, grid_to_grid_rly_cnt, bat_pchrg_rly_cnt, rack_v, rack_i, cell_max_v, cell_min_v, cell_avg_v, cell_max_t, cell_min_t, cell_avg_t',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', '2020_03_25_STT/TB_STT_ESS_DD.csv', 'eu-central-1')
);

TRUNCATE TABLE wems.TB_STT_ESS_MM;
SELECT aws_s3.table_import_from_s3(
   	'wems.TB_STT_ESS_MM',
	'colec_mm, utc_offset, device_id, device_type_cd, pv_pw_h, pv_pw_price, pv_pw_co2, cons_pw_h, cons_pw_price, cons_pw_co2, cons_grid_pw_h, cons_grid_pw_price, cons_grid_pw_co2, cons_pv_pw_h, cons_pv_pw_price, cons_pv_pw_co2, cons_bt_pw_h, cons_bt_pw_price, cons_bt_pw_co2, bt_chrg_pw_h, bt_chrg_pw_price, bt_chrg_pw_co2, bt_dchrg_pw_h, bt_dchrg_pw_price, bt_dchrg_pw_co2, grid_ob_pw_h, grid_ob_pw_price, grid_ob_pw_co2, grid_tr_pw_h, grid_tr_pw_price, grid_tr_pw_co2, grid_tr_pv_pw_h, grid_tr_pv_pw_price, grid_tr_pv_pw_co2, grid_tr_bt_pw_h, grid_tr_bt_pw_price, grid_tr_bt_pw_co2, create_dt, bt_soc, outlet_pw_h, outlet_pw_price, outlet_pw_co2, pcs_fd_pw_h, pcs_fd_pw_price, pcs_fd_pw_co2, pcs_pch_pw_h, pcs_pch_pw_price, pcs_pch_pw_co2',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', '2020_03_25_STT/TB_STT_ESS_MM.csv', 'eu-central-1')
);

TRUNCATE TABLE wems.TB_STT_ESS_TM;
SELECT aws_s3.table_import_from_s3(
   	'wems.TB_STT_ESS_TM',
	'colec_tm, device_id, device_type_cd, pv_pw_h, pv_pw_price, pv_pw_co2, cons_pw_h, cons_pw_price, cons_pw_co2, cons_grid_pw_h, cons_grid_pw_price, cons_grid_pw_co2, cons_pv_pw_h, cons_pv_pw_price, cons_pv_pw_co2, cons_bt_pw_h, cons_bt_pw_price, cons_bt_pw_co2, bt_chrg_pw_h, bt_chrg_pw_price, bt_chrg_pw_co2, bt_dchrg_pw_h, bt_dchrg_pw_price, bt_dchrg_pw_co2, grid_ob_pw_h, grid_ob_pw_price, grid_ob_pw_co2, grid_tr_pw_h, grid_tr_pw_price, grid_tr_pw_co2, grid_tr_pv_pw_h, grid_tr_pv_pw_price, grid_tr_pv_pw_co2, grid_tr_bt_pw_h, grid_tr_bt_pw_price, grid_tr_bt_pw_co2, create_dt, bt_soc, outlet_pw_h, outlet_pw_price, outlet_pw_co2, pv_pw, cons_pw, bt_pw, bt_soh, grid_pw, pcs_pw, pcs_tgt_pw, pcs_fd_pw_h, pcs_fd_pw_price, pcs_fd_pw_co2, pcs_pch_pw_h, pcs_pch_pw_price, pcs_pch_pw_co2, outlet_pw, pv_v1, pv_i1, pv_pw1, pv_v2, pv_i2, pv_pw2, inverter_v, inverter_i, inverter_pw, dc_link_v, g_rly_cnt, bat_rly_cnt, grid_to_grid_rly_cnt, bat_pchrg_rly_cnt, rack_v, rack_i, cell_max_v, cell_min_v, cell_avg_v, cell_max_t, cell_min_t, cell_avg_t, smtr_tr_cnter, smtr_ob_cnter',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', '2020_03_25_STT/TB_STT_ESS_TM.csv', 'eu-central-1')
);

TRUNCATE TABLE wems.TB_STT_ESS_YY;
SELECT aws_s3.table_import_from_s3(
   	'wems.TB_STT_ESS_YY',
	'colec_yy, utc_offset, device_id, device_type_cd, pv_pw_h, pv_pw_price, pv_pw_co2, cons_pw_h, cons_pw_price, cons_pw_co2, cons_grid_pw_h, cons_grid_pw_price, cons_grid_pw_co2, cons_pv_pw_h, cons_pv_pw_price, cons_pv_pw_co2, cons_bt_pw_h, cons_bt_pw_price, cons_bt_pw_co2, bt_chrg_pw_h, bt_chrg_pw_price, bt_chrg_pw_co2, bt_dchrg_pw_h, bt_dchrg_pw_price, bt_dchrg_pw_co2, grid_ob_pw_h, grid_ob_pw_price, grid_ob_pw_co2, grid_tr_pw_h, grid_tr_pw_price, grid_tr_pw_co2, grid_tr_pv_pw_h, grid_tr_pv_pw_price, grid_tr_pv_pw_co2, grid_tr_bt_pw_h, grid_tr_bt_pw_price, grid_tr_bt_pw_co2, create_dt, bt_soc, outlet_pw_h, outlet_pw_price, outlet_pw_co2, pcs_fd_pw_h, pcs_fd_pw_price, pcs_fd_pw_co2, pcs_pch_pw_h, pcs_pch_pw_price, pcs_pch_pw_co2',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', '2020_03_25_STT/TB_STT_ESS_YY.csv', 'eu-central-1')
);


TRUNCATE TABLE wems.TB_STT_QUITY_WRTY_DD;
SELECT aws_s3.table_import_from_s3(
   	'wems.TB_STT_QUITY_WRTY_DD',
	'colec_dd, device_id, device_type_cd, cell_max_vol_avg, cell_min_vol_avg, rack_cur_avg, cell_max_temp_avg, cell_min_temp_avg, emsboard_temp_max_avg, emsboard_temp_min_avg, last_soh, create_dt',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', '2020_03_25_STT/TB_STT_QUITY_WRTY_DD.csv', 'eu-central-1')
);


TRUNCATE TABLE wems.TB_STT_RANK_DD;
SELECT aws_s3.table_import_from_s3(
   	'wems.TB_STT_RANK_DD',
	'colec_dd, utc_offset, rank_type_cd, device_id, device_type_cd, rank_per, rank_no, total_cnt, cntry_cd, create_dt',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', '2020_03_25_STT/TB_STT_RANK_DD.csv', 'eu-central-1')
);


TRUNCATE TABLE wems.TB_STT_RANK_MM;
SELECT aws_s3.table_import_from_s3(
   	'wems.TB_STT_RANK_MM',
	'colec_mm, utc_offset, rank_type_cd, device_id, device_type_cd, rank_per, rank_no, total_cnt, cntry_cd, create_dt',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', '2020_03_25_STT/TB_STT_RANK_MM.csv', 'eu-central-1')
);









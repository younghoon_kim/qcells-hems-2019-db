
--------------------------------------------------------------------------------------------------------------------------------------
--              init DB
---------------------------------------------------------------------------------------------------------------------------------------

-- 1. user(role)
CREATE ROLE wems WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION;

ALTER ROLE wems SET search_path = wems, public, pg_catalog;
ALTER ROLE wems WITH PASSWORD 'ejQkffl!';	-- 더빨리!

GRANT wems TO postgres;	-- AWS RDS에서 반드시 필요

-- 2. Database: wemsdb 생성

-- DROP DATABASE wemsdb;
CREATE DATABASE wemsdb
    WITH 
    OWNER = wems
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8'
--    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

      
ALTER DATABASE wemsdb
    SET search_path TO 'wems, public';

-- show LC_COLLATE;
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

-- postgres 계정으로, 앞에서 생성한 wemsdb 에 접속함.
CREATE SCHEMA wems;
ALTER SCHEMA wems OWNER TO wems;

-- rds 의 s3 import 를 위한 extension 을 설치함.
CREATE EXTENSION pgcrypto schema public;
CREATE EXTENSION aws_s3 CASCADE schema public;   -- aws_commons 까지 설치됨

-- data 를 import 할때에, pg_settings 값중에 일부를 수정해 주기를 권장하고 있음.
-- (https://docs.aws.amazon.com/ko_kr/AmazonRDS/latest/UserGuide/PostgreSQL.Procedural.Importing.html#USER_PostgreSQL.S3Import.Overview)

-- pg_settings 값 확인
SELECT *
FROM pg_settings
WHERE name = ${pg_settings_name}


-- ip컬럼 확인 (8개)
TRUNCATE TABLE wems.tbd_eqp_hosthw;
SELECT aws_s3.table_import_from_s3(
      'wems.tbd_eqp_hosthw',
   'name, ip, agentequipid, description',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TBD_EQP_HOSTHW.csv', 'eu-central-1')
);

-- 10개
TRUNCATE TABLE wems.tb_bas_api_user;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_api_user',
   'user_id, user_pwd, auth_type_cd, comn_nm, create_id, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_API_USER.csv', 'eu-central-1')
);

-- 1373 -> 1553 -> 1568
TRUNCATE TABLE wems.tb_bas_api_user_device;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_api_user_device',
   'user_id, device_id, device_type_cd, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_API_USER_DEVICE.csv', 'eu-central-1')
);

-- 4,395 -> 4,883 -> 4968
TRUNCATE TABLE wems.tb_bas_city;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_city',
   'city_cd, latitude, longitude, utc_offset, city_nm, cntry_cd, cntry_nm, colec_flg, pop_cnt, create_dt, timezone_id',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_CITY.csv', 'eu-central-1')
);

-- 12
TRUNCATE TABLE wems.tb_bas_cntry;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_cntry',
   'cntry_cd, cntry_nm, mone_unit_cd, create_dt, lang_cd, basic_latitude, basic_longitude',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_CNTRY.csv', 'eu-central-1')
);

-- 232 -> 239 -> 241
TRUNCATE TABLE wems.tb_bas_ctl_grp;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_ctl_grp',
   'grp_id, grp_type_cd, grp_nm, ctl_type_cd, ctl_start_dt, ctl_end_dt, ctl_stus_cd, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_CTL_GRP.csv', 'eu-central-1')
);

-- 267 -> 261 -> 265
TRUNCATE TABLE wems.tb_bas_ctl_grp_mgt;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_ctl_grp_mgt',
   'grp_id, device_id, device_type_cd, create_id, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_CTL_GRP_MGT.csv', 'eu-central-1')
);

-- 4,223 -> 4,665 -> 4,740
TRUNCATE TABLE wems.tb_bas_device;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_device',
   'device_id, device_type_cd, prn_device_id, mkr_id, device_nm, modl_nm, made_dt, standard, capacity
   , instl_floor, instl_place, instl_dt, pctr_file, draw_file, ip_addr, unit_id, port_no, product_model_nm
   , ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, com_prot, remark
   , owner_nm, owner_addr, owner_tel_no, create_id, create_dt, update_id, update_dt, owner_email
   , instl_user_id, instl_addr, cntry_cd, city_cd, elpw_prod_cd, wrty_dt, ems_ip_addr, latitude, longitude
   , agree_sign, oper_test_dt, pctr_file_path, pctr_file_disp_name, pin_code, timezone_id, ems_update_dt
   , serial_no, battery1, battery2, battery3, battery4, battery5, upgrade_state, upgrade_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_DEVICE.csv', 'eu-central-1')
);

-- 409,632 -> 452,256 -> 459,456
TRUNCATE TABLE wems.tb_bas_elpw_unit_price;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_elpw_unit_price',
   'elpw_prod_cd, unit_ym, unit_hh, io_flag, unit_price, use_flag, create_id, create_dt, wkday_flag, unit_mi',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_ELPW_UNIT_PRICE.csv', 'eu-central-1')
);

-- 1
TRUNCATE TABLE wems.tb_bas_mkr;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_mkr',
   'mkr_id, mkr_nm, mkr_nm_s, mkr_addr, mkr_sales_email, mkr_tel_no, mkr_prms_no, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_MKR.csv', 'eu-central-1')
);

-- 245 -> 241
TRUNCATE TABLE wems.tb_bas_model_alarm_code_map;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_model_alarm_code_map',
   'product_model_type_cd, alarm_type_cd, alarm_cd, apply_user_flag, apply_instlr_flag, apply_oprtr_flag, 
   apply_admin_flag, noti_user_flag, noti_instlr_flag, noti_oprtr_flag, noti_admin_flag, create_id, create_dt, 
   update_id, update_dt, alarm_title, alarm_desc',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_MODEL_ALARM_CODE_MAP.csv', 'eu-central-1')
);

-- 0 -> 1 -> 0
TRUNCATE TABLE wems.tb_bas_off_device;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_off_device',
   'device_id, device_type_cd, work_flag, create_dt, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_OFF_DEVICE.csv', 'eu-central-1')
);

-- 613 -> 655 -> 666
TRUNCATE TABLE wems.tb_bas_off_device_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_off_device_hist',
   'device_id, device_type_cd, work_flag, create_dt, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_OFF_DEVICE_HIST.csv', 'eu-central-1')
);

-- 1,953 -> 2,389 -> 2,456
TRUNCATE TABLE wems.tb_bas_oprtr_device;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_oprtr_device',
   'user_id, device_id, device_type_cd, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_OPRTR_DEVICE.csv', 'eu-central-1')
);

-- 4
TRUNCATE TABLE wems.tb_bas_ptnr;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_ptnr',
   'ptnr_id, ptnr_nm, cntry_cd, homepage, addr, latitude, longitude, pctr_file, 
   pctr_file_path, pctr_file_disp_name, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_PTNR.csv', 'eu-central-1')
);

-- 41 -> 50
TRUNCATE TABLE wems.tb_bas_svc_cntr;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_svc_cntr',
   'svc_cntr_id, svc_cntr_nm, cntry_cd, addr, email, mpn_no, create_id, create_dt, update_id, update_dt, staff_nm, ord_seq',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_SVC_CNTR.csv', 'eu-central-1')
);

-- 한솔에서 전달받은 backup data 가 이나리, postgresql용으로 만들어 놓은 데이터를 impor 해야 함 (431 개)
TRUNCATE TABLE wems.tb_bas_timezone;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_timezone',
   'timezone_id, utc_offset, dst_start_exp, dst_end_exp, dst_use_flag, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_TIMEZONE.csv', 'eu-central-1')
);

-- 70 -> 69 -> 73
TRUNCATE TABLE wems.tb_bas_upt_grp;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_upt_grp',
   'grp_id, grp_nm, grp_desc, create_id, create_dt, update_id, update_dt, grp_upt_seq',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_UPT_GRP.csv', 'eu-central-1')
);

-- 7 -> 37 -> 39
TRUNCATE TABLE wems.tb_bas_upt_grp_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_upt_grp_hist',
   'grp_id, grp_upt_seq, grp_nm, grp_desc, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_UPT_GRP_HIST.csv', 'eu-central-1')
);

-- 69 -> 63 -> 68
TRUNCATE TABLE wems.tb_bas_upt_grp_mgt;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_upt_grp_mgt',
   'grp_id, device_id, device_type_cd, create_id, create_dt, firmware_exception',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_UPT_GRP_MGT.csv', 'eu-central-1')
);

-- 4,226 -> 4,652 -> 4,715
TRUNCATE TABLE wems.tb_bas_user;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_user',
   'user_id, user_nm, user_pwd, email, mpn_no, auth_type_cd, lang_cd, join_dt, leave_dt, agree_flag, lock_flag, 
   lock_start_dt, create_id, create_dt, update_id, update_dt, comn_nm, bld_area_val, bld_area_cd, last_unlock_dt, 
   alarm_mail_recv_flag, m_rpt_recv_flag, m_rpt_send_dt, terms_dt, migration_agree_too, migration_agree_dt, 
   user_edit, gdpr_agree_flag, gdpr_agree_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_USER.csv', 'eu-central-1')
);

-- 3,319 -> 3,662 -> 3,709
TRUNCATE TABLE wems.tb_bas_user_device;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_user_device',
   'user_id, device_id, device_type_cd, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_USER_DEVICE.csv', 'eu-central-1')
);

-- 828 -> 907 -> 922
TRUNCATE TABLE wems.tb_bas_user_hash;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_user_hash',
   'user_id, user_id_hash, user_pwd_hash, user_email_hash, salt, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_USER_HASH.csv', 'eu-central-1')
);

-- 3
TRUNCATE TABLE wems.tb_bas_utility_installer;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_utility_installer',
   'utility_id, installer_id, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_UTILITY_INSTALLER.csv', 'eu-central-1')
);

-- 9,489 -> 2,111,714 -> 758,279
TRUNCATE TABLE wems.tb_opr_act;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_act',
   'act_seq, act_type_cd, device_id, device_type_cd, ref_seq, ref_type_cd, ref_desc, ref_user_id, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_ACT.csv', 'eu-central-1')
);

-- 754,721
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_act',
   'act_seq, act_type_cd, device_id, device_type_cd, ref_seq, ref_type_cd, ref_desc, ref_user_id, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_ACT_2.csv', 'eu-central-1')
);

-- 601,544
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_act',
   'act_seq, act_type_cd, device_id, device_type_cd, ref_seq, ref_type_cd, ref_desc, ref_user_id, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_ACT_3.csv', 'eu-central-1')
);

-- 250 -> 1,928 -> 2,024
TRUNCATE TABLE wems.tb_opr_alarm;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_alarm',
   'alarm_id, alarm_id_seq, device_id, device_type_cd, alarm_type_cd, alarm_cd, event_dt, noti_flag, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_ALARM.csv', 'eu-central-1')
);

-- 0 -> 14
TRUNCATE TABLE wems.tb_opr_alarm_err_list;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_alarm_err_list',
   'alarm_id, alarm_id_seq',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_ALARM_ERR_LIST.csv', 'eu-central-1')
);

-- 112,248 -> 112,248 -> 899,374
TRUNCATE TABLE wems.tb_opr_alarm_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_alarm_hist',
   'alarm_id, alarm_id_seq, device_id, device_type_cd, alarm_type_cd, alarm_cd, event_dt, clear_dt, 
   manu_recv_flag, manu_recv_user_id, manu_recv_desc, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_ALARM_HIST.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.tb_opr_as_qna;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_as_qna',
   'as_qna_seq, device_id, device_type_cd, as_qna_type_cd, subject, contents1, contents2, repl_flag, create_id, create_dt, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_AS_QNA.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.tb_opr_as_qna_file;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_as_qna_file',
   'as_qna_seq, ord_seq, file_name, file_path, disp_name, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_AS_QNA_FILE.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.tb_opr_as_qna_repl;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_as_qna_repl',
   'as_qna_seq, ord_seq, contents1, contents2, create_id, create_dt, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_AS_QNA_REPL.csv', 'eu-central-1')
);

-- 8 -> 375,328 ->380,722
TRUNCATE TABLE wems.tb_opr_ctrl_result;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_ctrl_result',
   'req_id, req_type_cd, device_id, req_tm, res_tm, result_cd, cmd_type_cd, logfilename, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_CTRL_RESULT.csv', 'eu-central-1')
);

-- 201 -> 234 -> 237
TRUNCATE TABLE wems.tb_opr_cust_qna;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_cust_qna',
   'cust_qna_seq, subject, contents1, contents2, repl_flag, pwd, create_id, create_dt, update_dt, cust_nm, cust_tel_no, cust_email, cust_addr',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_CUST_QNA.csv', 'eu-central-1')
);

-- 22 -> 27 -> 28
TRUNCATE TABLE wems.tb_opr_cust_qna_file;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_cust_qna_file',
   'cust_qna_seq, ord_seq, file_name, file_path, disp_name, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_CUST_QNA_FILE.csv', 'eu-central-1')
);

-- 19 -> 20 -> 21
TRUNCATE TABLE wems.tb_opr_cust_qna_repl;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_cust_qna_repl',
   'cust_qna_seq, ord_seq, contents1, contents2, create_id, create_dt, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_CUST_QNA_REPL.csv', 'eu-central-1')
);

-- 84 
TRUNCATE TABLE wems.tb_opr_dnld;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_dnld',
   'dnld_seq, product_cd, product_model_cd, cntry_cd, dnld_type_cd, file_name, file_path, disp_name, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_DNLD.csv', 'eu-central-1')
);

-- 4,233 -> 4,677 -> 4,752
TRUNCATE TABLE wems.tb_opr_ess_config;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_ess_config',
   'device_id, device_type_cd, pv_max_pwr1, pv_max_pwr2, feed_in_limit, max_inverter_pw_cd, basicmode_cd, smtr_tp_cd, smtr_modl_cd, smtr_pulse_cnt, create_id, create_dt, grid_max_volt, grid_min_volt, grid_max_freq, grid_min_freq, rcp_p_enable, rcp_q_fix_enable, rcp_p_excited, acp_pf_enable_cd, acp_pu_enable_cd, rcp_q_fix_excited, rcp_f_enable_cd, rcp_f_excited_cd, acp_set_point, acp_pe_max, rcp_set_cos_pi, dci_variable, dci_trip_level, rcp_set_x1, rcp_set_y1, rcp_set_x2, rcp_set_y2, rcp_set_x3, rcp_set_y3, rcp_set_x4, rcp_set_y4, rcp_q_fix_data, algo_flag, algo_flag2, pemmode, backupmode, islandingmode, feedinmode, relay1attachlevel, relay1detachlevel, relay2attachlevel, relay2detachlevel, relay3attachlevel, relay3detachlevel, relay4attachlevel, relay4detachlevel, faultlockmode, faultlockvalue, faultlocktimelevel, smetertype, smeterd0id, apsetpointmode, apsetpointvalue, apfreqmode, apfreqx1, apfreqy1, apfreqx2, apfreqy2, apfreqx3, apfreqy3, apfreqx4, apfreqy4, apvoltagemode, apvoltagex1, apvoltagey1, apvoltagex2, apvoltagey2, apvoltagex3, apvoltagey3, apvoltagex4, apvoltagey4, rpsetpointmode, rpsetpointexcited, rpsetpointvalue, rpcospipmode, rpcospipexcited, rpcospipx1, rpcospipy1, rpcospipx2, rpcospipy2, rpcospipx3, rpcospipy3, rpqsetpointmode, rpqsetpointvalue, rpqumode, rpqux1, rpquy1, rpqux2, rpquy2, rpqux3, rpquy3, rpqux4, rpquy4, reserved1, reserved2, reserved3, reserved4, reserved5, reserved6, reserved7, reserved8, reserved9, reserved10, reserved11, reserved12, reserved13, reserved14, reserved15, reserved16, reserved17, reserved18, reserved19, reserved20, reserved21, reserved22, reserved23, reserved24, reserved25, reserved26, reserved27, reserved28, reserved29, reserved30, reserved31, reserved32, reserved33, reserved34, reserved35, reserved36, reserved37, reserved38, reserved39, reserved40, reserved41, reserved42, reserved43, reserved44, reserved45, reserved46, reserved47, reserved48, reserved49, reserved50',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_ESS_CONFIG.csv', 'eu-central-1')
);

-- 2,554 -> 2,230 -> 2,131
TRUNCATE TABLE wems.tb_opr_ess_config_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_ess_config_hist',
   'device_id, device_type_cd, pv_max_pwr1, pv_max_pwr2, feed_in_limit, max_inverter_pw_cd, basicmode_cd, smtr_tp_cd, smtr_modl_cd, smtr_pulse_cnt, create_id, create_dt, grid_max_volt, grid_min_volt, grid_max_freq, grid_min_freq, rcp_p_enable, rcp_q_fix_enable, rcp_p_excited, acp_pf_enable_cd, acp_pu_enable_cd, rcp_q_fix_excited, rcp_f_enable_cd, rcp_f_excited_cd, acp_set_point, acp_pe_max, rcp_set_cos_pi, dci_variable, dci_trip_level, rcp_set_x1, rcp_set_y1, rcp_set_x2, rcp_set_y2, rcp_set_x3, rcp_set_y3, rcp_set_x4, rcp_set_y4, rcp_q_fix_data, algo_flag, algo_flag2, pemmode, backupmode, islandingmode, feedinmode, relay1attachlevel, relay1detachlevel, relay2attachlevel, relay2detachlevel, relay3attachlevel, relay3detachlevel, relay4attachlevel, relay4detachlevel, faultlockmode, faultlockvalue, faultlocktimelevel, smetertype, smeterd0id, apsetpointmode, apsetpointvalue, apfreqmode, apfreqx1, apfreqy1, apfreqx2, apfreqy2, apfreqx3, apfreqy3, apfreqx4, apfreqy4, apvoltagemode, apvoltagex1, apvoltagey1, apvoltagex2, apvoltagey2, apvoltagex3, apvoltagey3, apvoltagex4, apvoltagey4, rpsetpointmode, rpsetpointexcited, rpsetpointvalue, rpcospipmode, rpcospipexcited, rpcospipx1, rpcospipy1, rpcospipx2, rpcospipy2, rpcospipx3, rpcospipy3, rpqsetpointmode, rpqsetpointvalue, rpqumode, rpqux1, rpquy1, rpqux2, rpquy2, rpqux3, rpquy3, rpqux4, rpquy4, reserved1, reserved2, reserved3, reserved4, reserved5, reserved6, reserved7, reserved8, reserved9, reserved10, reserved11, reserved12, reserved13, reserved14, reserved15, reserved16, reserved17, reserved18, reserved19, reserved20, reserved21, reserved22, reserved23, reserved24, reserved25, reserved26, reserved27, reserved28, reserved29, reserved30, reserved31, reserved32, reserved33, reserved34, reserved35, reserved36, reserved37, reserved38, reserved39, reserved40, reserved41, reserved42, reserved43, reserved44, reserved45, reserved46, reserved47, reserved48, reserved49, reserved50',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_ESS_CONFIG_HIST.csv', 'eu-central-1')
);

-- 3,990 -> 4,416 -> 4,496
TRUNCATE TABLE wems.tb_opr_ess_state;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_ess_state',
   'device_id, device_type_cd, colec_dt, oper_stus_cd, pv_pw, pv_pw_h, pv_pw_price, pv_pw_co2, pv_stus_cd, cons_pw, cons_pw_h, cons_pw_price, cons_pw_co2, bt_pw, bt_chrg_pw_h, bt_chrg_pw_price, bt_chrg_pw_co2, bt_dchrg_pw_h, bt_dchrg_pw_price, bt_dchrg_pw_co2, bt_soc, bt_soh, bt_stus_cd, grid_pw, grid_ob_pw_h, grid_ob_pw_price, grid_ob_pw_co2, grid_tr_pw_h, grid_tr_pw_price, grid_tr_pw_co2, grid_stus_cd, pcs_pw, pcs_fd_pw_h, pcs_pch_pw_h, ems_opmode, pcs_opmode1, pcs_opmode2, pcs_tgt_pw, feed_in_limit, create_dt, max_inverter_pw_cd, rfsh_prd, outlet_pw, outlet_pw_h, outlet_pw_price, outlet_pw_co2, pwr_range_cd_ess_grid, pwr_range_cd_grid_ess, pwr_range_cd_grid_load, pwr_range_cd_ess_load, pwr_range_cd_pv_ess, pcs_comm_err_rate, smtr_comm_err_rate, m2m_comm_err_rate, pcs_flag0, pcs_flag1, pcs_flag2, pcs_opmode_cmd1, pcs_opmode_cmd2, pv_v1, pv_i1, pv_pw1, pv_v2, pv_i2, pv_pw2, inverter_v, inverter_i, inverter_pw, dc_link_v, g_rly_cnt, bat_rly_cnt, grid_to_grid_rly_cnt, bat_pchrg_rly_cnt, bms_flag0, bms_diag0, rack_v, rack_i, cell_max_v, cell_min_v, cell_avg_v, cell_max_t, cell_min_t, cell_avg_t, tray_cnt, smtr_tp_cd, smtr_pulse_cnt, smtr_modl_cd, pv_max_pwr1, pv_max_pwr2, instl_rgn, mmtr_slv_addr, basicmode_cd, sys_st_cd, bt_chrg_st_cd, bt_dchrg_st_cd, pv_st_cd, grid_st_cd, smtr_st_cd, smtr_tr_cnter, smtr_ob_cnter, dn_enable, dc_start, dc_end, nc_start, nc_end, grid_code0, grid_code1, grid_code2, pcon_bat_targetpower, grid_code3, bdc_module_code0, bdc_module_code1, bdc_module_code2, bdc_module_code3, fault5_realtime_year, fault5_realtime_month, fault5_day, fault5_hour, fault5_minute, fault5_second, fault5_datahigh, fault5_datalow, fault6_realtime_year, fault6_realtime_month, fault6_realtime_day, fault6_realtime_hour, fault6_realtime_minute, fault6_realtime_second, fault6_datahigh, fault6_datalow, fault7_realtime_year, fault7_realtime_month, fault7_realtime_day, fault7_realtime_hour, fault7_realtime_minute, fault7_realtime_second, fault7_datahigh, fault7_datalow, fault8_realtime_year, fault8_realtime_month, fault8_realtime_day, fault8_realtime_hour, fault8_realtime_minute, fault8_realtime_second, fault8_datahigh, fault8_datalow, fault9_realtime_year, fault9_realtime_month, fault9_realtime_day, fault9_realtime_hour, fault9_realtime_minute, fault9_realtime_second, fault9_datahigh, fault9_datalow',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_ESS_STATE.csv', 'eu-central-1')
);

-- 484,351 -> 746,746 -> 753,922
TRUNCATE TABLE wems.tb_opr_ess_state_tl;
DROP INDEX wems.pk_opr_ess_state_tl;
ALTER TABLE wems.tb_opr_ess_state_tl ADD CONSTRAINT pk_opr_ess_state_tl PRIMARY KEY (colec_dd, device_id, device_type_cd);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_ess_state_tl',
   'colec_dd, device_id, device_type_cd, oper_stus_cd, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_ESS_STATE_TL.csv', 'eu-central-1')
);

-- 0 
TRUNCATE TABLE wems.tb_opr_event;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_event',
   'event_seq, cntry_cd, start_tm, end_tm, event_type_cd, subject, contents1, contents2, pctr_file, pctr_file_path, pctr_file_disp_name, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_EVENT.csv', 'eu-central-1')
);

-- 341 -> 335
TRUNCATE TABLE wems.tb_opr_faq;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_faq',
   'faq_seq, cntry_cd, faq_type_cd, subject, contents1, contents2, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_FAQ.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.tb_opr_instl_case;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_instl_case',
   'instl_case_seq, cntry_cd, apply_txt, capacity_txt, oper_start_ym, subject, pctr_file, pctr_file_path, pctr_file_disp_name, create_id, create_dt, update_id, update_dt, done_flag',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_INSTL_CASE.csv', 'eu-central-1')
);

-- 685 -> 798 -> 813
TRUNCATE TABLE wems.tb_opr_instlr_ess_state;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_instlr_ess_state',
   'user_id, colec_dd, tot_cnt, nml_cnt, nml_per, warn_cnt, warn_per, err_cnt, err_per, no_sig_cnt, no_sig_per, std_cnt, std_per, rct_instl_cnt, create_dt, update_dt, esm_cnt, esm_per',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_INSTLR_ESS_STATE.csv', 'eu-central-1')
);

-- 41
TRUNCATE TABLE wems.tb_opr_mng_file;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_mng_file',
   'file_type_cd, lang_cd, file_name, file_path, create_id, create_dt, update_id, update_dt, ord_seq, use_flag, disp_name',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_MNG_FILE.csv', 'eu-central-1')
);

-- 4 -> 6
TRUNCATE TABLE wems.tb_opr_mtnc;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_mtnc',
   'mtnc_seq, device_id, device_type_cd, mtnc_type_cd, mtnc_dt, subject, contents1, contents2, create_id, create_dt, update_dt, mtnc_stus_cd',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_MTNC.csv', 'eu-central-1')
);

-- 0 
TRUNCATE TABLE wems.tb_opr_mtnc_file;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_mtnc_file',
   'mtnc_seq, ord_seq, file_name, file_path, disp_name, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_MTNC_FILE.csv', 'eu-central-1')
);

-- 1 -> 0
TRUNCATE TABLE wems.tb_opr_mv;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_mv',
   'mv_seq, mv_url, pv_img_url, subject, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_MV.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.tb_opr_news;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_news',
   'news_seq, cntry_cd, subject, contents1, contents2, pctr_file, pctr_file_path, pctr_file_disp_name, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_NEWS.csv', 'eu-central-1')
);

-- 0 -> 274 -> 276
TRUNCATE TABLE wems.tb_opr_oper_ctrl;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_oper_ctrl',
   'grp_id, ems_oper_mode_cd, era_mode_cd, erm_mode_1_cd, erm_mode_2_cd, erm_mode_3_cd, feedin, target_soc, target_pw, algo_flag, acpsetpoint, acppemax, rcpsetcospi, dcivariable, dcitriplevel, rcpsetx1, rcpsety1, rcpsetx2, rcpsety2, rcpsetx3, rcpsety3, rcpsetx4, rcpsety4, create_id, create_dt, update_id, update_dt, inverter_pw, basicmode_cd, reversesshport, apcdrop, lockin, lockout, batteryapc, algo_flag2, rcpqfixdata',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_OPER_CTRL.csv', 'eu-central-1')
);

-- 0 -> 186,922 -> 189,716
TRUNCATE TABLE wems.tb_opr_oper_ctrl_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_oper_ctrl_hist',
   'req_id, device_id, device_type_cd, grp_id, grp_nm, ems_oper_mode_cd, era_mode_cd, erm_mode_1_cd, erm_mode_2_cd, erm_mode_3_cd, feedin, target_soc, target_pw, algo_flag, acpsetpoint, acppemax, rcpsetcospi, dcivariable, dcitriplevel, rcpsetx1, rcpsety1, rcpsetx2, rcpsety2, rcpsetx3, rcpsety3, rcpsetx4, rcpsety4, response_err_cd, create_id, create_dt, inverter_pw, basicmode_cd, cmd_req_status_cd, reversesshport, apcdrop, lockin, lockout, batteryapc, algo_flag2, rcpqfixdata',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_OPER_CTRL_HIST.csv', 'eu-central-1')
);

-- 0 -> 686 -> 638
TRUNCATE TABLE wems.tb_opr_oper_logfile_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_oper_logfile_hist',
   'req_id, device_id, device_type_cd, logfile_type_cd, req_tm, res_tm, cmd_req_status_cd, logfile_nm, create_dt, ip',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_OPER_LOGFILE_HIST.csv', 'eu-central-1')
);

-- 5 
TRUNCATE TABLE wems.tb_opr_pvcalc_ess_info;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_pvcalc_ess_info',
   'ess_model_nm, ess_img_file_nm, ess_pv_r_v, ess_power, ess_voc, ess_vmppt, ess_imp, use_flag, create_id, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_PVCALC_ESS_INFO.csv', 'eu-central-1')
);

-- 0 -> 466 -> 400
TRUNCATE TABLE wems.tb_opr_pvcalc_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_pvcalc_hist',
   'seq, user_id, ess_model_nm, pv_brand_nm, pv_prod_model_nm, pv_pmax, pv_voc, pv_vmpp, pv_isc, min_series_mp, min_series_voc, min_series_vmppt, max_parallel_d, max_series_a, max_c, basic_series_aa, user_parallel, user_series, pmax_value, pmax_result, voc_value, voc_result, vmppt_value, vmppt_result, isc_value, isc_result, done_flag, use_flag, create_id, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_PVCALC_HIST.csv', 'eu-central-1')
);

-- 30
TRUNCATE TABLE wems.tb_opr_pvcalc_pv_info;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_pvcalc_pv_info',
   'pv_brand_nm, pv_prod_model_nm, pv_pmax, pv_voc, pv_vmpp, pv_isc, use_flag, create_id, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_PVCALC_PV_INFO.csv', 'eu-central-1')
);


---------------------------------------------------------------------------------
-- SQL Error [23505]: ERROR: duplicate key value violates unique constraint "pk_opr_terms_hist"
--  Detail: Key (user_id, create_dt)=(absolutesolar, 2016-11-08 07:44:42+09) already exists.
-- 8,754 -> 

--11,978
TRUNCATE TABLE wems.tb_opr_terms_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_terms_hist',
   'user_id, terms_ver, terms_info, bld_area_val, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_TERMS_HIST.csv', 'eu-central-1')
);

-- 중복 데이타 조회 후 수동 삭제
SELECT *
FROM (
   SELECT *, COUNT(*) OVER(PARTITION BY USER_ID, CREATE_DT) AS CNT 
   FROM wems.TB_OPR_TERMS_HIST 
) A
WHERE CNT > 1;

ALTER TABLE wems.tb_opr_terms_hist ADD CONSTRAINT pk_opr_terms_hist PRIMARY KEY (user_id, create_dt);
---------------------------------------------------------------------------------

-- 597 -> 661 -> 674
TRUNCATE TABLE wems.tb_opr_voc;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_voc',
   'voc_seq, device_id, device_type_cd, voc_type_cd, subject, contents1, contents2, repl_flag, create_id, create_dt, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_VOC.csv', 'eu-central-1')
);

-- 0 
TRUNCATE TABLE wems.tb_opr_voc_file;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_voc_file',
   'voc_seq, ord_seq, file_name, file_path, disp_name, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_VOC_FILE.csv', 'eu-central-1')
);

-- 639 -> 691
TRUNCATE TABLE wems.tb_opr_voc_repl;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_voc_repl',
   'voc_seq, ord_seq, contents1, contents2, create_id, create_dt, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_VOC_REPL.csv', 'eu-central-1')
);

-- 10
TRUNCATE TABLE wems.tb_opr_was_state;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_was_state',
   'id, service_name, ipaddr, port, url, status, modify_date, create_date',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_WAS_STATE.csv', 'eu-central-1')
);

------------------------------------------------------------------------------------------

-- 84
TRUNCATE TABLE wems.tb_plf_alarm_code;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_alarm_code',
   'alarm_type_cd, alarm_cd, device_type_cd, alarm_desc, mapp_seq',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_ALARM_CODE.csv', 'eu-central-1')
);

-- 10
TRUNCATE TABLE wems.tb_plf_carb_emi_quity;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_carb_emi_quity',
   'cntry_cd, apply_year, carb_emi_quity, use_flag, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_CARB_EMI_QUITY.csv', 'eu-central-1')
);

-- 503 -> 1,469
TRUNCATE TABLE wems.tb_plf_code_desc_trans;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_code_desc_trans',
   'grp_cd, code, lang_cd, desc_trans',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_CODE_DESC_TRANS.csv', 'eu-central-1')
);

-- 88 
TRUNCATE TABLE wems.tb_plf_code_grp;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_code_grp',
   'grp_cd, grp_nm, remark, table_nm',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_CODE_GRP.csv', 'eu-central-1')
);

-- 465
TRUNCATE TABLE wems.tb_plf_code_info;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_code_info',
   'grp_cd, code, code_nm, ref_1, ref_2, ref_3, ref_4, ref_5, cd_desc, ord_seq, ref_6',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_CODE_INFO.csv', 'eu-central-1')
);

-- 581 
TRUNCATE TABLE wems.tb_plf_daq_type;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_daq_type',
   'daq_type_cd, value_ver, daq_type_nm, daq_type_desc, min_value, max_value, value_desc, unit_type, value_nm, value_ord, device_type_cd',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_DAQ_TYPE.csv', 'eu-central-1')
);

-- 10,036 -> 12,172 -> 12,266
TRUNCATE TABLE wems.tb_plf_device_fw_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_device_fw_hist',
   'req_id, device_id, device_type_cd, grp_id, ems_update_err_cd, pcs_update_err_cd, bms_update_err_cd, create_id, create_dt, update_id, update_dt, grp_upt_seq, fwi_upt_seq, cmd_req_status_cd',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_DEVICE_FW_HIST.csv', 'eu-central-1')
);

-- 593
TRUNCATE TABLE wems.tb_plf_elpw_prod;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_elpw_prod',
   'cntry_cd, elpw_conm, elpw_prod_cd, elpw_prod_nm, remark, create_dt, create_id, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_ELPW_PROD.csv', 'eu-central-1')
);

-- 72 -> 71 -> 75
TRUNCATE TABLE wems.tb_plf_firmware_info;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_firmware_info',
   'grp_id, device_type_cd, product_model_nm, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_fw_url, pcs_fw_url, bms_fw_url, ems_fw_size, pcs_fw_size, bms_fw_size, create_id, create_dt, update_id, update_dt, grp_upt_seq, fwi_upt_seq, stable_ver_flag',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_FIRMWARE_INFO.csv', 'eu-central-1')
);

-- 2,123 -> 2,240 -> 2,257
TRUNCATE TABLE wems.tb_plf_firmware_info_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_firmware_info_hist',
   'grp_id, grp_upt_seq, fwi_upt_seq, device_type_cd, product_model_nm, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_fw_url, pcs_fw_url, bms_fw_url, ems_fw_size, pcs_fw_size, bms_fw_size, create_id, create_dt, update_id, update_dt, stable_ver_flag',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_FIRMWARE_INFO_HIST.csv', 'eu-central-1')
);

-- 41
TRUNCATE TABLE wems.tb_plf_menu;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_menu',
   'menu_id, prn_menu_id, menu_nm, menu_type_cd, menu_desc, menu_url, menu_param, use_flag, popup_flag, ord_seq',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_MENU.csv', 'eu-central-1')
);

-- 62
TRUNCATE TABLE wems.tb_plf_menu_auth;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_menu_auth',
   'auth_type_cd, menu_id, use_flag',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_MENU_AUTH.csv', 'eu-central-1')
);

-- 0 -> 17
TRUNCATE TABLE wems.tb_plf_notice;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_notice',
   'noti_seq, cntry_cd, noti_type_cd, txt_title, txt_desc, txt_file, create_id, create_dt, update_id, update_dt, popup_flag, popup_start_tm, popup_end_tm, popup_tp_cd',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_NOTICE.csv', 'eu-central-1')
);

-- 100
TRUNCATE TABLE wems.tb_plf_num_list;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_num_list',
   'num, char_num, lpad_num',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_NUM_LIST.csv', 'eu-central-1')
);

-- 17 -> 26 -> 27
TRUNCATE TABLE wems.tb_plf_popup;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_popup',
   'popup_seq, cntry_cd, txt_title, file_name, file_path, disp_name, popup_start_tm, popup_end_tm, active_flag, create_id, create_dt, update_id, update_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_POPUP.csv', 'eu-central-1')
);

-- 한솔에서 전달 받은 백업 파일을 사용하지 말고, postgresql 용으로 미리 만들어 놓은 파일을 사용 (108 개)
TRUNCATE TABLE wems.tb_plf_tbl_meta;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_tbl_meta',
   'sub_area, table_nm, table_type, arcv_flag, arcv_period, fst_load_dt, load_type, partition_type, std_col_nm, std_col_type, del_flag, del_cycle, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_TBL_META.csv', 'eu-central-1')
);


-------------------------------------------------------------------------------------------------
-- 0
TRUNCATE TABLE wems.tb_raw_curr_weather;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_curr_weather',
   'city_cd, colec_dt, latitude, longitude, location_nm, country, day_type, temperature, temperature_max, temperature_min, symbol_var, symbol_cd, symbol_name, symbol_num, clouds_all, clouds_value, precipication_mode, precipication, wind_speed_mps, wind_speed_name, wind_direction_cd, wind_direction_deg, wind_direction_name, humidity, pressure, sunrise_dt, sunset_dt, sunrise_idx, sunset_idx, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_CURR_WEATHER.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.tb_raw_curr_weather_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_curr_weather_hist',
   'city_cd, colec_dt, latitude, longitude, location_nm, country, day_type, temperature, temperature_max, temperature_min, symbol_var, symbol_cd, symbol_name, symbol_num, clouds_all, clouds_value, precipication_mode, precipication, wind_speed_mps, wind_speed_name, wind_direction_cd, wind_direction_deg, wind_direction_name, humidity, pressure, sunrise_dt, sunset_dt, sunrise_idx, sunset_idx, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_CURR_WEATHER_HIST.csv', 'eu-central-1')
);

-- 1,232 -> 3,697,726
TRUNCATE TABLE wems.tb_raw_login;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_2.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_3.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_4.csv', 'eu-central-1')
);

SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_5.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_6.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_7.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_8.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_9.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_10.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_11.csv', 'eu-central-1')
);

-- 시작
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_12.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_13.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_14.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_15.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_16.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_17.csv', 'eu-central-1')
);
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_login',
   'device_id, create_tm, product_model_nm, password, ems_model_nm, pcs_model_nm, bms_model_nm, ems_ver, pcs_ver, bms_ver, ems_update_dt, ems_update_err_cd, pcs_update_dt, pcs_update_err_cd, bms_update_dt, bms_update_err_cd, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ems_tm, data_seq, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_LOGIN_18.csv', 'eu-central-1')
);


-- 0
TRUNCATE TABLE wems.tb_raw_rac_info;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_rac_info',
   'device_id, colec_dt, done_flag, col0, col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16, col17, col18, col19, col20, col21, col22, col23, col24, col25, col26, col27, col28, col29, col30, col31, col32, col33, col34, col35, col36, col37, col38, col39, col40, col41, col42, col43, col44, col45, col46, col47, col48, col49, col50, col51, col52, col53, col54, col55, col56, col57, col58, col59, col60, col61, col62, col63, col64, col65, col66, col67, col68, col69, col70, col71, col72, col73, col74, col75, rack_id, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_RAC_INFO.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.tb_raw_weather;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_weather',
   'city_cd, latitude, longitude, location_nm, location_type, country, timezone, time_to, time_from, day_type, temperature, temperature_max, temperature_min, symbol_var, symbol_cd, symbol_name, symbol_num, clouds_all, clouds_value, precipication_prob, precipication, wind_speed_mps, wind_speed_name, wind_direction_cd, wind_direction_deg, wind_direction_name, humidity, pressure, sunrise_dt, sunset_dt, sunrise_idx, sunset_idx, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_WEATHER.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.tb_raw_weather_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_weather_hist',
   'city_cd, latitude, longitude, location_nm, location_type, country, timezone, time_to, time_from, day_type, temperature, temperature_max, temperature_min, symbol_var, symbol_cd, symbol_name, symbol_num, clouds_all, clouds_value, precipication_prob, precipication, wind_speed_mps, wind_speed_name, wind_direction_cd, wind_direction_deg, wind_direction_name, humidity, pressure, sunrise_dt, sunset_dt, sunrise_idx, sunset_idx, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_WEATHER_HIST.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.tb_raw_weather_reqhist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_raw_weather_reqhist',
   'device_id, create_tm, response_tm, err_cd',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_RAW_WEATHER_REQHIST.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.tb_sys_exec_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_sys_exec_hist',
   'exec_seq, menu_id, func_id, exec_type_cd, login_seq, user_id, create_dt, exec_desc',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_SYS_EXEC_HIST.csv', 'eu-central-1')
);

-- 2 -> 162,287 -> 163,508
TRUNCATE TABLE wems.tb_sys_login_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_sys_login_hist',
   'login_seq, user_id, client_type_cd, login_dt, logout_dt, client_ip, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_SYS_LOGIN_HIST.csv', 'eu-central-1')
);

-- 0 -> 32,569 -> 32,601
TRUNCATE TABLE wems.tb_sys_sec_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_sys_sec_hist',
   'login_seq, user_id, client_type_cd, sec_type_cd, client_ip, ref_url, req_url, create_dt, sec_desc',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_SYS_SEC_HIST.csv', 'eu-central-1')
);

-- 145,015 -> 1,284
TRUNCATE TABLE wems.tb_sys_system_hist;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_sys_system_hist',
   'hist_seq, sys_type_cd, src_loc, err_cd, create_dt, err_desc',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_SYS_SYSTEM_HIST.csv', 'eu-central-1')
);

------------------------ 통계, RAW -------------------------------------------

-- 184,023,147   파일 없음
TRUNCATE TABLE wems.tb_opr_ess_state_hist;
DROP INDEX wems.pk_opr_ess_state_hist;
ALTER TABLE wems.tb_opr_ess_state_hist ADD CONSTRAINT pk_opr_ess_state_hist PRIMARY KEY (colec_dt, device_id, device_type_cd);

SELECT aws_s3.table_import_from_s3(
      'wems.tb_opr_ess_state_hist',
   'device_id, device_type_cd, colec_dt, oper_stus_cd, pv_pw, pv_pw_h, pv_pw_price, pv_pw_co2, pv_stus_cd, cons_pw, cons_pw_h, cons_pw_price, cons_pw_co2, bt_pw, bt_chrg_pw_h, bt_chrg_pw_price, bt_chrg_pw_co2, bt_dchrg_pw_h, bt_dchrg_pw_price, bt_dchrg_pw_co2, bt_soc, bt_soh, bt_stus_cd, grid_pw, grid_ob_pw_h, grid_ob_pw_price, grid_ob_pw_co2, grid_tr_pw_h, grid_tr_pw_price, grid_tr_pw_co2, grid_stus_cd, pcs_pw, pcs_fd_pw_h, pcs_pch_pw_h, ems_opmode, pcs_opmode1, pcs_opmode2, pcs_tgt_pw, feed_in_limit, create_dt, max_inverter_pw_cd, outlet_pw, outlet_pw_h, outlet_pw_price, outlet_pw_co2, pcs_comm_err_rate, smtr_comm_err_rate, m2m_comm_err_rate, pcs_flag0, pcs_flag1, pcs_flag2, pcs_opmode_cmd1, pcs_opmode_cmd2, pv_v1, pv_i1, pv_pw1, pv_v2, pv_i2, pv_pw2, inverter_v, inverter_i, inverter_pw, dc_link_v, g_rly_cnt, bat_rly_cnt, grid_to_grid_rly_cnt, bat_pchrg_rly_cnt, bms_flag0, bms_diag0, rack_v, rack_i, cell_max_v, cell_min_v, cell_avg_v, cell_max_t, cell_min_t, cell_avg_t, tray_cnt, smtr_tp_cd, smtr_pulse_cnt, smtr_modl_cd, pv_max_pwr1, pv_max_pwr2, instl_rgn, mmtr_slv_addr, basicmode_cd, sys_st_cd, bt_chrg_st_cd, bt_dchrg_st_cd, pv_st_cd, grid_st_cd, smtr_st_cd, smtr_tr_cnter, smtr_ob_cnter, grid_code0, grid_code1, grid_code2, pcon_bat_targetpower, grid_code3, bdc_module_code0, bdc_module_code1, bdc_module_code2, bdc_module_code3, fault5_realtime_year, fault5_realtime_month, fault5_day, fault5_hour, fault5_minute, fault5_second, fault5_datahigh, fault5_datalow, fault6_realtime_year, fault6_realtime_month, fault6_realtime_day, fault6_realtime_hour, fault6_realtime_minute, fault6_realtime_second, fault6_datahigh, fault6_datalow, fault7_realtime_year, fault7_realtime_month, fault7_realtime_day, fault7_realtime_hour, fault7_realtime_minute, fault7_realtime_second, fault7_datahigh, fault7_datalow, fault8_realtime_year, fault8_realtime_month, fault8_realtime_day, fault8_realtime_hour, fault8_realtime_minute, fault8_realtime_second, fault8_datahigh, fault8_datalow, fault9_realtime_year, fault9_realtime_month, fault9_realtime_day, fault9_realtime_hour, fault9_realtime_minute, fault9_realtime_second, fault9_datahigh, fault9_datalow',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_OPR_ESS_STATE_HIST.csv', 'eu-central-1')
);



-- 1
TRUNCATE TABLE wems.dc_cmd_session_ident;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_cmd_session_ident',
   'id, maxcmdqueue, description, priority, logmode, ackmode, max_session_cnt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_CMD_SESSION_IDENT.csv', 'eu-central-1')
);

-- 8
TRUNCATE TABLE wems.dc_cnf_connection;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_cnf_connection',
   'sequence, connectorid, agentequipid, protocoltype, status, commandflag, description, name, agentportno, porttype, userid, password',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_CNF_CONNECTION.csv', 'eu-central-1')
);

-- 8 
TRUNCATE TABLE wems.dc_cnf_connector;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_cnf_connector',
   'id, gatewayid, ruleid, status, description, junctiontype, cmdresponsetype, logcycle, create_date, modify_date, last_action_date, last_action, last_action_desc, name',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_CNF_CONNECTOR.csv', 'eu-central-1')
);

-- 0 
TRUNCATE TABLE wems.dc_cnf_event_consumer;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_cnf_event_consumer',
   'id, dbuserid, dbpassword, dbtns, hostname, timemode, listenport, status, logmode, ipaddress, bypasslistenport, loadinginterval, handlermode, targetinfo, logcycle, runmode, managerid, subopermode',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_CNF_EVENT_CONSUMER.csv', 'eu-central-1')
);

-- 1
TRUNCATE TABLE wems.dc_cnf_manager;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_cnf_manager',
   'id, ip, status, sockmgrport, description, name',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_CNF_MANAGER.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.dc_cnf_mapping;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_cnf_mapping',
   'agentequipid, agentportno, bscno, aciiheader',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_CNF_MAPPING.csv', 'eu-central-1')
);

-- 1
TRUNCATE TABLE wems.dc_cnf_server;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_cnf_server',
   'ip, guiport, cmdport, netfinderport, logport, netfinderip, sockmgrport',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_CNF_SERVER.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.dc_cnf_sub_proc;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_cnf_sub_proc',
   'id, id_str, parent, parentid, ipaddress, hostname, status, logcycle, description, bin_name, args',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_CNF_SUB_PROC.csv', 'eu-central-1')
);

-- 0 
TRUNCATE TABLE wems.dc_code_porttype;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_code_porttype',
   'porttype, description, extflag, section, cmdflag',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_CODE_PORTTYPE.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.dc_code_protocoltype;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_code_protocoltype',
   'protocoltype, description, need_portno, need_password, need_userid, extflag, porttype_able, need_ip',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_CODE_PROTOCOLTYPE.csv', 'eu-central-1')
);

-- 0 
TRUNCATE TABLE wems.dc_rul_identification;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_rul_identification',
   'id, ruleid, parentid, parsingruleid, idstring, outputflag, parsingflag, rawdataflag, update_user, update_date, xmlflag, editoruseflag, default_ident_flag',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_RUL_IDENTIFICATION.csv', 'eu-central-1')
);

-- 0 
TRUNCATE TABLE wems.dc_rul_parsing_template;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_rul_parsing_template',
   'id, parsingtemplategrpid, eventid, sequence, parsettype, headersize, datasize, skipline, nextflag, equipflag, consumers, ruleid, udpate_user, update_date, xmlkeyelementtag, xmlrootelementtag, xmlrootelementcdatatag, xmlrootelementpcdata, xmlrootpcdatakind, atomresulthideflag, eofhideflag',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_RUL_PARSING_TEMPLATE.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.dc_rul_parsing_template_grp;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_rul_parsing_template_grp',
   'id, identificationid, ruleid, update_user, update_date',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_RUL_PARSING_TEMPLATE_GRP.csv', 'eu-central-1')
);

-- 0 
TRUNCATE TABLE wems.dc_rul_rawdata;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_rul_rawdata',
   'ruleid, id, rawdata',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_RUL_RAWDATA.csv', 'eu-central-1')
);

-- 0
TRUNCATE TABLE wems.dc_rul_rule;
SELECT aws_s3.table_import_from_s3(
      'wems.dc_rul_rule',
   'id, generationcode, vendercode, version, identificationtype, linedecimal, update_user, update_date',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/DC_RUL_RULE.csv', 'eu-central-1')
);


/*------------------------------------------------------------------------------
 한솔DB 데이타 이관 후에 아래의 변경된 테이블에 대한 처리를 해야 합니다.

1. TB_OPR_ALARM 
2. TB_BAS_MODEL_ALARM_CODE_MAP 
3. TB_PLF_CODE_DESC_TRANS
------------------------------------------------------------------------------*/

alter table wems.TB_OPR_ALARM add column send_dt timestamp;
alter table wems.TB_BAS_MODEL_ALARM_CODE_MAP add column repeat_flag varchar(1);
alter table wems.TB_BAS_MODEL_ALARM_CODE_MAP add column repeat_interval varchar(4);
delete from wems.tb_plf_code_desc_trans;
alter table wems.tb_plf_code_desc_trans add column model_type varchar(10);
alter table wems.tb_plf_code_desc_trans drop  CONSTRAINT  pk_plf_code_desc_trans;
alter table wems.tb_plf_code_desc_trans add CONSTRAINT pk_plf_code_desc_trans PRIMARY KEY (grp_cd, code, lang_cd, model_type);  


/*------------------------------------------------------------------------------
 아래의 4개 테이블은 한솔에서 받은 CSV를 사용하지 않고 
 S3의 rdsmigration-bucket 폴더의 파일로 import해야 함

1. BATCH_INF 
2. TB_BAS_TIMEZONE
3. TB_PLF_CODE_DESC_TRANS
4. TB_PLF_TBL_META
------------------------------------------------------------------------------*/
TRUNCATE TABLE wems.batch_inf;
SELECT aws_s3.table_import_from_s3(
      'wems.batch_inf',
   'sys_nm, sp_nm, cycl_cd, cycl_time, exe_scdul, ord, use_yn, sp_desc, cretr, cret_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/BATCH_INF.csv', 'eu-central-1')
);


TRUNCATE TABLE wems.tb_bas_timezone;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_bas_timezone',
   'timezone_id, utc_offset, dst_start_exp, dst_end_exp, dst_use_flag, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_BAS_TIMEZONE.csv', 'eu-central-1')
);

TRUNCATE TABLE wems.tb_plf_code_desc_trans;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_code_desc_trans',
   'grp_cd, code, lang_cd, desc_trans, model_type',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_CODE_DESC_TRANS.csv', 'eu-central-1')
);


TRUNCATE TABLE wems.tb_plf_tbl_meta;
SELECT aws_s3.table_import_from_s3(
      'wems.tb_plf_tbl_meta',
   'sub_area, table_nm, table_type, arcv_flag, arcv_period, fst_load_dt, load_type, partition_type, std_col_nm, std_col_type, del_flag, del_cycle, create_dt',
      'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'hansol_meta/TB_PLF_TBL_META.csv', 'eu-central-1')
);

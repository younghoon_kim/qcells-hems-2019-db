
-- 2020.04.02 박동환 작성
--
-- TOC entry 1118 (class 1255 OID 1895817)
-- Name: bin2dec(character); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.bin2dec(binval character) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
 /******************************************************************************
   NAME:       BIN2DEC
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   2.0        2019-10-08     이재형                  postgresql 변환    
   
   NOTES:   Binary(2진수) to Decimal(10진수)

    Input : 

    Output :

 ******************************************************************************/
DECLARE
    i numeric;
    digits numeric;
    result numeric := 0;
    current_digit CHARACTER(1);
    current_digit_dec numeric;
BEGIN
    digits := LENGTH(binval);

    FOR i IN 1..digits LOOP
        current_digit := substr(binval, i, 1);
        current_digit_dec := current_digit::numeric;
        result := (result * 2) + current_digit_dec;
    END LOOP;
    RETURN result;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$$;


ALTER FUNCTION wems.bin2dec(binval character) OWNER TO wems;

--
-- TOC entry 1085 (class 1255 OID 1895818)
-- Name: dec2bin(numeric); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.dec2bin(n numeric) RETURNS text
    LANGUAGE plpgsql
    AS $$
 /******************************************************************************
   NAME:       DEC2BIN
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   2.0        2019-10-08     이재형                  postgresql 변환    
   
   NOTES:   Decimal(10진수) to Binary(2진수)

    Input : 

    Output :

 ******************************************************************************/
DECLARE
    binval CHARACTER VARYING(64);
    n2 numeric := n;
BEGIN
    IF n = 0 THEN
        binval := n::TEXT;
    ELSE
        WHILE (n2 > 0) LOOP
            binval := CONCAT_WS('', MOD(n2, 2), binval);
            n2 := TRUNC((n2 / 2::NUMERIC)::NUMERIC);
        END LOOP;
    END IF;
    RETURN binval;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$$;


ALTER FUNCTION wems.dec2bin(n numeric) OWNER TO wems;

--
-- TOC entry 1109 (class 1255 OID 1895819)
-- Name: dec2hex(numeric); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.dec2hex(n numeric) RETURNS text
    LANGUAGE plpgsql
    AS $$
 /******************************************************************************
   NAME:       DEC2HEX
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   2.0        2019-10-08     이재형                  postgresql 변환    
   
   NOTES:   Decimal(10진수) to hex(16진수)

    Input : 

    Output :

 ******************************************************************************/
DECLARE
    hexval CHARACTER VARYING(64);
    n2 numeric := n;
    digit numeric;
    hexdigit CHARACTER(1);
BEGIN
    WHILE (n2 > 0) LOOP
        digit := MOD(n2, 16);

        IF digit > 9 THEN
            hexdigit := CHR(TRUNC((ASCII('A') + digit - 10)::NUMERIC)::INTEGER);
        ELSE
            hexdigit := digit::char;
        END IF;
        hexval := CONCAT_WS('', hexdigit, hexval);
        n2 := TRUNC((n2 / 16::NUMERIC)::NUMERIC);
    END LOOP;
    RETURN hexval;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$$;


ALTER FUNCTION wems.dec2hex(n numeric) OWNER TO wems;

--
-- TOC entry 1086 (class 1255 OID 1895820)
-- Name: dec2oct(numeric); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.dec2oct(n numeric) RETURNS text
    LANGUAGE plpgsql
    AS $$
 /******************************************************************************
   NAME:       DEC2OCT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   2.0        2019-10-08     이재형                  postgresql 변환    
   
   NOTES:   Decimal(10진수) to oct(8진수)

    Input : 

    Output :

 ******************************************************************************/
DECLARE
    octval CHARACTER VARYING(64);
    n2 numeric := n;
BEGIN
    WHILE (n2 > 0) LOOP
        octval := CONCAT_WS('', MOD(n2, 8), octval);
        n2 := TRUNC((n2 / 8::NUMERIC)::NUMERIC);
    END LOOP;
    RETURN octval;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$$;


ALTER FUNCTION wems.dec2oct(n numeric) OWNER TO wems;

--
-- TOC entry 1119 (class 1255 OID 1895821)
-- Name: dec_varchar_sel(character varying, integer, character varying, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.dec_varchar_sel(cal_val character varying, int_val integer, sdienck character varying, table_name character varying, cal_name character varying) RETURNS character varying
    LANGUAGE plpgsql STRICT
    AS $$
 /******************************************************************************
  NAME:       DEC_VARCHAR_SEL
  PURPOSE:

  REVISIONS:
  Ver        Date           Author           Description
  ---------  ---------------  ---------------  ------------------------------------
  1.0        2019-11-22  박동환      		최초작성
  
  	NOTES:   암호해제

	USAGE:
			SELECT DEC_VARCHAR_SEL('Valent_Elio', 10, 'SDIENCK', 'TB_BAS_USER', 'user_id');

*****************************************************************************/
	DECLARE
		err_context	TEXT;
	
	BEGIN		
		IF sdienck IS NULL OR sdienck = '' THEN 
			sdienck := 'SDIENCK';
		END IF;
	
		RETURN (	SELECT FN_GET_DECRYPT(cal_val, sdienck)	);

		EXCEPTION WHEN OTHERS THEN 
			 GET STACKED DIAGNOSTICS err_context = MESSAGE_TEXT;
		RETURN err_context;			 
	END;
$$;


ALTER FUNCTION wems.dec_varchar_sel(cal_val character varying, int_val integer, sdienck character varying, table_name character varying, cal_name character varying) OWNER TO wems;

--
-- TOC entry 1120 (class 1255 OID 1895822)
-- Name: dec_varchar_sel(character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.dec_varchar_sel(cal_val character varying, int_val character varying, sdienck character varying, table_name character varying, cal_name character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
 /******************************************************************************
  NAME:       DEC_VARCHAR_SEL
  PURPOSE:

  REVISIONS:
  Ver        Date           Author           Description
  ---------  ---------------  ---------------  ------------------------------------
  1.0        2019-11-22  박동환      		최초작성
  
  	NOTES:   암호해제

	USAGE:
			SELECT DEC_VARCHAR_SEL('Valent_Elio', '10', 'SDIENCK', 'TB_BAS_USER', 'user_id');

*****************************************************************************/
	DECLARE
		err_context	TEXT;
	
	BEGIN		
		IF sdienck IS NULL OR sdienck = '' THEN 
			sdienck := 'SDIENCK';
		END IF;
	
		RETURN (	SELECT FN_GET_DECRYPT(cal_val, sdienck)	);

		EXCEPTION WHEN OTHERS THEN 
			 GET STACKED DIAGNOSTICS err_context = MESSAGE_TEXT;
		RETURN err_context;			 
	END;
$$;


ALTER FUNCTION wems.dec_varchar_sel(cal_val character varying, int_val character varying, sdienck character varying, table_name character varying, cal_name character varying) OWNER TO wems;

--
-- TOC entry 1068 (class 1255 OID 1895823)
-- Name: decode(numeric, character varying, character varying, numeric); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.decode(param numeric, con1 character varying, val1 character varying, val0 numeric) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
	DECLARE
		v_result	numeric;	
	
	BEGIN
		select CASE param WHEN  con1::numeric THEN val1::numeric ELSE val0 end into v_result;
		RETURN v_result;
	END;
$$;


ALTER FUNCTION wems.decode(param numeric, con1 character varying, val1 character varying, val0 numeric) OWNER TO wems;

--
-- TOC entry 1121 (class 1255 OID 1895824)
-- Name: decode(text, text, text, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.decode(param text, con1 text, val1 text, val0 text) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
	DECLARE
		v_result	VARCHAR(10000);	
	
	BEGIN
		select CASE param WHEN  con1 THEN val1 ELSE val0 end into v_result;
		RETURN v_result;
	END;
$$;


ALTER FUNCTION wems.decode(param text, con1 text, val1 text, val0 text) OWNER TO wems;

--
-- TOC entry 1115 (class 1255 OID 1895825)
-- Name: decode(text, integer, text, integer, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.decode(param text, con1 integer, val1 text, con2 integer, val2 text) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
	DECLARE
		v_result	VARCHAR(10000);	
	
	BEGIN
		select CASE param WHEN  con1 THEN val1 WHEN  con2 THEN val2  end into v_result;
		RETURN v_result;
	END;
$$;


ALTER FUNCTION wems.decode(param text, con1 integer, val1 text, con2 integer, val2 text) OWNER TO wems;

--
-- TOC entry 1116 (class 1255 OID 1895826)
-- Name: enc_index_varchar(character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.enc_index_varchar(cal_val character varying, sdienck character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/***************************************************************************
 * 사용법: SELECT ENC_INDEX_VARCHAR('USER_ID', 'SDIENCK');
 * 
 **************************************************************************/
DECLARE
	err_context	VARCHAR;

BEGIN
		IF sdienck IS NULL OR sdienck = '' THEN 
			sdienck := 'SDIENCK';
		END IF;
	
		RETURN (	SELECT FN_GET_ENCRYPT(cal_val, sdienck)	);
	
		EXCEPTION WHEN OTHERS THEN 
			 GET STACKED DIAGNOSTICS err_context = MESSAGE_TEXT;
		RETURN err_context;
			 
END;

$$;


ALTER FUNCTION wems.enc_index_varchar(cal_val character varying, sdienck character varying) OWNER TO wems;

--
-- TOC entry 1122 (class 1255 OID 1895827)
-- Name: enc_index_varchar_sel(character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.enc_index_varchar_sel(cal_val character varying, sdienck character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/***********************************
 * 사용법: SELECT ENC_INDEX_VARCHAR_SEL('userA', 'SDIENCK');
 * 
 ************************************/
DECLARE
	err_context	VARCHAR;

BEGIN
	IF sdienck IS NULL OR sdienck = '' THEN 
		sdienck := 'SDIENCK';
	END IF;
	
	RETURN (	SELECT FN_GET_ENCRYPT(cal_val, sdienck)	);
	
	EXCEPTION WHEN OTHERS THEN 
		 GET STACKED DIAGNOSTICS err_context = MESSAGE_TEXT;
	RETURN err_context;
			 
END;

$$;


ALTER FUNCTION wems.enc_index_varchar_sel(cal_val character varying, sdienck character varying) OWNER TO wems;

--
-- TOC entry 1123 (class 1255 OID 1895828)
-- Name: enc_varchar_ins(character varying, integer, character varying, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.enc_varchar_ins(cal_val character varying, int_val integer, sdienck character varying, table_name character varying, cal_name character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/***********************************
 * 사용법: SELECT ENC_VARCHAR_INS('Valent_Elio', 10, 'SDIENCK', 'TB_BAS_USER', 'user_id');
 * 
 ************************************/
DECLARE
	err_context	TEXT;
	
BEGIN		
	IF sdienck IS NULL OR sdienck = '' THEN 
		sdienck := 'SDIENCK';
	END IF;
	
	RETURN (	SELECT FN_GET_ENCRYPT(cal_val, sdienck)	);
		
	EXCEPTION WHEN OTHERS THEN 
		 GET STACKED DIAGNOSTICS err_context = MESSAGE_TEXT;
	RETURN err_context;
			 
END;

$$;


ALTER FUNCTION wems.enc_varchar_ins(cal_val character varying, int_val integer, sdienck character varying, table_name character varying, cal_name character varying) OWNER TO wems;

--
-- TOC entry 1124 (class 1255 OID 1895829)
-- Name: fn_bin_to_dec(text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_bin_to_dec(p_binary text) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_GET_CONV_NA
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-11-16   stjoo       1. Created this function.
   2.0        2019-09-24   이재형                  Postgresql 변환
   NOTES:   문자열로 입력된 이진수를 10진수의 숫자형으로 반환한다.

    Input : p_value - VARCHAR

    Output : NUMBER

    Usage :
            SELECT FN_BIN_TO_DEC('01010100') FROM DUAL;

******************************************************************************/
DECLARE
    v_number numeric := 0;
    i numeric := 0;
BEGIN
    WHILE LENGTH(p_binary) > 0 LOOP
        v_number := v_number + (right(p_binary, 1)::NUMERIC * POWER(2, i));
        p_binary := substr(p_binary, 1, LENGTH(p_binary) - 1);
        i := i + 1;
    END LOOP;
    RETURN v_number;
END;
$$;


ALTER FUNCTION wems.fn_bin_to_dec(p_binary text) OWNER TO wems;

--
-- TOC entry 1083 (class 1255 OID 1895830)
-- Name: fn_chk_pwd(character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_chk_pwd(i_usr_id character varying, i_pwd character varying) RETURNS character varying
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $_$
/******************************************************************************
  프로그램명   : FN_CHK_PWD
  작  성  일   : 2019-11-11
  작  성  자   : 박동환
  프로그램용도 : 암호화된 비밀번호조회함수
  참 고 사 항  :
                 < PROTOTYPE >
                   FN_CHK_PWD

                 <PARAMETER>
                   < INPUT >
                     1. i_usr_id   	사용자ID
				     2. i_pwd     	비밀번호

                   < OUTPUT >
                     1. o_result     	비번일치여부(일치:'T', 불일치:'F')

                 < MODULE DIAGRAM >
                 1.변수 및 커서 선언


  -----------------------------------------------------------------------------------------
  TABLE                                        CRUD
  ==================================================
  TB_RAW_LOGIN								 R
  ------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------
  수정일자      수정자      수정내용
  ======== ======= 	==================================
  20191111    박동환      최초작성
  -------------------------------------------------------------------------------------------
  
*. 참고(본 함수가 내부적으로 처리되는 방식에 대한 이해)
1. 비번 암호화 과정	
	select crypt('비밀번호', gen_salt('md5')); -- 결과:'$1$kxNDzbv0$UfEqWxPBViqimy0E.UC5d.' 를 DB에 저장
	
2. 검증과정(비밀번호, DB에 저장돼 있던 암호값(SALT))	
 	select crypt('비밀번호', '$1$kxNDzbv0$UfEqWxPBViqimy0E.UC5d.');	

*****************************************************************************/
BEGIN
	
	RETURN 
	(
		SELECT 	CASE WHEN USER_PWD = crypt(i_pwd, USER_PWD) THEN 'T' ELSE 'F' END
		FROM 	TB_BAS_USER
		WHERE 	USER_ID = i_usr_id
	)::character varying;

  	EXCEPTION WHEN OTHERS THEN		
		RETURN	'F'::character varying;

END;

$_$;


ALTER FUNCTION wems.fn_chk_pwd(i_usr_id character varying, i_pwd character varying) OWNER TO wems;

--
-- TOC entry 1125 (class 1255 OID 1895831)
-- Name: fn_get_capacity(text, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_capacity(p_value text, p_column text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
/*
*****************************************************************************
   NAME:       FN_GET_CAPACITY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-03-31   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                  Postgresql 변환
   
   NOTES:   입력받은  DEVICE_ID 의 TB_PLF_CODE_INFO 테이블의 p_column 값을 반환

	Input : p_value - DEVICE_ID
            p_column - TB_PLF_CODE_INFO 의 컬럼 명

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_CAPACITY('AR00460036Z114827015X', 'CODE') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(200);
    v_sql text;
BEGIN
   v_sql := 'SELECT  '||p_column||' 
			FROM    wems.tb_plf_code_info 
			WHERE grp_cd = ''CAPACITY_CD''   
   			AND cd_desc =     
			  		CASE         
			  			WHEN substr($2, 1, 2) = ''HS'' THEN wems.fn_get_product_model($3, ''REF_3''::TEXT)         
			  		    else substr($4, 7, 4)     
			  		END';
	
  	execute v_sql into v_value using p_column,p_value,p_value,p_value;
    RETURN v_value;

END;
$_$;


ALTER FUNCTION wems.fn_get_capacity(p_value text, p_column text) OWNER TO wems;

--
-- TOC entry 1126 (class 1255 OID 1895832)
-- Name: fn_get_char_count(text, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_char_count(p_str text, p_chr text) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
/*
*****************************************************************************
   NAME:       FN_GET_CHAR_COUNT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-09-16   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                  Postgresql 변환
   NOTES:   입력받은 문자열에 포함된 문자의 갯수를  반환한다.

	Input : p_str - 문자열
	        p_chr - 찾을 문자

    Output : NUMBER - 문자 갯수

	Usage :
	        SELECT FN_GET_CHAR_COUNT('123|23|123', '|') FROM DUAL;

*****************************************************************************
*/
DECLARE
    i_result numeric := 0;
BEGIN
    SELECT
        LENGTH(p_str) - LENGTH(REPLACE(p_str, p_chr, ''))
        INTO STRICT i_result;
    RETURN i_result;
END;
$$;


ALTER FUNCTION wems.fn_get_char_count(p_str text, p_chr text) OWNER TO wems;

--
-- TOC entry 1127 (class 1255 OID 1895833)
-- Name: fn_get_chartonum(text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_chartonum(p_value text) RETURNS text
    LANGUAGE plpgsql
    AS $$
/*
*****************************************************************************
   NAME:       FN_GET_CHARTONUM
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-04-17   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                  Postgresql 변환
   
   NOTES:   RAW 테이블 컬럼 중 문자열로 입력되지만 수치형 데이터로 처리해야 하는 경우 사용한다.

	Input : p_value - VARCHAR

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_CHARTONUM('NA') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(100);
   
BEGIN
    SELECT
        CASE p_value
            WHEN 'NA' THEN NULL
            ELSE p_value
        END
        INTO STRICT v_value;
    RETURN v_value;
   
END;
$$;


ALTER FUNCTION wems.fn_get_chartonum(p_value text) OWNER TO wems;

--
-- TOC entry 1128 (class 1255 OID 1895834)
-- Name: fn_get_conv_na(text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_conv_na(p_value text) RETURNS text
    LANGUAGE plpgsql
    AS $$
/*
*****************************************************************************
   NAME:       FN_GET_CONV_NA
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-04-17   yngwie       1. Created this function.

   NOTES:   RAW 테이블 컬럼 중 문자열로 입력되지만 수치형 데이터로 처리해야 하는 경우 사용한다.

	Input : p_value - VARCHAR

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_CONV_NA('NA') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(100);
BEGIN
    v_value := (CASE
        WHEN p_value = 'NA' THEN NULL
        ELSE p_value
    END)::TEXT;
    RETURN v_value;
    EXCEPTION
        WHEN others THEN
            RETURN SQLERRM;
END;
$$;


ALTER FUNCTION wems.fn_get_conv_na(p_value text) OWNER TO wems;

--
-- TOC entry 1129 (class 1255 OID 1895835)
-- Name: fn_get_dateformat(numeric); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_dateformat(p_val numeric) RETURNS text
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_GET_DATEFORMAT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   2.0        2019-09-24   박동환       Postgresql 변환

   NOTES:   일자포맷 구성

	Input   : 일자포맷 스트링 문자수

    Output : 일자포맷

	Usage :
	        FN_GET_DATEFORMAT(14);

******************************************************************************/
DECLARE
    v_result TEXT;
BEGIN
    IF p_val = 4 THEN
        v_result := 'YYYY';
    ELSIF p_val = 6 THEN
        v_result := 'YYYYMM';
    ELSIF p_val = 8 THEN
        v_result := 'YYYYMMDD';
    ELSIF p_val = 10 THEN
        v_result := 'YYYYMMDDHH24';
    ELSIF p_val = 14 THEN
        v_result := 'YYYYMMDDHH24MISS';
    ELSE
        v_result := 'YYYYMMDDHH24MISS';
    END IF;
    RETURN v_result;
END;
$$;


ALTER FUNCTION wems.fn_get_dateformat(p_val numeric) OWNER TO wems;

--
-- TOC entry 1130 (class 1255 OID 1895836)
-- Name: fn_get_decrypt(character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_decrypt(i_text character varying, i_key character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
  프로그램ID   : fn_get_decrypt
  작  성  일   : 2019-11-07
  작  성  자   : 박동환
  프로그램용도 : 문자열복호화
  참 고 사 항  :
                 < PROTOTYPE >
                   fn_get_decrypt

                 <PARAMETER>
                   < INPUT >
                     1.i_text	입력문자열

                   < OUTPUT >
                     1.o_text	출력문자열

                 < MODULE DIAGRAM >
                 1.변수 및 커서 선언


  ----------------------------------------------------------------------------
  TABLE                                                                  CRUD
  ============================================================================

  ----------------------------------------------------------------------------

  ----------------------------------------------------------------------------
  수정일자    수정자      수정내용
  =========== =========== ====================================================
  20191107    박동환      최초작성
  ----------------------------------------------------------------------------

*****************************************************************************/

DECLARE
	o_text	character varying:= '';

BEGIN

	SELECT CONVERT_FROM(DECRYPT(DECODE(i_text,'hex'),CONVERT_TO(i_key,'utf8'),'aes'),'utf8')
	INTO	o_text;
	
	RETURN	o_text::character varying;
	
 	EXCEPTION WHEN OTHERS THEN		
		RETURN	''::character varying;
 	
END;
$$;


ALTER FUNCTION wems.fn_get_decrypt(i_text character varying, i_key character varying) OWNER TO wems;

--
-- TOC entry 1131 (class 1255 OID 1895837)
-- Name: fn_get_device_type(text, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_device_type(p_value text, p_column text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
/*
*****************************************************************************
   NAME:       FN_GET_DEVICE_TYPE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-03-31   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                  Postgresql 변환
   NOTES:   입력받은  DEVICE_ID 의 TB_PLF_CODE_INFO 테이블의 p_column 값을 반환

	Input : p_value - DEVICE_ID
	        p_column - TB_PLF_CODE_INFO 의 컬럼 명

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_DEVICE_TYPE('AR00460036Z114827015X', 'CODE');

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(200);
    v_sql text;
begin
	
	
	v_sql := 'SELECT '||p_column||' 
    FROM wems.tb_plf_code_info     
    WHERE grp_cd = ''DEVICE_TYPE_CD'' 
    AND cd_desc =     
   				CASE       
   					WHEN substr($1, 1, 2) = ''HS'' THEN ''AR''         
   				    WHEN substr($2, 8, 4) = ''4601'' THEN ''AR''         
   				    WHEN substr($3, 8, 4) = ''5001'' THEN ''AR''       
   				    ELSE substr($4, 1, 2)     
   				end';
   			
   	execute v_sql into v_value using p_value, p_value, p_value,p_value;
   	return v_value;
END;
$_$;


ALTER FUNCTION wems.fn_get_device_type(p_value text, p_column text) OWNER TO wems;

--
-- TOC entry 1088 (class 1255 OID 1895838)
-- Name: fn_get_div_cons(numeric, numeric, numeric, numeric); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_div_cons(p_pv numeric, p_bt numeric, p_grid numeric, p_cons numeric) RETURNS text
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_GET_DIV_CONS
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-28   yngwie    Created this function.
   2.0        2019-09-24   이재형     Postgresql 변환
   
   NOTES:   가정내소비전력량-TOTAL 에서 GRID, PV, 배터리가 차지하는 전력량을 구한다.

	Input : p_pv - PV전력량
	        p_bt - 배터리전력량-방전
	        p_grid - Grid 수전량
	        p_cons - 가정내소비전력량-TOTAL

    Output : VARCHAR - 결과 값, 구분자 | 를 중심으로 순서대로 pv, bt, grid

	Usage :


******************************************************************************/
DECLARE
    i_result 		VARCHAR := NULL;
    i_sql 			TEXT;
    i_ratio_pv 	numeric;
    i_ratio_bt 	numeric;
    i_ratio_grid 	numeric;
    i_pv_val 		numeric;
    i_bt_val 		numeric;
    i_grid_val 	numeric;
BEGIN

    IF p_cons IS NOT NULL THEN
	 	SELECT 
	 		 MAX( CASE WHEN TP_CD = 'PV' THEN ROUND( p_cons * RATIO, 1) ELSE NULL END ) PV_VAL 
	 		, MAX( CASE WHEN TP_CD = 'BT' THEN ROUND( p_cons * RATIO, 1) ELSE NULL END ) BT_VAL 
	 		, MAX( CASE WHEN TP_CD = 'GRID' THEN ROUND( p_cons * RATIO, 1) ELSE NULL END ) GRID_VAL 
	 	into i_pv_val, i_bt_val, i_grid_val
	 	FROM ( 
	 		SELECT 
	 		    TP_CD
	 		    , AMOUNT 
	 		    , COALESCE(1.0 * AMOUNT / NULLIF(SUM(AMOUNT) OVER(),0), 0) AS RATIO 
	 		FROM ( 
		 			SELECT  p_pv AMOUNT, 'PV' TP_CD 
		 			UNION ALL 
		 			SELECT p_bt AMOUNT, 'BT' TP_CD  
		 			UNION ALL 
		 			SELECT p_grid AMOUNT, 'GRID' TP_CD 
	 			) a
	 	) b ;

		 i_result := i_pv_val || '|' || i_bt_val || '|' || i_grid_val;
		
	--	RAISE NOTICE 'i_pv_val[%] i_bt_val[%] i_grid_val[%] >>> i_result[%]' ,i_pv_val, i_bt_val, i_grid_val, i_result;
		
	END IF;
	
	-- RAISE NOTICE 'p_cons[%], i_result[%]' ,p_cons, i_result; 

    RETURN i_result;
   

END;
$$;


ALTER FUNCTION wems.fn_get_div_cons(p_pv numeric, p_bt numeric, p_grid numeric, p_cons numeric) OWNER TO wems;

--
-- TOC entry 1089 (class 1255 OID 1895839)
-- Name: fn_get_div_grid_tr(numeric, numeric, numeric); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_div_grid_tr(p_pv numeric, p_bt numeric, p_grid numeric) RETURNS text
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_GET_DIV_GRID_TR
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-28   yngwie     Created this function.
   2.0        2019-09-24   이재형      Postgresql 변환
   
   NOTES:   PV전력량과 배터리전력량-방전 을 가지고 Grid 매전량 Total 중에서 PV 와 배터리가 차지하는 전력량을 구한다.

	Input : p_pv - PV전력량
	        p_bt - 배터리전력량-방전
	        p_grid - Grid 매전량 Total

    Output : VARCHAR - 결과 값, 구분자 | 를 중심으로 앞은 pv, 뒤는 bt

	Usage :

******************************************************************************/
DECLARE
    i_result 		VARCHAR := NULL;
    i_ratio_pv 	NUMERIC;
    i_ratio_bt 	NUMERIC;
    i_pv_val 		NUMERIC;
    i_bt_val 		NUMERIC;
   
BEGIN
	
     IF p_grid IS NOT NULL THEN
 
		 	SELECT MAX( CASE WHEN TP_CD = 'PV' THEN ROUND(p_grid * RATIO) ELSE NULL END ) PV_VAL 
		 			, MAX( CASE WHEN TP_CD = 'BT' THEN ROUND(p_grid * RATIO) ELSE NULL END ) BT_VAL
		 	INTO i_pv_val, i_bt_val
		 	FROM ( 
		 		SELECT 	 TP_CD 
				 		    , AMOUNT 
				 		    , COALESCE(1.0 * AMOUNT / NULLIF(SUM(AMOUNT) OVER(),0), 0) AS RATIO 
		 		FROM ( 
		 			SELECT p_pv AMOUNT, 'PV' TP_CD 
		 			UNION ALL 
		 			SELECT p_bt AMOUNT, 'BT' TP_CD  
		 		) a
		 	)b ;
		
	    i_result := i_pv_val || '|' || i_bt_val;
	    
	END IF;
    
	RETURN i_result;

END;
$$;


ALTER FUNCTION wems.fn_get_div_grid_tr(p_pv numeric, p_bt numeric, p_grid numeric) OWNER TO wems;

--
-- TOC entry 1117 (class 1255 OID 2015077)
-- Name: fn_get_ems_state(text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_ems_state(p_uid text DEFAULT 'ALL'::text) RETURNS text
    LANGUAGE plpgsql
    AS $$
/*
*****************************************************************************
   NAME:       FN_GET_EMS_STATE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-04   stjoo           1. Created this function.
   2.0        2019-09-24   이재형          1. Postgresql 변환

   NOTES:   입력 받은 ESS UID 정보에 해당하는 현재 세션 상태 정보 반환

	Input : p_UID - EMS UID

	Output : session status

	Usage :
	        FN_GET_EMS_STATE('TESTBED-1000')

*****************************************************************************
*/
DECLARE
    last_create_dt CHARACTER VARYING(15);
    sqlStmt CHARACTER VARYING(300);
    isWildCh numeric;
    result RECORD;
	rsltStr TEXT := ''; 
BEGIN

    select position('%' in p_UID) INTO  isWildCh;

    IF p_UID = 'ALL' THEN
        FOR result IN
        SELECT
            device_id, TO_CHAR(create_dt, 'YYYYMMDD HH24:MI:SS') AS create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id
            FROM (SELECT device_id, create_dt, response_cd, 
							ems_ipaddress, ems_out_ipaddress, 
							connector_id, 
						ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY create_dt DESC, create_tm DESC, data_seq DESC) AS rn
                FROM wems.tb_raw_login)
            AS a
            WHERE rn = 1
        LOOP
--            RAISE DEBUG USING MESSAGE = 'ESS[ '|| result.DEVICE_ID || ' ] : ';
--            CASE result.RESPONSE_CD
--                WHEN '0' THEN RAISE DEBUG USING MESSAGE = ' ON  :';
--                WHEN '1' THEN RAISE DEBUG USING MESSAGE = ' OFF : Unknown UID';
--                WHEN '2' THEN RAISE DEBUG USING MESSAGE = ' OFF : Password Err';
--                WHEN '3' THEN RAISE DEBUG USING MESSAGE = ' OFF : UID,PASSWD Err';
--                WHEN '128' THEN RAISE DEBUG USING MESSAGE = ' OFF : Session Closed';
--                ELSE RAISE DEBUG USING MESSAGE = ' OFF : Unknown Err';
--            END CASE;
--            RAISE DEBUG USING MESSAGE =' (from '|| result.create_dt|| ')';
--            RAISE DEBUG USING MESSAGE ='  IP['||result.ems_out_ipaddress||']';
--				CASE result.RESPONSE_CD
--                WHEN '0' THEN RAISE DEBUG USING MESSAGE = '  GW['|| result.connector_id|| ']';
--                ELSE RAISE DEBUG USING MESSAGE = '';
--            END CASE;

				rsltStr=rsltStr||'ESS['||result.DEVICE_ID||'] : ';
            CASE result.RESPONSE_CD
                WHEN '0' THEN rsltStr = rsltStr||' ON :';
                WHEN '1' THEN rsltStr = rsltStr||' OFF : Unknown UID';
                WHEN '2' THEN rsltStr = rsltStr||' OFF : Password Err';
                WHEN '3' THEN rsltStr = rsltStr||' OFF : UID,PASSWD Err';
                WHEN '128' THEN rsltStr = rsltStr||' OFF : Session Closed';
                ELSE rsltStr = rsltStr||' OFF : Unknown Err';
            END CASE;
            rsltStr = rsltStr||' (from '||result.create_dt||')';
            rsltStr = rsltStr||' IP['||result.ems_out_ipaddress||']';
				CASE result.RESPONSE_CD
                WHEN '0' THEN rsltStr = rsltStr||' GW['||result.connector_id||']';
                ELSE rsltStr = rsltStr||'';
            END CASE;
				rsltStr = rsltStr||chr(10);
        END LOOP;

    ELSIF p_UID = 'ONLINE' THEN
        

            FOR result IN (
                       SELECT device_id, to_char(create_dt, 'YYYYMMDD HH24:MI:SS') as create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id
                       FROM (
                                 SELECT device_id, create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id
                                 , ROW_NUMBER() OVER(PARTITION BY device_id ORDER BY create_dt DESC , create_tm DESC, data_seq DESC) rn
                                 FROM TB_RAW_LOGIN
                                 WHERE CREATE_DT BETWEEN SYS_EXTRACT_UTC(now() - interval '7 day') AND SYS_EXTRACT_UTC(now())
                             ) a
                       WHERE rn = 1 AND RESPONSE_CD = '0' 
                   )
        LOOP
        
--            RAISE DEBUG USING MESSAGE =  'ESS[ '|| result.DEVICE_ID||' ] : ';
--            RAISE DEBUG USING MESSAGE = ' ON ';
--            RAISE DEBUG USING MESSAGE = ' (from '|| result.create_dt|| ')';
--            RAISE DEBUG USING MESSAGE = '  IP['|| result.ems_out_ipaddress|| ']';
--            RAISE DEBUG USING MESSAGE = '  GW['|| result.connector_id||']';

				rsltStr = rsltStr||'ESS ['||result.DEVICE_ID||'] : ON (from '|| result.create_dt|| ') IP['|| result.ems_out_ipaddress||'] GW['|| result.connector_id||']'||chr(10);
        END LOOP;

    ELSIF p_UID = 'OFFLINE' THEN
       
		FOR result IN (
			SELECT device_id, to_char(create_dt, 'YYYYMMDD HH24:MI:SS') as create_dt, response_cd, ems_ipaddress, ems_out_ipaddress
			FROM (
				SELECT
					device_id, create_dt, response_cd, ems_ipaddress, ems_out_ipaddress
					, ROW_NUMBER() OVER(PARTITION BY device_id ORDER BY create_dt DESC, create_tm DESC, data_seq DESC) rn
				FROM TB_RAW_LOGIN
				WHERE CREATE_DT BETWEEN SYS_EXTRACT_UTC(now() - interval '7 day') AND SYS_EXTRACT_UTC(now())
        	) a
  			WHERE rn = 1 AND RESPONSE_CD <> '0'
		)
		LOOP
--            RAISE DEBUG USING MESSAGE = 'ESS[ '|| result.DEVICE_ID|| ' ] : ';
--            RAISE DEBUG USING MESSAGE = ' OFF ';
--            RAISE DEBUG USING MESSAGE = ' (from '|| result.create_dt|| ')';
--            RAISE DEBUG USING MESSAGE = '  IP['|| result.ems_out_ipaddress|| ']';
			rsltStr = rsltStr||'ESS['|| result.DEVICE_ID||'] : OFF (from '||result.create_dt||') IP['||result.ems_out_ipaddress||']'||chr(10);           
		END LOOP;
		
	ELSIF p_UID IS NOT NULL AND isWildCh = 0 THEN
        FOR result IN
        SELECT
            device_id, TO_CHAR(create_dt, 'YYYYMMDD HH24:MI:SS') AS create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id
            FROM (SELECT
                device_id, create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY create_dt DESC, create_tm DESC, data_seq DESC) AS rn
                FROM wems.tb_raw_login
                WHERE device_id = p_UID) AS a
            WHERE rn < 20
        LOOP
--            RAISE DEBUG USING MESSAGE = 'ESS[ '|| result.DEVICE_ID|| ' ] : ';
--            CASE result.RESPONSE_CD
--                WHEN '0' THEN RAISE DEBUG USING MESSAGE = ' ON  :';
--                WHEN '1' THEN RAISE DEBUG USING MESSAGE = ' OFF : Unknown UID';
--                WHEN '2' THEN RAISE DEBUG USING MESSAGE = ' OFF : Password Err';
--                WHEN '3' THEN RAISE DEBUG USING MESSAGE = ' OFF : UID,PASSWD Err';
--                WHEN '128' THEN RAISE DEBUG USING MESSAGE = ' OFF : Session Closed  ';
--                ELSE RAISE DEBUG USING MESSAGE = ' OFF LINE: Unknown Err';
--            END CASE;
--            RAISE DEBUG USING MESSAGE = ' (from '|| result.create_dt|| ')';
--            RAISE DEBUG USING MESSAGE = '  IP['|| result.ems_out_ipaddress|| ']';
--
--            IF result.RESPONSE_CD = '0' THEN RAISE DEBUG USING MESSAGE = '  GW['|| result.connector_id|| ']';
--            END IF;

            rsltStr = rsltStr||' ESS[ '||result.DEVICE_ID||' ] : ';
            CASE result.RESPONSE_CD
                WHEN '0' THEN rsltStr = rsltStr||' ON  :';
                WHEN '1' THEN rsltStr = rsltStr||' OFF : Unknown UID';
                WHEN '2' THEN rsltStr = rsltStr||' OFF : Password Err';
                WHEN '3' THEN rsltStr = rsltStr||' OFF : UID,PASSWD Err';
                WHEN '128' THEN rsltStr = rsltStr||' OFF : Session Closed  ';
                ELSE rsltStr = rsltStr||' OFF LINE: Unknown Err';
            END CASE;
            rsltStr = rsltStr||' (from '|| result.create_dt|| ')';
            rsltStr = rsltStr||' IP['|| result.ems_out_ipaddress|| ']';
            IF result.RESPONSE_CD = '0' THEN rsltStr = rsltStr||' GW ['||result.connector_id||']';
            END IF;
        		rsltStr = rsltStr||chr(10);
        END LOOP;
		
    ELSIF p_UID IS NOT NULL AND isWildCh > 0 THEN
        FOR result IN
        SELECT
            device_id, TO_CHAR(create_dt, 'YYYYMMDD HH24:MI:SS') AS create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id
            FROM (SELECT
                device_id, create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY create_dt DESC, create_tm DESC, data_seq DESC) AS rn
                FROM wems.tb_raw_login
                WHERE device_id LIKE '%'|| p_UID|| '%'
				)AS a
            WHERE rn = 1
        LOOP
--            RAISE DEBUG USING MESSAGE = 'ESS[ '|| result.DEVICE_ID|| ' ] : ';
--            CASE result.RESPONSE_CD
--                WHEN '0' THEN RAISE DEBUG USING MESSAGE = ' ON  :';
--                WHEN '1' THEN RAISE DEBUG USING MESSAGE = ' OFF : Unknown UID';
--                WHEN '2' THEN RAISE DEBUG USING MESSAGE = ' OFF : Password Err';
--                WHEN '3' THEN RAISE DEBUG USING MESSAGE = ' OFF : UID,PASSWD Err';
--                WHEN '128' THEN RAISE DEBUG USING MESSAGE = ' OFF : Session Closed';
--                ELSE RAISE DEBUG USING MESSAGE = ' OFF : Unknown Err';
--            END CASE;
--            RAISE DEBUG USING MESSAGE = ' (from '|| result.create_dt|| ')';
--            RAISE DEBUG USING MESSAGE = '  IP['|| result.ems_out_ipaddress|| ']';
--            RAISE DEBUG USING MESSAGE = '  GW['|| result.connector_id|| ']';
        
            rsltStr = rsltStr||' ESS['||result.DEVICE_ID||'] : ';
            CASE result.RESPONSE_CD
                WHEN '0' THEN rsltStr = rsltStr||' ON : ';
                WHEN '1' THEN rsltStr = rsltStr||' OFF : Unknown UID';
                WHEN '2' THEN rsltStr = rsltStr||' OFF : Password Err';
                WHEN '3' THEN rsltStr = rsltStr||' OFF : UID,PASSWD Err';
                WHEN '128' THEN rsltStr = rsltStr||' OFF : Session Closed';
                ELSE rsltStr = rsltStr||' OFF : Unknown Err';
            END CASE;
            rsltStr = rsltStr||' (from '|| result.create_dt|| ')';
            rsltStr = rsltStr||' IP['|| result.ems_out_ipaddress|| ']';
            rsltStr = rsltStr||' GW['|| result.connector_id|| ']';
				rsltStr = rsltStr||chr(10);
        END LOOP;
    END IF;

    FOR result IN
    	SELECT
        COUNT(*) AS ess_count
		FROM (
			SELECT
            device_id, create_dt, response_cd, ems_ipaddress, ems_out_ipaddress, connector_id, ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY create_dt DESC, create_tm DESC, data_seq DESC) AS rn
			FROM wems.tb_raw_login
		) AS a
		WHERE rn = 1 AND a.response_cd = '0'
    LOOP
--        RAISE DEBUG USING MESSAGE = 'TOTAL ONLINE : ';
--        RAISE DEBUG USING MESSAGE = result.ESS_COUNT::TEXT;

        rsltStr = rsltStr||'TOTAL ONLINE : '||result.ESS_COUNT::TEXT||chr(10);
    END LOOP;
	
    RETURN rsltStr;
    EXCEPTION
        WHEN others THEN
            RAISE DEBUG USING MESSAGE = 'ERR CODE : '||TO_CHAR(SQLSTATE);
            RAISE DEBUG USING MESSAGE = 'ERR MESSAGE : '|| SQLERRM;
            RETURN - 1;
END;
/* update TB_OPR_ESS_STATE set OPER_STUS_CD='4' where DEVICE_ID = 'TESTBED-0007'; */
/* select FN_GET_EMS_STATE('TESTBED-0007') from dual; */
/* update TB_OPR_ESS_STATE set OPER_STUS_CD='1' where DEVICE_ID = 'TESTBED-0007' */
$$;


ALTER FUNCTION wems.fn_get_ems_state(p_uid text) OWNER TO wems;

--
-- TOC entry 1132 (class 1255 OID 1895842)
-- Name: fn_get_encrypt(character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_encrypt(i_text character varying, i_key character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
  프로그램ID   : fn_get_encrypt
  작  성  일   : 2019-11-07
  작  성  자   : 박동환
  프로그램용도 : 문자열암호화
  참 고 사 항  :
                 < PROTOTYPE >
                   fn_get_encrypt

                 <PARAMETER>
                   < INPUT >
                     1.i_text	입력문자열
                     2.i_key	입력키

                   < OUTPUT >
                     1.o_text	출력문자열

                 < MODULE DIAGRAM >
                 1.변수 및 커서 선언


  ----------------------------------------------------------------------------
  TABLE                                                                  CRUD
  ============================================================================

  ----------------------------------------------------------------------------

  ----------------------------------------------------------------------------
  수정일자    수정자      수정내용
  =========== =========== ====================================================
  20191107    박동환      최초작성
  ----------------------------------------------------------------------------

*****************************************************************************/

DECLARE
	o_text	character varying:= '';

BEGIN
	
	SELECT ENCODE(ENCRYPT(CONVERT_TO(i_text,'utf8'),CONVERT_TO(i_key,'utf8'),'aes'),'hex')
	INTO	o_text;

	RETURN	o_text::character varying;

  	EXCEPTION WHEN OTHERS THEN		
		RETURN	''::character varying;
			
END;
$$;


ALTER FUNCTION wems.fn_get_encrypt(i_text character varying, i_key character varying) OWNER TO wems;

--
-- TOC entry 1090 (class 1255 OID 1895843)
-- Name: fn_get_fmt_num(numeric); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_fmt_num(p_value numeric) RETURNS text
    LANGUAGE plpgsql
    AS $$
/*
*****************************************************************************
   NAME:       FN_GET_FMT_NUM
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-04-17   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                            Postgresql 변환
   NOTES:   RAW 테이블 컬럼 중 문자열로 입력되지만 수치형 데이터로 처리해야 하는 경우 사용한다.

	Input : p_value - VARCHAR

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_FMT_NUM('123.57') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(100);
BEGIN
    v_value :=
    CASE
        WHEN p_value = 0 THEN '0'
        WHEN position('.' in TO_CHAR(p_value)) > 0 THEN TO_CHAR(p_value, 'FM999,999,990.00')
        ELSE TO_CHAR(p_value, 'FM999,999,999')
    END;
   
    RETURN v_value;

END;
$$;


ALTER FUNCTION wems.fn_get_fmt_num(p_value numeric) OWNER TO wems;

--
-- TOC entry 1133 (class 1255 OID 1895844)
-- Name: fn_get_job_hist(); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_job_hist() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
    result RECORD;
/*
*****************************************************************************
   NAME:       FN_GET_JOB_HIST
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-12-05   jihoon           1. Created this function.
   2.0        2019-09-24   이재형                            Postgresql 변환

   NOTES:   TB_SYS_JOB_HIST 테이블 최신 데이터 확인

	Input : NULL

    Output : 0 또는 Exception 발생 시 -1

	Usage :
	        FN_GET_JOB_HIST('2014-12-05')

*****************************************************************************
*/
BEGIN

    FOR result IN
    SELECT
        job_seq, job_name, job_msg, exec_dt
        FROM (SELECT
            job_seq, job_name, job_msg, exec_dt
            FROM wems.tb_sys_job_hist AS a
            WHERE 1 = 1 AND job_msg LIKE 'TBR%' --'%TBR%'에서 변경, index 성능여부 추가로 확인 필요
            ORDER BY exec_dt DESC) AS var_sbq
        LIMIT 9
    LOOP
        RAISE DEBUG USING MESSAGE = result.JOB_SEQ|| '|'|| result.JOB_NAME||'|'|| result.JOB_MSG||'|'|| result.EXEC_DT;

    END LOOP;
    RETURN 0;
   
    EXCEPTION
        WHEN others THEN
            RETURN - 1;
END;
$$;


ALTER FUNCTION wems.fn_get_job_hist() OWNER TO wems;

--
-- TOC entry 1134 (class 1255 OID 1895845)
-- Name: fn_get_offset(text, text, text, text, numeric); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_offset(p_srcstr text, p_srcdf text, p_start text, p_end text, p_utcoffset numeric) RETURNS text
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_GET_OFFSET
   PURPOSE:   DST적용 OFFSET조회

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-05-13   yngwie       1. Created this function.
   2.0        2019-10-07   박동환        postgres 변환

   NOTES:   입력받은  날짜와 산식을 가지고 DST 가 적용된 날짜인지 아닌지를 계산하여 해당 OFFSET를 반환한다..


	Input : p_srcStr - 조회 년월일시
	        p_srcDf -  조회 년월일시 포맷
	        p_start - TB_BAS_TIMEZONE 테이블의  DST_START_EXP
	        p_end - TB_BAS_TIMEZONE 테이블의  DST_END_EXP
	        p_utcOffset - TB_BAS_TIMEZONE 테이블의  UTC_OFFSET

    Output : NUMBER offset - DST 시즌일 경우 p_utcOffset + 1

	Usage :
	        SELECT
				A.*
				, FN_GET_OFFSET('2015040100', 'YYYYMMDDHH24', A.DST_START_EXP, A.DST_END_EXP, A.UTC_OFFSET)
			FROM
				TB_BAS_TIMEZONE A

********************************************************************************/
DECLARE
    v_sql 		TEXT := NULL;
    v_sql_au 	TEXT := NULL;
    v_result 	NUMERIC := -99;

    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3


BEGIN

    IF p_start IS NULL OR p_end IS NULL THEN
        v_result := p_utcOffset;
    ELSE
    	-- DST적용국가(호주)
    	IF POSITION('{{prevYear}}' IN p_start) > 0 THEN

	    	v_sql := FORMAT(
	    			'SELECT CASE WHEN SUM(RESULT) > 0 THEN %s ELSE %s END FROM (	' 					||
						'SELECT CASE WHEN ''%s'' BETWEEN %s AND %s THEN 1 ELSE 0 END AS RESULT '	||
						'UNION ALL '
	    				, p_utcOffset + 1
						, p_utcOffset
						, p_srcStr
						, p_start
						, p_end )	;

			v_sql := REPLACE(v_sql, '{{year}}', SUBSTR(p_srcStr, 1, 4));
			v_sql := REPLACE(v_sql, '{{prevYear}}', TO_CHAR(SUBSTR(p_srcStr, 1, 4)::NUMERIC - 1, 'FM0000'));
			v_sql := REPLACE(v_sql, '{{df}}', p_srcDf);
			
			v_sql_au   := FORMAT(
					'SELECT 	CASE WHEN ''%s'' BETWEEN %s AND %s THEN 1 ELSE 0 END AS RESULT) S'
					, p_srcStr
					, p_start
					, p_end )	;

			v_sql_au := REPLACE(v_sql_au, '{{prevYear}}', SUBSTR(p_srcStr, 1, 4));
			v_sql_au := REPLACE(v_sql_au, '{{year}}', TO_CHAR(SUBSTR(p_srcStr, 1, 4)::NUMERIC + 1, 'FM0000'));
			v_sql_au := REPLACE(v_sql_au, '{{df}}', p_srcDf);
			
			 v_sql := CONCAT_WS('', v_sql, v_sql_au);
			
        ELSE
        
			v_sql   := FORMAT(
					'SELECT CASE WHEN ''%s'' BETWEEN %s AND %s THEN %s ELSE %s END'
					, p_srcStr
					, p_start
					, p_end
					, p_utcOffset + 1
					, p_utcOffset);
				
            v_sql := REPLACE(v_sql, '{{year}}', SUBSTR(p_srcStr, 1, 4));
            v_sql := REPLACE(v_sql, '{{df}}', p_srcDf);
           
        END IF;

        EXECUTE  v_sql INTO v_result;
--	    RAISE NOTICE 'FN_GET_OFFSET >>> SQL[%] RETURN[%]', v_sql, v_result;

    END IF;
   
    RETURN v_result;
   
    EXCEPTION WHEN OTHERS then
    	/*
		GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
	    RAISE NOTICE 'FN_GET_OFFSET >>> 오류[%] SQL[%]', v_err_msg, v_sql;
	   */
        RETURN - 99;
       
END;
$$;


ALTER FUNCTION wems.fn_get_offset(p_srcstr text, p_srcdf text, p_start text, p_end text, p_utcoffset numeric) OWNER TO wems;

--
-- TOC entry 1135 (class 1255 OID 1895846)
-- Name: fn_get_partition_name(text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_partition_name(p_tbl text) RETURNS text
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_GET_PARTITION_NAME
   PURPOSE:   파티션테이블명 조회

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2019-10-02   박동환        Postgresql 변환

   NOTES:   입력테이블에 대한 최신 파티션 테이블명 조회
   
    Input :  테이블명	        
	Output : 입력 테이블의 한 시간 전 시간을 반영하는 파티션 테이블명 
	
	TEST : SELECT FN_GET_PARTITION_NAME('TB_OPR_ALARM_HIST');   
	         --> 'pt_opr_alarm_hist_201910'
******************************************************************************/	
DECLARE
    v_ret TEXT := '';
BEGIN

	SELECT 	CHILD.RELNAME
	INTO		v_ret
	FROM 	PG_INHERITS
    JOIN 		PG_CLASS PARENT ON PG_INHERITS.INHPARENT = PARENT.OID
    JOIN 		PG_CLASS CHILD   ON PG_INHERITS.INHRELID   = CHILD.OID
    JOIN 		TB_PLF_TBL_META M ON (PARENT.RELNAME= LOWER(M.TABLE_NM)  )
    WHERE	M.PARTITION_TYPE IS NOT null
    AND		M.TABLE_NM = UPPER(p_tbl)
    AND		( CHILD.RELNAME like 'pt_'||SUBSTRING(LOWER(p_tbl), 4)||'%'||TO_CHAR(SYS_EXTRACT_UTC(NOW() -  interval '1 HOUR'), 'YYYYMMDD') || '%'
    				OR
				 CHILD.RELNAME like 'pt_'||SUBSTRING(LOWER(p_tbl), 4)||'%'||TO_CHAR(SYS_EXTRACT_UTC(NOW() -  interval '1 HOUR'), 'YYYYMM') || '%'
				)
	;

    RETURN v_ret;
   
END;
$$;


ALTER FUNCTION wems.fn_get_partition_name(p_tbl text) OWNER TO wems;

--
-- TOC entry 1091 (class 1255 OID 1895847)
-- Name: fn_get_product_model(text, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_product_model(p_value text, p_column text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
/*
*****************************************************************************
   NAME$       FN_GET_PRODUCT_MODEL
   PURPOSE$

   REVISIONS$
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-03-31   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                            Postgresql 변환
   NOTES$   입력받은  DEVICE_ID 의TB_PLF_CODE_INFO 테이블의 p_column 값을 반환

	Input $ p_value - DEVICE_ID
            p_column - TB_PLF_CODE_INFO 의 컬럼 명

    Output $ VARCHAR

	Usage $
	        SELECT FN_GET_PRODUCT_MODEL('AR00460036Z114827015X', 'CODE') FROM DUAL;
	        SELECT FN_GET_PRODUCT_MODEL('AR00460036Z114827015X', 'REF_5') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(200);
    v_sql text;
BEGIN

   
  	v_sql := 'SELECT  case when ($1=''REF_5'' and SUBSTR($2, 1, 2)=''HS'')  
    		  then (select cntry_cd from tb_bas_device where device_id=$3 ) else '||p_column||' end 
    FROM TB_PLF_CODE_INFO 
    WHERE 
     GRP_CD = ''PRODUCT_MODEL_CD'' 
     AND CODE    = CASE WHEN SUBSTR($4, 1, 2)=''HS'' THEN SUBSTR($5,1,8) WHEN SUBSTR($6, 1, 2)!=''AR'' THEN ''HHQC''||SUBSTR($7, 8, 4) ELSE CODE END 
     AND REF_2   = CASE WHEN SUBSTR($8, 1, 2)=''HS'' THEN REF_2 WHEN SUBSTR($9, 1, 2) != ''AR'' THEN (case when SUBSTR($10, 8, 4) = ''4601'' then ''0046'' when  SUBSTR($11, 8, 4) = ''5001'' then ''0050'' end ) ELSE SUBSTR($12, 3, 4)                 
	END 
     AND REF_3   = CASE WHEN SUBSTR($13, 1, 2)=''HS'' THEN REF_3 WHEN SUBSTR($14, 1, 2) != ''AR'' THEN ''0000'' ELSE FN_GET_CAPACITY($15, ''CD_DESC'') END 
    AND CD_DESC = CASE WHEN SUBSTR($16, 1, 2)=''HS'' THEN CD_DESC WHEN SUBSTR($17, 1, 2)!=''AR'' THEN CD_DESC  ELSE (CASE WHEN SUBSTR($18, LENGTH($19))= ''O'' THEN ''H'' ELSE SUBSTR($20, LENGTH($21)) END) END '
	;
	
	EXECUTE v_sql INTO v_value USING p_column, p_value, p_value, p_value, p_value,p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value, p_value;
	
    RETURN v_value;
	
END;
$_$;


ALTER FUNCTION wems.fn_get_product_model(p_value text, p_column text) OWNER TO wems;

--
-- TOC entry 1136 (class 1255 OID 1895848)
-- Name: fn_get_product_model_new(text, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_product_model_new(p_value text, p_column text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
/*
*****************************************************************************
   NAME:       FN_GET_PRODUCT_MODEL
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-03-31   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                            Postgresql 변환
   NOTES:   입력받은  DEVICE_ID 의TB_PLF_CODE_INFO 테이블의 p_column 값을 반환

	Input : p_value - DEVICE_ID
            p_column - TB_PLF_CODE_INFO 의 컬럼 명

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_PRODUCT_MODEL('AR00460036Z114827015X', 'CODE') FROM DUAL;
	        SELECT FN_GET_PRODUCT_MODEL('AR00460036Z114827015X', 'REF_5') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(200);
    v_sql text;
BEGIN
    v_sql := 'SELECT '|| p_column || ' 
			  FROM TB_PLF_CODE_INFO 
			  WHERE  GRP_CD = ''PRODUCT_MODEL_CD'' 
              AND code = SUBSTR($1, 1, 8) ';

    EXECUTE  v_sql INTO v_value USING p_value;

    RETURN v_value;

END;
$_$;


ALTER FUNCTION wems.fn_get_product_model_new(p_value text, p_column text) OWNER TO wems;

--
-- TOC entry 1137 (class 1255 OID 1895849)
-- Name: fn_get_product_model_new_qcell(text, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_product_model_new_qcell(p_value text, p_column text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
/*
*****************************************************************************
   NAME:       FN_GET_PRODUCT_MODEL
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-03-31   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                            Postgresql 변환
   
   NOTES:   입력받은  DEVICE_ID 의TB_PLF_CODE_INFO 테이블의 p_column 값을 반환

	Input : p_value - DEVICE_ID
            p_column - TB_PLF_CODE_INFO 의 컬럼 명

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_PRODUCT_MODEL('AR00460036Z114827015X', 'CODE') FROM DUAL;
	        SELECT FN_GET_PRODUCT_MODEL('AR00460036Z114827015X', 'REF_5') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(200);
    v_sql text;
BEGIN
    v_sql := '	SELECT '|| p_column || ' 
				FROM TB_PLF_CODE_INFO 
				WHERE GRP_CD = ''PRODUCT_MODEL_CD''
				AND code = ''HHQC''||SUBSTR($1, 8, 4) ';
    
    EXECUTE  v_sql INTO v_value USING p_value;

    RETURN v_value;

END;
$_$;


ALTER FUNCTION wems.fn_get_product_model_new_qcell(p_value text, p_column text) OWNER TO wems;

--
-- TOC entry 1138 (class 1255 OID 1895850)
-- Name: fn_get_product_model_type_cd(text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_product_model_type_cd(p_value text) RETURNS text
    LANGUAGE plpgsql
    AS $$
/*
*****************************************************************************
   NAME:       FN_GET_PRODUCT_MODEL
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-10-28   stjoo       1. Created this function.
   2.0        2019-09-24   이재형                            Postgresql 변환
   
   NOTES:   입력받은  DEVICE_ID 의  TB_BAS_DEVICE.PRODUCT_MODEL_TYPE_CD 를 반환

	Input : p_value - DEVICE_ID

    Output : VARCHAR

	Usage : SELECT FN_GET_PRODUCT_MODEL_TYPE_CD('AR00460036Z114827015X') FROM DUAL;


*****************************************************************************
*/
DECLARE
    v_value CHARACTER VARYING(200);
BEGIN
    
	SELECT SUBSTR(PRODUCT_MODEL_NM, 1, 7) 
	into v_value
	FROM TB_BAS_DEVICE 
	WHERE  DEVICE_ID = p_value;

    RETURN v_value;

END;
$$;


ALTER FUNCTION wems.fn_get_product_model_type_cd(p_value text) OWNER TO wems;

--
-- TOC entry 1139 (class 1255 OID 1895851)
-- Name: fn_get_push_update_result(text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_push_update_result(p_val text) RETURNS text
    LANGUAGE plpgsql
    AS $$
/*
*****************************************************************************
   NAME:       FN_GET_PUSH_UPDATE_RESULT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-09-04   yngwie       1. Created this function.

   NOTES:   RESULT_CD 를 가지고 EMS, PCS, BMS 의 Firmware Update 결과를 반환한다.

	Input : p_val - TB_OPR_CTRL_RESULT.RESULT_CD

    Output : VARCHAR - 결과 값, 구분자 | 를 기준으로 EMS|PCS|BMS 의 결과코드

	Usage :


*****************************************************************************
*/
DECLARE
    i_result CHARACTER VARYING(100) := '||';
BEGIN
    IF p_val = '0' THEN
        i_result := '0|0|0';
    ELSIF p_val = '1' THEN
        i_result := '1|1|1';
    ELSIF p_val = '2' THEN
        i_result := '2||';
    ELSIF p_val = '4' THEN
        i_result := '|4|';
    ELSIF p_val = '5' THEN
        i_result := 'NA|NA|NA';
    ELSIF p_val = '8' THEN
        i_result := '||8';
    ELSIF p_val = '16' THEN
        i_result := '16||';
    ELSIF p_val = '32' THEN
        i_result := '|32|';
    ELSIF p_val = '64' THEN
        i_result := '||64';
    ELSIF p_val = '112' THEN
        i_result := '16|32|64';
    ELSIF p_val = '255' THEN
        i_result := '255|255|255';
    ELSIF p_val = '6' THEN
        i_result := '2|4|';
    ELSIF p_val = '34' THEN
        i_result := '2|32|';
    ELSIF p_val = '10' THEN
        i_result := '2||8';
    ELSIF p_val = '66' THEN
        i_result := '2||64';
    ELSIF p_val = '20' THEN
        i_result := '16|4|';
    ELSIF p_val = '48' THEN
        i_result := '16|32|';
    ELSIF p_val = '24' THEN
        i_result := '16||8';
    ELSIF p_val = '80' THEN
        i_result := '16||64';
    ELSIF p_val = '12' THEN
        i_result := '|4|8';
    ELSIF p_val = '68' THEN
        i_result := '|4|64';
    ELSIF p_val = '40' THEN
        i_result := '|32|8';
    ELSIF p_val = '96' THEN
        i_result := '|32|64';
    ELSIF p_val = '14' THEN
        i_result := '2|4|8';
    ELSIF p_val = '70' THEN
        i_result := '2|4|64';
    ELSIF p_val = '42' THEN
        i_result := '2|32|8';
    ELSIF p_val = '98' THEN
        i_result := '2|32|64';
    ELSIF p_val = '84' THEN
        i_result := '16|4|64';
    ELSIF p_val = '28' THEN
        i_result := '16|4|8';
    ELSIF p_val = '56' THEN
        i_result := '16|32|8';
    END IF;
    RETURN i_result;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$$;


ALTER FUNCTION wems.fn_get_push_update_result(p_val text) OWNER TO wems;

--
-- TOC entry 1140 (class 1255 OID 1895852)
-- Name: fn_get_pwr_range_cd(text, numeric, numeric, text, text, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_pwr_range_cd(p_type text, p_value numeric, p_pv_value numeric, p_pv_stus_cd text, p_grid_stus_cd text, p_bt_stus_cd text) RETURNS text
    LANGUAGE plpgsql
    AS $$
/*
*****************************************************************************
   NAME:       FN_GET_PWR_RANGE_CD
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-05-21   yngwie       1. Created this function.
   2.0        2019-09-24   이재형                            Postgresql 변환
   NOTES:   입력받은  수치에 대한 파워 범위 코드를 반환한다.

	Input : p_type - 에너지 흐름 방향
	                 GRID_LOAD :
	                 GRID_ESS :
	                 ESS_GRID :
	                 ESS_LOAD :
	                 PV_ESS :
	        p_value - 전력값
	        p_pv_value - ESS 로 in, out 되는 경우 pv_pw 값이 필요함. 그 외는  null

			- PV 발전 파워 : 해당 값 [0, 1~499, 500 ~ 3999, 4000 ~ ](W)
			- ESS-부하 파워 : 해당 값/ PV출력 [0, 1~19,20~69,70~]('%) (PV출력이 0인 경우, 큼으로 처리)
			- ESS-Grid 파워 : 해당 값/ PV출력 [0, 1~19,20~69,70~]('%) (PV출력이 0인 경우, 큼으로 처리)
			- Grid-부하 파워 : 해당 값 [0, 1~999, 1000 ~ 5000, 5000 ~ ](W)

			p_pv_stus_cd - PV_STUS_CD (0: 정지, 1:발전)
			p_grid_stus_cd - GRID_STUS_CD (0:소비, 1:매전, 2:정지)
			p_bt_stus_cd - BT_STUS_CD (0:방전, 1:충전, 2:정지)

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_PWR_RANGE_CD('ESS_GRID', 36, 2, '1', '0', '0') FROM DUAL;

*****************************************************************************
*/
DECLARE
    v_result CHARACTER VARYING(1);
    v_value numeric;
    v_pv_value numeric;
BEGIN
    v_pv_value := ABS(p_pv_value);
    v_value := ABS(p_value);

    IF p_type = 'GRID_LOAD' THEN
        IF p_grid_stus_cd = '0' THEN
            IF v_value > 0 AND v_value < 1000 THEN
                v_result := 'S';
            ELSIF v_value >= 1000 AND v_value < 5000 THEN
                v_result := 'M';
            ELSIF v_value >= 5000 THEN
                v_result := 'L';
            END IF;
        ELSE
            v_result := NULL;
        END IF;
    ELSIF p_type = 'GRID_ESS' THEN
        IF p_grid_stus_cd = '0' AND p_bt_stus_cd = '1' THEN
            IF v_value > 0 AND v_value < 1000 THEN
                v_result := 'S';
            ELSIF v_value >= 1000 AND v_value < 5000 THEN
                v_result := 'M';
            ELSIF v_value >= 5000 THEN
                v_result := 'L';
            END IF;
        ELSE
            v_result := NULL;
        END IF;
    ELSIF p_type = 'ESS_GRID' THEN
        IF p_bt_stus_cd = '0' AND p_grid_stus_cd = '1' THEN
            IF v_pv_value = 0 THEN
                v_result := 'L';
            ELSIF v_value / v_pv_value > 0 AND v_value / v_pv_value < 20 THEN
                v_result := 'S';
            ELSIF v_value / v_pv_value >= 20 AND v_value / v_pv_value < 70 THEN
                v_result := 'M';
            ELSIF v_value / v_pv_value >= 70 THEN
                v_result := 'L';
            END IF;
        ELSE
            v_result := NULL;
        END IF;
    ELSIF p_type = 'ESS_LOAD' THEN
        IF p_bt_stus_cd = '0' THEN
            IF v_pv_value = 0 THEN
                v_result := 'L';
            ELSIF v_value / v_pv_value > 0 AND v_value / v_pv_value < 20 THEN
                v_result := 'S';
            ELSIF v_value / v_pv_value >= 20 AND v_value / v_pv_value < 70 THEN
                v_result := 'M';
            ELSIF v_value / v_pv_value >= 70 THEN
                v_result := 'L';
            END IF;
        ELSE
            v_result := NULL;
        END IF;
    ELSIF p_type = 'PV_ESS' THEN
        IF p_pv_stus_cd = '1' AND p_bt_stus_cd = '1' THEN
            IF v_pv_value > 0 AND v_pv_value < 500 THEN
                v_result := 'S';
            ELSIF v_pv_value >= 500 AND v_pv_value < 4000 THEN
                v_result := 'M';
            ELSIF v_pv_value >= 4000 THEN
                v_result := 'L';
            END IF;
        ELSE
            v_result := NULL;
        END IF;
    END IF;
    RETURN v_result;
   
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$$;


ALTER FUNCTION wems.fn_get_pwr_range_cd(p_type text, p_value numeric, p_pv_value numeric, p_pv_stus_cd text, p_grid_stus_cd text, p_bt_stus_cd text) OWNER TO wems;

--
-- TOC entry 1092 (class 1255 OID 1895853)
-- Name: fn_get_self_consumption(text, text, numeric, numeric); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_self_consumption(p_device_id text, p_colec_dd text, p_whbt numeric, p_whday numeric) RETURNS TABLE(panel_pw numeric, self_cons_wo_bt numeric, self_suff_wo_bt numeric, self_cons_wt_bt numeric, self_suff_wt_bt numeric)
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_GET_SELF_CONSUMPTION
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-05-20   yngwie     Created this function.
   2.0        2019-09-24   이재형      Postgresql 변환
   
   NOTES:   Self Consumption & Self Sufficiency 개선 예측 데이터를 구한다.

    Input : p_device_id VARCHAR 장비 ID, NULL 이면 장비 데이터 조회 없이 계산
            p_colec_dd VARCHAR  일통계 조회 날짜 (YYYYMMDD)
            p_whbt NUMBER 배터리 용량
            p_whday NUMBER 일 부하 용량

	Usage : SELECT * FROM TABLE(FN_GET_SELF_CONSUMPTION(NULL, NULL, 5, 12));

******************************************************************************/
DECLARE
    v_whbt numeric := p_whbt;
    v_whday numeric := p_whday;
BEGIN

    IF p_device_id IS NOT NULL AND (OCTET_LENGTH(p_device_id) = 21 OR OCTET_LENGTH(p_device_id) = 18) then
    
        SELECT (B.CONS_GRID_PW_H + B.CONS_PV_PW_H + B.CONS_BT_PW_H) * 0.001 AS CONS_PW_H , A.CAPACITY::numeric 
        INTO v_whday, v_whbt
        FROM TB_BAS_DEVICE A , TB_BAS_CITY C , TB_STT_ESS_DD B 
        WHERE  A.DEVICE_ID = p_device_id
        AND A.CITY_CD = C.CITY_CD 
        AND A.DEVICE_ID = B.DEVICE_ID 
        AND A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD  
        AND B.COLEC_DD = p_colec_dd 
        AND C.UTC_OFFSET = B.UTC_OFFSET;

    END IF;

    IF v_whday > 0 then
    	  
    	  RETURN QUERY
          WITH SELFCSM_RATE AS ( 	
         		SELECT '0600' AS FROM_TM, 0.25 AS RATE 
         	    UNION ALL 
         	    SELECT '0630' AS FROM_TM, 0.75 AS RATE 
         	   	UNION ALL 
         	    SELECT '0700' AS FROM_TM,  1.25 AS RATE 
         	   	UNION ALL 
         	    SELECT '0730' AS FROM_TM, 1.75 AS RATE 
         	   	UNION ALL 
         	    SELECT '0800' AS FROM_TM, 2.25 AS RATE 
         	   	UNION ALL 
         	    SELECT '0830' AS FROM_TM, 2.75 AS RATE 
         	   	UNION ALL 
         	    SELECT '0900' AS FROM_TM, 3.25 AS RATE 
         	   	UNION ALL 
         	    SELECT '0930' AS FROM_TM, 3.75 AS RATE 
         	   	UNION ALL 
         	    SELECT '1000' AS FROM_TM, 4.25 AS RATE 
         	   	UNION ALL 
         	    SELECT '1030' AS FROM_TM, 4.75 AS RATE
         	   	UNION ALL 
         	    SELECT '1100' AS FROM_TM, 5.25 AS RATE 
         	   	UNION ALL 
         	    SELECT '1130' AS FROM_TM, 5.75 AS RATE  
         	    UNION ALL 
         	    SELECT '1200' AS FROM_TM,  6.25 AS RATE
         	    UNION ALL 
         	    SELECT '1230' AS FROM_TM,  6.75 AS RATE 
         	    UNION ALL 
         	    SELECT '1300' AS FROM_TM,  7.25 AS RATE 
         	    UNION ALL 
         	    SELECT '1330' AS FROM_TM,  7.75 AS RATE
         	    UNION ALL 
         	    SELECT '1400' AS FROM_TM,  8.25 AS RATE 
         	    UNION ALL 
         	    SELECT '1430' AS FROM_TM,  8.75 AS RATE 
         	    UNION ALL  	
         	    SELECT '1500' AS FROM_TM,  9.25 AS RATE  
         	    UNION ALL  	
         	    SELECT '1530' AS FROM_TM,  9.75 AS RATE  
         	    UNION ALL  	
         	    SELECT '1600' AS FROM_TM, 10.25 AS RATE  
         	    UNION ALL  	
         	    SELECT '1630' AS FROM_TM, 10.75 AS RATE  
         	    UNION ALL  	
         	    SELECT '1700' AS FROM_TM, 11.25 AS RATE  
         	    UNION ALL  	
         	    SELECT '1730' AS FROM_TM, 11.75 AS RATE   )  
         SELECT  	PANEL_PW::float8  	, 
                    ROUND( ((SUM(PV_PW_PER_HALFHOUR) - SUM(PV_CONS_PER_HALFHOUR)) / SUM(PV_PW_PER_HALFHOUR))::numeric, 9)::float8 AS SELF_CONS_WO_BT, 
                    ROUND( ((SUM(PV_PW_PER_HALFHOUR) - SUM(PV_CONS_PER_HALFHOUR)) / v_whday)::numeric, 9)::float8 AS SELF_SUFF_WO_BT , 
                    ROUND( (((SUM(PV_PW_PER_HALFHOUR) - SUM(PV_CONS_PER_HALFHOUR)) + 
                   			CASE 
                   				WHEN SUM(PV_CONS_PER_HALFHOUR) > v_whbt THEN v_whbt 
                   				ELSE SUM(PV_CONS_PER_HALFHOUR) 
                   			END)  / SUM(PV_PW_PER_HALFHOUR))::numeric, 9)::float8 AS SELF_CONS_WT_BT ,
                   	ROUND( (((SUM(PV_PW_PER_HALFHOUR) - SUM(PV_CONS_PER_HALFHOUR)) + 
                   			CASE 
                   				WHEN SUM(PV_CONS_PER_HALFHOUR) > v_whbt THEN v_whbt 
                   			ELSE SUM(PV_CONS_PER_HALFHOUR) 
                   			END)  / v_whday)::numeric, 9)::float8 AS SELF_SUFF_WT_BT  
        FROM (  	
       			select	A.RATE, 
       					A.FROM_TM, 
       					A.CONS_DAY, 
       					0.5 * SIN(A.RATE / 12 * (asin(1)*2)::numeric(14,13)) * B.PANEL_PW AS PV_PW_PER_HALFHOUR, 
       					CASE 
       						WHEN 0.5 * SIN(A.RATE / 12 * (asin(1)*2)::numeric(14,13)) * B.PANEL_PW - A.CONS_DAY < 0 THEN 0  	   	  		
       						ELSE 0.5 * SIN(A.RATE / 12 * (asin(1)*2)::numeric(14,13)) * B.PANEL_PW - A.CONS_DAY   	   	 
       					END AS PV_CONS_PER_HALFHOUR , 
       					B.PANEL_PW  	
       			FROM  		( 
       							select	FROM_TM, 
       									RATE, 
       									CASE 
       										WHEN FROM_TM IN ('0700', '0730') THEN v_whday / 48*2  		  		
       										ELSE v_whday / 56  		  	  
       									END AS CONS_DAY  		  
       							FROM   SELFCSM_RATE  ) A  , 
       						( 
       							select generate_series(1,6,1) AS PANEL_PW  ) B  
       			) C  
       	GROUP BY  	PANEL_PW  
        ORDER BY  	PANEL_PW ;

    END IF;
END;
$$;


ALTER FUNCTION wems.fn_get_self_consumption(p_device_id text, p_colec_dd text, p_whbt numeric, p_whday numeric) OWNER TO wems;

--
-- TOC entry 1093 (class 1255 OID 1895855)
-- Name: fn_get_split(text, integer, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_split(p_str text, p_field_number integer, p_delim text) RETURNS text
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_GET_SPLIT
   PURPOSE: 문자열을 구분자로 자른 N번째 문자 반환
                Postgresql에서 제공하는 함수를 사용 권고

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-29   yngwie      Created this function.
   2.0        2019-09-25   박동환       Postgresql 변환

   NOTES:   입력받은 문자열을 구분자를 기준으로 잘라 반환한다.

	Input : p_str - 문자열
	        p_field_number - 구분자를 기준으로 문자열의 N번째 문자
	        p_delim - 구분자

    Output : VARCHAR - 구분자를 기준으로 자른 문자열

	Usage :
	        SELECT FN_GET_SPLIT('123|45|678', 3, '|');

******************************************************************************/
DECLARE
	o_result	TEXT := null;

BEGIN
	SELECT 	SPLIT_PART(p_str, p_delim, p_field_number)
	INTO 		o_result;

    RETURN o_result;

END;
$$;


ALTER FUNCTION wems.fn_get_split(p_str text, p_field_number integer, p_delim text) OWNER TO wems;

--
-- TOC entry 1141 (class 1255 OID 1895856)
-- Name: fn_get_std_col_name(text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_std_col_name(p_tbl text) RETURNS text
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_GET_STD_COL_NAME
   PURPOSE:   파티션테이블의 기준컬럼명 조회

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2019-10-02   박동환        Postgresql 변환

   NOTES:   입력테이블에 대한 파티션 기준컬럼명 조회
   
    Input :  테이블
	Output : 입력 테이블의 파티션 기준컬럼명 
	
	TEST : SELECT FN_GET_STD_COL_NAME('TB_OPR_ALARM_HIST');   
	         --> 'EVENT_DT'
******************************************************************************/	
DECLARE
    v_ret TEXT := '';
BEGIN
    SELECT   STD_COL_NM     
    INTO		v_ret
   	FROM 	TB_PLF_TBL_META
   	WHERE 	TABLE_NM = p_tbl
    ;
   
    RETURN v_ret;
END;
$$;


ALTER FUNCTION wems.fn_get_std_col_name(p_tbl text) OWNER TO wems;

--
-- TOC entry 1094 (class 1255 OID 1895857)
-- Name: fn_get_wkday_flag(text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_get_wkday_flag(p_curymd text) RETURNS text
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_GET_WKDAY_FLAG
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-05-13   yngwie       Created this function.
   2.0        2019-09-24   이재형        Postgresql 변환
   
   NOTES:   입력받은  인자들에 해당하는 TB_BAS_ELPW_UNIT_PRICE.WKDAY_FLAG 를 반환한다.

	Input : p_curYmd - 해당 년월일

    Output : VARCHAR

	Usage :
	        SELECT FN_GET_WKDAY_FLAG('20150701');

******************************************************************************/
BEGIN
    RETURN
	    CASE
	        WHEN TO_CHAR(TO_DATE(p_curYmd, 'YYYYMMDD'), 'd') IN ('1', '7') THEN '0'
	        ELSE '1'
	    END;
	   
    EXCEPTION WHEN OTHERS THEN
            RETURN '1';

END;
$$;


ALTER FUNCTION wems.fn_get_wkday_flag(p_curymd text) OWNER TO wems;

--
-- TOC entry 1142 (class 1255 OID 1895858)
-- Name: fn_is_dst(text, text, text, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_is_dst(p_srcstr text, p_srcdf text, p_start text, p_end text) RETURNS text
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_IS_DST
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-05-13   yngwie       1. Created this function.
   2.0        2019-10-07   박동환        postgres 변환

   NOTES:   해당 날짜가 DST 시즌인지 아닌지의 여부를 반환한다.


	Input : p_srcStr - 조회 년월일시
	        p_srcDf -  조회 년월일시 포맷
	        p_start - TB_BAS_TIMEZONE 테이블의  DST_START_EXP
	        p_end - TB_BAS_TIMEZONE 테이블의  DST_END_EXP

    Output : NUMBER 0 - DST 시즌이 아닐 경우, 1 - DST 시즌일 경우

	Usage :
	        SELECT
				A.*
				, FN_IS_DST('2015040100', 'YYYYMMDDHH24', DST_START_EXP, DST_END_EXP)
			FROM
				TB_BAS_TIMEZONE

********************************************************************************/
DECLARE
    v_sql			TEXT;
    v_sql_au 		TEXT;
    v_result 	NUMERIC := -1;

    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3
begin
	
    IF p_start IS NULL OR p_end IS NULL THEN
        v_result := 0;
    ELSE
    	-- DST적용국가(호주)
    	IF POSITION('{{prevYear}}' IN p_start) > 0  THEN
	    	v_sql := FORMAT(
	    			'SELECT SUM(RESULT) FROM (		' || 
						'SELECT CASE WHEN ''%s'' BETWEEN %s AND %s THEN 1 ELSE 0 END AS RESULT ' || 
						'UNION ALL '
						, p_srcStr
						, p_start
						, p_end )	;

            v_sql := REPLACE(v_sql, '{{year}}', SUBSTR(p_srcStr, 1, 4));
            v_sql := REPLACE(v_sql, '{{prevYear}}', TO_CHAR(SUBSTR(p_srcStr, 1, 4)::NUMERIC - 1, 'FM0000'));
            v_sql := REPLACE(v_sql, '{{df}}', p_srcDf);
           
   			v_sql_au   := FORMAT(
					'SELECT CASE WHEN ''%s'' BETWEEN %s AND %s THEN 1 ELSE 0 END AS RESULT) S'
					, p_srcStr
					, p_start
					, p_end )	;

            v_sql_au := REPLACE(v_sql_au, '{{prevYear}}', SUBSTR(p_srcStr, 1, 4));
            v_sql_au := REPLACE(v_sql_au, '{{year}}', TO_CHAR(SUBSTR(p_srcStr, 1, 4)::NUMERIC + 1, 'FM0000'));
            v_sql_au := REPLACE(v_sql_au, '{{df}}', p_srcDf);
            v_sql := CONCAT_WS('', v_sql, v_sql_au);
        ELSE
			v_sql   := FORMAT(
					'SELECT CASE WHEN ''%s'' BETWEEN %s AND %s THEN 1 ELSE 0 END'
					, p_srcStr
					, p_start
					, p_end );
				
            v_sql := REPLACE(v_sql, '{{year}}', SUBSTR(p_srcStr, 1, 4));
            v_sql := REPLACE(v_sql, '{{df}}', p_srcDf);
        END IF;
       
        EXECUTE  v_sql INTO v_result;
       
    END IF;
   
    RETURN v_result;
   
    EXCEPTION WHEN OTHERS then
    	/*
		GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
	    RAISE NOTICE 'FN_IS_DST >>> 오류[%] SQL[%]', v_err_msg, v_sql;
	    */
        RETURN - 1;
       
END;
$$;


ALTER FUNCTION wems.fn_is_dst(p_srcstr text, p_srcdf text, p_start text, p_end text) OWNER TO wems;

--
-- TOC entry 1143 (class 1255 OID 1895859)
-- Name: fn_long2char(text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_long2char(p_parti_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_LONG2CHAR
   PURPOSE: 입력 파티션명의 마지막 경계(bound) 조회
                함수명 변경 필요(예, FN_GET_PATITION_UP_BOUND)

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-29   yngwie      Created this function.
   2.0        2019-09-25   박동환       Postgresql 변환

   NOTES:   입력받은 문자열을 구분자를 기준으로 잘라 반환한다.

	Input : p_parti_name - 파티션명

    Output : TEXT - 입력 파티션의 bound(마지막 범위)

	Usage :
	        SELECT FN_LONG2CHAR('tb_opr_alarm_hist_pt_opr_alarm_hist_201905');   
	        -- 결과 : '20190601000000'

******************************************************************************/
DECLARE
    o_result TEXT := NULL;
   
begin
	SELECT 	CHR(39) || SPLIT_PART(pg_get_expr(relpartbound, oid, false), '''', 4)  || CHR(39)
	INTO 		o_result
	FROM 	pg_class
	WHERE 	relname = p_parti_name; -- 'tb_opr_alarm_hist_pt_opr_alarm_hist_201905';	
	
    RETURN o_result;
   
END;
$$;


ALTER FUNCTION wems.fn_long2char(p_parti_name text) OWNER TO wems;

--
-- TOC entry 1145 (class 1255 OID 1895860)
-- Name: fn_sp_exe_ed_log(character varying, numeric, character varying, timestamp without time zone, character varying, integer, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_sp_exe_ed_log(i_sp_nm character varying, i_job_seq numeric, i_exe_base character varying, i_st_dt timestamp without time zone, i_rslt_cd character varying, i_rslt_cnt integer, i_err_msg character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
  프로그램ID   : fn_sp_exe_ed_log
  작  성  일   : 2019-09-20
  작  성  자   : 박동환
  프로그램용도 : 프로시져실행이력 종료로그 기록
  참 고 사 항  :
                 < PROTOTYPE >
                   fn_sp_exe_ed_log

                 <PARAMETER>
                   < INPUT >
                     1.i_sp_nm		프로시져명
                     2.i_job_seq	서비스대상번호
                     3.i_exe_base	실행기준값(월별    :YYYYMM 
											   일별    :YYYYMMDD 
											   시간대별:YYYYMMDDHH0000 
											   15분    :YYYYMMDDHHMI00)
                     4.i_st_dt     	시작일시
                     5.i_rslt_cd   	실행상태코드(0:실행중,1:실행성공,그외:실행오류) 
                     6.i_rslt_cnt  	실행결과건수
                     7.i_err_msg   	오류메시지
                   < OUTPUT >
                     NONE

                 < MODULE DIAGRAM >
                 1.변수 및 커서 선언
                 2.프로시져실행이력 변경

  ----------------------------------------------------------------------------
  TABLE                                                                  CRUD
  ============================================================================
  TB_SYS_SP_EXE_HST(프로시져실행이력)												U
  ----------------------------------------------------------------------------

  ----------------------------------------------------------------------------
  수정일자    수정자      수정내용
  =========== =========== ====================================================
  20190920    박동환      최초작성
  ----------------------------------------------------------------------------

*****************************************************************************/

DECLARE
  ----------------------------------------------------------------------------
	--1.변수 및 커서 선언
  ----------------------------------------------------------------------------
  v_lap_time 	NUMERIC   := 0.000;             --경과초수
  v_ed_dt		TIMESTAMP := CLOCK_TIMESTAMP(); --종료일시

BEGIN
	
  ----------------------------------------------------------------------------
	--2.프로시져실행이력 변경
  ----------------------------------------------------------------------------
	  BEGIN  
		    SELECT DATE_PART('HOUR',   v_ed_dt-i_st_dt)*3600 + 
		           DATE_PART('MINUTE', v_ed_dt-i_st_dt)*60   +
		           DATE_PART('SECOND', v_ed_dt-i_st_dt)::NUMERIC(10,2)
		    INTO v_lap_time;

--    		RAISE notice 'FN_SP_EXE_ED_LOG - SP_NAME[%] JOB_SEQ[%] EXE_BASE[%] ST_DT[%]', 	i_sp_nm,	i_job_seq, i_exe_base, i_st_dt;
		              
			UPDATE TB_SYS_SP_EXE_HST
			   SET ed_dt    = v_ed_dt
			     , lap_time =  v_lap_time
			     , rslt_cd  = i_rslt_cd
			     , rslt_cnt = i_rslt_cnt
			     , err_msg  = i_err_msg
			 WHERE sp_nm 	= i_sp_nm
			   AND exe_base = i_exe_base
			   AND job_seq = i_job_seq
			   AND st_dt    = i_st_dt;
  
    EXCEPTION WHEN OTHERS THEN
    	RAISE notice 'FN_SP_EXE_ED_LOG EXCEPTION !!!  - SP_NAME[%] JOB_SEQ[%] ', 	i_sp_nm,	i_job_seq;
    
    END;
END;
$$;


ALTER FUNCTION wems.fn_sp_exe_ed_log(i_sp_nm character varying, i_job_seq numeric, i_exe_base character varying, i_st_dt timestamp without time zone, i_rslt_cd character varying, i_rslt_cnt integer, i_err_msg character varying) OWNER TO wems;

--
-- TOC entry 1096 (class 1255 OID 1895861)
-- Name: fn_sp_exe_st_log(character varying, numeric, character varying, timestamp without time zone, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_sp_exe_st_log(i_sp_nm character varying, i_job_seq numeric, i_exe_base character varying, i_st_dt timestamp without time zone, i_exe_mthd character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
  프로그램ID   : fn_sp_exe_st_log
  작  성  일   : 2019-09-20
  작  성  자   : 박동환
  프로그램용도 : 프로시져실행이력 시작로그 기록
  참 고 사 항  :
                 < PROTOTYPE >
                   fn_sp_exe_st_log

                 <PARAMETER>
                   < INPUT >
                     1.i_sp_nm		프로시져명
                     2.i_job_seq	서비스대상번호
                     3.i_exe_base	실행기준값(월별    :YYYYMM 
											   일별    :YYYYMMDD 
											   시간대별:YYYYMMDDHH0000 
											   15분    :YYYYMMDDHHMI00)
                     4.i_st_dt      시작일시
                     5.i_exe_mthd   실행사유모드(A:자동, M:수동)
                   < OUTPUT >
                     NONE

                 < MODULE DIAGRAM >
                 1.변수 및 커서 선언
                 2.프로시져실행이력 등록

  ----------------------------------------------------------------------------
  TABLE                                                                  CRUD
  ============================================================================
  TB_SYS_SP_EXE_HST(프로시져실행이력)												C
  ----------------------------------------------------------------------------

  ----------------------------------------------------------------------------
  수정일자    수정자      수정내용
  =========== =========== ====================================================
  20190920    박동환      최초작성
  ----------------------------------------------------------------------------

*****************************************************************************/

DECLARE
  ----------------------------------------------------------------------------
	--1.변수 및 커서 선언
  ----------------------------------------------------------------------------

BEGIN

  ----------------------------------------------------------------------------
	--2.프로시져실행이력 등록
  ----------------------------------------------------------------------------
	INSERT INTO TB_SYS_SP_EXE_HST
	     ( SP_NM            --프로그램명
	     , JOB_SEQ         --수행일련번호
	     , EXE_BASE         --실행기준값
	     , ST_DT            --시작일시
	     , ED_DT            --종료일시
	     , LAP_TIME    		--경과초수
	     , EXE_MTHD         --실행사유코드
	     , RSLT_CD			--결과코드(0:실행중,1:실행성공,2:실행중오류)
	     , RSLT_CNT         --적재수
	     , ERR_MSG			--오류메시지
	     )
	VALUES 
	     ( i_sp_nm
	     , i_job_seq
	     , i_exe_base
	     , i_st_dt
	     , NULL
	     , 0
	     , i_exe_mthd
	     , '0'
	     , 0
	     , NULL
	     );

END;
$$;


ALTER FUNCTION wems.fn_sp_exe_st_log(i_sp_nm character varying, i_job_seq numeric, i_exe_base character varying, i_st_dt timestamp without time zone, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1097 (class 1255 OID 1895862)
-- Name: fn_ts_cal_utc(numeric); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_ts_cal_utc(i_gap numeric) RETURNS timestamp without time zone
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_TS_CAL_UTC
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-25   yngwie      Created this function.
   2.0        2019-09-23   박동환       Postgresql 전환

   NOTES:   입력받은 GMT 시간차에 해당하는 날짜의 Timestamp 형태 반환

	Input : i_gap - GMT 시간차

    Output : TIMESTAMP

	Usage :
	        FN_TS_CAL_UTC(9)

******************************************************************************/
DECLARE
    o_return timestamp := NULL;
BEGIN

  	SELECT SYS_EXTRACT_UTC(now()) - concat(-i_gap,' hours')::interval
	INTO o_return;

	RETURN o_return;

	EXCEPTION WHEN others THEN 
   		RETURN o_return;

END;
$$;


ALTER FUNCTION wems.fn_ts_cal_utc(i_gap numeric) OWNER TO wems;

--
-- TOC entry 1146 (class 1255 OID 1895863)
-- Name: fn_ts_str_to_str(text, numeric, text, numeric, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_ts_str_to_str(p_srcstr text, p_srctz numeric, p_srcdf text, p_targettz numeric, p_targetdf text) RETURNS text
    LANGUAGE plpgsql IMMUTABLE
    AS $$
/******************************************************************************
   NAME:       FN_TS_STR_TO_STR
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-11   yngwie      Created this function.
   2.0        2019-09-23   박동환       Postgresql 전환

   NOTES:   입력받은 날짜형식의 문자열을 TIMESTAMP WITH TIME ZONE 형식으로 변환하여
            GMT 0 의 날짜형식의 문자열로 반환한다.

   TEST :
			SELECT
				TO_TIMESTAMP_TZ('20140612005800 +01', 'YYYYMMDDHH24MISS TZH') a -- SRC TS 를 TS WITH TIMEZONE 으로 변경
				, TO_CHAR( TO_TIMESTAMP_TZ('20140612005800 +01', 'YYYYMMDDHH24MISS TZH') ) b -- a 컬럼 TO_CHAR 로 변환
				, TO_CHAR( SYS_EXTRACT_UTC(TO_TIMESTAMP_TZ('20140612005800 +01', 'YYYYMMDDHH24MISS TZH')) ) c -- a 를 GMT 0 으로 변환
				, TO_CHAR( SYS_EXTRACT_UTC(TO_TIMESTAMP_TZ('20140612005800 +01', 'YYYYMMDDHH24MISS TZH')) + NUMTODSINTERVAL(9, 'HOUR') ) d -- c컬럼 TO_CHAR 로 변환
				, FN_TS_STR_TO_STR('20140612005800', 1, 'YYYYMMDDHH24MISS', 9, 'YYYYMMDDHH24MISS') e -- 함수 테스트
			FROM DUAL;


	Input : p_srcStr - 날짜형식의 문자열 (ex : 2014-04-21 01:00:00)
	        p_srcTz - p_dateStr 의 GMT Offset (-11 ~ 12)
	        p_srcDf - p_dateStr 의 날짜포맷(ex : YYYY-MM-DD HH24:MI:SS)
	        p_targetTz - 반환값의 GMT Offset (-11 ~ 12)
			p_targetDf - 반환값의 날짜포맷(ex : YYYY-MM-DD HH24:MI:SS)

	Usage :
	        FN_TS_STR_TO_STR('20140612005800', 1, 'YYYYMMDDHH24MISS', 9, 'YYYYMMDDHH24MISS')

******************************************************************************/
DECLARE
    o_return VARCHAR   := NULL;
BEGIN
	
	select CASE p_srcstr WHEN '' THEN '' ELSE to_char(to_timestamp(p_srcstr, p_srcdf) - concat(p_srctz,' hours')::interval - concat(-p_targettz,' hours')::interval, p_targetdf) END
	into o_return;

	return o_return;
   
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$$;


ALTER FUNCTION wems.fn_ts_str_to_str(p_srcstr text, p_srctz numeric, p_srcdf text, p_targettz numeric, p_targetdf text) OWNER TO wems;

--
-- TOC entry 1147 (class 1255 OID 1895864)
-- Name: fn_ts_str_to_str(text, numeric, text, text, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_ts_str_to_str(p_srcstr text, p_srctz numeric, p_srcdf text, p_targettz text, p_targetdf text) RETURNS text
    LANGUAGE plpgsql IMMUTABLE
    AS $$
/******************************************************************************
   NAME:       FN_TS_STR_TO_STR
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-11   yngwie      Created this function.
   2.0        2019-09-23   박동환       Postgresql 전환
   2.1        2019-10-14   김현덕       UI에서 fn_ts_str_to_str(text, integer, unknown, text, unknown) 과같이 호출을 하는 부분에 대응하기위해 추가 

   NOTES:   입력받은 날짜형식의 문자열을 TIMESTAMP WITH TIME ZONE 형식으로 변환하여
            GMT 0 의 날짜형식의 문자열로 반환한다.

   TEST :
			SELECT
				TO_TIMESTAMP_TZ('20140612005800 +01', 'YYYYMMDDHH24MISS TZH') a -- SRC TS 를 TS WITH TIMEZONE 으로 변경
				, TO_CHAR( TO_TIMESTAMP_TZ('20140612005800 +01', 'YYYYMMDDHH24MISS TZH') ) b -- a 컬럼 TO_CHAR 로 변환
				, TO_CHAR( SYS_EXTRACT_UTC(TO_TIMESTAMP_TZ('20140612005800 +01', 'YYYYMMDDHH24MISS TZH')) ) c -- a 를 GMT 0 으로 변환
				, TO_CHAR( SYS_EXTRACT_UTC(TO_TIMESTAMP_TZ('20140612005800 +01', 'YYYYMMDDHH24MISS TZH')) + NUMTODSINTERVAL(9, 'HOUR') ) d -- c컬럼 TO_CHAR 로 변환
				, FN_TS_STR_TO_STR('20140612005800', 1, 'YYYYMMDDHH24MISS', 9, 'YYYYMMDDHH24MISS') e -- 함수 테스트
			FROM DUAL;


	Input : p_srcStr - 날짜형식의 문자열 (ex : 2014-04-21 01:00:00)
	        p_srcTz - p_dateStr 의 GMT Offset (-11 ~ 12)
	        p_srcDf - p_dateStr 의 날짜포맷(ex : YYYY-MM-DD HH24:MI:SS)
	        p_targetTz - 반환값의 GMT Offset (-11 ~ 12)
			p_targetDf - 반환값의 날짜포맷(ex : YYYY-MM-DD HH24:MI:SS)

	Usage :
	        FN_TS_STR_TO_STR('20140612005800', 1, 'YYYYMMDDHH24MISS', 9, 'YYYYMMDDHH24MISS')

******************************************************************************/
DECLARE
    o_return VARCHAR   := NULL;
BEGIN
	
	select CASE p_srcstr WHEN '' THEN '' ELSE to_char(to_timestamp(p_srcstr, p_srcdf) - concat(p_srctz,' hours')::interval - concat(-p_targettz::numeric,' hours')::interval, p_targetdf) END
	into o_return;

	return o_return;
   
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$$;


ALTER FUNCTION wems.fn_ts_str_to_str(p_srcstr text, p_srctz numeric, p_srcdf text, p_targettz text, p_targetdf text) OWNER TO wems;

--
-- TOC entry 1148 (class 1255 OID 1895865)
-- Name: fn_ts_str_to_str_gmt(text, numeric, text, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.fn_ts_str_to_str_gmt(p_datestr text, p_tz numeric, p_srcdf text, p_targetdf text) RETURNS text
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       FN_TS_STR_TO_STR_GMT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-11   yngwie       1. Created this function.
   2.0        2019-09-23   박동환       Postgresql 전환

   NOTES:   입력받은 날짜형식의 문자열을 TIMESTAMP WITH TIME ZONE 형식으로 변환하여
            GMT 0 의 날짜형식의 문자열로 반환한다.
            => 결과적으로 입력된 시간에  OFFSET만큼 마이너스한 값과 동일(박동환)

	Input : p_dateStr - 날짜형식의 문자열 (ex : 2014-04-21 01:00:00)
	        p_tz - p_dateStr 의 GMT Offset (-11 ~ 12)
	        p_srcDf - p_dateStr 의 날짜포맷(ex : YYYY-MM-DD HH24:MI:SS)
	        p_targetDf - 반환값의 날짜포맷 (ex : YYYYMMDDHH24MISS)


	Usage : SELECT 절에서만 사용하고, 조건절에서는 사용하지 마세요. 알수 없는 오류 발생함.
	        SELECT FN_TS_STR_TO_STR_GMT('2014-06-11 08:12:00', 1, 'YYYY-MM-DD HH24:MI:SS', 'YYYYMMDD HH24MISS')  FROM DUAL;


******************************************************************************/
DECLARE
    o_return text := NULL;
BEGIN
 	select to_char(to_timestamp(p_datestr, p_srcdf) - concat(p_tz,' hours')::interval, p_targetdf)
	into o_return;

	return o_return;

     EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$$;


ALTER FUNCTION wems.fn_ts_str_to_str_gmt(p_datestr text, p_tz numeric, p_srcdf text, p_targetdf text) OWNER TO wems;

--
-- TOC entry 1098 (class 1255 OID 1895866)
-- Name: hex2dec(character); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.hex2dec(hexval character) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
     /*
     *****************************************************************************
       NAME:       HEX2DEC
       PURPOSE:
    
       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
  	   2.0        2019-10-08     이재형                  postgresql 변환    
  	   
       NOTES:   HEX(16진수) to  Decimal(10진수)
    
        Input : 
    
        Output :
    
     *****************************************************************************
     */
DECLARE
    i numeric;
    digits numeric;
    result numeric := 0;
    current_digit CHARACTER(1);
    current_digit_dec numeric;
BEGIN
    digits := LENGTH(hexval);

    FOR i IN 1..digits LOOP
        current_digit := substr(hexval, i, 1);

        IF current_digit IN ('A', 'B', 'C', 'D', 'E', 'F') THEN
            current_digit_dec := ASCII(current_digit) - ASCII('A') + 10;
        ELSE
            current_digit_dec := current_digit::numeric;
        END IF;
        result := (result * 16) + current_digit_dec;
    END LOOP;
    RETURN result;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$$;


ALTER FUNCTION wems.hex2dec(hexval character) OWNER TO wems;

--
-- TOC entry 1149 (class 1255 OID 1895867)
-- Name: instr(text, text, integer, integer); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.instr(str text, sub text, startpos integer DEFAULT 1, occurrence integer DEFAULT 1) RETURNS integer
    LANGUAGE plpgsql
    AS $$
declare 
    tail text;
    shift int;
    pos int;
    i int;
begin
    shift:= 0;
    if startpos = 0 or occurrence <= 0 then
        return 0;
    end if;
    if startpos < 0 then
        str:= reverse(str);
        sub:= reverse(sub);
        pos:= -startpos;
    else
        pos:= startpos;
    end if;
    for i in 1..occurrence loop
        shift:= shift+ pos;
        tail:= substr(str, shift);
        pos:= strpos(tail, sub);
        if pos = 0 then
            return 0;
        end if;
    end loop;
    if startpos > 0 then
        return pos+ shift- 1;
    else
        return length(str)- pos- shift+ 1;
    end if;
end $$;


ALTER FUNCTION wems.instr(str text, sub text, startpos integer, occurrence integer) OWNER TO wems;

--
-- TOC entry 1144 (class 1255 OID 1895868)
-- Name: last_day(date); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.last_day(p_date date) RETURNS date
    LANGUAGE plpgsql
    AS $_$
/******************************************************************************
   NAME:       last_day
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2019-10-07   박동환       Tibero 내장함수 Postgresql 변환
   
   NOTES:   입력월의 마지막일자 조회

	Input : p_date - date

    Output : date

	Usage :
	        SELECT LAST_DAY(TO_DATE('20191007', 'YYYYMMDD'));
	        => 2019-10-31

******************************************************************************/
DECLARE
    v_result DATE;
   
BEGIN
    SELECT 	(date_trunc('MONTH', $1) + INTERVAL '1 MONTH - 1 day')::date
    INTO 		v_result;
   
   RETURN v_result;
   
END;
$_$;


ALTER FUNCTION wems.last_day(p_date date) OWNER TO wems;

--
-- TOC entry 1099 (class 1255 OID 1895869)
-- Name: next_day(date, integer); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.next_day(p_date date, p_weekday integer) RETURNS date
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       next_day
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2019-10-07   박동환       Tibero 내장함수 Postgresql 변환
   
   NOTES:   입력일 초과 후 입력요일 조회

	Input : p_date - date

    Output : date

	Usage :
	        SELECT NEXT_DAY(TO_DATE('20191007', 'YYYYMMDD'), 1);		-- 1:일요일
	        ==> 2019-10-13 (기준일 이후 일요일)

******************************************************************************/
DECLARE
       v_dow				integer;
       v_add_days		integer;
      
BEGIN
	v_dow := to_number(to_char(p_date,'D'),	'9');
	
	IF v_dow < p_weekday	THEN
	     v_add_days := p_weekday - v_dow;
    ELSE
	     v_add_days := p_weekday - v_dow + 7;
	END IF;

	RETURN p_date + CONCAT(v_add_days, ' DAY')::INTERVAL;
   
END;
$$;


ALTER FUNCTION wems.next_day(p_date date, p_weekday integer) OWNER TO wems;

--
-- TOC entry 1100 (class 1255 OID 1895870)
-- Name: nvl(double precision, integer); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.nvl(expr1 double precision, expr2 integer) RETURNS text
    LANGUAGE plpgsql
    AS $_$
	DECLARE
		v_result	VARCHAR(10000);	  
	BEGIN
		SELECT coalesce($1, $2) into v_result ;
		return v_result;
	end;
$_$;


ALTER FUNCTION wems.nvl(expr1 double precision, expr2 integer) OWNER TO wems;

--
-- TOC entry 1150 (class 1255 OID 1895871)
-- Name: nvl(text, text); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.nvl(expr1 text, expr2 text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
	DECLARE
		v_result	VARCHAR(10000);	  
	BEGIN
		SELECT coalesce($1, $2) into v_result ;
		return v_result;
	end;
$_$;


ALTER FUNCTION wems.nvl(expr1 text, expr2 text) OWNER TO wems;

--
-- TOC entry 1151 (class 1255 OID 1895872)
-- Name: oct2dec(character); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.oct2dec(octval character) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
     /*
     *****************************************************************************
       NAME:       OCT2DEC
       PURPOSE:
    
       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
  	   2.0        2019-10-08     이재형                  postgresql 변환    
  	   
       NOTES:   oct(8진수) to  Decimal(10진수)
    
        Input : 
    
        Output :
    
     *****************************************************************************
     */
DECLARE
    i numeric;
    digits numeric;
    result numeric := 0;
    current_digit CHARACTER(1);
    current_digit_dec numeric;
BEGIN
    digits := LENGTH(octval);

    FOR i IN 1..digits LOOP
        current_digit := substr(octval, i, 1);
        current_digit_dec := current_digit::numeric;
        result := (result * 8) + current_digit_dec;
    END LOOP;
    RETURN result;
    EXCEPTION
        WHEN others THEN
            RETURN NULL;
END;
$$;


ALTER FUNCTION wems.oct2dec(octval character) OWNER TO wems;

--
-- TOC entry 1152 (class 1255 OID 1895873)
-- Name: sp_add_unit_price_mm(text, text, numeric, numeric, numeric, text, text); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_add_unit_price_mm(p_prod_cd text, p_ym text, p_fromhour numeric, p_tohour numeric, p_price numeric, p_wkday text, p_io text)
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_ADD_UNIT_PRICE_MM
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-08-21   yngwie   Created this function.
   2.0        2019-09-25   이재형    postgresql 변환
   
   NOTES:   기준 요금제를 월 단위로 추가한다.

	Input : p_prod_cd - 요금제코드
	        p_ym - 적용년월
	        p_fromHour - 적용시작시각
	        p_toHour - 적용종료시각
	        p_price - 단가
	        p_wkday - 주중, 주말 구분(1: 주중, 0:주말)
	        p_io - 매/수가 구분(O:수가, T:매가)

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
    v_log_fn 		VARCHAR := 'SP_ADD_UNIT_PRICE_MM';
    v_log_seq 	NUMERIC := 0; 		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd	VARCHAR := 'M';							-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt		TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd		VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt		INTEGER   := 0;     						-- 적재수
    v_work_num	INTEGER   := 0;     						-- 처리건수
    v_err_msg	VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1	VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2	VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3	VARCHAR   := NULL;  					-- 메시지내용3
	
    p_dt 			VARCHAR   := ' ' ;
   
    -- (2)업무처리 변수

BEGIN

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
    
	    -- 기존에 입력된 요금제가 있으면 삭제
	    DELETE FROM TB_BAS_ELPW_UNIT_PRICE
	    WHERE 	elpw_prod_cd = p_prod_cd 
	    AND 		unit_ym = p_ym 
	    AND 		unit_hh BETWEEN TO_CHAR(p_fromHour, 'FM00') AND TO_CHAR(p_toHour, 'FM00') 
	    AND 		wkday_flag = p_wkday 
	    AND 		io_flag = p_io;
	
	    FOR y IN p_fromHour..p_toHour LOOP
	        INSERT INTO TB_BAS_ELPW_UNIT_PRICE (elpw_prod_cd, unit_ym, unit_hh, io_flag, unit_price, wkday_flag, use_flag, create_id)
	        VALUES (p_prod_cd, p_ym, TO_CHAR(y, 'FM00'), p_io, p_price, p_wkday, 'Y', 'SYSTEM');
	    END LOOP;
	    
	
	    GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	     v_rslt_cnt := v_rslt_cnt + v_work_num;

	    -- 실행성공
		v_rslt_cd := '1';
    
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
END;
$$;


ALTER PROCEDURE wems.sp_add_unit_price_mm(p_prod_cd text, p_ym text, p_fromhour numeric, p_tohour numeric, p_price numeric, p_wkday text, p_io text) OWNER TO wems;

--
-- TOC entry 1153 (class 1255 OID 1895875)
-- Name: sp_add_unit_price_yy(text, text, numeric, numeric, numeric, text, text); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_add_unit_price_yy(p_prod_cd text, p_year text, p_fromhour numeric, p_tohour numeric, p_price numeric, p_wkday text, p_io text)
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_ADD_UNIT_PRICE_YY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-08-21   yngwie    Created this function.
   2.0        2019-09-25   이재형     postgresql 변환
   
   NOTES:   기준 요금제를 연 단위로 추가한다.

	Input : p_prod_cd - 요금제코드
	        p_year - 적용년
	        p_fromHour - 적용시작시각
	        p_toHour - 적용종료시각
	        p_price - 단가
	        p_wkday - 주중, 주말 구분(1: 주중, 0:주말)
	        p_io - 매/수가 구분(O:수가, T:매가)

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
    v_log_fn 			VARCHAR := 'SP_ADD_UNIT_PRICE_YY';
    v_log_seq 		NUMERIC := 0;		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';							-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3
	
    p_dt 				VARCHAR   	:= ' ' ;
   
    -- (2)업무처리 변수

BEGIN

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
    
	    FOR x IN 1..12 LOOP
	        PERFORM wems.SP_ADD_UNIT_PRICE_MM(p_prod_cd, p_year||TO_CHAR(x, 'FM00'), p_fromHour, p_toHour, p_price, p_wkday, p_io);
	    END LOOP;
	
	    -- 수행시간 계산
	    GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	    v_rslt_cnt := v_rslt_cnt + v_work_num;
    
    
	    --실행성공
	    v_rslt_cd := '1';
    
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
END;
$$;


ALTER PROCEDURE wems.sp_add_unit_price_yy(p_prod_cd text, p_year text, p_fromhour numeric, p_tohour numeric, p_price numeric, p_wkday text, p_io text) OWNER TO wems;

--
-- TOC entry 1101 (class 1255 OID 1895876)
-- Name: sp_aggr_admin_ess_state(); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_aggr_admin_ess_state()
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_AGGR_ADMIN_ESS_STATE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-09-04   yngwie       1. Created this function.
   1.1        2015-09-19   yngwie       1. UI 에서 SQL 로 조회하도록 변경, 사용하지 않음
   2.0        2019-09-20   박동환        Postgresql 변환

   NOTES: 전체 ESS 운용 현황 집계

    Input :

    Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_AGGR_ADMIN_ESS_STATE';
    v_log_seq 		NUMERIC := 0;	-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';							-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    p_dt				VARCHAR   := ''; 
   
BEGIN

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';

--	    RAISE NOTICE 'v_log_fn:[%] v_log_seq[%] START[%] p_dt[%]', v_log_fn, v_log_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), p_dt;

    	DELETE FROM TB_OPR_ADMIN_ESS_STATE;
        GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--     	RAISE NOTICE 'v_log_fn[%] Delete TB_OPR_ADMIN_ESS_STATE Table [%]건', v_log_fn, v_work_num;

		v_rslt_cd := 'B';
    
        INSERT INTO TB_OPR_ADMIN_ESS_STATE ( 
            CNTRY_CD, COLEC_DD
            , INSTLR_CNT, INSTLR_NA_CNT, TOT_CNT        
            , NML_CNT, NML_PER        
            , WARN_CNT, WARN_PER   
            , ERR_CNT, ERR_PER        
            , NO_SIG_CNT, NO_SIG_PER     
            , STD_CNT, STD_PER        
            , RCT_INSTL_CNT 
            , CREATE_DT 
        ) 
         WITH NU AS (
            SELECT		COUNT(*) AS CNT
            FROM (
                SELECT	A.USER_ID
                FROM    TB_BAS_USER A
                WHERE   DEC_VARCHAR_SEL(A.AUTH_TYPE_CD, 10, 'SDIENCK', 'TB_BAS_USER', 'AUTH_TYPE_CD') = '20'
                GROUP BY  A.USER_ID
                EXCEPT 	-- ORACLE에선 MINUS
                SELECT   A.USER_ID
                FROM    TB_BAS_USER A
                   		  , TB_BAS_DEVICE B
                WHERE   DEC_VARCHAR_SEL(A.AUTH_TYPE_CD, 10,  'SDIENCK', 'TB_BAS_USER', 'AUTH_TYPE_CD') = '20'
                    AND 	A.USER_ID = B.INSTL_USER_ID
                GROUP BY A.USER_ID
            ) T
         ), U AS (
            SELECT 
                CNTRY_CD
                , COUNT(*) AS CNT
            FROM (
                SELECT	CNTRY_CD
                    	,  B.INSTL_USER_ID
                FROM
                    TB_BAS_USER A
                    , TB_BAS_DEVICE B
                WHERE 
                    DEC_VARCHAR_SEL(A.AUTH_TYPE_CD, 10, 'SDIENCK', 'TB_BAS_USER', 'AUTH_TYPE_CD') = '20'
                    AND A.USER_ID = B.INSTL_USER_ID
                GROUP BY
                    B.CNTRY_CD
                    , B.INSTL_USER_ID
            ) Z
            GROUP BY   CNTRY_CD
         ), A AS (
            SELECT 
                MAX(U.CNT) AS INSTLR_CNT
                , MAX(NU.CNT) AS INSTLR_NA_CNT
                , COUNT(*) AS TOT_CNT
                , B.CNTRY_CD
                , SUM(CASE WHEN C.OPER_STUS_CD = '0' THEN 1 ELSE 0 END) AS NML_CNT 
                , SUM(CASE WHEN C.OPER_STUS_CD = '1' THEN 1 ELSE 0 END) AS WARN_CNT 
                , SUM(CASE WHEN C.OPER_STUS_CD = '2' THEN 1 ELSE 0 END) AS ERR_CNT 
                , SUM(CASE WHEN C.OPER_STUS_CD = '3' THEN 1 ELSE 0 END) AS NO_SIG_CNT 
                , SUM(CASE WHEN C.OPER_STUS_CD IS NULL OR C.OPER_STUS_CD = '4' THEN 1 ELSE 0 END) AS STD_CNT 
				/* 박동환, 2019.09.25 아래 주석처리된 원본의 조회기간이 이해가 안됨
                , SUM(CASE WHEN B.INSTL_DT BETWEEN TO_CHAR(SYS_EXTRACT_UTC(NOW()) - (7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 1), 'YYYYMMDD') 
                                                   		AND TO_CHAR(SYS_EXTRACT_UTC(NOW()) - (7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 7), 'YYYYMMDD') 
                */
                , SUM(CASE WHEN B.INSTL_DT BETWEEN TO_CHAR(SYS_EXTRACT_UTC(NOW()) - CONCAT((7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 1)::varCHAR, 'DAY' )::interval, 'YYYYMMDD')
                                                   		AND TO_CHAR(SYS_EXTRACT_UTC(NOW()) - CONCAT((7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 7)::varCHAR, 'DAY' )::interval, 'YYYYMMDD')
                      THEN 1 ELSE 0 END) AS RCT_INSTL_CNT 
            FROM 
                 TB_BAS_DEVICE B
                , TB_OPR_ESS_STATE C
                , NU
                , U
            WHERE	B.DEVICE_ID = C.DEVICE_ID	-- (+) 
                AND 	B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD	-- (+)
                AND 	B.CNTRY_CD = U.CNTRY_CD
            GROUP BY	B.CNTRY_CD
         )
         SELECT 
             A.CNTRY_CD 
            , TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'YYYYMMDD') AS COLEC_DD 
            , A.INSTLR_CNT
            , A.INSTLR_NA_CNT
            , A.TOT_CNT 
            , A.NML_CNT 
            , ROUND(A.NML_CNT / A.TOT_CNT * 100, 1) AS NML_PER 
            , A.WARN_CNT 
            , ROUND(A.WARN_CNT / A.TOT_CNT * 100, 1) AS WARN_PER 
            , A.ERR_CNT 
            , ROUND(A.ERR_CNT / A.TOT_CNT * 100, 1) AS ERR_PER 
            , A.NO_SIG_CNT 
            , TRUNC(A.NO_SIG_CNT / A.TOT_CNT * 100, 1) AS NO_SIG_PER 
            , A.STD_CNT 
            , ROUND(A.STD_CNT / A.TOT_CNT * 100, 1) AS STD_PER 
            , A.RCT_INSTL_CNT 
            , SYS_EXTRACT_UTC(NOW()) AS CREATE_DT 
         FROM 	A 
         UNION ALL
         SELECT
             'ALL' AS CNTRY_CD 
            , TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'YYYYMMDD') AS COLEC_DD 
            , SUM(A.INSTLR_CNT)
            , MAX(A.INSTLR_NA_CNT)
            , SUM(A.TOT_CNT)
            , SUM(A.NML_CNT)
            , ROUND(SUM(A.NML_CNT) / SUM(A.TOT_CNT) * 100, 1) AS NML_PER 
            , SUM(A.WARN_CNT) 
            , ROUND(SUM(A.WARN_CNT) / SUM(A.TOT_CNT) * 100, 1) AS WARN_PER 
            , SUM(A.ERR_CNT) 
            , ROUND(SUM(A.ERR_CNT) / SUM(A.TOT_CNT) * 100, 1) AS ERR_PER 
            , SUM(A.NO_SIG_CNT) 
            , TRUNC(SUM(A.NO_SIG_CNT) / SUM(A.TOT_CNT) * 100, 1) AS NO_SIG_PER 
            , SUM(A.STD_CNT) 
            , ROUND(SUM(A.STD_CNT) / SUM(A.TOT_CNT) * 100, 1) AS STD_PER 
            , SUM(A.RCT_INSTL_CNT)
            , SYS_EXTRACT_UTC(NOW()) AS CREATE_DT 
         FROM A 
        ;
        
       GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	

 --   	RAISE NOTICE 'v_log_fn[%] Insert TB_OPR_ADMIN_ESS_STATE Table [%]건', v_log_fn, v_work_num;
   	   
        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$$;


ALTER PROCEDURE wems.sp_aggr_admin_ess_state() OWNER TO wems;

--
-- TOC entry 1110 (class 1255 OID 1895878)
-- Name: sp_aggr_device(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_aggr_device(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_AGGR_DEVICE
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-21   yngwie       1. Created this function.
   2.0        2019-09-25   박동환        Postgresql 변환

   NOTES: 	ESS수집데이터에서 수집된 장비상태 갱신
  				(입력시간 무관하게 호출된 시간 기준 처리)
   
    Input : 
            
    Output : 
    
	Usage : SELECT SP_AGGR_DEVICE('SP_AGGR_DEVICE', 0, '20191024', 'M')
    
******************************************************************************/ 
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
   	v_update_id		VARCHAR := NULL;
  	v_exe_tm			TIMESTAMP := SYS_EXTRACT_UTC( TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 12)||'00', 'YYYYMMDDHH24MISS'));
  
BEGIN
    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

	    -- RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] START[%] i_exe_base[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base;
        ------------------------------------------------
        -- 1. ESS 의 Login 정보를 가지고 TB_BAS_DEVICE 테이블의 Model, Firmware Version 정보 갱신
        ------------------------------------------------
        
	  	SELECT ENC_VARCHAR_INS('SYSTEM', 11, 'SDIENCK', 'UPDATE_ID', 'TB_BAS_DEVICE') 
	  	INTO 	v_update_id;
	   
       	WITH R AS ( 
	        SELECT  
	            A.DEVICE_ID, B.DEVICE_TYPE_CD, A.RESPONSE_CD
	            , A.EMS_MODEL_NM, A.PCS_MODEL_NM, A.BMS_MODEL_NM
	            , A.EMS_UPDATE_DT
	            , A.EMS_VER, A.PCS_VER, A.BMS_VER
	            , A.EMS_OUT_IPADDRESS, A.EMS_IPADDRESS
	--          , CASE WHEN C.DEVICE_ID IS NULL THEN 'Y' ELSE 'N' END FW_INS_FLAG
	        FROM (  
	            SELECT   
	                ROW_NUMBER() OVER (PARTITION BY A.DEVICE_ID ORDER BY A.CREATE_DT DESC, A.CREATE_TM DESC, A.DATA_SEQ DESC) SEQ  
	                , A.DEVICE_ID, A.CREATE_TM, A.RESPONSE_CD
	                , A.EMS_MODEL_NM, A.PCS_MODEL_NM, A.BMS_MODEL_NM
	                , A.EMS_UPDATE_DT
	                , A.EMS_VER, A.PCS_VER, A.BMS_VER
	                , A.EMS_OUT_IPADDRESS, A.EMS_IPADDRESS
	            FROM    TB_RAW_LOGIN A
	            WHERE 	A.RESPONSE_CD = '0'
	                AND 	A.CREATE_DT > v_exe_tm - INTERVAL '5 MINUTE'
	                AND 	A.EMS_IPADDRESS <> 'BY SP_CHECK_ESS_LOGIN'
	            ) A  
	            , TB_BAS_DEVICE B
	        WHERE   A.SEQ = 1
	            AND 	A.DEVICE_ID = B.DEVICE_ID
		) 
        UPDATE TB_BAS_DEVICE D 
        SET 
        	 EMS_MODEL_NM = R.EMS_MODEL_NM 
	        , PCS_MODEL_NM =R.PCS_MODEL_NM
	        , BMS_MODEL_NM =R.BMS_MODEL_NM 
	        , EMS_VER = R.EMS_VER 
	        , PCS_VER = R.PCS_VER 
	        , BMS_VER =R.BMS_VER
	        , IP_ADDR = R.EMS_OUT_IPADDRESS
	        , EMS_IP_ADDR = R.EMS_IPADDRESS
	        , EMS_UPDATE_DT = R.EMS_UPDATE_DT
	        , UPDATE_ID = v_update_id
	        , UPDATE_DT = SYS_EXTRACT_UTC(now()) 
	    FROM 		R
        WHERE		D.DEVICE_ID = R.DEVICE_ID
        AND 			D.DEVICE_TYPE_CD = R.DEVICE_TYPE_CD
        ;		
        
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	   	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		-- RAISE notice 'i_sp_nm[%] Update TB_BAS_DEVICE Table [%]건', i_sp_nm, v_work_num;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_aggr_device(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1102 (class 1255 OID 1895880)
-- Name: sp_aggr_instlr_ess_state(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_aggr_instlr_ess_state(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$

/******************************************************************************
   NAME:       SP_AGGR_INSTLR_ESS_STATE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-07-07   yngwie       1. Created this function.
   1.1        2016-01-05   jkm1020     1. ESM (EMS Sleep Mode) 추가 - ESM_CNT, ESM_PER
   2.0        2019-09-25   박동환        Postgresql 변환

   NOTES: 	인스톨러별 ESS 운용 현황 집계
  				(입력시간 무관하게 호출된 시간 기준 처리)

    Input :

    Output :

	Usage : SELECT SP_AGGR_INSTLR_ESS_STATE('SP_AGGR_INSTLR_ESS_STATE', 0, '201910221237', 'M');

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
   
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
  
		v_rslt_cd := 'A';

	    --RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] i_exe_base[%] START[%]', i_sp_nm, i_site_seq, i_exe_base, now();

	   -- 집계 테이블 삭제
	    DELETE FROM TB_OPR_INSTLR_ESS_STATE;
	   
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       

      	--RAISE NOTICE 'i_sp_nm[%] Delete TB_OPR_INSTLR_ESS_STATE Table [%]건', i_sp_nm, v_work_num;

        
        WITH R AS ( 
            SELECT 	 A.USER_ID 
--            			, A.AUTH_TYPE_CD
						, C.OPER_STUS_CD
						, B.INSTL_DT
						, DEC_VARCHAR_SEL(AUTH_TYPE_CD, 10, 'SDIENCK', 'TB_BAS_USER', 'AUTH_TYPE_CD') as AUTH_TYPE_CD
            FROM      TB_BAS_USER A 
                		, TB_BAS_DEVICE B 
                		, TB_OPR_ESS_STATE C 
            WHERE 	A.USER_ID = B.INSTL_USER_ID 
                AND 	B.DEVICE_ID = C.DEVICE_ID 
                AND 	B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD
        ), A AS ( 
            SELECT   USER_ID 
                , COUNT(*) AS TOT_CNT 
                , SUM(CASE WHEN OPER_STUS_CD = '0' THEN 1 ELSE 0 END) AS NML_CNT 
                , SUM(CASE WHEN OPER_STUS_CD = '1' THEN 1 ELSE 0 END) AS WARN_CNT 
                , SUM(CASE WHEN OPER_STUS_CD = '2' THEN 1 ELSE 0 END) AS ERR_CNT 
                , SUM(CASE WHEN OPER_STUS_CD = '3' THEN 1 ELSE 0 END) AS NO_SIG_CNT 
                , SUM(CASE WHEN OPER_STUS_CD IS NULL OR OPER_STUS_CD = '4' THEN 1 ELSE 0 END) AS STD_CNT 
				/* 박동환, 2019.09.25 아래 주석처리된 원본의 조회기간이 이해가 안됨
                , SUM(CASE WHEN INSTL_DT BETWEEN TO_CHAR(SYS_EXTRACT_UTC(NOW()) - (7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 1), 'YYYYMMDD') 
                                                   		AND TO_CHAR(SYS_EXTRACT_UTC(NOW()) - (7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 7), 'YYYYMMDD') 
                */
                , SUM(CASE WHEN INSTL_DT BETWEEN TO_CHAR(SYS_EXTRACT_UTC(NOW()) - CONCAT((7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 1)::varCHAR, 'DAY' )::interval, 'YYYYMMDD')
                                    		AND TO_CHAR(SYS_EXTRACT_UTC(NOW()) - CONCAT((7 + TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'D')::NUMERIC - 7)::varCHAR, 'DAY' )::interval, 'YYYYMMDD')
                      THEN 1 ELSE 0 END) AS RCT_INSTL_CNT 
                , SUM(CASE WHEN OPER_STUS_CD = '5' THEN 1 ELSE 0 END) AS ESM_CNT                          
            FROM 	R 
            WHERE 	AUTH_TYPE_CD = '20' 
            GROUP BY USER_ID 
        ) 
       	INSERT INTO TB_OPR_INSTLR_ESS_STATE (
            USER_ID      
            , COLEC_DD   
            , TOT_CNT        
            , NML_CNT        
            , NML_PER        
            , WARN_CNT   
            , WARN_PER   
            , ERR_CNT        
            , ERR_PER        
            , NO_SIG_CNT     
            , NO_SIG_PER     
            , STD_CNT        
            , STD_PER        
            , RCT_INSTL_CNT 
            , CREATE_DT 
            , ESM_CNT 
            , ESM_PER 
        ) 
        SELECT 
            A.USER_ID 
            , TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'YYYYMMDD') AS COLEC_DD 
            , A.TOT_CNT 
            , A.NML_CNT 
            , ROUND(A.NML_CNT / A.TOT_CNT * 100, 1) AS NML_PER 
            , A.WARN_CNT 
            , ROUND(A.WARN_CNT / A.TOT_CNT * 100, 1) AS WARN_PER 
            , A.ERR_CNT 
            , ROUND(A.ERR_CNT / A.TOT_CNT * 100, 1) AS ERR_PER 
            , A.NO_SIG_CNT 
            , TRUNC(A.NO_SIG_CNT / A.TOT_CNT * 100, 1) AS NO_SIG_PER 
            , A.STD_CNT 
            , ROUND(A.STD_CNT / A.TOT_CNT * 100, 1) AS STD_PER 
            , RCT_INSTL_CNT 
            , SYS_EXTRACT_UTC(NOW()) AS CREATE_DT 
            , A.ESM_CNT        
            , ROUND(A.ESM_CNT / A.TOT_CNT * 100, 1) AS ESM_PER 
        FROM  A 
        ;
        
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       

      	--RAISE NOTICE 'i_sp_nm[%] Insert TB_OPR_INSTLR_ESS_STATE Table [%]건', i_sp_nm, v_work_num;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );

   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_aggr_instlr_ess_state(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1103 (class 1255 OID 1895882)
-- Name: sp_aggr_state(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_aggr_state(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
/******************************************************************************
  NAME:       SP_AGGR_STATE
  PURPOSE:

  REVISIONS:
  Ver        Date        Author           Description
  ---------  ----------  ---------------  ------------------------------------
  1.0        2014-06-16   yngwie      Created this function.
  1.1        2016-01-05   jkm1020    ESM (EMS Sleep Mode) 추가
  2.0        2019-09-24   박동환       Postgresql 변환
  2.1        2020-01-22   박동환       오류수정(A.COL126<>'NA' => A.COL126 IS NULL OR A.COL126<>'NA')  
  
  NOTES:   1분주기 수집데이터를 가지고 상태/예측/장애 데이터를 생성한다.
                   < INPUT >
                     1.i_sp_nm        프로시져명
                     2.i_site_seq	     서비스대상번호
                     3.i_exe_base     실행기준값(월별    :YYYYMM 
                                                일별    :YYYYMMDD 
                                                시간대별:YYYYMMDDHH0000 
                                                15분    :YYYYMMDDHHMI00)
                     4.i_exe_mthd     실행방법모드(A:자동, M:수동)

                   < OUTPUT >
                     1.v_rslt_cd     실행상태코드 (0:실행중,1:실행성공,그외:실행오류)  

*****************************************************************************/
-- 수동 실행 스크립트
-- SELECT SP_AGGR_STATE('SP_AGGR_STATE', 0, '20191024102000', 'M');

DECLARE
    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
    v_st_dt                 	TIMESTAMP := CLOCK_TIMESTAMP(); 	-- 시작일시
    v_rslt_cd          		VARCHAR  	:= '0';   				-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt              	INTEGER   	:= 0;     				-- 적재수
    v_err_msg      			VARCHAR   	:= NULL;  			-- 오류메시지
    v_err_msg1              VARCHAR  	:= NULL;  			-- 메시지내용1
    v_err_msg2              VARCHAR   	:= NULL;  			-- 메시지내용2
    v_err_msg3              VARCHAR   	:= NULL;  			-- 메시지내용3

    -- (2)업무처리 변수
    v_work_num    INTEGER   := 0;     -- 처리건수
    v_handleFlag 	VARCHAR;
    v_selectDt   		VARCHAR; --TIMESTAMP WITH TIME ZONE;
    v_baseTm   		TIMESTAMPTZ;
    v_row           	record; 
	
   	v_sql				VARCHAR;
    v_tbl				VARCHAR;
   	v_ExeCnt1 		INTEGER 		:= 0;
   	v_ExeCnt2 		INTEGER 		:= 0;
   	v_ExeCnt3 		INTEGER 		:= 0;
  
BEGIN
   	v_baseTm 		:= SYS_EXTRACT_UTC(TO_TIMESTAMP(i_exe_base, 'YYYYMMDDHH24MISS'));
   	v_handleFlag 	:= TO_CHAR(v_baseTm, 'YYYYMMDDHH24MISS');

	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base 
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
--	    RAISE NOTICE 'i_sp_nm[%] i_exe_base[%] v_handleFlag[%] v_baseTm[%] START[%]', i_sp_nm, i_exe_base, v_handleFlag, v_baseTm, TO_CHAR(now(), 'YYYY-MM-DD HH24:MI:SS');
       	
       	-- EMS 처리
		TRUNCATE TABLE TB_RAW_EMS_INFO_TMP;

        INSERT INTO TB_RAW_EMS_INFO_TMP 
        SELECT   DEVICE_ID, COLEC_DT, DONE_FLAG  
            , COL0, COL1, COL2, COL3, COL4, COL5, COL6, COL7, COL8, COL9, COL10  
            , COL11, COL12, COL13, COL14, COL15, COL16, COL17, COL18, COL19, COL20  
            , COL21, COL22, COL23, COL24, COL25, COL26, COL27, COL28, COL29, COL30 
            , COL31, COL32, COL33, COL34, COL35, COL36, COL37, COL38, COL39, COL40 
            , COL41, COL42, COL43, COL44, COL45, COL46, COL47, COL48, COL49, COL50
            , COL51, COL52, COL53, COL54, COL55, COL56, COL57, COL58, COL59, COL60 
            , COL61, COL62, COL63, COL64, COL65, COL66, COL67, COL68, COL69, COL70 
            , COL71, COL72, COL73, COL74, COL75, COL76, COL77, COL78, COL79, COL80 
            , COL81, COL82, COL83, COL84, COL85, COL86, COL87 
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR( COL88, 3))) )::NUMERIC, 'FM00000000') AS COL88  
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR( COL89, 3))) )::NUMERIC, 'FM00000000') AS COL89  
            , COL90, COL91, COL92, COL93, COL94, COL95, COL96, COL97, COL98, COL99, COL100
			, COL101, COL102, COL103, COL104, COL105, COL106, COL107, COL108, COL109, COL110 
			, COL111, COL112, COL113, COL114, COL115, COL116, COL117, COL118, COL119, COL120 
			, COL121, COL122, COL123, COL124, COL125, COL126, COL127, COL128, COL129, COL130 
			, COL131, COL132, COL133, COL134, COL135, COL136, COL137, COL138, COL139, COL140 
			, COL141, COL142, COL143, COL144, COL145, COL146, COL147, COL148, COL149, COL150 
			, COL151, COL152, COL153, COL154, COL155, COL156, COL157, COL158, COL159, COL160 
			, COL161, COL162, COL163, COL164, COL165, COL166, COL167, COL168, COL169, COL170 
			, COL171, COL172, COL173, COL174, COL175, COL176, COL177, COL178, COL179, COL180 
			, COL181, COL182, COL183, COL184, COL185, COL186, COL187, COL188, COL189, COL190 
			, COL191, COL192, COL193, COL194, COL195, COL196, COL197, COL198, COL199, COL200 
			, COL201, COL202, COL203, COL204, COL205, COL206, COL207, COL208, COL209, COL210 
			, COL211, COL212, COL213, COL214, COL215, COL216, COL217, COL218, COL219, COL220 
			, COL221, COL222, COL223, COL224, COL225, COL226, COL227, COL228, COL229, COL230 
			, COL231, COL232, COL233, COL234, COL235, COL236, COL237, COL238, COL239, COL240 
			, COL241, COL242, COL243, COL244, COL245, COL246, COL247, COL248, COL249, COL250 
			, COL251, COL252, COL253, COL254, COL255, COL256, COL257, COL258, COL259, COL260 
			, COL261, COL262, COL263, COL264, COL265, COL266, COL267, COL268, COL269, COL270
			, COL271, COL272, COL273, COL274, COL275, COL276, COL277, COL278, COL279, COL280
			, COL281, COL282, COL283, COL284, COL285, COL286, COL287, COL288, COL289, COL290
			, COL291, COL292, COL293, COL294, COL295, COL296, COL297, COL298, COL299, COL300
			, COL301, COL302, COL303, COL304, COL305, COL306, COL307, COL308, COL309, COL310
			, COL311, COL312, COL313, COL314, COL315, COL316, COL317, COL318, COL319, COL320
			, COL321, COL322, COL323, COL324, COL325, COL326, COL327, COL328, COL329, COL330
			, COL331, COL332, COL333, COL334, COL335, COL336, COL337, COL338, COL339, COL340
			, COL341            
			, CREATE_DT 
            , HANDLE_FLAG  
        FROM   	TB_RAW_EMS_INFO
		WHERE  	CREATE_DT BETWEEN TO_TIMESTAMP(SUBSTR(v_handleFlag, 1, 12)||'00', 'YYYYMMDDHH24MISS')
									AND 		TO_TIMESTAMP(SUBSTR(v_handleFlag, 1, 12)||'59', 'YYYYMMDDHH24MISS')
		;
   	
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	    v_ExeCnt1 := v_work_num;
--		RAISE NOTICE 'i_sp_nm[%] Insert TB_RAW_EMS_INFO_TMP Table [%]건', i_sp_nm, v_work_num;

                  
   		v_rslt_cd := 'B';
		TRUNCATE TABLE TB_RAW_PCS_INFO_TMP;

       	INSERT INTO TB_RAW_PCS_INFO_TMP 
        SELECT 
            A.DEVICE_ID, A.COLEC_DT, A.DONE_FLAG    
			, TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL0, 3))) )::NUMERIC, 'FM00000000') AS COL0  
			, TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL1, 3))) )::NUMERIC, 'FM00000000') AS COL1  
			, TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL2, 3))) )::NUMERIC, 'FM00000000') AS COL2  
			, A.COL3, A.COL4, A.COL5, A.COL6, A.COL7, A.COL8, A.COL9, A.COL10
			, A.COL11, A.COL12, A.COL13, A.COL14, A.COL15, A.COL16, A.COL17, A.COL18, A.COL19, A.COL20
			, A.COL21, A.COL22, A.COL23, A.COL24, A.COL25, A.COL26, A.COL27, A.COL28, A.COL29, A.COL30
			, A.COL31, A.COL32, A.COL33, A.COL34, A.COL35, A.COL36, A.COL37, A.COL38, A.COL39, A.COL40
			, A.COL41, A.COL42, A.COL43, A.COL44, A.COL45, A.COL46, A.COL47, A.COL48, A.COL49, A.COL50
			, A.COL51, A.COL52, A.COL53, A.COL54, A.COL55, A.COL56, A.COL57, A.COL58, A.COL59, A.COL60
			, A.COL61, A.COL62, A.COL63, A.COL64, A.COL65, A.COL66, A.COL67, A.COL68, A.COL69, A.COL70
			, A.COL71, A.COL72, A.COL73, A.COL74, A.COL75, A.COL76, A.COL77, A.COL78, A.COL79, A.COL80
			, A.COL81, A.COL82, A.COL83, A.COL84, A.COL85, A.COL86, A.COL87, A.COL88, A.COL89, A.COL90
			, A.COL91, A.COL92, A.COL93, A.COL94, A.COL95, A.COL96, A.COL97, A.COL98, A.COL99, A.COL100
			, A.COL101, A.COL102, A.COL103, A.COL104, A.COL105, A.COL106, A.COL107, A.COL108, A.COL109, A.COL110
			, A.COL111, A.COL112, A.COL113, A.COL114, A.COL115, A.COL116, A.COL117, A.COL118, A.COL119, A.COL120
			, A.COL121, A.COL122, A.COL123, A.COL124, A.COL125, A.COL126, A.COL127, A.COL128, A.COL129, A.COL130
			, A.COL131, A.COL132, A.COL133, A.COL134, A.COL135, A.COL136, A.COL137, A.COL138, A.COL139, A.COL140
			, A.COL141, A.COL142, A.COL143, A.COL144, A.COL145, A.COL146, A.COL147, A.COL148, A.COL149, A.COL150
			, A.COL151, A.COL152, A.COL153, A.COL154, A.COL155, A.COL156, A.COL157, A.COL158, A.COL159, A.COL160
			, A.COL161, A.COL162, A.COL163, A.COL164, A.COL165, A.COL166, A.COL167, A.COL168, A.COL169, A.COL170
			, A.COL171, A.COL172, A.COL173, A.COL174, A.COL175, A.COL176, A.COL177, A.COL178, A.COL179, A.COL180
			, A.COL181, A.COL182, A.COL183, A.COL184, A.COL185, A.COL186, A.COL187, A.COL188, A.COL189, A.COL190
			, A.COL191, A.COL192, A.COL193, A.COL194, A.COL195, A.COL196, A.COL197, A.COL198, A.COL199, A.COL200
			, A.COL201, A.COL202, A.COL203, A.COL204, A.COL205, A.COL206, A.COL207, A.COL208, A.COL209, A.COL210
			, A.COL211, A.COL212, A.COL213, A.COL214, A.COL215, A.COL216, A.COL217, A.COL218, A.COL219, A.COL220
			, A.COL221, A.COL222, A.COL223, A.COL224, A.COL225, A.COL226 
			, A.CREATE_DT 
			, A.HANDLE_FLAG 
        FROM TB_RAW_EMS_INFO_TMP B, 
        		 TB_RAW_PCS_INFO A		
        WHERE  B.COLEC_DT = A.COLEC_DT 
            AND B.DEVICE_ID = A.DEVICE_ID  ;
       
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--		RAISE NOTICE 'i_sp_nm[%] Insert TB_RAW_PCS_INFO_TMP Table [%]건', i_sp_nm, v_work_num;

	
		v_rslt_cd := 'C';
        TRUNCATE TABLE TB_RAW_BMS_INFO_TMP;

        INSERT INTO TB_RAW_BMS_INFO_TMP 
        SELECT 
             A.DEVICE_ID, A.COLEC_DT, A.DONE_FLAG  
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL0, 3))) )::NUMERIC, 'FM00000000') AS COL0
            , A.COL1, A.COL2, A.COL3, A.COL4, A.COL5, A.COL6, A.COL7, A.COL8, A.COL9, A.COL10    
            , A.COL11, A.COL12, A.COL13, A.COL14, A.COL15, A.COL16, A.COL17, A.COL18, A.COL19    
            , CASE WHEN A.COL20 IS NOT NULL AND A.COL20::NUMERIC> 4 THEN '0' ELSE A.COL20 end COL20  
            , A.COL21, A.COL22, A.COL23    
--          , A.COL24    
            , (CASE WHEN POSITION('0x' in A.COL24) > 0 THEN HEX2DEC(UPPER(SUBSTR(A.COL24, 3))) ELSE A.COL24::NUMERIC END)::VARCHAR COL24
            , A.COL25, A.COL26, A.COL27, A.COL28, A.COL29, A.COL30    
            , A.COL31, A.COL32, A.COL33, A.COL34, A.COL35, A.COL36, A.COL37, A.COL38, A.COL39, A.COL40    
            , A.CREATE_DT 
            , A.HANDLE_FLAG 
        FROM 	TB_RAW_EMS_INFO_TMP B , 
        			TB_RAW_BMS_INFO A 
        WHERE  	B.COLEC_DT = A.COLEC_DT 
           AND 	B.DEVICE_ID = A.DEVICE_ID	;
                
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;
--		RAISE NOTICE 'i_sp_nm[%] Insert TB_RAW_BMS_INFO_TMP Table [%]건', i_sp_nm, v_work_num;
   	   

		IF v_ExeCnt1 > 0 THEN
       
            ------------------------
            -- 1. 장애 데이터 처리
            ------------------------
	 		v_rslt_cd := 'D';

 	        TRUNCATE TABLE TB_OPR_ALARM_TMP;
	        TRUNCATE TABLE TB_OPR_ALARM_TMP_RAW;

            -- TEMP 테이블에 장애 발생/미발생 데이터 생성
            -- 2015.09.03 yngwie 모델별 에러코드 적용 관련 으로 TB_OPR_ALARM_TMP_RAW 테이블에 먼저 insert 한 후
            --                   필터링한 데이터를 TB_OPR_ALARM_TMP 로 옮긴 다음에 처리하는 것으로 변경
            -- 2015.09.16 yngwie 장애 스트링 처리로 변경
            v_sql := FORMAT('
            WITH A AS ( 
                SELECT  	A.DEVICE_ID   
		                    , B.DEVICE_TYPE_CD  
		                    , A.COLEC_DT 
		                    , A.COL126 
		                    , FN_GET_CHAR_COUNT(A.COL126, ''$'') AS POS
                FROM 	(   
                        SELECT     ROW_NUMBER() OVER (PARTITION BY A.DEVICE_ID ORDER BY A.COLEC_DT DESC ) SEQ   
		                            , A.DEVICE_ID 
		                            , A.COLEC_DT 
		                            , A.COL126 
                        FROM		TB_RAW_EMS_INFO_TMP A  
                        WHERE   (A.COL126 IS NULL OR A.COL126 <> ''NA'')  
                    ) A    
                    , TB_BAS_DEVICE B   
                WHERE   A.SEQ = 1    
                    AND 	A.DEVICE_ID = B.DEVICE_ID  
            ) 
            INSERT INTO TB_OPR_ALARM_TMP_RAW (
                ALARM_ID 
           --     , ALARM_ID_SEQ 
                , DEVICE_ID 
                , DEVICE_TYPE_CD 
           --     , ALARM_TYPE_CD 
                , ALARM_CD 
                , EVENT_DT 
                , ERR_OCUR 
                , POS 
           --     , INS_FLAG 
                , CREATE_DT 
			)  
            SELECT	*
            FROM (  
	            SELECT  
		                 CONCAT( CONCAT( CONCAT(''ALM_'' , A.DEVICE_ID) , ''_'' ), A.COLEC_DT) AS ALARM_ID  
		         --       , NULL AS ALARM_ID_SEQ  
		                , A.DEVICE_ID  
		                , A.DEVICE_TYPE_CD  
		        --        , NULL AS ALARM_TYPE_CD  
		                , (CASE WHEN LENGTH(FN_GET_SPLIT(A.COL126::TEXT, B.LVL::INT4, ''$''::TEXT)) > 4 THEN NULL ELSE FN_GET_SPLIT(A.COL126::TEXT, B.LVL::INT4, ''$''::TEXT) END) AS ALARM_CD 
		                , A.COLEC_DT AS EVENT_DT  
		                , 1 AS ERR_OCUR  
		                , A.POS  
		         --       , NULL AS INS_FLAG  
		                , TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'') AS CREATE_DT  
	            FROM  	A 
			                , (	SELECT LEVEL AS LVL  
			                	FROM	GENERATE_SERIES(1, ( SELECT MAX(POS) FROM A ) ) level 
			                ) B
	            WHERE 	LENGTH(A.COL126) > 0 
            ) T
            WHERE ALARM_CD IS NOT NULL ;'
            , v_handleFlag);
           
            EXECUTE v_sql;
           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;
--			RAISE NOTICE 'i_sp_nm[%] Insert TB_OPR_ALARM_TMP_RAW ALARM [%]건 SQL[%]', i_sp_nm, v_work_num, v_sql;
                        
 	 		v_rslt_cd := 'E';
             
            -- 2015.09.03 yngwie 모델별 에러코드 적용 관련
            -- TB_BAS_MODEL_ALARM_CODE_MAP 에 등록되어 있는 장애들만 임시 테이블로 옮긴다.
            WITH ESS AS ( 
                SELECT   A.* 
                    		, SUBSTR(B.PRODUCT_MODEL_NM, 1, 7) AS PRODUCT_MODEL_TYPE_CD 
                FROM 	TB_OPR_ALARM_TMP_RAW A 
                    		, TB_BAS_DEVICE B 
                WHERE   A.DEVICE_ID = B.DEVICE_ID 
                    AND 	A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD 
            ) 
            INSERT INTO TB_OPR_ALARM_TMP ( 
                 ALARM_ID  
                , ALARM_ID_SEQ  
                , DEVICE_ID  
                , DEVICE_TYPE_CD  
                , ALARM_TYPE_CD  
                , ALARM_CD  
                , EVENT_DT  
                , ERR_OCUR  
                , POS  
                , INS_FLAG  
                , CREATE_DT 
            ) 
            SELECT 
                 B.ALARM_ID 
                , B.ALARM_ID_SEQ 
                , B.DEVICE_ID 
                , B.DEVICE_TYPE_CD 
                , A.ALARM_TYPE_CD 
                , (CASE WHEN LENGTH(B.ALARM_CD) > 4 THEN NULL ELSE B.ALARM_CD END) AS ALARM_CD 
                , B.EVENT_DT 
                , B.ERR_OCUR 
                , B.POS 
                , B.INS_FLAG 
                , B.CREATE_DT 
            FROM		TB_BAS_MODEL_ALARM_CODE_MAP A 
                		, ESS B 
            WHERE	A.PRODUCT_MODEL_TYPE_CD like concat(B.PRODUCT_MODEL_TYPE_CD, '%')
            AND 		A.ALARM_CD = B.ALARM_CD ;

           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--			RAISE NOTICE 'i_sp_nm[%] Insert TB_OPR_ALARM_TMP ALARM [%]건', i_sp_nm, v_work_num;
                                
		
            -- TEMP 테이블에 TB_RAW_LOGIN 테이블의 레코드를 확인해서 연결장애 발생/미발생 데이터도 추가한다
            -- 2014-08-01  연결장애 발생 후 한시간 이상 지난 장비만 연결장애로 처리하도록 변경
            -- 2015. ?? 연결장애 발생 후 7분이 지난 장비만 연결장애로 처리하도록 변경
            -- sleep 모드는 장애로 넣으면 안된다. 20160105
            
   	 		v_rslt_cd := 'F';
   
   	 		v_sql := FORMAT('
            INSERT INTO TB_OPR_ALARM_TMP (
                 ALARM_ID 
--                , ALARM_ID_SEQ 
                , DEVICE_ID 
                , DEVICE_TYPE_CD 
                , ALARM_TYPE_CD 
                , ALARM_CD 
                , EVENT_DT 
                , ERR_OCUR 
                , POS 
--                , INS_FLAG 
                , CREATE_DT 
            )          
            SELECT 
                CONCAT( CONCAT( CONCAT(''ALM_'' , A.DEVICE_ID) , ''_'' ), TO_CHAR(A.CREATE_DT, ''YYYYMMDDHH24MISS'')) AS ALARM_ID  
 --               , NULL AS ALARM_ID_SEQ 
                , A.DEVICE_ID    
                , B.DEVICE_TYPE_CD    
                , ''1'' AS ALARM_TYPE_CD 	-- 2015.09.03 yngwie, 연결장애는 빈번히 발생하므로 ALARM_TYPE_CD 를 1 (Notification) 으로 변경
                , ''C128'' AS ALARM_CD  		-- 2015.09.03 yngwie, 연결장애는 C128 로 통일 (모델별 에러코드 적용 관련)
                , TO_CHAR(A.CREATE_DT, ''YYYYMMDDHH24MISS'') AS EVENT_DT 
                , CASE WHEN A.RESPONSE_CD <> ''0'' AND A.GAP > 7 THEN 1 ELSE 0 END AS ERR_OCUR 
                , 0 AS POS 
--                , NULL AS INS_FLAG 
                , TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'')  AS CREATE_DT 
            FROM (    
                SELECT     
                    ROW_NUMBER() OVER (PARTITION BY A.DEVICE_ID ORDER BY A.CREATE_DT DESC, A.CREATE_TM DESC, A.DATA_SEQ DESC) SEQ    
                    , A.DEVICE_ID    
                    , A.CREATE_DT    
                    , A.RESPONSE_CD    
                    , EXTRACT(DAY FROM TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'') - CREATE_DT) * 24 * 60 +   
                     EXTRACT(HOUR FROM TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'') - CREATE_DT) * 60 +    
                     EXTRACT(MINUTE FROM TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'') - CREATE_DT)  GAP    
                FROM 	TB_RAW_LOGIN A 
                WHERE 	A.CREATE_DT BETWEEN TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'')- INTERVAL ''1 DAY'' AND TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'')  
            ) A 
            , TB_BAS_DEVICE B                    
            WHERE  	A.SEQ = 1		-- 2016.01.11 Slepp 모드는 연결 장애에서 제외 시킨다. jkm1020 => NOT IN 추가  
                AND 	A.DEVICE_ID = B.DEVICE_ID 
                AND 	A.RESPONSE_CD <> ''0'' AND A.GAP > 7 
                AND 	A.DEVICE_ID NOT IN (SELECT DEVICE_ID FROM TB_OPR_ESS_STATE WHERE OPER_STUS_CD =''5'') ; ',
                v_handleFlag, v_handleFlag, v_handleFlag, v_handleFlag, v_handleFlag, v_handleFlag
            );

           	EXECUTE v_sql;
           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--			RAISE NOTICE 'i_sp_nm[%] Insert TB_OPR_ALARM_TMP ALARM [%]건 SQL[%]', i_sp_nm, v_work_num, v_sql;
            
   	 		v_rslt_cd := 'G';

   	 		v_ExeCnt1 := 0;
   	 	
   	 		SELECT 	COUNT(*) 
   	 		INTO 		v_ExeCnt1
   	 		WHERE EXISTS (SELECT * FROM TB_OPR_ALARM_TMP WHERE ERR_OCUR = 1);
   	 	
           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--			RAISE NOTICE 'i_sp_nm[%] Check ALARM Records Exists [%]건', i_sp_nm, v_work_num;
            
            IF v_ExeCnt1 > 0 then
      	 		v_rslt_cd := 'J';

                -- 2015.06.24 연결장애는 EVENT_DT 를 TB_RAW_EMS_INFO 레코드 중 가장 최근의 COLEC_DT로  처리하도록 변경
                v_sql := FORMAT('
                WITH B AS ( 
                    SELECT 	MAX(B.COLEC_DT) AS COLEC_DT 
		                        , B.DEVICE_ID 
		                        , B.DEVICE_TYPE_CD 
                    FROM 	TB_OPR_ALARM_TMP A,
								TB_OPR_ESS_STATE_HIST B 
					WHERE	A.ALARM_CD = ''C128''  -- 2015.09.03 yngwie, 연결장애는 C128 로 통일 (모델별 에러코드 적용 관련)
                    AND 		A.ERR_OCUR = 1 
                    AND 		A.DEVICE_ID = B.DEVICE_ID 
                    AND 		A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD
                    AND 		B.COLEC_DT BETWEEN TO_CHAR(TO_TIMESTAMP(''%s'', ''YYYYMMDDHH24MISS'') - INTERVAL ''1 DAY'', ''YYYYMMDDHH24MISS'') AND ''%s'' 
                    GROUP BY B.DEVICE_ID, B.DEVICE_TYPE_CD 
                ) 
                UPDATE  TB_OPR_ALARM_TMP A
                    SET 	EVENT_DT = B.COLEC_DT 
		      	FROM B
				WHERE	A.DEVICE_ID = B.DEVICE_ID 
                    AND 	A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD  ;	'
                ,	v_handleFlag, v_handleFlag);
                
            	EXECUTE v_sql;
	           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--				RAISE NOTICE 'i_sp_nm[%] Merge TB_OPR_ALARM_TMP from TB_OPR_ESS_STATE_HIST Records : [%]건 SQL[%]', i_sp_nm, v_work_num, v_sql;

	   	 		v_rslt_cd := 'H';
   	 			v_ExeCnt2 := 0;
   	 		
	            -- 새로운 장애를 장애 테이블에 추가한다.
	            WITH S AS (
	   				SELECT 	  ALARM_ID 
					            , ROW_NUMBER() OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY ALARM_CD) AS ALARM_ID_SEQ 
					            , DEVICE_ID 
					            , DEVICE_TYPE_CD 
					            , ALARM_TYPE_CD 
					            , ALARM_CD 
					            , EVENT_DT 
					FROM 	TB_OPR_ALARM_TMP 
					WHERE 	ERR_OCUR = 1
					    AND 	(DEVICE_ID, DEVICE_TYPE_CD, ALARM_TYPE_CD, ALARM_CD) NOT IN ( 
					        SELECT DEVICE_ID  
					            	, DEVICE_TYPE_CD  
					            	, ALARM_TYPE_CD  
					            	, ALARM_CD  
					        FROM TB_OPR_ALARM ) 
				)
	            INSERT INTO TB_OPR_ALARM (ALARM_ID, ALARM_ID_SEQ, DEVICE_ID, DEVICE_TYPE_CD, ALARM_TYPE_CD, ALARM_CD, EVENT_DT)
				SELECT 	  ALARM_ID, ALARM_ID_SEQ, DEVICE_ID, DEVICE_TYPE_CD, ALARM_TYPE_CD
							, (CASE WHEN LENGTH(ALARM_CD) > 4 THEN NULL ELSE ALARM_CD END) AS ALARM_CD, EVENT_DT 
				FROM S   ; 
               
		      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		      	v_ExeCnt2 := v_ExeCnt2+v_work_num;
		        v_rslt_cnt := v_rslt_cnt + v_work_num;

--			  RAISE notice 'i_sp_nm[%] Add New ALARM Records :[%]건', i_sp_nm, v_ExeCnt2;
            
            END IF;
            
           	-- modified by stjoo(20160805) : 주석처리
			--  데이타 이전작업 완료로 PK충돌이 발생할 소지가 없으므로 속도향상을 위해 INSERT 구문으로 변경
           	v_rslt_cd := 'I';

           	v_ExeCnt1 := 0;
			v_ExeCnt2 := 0;     

	        -- 2015.09.19 yngwie 해지된 알람 조회
	        WITH B AS (
	            SELECT	 DEVICE_ID 
			                , DEVICE_TYPE_CD 
			                , ALARM_TYPE_CD 
			                , ALARM_CD 
	            FROM   TB_OPR_ALARM
	            EXCEPT
	            SELECT
			                DEVICE_ID 
			                , DEVICE_TYPE_CD 
			                , ALARM_TYPE_CD 
			                , ALARM_CD 
	            FROM 	TB_OPR_ALARM_TMP
	            WHERE	ERR_OCUR = 1
	        ), EMS AS (
	            SELECT        DEVICE_ID
				                , MAX(COLEC_DT) AS COLEC_DT
	            FROM         TB_RAW_EMS_INFO_TMP
	            GROUP BY    DEVICE_ID
	        ), R as (
		        SELECT 
		            A.ALARM_ID 
		            , A.ALARM_ID_SEQ 
		            , A.DEVICE_ID 
		            , A.DEVICE_TYPE_CD 
		            , A.ALARM_TYPE_CD 
		            , A.ALARM_CD 
		            , A.EVENT_DT 
		            , EMS.COLEC_DT AS CLEAR_DT 
		            , 'N' AS MANU_RECV_FLAG 
		            , NULL AS MANU_RECV_USER_ID 
		            , NULL AS MANU_RECV_DESC 
		            , v_baseTm AS CREATE_DT 
		        FROM 	TB_OPR_ALARM A
				            , B
				            , EMS
		        WHERE
		            A.DEVICE_ID = B.DEVICE_ID 
		            AND A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD 
		            AND A.ALARM_TYPE_CD = B.ALARM_TYPE_CD 
		            AND A.ALARM_CD = B.ALARM_CD 
		            AND EMS.DEVICE_ID = A.DEVICE_ID
	    	
			), ins as (
--				RAISE notice 'i_sp_nm[%] Clear ALARM Records, DEVICE_ID[%] ALARM_CD[%] EVENT_DT[%] CLEAR_DT[%] ALARM_ID[%] ALARM_ID_SEQT[%]',
--										i_sp_nm, v_row.DEVICE_ID, v_row.ALARM_CD, v_row.EVENT_DT, v_row.CLEAR_DT, v_row.ALARM_ID, v_row.ALARM_ID_SEQ;
                
                -- 해지된 장애는 이력테이블로 옮긴다.
               	INSERT INTO TB_OPR_ALARM_HIST (ALARM_ID, ALARM_ID_SEQ, DEVICE_ID, DEVICE_TYPE_CD, ALARM_TYPE_CD, ALARM_CD, EVENT_DT, CLEAR_DT, MANU_RECV_FLAG)
            	SELECT ALARM_ID
                        , ALARM_ID_SEQ
                        , DEVICE_ID
                        , DEVICE_TYPE_CD
                        , ALARM_TYPE_CD
                        , (CASE WHEN LENGTH(ALARM_CD) > 4 THEN NULL ELSE ALARM_CD END)
                        , EVENT_DT
                        , CLEAR_DT
                        , MANU_RECV_FLAG
                FROM R
                  
               RETURNING *
			), del as (
                -- 해지된 장애는 현황테이블에서 삭제한다
                DELETE FROM TB_OPR_ALARM
                WHERE 	(DEVICE_ID, DEVICE_TYPE_CD, ALARM_TYPE_CD, ALARM_CD) 
                			IN (
		                			SELECT DEVICE_ID, DEVICE_TYPE_CD, ALARM_TYPE_CD, ALARM_CD 
		                			FROM R 
		                		)
		                		
              	RETURNING *
			)
			SELECT SUM(cnt1), SUM(cnt2)
			INTO 	v_ExeCnt1, v_ExeCnt2
			FROM (
			   SELECT 	COUNT(ins.*)::INTEGER cnt1, 0 cnt2
			   FROM 	ins
			UNION ALL
			   SELECT 	0 cnt1, COUNT(del.*)::INTEGER cnt2
			   FROM 	del
			) Z;
			
--			RAISE notice 'i_sp_nm[%] insert TB_OPR_ALARM_HIST : [%]건', i_sp_nm, v_ExeCnt1;
--			RAISE notice 'i_sp_nm[%] delete TB_OPR_ALARM : [%]건', i_sp_nm, v_ExeCnt2;
           
            ------------------------    
            -- 2. 상태정보 데이터 처리
            ------------------------
            v_rslt_cd := 'J';
	        TRUNCATE TABLE TB_OPR_ESS_STATE_TMP;

            -- 대상 레코드들의 CO2와 금액을 계산한 결과를 TEMP 테이블에 Insert
            -- 2014.08.12 E700번대 장애는 사용자에게 보여주지 않도록 수정
            -- 2014.08.20 E900번대와 C999 장애만 사용자에게 보여주도록 수정 => 장애 발생된 상태 그대로 보여주도록 원복함
            INSERT INTO TB_OPR_ESS_STATE_TMP (
	              DEVICE_ID, DEVICE_TYPE_CD, COLEC_DT, OPER_STUS_CD 
	            , PV_PW, PV_PW_H, PV_PW_PRICE, PV_PW_CO2, PV_STUS_CD 
	            , CONS_PW, CONS_PW_H, CONS_PW_PRICE 
	            , CONS_PW_CO2 
	            , BT_PW, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2, BT_DCHRG_PW_H, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2, BT_SOC, BT_SOH, BT_STUS_CD
	            , GRID_PW, GRID_OB_PW_H, GRID_OB_PW_PRICE, GRID_OB_PW_CO2, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2, GRID_STUS_CD
	            , PCS_PW, PCS_FD_PW_H, PCS_PCH_PW_H, EMS_OPMODE, PCS_OPMODE1, PCS_OPMODE2, PCS_TGT_PW, FEED_IN_LIMIT, MAX_INVERTER_PW_CD
	            , OUTLET_PW, OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2
	            , CREATE_DT
	            -- [ 2015.07.06 yngwie 수집 컬럼 추가 
	            , PCS_COMM_ERR_RATE, SMTR_COMM_ERR_RATE, M2M_COMM_ERR_RATE, SMTR_TP_CD, SMTR_PULSE_CNT, SMTR_MODL_CD
	            , PV_MAX_PWR1, PV_MAX_PWR2, INSTL_RGN, MMTR_SLV_ADDR, BASICMODE_CD
	            , PCS_FLAG0, PCS_FLAG1, PCS_FLAG2, PCS_OPMODE_CMD1, PCS_OPMODE_CMD2, PV_V1, PV_I1, PV_PW1, PV_V2, PV_I2, PV_PW2
	            , INVERTER_V, INVERTER_I, INVERTER_PW, DC_LINK_V
	            , G_RLY_CNT, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT
	            , BMS_FLAG0, BMS_DIAG0, RACK_V, RACK_I
	            , CELL_MAX_V, CELL_MIN_V, CELL_AVG_V, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T, TRAY_CNT
	            , SYS_ST_CD, BT_CHRG_ST_CD, BT_DCHRG_ST_CD, PV_ST_CD, GRID_ST_CD, SMTR_ST_CD, SMTR_TR_CNTER, SMTR_OB_CNTER
	            , DN_ENABLE, DC_START, DC_END, NC_START, NC_END
	            -- ]            
	           	--[신규모델에 대한 모니터링 추가
	           	, GRID_CODE0, GRID_CODE1, GRID_CODE2, PCON_BAT_TARGETPOWER, GRID_CODE3
	           	, BDC_MODULE_CODE0, BDC_MODULE_CODE1, BDC_MODULE_CODE2, BDC_MODULE_CODE3
	           	, FAULT5_REALTIME_YEAR, FAULT5_REALTIME_MONTH, FAULT5_DAY, FAULT5_HOUR, FAULT5_MINUTE, FAULT5_SECOND, FAULT5_DATAHIGH, FAULT5_DATALOW
	           	, FAULT6_REALTIME_YEAR, FAULT6_REALTIME_MONTH, FAULT6_REALTIME_DAY, FAULT6_REALTIME_HOUR, FAULT6_REALTIME_MINUTE, FAULT6_REALTIME_SECOND, FAULT6_DATAHIGH, FAULT6_DATALOW
	           	, FAULT7_REALTIME_YEAR, FAULT7_REALTIME_MONTH, FAULT7_REALTIME_DAY, FAULT7_REALTIME_HOUR, FAULT7_REALTIME_MINUTE, FAULT7_REALTIME_SECOND, FAULT7_DATAHIGH, FAULT7_DATALOW
	           	, FAULT8_REALTIME_YEAR, FAULT8_REALTIME_MONTH, FAULT8_REALTIME_DAY, FAULT8_REALTIME_HOUR, FAULT8_REALTIME_MINUTE, FAULT8_REALTIME_SECOND, FAULT8_DATAHIGH, FAULT8_DATALOW 
	           	, FAULT9_REALTIME_YEAR, FAULT9_REALTIME_MONTH, FAULT9_REALTIME_DAY, FAULT9_REALTIME_HOUR, FAULT9_REALTIME_MINUTE, FAULT9_REALTIME_SECOND, FAULT9_DATAHIGH, FAULT9_DATALOW
	           	--] 
            ) 
            SELECT  
                 A.DEVICE_ID AS DEVICE_ID   
                , B.DEVICE_TYPE_CD AS DEVICE_TYPE_CD   
                , A.COLEC_DT AS COLEC_DT   
                , 0 AS OPER_STUS_CD 
                , A.COL8::NUMERIC AS PV_PW   
                , A.COL17::NUMERIC AS PV_PW_H   
                , TRUNC(ABS(A.COL17::NUMERIC) * ET.UNIT_PRICE * .001, 2) AS PV_PW_PRICE   
                , ABS(A.COL17::NUMERIC) * D.CARB_EMI_QUITY * .001 AS PV_PW_CO2   
                , SUBSTR(A.COL88, 3, 1) AS PV_STUS_CD   
                , A.COL7::NUMERIC AS CONS_PW   
                , A.COL16::NUMERIC AS CONS_PW_H   
                , TRUNC(ABS(A.COL16::NUMERIC) * E.UNIT_PRICE * .001, 2) AS CONS_PW_PRICE   
                , ABS(A.COL16::NUMERIC) * D.CARB_EMI_QUITY * .001 AS CONS_PW_CO2   
                , A.COL10::NUMERIC AS BT_PW   
                , A.COL20::NUMERIC AS BT_CHRG_PW_H   
                , TRUNC(ABS(A.COL20::NUMERIC) * E.UNIT_PRICE * .001, 2) AS BT_CHRG_PW_PRICE   
                , ABS(A.COL20::NUMERIC) * D.CARB_EMI_QUITY * .001 AS BT_CHRG_PW_CO2   
                , A.COL21::NUMERIC AS BT_DCHRG_PW_H   
                , TRUNC(ABS(A.COL21::NUMERIC) * E.UNIT_PRICE * .001, 2) AS BT_DCHRG_PW_PRICE   
                , ABS(A.COL21::NUMERIC) * D.CARB_EMI_QUITY * .001 AS BT_DCHRG_PW_CO2   
                , CASE WHEN (A.COL11::NUMERIC >= 1000 OR A.COL11::NUMERIC < -999) THEN -999 ELSE A.COL11::NUMERIC END AS BT_SOC   
                , (COALESCE(FN_GET_CONV_NA(A.COL13), '0'))::NUMERIC AS BT_SOH   
                , CASE   
                     WHEN SUBSTR(A.COL88, 1, 1) = '1' THEN '0'  
                     WHEN SUBSTR(A.COL88, 2, 1) = '1' THEN '1'  
                     ELSE '2'   
                  END AS BT_STUS_CD   
                , A.COL6::NUMERIC AS GRID_PW   
                , A.COL15::NUMERIC AS GRID_OB_PW_H   
                , TRUNC(ABS(A.COL15::NUMERIC) * E.UNIT_PRICE * .001, 2) AS GRID_OB_PW_PRICE   
                , ABS(A.COL15::NUMERIC) * D.CARB_EMI_QUITY * .001 AS GRID_OB_PW_CO2   
                , A.COL14::NUMERIC AS GRID_TR_PW_H   
                , TRUNC(ABS(A.COL14::NUMERIC) * ET.UNIT_PRICE * .001, 2) AS GRID_TR_PW_PRICE   
                , ABS(A.COL14::NUMERIC) * D.CARB_EMI_QUITY * .001 AS GRID_TR_PW_CO2   
                , CASE   
                     WHEN COALESCE(A.COL6, '0')::NUMERIC > 0 THEN '0'
                     WHEN COALESCE(A.COL6, '0')::NUMERIC = 0 THEN '2'
                     ELSE '1'  
                  END GRID_STUS_CD   
                , A.COL9::NUMERIC AS PCS_PW   
                , A.COL18::NUMERIC AS PCS_FD_PW_H   
                , A.COL19::NUMERIC AS PCS_PCH_PW_H  
                , A.COL0 AS EMS_OPMODE    
                , A.COL1 AS PCS_OPMODE1   
                , A.COL2 AS PCS_OPMODE2   
                , A.COL3::NUMERIC AS PCS_TGT_PW   
                , A.COL331 AS FEED_IN_LIMIT   
                , A.COL332 AS MAX_INVERTER_PW_CD 
            	-- [ 2015.05.13 yngwie OUTLET_PW 추가
               	, FN_GET_CONV_NA(A.COL5)::NUMERIC AS OUTLET_PW   
               	, FN_GET_CONV_NA(A.COL22)::NUMERIC AS OUTLET_PW_H   
               	, TRUNC(ABS(FN_GET_CONV_NA(A.COL22)::NUMERIC) * E.UNIT_PRICE * .001, 2) AS OUTLET_PW_PRICE   
               	, ABS(FN_GET_CONV_NA(A.COL22)::NUMERIC) * D.CARB_EMI_QUITY * .001 AS OUTLET_PW_CO2   
            	-- ]
               	, v_baseTm AS CREATE_DT    
            	-- [ 2015.07.06 yngwie 수집 컬럼 추가
				, FN_GET_CONV_NA(A.COL34)::NUMERIC AS PCS_COMM_ERR_RATE 
				, FN_GET_CONV_NA(A.COL35)::NUMERIC AS SMTR_COMM_ERR_RATE 
				, FN_GET_CONV_NA(A.COL36)::NUMERIC AS M2M_COMM_ERR_RATE          
				, A.COL324 AS SMTR_TP_CD
				, FN_GET_CONV_NA(A.COL325)::NUMERIC AS SMTR_PULSE_CNT 
				, A.COL326 AS SMTR_MODL_CD
				, FN_GET_CONV_NA(A.COL328)::NUMERIC AS PV_MAX_PWR1 
				, FN_GET_CONV_NA(A.COL329)::NUMERIC AS PV_MAX_PWR2 
				, A.COL330 AS INSTL_RGN
				, FN_GET_CONV_NA(A.COL334)::NUMERIC AS MMTR_SLV_ADDR 
				, A.COL335 AS BASICMODE_CD 
	            , P.COL0                                                       AS PCS_FLAG0 
	            , P.COL1                                                       AS PCS_FLAG1 
	            , P.COL2                                                       AS PCS_FLAG2 
	            -- ]
	            -- [ 2015.07.13 modified by stjoo : P.COL20s value was NA on test ems.
              	, FN_GET_CONV_NA(P.COL20)                             AS PCS_OPMODE_CMD1 
              	, P.COL21                                                      AS PCS_OPMODE_CMD2 
              	, FN_GET_CONV_NA(P.COL38)::NUMERIC   AS PV_V1 
              	, FN_GET_CONV_NA(P.COL39)::NUMERIC   AS PV_I1 
				, CASE WHEN FN_GET_CONV_NA(P.COL40)::NUMERIC >= 100000 OR FN_GET_CONV_NA(P.COL40)::NUMERIC <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL40)::NUMERIC END AS PV_PW1  
				, FN_GET_CONV_NA(P.COL42)::NUMERIC   AS PV_V2 
				, FN_GET_CONV_NA(P.COL43)::NUMERIC   AS PV_I2 
				, CASE WHEN FN_GET_CONV_NA(P.COL44)::NUMERIC >= 100000 OR FN_GET_CONV_NA(P.COL44)::NUMERIC <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL44)::NUMERIC END AS PV_PW2  
				, CASE WHEN FN_GET_CONV_NA(P.COL53)::NUMERIC >= 100000 OR FN_GET_CONV_NA(P.COL53)::NUMERIC <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL53)::NUMERIC END AS INVERTER_V 
				, CASE WHEN FN_GET_CONV_NA(P.COL54)::NUMERIC >= 10000 OR FN_GET_CONV_NA(P.COL54)::NUMERIC <= -10000 THEN 0 ELSE FN_GET_CONV_NA(P.COL54)::NUMERIC END AS INVERTER_I 
				, FN_GET_CONV_NA(P.COL55)::NUMERIC   AS INVERTER_PW 
				, CASE WHEN FN_GET_CONV_NA(P.COL59)::NUMERIC >= 100000 OR FN_GET_CONV_NA(P.COL59)::NUMERIC <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL59)::NUMERIC END AS DC_LINK_V 
				, FN_GET_CONV_NA(P.COL61)::NUMERIC   AS G_RLY_CNT 
				, FN_GET_CONV_NA(P.COL62)::NUMERIC   AS BAT_RLY_CNT 
				, FN_GET_CONV_NA(P.COL63)::NUMERIC   AS GRID_TO_GRID_RLY_CNT 
				, FN_GET_CONV_NA(P.COL64)::NUMERIC   AS BAT_PCHRG_RLY_CNT   
				, M.COL0                               AS BMS_FLAG0 
				, M.COL1                               AS BMS_DIAG0 
				, FN_GET_CONV_NA(M.COL16)::NUMERIC   AS RACK_V 
				, FN_GET_CONV_NA(M.COL17)::NUMERIC   AS RACK_I 
				, FN_GET_CONV_NA(M.COL18)::NUMERIC   AS CELL_MAX_V 
				, FN_GET_CONV_NA(M.COL19)::NUMERIC   AS CELL_MIN_V 
				, FN_GET_CONV_NA(M.COL20)::NUMERIC   AS CELL_AVG_V 
				, FN_GET_CONV_NA(M.COL21)::NUMERIC   AS CELL_MAX_T 
				, FN_GET_CONV_NA(M.COL22)::NUMERIC   AS CELL_MIN_T 
				, FN_GET_CONV_NA(M.COL23)::NUMERIC   AS CELL_AVG_T 
				, FN_GET_CONV_NA(M.COL24)::NUMERIC   AS TRAY_CNT 
				, CASE WHEN SUBSTR(A.COL88, 4, 1) = '0' AND SUBSTR(A.COL88, 8, 1) = '1' THEN '1' ELSE '0' END AS SYS_ST_CD
				, SUBSTR(A.COL89, 7, 1) AS BT_CHRG_ST_CD 
				, SUBSTR(A.COL89, 6, 1) AS BT_DCHRG_ST_CD 
				, SUBSTR(A.COL89, 8, 1) AS PV_ST_CD 
				, SUBSTR(A.COL89, 5, 1) AS GRID_ST_CD 
				, SUBSTR(A.COL89, 4, 1) AS SMTR_ST_CD 
				, A.COL127::NUMERIC AS SMTR_TR_CNTER 
				, A.COL128::NUMERIC AS SMTR_OB_CNTER 
				, A.COL337 AS DN_ENABLE 
				, A.COL338 AS DC_START 
				, A.COL339 AS DC_END 
				, A.COL340 AS NC_START 
				, A.COL341 AS NC_END 
            	-- ]     
				--[신규모델로 모니터링 추가
	            , CASE WHEN P.COL3  ='NA' THEN NULL ELSE P.COL3   END as GRID_CODE0 
	            , CASE WHEN P.COL4  ='NA' THEN NULL ELSE P.COL4   END as GRID_CODE1 
	            , CASE WHEN P.COL5  ='NA' THEN NULL ELSE P.COL5   END as GRID_CODE2 
	            , CASE WHEN P.COL19 ='NA' THEN NULL ELSE P.COL19  END as PCON_BAT_TARGETPOWER 
	            , CASE WHEN P.COL31 ='NA' THEN NULL ELSE P.COL31  END as GRID_CODE3 
	            , CASE WHEN P.COL32 ='NA' THEN NULL ELSE P.COL32  END as BDC_MODULE_CODE0 
	            , CASE WHEN P.COL33 ='NA' THEN NULL ELSE P.COL33  END as BDC_MODULE_CODE1 
	            , CASE WHEN P.COL34 ='NA' THEN NULL ELSE P.COL34  END as BDC_MODULE_CODE2 
	            , CASE WHEN P.COL35 ='NA' THEN NULL ELSE P.COL35  END as BDC_MODULE_CODE3 
	            , CASE WHEN P.COL36 ='NA' THEN NULL ELSE P.COL36  END as FAULT5_REALTIME_YEAR 
	            , CASE WHEN P.COL37 ='NA' THEN NULL ELSE P.COL37  END as FAULT5_REALTIME_MONTH 
	            , CASE WHEN P.COL87 ='NA' THEN NULL ELSE P.COL87  END as FAULT5_DAY 
	            , CASE WHEN P.COL88 ='NA' THEN NULL ELSE P.COL88  END as FAULT5_HOUR 
	            , CASE WHEN P.COL89 ='NA' THEN NULL ELSE P.COL89  END as FAULT5_MINUTE 
	            , CASE WHEN P.COL90 ='NA' THEN NULL ELSE P.COL90  END as FAULT5_SECOND 
	            , CASE WHEN P.COL91 ='NA' THEN NULL ELSE P.COL91  END as FAULT5_DATAHIGH 
	            , CASE WHEN P.COL162='NA' THEN NULL ELSE P.COL162 END as FAULT5_DATALOW 
	            , CASE WHEN P.COL163='NA' THEN NULL ELSE P.COL163 END as FAULT6_REALTIME_YEAR 
	            , CASE WHEN P.COL164='NA' THEN NULL ELSE P.COL164 END as FAULT6_REALTIME_MONTH 
	            , CASE WHEN P.COL165='NA' THEN NULL ELSE P.COL165 END as FAULT6_REALTIME_DAY 
	            , CASE WHEN P.COL166='NA' THEN NULL ELSE P.COL166 END as FAULT6_REALTIME_HOUR 
	            , CASE WHEN P.COL167='NA' THEN NULL ELSE P.COL167 END as FAULT6_REALTIME_MINUTE 
	            , CASE WHEN P.COL168='NA' THEN NULL ELSE P.COL168 END as FAULT6_REALTIME_SECOND 
	            , CASE WHEN P.COL169='NA' THEN NULL ELSE P.COL169 END as FAULT6_DATAHIGH 
	            , CASE WHEN P.COL170='NA' THEN NULL ELSE P.COL170 END as FAULT6_DATALOW 
	            , CASE WHEN P.COL171='NA' THEN NULL ELSE P.COL171 END as FAULT7_REALTIME_YEAR 
	            , CASE WHEN P.COL177='NA' THEN NULL ELSE P.COL177 END as FAULT7_REALTIME_MONTH 
	            , CASE WHEN P.COL178='NA' THEN NULL ELSE P.COL178 END as FAULT7_REALTIME_DAY 
	            , CASE WHEN P.COL179='NA' THEN NULL ELSE P.COL179 END as FAULT7_REALTIME_HOUR 
	            , CASE WHEN P.COL187='NA' THEN NULL ELSE P.COL187 END as FAULT7_REALTIME_MINUTE 
	            , CASE WHEN P.COL188='NA' THEN NULL ELSE P.COL188 END as FAULT7_REALTIME_SECOND 
	            , CASE WHEN P.COL189='NA' THEN NULL ELSE P.COL189 END as FAULT7_DATAHIGH 
	            , CASE WHEN P.COL191='NA' THEN NULL ELSE P.COL191 END as FAULT7_DATALOW 
	            , CASE WHEN P.COL192='NA' THEN NULL ELSE P.COL192 END as FAULT8_REALTIME_YEAR 
	            , CASE WHEN P.COL193='NA' THEN NULL ELSE P.COL193 END as FAULT8_REALTIME_MONTH 
	            , CASE WHEN P.COL194='NA' THEN NULL ELSE P.COL194 END as FAULT8_REALTIME_DAY 
	            , CASE WHEN P.COL195='NA' THEN NULL ELSE P.COL195 END as FAULT8_REALTIME_HOUR 
	            , CASE WHEN P.COL196='NA' THEN NULL ELSE P.COL196 END as FAULT8_REALTIME_MINUTE 
	            , CASE WHEN P.COL197='NA' THEN NULL ELSE P.COL197 END as FAULT8_REALTIME_SECOND 
	            , CASE WHEN P.COL198='NA' THEN NULL ELSE P.COL198 END as FAULT8_DATAHIGH 
	            , CASE WHEN P.COL199='NA' THEN NULL ELSE P.COL199 END as FAULT8_DATALOW 
	            , CASE WHEN P.COL200='NA' THEN NULL ELSE P.COL200 END as FAULT9_REALTIME_YEAR 
	            , CASE WHEN P.COL201='NA' THEN NULL ELSE P.COL201 END as FAULT9_REALTIME_MONTH 
	            , CASE WHEN P.COL202='NA' THEN NULL ELSE P.COL202 END as FAULT9_REALTIME_DAY 
	            , CASE WHEN P.COL203='NA' THEN NULL ELSE P.COL203 END as FAULT9_REALTIME_HOUR 
	            , CASE WHEN P.COL204='NA' THEN NULL ELSE P.COL204 END as FAULT9_REALTIME_MINUTE 
	            , CASE WHEN P.COL205='NA' THEN NULL ELSE P.COL205 END as FAULT9_REALTIME_SECOND 
	            , CASE WHEN P.COL206='NA' THEN NULL ELSE P.COL206 END as FAULT9_DATAHIGH 
	            , CASE WHEN P.COL207='NA' THEN NULL ELSE P.COL207 END as FAULT9_DATALOW 
	            --]
            FROM  	 TB_RAW_EMS_INFO_TMP A
		                , TB_RAW_PCS_INFO_TMP P   
		                , TB_RAW_BMS_INFO_TMP M   
		                , TB_BAS_DEVICE B   
		                , TB_PLF_CARB_EMI_QUITY D   
		                , TB_BAS_ELPW_UNIT_PRICE E 
		                , TB_BAS_ELPW_UNIT_PRICE ET 
            WHERE  A.DEVICE_ID = P.DEVICE_ID 
                AND A.COLEC_DT = P.COLEC_DT   
                AND A.DEVICE_ID = M.DEVICE_ID   
                AND A.COLEC_DT = M.COLEC_DT   
                AND A.DEVICE_ID = B.DEVICE_ID   
                AND B.CNTRY_CD = D.CNTRY_CD   
                AND D.USE_FLAG = 'Y'
                AND B.ELPW_PROD_CD = E.ELPW_PROD_CD   
                AND E.IO_FLAG = 'O'
                AND E.USE_FLAG = 'Y'
            -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가
                AND E.WKDAY_FLAG = FN_GET_WKDAY_FLAG(SUBSTR(A.COLEC_DT, 1, 8)) 
            -- 2015.07.31 요금제 년월 변경
            --  AND SUBSTR(A.COLEC_DT, 1, 6) = E.UNIT_YM   
                AND E.UNIT_YM = '000000'
                AND SUBSTR(A.COLEC_DT, 9, 2) = E.UNIT_HH 
                AND B.ELPW_PROD_CD = ET.ELPW_PROD_CD  
                AND ET.IO_FLAG = 'T'   
                AND ET.USE_FLAG = 'Y'  
            -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가, T 는  1 만 있음
            -- 2015.08.06 modified by yngwie T 도 주말 0 추가
            --  AND ET.WKDAY_FLAG = '1'   
                AND ET.WKDAY_FLAG = FN_GET_WKDAY_FLAG(SUBSTR(A.COLEC_DT, 1, 8))
            -- 2015.07.31 요금제 년월 변경
            --  AND SUBSTR(A.COLEC_DT, 1, 6) = ET.UNIT_YM  
                AND ET.UNIT_YM = '000000'   
                AND SUBSTR(A.COLEC_DT, 9, 2) = ET.UNIT_HH
            ;

	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--	      	RAISE notice 'i_sp_nm[%] Insert TB_OPR_ESS_STATE_TMP Table :  [%]건', i_sp_nm, v_work_num;
            
            -- 발생한 ALARM_TYPE_CD 를  가지고 OPER_STUS_CD 를 업데이트 한다.
            -- 2014.08.12 E700번대 장애는 사용자에게 보여주지 않도록 수정
            -- 2014.08.20 E900번대와 C999 장애만 사용자에게 보여주도록 수정 => 장애 발생된 상태 그대로 보여주도록 원복함
            -- [ 2015.01.07 
--                SELECT * FROM TB_PLF_CODE_INFO WHERE GRP_CD = 'OPER_STUS_CD';
--                OPER_STUS_CD = 0 : 운전중
--                OPER_STUS_CD = 1 : 경고
--                OPER_STUS_CD = 2 : 오류
--                OPER_STUS_CD = 3 : 통신불가
--                OPER_STUS_CD = 4 : 미인증
--               ] 
	      
           	v_rslt_cd := 'K';

	      	v_ExeCnt1 := 0;
			v_ExeCnt2 := 0;
			v_ExeCnt3 := 0;

		
            WITH ERR_INFO AS (  
                SELECT  
                    DEVICE_ID   
                    , DEVICE_TYPE_CD   
                    , CASE    
                        WHEN MAX(ALARM_TYPE_CD::INTEGER) = 1 AND ALARM_CD = 'C128' THEN '3'
                        WHEN MAX(ALARM_TYPE_CD::INTEGER) = 3 THEN '2' 
--                      WHEN MAX(ALARM_TYPE_CD::INTEGER) IN (1, 2) AND ALARM_CD <> 'C128' THEN '1'
                        WHEN MAX(ALARM_TYPE_CD::INTEGER) = 2 THEN '1'
                        ELSE '0'
                      END AS OPER_STUS_CD
                    , MAX(EVENT_DT) AS EVENT_DT
                FROM TB_OPR_ALARM 
            --E202오류의 경우 사용자에게 보여주지 않도록 한다.
                WHERE ALARM_CD NOT IN ('E202')  
                GROUP BY   DEVICE_ID, DEVICE_TYPE_CD, ALARM_CD
             ), DEV as (
	            SELECT 	 DEVICE_ID 
			                , DEVICE_TYPE_CD 
			                , MAX(OPER_STUS_CD) AS OPER_STUS_CD 
			                , MAX(EVENT_DT) AS EVENT_DT 
	            FROM 	ERR_INFO 
	            GROUP BY  DEVICE_ID, DEVICE_TYPE_CD
             ), U1 as (
				-- 통신 단절인 장비의 경우 1분 데이터가 수집되지 않으므로 상태를 현황 임시 테이블에도 Update 한다.
				UPDATE TB_OPR_ESS_STATE_TMP U
				SET 		OPER_STUS_CD = DEV.OPER_STUS_CD 
				FROM 	DEV
				WHERE 	U.DEVICE_ID = DEV.DEVICE_ID
				AND 		U.DEVICE_TYPE_CD =DEV.DEVICE_TYPE_CD
				
				returning *
             ), U2 as (
	            -- 통신 단절인 장비는 현황 임시 테이블에 데이터가 수집되지 않을 수 있으니 현황 테이블에도 Update 한다.
	            UPDATE 	TB_OPR_ESS_STATE U
	            SET 		OPER_STUS_CD = DEV.OPER_STUS_CD
				FROM 	DEV
	            WHERE 	U.DEVICE_ID = DEV.DEVICE_ID
	            AND 		U.DEVICE_TYPE_CD = DEV.DEVICE_TYPE_CD

				returning *
             ), U3 as (
	            UPDATE 	TB_OPR_ESS_STATE_HIST U
	            SET 		OPER_STUS_CD = DEV.OPER_STUS_CD
				FROM 	DEV
	            WHERE 	U.DEVICE_ID = DEV.DEVICE_ID
	            AND 		U.DEVICE_TYPE_CD = DEV.DEVICE_TYPE_CD
	            AND 		U.COLEC_DT = DEV.EVENT_DT

				returning *
             )
             SELECT SUM(cnt1), SUM(cnt2), SUM(cnt3)
             INTO 	v_ExeCnt1, v_ExeCnt2, v_ExeCnt3
             FROM (
	            SELECT 	COUNT(U1.*)::INTEGER cnt1, 0 cnt2, 0 cnt3
	            FROM 	U1
				UNION ALL
	            SELECT 	0 cnt1, COUNT(U2.*)::INTEGER cnt2, 0 cnt3
	            FROM 	U2
				UNION 	ALL
	            SELECT 	0 cnt1, 0 cnt2, COUNT(U3.*)::INTEGER cnt3
	            FROM 	U3
			) Z;
            
            GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		
--	      	RAISE NOTICE 'i_sp_nm[%] Update TB_OPR_ESS_STATE_TMP Table For OPER_STUS_CD : [%]', i_sp_nm, v_ExeCnt1;
--	      	RAISE NOTICE 'i_sp_nm[%] Update TB_OPR_ESS_STATE Table For OPER_STUS_CD : [%]', i_sp_nm, v_ExeCnt2;
--	      	RAISE NOTICE 'i_sp_nm[%] Update TB_OPR_ESS_STATE_HIST Table For OPER_STUS_CD : [%]', i_sp_nm, v_ExeCnt3;
   
       
            -- #411 Sleep 모드의 상태의 장비를 절전 상태로 상태 변경을 한다. 20160105 jkm1020
          	v_rslt_cd := 'L';

	      	v_ExeCnt1 := 0;
			v_ExeCnt2 := 0;
			v_ExeCnt3 := 0;

            WITH ess_st AS (
	            SELECT  DEVICE_ID  
	                 	, DEVICE_TYPE_CD   
	                 	, '5' AS OPER_STUS_CD  
	                 	, COLEC_DT
	            FROM   TB_OPR_ESS_STATE_TMP 
	            WHERE  EMS_OPMODE = '9'
	        ), u1 as (
	            -- 통신 단절인 장비의 경우 1분 데이터가 수집되지 않으므로 상태를 현황 임시 테이블에도 Update 한다.
	            UPDATE 	TB_OPR_ESS_STATE_TMP B
	            SET 		OPER_STUS_CD =A.OPER_STUS_CD
	            FROM 	ess_st A 
	            WHERE 	B.DEVICE_ID = A.DEVICE_ID
	            AND 		B.DEVICE_TYPE_CD = A.DEVICE_TYPE_CD
	            returning *
			), u2 as (
	            -- 통신 단절인 장비는 현황 임시 테이블에 데이터가 수집되지 않을 수 있으니 현황 테이블에도 Update 한다.
	            UPDATE 	TB_OPR_ESS_STATE B
	            SET 		OPER_STUS_CD = A.OPER_STUS_CD
	            FROM		ess_st A
	            WHERE 	B.DEVICE_ID = A.DEVICE_ID
	            AND 		B.DEVICE_TYPE_CD = A.DEVICE_TYPE_CD
	            returning *
			), u3 as (
	            UPDATE 	TB_OPR_ESS_STATE_HIST B
	            SET 		OPER_STUS_CD = A.OPER_STUS_CD
	            FROM 	ess_st A
	            WHERE 	B.DEVICE_ID =A.DEVICE_ID
	            AND 		B.DEVICE_TYPE_CD = A.DEVICE_TYPE_CD
	            AND 		B.COLEC_DT = A.COLEC_DT
	            returning *
			)
             SELECT 	SUM(cnt1), SUM(cnt2), SUM(cnt3)
             INTO 	v_ExeCnt1, v_ExeCnt2, v_ExeCnt3
             FROM (
	            SELECT 	COUNT(u1.*)::INTEGER cnt1, 0 cnt2, 0 cnt3
	            FROM 	u1
				UNION ALL
	            SELECT 	0 cnt1, COUNT(u2.*)::INTEGER cnt2, 0 cnt3
	            FROM 	u2
				UNION ALL
	            SELECT 	0 cnt1, 0 cnt2, COUNT(u3.*)::INTEGER cnt3
	            FROM 	u3
			) Z;

--	      	RAISE notice 'i_sp_nm[%] Update TB_OPR_ESS_STATE_TMP Table For OPER_STUS_CD Sleep Mode : [%]', i_sp_nm, v_ExeCnt1;
--	      	RAISE notice 'i_sp_nm[%] Update TB_OPR_ESS_STATE Table For OPER_STUS_CD Sleep Mode : [%]', i_sp_nm, v_ExeCnt2;
--	      	RAISE notice 'i_sp_nm[%] Update TB_OPR_ESS_STATE_HIST Table For OPER_STUS_CD Sleep Mode : [%]', i_sp_nm, v_ExeCnt3;

          	v_rslt_cd := 'M';

            --- 	2015.07.07 yngwie 장비별 상태 타임라인 데이터 처리
            v_sql := FORMAT('
	            WITH PRIORITY AS ( 
	                SELECT 1 AS PRIORITY, ''2'' AS OPER_STUS_CD UNION ALL 
	                SELECT 2 AS PRIORITY, ''1'' AS OPER_STUS_CD UNION ALL 
	                SELECT 3 AS PRIORITY, ''3'' AS OPER_STUS_CD 
	            ), UALL AS ( 
	                SELECT 	''%s'' AS COLEC_DD 
			                    , A.DEVICE_ID 
			                    , A.DEVICE_TYPE_CD 
			                    , A.OPER_STUS_CD 
			                    , B.PRIORITY 
	                FROM 	TB_OPR_ESS_STATE A
			                    , PRIORITY B 
	                WHERE 	A.OPER_STUS_CD = B.OPER_STUS_CD 
	                UNION ALL 
	                SELECT   A.COLEC_DD 
			                    , A.DEVICE_ID 
			                    , A.DEVICE_TYPE_CD 
			                    , A.OPER_STUS_CD 
			                    , B.PRIORITY 
	                FROM 	TB_OPR_ESS_STATE_TL A 
			                    , PRIORITY B 
	                WHERE   A.OPER_STUS_CD = B.OPER_STUS_CD 
	        	    AND 		A.COLEC_DD = ''%s''
	            ), B  AS ( 
	                SELECT    * 
	                FROM ( 
	                    SELECT 	COLEC_DD 
			                        , DEVICE_ID 
			                        , DEVICE_TYPE_CD 
			                        , OPER_STUS_CD 
			                        , PRIORITY 
			                        , ROW_NUMBER() OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY PRIORITY) AS RK 
	                    FROM 	UALL 
	                ) T
	                WHERE RK = 1 
	            ), U AS (
	                UPDATE TB_OPR_ESS_STATE_TL A
	                SET        OPER_STUS_CD = B.OPER_STUS_CD 
	                FROM    B
					WHERE   A.DEVICE_ID = B.DEVICE_ID 
	                AND 		A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD 
	                AND 		A.COLEC_DD = B.COLEC_DD 
					
	                RETURNING A.*
	            )
	            INSERT INTO TB_OPR_ESS_STATE_TL ( DEVICE_ID, DEVICE_TYPE_CD, COLEC_DD, OPER_STUS_CD ) 
	            SELECT 	B.DEVICE_ID, B.DEVICE_TYPE_CD, B.COLEC_DD, B.OPER_STUS_CD 
	           	FROM 	B
				WHERE NOT EXISTS (  
					SELECT 	1 
				    FROM 	U 
					WHERE	COLEC_DD 			= B.COLEC_DD  
						AND 	DEVICE_ID			= B.DEVICE_ID  
						AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
				  ) ; '
	              , SUBSTR(v_handleFlag, 1, 8), SUBSTR(v_handleFlag, 1, 8) );
             
          	EXECUTE v_sql;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--	      	RAISE notice 'i_sp_nm[%] Merge TB_OPR_ESS_STATE_TL :  [%]건 SQL[%]', i_sp_nm, v_work_num, v_sql;

           	v_rslt_cd := 'N';

            -- TEMP 테이블의 레코드 중 가장 최근 데이터를 상태정보 테이블에 Merge
            WITH B AS (
				SELECT	* 
				FROM (   
				        SELECT	ROW_NUMBER() OVER (PARTITION BY A.DEVICE_ID ORDER BY A.COLEC_DT DESC ) SEQ   
				            		, A.*   
				        FROM   TB_OPR_ESS_STATE_TMP A   
				    ) A 
				    WHERE A.SEQ = 1 
            ), U as ( 
	            UPDATE TB_OPR_ESS_STATE A
	            SET
		              COLEC_DT         		= B.COLEC_DT           
		            , OPER_STUS_CD     	= B.OPER_STUS_CD       
		            , PV_PW            		= B.PV_PW              
		            , PV_PW_H          		= B.PV_PW_H            
		            , PV_PW_PRICE      	= B.PV_PW_PRICE        
		            , PV_PW_CO2        	= B.PV_PW_CO2          
		            , PV_STUS_CD       	= B.PV_STUS_CD         
		            , CONS_PW          	= B.CONS_PW            
		            , CONS_PW_H        	= B.CONS_PW_H          
		            , CONS_PW_PRICE    	= B.CONS_PW_PRICE      
		            , CONS_PW_CO2      	= B.CONS_PW_CO2        
		            , BT_PW            		= B.BT_PW              
		            , BT_CHRG_PW_H     	= B.BT_CHRG_PW_H       
		            , BT_CHRG_PW_PRICE = B.BT_CHRG_PW_PRICE   
		            , BT_CHRG_PW_CO2   = B.BT_CHRG_PW_CO2     
		            , BT_DCHRG_PW_H    	= B.BT_DCHRG_PW_H      
		            , BT_DCHRG_PW_PRICE= B.BT_DCHRG_PW_PRICE  
		            , BT_DCHRG_PW_CO2 	= B.BT_DCHRG_PW_CO2    
		            , BT_SOC           		= B.BT_SOC             
		            , BT_SOH           		= B.BT_SOH             
		            , BT_STUS_CD       	= B.BT_STUS_CD         
		            , GRID_PW          		= B.GRID_PW            
		            , GRID_OB_PW_H     	= B.GRID_OB_PW_H          
		            , GRID_OB_PW_PRICE 	= B.GRID_OB_PW_PRICE      
		            , GRID_OB_PW_CO2   	= B.GRID_OB_PW_CO2        
		            , GRID_TR_PW_H     	= B.GRID_TR_PW_H       
		            , GRID_TR_PW_PRICE 	= B.GRID_TR_PW_PRICE   
		            , GRID_TR_PW_CO2   	= B.GRID_TR_PW_CO2     
		            , GRID_STUS_CD     	= B.GRID_STUS_CD       
		            , PCS_PW           		= B.PCS_PW             
		            , PCS_FD_PW_H      	= B.PCS_FD_PW_H        
		            , PCS_PCH_PW_H     	= B.PCS_PCH_PW_H       
		            , CREATE_DT        		= COALESCE(B.CREATE_DT, v_baseTm)           
		            , EMS_OPMODE       	= B.EMS_OPMODE       
		            , PCS_OPMODE1      	= B.PCS_OPMODE1       
		            , PCS_OPMODE2      	= B.PCS_OPMODE2       
		            , PCS_TGT_PW       	= B.PCS_TGT_PW       
		            , FEED_IN_LIMIT    	= B.FEED_IN_LIMIT       
		            , MAX_INVERTER_PW_CD    = B.MAX_INVERTER_PW_CD       
		            --, RFSH_PRD = ROUND((TO_DATE(B.COLEC_DT, 'YYYYMMDDHH24MISS') - TO_DATE(A.COLEC_DT, 'YYYYMMDDHH24MISS')) * 60 * 60 * 24) 
		            , RFSH_PRD = 60 
		            -- [ 2015.05.13 yngwie OUTLET_PW 추가
		            , OUTLET_PW        	= B.OUTLET_PW       
		            , OUTLET_PW_H      	= B.OUTLET_PW_H       
		            , OUTLET_PW_PRICE  	= B.OUTLET_PW_PRICE       
		            , OUTLET_PW_CO2    	= B.OUTLET_PW_CO2       
		            , PWR_RANGE_CD_ESS_GRID    	= FN_GET_PWR_RANGE_CD('ESS_GRID',  B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , PWR_RANGE_CD_GRID_ESS    	= FN_GET_PWR_RANGE_CD('GRID_ESS',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , PWR_RANGE_CD_GRID_LOAD  = FN_GET_PWR_RANGE_CD('GRID_LOAD', B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , PWR_RANGE_CD_ESS_LOAD    = FN_GET_PWR_RANGE_CD('ESS_LOAD',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , PWR_RANGE_CD_PV_ESS      	= FN_GET_PWR_RANGE_CD('PV_ESS',    B.PV_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            -- ]
		            -- [ 2015.07.06 yngwie 수집 컬럼 추가
		            , PCS_COMM_ERR_RATE   = B.PCS_COMM_ERR_RATE 
		            , SMTR_COMM_ERR_RATE  = B.SMTR_COMM_ERR_RATE 
		            , M2M_COMM_ERR_RATE   = B.M2M_COMM_ERR_RATE       
		            , SMTR_TP_CD          = B.SMTR_TP_CD 
		            , SMTR_PULSE_CNT      = B.SMTR_PULSE_CNT 
		            , SMTR_MODL_CD        = B.SMTR_MODL_CD 
		            , PV_MAX_PWR1         = B.PV_MAX_PWR1 
		            , PV_MAX_PWR2         = B.PV_MAX_PWR2 
		            , INSTL_RGN           = B.INSTL_RGN 
		            , MMTR_SLV_ADDR       = B.MMTR_SLV_ADDR 
		            , BASICMODE_CD        = B.BASICMODE_CD 
		            , PCS_FLAG0       = B.PCS_FLAG0  
		            , PCS_FLAG1       = B.PCS_FLAG1  
		            , PCS_FLAG2       = B.PCS_FLAG2  
		            , PCS_OPMODE_CMD1         = B.PCS_OPMODE_CMD1  
		            , PCS_OPMODE_CMD2         = B.PCS_OPMODE_CMD2  
		            , PV_V1       	= B.PV_V1  
		            , PV_I1       		= B.PV_I1  
		            , PV_PW1      	= B.PV_PW1  
		            , PV_V2       	= B.PV_V2  
		            , PV_I2       		= B.PV_I2  
		            , PV_PW2      	= B.PV_PW2  
		            , INVERTER_V   = B.INVERTER_V  
		            , INVERTER_I    = B.INVERTER_I  
		            , INVERTER_PW = B.INVERTER_PW  
		            , DC_LINK_V     = B.DC_LINK_V  
		            , G_RLY_CNT     = B.G_RLY_CNT  
		            , BAT_RLY_CNT  = B.BAT_RLY_CNT  
		            , GRID_TO_GRID_RLY_CNT	= B.GRID_TO_GRID_RLY_CNT  
		            , BAT_PCHRG_RLY_CNT       = B.BAT_PCHRG_RLY_CNT    
		            , BMS_FLAG0       = B.BMS_FLAG0  
		            , BMS_DIAG0       = B.BMS_DIAG0  
		            , RACK_V      		= B.RACK_V  
		            , RACK_I      		= B.RACK_I  
		            , CELL_MAX_V      	= B.CELL_MAX_V  
		            , CELL_MIN_V      	= B.CELL_MIN_V  
		            , CELL_AVG_V      	= B.CELL_AVG_V  
		            , CELL_MAX_T     	= B.CELL_MAX_T  
		            , CELL_MIN_T      	= B.CELL_MIN_T  
		            , CELL_AVG_T      	= B.CELL_AVG_T  
		            , TRAY_CNT        	= B.TRAY_CNT 
		            , SYS_ST_CD       	= B.SYS_ST_CD 
		            , BT_CHRG_ST_CD  = B.BT_CHRG_ST_CD 
		            , BT_DCHRG_ST_CD= B.BT_DCHRG_ST_CD 
		            , PV_ST_CD        	= B.PV_ST_CD 
		            , GRID_ST_CD      	= B.GRID_ST_CD 
		            , SMTR_ST_CD      	= B.SMTR_ST_CD 
		            , SMTR_TR_CNTER 	= B.SMTR_TR_CNTER 
		            , SMTR_OB_CNTER	= B.SMTR_OB_CNTER 
		            , DN_ENABLE       	= B.DN_ENABLE         
		            , DC_START        	= B.DC_START         
		            , DC_END          	= B.DC_END         
		            , NC_START        	= B.NC_START         
		            , NC_END          	= B.NC_END                             
		            -- ]
		            --[신규모델로 모니터링 추가
		            , GRID_CODE0 		= B.GRID_CODE0 
		            , GRID_CODE1 		= B.GRID_CODE1 
		            , GRID_CODE2 		= B.GRID_CODE2 
		            , PCON_BAT_TARGETPOWER =   B.PCON_BAT_TARGETPOWER 
		            , GRID_CODE3 =             B.GRID_CODE3 
		            , BDC_MODULE_CODE0 =       B.BDC_MODULE_CODE0 
		            , BDC_MODULE_CODE1 =       B.BDC_MODULE_CODE1 
		            , BDC_MODULE_CODE2 =       B.BDC_MODULE_CODE2 
		            , BDC_MODULE_CODE3 =       B.BDC_MODULE_CODE3 
		            , FAULT5_REALTIME_YEAR =   B.FAULT5_REALTIME_YEAR 
		            , FAULT5_REALTIME_MONTH =  B.FAULT5_REALTIME_MONTH 
		            , FAULT5_DAY =             B.FAULT5_DAY 
		            , FAULT5_HOUR =            B.FAULT5_HOUR 
		            , FAULT5_MINUTE =          B.FAULT5_MINUTE 
		            , FAULT5_SECOND =          B.FAULT5_SECOND 
		            , FAULT5_DATAHIGH =        B.FAULT5_DATAHIGH 
		            , FAULT5_DATALOW =         B.FAULT5_DATALOW 
		            , FAULT6_REALTIME_YEAR =   B.FAULT6_REALTIME_YEAR 
		            , FAULT6_REALTIME_MONTH =  B.FAULT6_REALTIME_MONTH 
		            , FAULT6_REALTIME_DAY =    B.FAULT6_REALTIME_DAY 
		            , FAULT6_REALTIME_HOUR =   B.FAULT6_REALTIME_HOUR 
		            , FAULT6_REALTIME_MINUTE = B.FAULT6_REALTIME_MINUTE 
		            , FAULT6_REALTIME_SECOND = B.FAULT6_REALTIME_SECOND 
		            , FAULT6_DATAHIGH =        B.FAULT6_DATAHIGH 
		            , FAULT6_DATALOW =         B.FAULT6_DATALOW 
		            , FAULT7_REALTIME_YEAR =   B.FAULT7_REALTIME_YEAR 
		            , FAULT7_REALTIME_MONTH =  B.FAULT7_REALTIME_MONTH 
		            , FAULT7_REALTIME_DAY =    B.FAULT7_REALTIME_DAY 
		            , FAULT7_REALTIME_HOUR =   B.FAULT7_REALTIME_HOUR 
		            , FAULT7_REALTIME_MINUTE = B.FAULT7_REALTIME_MINUTE 
		            , FAULT7_REALTIME_SECOND = B.FAULT7_REALTIME_SECOND 
		            , FAULT7_DATAHIGH =        B.FAULT7_DATAHIGH 
		            , FAULT7_DATALOW =         B.FAULT7_DATALOW 
		            , FAULT8_REALTIME_YEAR =   B.FAULT8_REALTIME_YEAR 
		            , FAULT8_REALTIME_MONTH =  B.FAULT8_REALTIME_MONTH 
		            , FAULT8_REALTIME_DAY =    B.FAULT8_REALTIME_DAY 
		            , FAULT8_REALTIME_HOUR =   B.FAULT8_REALTIME_HOUR 
		            , FAULT8_REALTIME_MINUTE = B.FAULT8_REALTIME_MINUTE 
		            , FAULT8_REALTIME_SECOND = B.FAULT8_REALTIME_SECOND 
		            , FAULT8_DATAHIGH =        B.FAULT8_DATAHIGH 
		            , FAULT8_DATALOW =         B.FAULT8_DATALOW 
		            , FAULT9_REALTIME_YEAR =   B.FAULT9_REALTIME_YEAR 
		            , FAULT9_REALTIME_MONTH =  B.FAULT9_REALTIME_MONTH 
		            , FAULT9_REALTIME_DAY =    B.FAULT9_REALTIME_DAY 
		            , FAULT9_REALTIME_HOUR =   B.FAULT9_REALTIME_HOUR 
		            , FAULT9_REALTIME_MINUTE = B.FAULT9_REALTIME_MINUTE 
		            , FAULT9_REALTIME_SECOND = B.FAULT9_REALTIME_SECOND 
		            , FAULT9_DATAHIGH =        B.FAULT9_DATAHIGH 
		            , FAULT9_DATALOW =         B.FAULT9_DATALOW 
		            --]        
		        FROM	B
	            WHERE 	A.DEVICE_ID = B.DEVICE_ID             
	            AND 		A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD   
	            
	            RETURNING A.*
	        )                           
            INSERT INTO TB_OPR_ESS_STATE ( 
				DEVICE_ID, DEVICE_TYPE_CD, COLEC_DT, OPER_STUS_CD                            
	            , PV_PW, PV_PW_H, PV_PW_PRICE, PV_PW_CO2, PV_STUS_CD                              
	            , CONS_PW, CONS_PW_H, CONS_PW_PRICE, CONS_PW_CO2                             
	            , BT_PW, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2, BT_DCHRG_PW_H, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2, BT_SOC, BT_SOH, BT_STUS_CD
	            , GRID_PW, GRID_OB_PW_H, GRID_OB_PW_PRICE, GRID_OB_PW_CO2, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2, GRID_STUS_CD                            
	            , PCS_PW, PCS_FD_PW_H, PCS_PCH_PW_H
	            , CREATE_DT                               
	            , EMS_OPMODE, PCS_OPMODE1, PCS_OPMODE2                               
	            , PCS_TGT_PW, FEED_IN_LIMIT, MAX_INVERTER_PW_CD                               
	            -- [ 2015.05.13 yngwie OUTLET_PW 추가
	            , OUTLET_PW, OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2                               
	            , PWR_RANGE_CD_ESS_GRID, PWR_RANGE_CD_GRID_ESS, PWR_RANGE_CD_GRID_LOAD, PWR_RANGE_CD_ESS_LOAD, PWR_RANGE_CD_PV_ESS             
	            -- ]
	            -- [ 2015.07.06 yngwie 수집 컬럼 추가
	            , PCS_COMM_ERR_RATE, SMTR_COMM_ERR_RATE, M2M_COMM_ERR_RATE  
	            , SMTR_TP_CD, SMTR_PULSE_CNT, SMTR_MODL_CD, PV_MAX_PWR1, PV_MAX_PWR2, INSTL_RGN, MMTR_SLV_ADDR, BASICMODE_CD    
	            , PCS_FLAG0, PCS_FLAG1, PCS_FLAG2, PCS_OPMODE_CMD1, PCS_OPMODE_CMD2  
	            , PV_V1, PV_I1, PV_PW1, PV_V2, PV_I2, PV_PW2, INVERTER_V, INVERTER_I, INVERTER_PW, DC_LINK_V  
	            , G_RLY_CNT, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT   
	            , BMS_FLAG0, BMS_DIAG0, RACK_V, RACK_I  
	            , CELL_MAX_V, CELL_MIN_V, CELL_AVG_V, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T  
	            , TRAY_CNT, SYS_ST_CD, BT_CHRG_ST_CD, BT_DCHRG_ST_CD, PV_ST_CD, GRID_ST_CD, SMTR_ST_CD 
	            , SMTR_TR_CNTER, SMTR_OB_CNTER, DN_ENABLE, DC_START, DC_END, NC_START, NC_END                                
	            -- ]            
	         	--[신규모델로 모니터링 추가
				, GRID_CODE0, GRID_CODE1, GRID_CODE2 
				, PCON_BAT_TARGETPOWER, GRID_CODE3 
				, BDC_MODULE_CODE0, BDC_MODULE_CODE1, BDC_MODULE_CODE2, BDC_MODULE_CODE3 
				, FAULT5_REALTIME_YEAR, FAULT5_REALTIME_MONTH, FAULT5_DAY, FAULT5_HOUR, FAULT5_MINUTE, FAULT5_SECOND, FAULT5_DATAHIGH, FAULT5_DATALOW 
				, FAULT6_REALTIME_YEAR, FAULT6_REALTIME_MONTH, FAULT6_REALTIME_DAY, FAULT6_REALTIME_HOUR, FAULT6_REALTIME_MINUTE, FAULT6_REALTIME_SECOND, FAULT6_DATAHIGH, FAULT6_DATALOW 
				, FAULT7_REALTIME_YEAR, FAULT7_REALTIME_MONTH, FAULT7_REALTIME_DAY, FAULT7_REALTIME_HOUR, FAULT7_REALTIME_MINUTE, FAULT7_REALTIME_SECOND, FAULT7_DATAHIGH, FAULT7_DATALOW 
				, FAULT8_REALTIME_YEAR, FAULT8_REALTIME_MONTH, FAULT8_REALTIME_DAY, FAULT8_REALTIME_HOUR, FAULT8_REALTIME_MINUTE, FAULT8_REALTIME_SECOND, FAULT8_DATAHIGH, FAULT8_DATALOW 
				, FAULT9_REALTIME_YEAR, FAULT9_REALTIME_MONTH, FAULT9_REALTIME_DAY, FAULT9_REALTIME_HOUR, FAULT9_REALTIME_MINUTE, FAULT9_REALTIME_SECOND, FAULT9_DATAHIGH, FAULT9_DATALOW             
	            --]  
	        )
			SELECT                              
	               B.DEVICE_ID, B.DEVICE_TYPE_CD, B.COLEC_DT, B.OPER_STUS_CD                            
	              , B.PV_PW, B.PV_PW_H, B.PV_PW_PRICE, B.PV_PW_CO2, B.PV_STUS_CD                              
	              , B.CONS_PW, B.CONS_PW_H, B.CONS_PW_PRICE, B.CONS_PW_CO2                             
	              , B.BT_PW, B.BT_CHRG_PW_H, B.BT_CHRG_PW_PRICE, B.BT_CHRG_PW_CO2, B.BT_DCHRG_PW_H, B.BT_DCHRG_PW_PRICE, B.BT_DCHRG_PW_CO2, B.BT_SOC, B.BT_SOH, B.BT_STUS_CD                              
	              , B.GRID_PW, B.GRID_OB_PW_H, B.GRID_OB_PW_PRICE, B.GRID_OB_PW_CO2, B.GRID_TR_PW_H, B.GRID_TR_PW_PRICE, B.GRID_TR_PW_CO2, B.GRID_STUS_CD
	              , B.PCS_PW, B.PCS_FD_PW_H, B.PCS_PCH_PW_H
	              , COALESCE(B.CREATE_DT, v_baseTm)                               
	              , B.EMS_OPMODE, B.PCS_OPMODE1, B.PCS_OPMODE2, B.PCS_TGT_PW, B.FEED_IN_LIMIT, B.MAX_INVERTER_PW_CD                               
	            -- [ 2015.05.13 yngwie OUTLET_PW 추가
	              , B.OUTLET_PW, B.OUTLET_PW_H, B.OUTLET_PW_PRICE, B.OUTLET_PW_CO2                               
	              , FN_GET_PWR_RANGE_CD('ESS_GRID',  B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('GRID_ESS',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('GRID_LOAD', B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('ESS_LOAD',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('PV_ESS',    B.PV_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD)
	            -- ]
	            -- [ 2015.07.06 yngwie 수집 컬럼 추가
	              , B.PCS_COMM_ERR_RATE, B.SMTR_COMM_ERR_RATE, B.M2M_COMM_ERR_RATE      
	              , B.SMTR_TP_CD, B.SMTR_PULSE_CNT, B.SMTR_MODL_CD, B.PV_MAX_PWR1, B.PV_MAX_PWR2, B.INSTL_RGN, B.MMTR_SLV_ADDR, B.BASICMODE_CD 
	              , B.PCS_FLAG0, B.PCS_FLAG1, B.PCS_FLAG2, B.PCS_OPMODE_CMD1, B.PCS_OPMODE_CMD2  
	              , B.PV_V1, B.PV_I1, B.PV_PW1, B.PV_V2, B.PV_I2, B.PV_PW2  
	              , B.INVERTER_V, B.INVERTER_I, B.INVERTER_PW, B.DC_LINK_V  
	              , B.G_RLY_CNT, B.BAT_RLY_CNT, B.GRID_TO_GRID_RLY_CNT, B.BAT_PCHRG_RLY_CNT   
	              , B.BMS_FLAG0, B.BMS_DIAG0, B.RACK_V, B.RACK_I  
	              , B.CELL_MAX_V, B.CELL_MIN_V, B.CELL_AVG_V, B.CELL_MAX_T, B.CELL_MIN_T, B.CELL_AVG_T  
	              , B.TRAY_CNT, B.SYS_ST_CD, B.BT_CHRG_ST_CD, B.BT_DCHRG_ST_CD, B.PV_ST_CD, B.GRID_ST_CD, B.SMTR_ST_CD 
	              , B.SMTR_TR_CNTER, B.SMTR_OB_CNTER, B.DN_ENABLE 
	              , B.DC_START, B.DC_END, B.NC_START, B.NC_END                        
	           		-- ]
	        		--[신규모델로 모니터링 추가
	              , B.GRID_CODE0, B.GRID_CODE1, B.GRID_CODE2, B.PCON_BAT_TARGETPOWER, B.GRID_CODE3 
	              , B.BDC_MODULE_CODE0, B.BDC_MODULE_CODE1, B.BDC_MODULE_CODE2, B.BDC_MODULE_CODE3 
	              , B.FAULT5_REALTIME_YEAR, B.FAULT5_REALTIME_MONTH, B.FAULT5_DAY, B.FAULT5_HOUR, B.FAULT5_MINUTE, B.FAULT5_SECOND, B.FAULT5_DATAHIGH, B.FAULT5_DATALOW 
	              , B.FAULT6_REALTIME_YEAR, B.FAULT6_REALTIME_MONTH, B.FAULT6_REALTIME_DAY, B.FAULT6_REALTIME_HOUR, B.FAULT6_REALTIME_MINUTE, B.FAULT6_REALTIME_SECOND, B.FAULT6_DATAHIGH, B.FAULT6_DATALOW 
	              , B.FAULT7_REALTIME_YEAR, B.FAULT7_REALTIME_MONTH, B.FAULT7_REALTIME_DAY, B.FAULT7_REALTIME_HOUR, B.FAULT7_REALTIME_MINUTE, B.FAULT7_REALTIME_SECOND, B.FAULT7_DATAHIGH, B.FAULT7_DATALOW 
	              , B.FAULT8_REALTIME_YEAR, B.FAULT8_REALTIME_MONTH, B.FAULT8_REALTIME_DAY, B.FAULT8_REALTIME_HOUR, B.FAULT8_REALTIME_MINUTE, B.FAULT8_REALTIME_SECOND, B.FAULT8_DATAHIGH, B.FAULT8_DATALOW 
	              , B.FAULT9_REALTIME_YEAR, B.FAULT9_REALTIME_MONTH, B.FAULT9_REALTIME_DAY, B.FAULT9_REALTIME_HOUR, B.FAULT9_REALTIME_MINUTE, B.FAULT9_REALTIME_SECOND, B.FAULT9_DATAHIGH, B.FAULT9_DATALOW 
	            	--]           
			FROM B
			WHERE NOT EXISTS (  
				SELECT 	1 
			    FROM 	U 
				WHERE	DEVICE_ID			= B.DEVICE_ID  
					AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
			  )
			 ;                 

           GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--	      	RAISE notice 'i_sp_nm[%] STATE New Records :  [%]건', i_sp_nm, v_work_num;
            
			v_rslt_cd := 'O';
 
            -- TEMP 테이블의 레코드를 이력테이블에 Insert
			INSERT INTO TB_OPR_ESS_STATE_HIST (
				 DEVICE_ID, DEVICE_TYPE_CD, COLEC_DT, OPER_STUS_CD 
                , PV_PW, PV_PW_H, PV_PW_PRICE, PV_PW_CO2, PV_STUS_CD 
                , CONS_PW, CONS_PW_H, CONS_PW_PRICE, CONS_PW_CO2 
                , BT_PW, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2, BT_DCHRG_PW_H, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2, BT_SOC, BT_SOH, BT_STUS_CD 
                , GRID_PW, GRID_OB_PW_H, GRID_OB_PW_PRICE, GRID_OB_PW_CO2, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2, GRID_STUS_CD 
                , PCS_PW, PCS_FD_PW_H, PCS_PCH_PW_H, EMS_OPMODE, PCS_OPMODE1, PCS_OPMODE2, PCS_TGT_PW, FEED_IN_LIMIT
                , CREATE_DT
                , MAX_INVERTER_PW_CD 
                , OUTLET_PW, OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2         -- [ 2015.05.13 yngwie OUTLET_PW 추가   
            -- [ 2015.07.06 yngwie 수집 컬럼 추가
                , PCS_COMM_ERR_RATE, SMTR_COMM_ERR_RATE, M2M_COMM_ERR_RATE, SMTR_TP_CD, SMTR_PULSE_CNT, SMTR_MODL_CD, PV_MAX_PWR1, PV_MAX_PWR2    
                , INSTL_RGN, MMTR_SLV_ADDR, BASICMODE_CD
	            , PCS_FLAG0, PCS_FLAG1, PCS_FLAG2, PCS_OPMODE_CMD1, PCS_OPMODE_CMD2  
	            , PV_V1, PV_I1, PV_PW1, PV_V2, PV_I2, PV_PW2  
	            , INVERTER_V, INVERTER_I, INVERTER_PW, DC_LINK_V  
	            , G_RLY_CNT, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT    
	            , BMS_FLAG0, BMS_DIAG0, RACK_V, RACK_I  
	            , CELL_MAX_V, CELL_MIN_V, CELL_AVG_V, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T  
	            , TRAY_CNT, SYS_ST_CD 
	            , BT_CHRG_ST_CD, BT_DCHRG_ST_CD, PV_ST_CD, GRID_ST_CD 
	            , SMTR_ST_CD, SMTR_TR_CNTER, SMTR_OB_CNTER             
            -- ]         
				--[신규모델로 모니터링 추가
				, GRID_CODE0, GRID_CODE1, GRID_CODE2 
				, PCON_BAT_TARGETPOWER, GRID_CODE3 
				, BDC_MODULE_CODE0, BDC_MODULE_CODE1, BDC_MODULE_CODE2, BDC_MODULE_CODE3 
				, FAULT5_REALTIME_YEAR, FAULT5_REALTIME_MONTH, FAULT5_DAY, FAULT5_HOUR, FAULT5_MINUTE, FAULT5_SECOND, FAULT5_DATAHIGH, FAULT5_DATALOW 
				, FAULT6_REALTIME_YEAR, FAULT6_REALTIME_MONTH, FAULT6_REALTIME_DAY, FAULT6_REALTIME_HOUR, FAULT6_REALTIME_MINUTE, FAULT6_REALTIME_SECOND, FAULT6_DATAHIGH, FAULT6_DATALOW 
				, FAULT7_REALTIME_YEAR, FAULT7_REALTIME_MONTH, FAULT7_REALTIME_DAY, FAULT7_REALTIME_HOUR, FAULT7_REALTIME_MINUTE, FAULT7_REALTIME_SECOND, FAULT7_DATAHIGH, FAULT7_DATALOW 
				, FAULT8_REALTIME_YEAR, FAULT8_REALTIME_MONTH, FAULT8_REALTIME_DAY, FAULT8_REALTIME_HOUR, FAULT8_REALTIME_MINUTE, FAULT8_REALTIME_SECOND, FAULT8_DATAHIGH, FAULT8_DATALOW 
				, FAULT9_REALTIME_YEAR, FAULT9_REALTIME_MONTH, FAULT9_REALTIME_DAY, FAULT9_REALTIME_HOUR, FAULT9_REALTIME_MINUTE, FAULT9_REALTIME_SECOND, FAULT9_DATAHIGH, FAULT9_DATALOW             
				  --]
			)
            SELECT DEVICE_ID, DEVICE_TYPE_CD, COLEC_DT, OPER_STUS_CD 
	                , PV_PW, PV_PW_H, PV_PW_PRICE, PV_PW_CO2, PV_STUS_CD 
	                , CONS_PW, CONS_PW_H, CONS_PW_PRICE, CONS_PW_CO2 
	                , BT_PW, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2, BT_DCHRG_PW_H, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2, BT_SOC, BT_SOH, BT_STUS_CD 
	                , GRID_PW, GRID_OB_PW_H, GRID_OB_PW_PRICE, GRID_OB_PW_CO2, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2, GRID_STUS_CD 
	                , PCS_PW, PCS_FD_PW_H, PCS_PCH_PW_H, EMS_OPMODE, PCS_OPMODE1, PCS_OPMODE2, PCS_TGT_PW, FEED_IN_LIMIT
	                , CREATE_DT
	                , MAX_INVERTER_PW_CD 
	                , OUTLET_PW, OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2
	                , PCS_COMM_ERR_RATE, SMTR_COMM_ERR_RATE, M2M_COMM_ERR_RATE, SMTR_TP_CD, SMTR_PULSE_CNT, SMTR_MODL_CD, PV_MAX_PWR1, PV_MAX_PWR2    
	                , INSTL_RGN, MMTR_SLV_ADDR, BASICMODE_CD
		            , PCS_FLAG0, PCS_FLAG1, PCS_FLAG2, PCS_OPMODE_CMD1, PCS_OPMODE_CMD2  
		            , PV_V1, PV_I1, PV_PW1, PV_V2, PV_I2, PV_PW2  
		            , INVERTER_V, INVERTER_I, INVERTER_PW, DC_LINK_V  
		            , G_RLY_CNT, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT    
		            , BMS_FLAG0, BMS_DIAG0, RACK_V, RACK_I  
		            , CELL_MAX_V, CELL_MIN_V, CELL_AVG_V, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T  
		            , TRAY_CNT, SYS_ST_CD 
		            , BT_CHRG_ST_CD, BT_DCHRG_ST_CD, PV_ST_CD, GRID_ST_CD 
		            , SMTR_ST_CD, SMTR_TR_CNTER, SMTR_OB_CNTER                      
		            , GRID_CODE0, GRID_CODE1, GRID_CODE2 
		            , PCON_BAT_TARGETPOWER, GRID_CODE3 
		            , BDC_MODULE_CODE0, BDC_MODULE_CODE1, BDC_MODULE_CODE2, BDC_MODULE_CODE3 
		            , FAULT5_REALTIME_YEAR, FAULT5_REALTIME_MONTH, FAULT5_DAY, FAULT5_HOUR, FAULT5_MINUTE, FAULT5_SECOND, FAULT5_DATAHIGH, FAULT5_DATALOW 
		            , FAULT6_REALTIME_YEAR, FAULT6_REALTIME_MONTH, FAULT6_REALTIME_DAY, FAULT6_REALTIME_HOUR, FAULT6_REALTIME_MINUTE, FAULT6_REALTIME_SECOND, FAULT6_DATAHIGH, FAULT6_DATALOW 
		            , FAULT7_REALTIME_YEAR, FAULT7_REALTIME_MONTH, FAULT7_REALTIME_DAY, FAULT7_REALTIME_HOUR, FAULT7_REALTIME_MINUTE, FAULT7_REALTIME_SECOND, FAULT7_DATAHIGH, FAULT7_DATALOW 
		            , FAULT8_REALTIME_YEAR, FAULT8_REALTIME_MONTH, FAULT8_REALTIME_DAY, FAULT8_REALTIME_HOUR, FAULT8_REALTIME_MINUTE, FAULT8_REALTIME_SECOND, FAULT8_DATAHIGH, FAULT8_DATALOW 
		            , FAULT9_REALTIME_YEAR, FAULT9_REALTIME_MONTH, FAULT9_REALTIME_DAY, FAULT9_REALTIME_HOUR, FAULT9_REALTIME_MINUTE, FAULT9_REALTIME_SECOND, FAULT9_DATAHIGH, FAULT9_DATALOW             
            FROM 	TB_OPR_ESS_STATE_TMP;  
           
           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--	      	RAISE notice 'i_sp_nm[%] Insert TB_OPR_ESS_STATE_HIST Table :  [%]건', i_sp_nm, v_work_num;
	      
--	      	v_err_msg ='Insert TB_OPR_ESS_STATE_HIST Table : '||v_work_num::TEXT;

	      
	      /*--------------------------------------------------------------- 예측관련 업무 배제결정 ( QCells ) -----------------------------------------------

--                2015.09.02 yngwie 장비별 CONFIG 데이터 처리.
--                TB_OPR_ESS_CONFIG 테이블에 정보가 없는 장비는
--                TB_RAW_EMS_INFO 에 올라온 정보로 추가하고 _HIST 에도 INSERT 한다.

--			 2015.11.12 stjoo 장비별 CONFIG 데이터 처리 삭제
--			 CONFIG 정보가 없는 기존에 운영하던 장비들을 위해 
--			 임시로 적용하던 로직이였습니다.			
            
            ------------------------
            -- 3.1 예측정보 데이터 처리
            ------------------------
			v_rslt_cd := 'P';

			TRUNCATE TABLE TB_OPR_ESS_PREDICT_TMP;

            -- 대상 레코드들의 CO2와 금액을 계산한 결과를 예측정보 TMP 테이블에 Insert
            INSERT INTO TB_OPR_ESS_PREDICT_TMP 
            SELECT 
                A.COLEC_DT  
                , A.DEVICE_ID  
                , A.DEVICE_TYPE_CD  
                , A.PREDICT_DD  
                , A.PREDICT_HH  
                , A.CONS_VAL AS CONS_PREDICT_PW_H  
                , TRUNC(ABS(A.CONS_VAL) * D.CARB_EMI_QUITY * .001, 2) AS CONS_PREDICT_CO2  
                , TRUNC(ABS(A.CONS_VAL) * E.UNIT_PRICE * .001, 2) AS CONS_PREDICT_PRICE  
                , A.PV_VAL AS PV_PREDICT_PW_H  
                , TRUNC(A.PV_VAL * D.CARB_EMI_QUITY * .001, 2) AS PV_PREDICT_CO2  
                , TRUNC(A.PV_VAL * ET.UNIT_PRICE * .001, 2) AS PV_PREDICT_PRICE  
                , v_baseTm AS CREATE_DT  
            FROM  
                ( 
                    SELECT  
                        A.COLEC_DT  
                        , A.DEVICE_ID  
                        , A.DEVICE_TYPE_CD  
                        , A.CNTRY_CD 
                        , A.ELPW_PROD_CD 
                        , FN_TS_STR_TO_STR(A.UTC_COLEC_DT || H.HH, A.UTC_OFFSET, 'YYYYMMDDHH24', 0, 'YYYYMMDD') AS PREDICT_DD  
                        , FN_TS_STR_TO_STR(A.UTC_COLEC_DT || H.HH, A.UTC_OFFSET, 'YYYYMMDDHH24', 0, 'HH24') AS PREDICT_HH 
                        , MAX(CASE  
                            WHEN H.HH = '00' THEN A.COL193  
                            WHEN H.HH = '01' THEN A.COL194  
                            WHEN H.HH = '02' THEN A.COL195  
                            WHEN H.HH = '03' THEN A.COL196  
                            WHEN H.HH = '04' THEN A.COL197  
                            WHEN H.HH = '05' THEN A.COL198  
                            WHEN H.HH = '06' THEN A.COL199  
                            WHEN H.HH = '07' THEN A.COL200  
                            WHEN H.HH = '08' THEN A.COL201  
                            WHEN H.HH = '09' THEN A.COL202  
                            WHEN H.HH = '10' THEN A.COL203  
                            WHEN H.HH = '11' THEN A.COL204  
                            WHEN H.HH = '12' THEN A.COL205  
                            WHEN H.HH = '13' THEN A.COL206  
                            WHEN H.HH = '14' THEN A.COL207  
                            WHEN H.HH = '15' THEN A.COL208  
                            WHEN H.HH = '16' THEN A.COL209  
                            WHEN H.HH = '17' THEN A.COL210  
                            WHEN H.HH = '18' THEN A.COL211  
                            WHEN H.HH = '19' THEN A.COL212  
                            WHEN H.HH = '20' THEN A.COL213  
                            WHEN H.HH = '21' THEN A.COL214  
                            WHEN H.HH = '22' THEN A.COL215  
                            WHEN H.HH = '23' THEN A.COL216  
                            ELSE NULL  
                        END)::NUMERIC PV_VAL  
                        , MAX(CASE  
                            WHEN H.HH = '00' THEN A.COL217
                            WHEN H.HH = '01' THEN A.COL218
                            WHEN H.HH = '02' THEN A.COL219
                            WHEN H.HH = '03' THEN A.COL220
                            WHEN H.HH = '04' THEN A.COL221
                            WHEN H.HH = '05' THEN A.COL222
                            WHEN H.HH = '06' THEN A.COL223
                            WHEN H.HH = '07' THEN A.COL224
                            WHEN H.HH = '08' THEN A.COL225
                            WHEN H.HH = '09' THEN A.COL226
                            WHEN H.HH = '10' THEN A.COL227
                            WHEN H.HH = '11' THEN A.COL228
                            WHEN H.HH = '12' THEN A.COL229
                            WHEN H.HH = '13' THEN A.COL230
                            WHEN H.HH = '14' THEN A.COL231
                            WHEN H.HH = '15' THEN A.COL232
                            WHEN H.HH = '16' THEN A.COL233
                            WHEN H.HH = '17' THEN A.COL234
                            WHEN H.HH = '18' THEN A.COL235
                            WHEN H.HH = '19' THEN A.COL236
                            WHEN H.HH = '20' THEN A.COL237
                            WHEN H.HH = '21' THEN A.COL238
                            WHEN H.HH = '22' THEN A.COL239
                            WHEN H.HH = '23' THEN A.COL240
                            ELSE NULL  
                        END)::NUMERIC CONS_VAL 
                    FROM  
                        (  
                            SELECT  
                                FN_TS_STR_TO_STR(A.COLEC_DT, 0, 'YYYYMMDDHH24MISS', CT.UTC_OFFSET, 'YYYYMMDD') AS UTC_COLEC_DT 
                                , CT.UTC_OFFSET  
                                , B.CITY_CD  
                                , B.CNTRY_CD  
                                , B.ELPW_PROD_CD  
                                , B.DEVICE_TYPE_CD  
                                , A.*  
                            FROM   
                                (  
                                    SELECT   
                                        A.COLEC_DT 
                                        , A.DEVICE_ID 
                                        , FN_GET_CONV_NA(A.COL193) AS COL193
                                        , FN_GET_CONV_NA(A.COL194) AS COL194
                                        , FN_GET_CONV_NA(A.COL195) AS COL195
                                        , FN_GET_CONV_NA(A.COL196) AS COL196
                                        , FN_GET_CONV_NA(A.COL197) AS COL197
                                        , FN_GET_CONV_NA(A.COL198) AS COL198
                                        , FN_GET_CONV_NA(A.COL199) AS COL199
                                        , FN_GET_CONV_NA(A.COL200) AS COL200
                                        , FN_GET_CONV_NA(A.COL201) AS COL201
                                        , FN_GET_CONV_NA(A.COL202) AS COL202
                                        , FN_GET_CONV_NA(A.COL203) AS COL203
                                        , FN_GET_CONV_NA(A.COL204) AS COL204
                                        , FN_GET_CONV_NA(A.COL205) AS COL205
                                        , FN_GET_CONV_NA(A.COL206) AS COL206
                                        , FN_GET_CONV_NA(A.COL207) AS COL207
                                        , FN_GET_CONV_NA(A.COL208) AS COL208
                                        , FN_GET_CONV_NA(A.COL209) AS COL209
                                        , FN_GET_CONV_NA(A.COL210) AS COL210
                                        , FN_GET_CONV_NA(A.COL211) AS COL211
                                        , FN_GET_CONV_NA(A.COL212) AS COL212
                                        , FN_GET_CONV_NA(A.COL213) AS COL213
                                        , FN_GET_CONV_NA(A.COL214) AS COL214
                                        , FN_GET_CONV_NA(A.COL215) AS COL215
                                        , FN_GET_CONV_NA(A.COL216) AS COL216
                                        , FN_GET_CONV_NA(A.COL217) AS COL217 
                                        , FN_GET_CONV_NA(A.COL218) AS COL218 
                                        , FN_GET_CONV_NA(A.COL219) AS COL219 
                                        , FN_GET_CONV_NA(A.COL220) AS COL220 
                                        , FN_GET_CONV_NA(A.COL221) AS COL221 
                                        , FN_GET_CONV_NA(A.COL222) AS COL222 
                                        , FN_GET_CONV_NA(A.COL223) AS COL223 
                                        , FN_GET_CONV_NA(A.COL224) AS COL224 
                                        , FN_GET_CONV_NA(A.COL225) AS COL225 
                                        , FN_GET_CONV_NA(A.COL226) AS COL226 
                                        , FN_GET_CONV_NA(A.COL227) AS COL227 
                                        , FN_GET_CONV_NA(A.COL228) AS COL228 
                                        , FN_GET_CONV_NA(A.COL229) AS COL229 
                                        , FN_GET_CONV_NA(A.COL230) AS COL230 
                                        , FN_GET_CONV_NA(A.COL231) AS COL231 
                                        , FN_GET_CONV_NA(A.COL232) AS COL232 
                                        , FN_GET_CONV_NA(A.COL233) AS COL233 
                                        , FN_GET_CONV_NA(A.COL234) AS COL234 
                                        , FN_GET_CONV_NA(A.COL235) AS COL235 
                                        , FN_GET_CONV_NA(A.COL236) AS COL236 
                                        , FN_GET_CONV_NA(A.COL237) AS COL237 
                                        , FN_GET_CONV_NA(A.COL238) AS COL238 
                                        , FN_GET_CONV_NA(A.COL239) AS COL239 
                                        , FN_GET_CONV_NA(A.COL240) AS COL240    
                                    FROM TB_RAW_EMS_INFO_TMP A   
                                ) A  
                                , TB_BAS_DEVICE B  
                                , TB_BAS_CITY CT  
                            WHERE 	A.DEVICE_ID = B.DEVICE_ID   
                                AND 	B.CITY_CD = CT.CITY_CD   
                        ) A  
                        , (SELECT TO_CHAR(TM, '09') HH FROM GENERATE_SERIES(0, 23) TM) H 
                    GROUP BY  
                        A.COLEC_DT 
                        , A.DEVICE_ID 
                        , A.DEVICE_TYPE_CD 
                        , A.CNTRY_CD 
                        , A.ELPW_PROD_CD 
                        , A.UTC_COLEC_DT 
                        , A.UTC_OFFSET 
                        , H.HH 
                ) A 
                , TB_PLF_CARB_EMI_QUITY D  
                , TB_BAS_ELPW_UNIT_PRICE E  
                , TB_BAS_ELPW_UNIT_PRICE ET 
            WHERE  
                A.CNTRY_CD = D.CNTRY_CD  
                AND D.USE_FLAG = 'Y'  
                AND A.ELPW_PROD_CD = E.ELPW_PROD_CD 
                AND E.IO_FLAG = 'O' 
                AND E.USE_FLAG = 'Y'  
            -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가
                AND E.WKDAY_FLAG = FN_GET_WKDAY_FLAG(PREDICT_DD) 
            -- 2015.07.31 요금제 년월 변경
            --  AND SUBSTR(PREDICT_DD, 1, 6) = E.UNIT_YM  
                AND E.UNIT_YM = '000000' 
                AND PREDICT_HH = E.UNIT_HH  
                AND A.ELPW_PROD_CD = ET.ELPW_PROD_CD 
                AND ET.IO_FLAG = 'T' 
                AND ET.USE_FLAG = 'Y'  
            -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가, T 는  1 만 있음
            -- 2015.08.06 modified by yngwie T 도 주말 0 추가
            --  AND ET.WKDAY_FLAG = '1'  
                AND ET.WKDAY_FLAG = FN_GET_WKDAY_FLAG(PREDICT_DD) 

            -- 2015.07.31 요금제 년월 변경
            --  AND SUBSTR(PREDICT_DD, 1, 6) = ET.UNIT_YM 
                AND ET.UNIT_YM ='000000'
                AND PREDICT_HH = ET.UNIT_HH 
            ;
            
           GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	      	RAISE notice 'i_sp_nm[%] Insert PREDICT Tmp  Table :  [%]건', i_sp_nm, v_work_num;
			
   			v_rslt_cd := 'Q';

            -- 예측정보 Tmp 테이블의 레코드를 예측정보이력 테이블에 Insert 
            DELETE FROM TB_OPR_ESS_PREDICT_HIST
            WHERE (COLEC_DT, DEVICE_ID, DEVICE_TYPE_CD) IN ( 
                SELECT COLEC_DT, DEVICE_ID, DEVICE_TYPE_CD FROM TB_OPR_ESS_PREDICT_TMP 
            ) ;
           
           GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;
	      	RAISE notice 'i_sp_nm[%] Delete TB_OPR_ESS_PREDICT_HIST Old Records : [%]건', i_sp_nm, v_work_num;
            
   			v_rslt_cd := 'R';

            INSERT INTO TB_OPR_ESS_PREDICT_HIST ( 
                COLEC_DT 
                , DEVICE_ID 
                , DEVICE_TYPE_CD 
                , PREDICT_DD 
                , PREDICT_HH 
                , CONS_PREDICT_PW_H 
                , CONS_PREDICT_CO2   
                , CONS_PREDICT_PRICE 
                , PV_PREDICT_PW_H    
                , PV_PREDICT_CO2     
                , PV_PREDICT_PRICE   
            ) 
            SELECT 
                COLEC_DT 
                , DEVICE_ID 
                , DEVICE_TYPE_CD 
                , PREDICT_DD 
                , PREDICT_HH 
                , CONS_PREDICT_PW_H 
                , CONS_PREDICT_CO2   
                , CONS_PREDICT_PRICE 
                , PV_PREDICT_PW_H    
                , PV_PREDICT_CO2     
                , PV_PREDICT_PRICE 
            FROM 
                TB_OPR_ESS_PREDICT_TMP 
            ;
           
           GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;
	      	RAISE NOTICE 'i_sp_nm[%] TB_OPR_ESS_PREDICT_HIST New Records : [%]건', i_sp_nm, v_work_num;
            
            -- 2015.06.23 yngwie
            -- 일자별로 가장 최근 데이터만 유지하도록 변경, 관련 일감 #129 
            SELECT 	SUBSTR(MAX(COLEC_DT), 1, 8) 
            INTO 		v_selectDt
            FROM 	TB_OPR_ESS_PREDICT_TMP;
            
            --2016.02.17 하루 이상 데이터가 적재되지 않은 경우 v_selectDt가 NULL이 되면서 아래 DELETE문 수행속도에 지장이 발생하여 오늘날짜로 입력 이지훈
            IF v_selectDt IS NULL THEN
                SELECT TO_CHAR(NOW(), 'YYYYMMDD') INTO v_selectDt;
            END IF; 
            
	      	RAISE notice 'i_sp_nm[%] TB_OPR_ESS_PREDICT_TMP 최근일 : [%]', i_sp_nm, v_selectDt;

  			v_rslt_cd := 'S';

            DELETE FROM TB_OPR_ESS_PREDICT_HIST 
--            WHERE 	COLEC_DT LIKE (v_selectDt || '%')
            WHERE 	(COLEC_DT >= v_selectDt||'000000' AND COLEC_DT <= v_selectDt||'235959')
            AND 		(COLEC_DT, DEVICE_ID) NOT IN ( 
				            SELECT 	MAX(COLEC_DT) 
				                		, DEVICE_ID 
				            FROM		TB_OPR_ESS_PREDICT_HIST
				            WHERE 	(COLEC_DT >= v_selectDt||'000000' AND COLEC_DT <= v_selectDt||'235959')
--				            WHERE 	SUBSTR(COLEC_DT, 1, 8) = v_selectDt
				            GROUP BY DEVICE_ID 
				            ) 
            ;

            GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	      	RAISE notice 'i_sp_nm[%] Delete TB_OPR_ESS_PREDICT_HIST : [%], v_selectDt[%]', i_sp_nm, v_work_num, v_selectDt;
        

            ------------------------
            -- 3.2 예측정보 ADVICE 데이터 처리
            ------------------------
            -- 우선 장애 대응을 위해 기능 Disable 이지훈
           SELECT COUNT(*)  
           INTO v_ExeCnt1
           FROM TB_RAW_EMS_INFO_TMP;

	      RAISE notice 'i_sp_nm[%] SELECT COUNT(*) FROM TB_RAW_EMS_INFO_TMP :  [%]', i_sp_nm, v_ExeCnt1;

	      
           	SELECT 	COUNT(COLEC_DT) 
            INTO 		v_ExeCnt1
           	FROM 	TB_RAW_EMS_INFO_TMP 
           	WHERE 	COLEC_DT IS NULL;
           
	      	RAISE notice 'i_sp_nm[%] SELECT COUNT(COLEC_DT) FROM TB_RAW_EMS_INFO_TMP WHERE COLEC_DT IS NULL : [%]', i_sp_nm, v_ExeCnt1;
            
  			v_rslt_cd := 'T';
  
            WITH B AS ( 
                SELECT 
                    FN_TS_STR_TO_STR(A.COLEC_DT, 0, 'YYYYMMDDHH24MISS', CT.UTC_OFFSET, 'YYYYMMDD') AS COLEC_DD 
                    , A.DEVICE_ID 
                    , B.DEVICE_TYPE_CD 
                    , CASE WHEN COL254 IS NULL THEN NULL ELSE 
                      FN_GET_FMT_NUM( COL254 ) 
                      || '|' || FN_GET_FMT_NUM( COL253 )
                      || '|' || FN_GET_FMT_NUM( COL257 )
                      || '|' || FN_GET_FMT_NUM( COL258 )
                      || '|' || FN_GET_FMT_NUM( COL251 )
                      || '|' || FN_GET_FMT_NUM( COL252 )
                      || '|' || COL259 * 100
                      || '|' || COL260 * 100 END AS DFLT_REF 
                    , 1 AS CASE_CNT 
                    , CASE WHEN (COL251 > B.CAPACITY::NUMERIC / 12) AND COL252 > B.CAPACITY::NUMERIC / 12 THEN 'Y' ELSE 'N' END CASE_YN 
                    , FN_GET_FMT_NUM( B.CAPACITY::NUMERIC / 12 )
                      || '|' || COL261 * 10
                      || '|' || E.REF_1
                      || '|' || FN_GET_FMT_NUM( COL262 ) AS CASE_REF 
                FROM 
                    ( 
                        SELECT 
                            ROW_NUMBER() OVER (PARTITION BY DEVICE_ID ORDER BY COLEC_DT DESC ) SEQ 
                            , COLEC_DT 
                            , DEVICE_ID 
                            , CASE WHEN COL251 = 'NA' THEN NULL ELSE (COL251)::NUMERIC END AS COL251 
                            , CASE WHEN COL252 = 'NA' THEN NULL ELSE (COL252)::NUMERIC END AS COL252 
                            , CASE WHEN COL253 = 'NA' THEN NULL ELSE (COL253)::NUMERIC END AS COL253 
                            , CASE WHEN COL254 = 'NA' THEN NULL ELSE (COL254)::NUMERIC END AS COL254 
                            , CASE WHEN COL255 = 'NA' THEN NULL ELSE (COL255)::NUMERIC END AS COL255 
                            , CASE WHEN COL256 = 'NA' THEN NULL ELSE (COL256)::NUMERIC END AS COL256 
                            , CASE WHEN COL257 = 'NA' THEN NULL ELSE (COL257)::NUMERIC END AS COL257 
                            , CASE WHEN COL258 = 'NA' THEN NULL ELSE (COL258)::NUMERIC END AS COL258 
                            , CASE WHEN COL259 = 'NA' THEN NULL ELSE (COL259)::NUMERIC END AS COL259 
                            , CASE WHEN COL260 = 'NA' THEN NULL ELSE (COL260)::NUMERIC END AS COL260 
                            , CASE WHEN COL261 = 'NA' THEN NULL ELSE (COL261)::NUMERIC END AS COL261 
                            , CASE WHEN COL262 = 'NA' THEN NULL ELSE (COL262)::NUMERIC END AS COL262 
                        FROM    TB_RAW_EMS_INFO_TMP 
                     ) A 
                     , TB_BAS_DEVICE B   
                     , TB_BAS_CITY CT 
                     , TB_BAS_CNTRY D 
                     , (SELECT CODE, REF_1 FROM TB_PLF_CODE_INFO WHERE GRP_CD = 'MONE_UNIT_CD') E 
                WHERE  
                    A.SEQ = 1 
                    AND A.DEVICE_ID = B.DEVICE_ID    
                    AND B.CITY_CD = CT.CITY_CD   
                    AND B.CNTRY_CD = D.CNTRY_CD 
                    AND D.MONE_UNIT_CD = E.CODE 
            ), U AS (
	            UPDATE  TB_STT_PREDICT_ADVICE_DD A  
	            SET 
	                 DFLT_REF = B.DFLT_REF 
	                , CASE_CNT = B.CASE_CNT 
	                , CASE_YN  = B.CASE_YN 
	                , CASE_REF = B.CASE_REF 
	                , CREATE_DT = SYS_EXTRACT_UTC(NOW()) 
	            FROM B
	            WHERE
	                A.COLEC_DD = B.COLEC_DD 
	                AND A.DEVICE_ID = B.DEVICE_ID 
	                AND A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD 
	                
	           RETURNING A.*
            ) 
            INSERT INTO TB_STT_PREDICT_ADVICE_DD ( 
                  COLEC_DD 
                , DEVICE_ID 
                , DEVICE_TYPE_CD 
                , DFLT_REF 
                , CASE_CNT 
                , CASE_YN 
                , CASE_REF 
            ) 
            SELECT
                  B.COLEC_DD 
                , B.DEVICE_ID 
                , B.DEVICE_TYPE_CD 
                , B.DFLT_REF 
                , B.CASE_CNT 
                , B.CASE_YN 
                , B.CASE_REF 
			FROM B
			WHERE NOT EXISTS (  
				SELECT 	1 
			    FROM 	U 
				WHERE	COLEC_DD 			= B.COLEC_DD  
					AND 	DEVICE_ID			= B.DEVICE_ID  
					AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
			  )
			 ;

            
            GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	      	RAISE notice 'i_sp_nm[%] Merge TB_STT_PREDICT_ADVICE_DD Records : [%]', i_sp_nm, v_work_num;
            */
           
--		ELSE 
--    		RAISE NOTICE 'i_sp_nm[%] No New Record', i_sp_nm;

        END IF;
       
--    	RAISE NOTICE 'i_sp_nm[%] SP End !!! ', i_sp_nm;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
        
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
        
--       	RAISE NOTICE 'i_sp_nm[%] SP EXCEPTION ERR[%]', i_sp_nm, v_err_msg;

    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
	return v_rslt_cd;

END;


$_$;


ALTER FUNCTION wems.sp_aggr_state(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1104 (class 1255 OID 1895884)
-- Name: sp_aggr_state_restore(text, text, text); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_aggr_state_restore(p_device_id text, p_from_datetime text, p_to_datetime text)
    LANGUAGE plpgsql
    AS $$
 /******************************************************************************
   NAME:       SP_AGGR_STATE_RESTORE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-16   yngwie       Created this function.
   1.1        2016-01-05   jkm1020     ESM (EMS Sleep Mode) 추가
   2.0        2019-10-08     이재형      postgresql 변환    
   
   NOTES:   1분주기 수집데이터를 가지고 상태/예측/장애 데이터를 생성한다.

    Input :

    Output :

 ******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_AGGR_STATE_RESTORE';
    v_log_seq 		NUMERIC := 0;		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';							-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    p_dt    			VARCHAR	:= ''; 
    v_curTm     		VARCHAR(4);
    v_handleFlag 	VARCHAR;
    v_selectDt   		VARCHAR; --TIMESTAMP WITH TIME ZONE;
    v_row           	record; 
	
   	v_sql				VARCHAR;
    v_tbl				VARCHAR;
   	v_ExeCnt1 		INTEGER 		:= 0;
   	v_ExeCnt2 		INTEGER 		:= 0;
   	v_ExeCnt3 		INTEGER 		:= 0;
 
    arrayTbl 			VARCHAR ARRAY := ARRAY['TB_RAW_EMS_INFO', 'TB_RAW_PCS_INFO', 'TB_RAW_BMS_INFO'];
    arrayRawTbl 	VARCHAR ARRAY := ARRAY['TB_RAW_EMS_INFO', 'TB_RAW_PCS_INFO', 'TB_RAW_BMS_INFO'];
   
BEGIN

	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt 
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
  
		v_rslt_cd := 'A';
	
		/*
        2015.04.16 yngwie
        DONE_FLAG 에 임시로 마킹을 할 Y, N 을 제외한 문자를 임의 생성한다.
        
        2015.04.17 개발/운영DB 에서 DBMS_RANDOM 패키지 사용못함
        2015.07.03 raw 데이터 처리여부는 handleFlag 를 사용하는 것으로 변경
        */

        v_handleFlag := TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'YYYYMMDDHH24MISS');   
        v_selectDt := 'SYS_EXTRACT_UTC(NOW()) - INTERVAL ''5 MINUTE'' ';
       
        INSERT INTO TB_RAW_EMS_INFO_TMP 
        -- modified by stjoo 20151229 : add hint statement for performance improvement
        SELECT /*+ index(a PK_RAW_EMS_INFO_1) */ 
            DEVICE_ID  
            , COLEC_DT  
            , DONE_FLAG  
            , COL0, COL1, COL2, COL3, COL4, COL5, COL6, COL7, COL8, COL9, COL10  
            , COL11, COL12, COL13, COL14, COL15, COL16, COL17, COL18, COL19, COL20  
            , COL21, COL22, COL23, COL24, COL25, COL26, COL27, COL28, COL29, COL30 
            , COL31, COL32, COL33, COL34, COL35, COL36, COL37, COL38, COL39, COL40 
            , COL41, COL42, COL43, COL44, COL45, COL46, COL47, COL48, COL49, COL50
            , COL51, COL52, COL53, COL54, COL55, COL56, COL57, COL58, COL59, COL60 
            , COL61, COL62, COL63, COL64, COL65, COL66, COL67, COL68, COL69, COL70 
            , COL71, COL72, COL73, COL74, COL75, COL76, COL77, COL78, COL79, COL80 
            , COL81, COL82, COL83, COL84, COL85, COL86, COL87 
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR( COL88, 3))) ), 'FM00000000') AS COL88  
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR( COL89, 3))) ), 'FM00000000') AS COL89  
            , COL90, COL91, COL92, COL93, COL94, COL95, COL96, COL97, COL98, COL99, COL100
			, COL101, COL102, COL103, COL104, COL105, COL106, COL107, COL108, COL109, COL110 
			, COL111, COL112, COL113, COL114, COL115, COL116, COL117, COL118, COL119, COL120 
			, COL121, COL122, COL123, COL124, COL125, COL126, COL127, COL128, COL129, COL130
			, COL131, COL132, COL133, COL134, COL135, COL136, COL137, COL138, COL139, COL140
			, COL141, COL142, COL143, COL144, COL145, COL146, COL147, COL148, COL149, COL150
			, COL151, COL152, COL153, COL154, COL155, COL156, COL157, COL158, COL159, COL160
			, COL161, COL162, COL163, COL164, COL165, COL166, COL167, COL168, COL169, COL170
			, COL171, COL172, COL173, COL174, COL175, COL176, COL177, COL178, COL179, COL180
			, COL181, COL182, COL183, COL184, COL185, COL186, COL187, COL188, COL189, COL190
			, COL191, COL192, COL193, COL194, COL195, COL196, COL197, COL198, COL199, COL200
			, COL201, COL202, COL203, COL204, COL205, COL206, COL207, COL208, COL209, COL210 
			, COL211, COL212, COL213, COL214, COL215, COL216, COL217, COL218, COL219, COL220 
			, COL221, COL222, COL223, COL224, COL225, COL226, COL227, COL228, COL229, COL230 
			, COL231, COL232, COL233, COL234, COL235, COL236, COL237, COL238, COL239, COL240 
			, COL241, COL242, COL243, COL244, COL245, COL246, COL247, COL248, COL249, COL250 
			, COL251, COL252, COL253, COL254, COL255, COL256, COL257, COL258, COL259, COL260 
			, COL261, COL262, COL263, COL264, COL265, COL266, COL267, COL268, COL269, COL270
			, COL271, COL272, COL273, COL274, COL275, COL276, COL277, COL278, COL279, COL280
			, COL281, COL282, COL283, COL284, COL285, COL286, COL287, COL288, COL289, COL290
			, COL291, COL292, COL293, COL294, COL295, COL296, COL297, COL298, COL299, COL300
			, COL301, COL302, COL303, COL304, COL305, COL306, COL307, COL308, COL309, COL310
			, COL311, COL312, COL313, COL314, COL315, COL316, COL317, COL318, COL319, COL320
			, COL321, COL322, COL323, COL324, COL325, COL326, COL327, COL328, COL329, COL330
			, COL331, COL332, COL333, COL334, COL335, COL336, COL337, COL338, COL339, COL340
			, COL341            
			, CREATE_DT 
            , HANDLE_FLAG 
        FROM  TB_RAW_EMS_INFO --  arrayRawTbl[0]
         WHERE COLEC_DT BETWEEN  p_from_datetime  AND p_to_datetime
         AND DEVICE_ID = case p_device_id 
         					when '' then DEVICE_ID
         					when null  then DEVICE_ID
         					else p_device_id
         				  end 
         AND COALESCE(DONE_FLAG, 'N') = 'N' 
         AND COALESCE(HANDLE_FLAG, 'N') = 'N';

        
        GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	    v_ExeCnt1 := v_work_num;
        
	   	-- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Insert TB_RAW_EMS_INFO_TMP Table : ', v_work_num));
		RAISE NOTICE 'v_log_fn[%] Insert TB_RAW_EMS_INFO_TMP Table [%]건 ', v_log_fn, v_work_num;
	   
		INSERT INTO TB_RAW_PCS_INFO_TMP 
        -- modified by stjoo 20151229 : add hint statement for performance improvement
        SELECT /*+ index(a PK_RAW_PCS_INFO) */ 
            A.DEVICE_ID  
            , A.COLEC_DT     
            , A.DONE_FLAG    
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL0, 3))) ), 'FM00000000') AS COL0  
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL1, 3))) ), 'FM00000000') AS COL1  
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL2, 3))) ), 'FM00000000') AS COL2  
    		 , A.COL3, A.COL4, A.COL5, A.COL6, A.COL7, A.COL8, A.COL9, A.COL10
    		 , A.COL11, A.COL12, A.COL13, A.COL14, A.COL15, A.COL16, A.COL17, A.COL18, A.COL19, A.COL20
    		 , A.COL21, A.COL22, A.COL23, A.COL24, A.COL25, A.COL26, A.COL27, A.COL28, A.COL29, A.COL30
    		 , A.COL31, A.COL32, A.COL33, A.COL34, A.COL35, A.COL36, A.COL37, A.COL38, A.COL39, A.COL40
    		 , A.COL41, A.COL42, A.COL43, A.COL44, A.COL45, A.COL46, A.COL47, A.COL48, A.COL49, A.COL50
    		 , A.COL51, A.COL52, A.COL53, A.COL54, A.COL55, A.COL56, A.COL57, A.COL58, A.COL59, A.COL60
    		 , A.COL61, A.COL62, A.COL63, A.COL64, A.COL65, A.COL66, A.COL67, A.COL68, A.COL69, A.COL70
    		 , A.COL71, A.COL72, A.COL73, A.COL74, A.COL75, A.COL76, A.COL77, A.COL78, A.COL79, A.COL80
    		 , A.COL81, A.COL82, A.COL83, A.COL84, A.COL85, A.COL86, A.COL87, A.COL88, A.COL89, A.COL90
    		 , A.COL91, A.COL92, A.COL93, A.COL94, A.COL95, A.COL96, A.COL97, A.COL98, A.COL99, A.COL100
    		 , A.COL101, A.COL102, A.COL103, A.COL104, A.COL105, A.COL106, A.COL107, A.COL108, A.COL109, A.COL110
    		 , A.COL111, A.COL112, A.COL113, A.COL114, A.COL115, A.COL116, A.COL117, A.COL118, A.COL119, A.COL120
    		 , A.COL121, A.COL122, A.COL123, A.COL124, A.COL125, A.COL126, A.COL127, A.COL128, A.COL129, A.COL130
    		 , A.COL131, A.COL132, A.COL133, A.COL134, A.COL135, A.COL136, A.COL137, A.COL138, A.COL139, A.COL140
    		 , A.COL141, A.COL142, A.COL143, A.COL144, A.COL145, A.COL146, A.COL147, A.COL148, A.COL149, A.COL150
    		 , A.COL151, A.COL152, A.COL153, A.COL154, A.COL155, A.COL156, A.COL157, A.COL158, A.COL159, A.COL160
    		 , A.COL161, A.COL162, A.COL163, A.COL164, A.COL165, A.COL166, A.COL167, A.COL168, A.COL169, A.COL170
    		 , A.COL171, A.COL172, A.COL173, A.COL174, A.COL175, A.COL176, A.COL177, A.COL178, A.COL179, A.COL180
    		 , A.COL181, A.COL182, A.COL183, A.COL184, A.COL185, A.COL186, A.COL187, A.COL188, A.COL189, A.COL190
    		 , A.COL191, A.COL192, A.COL193, A.COL194, A.COL195, A.COL196, A.COL197, A.COL198, A.COL199, A.COL200
    		 , A.COL201, A.COL202, A.COL203, A.COL204, A.COL205, A.COL206, A.COL207, A.COL208, A.COL209, A.COL210
    		 , A.COL211, A.COL212, A.COL213, A.COL214, A.COL215, A.COL216, A.COL217, A.COL218, A.COL219, A.COL220
    		 , A.COL221, A.COL222, A.COL223, A.COL224, A.COL225, A.COL226 
            , A.CREATE_DT 
            , A.HANDLE_FLAG 
         FROM TB_RAW_EMS_INFO_TMP B , 
         		TB_RAW_PCS_INFO  A
         WHERE  B.COLEC_DT = A.COLEC_DT 
         AND B.DEVICE_ID = A.DEVICE_ID;
        
        GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   
	    -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Insert TB_RAW_PCS_INFO_TMP Table : ', v_work_num));
		RAISE NOTICE 'v_log_fn[%] Insert TB_RAW_PCS_INFO_TMP Table [%]건', v_log_fn, v_work_num;


        INSERT INTO TB_RAW_BMS_INFO_TMP 
        /* modified by stjoo 20151229 : add hint statement for performance improvement */
        SELECT /*+ index(a PK_RAW_BMS_INFO) */ 
             A.DEVICE_ID 
            , A.COLEC_DT    
            , A.DONE_FLAG  
            , TO_CHAR(DEC2BIN( HEX2DEC(UPPER(SUBSTR(A.COL0, 3))) ), 'FM00000000') AS COL0
            , A.COL1, A.COL2, A.COL3, A.COL4, A.COL5, A.COL6, A.COL7, A.COL8, A.COL9, A.COL10    
            , A.COL11, A.COL12, A.COL13, A.COL14, A.COL15, A.COL16, A.COL17, A.COL18, A.COL19    
--          , A.COL20   
            , case when COALESCE(A.COL20,0)::numeric > 4 then '0' else A.COL20 end COL20  
            , A.COL21, A.COL22, A.COL23    
--          , A.COL24    
            , CASE WHEN position('0x' in A.COL24) > 0 THEN TO_CHAR(HEX2DEC(UPPER(SUBSTR(A.COL24, 3)))) ELSE A.COL24 END COL24
            , A.COL25, A.COL26, A.COL27, A.COL28, A.COL29, A.COL30    
            , A.COL31, A.COL32, A.COL33, A.COL34, A.COL35, A.COL36, A.COL37, A.COL38, A.COL39, A.COL40    
            , A.CREATE_DT 
            , A.HANDLE_FLAG 
        FROM 
            TB_RAW_EMS_INFO_TMP B , 
        	TB_RAW_BMS_INFO A -- arrayRawTbl[2]
        WHERE  	B.COLEC_DT = A.COLEC_DT 
           AND 	B.DEVICE_ID = A.DEVICE_ID
		;
	
      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   
	    -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Insert TB_RAW_BMS_INFO_TMP Table : ', v_work_num));
		RAISE NOTICE 'v_log_fn[%] Insert TB_RAW_BMS_INFO_TMP Table [%]건', v_log_fn, v_work_num;
 	
        IF v_ExeCnt1 > 0 THEN
            /* ---------------------- */
            /* 0. 다른 세션에서 처리되지 않도록 플래그 설정 */
            /* ---------------------- */

              FOREACH v_tbl SLICE 1 IN ARRAY arrayTbl
			LOOP
                v_sql := FORMAT(
                	'UPDATE %s B '	||
                	'SET 		B.HANDLE_FLAG = %s ' 	|| 
                	'WHERE 	COALESCE(B.DONE_FLAG, ''N'') = ''N'' '	||
	                'AND 		COALESCE(B.HANDLE_FLAG, ''N'') = ''N'' '	||
	                'AND 		(B.DEVICE_ID, B.COLEC_DT) IN ( SELECT   DEVICE_ID, COLEC_DT FROM %s_TMP ) ',
	                v_tbl, v_handleFlag, v_tbl
	               );
			     
	            EXECUTE v_sql;

	           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--				CALL wems.SP_LOG(v_log_seq, v_log_fn, 'Update ' || v_tbl || ' Records Temporary flag : ' || v_work_num || ', ' || v_handleFlag);
				RAISE NOTICE 'v_log_fn[%] Update % [%]건 v_handleFlag[%]', v_log_fn, v_tbl, v_work_num, v_handleFlag;
			END LOOP;
				

          
		  
            /* ---------------------- */
            /* 2. 상태정보 데이터 처리 */
            /* ---------------------- */
            /* 대상 레코드들의 CO2와 금액을 계산한 결과를 TEMP 테이블에 Insert */
            /* 2014.08.12 E700번대 장애는 사용자에게 보여주지 않도록 수정 */
            /* 2014.08.20 E900번대와 C999 장애만 사용자에게 보여주도록 수정 => 장애 발생된 상태 그대로 보여주도록 원복함 */
            
			 INSERT INTO TB_OPR_ESS_STATE_TMP (
	              DEVICE_ID 
	            , DEVICE_TYPE_CD 
	            , COLEC_DT 
	            , OPER_STUS_CD 
	            , PV_PW 
	            , PV_PW_H 
	            , PV_PW_PRICE 
	            , PV_PW_CO2 
	            , PV_STUS_CD 
	            , CONS_PW 
	            , CONS_PW_H 
	            , CONS_PW_PRICE 
	            , CONS_PW_CO2 
	            , BT_PW 
	            , BT_CHRG_PW_H 
	            , BT_CHRG_PW_PRICE 
	            , BT_CHRG_PW_CO2 
	            , BT_DCHRG_PW_H 
	            , BT_DCHRG_PW_PRICE 
	            , BT_DCHRG_PW_CO2 
	            , BT_SOC 
	            , BT_SOH 
	            , BT_STUS_CD 
	            , GRID_PW 
	            , GRID_OB_PW_H 
	            , GRID_OB_PW_PRICE 
	            , GRID_OB_PW_CO2 
	            , GRID_TR_PW_H 
	            , GRID_TR_PW_PRICE 
	            , GRID_TR_PW_CO2 
	            , GRID_STUS_CD 
	            , PCS_PW 
	            , PCS_FD_PW_H 
	            , PCS_PCH_PW_H 
	            , EMS_OPMODE 
	            , PCS_OPMODE1 
	            , PCS_OPMODE2 
	            , PCS_TGT_PW 
	            , FEED_IN_LIMIT 
	            , MAX_INVERTER_PW_CD 
	            , OUTLET_PW 
	            , OUTLET_PW_H 
	            , OUTLET_PW_PRICE 
	            , OUTLET_PW_CO2 
	            , CREATE_DT 
	            -- [ 2015.07.06 yngwie 수집 컬럼 추가
	            , PCS_COMM_ERR_RATE 
	            , SMTR_COMM_ERR_RATE 
	            , M2M_COMM_ERR_RATE            
	            , SMTR_TP_CD 
	            , SMTR_PULSE_CNT 
	            , SMTR_MODL_CD 
	            , PV_MAX_PWR1 
	            , PV_MAX_PWR2 
	            , INSTL_RGN 
	            , MMTR_SLV_ADDR 
	            , BASICMODE_CD 
	            , PCS_FLAG0 
	            , PCS_FLAG1 
	            , PCS_FLAG2 
	            , PCS_OPMODE_CMD1 
	            , PCS_OPMODE_CMD2 
	            , PV_V1 
	            , PV_I1 
	            , PV_PW1 
	            , PV_V2 
	            , PV_I2 
	            , PV_PW2 
	            , INVERTER_V 
	            , INVERTER_I 
	            , INVERTER_PW 
	            , DC_LINK_V 
	            , G_RLY_CNT 
	            , BAT_RLY_CNT 
	            , GRID_TO_GRID_RLY_CNT 
	            , BAT_PCHRG_RLY_CNT 
	            , BMS_FLAG0 
	            , BMS_DIAG0 
	            , RACK_V 
	            , RACK_I 
	            , CELL_MAX_V 
	            , CELL_MIN_V 
	            , CELL_AVG_V 
	            , CELL_MAX_T 
	            , CELL_MIN_T 
	            , CELL_AVG_T 
	            , TRAY_CNT 
	            , SYS_ST_CD 
	            , BT_CHRG_ST_CD 
	            , BT_DCHRG_ST_CD 
	            , PV_ST_CD 
	            , GRID_ST_CD 
	            , SMTR_ST_CD 
	            , SMTR_TR_CNTER 
	            , SMTR_OB_CNTER        
	            , DN_ENABLE    
	            , DC_START     
	            , DC_END   
	            , NC_START                 
	            , NC_END       
	            -- ]            
	           --[신규모델에 대한 모니터링 추가
	            , GRID_CODE0 
	            , GRID_CODE1 
	            , GRID_CODE2 
	            , PCON_BAT_TARGETPOWER 
	            , GRID_CODE3 
	            , BDC_MODULE_CODE0 
	            , BDC_MODULE_CODE1 
	            , BDC_MODULE_CODE2 
	            , BDC_MODULE_CODE3 
	            , FAULT5_REALTIME_YEAR 
	            , FAULT5_REALTIME_MONTH 
	            , FAULT5_DAY 
	            , FAULT5_HOUR 
	            , FAULT5_MINUTE 
	            , FAULT5_SECOND 
	            , FAULT5_DATAHIGH 
	            , FAULT5_DATALOW 
	            , FAULT6_REALTIME_YEAR 
	            , FAULT6_REALTIME_MONTH 
	            , FAULT6_REALTIME_DAY 
	            , FAULT6_REALTIME_HOUR 
	            , FAULT6_REALTIME_MINUTE 
	            , FAULT6_REALTIME_SECOND 
	            , FAULT6_DATAHIGH 
	            , FAULT6_DATALOW 
	            , FAULT7_REALTIME_YEAR 
	            , FAULT7_REALTIME_MONTH 
	            , FAULT7_REALTIME_DAY 
	            , FAULT7_REALTIME_HOUR 
	            , FAULT7_REALTIME_MINUTE 
	            , FAULT7_REALTIME_SECOND 
	            , FAULT7_DATAHIGH 
	            , FAULT7_DATALOW 
	            , FAULT8_REALTIME_YEAR 
	            , FAULT8_REALTIME_MONTH 
	            , FAULT8_REALTIME_DAY 
	            , FAULT8_REALTIME_HOUR 
	            , FAULT8_REALTIME_MINUTE 
	            , FAULT8_REALTIME_SECOND 
	            , FAULT8_DATAHIGH 
	            , FAULT8_DATALOW 
	            , FAULT9_REALTIME_YEAR 
	            , FAULT9_REALTIME_MONTH 
	            , FAULT9_REALTIME_DAY 
	            , FAULT9_REALTIME_HOUR 
	            , FAULT9_REALTIME_MINUTE 
	            , FAULT9_REALTIME_SECOND 
	            , FAULT9_DATAHIGH 
	            , FAULT9_DATALOW 
	            --]
            ) 
            SELECT   
                A.DEVICE_ID AS DEVICE_ID   
                , B.DEVICE_TYPE_CD AS DEVICE_TYPE_CD   
                , A.COLEC_DT AS COLEC_DT   
                , 0 AS OPER_STUS_CD 
                , A.COL8::numeric AS PV_PW   
                , A.COL17::numeric AS PV_PW_H   
                , TRUNC(ABS(A.COL17)::numeric * ET.UNIT_PRICE * .001, 2) AS PV_PW_PRICE   
                , ABS(A.COL17)::numeric * D.CARB_EMI_QUITY * .001 AS PV_PW_CO2   
                , SUBSTR(A.COL88, 3, 1) AS PV_STUS_CD   
                , A.COL7::numeric AS CONS_PW   
                , A.COL16::numeric AS CONS_PW_H   
                , TRUNC(ABS(A.COL16::numeric) * E.UNIT_PRICE * .001, 2) AS CONS_PW_PRICE   
                , ABS(A.COL16::numeric) * D.CARB_EMI_QUITY * .001 AS CONS_PW_CO2   
                , A.COL10::numeric AS BT_PW   
                , A.COL20::numeric AS BT_CHRG_PW_H   
                , TRUNC(ABS(A.COL20::numeric) * E.UNIT_PRICE * .001, 2) AS BT_CHRG_PW_PRICE   
                , ABS(A.COL20::numeric) * D.CARB_EMI_QUITY * .001 AS BT_CHRG_PW_CO2   
                , A.COL21::numeric AS BT_DCHRG_PW_H   
                , TRUNC(ABS(A.COL21::numeric) * E.UNIT_PRICE * .001, 2) AS BT_DCHRG_PW_PRICE   
                , ABS(A.COL21::numeric) * D.CARB_EMI_QUITY * .001 AS BT_DCHRG_PW_CO2   
                , case when (A.COL11::numeric >= 1000 OR A.COL11::numeric < -999) then -999 else A.COL11::numeric end AS BT_SOC   
                , COALESCE(FN_GET_CONV_NA(A.COL13), 0)::numeric AS BT_SOH   
                , CASE   
                     WHEN SUBSTR(A.COL88, 1, 1) = '1' THEN '0'  
                     WHEN SUBSTR(A.COL88, 2, 1) = '1' THEN '1'  
                     ELSE '2'   
                  END AS BT_STUS_CD   
                , A.COL6::numeric AS GRID_PW   
                , A.COL15::numeric AS GRID_OB_PW_H   
                , TRUNC(ABS(A.COL15::numeric) * E.UNIT_PRICE * .001, 2) AS GRID_OB_PW_PRICE   
                , ABS(A.COL15::numeric) * D.CARB_EMI_QUITY * .001 AS GRID_OB_PW_CO2   
                , A.COL14::numeric AS GRID_TR_PW_H   
                , TRUNC(ABS(A.COL14::numeric) * ET.UNIT_PRICE * .001, 2) AS GRID_TR_PW_PRICE   
                , ABS(A.COL14::numeric) * D.CARB_EMI_QUITY * .001 AS GRID_TR_PW_CO2   
                , CASE   
                     WHEN COALESCE(A.COL6::numeric, 0) > 0 THEN '0'
                     WHEN COALESCE(A.COL6::numeric, 0) = 0 THEN '2'
                     ELSE '1'  
                  END GRID_STUS_CD   
                , A.COL9::numeric AS PCS_PW   
                , A.COL18::numeric AS PCS_FD_PW_H   
                , A.COL19::numeric AS PCS_PCH_PW_H  
                , A.COL0 AS EMS_OPMODE    
                , A.COL1 AS PCS_OPMODE1   
                , A.COL2 AS PCS_OPMODE2   
                , A.COL3 AS PCS_TGT_PW   
                , A.COL331 AS FEED_IN_LIMIT   
                , A.COL332 AS MAX_INVERTER_PW_CD 
            -- [ 2015.05.13 yngwie OUTLET_PW 추가
               , FN_GET_CONV_NA(A.COL5)::numeric AS OUTLET_PW   
               , FN_GET_CONV_NA(A.COL22)::numeric AS OUTLET_PW_H   
               , TRUNC(ABS(FN_GET_CONV_NA(A.COL22)::numeric) * E.UNIT_PRICE * .001, 2) AS OUTLET_PW_PRICE   
               , ABS(FN_GET_CONV_NA(A.COL22)::numeric) * D.CARB_EMI_QUITY * .001 AS OUTLET_PW_CO2   
            -- ]
               , SYS_EXTRACT_UTC(NOW()) AS CREATE_DT    
            -- [ 2015.07.06 yngwie 수집 컬럼 추가
				, FN_GET_CONV_NA(A.COL34)::numeric AS PCS_COMM_ERR_RATE 
				, FN_GET_CONV_NA(A.COL35)::numeric AS SMTR_COMM_ERR_RATE 
				, FN_GET_CONV_NA(A.COL36)::numeric AS M2M_COMM_ERR_RATE          
				, A.COL324 AS SMTR_TP_CD
				, FN_GET_CONV_NA(A.COL325)::numeric AS SMTR_PULSE_CNT 
				, A.COL326 AS SMTR_MODL_CD
				, FN_GET_CONV_NA(A.COL328)::numeric AS PV_MAX_PWR1 
				, FN_GET_CONV_NA(A.COL329)::numeric AS PV_MAX_PWR2 
				, A.COL330 AS INSTL_RGN
				, FN_GET_CONV_NA(A.COL334)::numeric AS MMTR_SLV_ADDR 
				, A.COL335 AS BASICMODE_CD 
	            , P.COL0                                                       AS PCS_FLAG0 
	            , P.COL1                                                       AS PCS_FLAG1 
	            , P.COL2                                                       AS PCS_FLAG2 
	            -- ]
	            -- [ 2015.07.13 modified by stjoo : P.COL20s value was NA on test ems.
              , FN_GET_CONV_NA(P.COL20)                                      AS PCS_OPMODE_CMD1 
              , P.COL21                                                      AS PCS_OPMODE_CMD2 
              , FN_GET_CONV_NA(P.COL38)::numeric   AS PV_V1 
              , FN_GET_CONV_NA(P.COL39)::numeric   AS PV_I1 
              , CASE WHEN FN_GET_CONV_NA(P.COL40)::numeric >= 100000 OR FN_GET_CONV_NA(P.COL40)::numeric <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL40)::numeric END AS PV_PW1  
              , FN_GET_CONV_NA(P.COL42)::numeric   AS PV_V2 
              , FN_GET_CONV_NA(P.COL43)::numeric   AS PV_I2 
              , CASE WHEN FN_GET_CONV_NA(P.COL44)::numeric >= 100000 OR FN_GET_CONV_NA(P.COL44)::numeric <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL44)::numeric END AS PV_PW2  
              , CASE WHEN FN_GET_CONV_NA(P.COL53)::numeric >= 100000 OR FN_GET_CONV_NA(P.COL53)::numeric <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL53)::numeric END AS INVERTER_V 
              , CASE WHEN FN_GET_CONV_NA(P.COL54)::numeric >= 10000 OR FN_GET_CONV_NA(P.COL54)::numeric <= -10000 THEN 0 ELSE FN_GET_CONV_NA(P.COL54)::numeric END AS INVERTER_I 
              , FN_GET_CONV_NA(P.COL55)::numeric   AS INVERTER_PW 
              , CASE WHEN FN_GET_CONV_NA(P.COL59)::numeric >= 100000 OR FN_GET_CONV_NA(P.COL59)::numeric <= -100000 THEN 0 ELSE FN_GET_CONV_NA(P.COL59)::numeric END AS DC_LINK_V 
              , FN_GET_CONV_NA(P.COL61)::numeric   AS G_RLY_CNT 
              , FN_GET_CONV_NA(P.COL62)::numeric   AS BAT_RLY_CNT 
              , FN_GET_CONV_NA(P.COL63)::numeric   AS GRID_TO_GRID_RLY_CNT 
              , FN_GET_CONV_NA(P.COL64)::numeric   AS BAT_PCHRG_RLY_CNT   
              , M.COL0                               AS BMS_FLAG0 
              , M.COL1                               AS BMS_DIAG0 
              , FN_GET_CONV_NA(M.COL16)::numeric   AS RACK_V 
              , FN_GET_CONV_NA(M.COL17)::numeric   AS RACK_I 
              , FN_GET_CONV_NA(M.COL18)::numeric   AS CELL_MAX_V 
              , FN_GET_CONV_NA(M.COL19)::numeric   AS CELL_MIN_V 
              , FN_GET_CONV_NA(M.COL20)::numeric   AS CELL_AVG_V 
              , FN_GET_CONV_NA(M.COL21)::numeric   AS CELL_MAX_T 
              , FN_GET_CONV_NA(M.COL22)::numeric   AS CELL_MIN_T 
              , FN_GET_CONV_NA(M.COL23)::numeric   AS CELL_AVG_T 
              , FN_GET_CONV_NA(M.COL24)::numeric   AS TRAY_CNT 
              , CASE WHEN SUBSTR(A.COL88, 4, 1) = '0' AND SUBSTR(A.COL88, 8, 1) = '1' THEN '1' ELSE '0' END AS SYS_ST_CD
              , SUBSTR(A.COL89, 7, 1) AS BT_CHRG_ST_CD 
              , SUBSTR(A.COL89, 6, 1) AS BT_DCHRG_ST_CD 
              , SUBSTR(A.COL89, 8, 1) AS PV_ST_CD 
              , SUBSTR(A.COL89, 5, 1) AS GRID_ST_CD 
              , SUBSTR(A.COL89, 4, 1) AS SMTR_ST_CD 
              , A.COL127 AS SMTR_TR_CNTER 
              , A.COL128 AS SMTR_OB_CNTER 
              , A.COL337 AS DN_ENABLE 
              , A.COL338 AS DC_START 
              , A.COL339 AS DC_END 
              , A.COL340 AS NC_START 
              , A.COL341 AS NC_END 
            -- ]     
			--[신규모델로 모니터링 추가
              , CASE WHEN P.COL3  ='NA' THEN NULL ELSE P.COL3   END as GRID_CODE0 
              , CASE WHEN P.COL4  ='NA' THEN NULL ELSE P.COL4   END as GRID_CODE1 
              , CASE WHEN P.COL5  ='NA' THEN NULL ELSE P.COL5   END as GRID_CODE2 
              , CASE WHEN P.COL19 ='NA' THEN NULL ELSE P.COL19  END as PCON_BAT_TARGETPOWER 
              , CASE WHEN P.COL31 ='NA' THEN NULL ELSE P.COL31  END as GRID_CODE3 
              , CASE WHEN P.COL32 ='NA' THEN NULL ELSE P.COL32  END as BDC_MODULE_CODE0 
              , CASE WHEN P.COL33 ='NA' THEN NULL ELSE P.COL33  END as BDC_MODULE_CODE1 
              , CASE WHEN P.COL34 ='NA' THEN NULL ELSE P.COL34  END as BDC_MODULE_CODE2 
              , CASE WHEN P.COL35 ='NA' THEN NULL ELSE P.COL35  END as BDC_MODULE_CODE3 
              , CASE WHEN P.COL36 ='NA' THEN NULL ELSE P.COL36  END as FAULT5_REALTIME_YEAR 
              , CASE WHEN P.COL37 ='NA' THEN NULL ELSE P.COL37  END as FAULT5_REALTIME_MONTH 
              , CASE WHEN P.COL87 ='NA' THEN NULL ELSE P.COL87  END as FAULT5_DAY 
              , CASE WHEN P.COL88 ='NA' THEN NULL ELSE P.COL88  END as FAULT5_HOUR 
              , CASE WHEN P.COL89 ='NA' THEN NULL ELSE P.COL89  END as FAULT5_MINUTE 
              , CASE WHEN P.COL90 ='NA' THEN NULL ELSE P.COL90  END as FAULT5_SECOND 
              , CASE WHEN P.COL91 ='NA' THEN NULL ELSE P.COL91  END as FAULT5_DATAHIGH 
              , CASE WHEN P.COL162='NA' THEN NULL ELSE P.COL162 END as FAULT5_DATALOW 
              , CASE WHEN P.COL163='NA' THEN NULL ELSE P.COL163 END as FAULT6_REALTIME_YEAR 
              , CASE WHEN P.COL164='NA' THEN NULL ELSE P.COL164 END as FAULT6_REALTIME_MONTH 
              , CASE WHEN P.COL165='NA' THEN NULL ELSE P.COL165 END as FAULT6_REALTIME_DAY 
              , CASE WHEN P.COL166='NA' THEN NULL ELSE P.COL166 END as FAULT6_REALTIME_HOUR 
              , CASE WHEN P.COL167='NA' THEN NULL ELSE P.COL167 END as FAULT6_REALTIME_MINUTE 
              , CASE WHEN P.COL168='NA' THEN NULL ELSE P.COL168 END as FAULT6_REALTIME_SECOND 
              , CASE WHEN P.COL169='NA' THEN NULL ELSE P.COL169 END as FAULT6_DATAHIGH 
              , CASE WHEN P.COL170='NA' THEN NULL ELSE P.COL170 END as FAULT6_DATALOW 
              , CASE WHEN P.COL171='NA' THEN NULL ELSE P.COL171 END as FAULT7_REALTIME_YEAR 
              , CASE WHEN P.COL177='NA' THEN NULL ELSE P.COL177 END as FAULT7_REALTIME_MONTH 
              , CASE WHEN P.COL178='NA' THEN NULL ELSE P.COL178 END as FAULT7_REALTIME_DAY 
              , CASE WHEN P.COL179='NA' THEN NULL ELSE P.COL179 END as FAULT7_REALTIME_HOUR 
              , CASE WHEN P.COL187='NA' THEN NULL ELSE P.COL187 END as FAULT7_REALTIME_MINUTE 
              , CASE WHEN P.COL188='NA' THEN NULL ELSE P.COL188 END as FAULT7_REALTIME_SECOND 
              , CASE WHEN P.COL189='NA' THEN NULL ELSE P.COL189 END as FAULT7_DATAHIGH 
              , CASE WHEN P.COL191='NA' THEN NULL ELSE P.COL191 END as FAULT7_DATALOW 
              , CASE WHEN P.COL192='NA' THEN NULL ELSE P.COL192 END as FAULT8_REALTIME_YEAR 
              , CASE WHEN P.COL193='NA' THEN NULL ELSE P.COL193 END as FAULT8_REALTIME_MONTH 
              , CASE WHEN P.COL194='NA' THEN NULL ELSE P.COL194 END as FAULT8_REALTIME_DAY 
              , CASE WHEN P.COL195='NA' THEN NULL ELSE P.COL195 END as FAULT8_REALTIME_HOUR 
              , CASE WHEN P.COL196='NA' THEN NULL ELSE P.COL196 END as FAULT8_REALTIME_MINUTE 
              , CASE WHEN P.COL197='NA' THEN NULL ELSE P.COL197 END as FAULT8_REALTIME_SECOND 
              , CASE WHEN P.COL198='NA' THEN NULL ELSE P.COL198 END as FAULT8_DATAHIGH 
              , CASE WHEN P.COL199='NA' THEN NULL ELSE P.COL199 END as FAULT8_DATALOW 
              , CASE WHEN P.COL200='NA' THEN NULL ELSE P.COL200 END as FAULT9_REALTIME_YEAR 
              , CASE WHEN P.COL201='NA' THEN NULL ELSE P.COL201 END as FAULT9_REALTIME_MONTH 
              , CASE WHEN P.COL202='NA' THEN NULL ELSE P.COL202 END as FAULT9_REALTIME_DAY 
              , CASE WHEN P.COL203='NA' THEN NULL ELSE P.COL203 END as FAULT9_REALTIME_HOUR 
              , CASE WHEN P.COL204='NA' THEN NULL ELSE P.COL204 END as FAULT9_REALTIME_MINUTE 
              , CASE WHEN P.COL205='NA' THEN NULL ELSE P.COL205 END as FAULT9_REALTIME_SECOND 
              , CASE WHEN P.COL206='NA' THEN NULL ELSE P.COL206 END as FAULT9_DATAHIGH 
              , CASE WHEN P.COL207='NA' THEN NULL ELSE P.COL207 END as FAULT9_DATALOW 
            --]
            FROM   
                TB_RAW_EMS_INFO_TMP A   
                , TB_RAW_PCS_INFO_TMP P   
                , TB_RAW_BMS_INFO_TMP M   
                , TB_BAS_DEVICE B   
                , TB_PLF_CARB_EMI_QUITY D   
                , TB_BAS_ELPW_UNIT_PRICE E 
                , TB_BAS_ELPW_UNIT_PRICE ET 
            WHERE   
                A.DEVICE_ID = P.DEVICE_ID
                AND A.COLEC_DT = P.COLEC_DT   
                AND A.DEVICE_ID = M.DEVICE_ID   
                AND A.COLEC_DT = M.COLEC_DT   
                AND A.DEVICE_ID = B.DEVICE_ID   
                AND B.CNTRY_CD = D.CNTRY_CD   
                AND D.USE_FLAG = 'Y'
                AND B.ELPW_PROD_CD = E.ELPW_PROD_CD   
                AND E.IO_FLAG = 'O'
                AND E.USE_FLAG = 'Y'
            -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가
                AND E.WKDAY_FLAG = FN_GET_WKDAY_FLAG(SUBSTR(A.COLEC_DT, 1, 8)) 
            -- 2015.07.31 요금제 년월 변경
            --  AND SUBSTR(A.COLEC_DT, 1, 6) = E.UNIT_YM   
                AND E.UNIT_YM = '000000'
                AND SUBSTR(A.COLEC_DT, 9, 2) = E.UNIT_HH 
                AND B.ELPW_PROD_CD = ET.ELPW_PROD_CD  
                AND ET.IO_FLAG = 'T'   
                AND ET.USE_FLAG = 'Y'  
            -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가, T 는  1 만 있음
            -- 2015.08.06 modified by yngwie T 도 주말 0 추가
            --  AND ET.WKDAY_FLAG = '1'   
                AND ET.WKDAY_FLAG = FN_GET_WKDAY_FLAG(SUBSTR(A.COLEC_DT, 1, 8))
            -- 2015.07.31 요금제 년월 변경
            --  AND SUBSTR(A.COLEC_DT, 1, 6) = ET.UNIT_YM  
                AND ET.UNIT_YM = '000000'   
                AND SUBSTR(A.COLEC_DT, 9, 2) = ET.UNIT_HH 
            ;

	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	       -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Insert TB_OPR_ESS_STATE_TMP Table :  ', v_work_num));
	      	RAISE notice 'v_log_fn[%] Insert TB_OPR_ESS_STATE_TMP Table :  [%]건', v_log_fn, v_work_num;
            

	      	v_ExeCnt1 := 0;
			v_ExeCnt2 := 0;
			v_ExeCnt3 := 0;

            FOR v_row IN ( 
		      
	            -- 2015.01.07 stjoo,  아래 코드만 오류로 처리 E601 ~ 603, E901 ~ 907, C128
	            -- 2015.07.14 yngwie, 호주 장비인 경우 E706, E708 장애로 처리
	            -- 2015.09.03 yngwie, 호주 장비인 경우 E706, E708 장애로 처리 => 모델별 에러코드 정의에 등록되어 있으므로 구분할 필요 없음 
	            WITH ERR_INFO AS (  
	                SELECT  
	                    DEVICE_ID   
	                    , DEVICE_TYPE_CD   
	                    , CASE    
	                        WHEN MAX(ALARM_TYPE_CD::INTEGER) = 1 AND ALARM_CD = 'C128' THEN '3'
	                        WHEN MAX(ALARM_TYPE_CD::INTEGER) = 3 THEN '2' 
	--                      WHEN MAX(ALARM_TYPE_CD::INTEGER) IN (1, 2) AND ALARM_CD <> 'C128' THEN '1'
	                        WHEN MAX(ALARM_TYPE_CD::INTEGER) = 2 THEN '1'
	                        ELSE '0'
	                      END AS OPER_STUS_CD
	                    , MAX(EVENT_DT) AS EVENT_DT
	                FROM TB_OPR_ALARM 
	            --E202오류의 경우 사용자에게 보여주지 않도록 한다.
	                WHERE ALARM_CD NOT IN ('E202')  
	                GROUP BY   DEVICE_ID, DEVICE_TYPE_CD, ALARM_CD
	             ) 
	            SELECT 
	                DEVICE_ID 
	                , DEVICE_TYPE_CD 
	                , MAX(OPER_STUS_CD) AS OPER_STUS_CD 
	                , MAX(EVENT_DT) AS EVENT_DT 
	            FROM        ERR_INFO 
	            GROUP BY  DEVICE_ID, DEVICE_TYPE_CD
	            
			) LOOP
	      		RAISE NOTICE 'v_log_fn[%] v_log_seq[%] 알람 OPER_STUS_CD 변경 DEVICE_ID[%] OPER_STUS_CD[%] DEVICE_TYPE_CD[%] COLEC_DT[%]', 
	      							v_log_fn, v_log_seq, v_row.DEVICE_ID, v_row.OPER_STUS_CD,  v_row.DEVICE_TYPE_CD,  v_row.EVENT_DT;
            
	            -- 통신 단절인 장비의 경우 1분 데이터가 수집되지 않으므로 상태를 현황 임시 테이블에도 Update 한다.
	            UPDATE TB_OPR_ESS_STATE_TMP
	            SET OPER_STUS_CD = v_row.OPER_STUS_CD 
	            WHERE DEVICE_ID = v_row.DEVICE_ID
	            AND DEVICE_TYPE_CD =v_row.DEVICE_TYPE_CD;
	           
       	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        	v_rslt_cnt := v_rslt_cnt + v_work_num;	
				v_ExeCnt1 := v_ExeCnt1 + v_work_num;
			
	            -- 통신 단절인 장비는 현황 임시 테이블에 데이터가 수집되지 않을 수 있으니 현황 테이블에도 Update 한다.
	            UPDATE TB_OPR_ESS_STATE 
	            SET OPER_STUS_CD = v_row.OPER_STUS_CD
	            WHERE DEVICE_ID = v_row.DEVICE_ID
	            AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;

       	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
				v_ExeCnt2 := v_ExeCnt2 + v_work_num;

	        
	            UPDATE TB_OPR_ESS_STATE_HIST
	            SET OPER_STUS_CD = v_row.OPER_STUS_CD
	            WHERE DEVICE_ID = v_row.DEVICE_ID
	            AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD
	            AND COLEC_DT = v_row.EVENT_DT;

       	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
				v_ExeCnt3 := v_ExeCnt3 + v_work_num;
	        
			END LOOP;

           -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Update TB_OPR_ESS_STATE_TMP Table For OPER_STUS_CD :  ', v_ExeCnt1));
           -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Update TB_OPR_ESS_STATE Table For OPER_STUS_CD :  ', v_ExeCnt2));
           -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Update TB_OPR_ESS_STATE_HIST Table For OPER_STUS_CD :  ', v_ExeCnt3));
	      	RAISE NOTICE 'v_log_fn[%] Update TB_OPR_ESS_STATE_TMP Table For OPER_STUS_CD : [%]', v_log_fn, v_ExeCnt1;
	      	RAISE NOTICE 'v_log_fn[%] Update TB_OPR_ESS_STATE Table For OPER_STUS_CD : [%]', v_log_fn, v_ExeCnt2;
	      	RAISE NOTICE 'v_log_fn[%] Update TB_OPR_ESS_STATE_HIST Table For OPER_STUS_CD : [%]', v_log_fn, v_ExeCnt3;
   
       
            -- #411 Sleep 모드의 상태의 장비를 절전 상태로 상태 변경을 한다. 20160105 jkm1020
	      	v_ExeCnt1 := 0;
			v_ExeCnt2 := 0;
			v_ExeCnt3 := 0;

            FOR v_row IN ( 
	            SELECT  DEVICE_ID  
	                 	, DEVICE_TYPE_CD   
	                 	, '5' AS OPER_STUS_CD  
	                 	, COLEC_DT
	            FROM   TB_OPR_ESS_STATE_TMP 
	            WHERE  EMS_OPMODE = '9'
	            
 			) LOOP
	      		RAISE NOTICE 'v_log_fn[%] v_log_seq[%]  Sleep상태의 장비를 절전상태로 변경 DEVICE_ID[%] OPER_STUS_CD[%] DEVICE_TYPE_CD[%] COLEC_DT[%]', 
	      							v_log_fn, v_log_seq, v_row.DEVICE_ID, v_row.OPER_STUS_CD,  v_row.DEVICE_TYPE_CD,  v_row.COLEC_DT;

	      						
	            -- 통신 단절인 장비의 경우 1분 데이터가 수집되지 않으므로 상태를 현황 임시 테이블에도 Update 한다.
	            UPDATE 	TB_OPR_ESS_STATE_TMP 
	            SET 		OPER_STUS_CD =v_row.OPER_STUS_CD
	            WHERE 	DEVICE_ID = v_row.DEVICE_ID
	            AND 		DEVICE_TYPE_CD =v_row.DEVICE_TYPE_CD;
	           
       	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        	v_rslt_cnt := v_rslt_cnt + v_work_num;	
				v_ExeCnt1 := v_ExeCnt1 + v_work_num;
			
	            
	            -- 통신 단절인 장비는 현황 임시 테이블에 데이터가 수집되지 않을 수 있으니 현황 테이블에도 Update 한다.
	            UPDATE 	TB_OPR_ESS_STATE 
	            SET 		OPER_STUS_CD = v_row.OPER_STUS_CD
	            WHERE 	DEVICE_ID = v_row.DEVICE_ID
	            AND 		DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;

       	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        	v_rslt_cnt := v_rslt_cnt + v_work_num;	
				v_ExeCnt2 := v_ExeCnt2 + v_work_num;

			
	            UPDATE TB_OPR_ESS_STATE_HIST 
	            SET 		OPER_STUS_CD = v_row.OPER_STUS_CD
	            WHERE 	DEVICE_ID =v_row.DEVICE_ID
	            AND 		DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD
	            AND 		COLEC_DT = v_row.COLEC_DT;

       	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        	v_rslt_cnt := v_rslt_cnt + v_work_num;	
				v_ExeCnt3 := v_ExeCnt3 + v_work_num;

            END LOOP;

           -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Update TB_OPR_ESS_STATE_TMP Table For OPER_STUS_CD Sleep Mode :  ', v_ExeCnt1));
           -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Update TB_OPR_ESS_STATE Table For OPER_STUS_CD Sleep Mode  :  ', v_ExeCnt2));
           -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Update TB_OPR_ESS_STATE_HIST Table For OPER_STUS_CD Sleep Mode :  ', v_ExeCnt3));
	      	RAISE notice 'v_log_fn[%] Update TB_OPR_ESS_STATE_TMP Table For OPER_STUS_CD Sleep Mode : [%]', v_log_fn, v_ExeCnt1;
	      	RAISE notice 'v_log_fn[%] Update TB_OPR_ESS_STATE Table For OPER_STUS_CD Sleep Mode : [%]', v_log_fn, v_ExeCnt2;
	      	RAISE notice 'v_log_fn[%] Update TB_OPR_ESS_STATE_HIST Table For OPER_STUS_CD Sleep Mode : [%]', v_log_fn, v_ExeCnt3;

            
            --- 	2015.07.07 yngwie 장비별 상태 타임라인 데이터 처리
            WITH B  AS ( 
                WITH PRIORITY AS ( 
                    SELECT 1 AS PRIORITY, '2' AS OPER_STUS_CD FROM DUAL UNION ALL 
                    SELECT 2 AS PRIORITY, '1' AS OPER_STUS_CD FROM DUAL UNION ALL 
                    SELECT 3 AS PRIORITY, '3' AS OPER_STUS_CD FROM DUAL 
                ), UALL AS ( 
                    SELECT 
                        TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'YYYYMMDD') AS COLEC_DD 
                        , A.DEVICE_ID 
                        , A.DEVICE_TYPE_CD 
                        , A.OPER_STUS_CD 
                        , B.PRIORITY 
                    FROM 
                        TB_OPR_ESS_STATE A 
                        , PRIORITY B 
                    WHERE 
                        A.OPER_STUS_CD = B.OPER_STUS_CD 
                    UNION ALL 
                    SELECT 
                        A.COLEC_DD 
                        , A.DEVICE_ID 
                        , A.DEVICE_TYPE_CD 
                        , A.OPER_STUS_CD 
                        , B.PRIORITY 
                    FROM 
                        TB_OPR_ESS_STATE_TL A 
                        , PRIORITY B 
                    WHERE 
                        A.OPER_STUS_CD = B.OPER_STUS_CD 
                        AND A.COLEC_DD = TO_CHAR(SYS_EXTRACT_UTC(NOW()), 'YYYYMMDD') 
                ) 
                SELECT    * 
                FROM ( 
                    SELECT 
                        COLEC_DD 
                        , DEVICE_ID 
                        , DEVICE_TYPE_CD 
                        , OPER_STUS_CD 
                        , PRIORITY 
                        , ROW_NUMBER() OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY PRIORITY) AS RK 
                    FROM 
                        UALL 
                ) T
                WHERE RK = 1 
            ), U as (
                UPDATE TB_OPR_ESS_STATE_TL A
                SET        A.OPER_STUS_CD = B.OPER_STUS_CD 
                FROM    B
				WHERE   A.DEVICE_ID = B.DEVICE_ID 
                AND 		A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD 
                AND 		A.COLEC_DD = B.COLEC_DD 
				
                RETURNING U.*
            )
            INSERT INTO TB_OPR_ESS_STATE_TL (  
                 DEVICE_ID 
                , DEVICE_TYPE_CD 
                , COLEC_DD 
                , OPER_STUS_CD 
            ) 
            SELECT 
                B.DEVICE_ID 
                , B.DEVICE_TYPE_CD 
                , B.COLEC_DD 
                , B.OPER_STUS_CD 
           	FROM B
			WHERE NOT EXISTS (  
				SELECT 	1 
			    FROM 	U 
				WHERE	COLEC_DD 			= B.COLEC_DD  
					AND 	DEVICE_ID			= B.DEVICE_ID  
					AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
			  )
            ;            
           
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	       -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Merge TB_OPR_ESS_STATE_TL :  : ', v_work_num));
	      	RAISE notice 'v_log_fn[%] Merge TB_OPR_ESS_STATE_TL :  [%]건', v_log_fn, v_work_num;


            -- TEMP 테이블의 레코드 중 가장 최근 데이터를 상태정보 테이블에 Merge
            WITH B AS (
				SELECT	* 
				FROM (   
				        SELECT	ROW_NUMBER() OVER (PARTITION BY A.DEVICE_ID ORDER BY A.COLEC_DT DESC ) SEQ   
				            		, A.*   
				        FROM   TB_OPR_ESS_STATE_TMP A   
				    ) A 
				    WHERE A.SEQ = 1 
            ), U as ( 
	            UPDATE TB_OPR_ESS_STATE A
	            SET
		              A.COLEC_DT         = B.COLEC_DT           
		            , A.OPER_STUS_CD     = B.OPER_STUS_CD       
		            , A.PV_PW            = B.PV_PW              
		            , A.PV_PW_H          = B.PV_PW_H            
		            , A.PV_PW_PRICE      = B.PV_PW_PRICE        
		            , A.PV_PW_CO2        = B.PV_PW_CO2          
		            , A.PV_STUS_CD       = B.PV_STUS_CD         
		            , A.CONS_PW          = B.CONS_PW            
		            , A.CONS_PW_H        = B.CONS_PW_H          
		            , A.CONS_PW_PRICE    = B.CONS_PW_PRICE      
		            , A.CONS_PW_CO2      = B.CONS_PW_CO2        
		            , A.BT_PW            = B.BT_PW              
		            , A.BT_CHRG_PW_H     = B.BT_CHRG_PW_H       
		            , A.BT_CHRG_PW_PRICE = B.BT_CHRG_PW_PRICE   
		            , A.BT_CHRG_PW_CO2   = B.BT_CHRG_PW_CO2     
		            , A.BT_DCHRG_PW_H    = B.BT_DCHRG_PW_H      
		            , A.BT_DCHRG_PW_PRICE= B.BT_DCHRG_PW_PRICE  
		            , A.BT_DCHRG_PW_CO2  = B.BT_DCHRG_PW_CO2    
		            , A.BT_SOC           = B.BT_SOC             
		            , A.BT_SOH           = B.BT_SOH             
		            , A.BT_STUS_CD       = B.BT_STUS_CD         
		            , A.GRID_PW          = B.GRID_PW            
		            , A.GRID_OB_PW_H     = B.GRID_OB_PW_H          
		            , A.GRID_OB_PW_PRICE = B.GRID_OB_PW_PRICE      
		            , A.GRID_OB_PW_CO2   = B.GRID_OB_PW_CO2        
		            , A.GRID_TR_PW_H     = B.GRID_TR_PW_H       
		            , A.GRID_TR_PW_PRICE = B.GRID_TR_PW_PRICE   
		            , A.GRID_TR_PW_CO2   = B.GRID_TR_PW_CO2     
		            , A.GRID_STUS_CD     = B.GRID_STUS_CD       
		            , A.PCS_PW           = B.PCS_PW             
		            , A.PCS_FD_PW_H      = B.PCS_FD_PW_H        
		            , A.PCS_PCH_PW_H     = B.PCS_PCH_PW_H       
		            , A.CREATE_DT        = COALESCE(B.CREATE_DT, SYS_EXTRACT_UTC(NOW()))           
		            , A.EMS_OPMODE       = B.EMS_OPMODE       
		            , A.PCS_OPMODE1      = B.PCS_OPMODE1       
		            , A.PCS_OPMODE2      = B.PCS_OPMODE2       
		            , A.PCS_TGT_PW       = B.PCS_TGT_PW       
		            , A.FEED_IN_LIMIT    = B.FEED_IN_LIMIT       
		            , A.MAX_INVERTER_PW_CD    = B.MAX_INVERTER_PW_CD       
		            --, A.RFSH_PRD = ROUND((TO_DATE(B.COLEC_DT, 'YYYYMMDDHH24MISS') - TO_DATE(A.COLEC_DT, 'YYYYMMDDHH24MISS')) * 60 * 60 * 24) 
		            , A.RFSH_PRD = 60 
		            -- [ 2015.05.13 yngwie OUTLET_PW 추가
		            , A.OUTLET_PW        = B.OUTLET_PW       
		            , A.OUTLET_PW_H      = B.OUTLET_PW_H       
		            , A.OUTLET_PW_PRICE  = B.OUTLET_PW_PRICE       
		            , A.OUTLET_PW_CO2    = B.OUTLET_PW_CO2       
		            , A.PWR_RANGE_CD_ESS_GRID    = FN_GET_PWR_RANGE_CD('ESS_GRID',  B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , A.PWR_RANGE_CD_GRID_ESS    = FN_GET_PWR_RANGE_CD('GRID_ESS',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , A.PWR_RANGE_CD_GRID_LOAD   = FN_GET_PWR_RANGE_CD('GRID_LOAD', B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , A.PWR_RANGE_CD_ESS_LOAD    = FN_GET_PWR_RANGE_CD('ESS_LOAD',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            , A.PWR_RANGE_CD_PV_ESS      = FN_GET_PWR_RANGE_CD('PV_ESS',    B.PV_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
		            -- ]
		            -- [ 2015.07.06 yngwie 수집 컬럼 추가
		            , A.PCS_COMM_ERR_RATE   = B.PCS_COMM_ERR_RATE 
		            , A.SMTR_COMM_ERR_RATE  = B.SMTR_COMM_ERR_RATE 
		            , A.M2M_COMM_ERR_RATE   = B.M2M_COMM_ERR_RATE       
		            , A.SMTR_TP_CD          = B.SMTR_TP_CD 
		            , A.SMTR_PULSE_CNT      = B.SMTR_PULSE_CNT 
		            , A.SMTR_MODL_CD        = B.SMTR_MODL_CD 
		            , A.PV_MAX_PWR1         = B.PV_MAX_PWR1 
		            , A.PV_MAX_PWR2         = B.PV_MAX_PWR2 
		            , A.INSTL_RGN           = B.INSTL_RGN 
		            , A.MMTR_SLV_ADDR       = B.MMTR_SLV_ADDR 
		            , A.BASICMODE_CD        = B.BASICMODE_CD 
		            , A.PCS_FLAG0       = B.PCS_FLAG0  
		            , A.PCS_FLAG1       = B.PCS_FLAG1  
		            , A.PCS_FLAG2       = B.PCS_FLAG2  
		            , A.PCS_OPMODE_CMD1         = B.PCS_OPMODE_CMD1  
		            , A.PCS_OPMODE_CMD2         = B.PCS_OPMODE_CMD2  
		            , A.PV_V1       = B.PV_V1  
		            , A.PV_I1       = B.PV_I1  
		            , A.PV_PW1      = B.PV_PW1  
		            , A.PV_V2       = B.PV_V2  
		            , A.PV_I2       = B.PV_I2  
		            , A.PV_PW2      = B.PV_PW2  
		            , A.INVERTER_V      = B.INVERTER_V  
		            , A.INVERTER_I      = B.INVERTER_I  
		            , A.INVERTER_PW         = B.INVERTER_PW  
		            , A.DC_LINK_V       = B.DC_LINK_V  
		            , A.G_RLY_CNT       = B.G_RLY_CNT  
		            , A.BAT_RLY_CNT         = B.BAT_RLY_CNT  
		            , A.GRID_TO_GRID_RLY_CNT        = B.GRID_TO_GRID_RLY_CNT  
		            , A.BAT_PCHRG_RLY_CNT           = B.BAT_PCHRG_RLY_CNT    
		            , A.BMS_FLAG0       = B.BMS_FLAG0  
		            , A.BMS_DIAG0       = B.BMS_DIAG0  
		            , A.RACK_V      = B.RACK_V  
		            , A.RACK_I      = B.RACK_I  
		            , A.CELL_MAX_V      = B.CELL_MAX_V  
		            , A.CELL_MIN_V      = B.CELL_MIN_V  
		            , A.CELL_AVG_V      = B.CELL_AVG_V  
		            , A.CELL_MAX_T      = B.CELL_MAX_T  
		            , A.CELL_MIN_T      = B.CELL_MIN_T  
		            , A.CELL_AVG_T      = B.CELL_AVG_T  
		            , A.TRAY_CNT        = B.TRAY_CNT 
		            , A.SYS_ST_CD       = B.SYS_ST_CD 
		            , A.BT_CHRG_ST_CD   = B.BT_CHRG_ST_CD 
		            , A.BT_DCHRG_ST_CD  = B.BT_DCHRG_ST_CD 
		            , A.PV_ST_CD        = B.PV_ST_CD 
		            , A.GRID_ST_CD      = B.GRID_ST_CD 
		            , A.SMTR_ST_CD      = B.SMTR_ST_CD 
		            , A.SMTR_TR_CNTER   = B.SMTR_TR_CNTER 
		            , A.SMTR_OB_CNTER   = B.SMTR_OB_CNTER 
		            , A.DN_ENABLE       = B.DN_ENABLE         
		            , A.DC_START        = B.DC_START         
		            , A.DC_END          = B.DC_END         
		            , A.NC_START        = B.NC_START         
		            , A.NC_END          = B.NC_END                             
		            -- ]
		            --[신규모델로 모니터링 추가
		            , A.GRID_CODE0 =             B.GRID_CODE0 
		            , A.GRID_CODE1 =             B.GRID_CODE1 
		            , A.GRID_CODE2 =             B.GRID_CODE2 
		            , A.PCON_BAT_TARGETPOWER =   B.PCON_BAT_TARGETPOWER 
		            , A.GRID_CODE3 =             B.GRID_CODE3 
		            , A.BDC_MODULE_CODE0 =       B.BDC_MODULE_CODE0 
		            , A.BDC_MODULE_CODE1 =       B.BDC_MODULE_CODE1 
		            , A.BDC_MODULE_CODE2 =       B.BDC_MODULE_CODE2 
		            , A.BDC_MODULE_CODE3 =       B.BDC_MODULE_CODE3 
		            , A.FAULT5_REALTIME_YEAR =   B.FAULT5_REALTIME_YEAR 
		            , A.FAULT5_REALTIME_MONTH =  B.FAULT5_REALTIME_MONTH 
		            , A.FAULT5_DAY =             B.FAULT5_DAY 
		            , A.FAULT5_HOUR =            B.FAULT5_HOUR 
		            , A.FAULT5_MINUTE =          B.FAULT5_MINUTE 
		            , A.FAULT5_SECOND =          B.FAULT5_SECOND 
		            , A.FAULT5_DATAHIGH =        B.FAULT5_DATAHIGH 
		            , A.FAULT5_DATALOW =         B.FAULT5_DATALOW 
		            , A.FAULT6_REALTIME_YEAR =   B.FAULT6_REALTIME_YEAR 
		            , A.FAULT6_REALTIME_MONTH =  B.FAULT6_REALTIME_MONTH 
		            , A.FAULT6_REALTIME_DAY =    B.FAULT6_REALTIME_DAY 
		            , A.FAULT6_REALTIME_HOUR =   B.FAULT6_REALTIME_HOUR 
		            , A.FAULT6_REALTIME_MINUTE = B.FAULT6_REALTIME_MINUTE 
		            , A.FAULT6_REALTIME_SECOND = B.FAULT6_REALTIME_SECOND 
		            , A.FAULT6_DATAHIGH =        B.FAULT6_DATAHIGH 
		            , A.FAULT6_DATALOW =         B.FAULT6_DATALOW 
		            , A.FAULT7_REALTIME_YEAR =   B.FAULT7_REALTIME_YEAR 
		            , A.FAULT7_REALTIME_MONTH =  B.FAULT7_REALTIME_MONTH 
		            , A.FAULT7_REALTIME_DAY =    B.FAULT7_REALTIME_DAY 
		            , A.FAULT7_REALTIME_HOUR =   B.FAULT7_REALTIME_HOUR 
		            , A.FAULT7_REALTIME_MINUTE = B.FAULT7_REALTIME_MINUTE 
		            , A.FAULT7_REALTIME_SECOND = B.FAULT7_REALTIME_SECOND 
		            , A.FAULT7_DATAHIGH =        B.FAULT7_DATAHIGH 
		            , A.FAULT7_DATALOW =         B.FAULT7_DATALOW 
		            , A.FAULT8_REALTIME_YEAR =   B.FAULT8_REALTIME_YEAR 
		            , A.FAULT8_REALTIME_MONTH =  B.FAULT8_REALTIME_MONTH 
		            , A.FAULT8_REALTIME_DAY =    B.FAULT8_REALTIME_DAY 
		            , A.FAULT8_REALTIME_HOUR =   B.FAULT8_REALTIME_HOUR 
		            , A.FAULT8_REALTIME_MINUTE = B.FAULT8_REALTIME_MINUTE 
		            , A.FAULT8_REALTIME_SECOND = B.FAULT8_REALTIME_SECOND 
		            , A.FAULT8_DATAHIGH =        B.FAULT8_DATAHIGH 
		            , A.FAULT8_DATALOW =         B.FAULT8_DATALOW 
		            , A.FAULT9_REALTIME_YEAR =   B.FAULT9_REALTIME_YEAR 
		            , A.FAULT9_REALTIME_MONTH =  B.FAULT9_REALTIME_MONTH 
		            , A.FAULT9_REALTIME_DAY =    B.FAULT9_REALTIME_DAY 
		            , A.FAULT9_REALTIME_HOUR =   B.FAULT9_REALTIME_HOUR 
		            , A.FAULT9_REALTIME_MINUTE = B.FAULT9_REALTIME_MINUTE 
		            , A.FAULT9_REALTIME_SECOND = B.FAULT9_REALTIME_SECOND 
		            , A.FAULT9_DATAHIGH =        B.FAULT9_DATAHIGH 
		            , A.FAULT9_DATALOW =         B.FAULT9_DATALOW 
		            --]        
	            WHERE 	A.DEVICE_ID = B.DEVICE_ID             
	            AND 		A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD   
	            
	            RETURNING A.*
	        )                           
            INSERT INTO TB_OPR_ESS_STATE ( 
	               DEVICE_ID                               
	              , DEVICE_TYPE_CD                          
	              , COLEC_DT                                
	              , OPER_STUS_CD                            
	              , PV_PW                                   
	              , PV_PW_H                                 
	              , PV_PW_PRICE                             
	              , PV_PW_CO2                               
	              , PV_STUS_CD                              
	              , CONS_PW                                 
	              , CONS_PW_H                               
	              , CONS_PW_PRICE                           
	              , CONS_PW_CO2                             
	              , BT_PW                                   
	              , BT_CHRG_PW_H                            
	              , BT_CHRG_PW_PRICE                        
	              , BT_CHRG_PW_CO2                          
	              , BT_DCHRG_PW_H                           
	              , BT_DCHRG_PW_PRICE                       
	              , BT_DCHRG_PW_CO2                         
	              , BT_SOC                                  
	              , BT_SOH                                  
	              , BT_STUS_CD                              
	              , GRID_PW                                 
	              , GRID_OB_PW_H                               
	              , GRID_OB_PW_PRICE                           
	              , GRID_OB_PW_CO2                             
	              , GRID_TR_PW_H                            
	              , GRID_TR_PW_PRICE                        
	              , GRID_TR_PW_CO2                          
	              , GRID_STUS_CD                            
	              , PCS_PW                                 
	              , PCS_FD_PW_H                                 
	              , PCS_PCH_PW_H                                 
	              , CREATE_DT                               
	              , EMS_OPMODE                               
	              , PCS_OPMODE1                               
	              , PCS_OPMODE2                               
	              , PCS_TGT_PW                               
	              , FEED_IN_LIMIT                               
	              , MAX_INVERTER_PW_CD                               
	            -- [ 2015.05.13 yngwie OUTLET_PW 추가
	              , OUTLET_PW                               
	              , OUTLET_PW_H                               
	              , OUTLET_PW_PRICE                               
	              , OUTLET_PW_CO2                               
	              , PWR_RANGE_CD_ESS_GRID 
	              , PWR_RANGE_CD_GRID_ESS 
	              , PWR_RANGE_CD_GRID_LOAD 
	              , PWR_RANGE_CD_ESS_LOAD 
	              , PWR_RANGE_CD_PV_ESS             
	            -- ]
	            -- [ 2015.07.06 yngwie 수집 컬럼 추가
	              , PCS_COMM_ERR_RATE  
	              , SMTR_COMM_ERR_RATE 
	              , M2M_COMM_ERR_RATE  
	              , SMTR_TP_CD          
	              , SMTR_PULSE_CNT      
	              , SMTR_MODL_CD    
	              , PV_MAX_PWR1     
	              , PV_MAX_PWR2     
	              , INSTL_RGN       
	              , MMTR_SLV_ADDR   
	              , BASICMODE_CD    
	              , PCS_FLAG0  
	              , PCS_FLAG1  
	              , PCS_FLAG2  
	              , PCS_OPMODE_CMD1  
	              , PCS_OPMODE_CMD2  
	              , PV_V1  
	              , PV_I1  
	              , PV_PW1  
	              , PV_V2  
	              , PV_I2  
	              , PV_PW2  
	              , INVERTER_V  
	              , INVERTER_I  
	              , INVERTER_PW  
	              , DC_LINK_V  
	              , G_RLY_CNT  
	              , BAT_RLY_CNT  
	              , GRID_TO_GRID_RLY_CNT  
	              , BAT_PCHRG_RLY_CNT   
	              , BMS_FLAG0  
	              , BMS_DIAG0  
	              , RACK_V  
	              , RACK_I  
	              , CELL_MAX_V  
	              , CELL_MIN_V  
	              , CELL_AVG_V  
	              , CELL_MAX_T  
	              , CELL_MIN_T  
	              , CELL_AVG_T  
	              , TRAY_CNT 
	              , SYS_ST_CD 
	              , BT_CHRG_ST_CD 
	              , BT_DCHRG_ST_CD 
	              , PV_ST_CD 
	              , GRID_ST_CD 
	              , SMTR_ST_CD 
	              , SMTR_TR_CNTER 
	              , SMTR_OB_CNTER 
	              , DN_ENABLE     
	              , DC_START     
	              , DC_END     
	              , NC_START     
	              , NC_END                                
	            -- ]            
	         --[신규모델로 모니터링 추가
	              , GRID_CODE0 
	              , GRID_CODE1 
	              , GRID_CODE2 
	              , PCON_BAT_TARGETPOWER 
	              , GRID_CODE3 
	              , BDC_MODULE_CODE0 
	              , BDC_MODULE_CODE1 
	              , BDC_MODULE_CODE2 
	              , BDC_MODULE_CODE3 
	              , FAULT5_REALTIME_YEAR 
	              , FAULT5_REALTIME_MONTH 
	              , FAULT5_DAY 
	              , FAULT5_HOUR 
	              , FAULT5_MINUTE 
	              , FAULT5_SECOND 
	              , FAULT5_DATAHIGH 
	              , FAULT5_DATALOW 
	              , FAULT6_REALTIME_YEAR 
	              , FAULT6_REALTIME_MONTH 
	              , FAULT6_REALTIME_DAY 
	              , FAULT6_REALTIME_HOUR 
	              , FAULT6_REALTIME_MINUTE 
	              , FAULT6_REALTIME_SECOND 
	              , FAULT6_DATAHIGH 
	              , FAULT6_DATALOW 
	              , FAULT7_REALTIME_YEAR 
	              , FAULT7_REALTIME_MONTH 
	              , FAULT7_REALTIME_DAY 
	              , FAULT7_REALTIME_HOUR 
	              , FAULT7_REALTIME_MINUTE 
	              , FAULT7_REALTIME_SECOND 
	              , FAULT7_DATAHIGH 
	              , FAULT7_DATALOW 
	              , FAULT8_REALTIME_YEAR 
	              , FAULT8_REALTIME_MONTH 
	              , FAULT8_REALTIME_DAY 
	              , FAULT8_REALTIME_HOUR 
	              , FAULT8_REALTIME_MINUTE 
	              , FAULT8_REALTIME_SECOND 
	              , FAULT8_DATAHIGH 
	              , FAULT8_DATALOW 
	              , FAULT9_REALTIME_YEAR 
	              , FAULT9_REALTIME_MONTH 
	              , FAULT9_REALTIME_DAY 
	              , FAULT9_REALTIME_HOUR 
	              , FAULT9_REALTIME_MINUTE 
	              , FAULT9_REALTIME_SECOND 
	              , FAULT9_DATAHIGH 
	              , FAULT9_DATALOW 
	            --]  
	            )
				SELECT                              
	               B.DEVICE_ID                               
	              , B.DEVICE_TYPE_CD                          
	              , B.COLEC_DT                                
	              , B.OPER_STUS_CD                            
	              , B.PV_PW                                   
	              , B.PV_PW_H                                 
	              , B.PV_PW_PRICE                             
	              , B.PV_PW_CO2                               
	              , B.PV_STUS_CD                              
	              , B.CONS_PW                                 
	              , B.CONS_PW_H                               
	              , B.CONS_PW_PRICE                           
	              , B.CONS_PW_CO2                             
	              , B.BT_PW                                   
	              , B.BT_CHRG_PW_H                            
	              , B.BT_CHRG_PW_PRICE                        
	              , B.BT_CHRG_PW_CO2                          
	              , B.BT_DCHRG_PW_H                           
	              , B.BT_DCHRG_PW_PRICE                       
	              , B.BT_DCHRG_PW_CO2                         
	              , B.BT_SOC                                  
	              , B.BT_SOH                                  
	              , B.BT_STUS_CD                              
	              , B.GRID_PW                                 
	              , B.GRID_OB_PW_H                               
	              , B.GRID_OB_PW_PRICE                           
	              , B.GRID_OB_PW_CO2                             
	              , B.GRID_TR_PW_H                            
	              , B.GRID_TR_PW_PRICE                        
	              , B.GRID_TR_PW_CO2                          
	              , B.GRID_STUS_CD                            
	              , B.PCS_PW                                 
	              , B.PCS_FD_PW_H                                 
	              , B.PCS_PCH_PW_H                                 
	              , COALESCE(B.CREATE_DT, SYS_EXTRACT_UTC(NOW()))                               
	              , B.EMS_OPMODE                               
	              , B.PCS_OPMODE1                               
	              , B.PCS_OPMODE2                               
	              , B.PCS_TGT_PW                               
	              , B.FEED_IN_LIMIT                               
	              , B.MAX_INVERTER_PW_CD                               
	            -- [ 2015.05.13 yngwie OUTLET_PW 추가
	              , B.OUTLET_PW                               
	              , B.OUTLET_PW_H                               
	              , B.OUTLET_PW_PRICE                               
	              , B.OUTLET_PW_CO2                               
	              , FN_GET_PWR_RANGE_CD('ESS_GRID',  B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('GRID_ESS',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('GRID_LOAD', B.GRID_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('ESS_LOAD',  B.PCS_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD) 
	              , FN_GET_PWR_RANGE_CD('PV_ESS',    B.PV_PW, B.PV_PW, B.PV_STUS_CD, B.GRID_STUS_CD, B.BT_STUS_CD)
	            -- ]
	            -- [ 2015.07.06 yngwie 수집 컬럼 추가
	              , B.PCS_COMM_ERR_RATE 
	              , B.SMTR_COMM_ERR_RATE 
	              , B.M2M_COMM_ERR_RATE      
	              , B.SMTR_TP_CD 
	              , B.SMTR_PULSE_CNT 
	              , B.SMTR_MODL_CD 
	              , B.PV_MAX_PWR1 
	              , B.PV_MAX_PWR2 
	              , B.INSTL_RGN 
	              , B.MMTR_SLV_ADDR 
	              , B.BASICMODE_CD 
	              , B.PCS_FLAG0  
	              , B.PCS_FLAG1  
	              , B.PCS_FLAG2  
	              , B.PCS_OPMODE_CMD1  
	              , B.PCS_OPMODE_CMD2  
	              , B.PV_V1  
	              , B.PV_I1  
	              , B.PV_PW1  
	              , B.PV_V2  
	              , B.PV_I2  
	              , B.PV_PW2  
	              , B.INVERTER_V  
	              , B.INVERTER_I  
	              , B.INVERTER_PW  
	              , B.DC_LINK_V  
	              , B.G_RLY_CNT  
	              , B.BAT_RLY_CNT  
	              , B.GRID_TO_GRID_RLY_CNT  
	              , B.BAT_PCHRG_RLY_CNT   
	              , B.BMS_FLAG0  
	              , B.BMS_DIAG0  
	              , B.RACK_V  
	              , B.RACK_I  
	              , B.CELL_MAX_V  
	              , B.CELL_MIN_V  
	              , B.CELL_AVG_V  
	              , B.CELL_MAX_T  
	              , B.CELL_MIN_T  
	              , B.CELL_AVG_T  
	              , B.TRAY_CNT          
	              , B.SYS_ST_CD 
	              , B.BT_CHRG_ST_CD 
	              , B.BT_DCHRG_ST_CD 
	              , B.PV_ST_CD 
	              , B.GRID_ST_CD 
	              , B.SMTR_ST_CD 
	              , B.SMTR_TR_CNTER 
	              , B.SMTR_OB_CNTER 
	              , B.DN_ENABLE 
	              , B.DC_START 
	              , B.DC_END 
	              , B.NC_START 
	              , B.NC_END                        
	            -- ]
	        --[신규모델로 모니터링 추가
	              , B.GRID_CODE0 
	              , B.GRID_CODE1 
	              , B.GRID_CODE2 
	              , B.PCON_BAT_TARGETPOWER 
	              , B.GRID_CODE3 
	              , B.BDC_MODULE_CODE0 
	              , B.BDC_MODULE_CODE1 
	              , B.BDC_MODULE_CODE2 
	              , B.BDC_MODULE_CODE3 
	              , B.FAULT5_REALTIME_YEAR 
	              , B.FAULT5_REALTIME_MONTH 
	              , B.FAULT5_DAY 
	              , B.FAULT5_HOUR 
	              , B.FAULT5_MINUTE 
	              , B.FAULT5_SECOND 
	              , B.FAULT5_DATAHIGH 
	              , B.FAULT5_DATALOW 
	              , B.FAULT6_REALTIME_YEAR 
	              , B.FAULT6_REALTIME_MONTH 
	              , B.FAULT6_REALTIME_DAY 
	              , B.FAULT6_REALTIME_HOUR 
	              , B.FAULT6_REALTIME_MINUTE 
	              , B.FAULT6_REALTIME_SECOND 
	              , B.FAULT6_DATAHIGH 
	              , B.FAULT6_DATALOW 
	              , B.FAULT7_REALTIME_YEAR 
	              , B.FAULT7_REALTIME_MONTH 
	              , B.FAULT7_REALTIME_DAY 
	              , B.FAULT7_REALTIME_HOUR 
	              , B.FAULT7_REALTIME_MINUTE 
	              , B.FAULT7_REALTIME_SECOND 
	              , B.FAULT7_DATAHIGH 
	              , B.FAULT7_DATALOW 
	              , B.FAULT8_REALTIME_YEAR 
	              , B.FAULT8_REALTIME_MONTH 
	              , B.FAULT8_REALTIME_DAY 
	              , B.FAULT8_REALTIME_HOUR 
	              , B.FAULT8_REALTIME_MINUTE 
	              , B.FAULT8_REALTIME_SECOND 
	              , B.FAULT8_DATAHIGH 
	              , B.FAULT8_DATALOW 
	              , B.FAULT9_REALTIME_YEAR 
	              , B.FAULT9_REALTIME_MONTH 
	              , B.FAULT9_REALTIME_DAY 
	              , B.FAULT9_REALTIME_HOUR 
	              , B.FAULT9_REALTIME_MINUTE 
	              , B.FAULT9_REALTIME_SECOND 
	              , B.FAULT9_DATAHIGH 
	              , B.FAULT9_DATALOW 
	            --]           
			FROM B
			WHERE NOT EXISTS (  
				SELECT 	1 
			    FROM 	U 
				WHERE	DEVICE_ID			= B.DEVICE_ID  
					AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
			  )
			 ;                 

           GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	       -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'STATE New Records ', v_work_num));
	      	RAISE notice 'v_log_fn[%] STATE New Records :  [%]건', v_log_fn, v_work_num;
           
            -- TEMP 테이블의 레코드를 이력테이블에 Insert
            INSERT INTO TB_OPR_ESS_STATE_HIST (
                 DEVICE_ID 
                , DEVICE_TYPE_CD 
                , COLEC_DT 
                , OPER_STUS_CD 
                , PV_PW 
                , PV_PW_H 
                , PV_PW_PRICE 
                , PV_PW_CO2 
                , PV_STUS_CD 
                , CONS_PW 
                , CONS_PW_H 
                , CONS_PW_PRICE 
                , CONS_PW_CO2 
                , BT_PW 
                , BT_CHRG_PW_H 
                , BT_CHRG_PW_PRICE 
                , BT_CHRG_PW_CO2 
                , BT_DCHRG_PW_H 
                , BT_DCHRG_PW_PRICE 
                , BT_DCHRG_PW_CO2 
                , BT_SOC 
                , BT_SOH 
                , BT_STUS_CD 
                , GRID_PW 
                , GRID_OB_PW_H 
                , GRID_OB_PW_PRICE 
                , GRID_OB_PW_CO2 
                , GRID_TR_PW_H 
                , GRID_TR_PW_PRICE 
                , GRID_TR_PW_CO2 
                , GRID_STUS_CD 
                , PCS_PW 
                , PCS_FD_PW_H 
                , PCS_PCH_PW_H 
                , EMS_OPMODE 
                , PCS_OPMODE1 
                , PCS_OPMODE2 
                , PCS_TGT_PW 
                , FEED_IN_LIMIT 
                , CREATE_DT 
                , MAX_INVERTER_PW_CD 
            -- [ 2015.05.13 yngwie OUTLET_PW 추가
                , OUTLET_PW 
                , OUTLET_PW_H 
                , OUTLET_PW_PRICE 
                , OUTLET_PW_CO2 
            -- ]
            -- [ 2015.07.06 yngwie 수집 컬럼 추가
                , PCS_COMM_ERR_RATE  
                , SMTR_COMM_ERR_RATE 
                , M2M_COMM_ERR_RATE  
                , SMTR_TP_CD 
                , SMTR_PULSE_CNT 
                , SMTR_MODL_CD 
                , PV_MAX_PWR1 
                , PV_MAX_PWR2 
                , INSTL_RGN 
                , MMTR_SLV_ADDR 
                , BASICMODE_CD 
              , PCS_FLAG0  
              , PCS_FLAG1  
              , PCS_FLAG2  
              , PCS_OPMODE_CMD1  
              , PCS_OPMODE_CMD2  
              , PV_V1  
              , PV_I1  
              , PV_PW1  
              , PV_V2  
              , PV_I2  
              , PV_PW2  
              , INVERTER_V  
              , INVERTER_I  
              , INVERTER_PW  
              , DC_LINK_V  
              , G_RLY_CNT  
              , BAT_RLY_CNT  
              , GRID_TO_GRID_RLY_CNT  
              , BAT_PCHRG_RLY_CNT    
              , BMS_FLAG0  
              , BMS_DIAG0  
              , RACK_V  
              , RACK_I  
              , CELL_MAX_V  
              , CELL_MIN_V  
              , CELL_AVG_V  
              , CELL_MAX_T  
              , CELL_MIN_T  
              , CELL_AVG_T  
              , TRAY_CNT   
              , SYS_ST_CD 
              , BT_CHRG_ST_CD 
              , BT_DCHRG_ST_CD 
              , PV_ST_CD 
              , GRID_ST_CD 
              , SMTR_ST_CD 
              , SMTR_TR_CNTER   
              , SMTR_OB_CNTER                                  
            -- ]         
  			--[신규모델로 모니터링 추가
	          , GRID_CODE0 
	          , GRID_CODE1 
	          , GRID_CODE2 
	          , PCON_BAT_TARGETPOWER 
	          , GRID_CODE3 
	          , BDC_MODULE_CODE0 
	          , BDC_MODULE_CODE1 
	          , BDC_MODULE_CODE2 
	          , BDC_MODULE_CODE3 
	          , FAULT5_REALTIME_YEAR 
	          , FAULT5_REALTIME_MONTH 
	          , FAULT5_DAY 
	          , FAULT5_HOUR 
	          , FAULT5_MINUTE 
	          , FAULT5_SECOND 
	          , FAULT5_DATAHIGH 
	          , FAULT5_DATALOW 
	          , FAULT6_REALTIME_YEAR 
	          , FAULT6_REALTIME_MONTH 
	          , FAULT6_REALTIME_DAY 
	          , FAULT6_REALTIME_HOUR 
	          , FAULT6_REALTIME_MINUTE 
	          , FAULT6_REALTIME_SECOND 
	          , FAULT6_DATAHIGH 
	          , FAULT6_DATALOW 
	          , FAULT7_REALTIME_YEAR 
	          , FAULT7_REALTIME_MONTH 
	          , FAULT7_REALTIME_DAY 
	          , FAULT7_REALTIME_HOUR 
	          , FAULT7_REALTIME_MINUTE 
	          , FAULT7_REALTIME_SECOND 
	          , FAULT7_DATAHIGH 
	          , FAULT7_DATALOW 
	          , FAULT8_REALTIME_YEAR 
	          , FAULT8_REALTIME_MONTH 
	          , FAULT8_REALTIME_DAY 
	          , FAULT8_REALTIME_HOUR 
	          , FAULT8_REALTIME_MINUTE 
	          , FAULT8_REALTIME_SECOND 
	          , FAULT8_DATAHIGH 
	          , FAULT8_DATALOW 
	          , FAULT9_REALTIME_YEAR 
	          , FAULT9_REALTIME_MONTH 
	          , FAULT9_REALTIME_DAY 
	          , FAULT9_REALTIME_HOUR 
	          , FAULT9_REALTIME_MINUTE 
	          , FAULT9_REALTIME_SECOND 
	          , FAULT9_DATAHIGH 
	          , FAULT9_DATALOW             
	          --]
            ) 
            SELECT 
                DEVICE_ID 
                , DEVICE_TYPE_CD 
                , COLEC_DT 
                , OPER_STUS_CD 
                , PV_PW 
                , PV_PW_H 
                , PV_PW_PRICE 
                , PV_PW_CO2 
                , PV_STUS_CD 
                , CONS_PW 
                , CONS_PW_H 
                , CONS_PW_PRICE 
                , CONS_PW_CO2 
                , BT_PW 
                , BT_CHRG_PW_H 
                , BT_CHRG_PW_PRICE 
                , BT_CHRG_PW_CO2 
                , BT_DCHRG_PW_H 
                , BT_DCHRG_PW_PRICE 
                , BT_DCHRG_PW_CO2 
                , BT_SOC 
                , BT_SOH 
                , BT_STUS_CD 
                , GRID_PW 
                , GRID_OB_PW_H 
                , GRID_OB_PW_PRICE 
                , GRID_OB_PW_CO2 
                , GRID_TR_PW_H 
                , GRID_TR_PW_PRICE 
                , GRID_TR_PW_CO2 
                , GRID_STUS_CD 
                , PCS_PW 
                , PCS_FD_PW_H 
                , PCS_PCH_PW_H 
                , EMS_OPMODE 
                , PCS_OPMODE1 
                , PCS_OPMODE2 
                , PCS_TGT_PW 
                , FEED_IN_LIMIT 
                , CREATE_DT 
                , MAX_INVERTER_PW_CD 
            -- [ 2015.05.13 yngwie OUTLET_PW 추가
                , OUTLET_PW 
                , OUTLET_PW_H 
                , OUTLET_PW_PRICE 
                , OUTLET_PW_CO2            
            -- ]
            -- [ 2015.07.06 yngwie 수집 컬럼 추가
                , PCS_COMM_ERR_RATE  
                , SMTR_COMM_ERR_RATE 
                , M2M_COMM_ERR_RATE  
                , SMTR_TP_CD         
                , SMTR_PULSE_CNT     
                , SMTR_MODL_CD   
                , PV_MAX_PWR1    
                , PV_MAX_PWR2    
                , INSTL_RGN          
                , MMTR_SLV_ADDR  
                , BASICMODE_CD
	            , PCS_FLAG0  
	            , PCS_FLAG1  
	            , PCS_FLAG2  
	            , PCS_OPMODE_CMD1  
	            , PCS_OPMODE_CMD2  
	            , PV_V1  
	            , PV_I1  
	            , PV_PW1  
	            , PV_V2  
	            , PV_I2  
	            , PV_PW2  
	            , INVERTER_V  
	            , INVERTER_I  
	            , INVERTER_PW  
	            , DC_LINK_V  
	            , G_RLY_CNT  
	            , BAT_RLY_CNT  
	            , GRID_TO_GRID_RLY_CNT  
	            , BAT_PCHRG_RLY_CNT    
	            , BMS_FLAG0  
	            , BMS_DIAG0  
	            , RACK_V  
	            , RACK_I  
	            , CELL_MAX_V  
	            , CELL_MIN_V  
	            , CELL_AVG_V  
	            , CELL_MAX_T  
	            , CELL_MIN_T  
	            , CELL_AVG_T  
	            , TRAY_CNT       
	            , SYS_ST_CD 
	            , BT_CHRG_ST_CD 
	            , BT_DCHRG_ST_CD 
	            , PV_ST_CD 
	            , GRID_ST_CD 
	            , SMTR_ST_CD  
	            , SMTR_TR_CNTER 
	            , SMTR_OB_CNTER                      
	            -- ]            
	 			--[신규모델로 모니터링 추가
	            , GRID_CODE0 
	            , GRID_CODE1 
	            , GRID_CODE2 
	            , PCON_BAT_TARGETPOWER 
	            , GRID_CODE3 
	            , BDC_MODULE_CODE0 
	            , BDC_MODULE_CODE1 
	            , BDC_MODULE_CODE2 
	            , BDC_MODULE_CODE3 
	            , FAULT5_REALTIME_YEAR 
	            , FAULT5_REALTIME_MONTH 
	            , FAULT5_DAY 
	            , FAULT5_HOUR 
	            , FAULT5_MINUTE 
	            , FAULT5_SECOND 
	            , FAULT5_DATAHIGH 
	            , FAULT5_DATALOW 
	            , FAULT6_REALTIME_YEAR 
	            , FAULT6_REALTIME_MONTH 
	            , FAULT6_REALTIME_DAY 
	            , FAULT6_REALTIME_HOUR 
	            , FAULT6_REALTIME_MINUTE 
	            , FAULT6_REALTIME_SECOND 
	            , FAULT6_DATAHIGH 
	            , FAULT6_DATALOW 
	            , FAULT7_REALTIME_YEAR 
	            , FAULT7_REALTIME_MONTH 
	            , FAULT7_REALTIME_DAY 
	            , FAULT7_REALTIME_HOUR 
	            , FAULT7_REALTIME_MINUTE 
	            , FAULT7_REALTIME_SECOND 
	            , FAULT7_DATAHIGH 
	            , FAULT7_DATALOW 
	            , FAULT8_REALTIME_YEAR 
	            , FAULT8_REALTIME_MONTH 
	            , FAULT8_REALTIME_DAY 
	            , FAULT8_REALTIME_HOUR 
	            , FAULT8_REALTIME_MINUTE 
	            , FAULT8_REALTIME_SECOND 
	            , FAULT8_DATAHIGH 
	            , FAULT8_DATALOW 
	            , FAULT9_REALTIME_YEAR 
	            , FAULT9_REALTIME_MONTH 
	            , FAULT9_REALTIME_DAY 
	            , FAULT9_REALTIME_HOUR 
	            , FAULT9_REALTIME_MINUTE 
	            , FAULT9_REALTIME_SECOND 
	            , FAULT9_DATAHIGH 
	            , FAULT9_DATALOW             
	            --]
            FROM 	TB_OPR_ESS_STATE_TMP 
            ;
            
           GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	       -- CALL wems.sp_log(v_log_seq, v_log_fn, CONCAT_WS('', 'Insert TB_OPR_ESS_STATE_HIST Table :  ', v_work_num));
	      	RAISE notice 'v_log_fn[%] Insert TB_OPR_ESS_STATE_HIST Table :  [%]건', v_log_fn, v_work_num;

        END IF;
       
    	RAISE NOTICE 'v_log_fn[%] SP End !!! ', v_log_fn;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$$;


ALTER PROCEDURE wems.sp_aggr_state_restore(p_device_id text, p_from_datetime text, p_to_datetime text) OWNER TO wems;

--
-- TOC entry 1105 (class 1255 OID 1895886)
-- Name: sp_check_ess_login_status(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_check_ess_login_status(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/* *****************************************************************************
   NAME:       SP_CHECK_ESS_LOGIN_STATUS
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-09-11   yngwie     	Created this function.
   2.0        2019-09-25   이재형		postgresql 변환   
   2.1        2019-12-12   박동환		성능개선   
   2.2        2020-01-23   박동환		성능개선(where문의 SYS_EXTRACT_UTC(now())를 상수화)
   2.3		  2020-02-21   김현덕		Offline 상태이지만 데이터가 올라오는 장비의 상태를 바꿔주는 부분의 오류수정
   
   NOTES:   TB_RAW_EMS_INFO 테이블의 수집상황을 가지고 ESS 의 LOGIN 여부를 판단해서 TB_RAW_LOGIN 테이블에 로그인 상태정보를 기록한다.
  				(입력시간 무관하게 호출된 시간 기준 처리)

	Input :

	Usage :	SELECT SP_CHECK_ESS_LOGIN_STATUS('SP_CHECK_ESS_LOGIN_STATUS', 0, '20191024123000', 'M')

******************************************************************************/

DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3
	
    -- (2)업무처리 변수
	v_cnt				INTEGER  	:= 0;     						-- 적재수
   
BEGIN
	
	-- 2분마다 실행
	IF SUBSTR(i_exe_base, 11, 2)::NUMERIC % 2 = 1 THEN 
		RETURN '1';	
	END IF;

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
  
		WITH CT AS (
	   	SELECT SYS_EXTRACT_UTC(now()) tm
	   ), A AS (
	   	SELECT *
	   	FROM (
		   	SELECT  	*, ROW_NUMBER() OVER (PARTITION BY DEVICE_ID ORDER BY CREATE_DT DESC, CREATE_TM DESC, DATA_SEQ DESC) SEQ  			    
				FROM   	TB_RAW_LOGIN   
				CROSS JOIN CT
			) Z
			WHERE SEQ = 1
	   ), norLogin AS(
			SELECT *
			FROM (
				SELECT  	*, ROW_NUMBER() OVER (PARTITION BY DEVICE_ID ORDER BY CREATE_DT DESC, CREATE_TM DESC, DATA_SEQ DESC) SEQ  			    
				FROM   	TB_RAW_LOGIN
				JOIN CT ON 1=1
				WHERE response_cd='0' and connector_id IS NOT NULL
			) Z
			WHERE SEQ = 1
	   ), B AS (
			SELECT 	 B.DEVICE_ID
						, DATE_PART('MINUTE', (A.tm - TO_TIMESTAMP(MAX(B.COLEC_DT), 'YYYYMMDDHH24MISS')) ) GAP_MIN  	    	
			FROM 	TB_RAW_EMS_INFO B
						, A
			WHERE 	B.COLEC_DT > TO_CHAR(A.tm - INTERVAL '1 hour', 'YYYYMMDDHH24MISS') 
			AND 		B.COLEC_DT <= TO_CHAR(A.tm, 'YYYYMMDDHH24MISS')
			AND		A.DEVICE_ID =  B.DEVICE_ID
			GROUP BY B.DEVICE_ID, A.tm
			
	   ), C AS (
	       SELECT   		
					CASE
						WHEN A.RESPONSE_CD = '0' AND B.GAP_MIN > 7 THEN 'ABNORMAL'    		     
						WHEN A.RESPONSE_CD = '128' AND B.GAP_MIN BETWEEN 0 AND 6 THEN 'NORMAL'   		
						ELSE NULL 
					END STATUS
					, A.*  	
		   FROM  A, B 
		   WHERE A.DEVICE_ID = B.DEVICE_ID
		   
	   ), sts as (
	   		SELECT  	STATUS, 
						SEQ, 
						DEVICE_ID, 
						CREATE_TM, 
						PRODUCT_MODEL_NM,
						PASSWORD  , 
						EMS_MODEL_NM, 
						PCS_MODEL_NM, 
						BMS_MODEL_NM, 
						EMS_VER , 
						PCS_VER , 
						BMS_VER , 
						EMS_UPDATE_DT, 
						EMS_UPDATE_ERR_CD, 
						PCS_UPDATE_DT, 
						PCS_UPDATE_ERR_CD, 
						BMS_UPDATE_DT, 
						BMS_UPDATE_ERR_CD, 
						RESPONSE_CD, 
						EMS_IPADDRESS, 
						EMS_OUT_IPADDRESS, 
						CONNECTOR_ID, 
						EMS_TM, 
						DATA_SEQ, 
						CREATE_DT,
						tm
		    FROM C
		    WHERE  	STATUS IN ('ABNORMAL','NORMAL')
		    
	   ), abn AS (
			 INSERT INTO TB_RAW_LOGIN ( 
			 	DEVICE_ID 
			 	, CREATE_TM 
			 	, PRODUCT_MODEL_NM 
			 	, RESPONSE_CD 
			 	, CREATE_DT  
			 	, DATA_SEQ 
			 	, EMS_IPADDRESS 
			 	, EMS_OUT_IPADDRESS 
			 	, EMS_TM  
			 	, EMS_MODEL_NM  
			 	, PCS_MODEL_NM  
			 	, BMS_MODEL_NM  
			 	, EMS_VER  
			 	, PCS_VER  
			 	, BMS_VER  
			 	, EMS_UPDATE_DT  
			 	, EMS_UPDATE_ERR_CD  
			 	, PCS_UPDATE_DT  
			 	, PCS_UPDATE_ERR_CD 
			 	, BMS_UPDATE_DT  
			 	, BMS_UPDATE_ERR_CD  
			 )
			 SELECT
			 	DEVICE_ID
			 	, TO_CHAR( tm, 'YYYYMMDDHH24MISS') AS CREATE_TM 
			 	, PRODUCT_MODEL_NM 
			 	, '128' AS RESPONSE_CD 
			 	, tm AS CREATE_DT  
			 	, nextval('SQ_RAW_LOGIN') AS DATA_SEQ 
			 	, 'BY SP_CHECK_ESS_LOGIN' AS EMS_IPADDRESS 
			 	, '127.0.0.1' AS EMS_OUT_IPADDRESS 
			 	, TO_CHAR(tm, 'YYYYMMDDHH24MISS') AS EMS_TM  
			 	, EMS_MODEL_NM  
			 	, PCS_MODEL_NM  
			 	, BMS_MODEL_NM  
			 	, EMS_VER  
			 	, PCS_VER  
			 	, BMS_VER  
			 	, EMS_UPDATE_DT  
			 	, EMS_UPDATE_ERR_CD  
			 	, PCS_UPDATE_DT  
			 	, PCS_UPDATE_ERR_CD  
			 	, BMS_UPDATE_DT  
			 	, BMS_UPDATE_ERR_CD  
			 FROM	 	sts 
			 WHERE	 	STATUS = 'ABNORMAL'
			 
			 RETURNING * 
	   ), nor AS (
	         INSERT INTO TB_RAW_LOGIN (  
			 	DEVICE_ID 
			 	, CREATE_TM 
			 	, PRODUCT_MODEL_NM 
			 	, PASSWORD 
			 	, EMS_MODEL_NM 
			 	, PCS_MODEL_NM 
			 	, BMS_MODEL_NM 
			 	, EMS_VER 
			 	, PCS_VER 
			 	, BMS_VER 
			 	, EMS_UPDATE_DT 
			 	, EMS_UPDATE_ERR_CD 
			 	, PCS_UPDATE_DT 
			 	, PCS_UPDATE_ERR_CD 
			 	, BMS_UPDATE_DT 
			 	, BMS_UPDATE_ERR_CD 
			 	, RESPONSE_CD 
			 	, EMS_IPADDRESS 
			 	, EMS_OUT_IPADDRESS 
			 	, CONNECTOR_ID 
			 	, EMS_TM 
			 	, DATA_SEQ 
			 ) 
			 SELECT 
			 	DEVICE_ID
			 	, TO_CHAR(tm, 'YYYYMMDDHH24MISS') AS CREATE_TM 
			 	, PRODUCT_MODEL_NM 
			 	, PASSWORD
			     , EMS_MODEL_NM
			 	, PCS_MODEL_NM 
			 	, BMS_MODEL_NM 
			 	, EMS_VER 
			 	, PCS_VER 
			 	, BMS_VER
			 	, EMS_UPDATE_DT
			 	, EMS_UPDATE_ERR_CD 
			 	, PCS_UPDATE_DT
			 	, PCS_UPDATE_ERR_CD 
			 	, BMS_UPDATE_DT
			 	, BMS_UPDATE_ERR_CD
			 	, '0' AS RESPONSE_CD
			     , 'BY SP_CHECK_ESS_LOGIN' AS EMS_IPADDRESS 
			 	, EMS_OUT_IPADDRESS 
			 	, CONNECTOR_ID
			 	, TO_CHAR(tm, 'YYYYMMDDHH24MISS') AS EMS_TM 
			 	, nextval('SQ_RAW_LOGIN') AS DATA_SEQ
			 FROM (
			 	SELECT	norLogin.* 
			 	FROM	norLogin, sts  
			 	WHERE  sts.STATUS = 'NORMAL' 
					AND sts.DEVICE_ID = norLogin.DEVICE_ID 
			 		AND norLogin.CREATE_TM > TO_CHAR(sts.tm - interval '7 day', 'YYYYMMDDHH24MISS') 
					AND norLogin.CREATE_TM <= TO_CHAR(sts.tm, 'YYYYMMDDHH24MISS')  
			 ) tmpA
			
			 RETURNING * 
		) 
		SELECT  count(CNT)
		FROM (
			SELECT count(*) CNT FROM abn
			UNION ALL 
			SELECT count(*) CNT FROM nor
			) Z
		INTO v_work_num;
	   
	
	     v_rslt_cnt := v_rslt_cnt + v_work_num;
	    
	    
	     -- 실행성공
	     v_rslt_cd := '1';
    
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_check_ess_login_status(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1106 (class 1255 OID 1895888)
-- Name: sp_check_lock_user(text, text); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_check_lock_user(p_userid text, INOUT p_ret text)
    LANGUAGE plpgsql
    AS $_$
/******************************************************************************
   NAME:       SP_CHECK_LOCK_USER
   PURPOSE:

   REVISIONS:
   Ver        Date            Author           Description
   ---------  ---------------  ---------------  ------------------------------------
   1.0        2015-04-27   yngwie       Created this function.
   2.0        2019-09-26   이재형        postgresql 변환
   
   NOTES:  사용자가 계정잠금 대상이면 계정을 잠그고 결과 반환

	Input : p_userId - 사용자 ID
            p_ret - 결과 (Y : 계정잠김, N : 계정 잠기지 않음)

	Usage :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
    v_log_fn 			VARCHAR := 'SP_CHECK_LOCK_USER';
    v_log_seq 		NUMERIC := 0;		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';	-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3
	
    p_dt 				VARCHAR   := ' ' ;
   
    -- (2)업무처리 변수
	v_flag 			CHARACTER VARYING(4);
    v_lockCount 	NUMERIC 	:= 5;	-- 계정잠금 최소 카운트
    v_cnt 				INTEGER 		:= 0;
BEGIN

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
    	p_ret := 'N';
   
		-- 0. 등록된 계정인지 확인한다.
	    SELECT 	COUNT(*) into v_cnt
	    FROM 	TB_BAS_USER 
	    WHERE 	USER_ID = ENC_VARCHAR_INS($1, 12, 'SDIENCK', 'TB_BAS_USER', 'USER_ID');
	
	    IF v_cnt > 0 THEN
	        -- 1. 사용자 정보에서 현재 계정잠금 상태인지 확인한다.
	    	SELECT COALESCE(DEC_VARCHAR_SEL(LOCK_FLAG, 10, 'SDIENCK', 'TB_BAS_USER', 'LOCK_FLAG'), 'N') into v_flag
	    	FROM TB_BAS_USER 
	     	WHERE USER_ID = ENC_VARCHAR_INS(p_userId, 12, 'SDIENCK', 'TB_BAS_USER', 'USER_ID');
	
	        -- 2. 현재  계정잠금 상태가 아니면 계정잠금 상황인지 체크한다. 
	        IF v_flag <> 'Y' THEN
	             SELECT COUNT(*) AS CNT 
	             into v_cnt
	             FROM TB_SYS_SEC_HIST A 
	             WHERE  A.USER_ID = ENC_VARCHAR_INS(p_userId, 12, 'SDIENCK', 'TB_SYS_SEC_HIST', 'USER_ID') 
	             AND A.REF_URL = '/login.do' 
	             AND A.CREATE_DT > ( 
				            			SELECT MAX(UNLOCK_DT) 
				            			FROM ( 
				            				SELECT LAST_UNLOCK_DT AS UNLOCK_DT 
				            			 	FROM TB_BAS_USER 
											WHERE USER_ID = ENC_VARCHAR_INS(p_userId, 12, 'SDIENCK', 'TB_BAS_USER', 'USER_ID') 			
											UNION ALL 
											SELECT  MAX(LOGIN_DT) AS UNLOCK_DT 
											FROM TB_SYS_LOGIN_HIST 
											WHERE  USER_ID = ENC_VARCHAR_INS(p_userId, 12, 'SDIENCK', 'TB_BAS_USER', 'USER_ID') 	
										) a
								)  ;
	
	           
	            -- 건수가 v_lockCount 회 이상이면 잠금 처리
	            IF v_cnt >= v_lockCount then
	                UPDATE TB_BAS_USER   SET LOCK_FLAG = ENC_VARCHAR_INS('Y', 12, 'SDIENCK', 'TB_BAS_USER', 'LOCK_FLAG') 
	               			, LOCK_START_DT = SYS_EXTRACT_UTC(now()) 
	               			, UPDATE_DT = SYS_EXTRACT_UTC(now())
	               			, UPDATE_ID =  ENC_VARCHAR_INS('SYSTEM', 12, 'SDIENCK', 'TB_BAS_USER', 'UPDATE_ID') 
	               	WHERE  USER_ID = ENC_VARCHAR_INS(p_userId, 12, 'SDIENCK', 'TB_BAS_USER', 'USER_ID');
	
	                p_ret := 'Y';
	               
	            END IF;
	
	        ELSE
	            p_ret := 'Y';
	        END IF;
	    END IF;
   
	    GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	    v_rslt_cnt := v_rslt_cnt + v_work_num;
	    
	    -- 실행성공
	    v_rslt_cd := '1';
    
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
END;
$_$;


ALTER PROCEDURE wems.sp_check_lock_user(p_userid text, INOUT p_ret text) OWNER TO wems;

--
-- TOC entry 1107 (class 1255 OID 1895890)
-- Name: sp_crt_dmmy_data(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_crt_dmmy_data(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$

/******************************************************************************
   NAME:       SP_CRT_DMMY_DATA
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2019-10-02   이재형   Created this function.
   1.1        2019-10-11   이재형   파라미터에 따라 해당 날짜 만들도록 수정
   
   NOTES:   제공 받은 2일 raw 데이터를 매일 1회 익일 데이터로 복사해 생성한다.

	Input : i_exe_base 기준 날짜

	Usage : SELECT SP_CRT_DMMY_DATA('SP_CRT_DMMY_DATA', 0, '20191024', 'M')


******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3
	
    -- (2)업무처리 변수
	v_diff				NUMERIC   := 0;
    v_cnt           	NUMERIC   := 0;
    v_sql           	TEXT;
   
BEGIN
	-- Job에서 자동실행 시에
	IF i_exe_mthd = 'A' THEN
		i_exe_base := TO_CHAR(TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD')+INTERVAL '2 DAY', 'YYYYMMDD');
	ELSE
		i_exe_base := SUBSTR(i_exe_base, 1, 8);
	END IF;

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN  
		v_rslt_cd := 'A';
			
		-- diff day
		SELECT 	DATE_PART('day',TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000', 'YYYYMMDDHH24MISS')-TO_TIMESTAMP('20190828000000','YYYYMMDDHH24MISS')) 
		INTO		v_diff;
	   
		/*
		v_sql := FORMAT('TRUNCATE TABLE PT_RAW_BMS_INFO_%s', SUBSTR(i_exe_base, 1, 8));
		EXECUTE v_sql;
		
		v_sql := FORMAT('TRUNCATE TABLE PT_RAW_PCS_INFO_%s', SUBSTR(i_exe_base, 1, 8));
		EXECUTE v_sql;

		v_sql := FORMAT('TRUNCATE TABLE PT_RAW_EMS_INFO_%s', SUBSTR(i_exe_base, 1, 8));
		EXECUTE v_sql;
		*/

		v_rslt_cd := 'B';
		
		DELETE FROM TB_RAW_BMS_INFO
		WHERE 	CREATE_DT >=TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000 09', 'YYYYMMDDHH24MISS TZH')
		AND 		CREATE_DT <  TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000 09', 'YYYYMMDDHH24MISS TZH')+INTERVAL '1 DAY';
	
		INSERT INTO TB_RAW_BMS_INFO
		SELECT DISTINCT device_id,
				TO_CHAR(TO_TIMESTAMP(colec_dt,'YYYYMMDDHH24MISS')+(v_diff||'day')::INTERVAL,'YYYYMMDDHH24MISS') as colec_dt, 
				NULL as done_flag, 
				col0, col1, col2, col3, col4, col5, col6, col7, 
				col8, col9, col10, col11, col12, col13, col14, 
				col15, col16, col17, col18, col19, col20, col21, 
				col22, col23, col24, col25, col26, col27, col28, col29, 
				col30, col31, col32, col33, col34, col35, col36, col37, 
				col38, col39, col40, 
				create_dt+(v_diff||'day')::INTERVAL as create_dt, NULL as handle_flag
		FROM 	TB_RAW_BMS_INFO
		WHERE 	CREATE_DT >=TO_TIMESTAMP('20190828000000 09', 'YYYYMMDDHH24MISS TZH')
		AND 		CREATE_DT <  TO_TIMESTAMP('20190829000000 09', 'YYYYMMDDHH24MISS TZH');

		
		GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	    v_rslt_cnt := v_rslt_cnt + v_work_num;

	    v_rslt_cd := 'C';

	   	DELETE FROM TB_RAW_PCS_INFO
		WHERE 	CREATE_DT >=TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000 09', 'YYYYMMDDHH24MISS TZH')
		AND 		CREATE_DT <  TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000 09', 'YYYYMMDDHH24MISS TZH')+INTERVAL '1 DAY';
		
		INSERT INTO TB_RAW_PCS_INFO
		SELECT 	DISTINCT device_id, 
					to_char(TO_TIMESTAMP(colec_dt,'YYYYMMDDHH24MISS')+(v_diff||'day')::INTERVAL,'YYYYMMDDHH24MISS') as COLEC_DT,
					NULL as done_flag, 
					col0, col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, 
					col15, col16, col17, col18, col19, col20, col21, col22, col23, col24, col25, col26, col27, col28, col29, col30, col31, col32, 
					col33, col34, col35, col36, col37, col38, col39, col40, col41, col42, col43, col44, col45, col46, col47, col48, col49, col50, 
					col51, col52, col53, col54, col55, col56, col57, col58, col59, col60, col61, col62, col63, col64, col65, col66, col67, col68, 
					col69, col70, col71, col72, col73, col74, col75, col76, col77, col78, col79, col80, col81, col82, col83, col84, col85, col86, 
					col87, col88, col89, col90, col91, col92, col93, col94, col95, col96, col97, col98, col99, col100, col101, col102, col103, col104, 
					col105, col106, col107, col108, col109, col110, col111, col112, col113, col114, col115, col116, col117, col118, col119, col120, 
					col121, col122, col123, col124, col125, col126, col127, col128, col129, col130, col131, col132, col133, col134, col135, col136, 
					col137, col138, col139, col140, col141, col142, col143, col144, col145, col146, col147, col148, col149, col150, col151, col152, 
					col153, col154, col155, col156, col157, col158, col159, col160, col161, col162, col163, col164, col165, col166, col167, col168, 
					col169, col170, col171, col172, col173, col174, col175, col176, col177, col178, col179, col180, col181, col182, col183, col184, 
					col185, col186, col187, col188, col189, col190, col191, col192, col193, col194, col195, col196, col197, col198, col199, col200, 
					col201, col202, col203, col204, col205, col206, col207, col208, col209, col210, col211, col212, col213, col214, col215, col216, 
					col217, col218, col219, col220, col221, col222, col223, col224, col225, col226, 
					create_dt+(v_diff||'day')::INTERVAL as create_dt, NULL as handle_flag
			FROM TB_RAW_PCS_INFO
		WHERE 	CREATE_DT >=TO_TIMESTAMP('20190828000000 09', 'YYYYMMDDHH24MISS TZH')
		AND 		CREATE_DT <  TO_TIMESTAMP('20190829000000 09', 'YYYYMMDDHH24MISS TZH');
		
		GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	     v_rslt_cnt := v_rslt_cnt + v_work_num;

	    v_rslt_cd := 'D';

   	   	DELETE FROM TB_RAW_EMS_INFO
		WHERE 	CREATE_DT >=TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000 09', 'YYYYMMDDHH24MISS TZH')
		AND 		CREATE_DT <  TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8)||'000000 09', 'YYYYMMDDHH24MISS TZH')+INTERVAL '1 DAY';


		INSERT INTO TB_RAW_EMS_INFO
		SELECT DISTINCT device_id, 
				TO_CHAR(TO_TIMESTAMP(colec_dt,'YYYYMMDDHH24MISS')+(v_diff||'day')::INTERVAL,'YYYYMMDDHH24MISS') as colec_dt, 
				NULL as done_flag, 
				col0, col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16, col17, col18, col19, col20, 
				col21, col22, col23, col24, col25, col26, col27, col28, col29, col30, col31, col32, col33, col34, col35, col36, col37, col38, col39, col40, 
				col41, col42, col43, col44, col45, col46, col47, col48, col49, col50, col51, col52, col53, col54, col55, col56, col57, col58, col59, col60, 
				col61, col62, col63, col64, col65, col66, col67, col68, col69, col70, col71, col72, col73, col74, col75, col76, col77, col78, col79, col80,
				col81, col82, col83, col84, col85, col86, col87, col88, col89, col90, col91, col92, col93, col94, col95, col96, col97, col98, col99, col100,
				col101, col102, col103, col104, col105, col106, col107, col108, col109, col110, col111, col112, col113, col114, col115, col116, col117, col118,
				col119, col120, col121, col122, col123, col124, col125, col126, col127, col128, col129, col130, col131, col132, col133, col134, col135, col136,
				col137, col138, col139, col140, col141, col142, col143, col144, col145, col146, col147, col148, col149, col150, col151, col152, col153, col154,
				col155, col156, col157, col158, col159, col160, col161, col162, col163, col164, col165, col166, col167, col168, col169, col170, col171, col172,
				col173, col174, col175, col176, col177, col178, col179, col180, col181, col182, col183, col184, col185, col186, col187, col188, col189, col190,
				col191, col192, col193, col194, col195, col196, col197, col198, col199, col200, col201, col202, col203, col204, col205, col206, col207, col208,
				col209, col210, col211, col212, col213, col214, col215, col216, col217, col218, col219, col220, col221, col222, col223, col224, col225, col226,
				col227, col228, col229, col230, col231, col232, col233, col234, col235, col236, col237, col238, col239, col240, col241, col242, col243, col244,
				col245, col246, col247, col248, col249, col250, col251, col252, col253, col254, col255, col256, col257, col258, col259, col260, col261, col262,
				col263, col264, col265, col266, col267, col268, col269, col270, col271, col272, col273, col274, col275, col276, col277, col278, col279, col280,
				col281, col282, col283, col284, col285, col286, col287, col288, col289, col290, col291, col292, col293, col294, col295, col296, col297, col298,
				col299, col300, col301, col302, col303, col304, col305, col306, col307, col308, col309, col310, col311, col312, col313, col314, col315, col316,
				col317, col318, col319, col320, col321, col322, col323, col324, col325, col326, col327, col328, col329, col330, col331, col332, col333, col334,
				col335, col336, col337, col338, col339, col340, col341, 
				create_dt+(v_diff||'day')::INTERVAL as create_dt, NULL as handle_flag
		FROM 	TB_RAW_EMS_INFO
		WHERE 	CREATE_DT >=TO_TIMESTAMP('20190828000000 09', 'YYYYMMDDHH24MISS TZH')
		AND 		CREATE_DT <  TO_TIMESTAMP('20190829000000 09', 'YYYYMMDDHH24MISS TZH');

		GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	     v_rslt_cnt := v_rslt_cnt + v_work_num;
	    
	    
	    -- TB_RAW_LOGIN 월단위처리
	    
	      -- 실행성공
	     v_rslt_cd := '1';
    
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_crt_dmmy_data(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1154 (class 1255 OID 1895892)
-- Name: sp_current_dt_test(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_current_dt_test(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$

/******************************************************************************
   NAME:       sp_current_dt_test
   PURPOSE:

   REVISIONS:
   Ver        Date        	Author        Description
   ---------  ---------------  ---------------  -----------------------------------
   1.0        2019-10-24   박동환    	최초작성
   
   NOTES:   배치수행 시에 조회되는 로컬시간과 UTC시간을 테스트 테이블에 저장

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3
	
BEGIN
	
    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
		
 		INSERT INTO TB_TEST ( TEST_NM, TEST_VAL1, TEST_VAL2 )
 		SELECT 	'RDS Time'
 					, 'UTC : '||TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS')
 					, 'Local : '||TO_CHAR(now(), 'YYYY-MM-DD HH24:MI:SS');
 
		GET DIAGNOSTICS v_work_num = ROW_COUNT;
	

		--실행성공
     	v_rslt_cd := '1';
    
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );                           
             
   	return v_rslt_cd;
   
END;
$$;


ALTER FUNCTION wems.sp_current_dt_test(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1108 (class 1255 OID 1895893)
-- Name: sp_del_batch_job_data(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_del_batch_job_data(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exec_mthd character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$

/******************************************************************************
  프로그램ID   : SP_DEL_BATCH_JOB_DATA
  작  성  일   : 2019-10-29
  작  성  자   : 박동환
  프로그램용도 : Spring Batch 작업 테이블들에 불필요한 데이타 정리
  참 고 사 항  :
                 < PROTOTYPE >
                   sp_del_batch_job_data

                 <PARAMETER>
                     1.i_sp_nm		프로시져명
                     2.i_site_seq	사이트일련번호
                     3.i_exec_base	실행기준값(시간대별:YYYYMMDDHH0000 )
                     4.i_exec_mthd	실행사유모드 (A:자동, M:수동)


  ----------------------------------------------------------------------------
  TABLE                                                                CRUD
  ============================================================================
  BATCH_STEP_EXECUTION_CONTEXT							D
  BATCH_STEP_EXECUTION										DR
  BATCH_JOB_EXECUTION_CONTEXT							D
  BATCH_JOB_EXECUTION_PARAMS							D
  BATCH_JOB_EXECUTION										DR
  BATCH_JOB_INSTANCE                                          D            
  ----------------------------------------------------------------------------

  ----------------------------------------------------------------------------
  수정일자       수정자      수정내용
  =========== =========== ====================================================
  20191029    박동환      최초작성
  ----------------------------------------------------------------------------

*****************************************************************************/
-- 실행 스크립트
-- SELECT SP_DEL_BATCH_JOB_DATA('SP_DEL_BATCH_JOB_DATA', 0,  '20181201', 'M');

DECLARE
    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
    v_st_dt                 	TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd          		VARCHAR   	:= '0';   			-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt              	INTEGER   	:= 0;     			-- 적재수
    v_err_msg      			VARCHAR   	:= NULL;  		-- SQL오류메시지내용
    v_err_msg1              VARCHAR   	:= NULL;  		-- 메시지내용1
    v_err_msg2              VARCHAR   	:= NULL;  		-- 메시지내용2
    v_err_msg3              VARCHAR   	:= NULL;  		-- 메시지내용3

   -- (2)업무처리 변수
    v_work_cnt              INTEGER   := 0;     -- 처리건수

BEGIN
    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                                   , i_site_seq
                                   , i_exe_base
                                   , v_st_dt
                                   , i_exec_mthd
                                   );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
        v_rslt_cd := 'A';

		DELETE FROM BATCH_STEP_EXECUTION_CONTEXT WHERE STEP_EXECUTION_ID 
			IN (SELECT STEP_EXECUTION_ID FROM BATCH_STEP_EXECUTION WHERE START_TIME < TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD') );
	
		DELETE FROM BATCH_STEP_EXECUTION WHERE START_TIME < TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD');
	
		DELETE FROM BATCH_JOB_EXECUTION_CONTEXT WHERE JOB_EXECUTION_ID 
			IN (SELECT JOB_EXECUTION_ID FROM BATCH_JOB_EXECUTION WHERE CREATE_TIME < TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD') );
	
		DELETE FROM BATCH_JOB_EXECUTION_PARAMS WHERE JOB_EXECUTION_ID 
			IN (SELECT JOB_EXECUTION_ID FROM BATCH_JOB_EXECUTION WHERE CREATE_TIME < TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD') );
	
		DELETE FROM BATCH_JOB_EXECUTION WHERE CREATE_TIME < TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD');
	
		DELETE FROM BATCH_JOB_INSTANCE WHERE JOB_INSTANCE_ID NOT IN (SELECT JOB_INSTANCE_ID FROM BATCH_JOB_EXECUTION);
	
		-- [배치수행이력] 테이블 정리 
		DELETE FROM TB_SYS_SP_EXE_HST WHERE ST_DT < TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD') - INTERVAL '20 DAY';
		
		-- [M2M서버 접속이력] 테이블 정리 (1일 10만건)
		DELETE FROM TB_RAW_LOGIN WHERE CREATE_DT < TO_TIMESTAMP(SUBSTR(i_exe_base, 1, 8), 'YYYYMMDD000000') - INTERVAL '7 DAY';

		GET DIAGNOSTICS v_work_cnt = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_cnt;
        
        /* 실행성공 */
        v_rslt_cd := '1';
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;
        
        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
                               
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                                    , i_site_seq
                                    , i_exe_base
                                    , v_st_dt
                                    , v_rslt_cd
                                    , v_rslt_cnt
                                    , v_err_msg
                                    );
END;
	
$$;


ALTER FUNCTION wems.sp_del_batch_job_data(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exec_mthd character varying) OWNER TO wems;

--
-- TOC entry 1155 (class 1255 OID 1895895)
-- Name: sp_fw_update_result(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_fw_update_result(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_FW_UPDATE_RESULT
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-08-20   yngwie       Created this function.
   2.0        2019-10-02   박동환        Postgresql 변환

   NOTES:   펌웨어 업데이트 명령 처리
  				(입력시간 무관하게 호출된 시간 기준 처리)
   
    Input : 
	        
	Output : 
	
	Usage : SELECT SP_FW_UPDATE_RESULT('SP_FW_UPDATE_RESULT', 0, '201910221237', 'M');

*******************************************************************************/	
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_row           			record; 
   
	v_ems_update_err_cd	VARCHAR   := '';  
	v_pcs_update_err_cd   VARCHAR   := '';  
	v_bms_update_err_cd  VARCHAR   := '';  
	v_ems_model_nm      VARCHAR   := '';  
	v_bms_model_nm      VARCHAR   := '';  
	v_pcs_model_nm       VARCHAR   := '';  
	v_ems_ver               VARCHAR   := '';  
	v_bms_ver               VARCHAR   := '';  
	v_pcs_ver                	VARCHAR   := '';  
	v_ems_update_dt       VARCHAR   := '';  
	v_device_id       		VARCHAR   := '';  

	v_result       			VARCHAR   := '';  
	v_ems_cd       			VARCHAR   := '';   
	v_pcs_cd       			VARCHAR   := '';   
	v_bms_cd       			VARCHAR   := '';  
    v_update_cnt1			INTEGER   := 0;     						-- 적재수
    v_update_cnt2			INTEGER   := 0;     						-- 적재수
  
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
  
		v_rslt_cd := 'A';

	    --RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] i_exe_base[%] START[%]', i_sp_nm, i_site_seq, i_exe_base, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	   	FOR v_row IN ( 
			SELECT
				A.REQ_ID
				, A.DEVICE_ID
				, A.CREATE_DT
				, B.RESULT_CD
			FROM
				TB_PLF_DEVICE_FW_HIST A
				, TB_OPR_CTRL_RESULT B
			WHERE
				A.EMS_UPDATE_ERR_CD IS NULL
				AND A.PCS_UPDATE_ERR_CD IS NULL
				AND A.BMS_UPDATE_ERR_CD IS NULL
				AND A.REQ_ID = B.REQ_ID
				AND A.DEVICE_ID = B.DEVICE_ID
				AND B.REQ_TYPE_CD = 'M2M'
		) LOOP
			-- 푸시 업데이트 응답 처리
            IF v_row.RESULT_CD = '0' THEN 
                -- TB_RAW_LOGIN 테이블에서 대상 레코드보다 CREATE_DT 가 이후인 건들 중 가장 최근인 건을 가져온다.
                v_device_id := ''; 

				SELECT /*+ INDEX_DESC(A PK_RAW_LOGIN) */
							  EMS_UPDATE_ERR_CD
							, PCS_UPDATE_ERR_CD
							, BMS_UPDATE_ERR_CD
							, EMS_MODEL_NM
							, BMS_MODEL_NM
							, PCS_MODEL_NM
							, EMS_VER
							, BMS_VER
							, PCS_VER
							, EMS_UPDATE_DT
							, DEVICE_ID
				INTO 		  v_ems_update_err_cd
							, v_pcs_update_err_cd
							, v_bms_update_err_cd
							, v_ems_model_nm
							, v_bms_model_nm
							, v_pcs_model_nm
							, v_ems_ver
							, v_bms_ver
							, v_pcs_ver
							, v_ems_update_dt
							, v_device_id
				FROM		TB_RAW_LOGIN A
				WHERE	DEVICE_ID = v_row.DEVICE_ID
				AND 		RESPONSE_CD = '0'
				AND 		CREATE_DT > v_row.CREATE_DT
				ORDER BY CREATE_DT DESC
				LIMIT 1;
				
                --RAISE NOTICE 'i_sp_nm[%] select TB_RAW_LOGIN [%]건>>> REQ_ID{%] DEVICE_ID[%] EMS_UPDATE_ERR_CD[%] PCS_UPDATE_ERR_CD[%] BMS_UPDATE_ERR_CD[%]'
				--				, i_sp_nm, v_work_num, v_row.REQ_ID, v_row.DEVICE_ID, v_ems_update_err_cd, v_pcs_update_err_cd, v_bms_update_err_cd;
								
				-- 위 조회결과 레코드가 1건이 있으면 TB_PLF_DEVICE_FW_HIST 테이블의 해당 레코드에 업데이트 결과를 쓴다.
				-- 결과가 없으면 SKIP
				IF v_device_id != '' THEN
		            -- 	2015.07.22 yngwie          	CMD_REQ_STATUS_CD 에 진행상태코드 update
					UPDATE TB_PLF_DEVICE_FW_HIST
					SET EMS_UPDATE_ERR_CD = v_ems_update_err_cd
					    , PCS_UPDATE_ERR_CD = v_pcs_update_err_cd
					    , BMS_UPDATE_ERR_CD = v_bms_update_err_cd
					    , UPDATE_ID = ENC_VARCHAR_INS('SYSTEM', 11, 'SDIENCK', 'UPDATE_ID', 'TB_PLF_DEVICE_FW_HIST')
					    , UPDATE_DT = SYS_EXTRACT_UTC(NOW())
					    , CMD_REQ_STATUS_CD = '51'
					WHERE 	REQ_ID = v_row.REQ_ID
					    AND 	DEVICE_ID = v_row.DEVICE_ID;
					   
		           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
			       	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
			       	v_update_cnt1 := v_update_cnt1 + v_work_num;

	                --RAISE NOTICE 'i_sp_nm[%] update TB_PLF_DEVICE_FW_HIST [%]건>>> REQ_ID{%] DEVICE_ID[%] EMS_UPDATE_ERR_CD[%] PCS_UPDATE_ERR_CD[%] BMS_UPDATE_ERR_CD[%]'
					--				, i_sp_nm, v_work_num, v_row.REQ_ID, v_row.DEVICE_ID, v_ems_update_err_cd, v_pcs_update_err_cd, v_bms_update_err_cd;
	                
	                -- 장비 테이블에 모델정보와 버전을 업데이트한다.
					UPDATE TB_BAS_DEVICE
					SET EMS_MODEL_NM = v_ems_model_nm
					    , PCS_MODEL_NM = v_pcs_model_nm
					    , BMS_MODEL_NM = v_bms_model_nm
					    , EMS_VER = v_ems_ver
					    , PCS_VER = v_pcs_ver
					    , BMS_VER = v_bms_ver
						, EMS_UPDATE_DT = v_ems_update_dt
					    , UPDATE_ID = ENC_VARCHAR_INS('SYSTEM', 11, 'SDIENCK', 'UPDATE_ID', 'TB_BAS_DEVICE')
					    , UPDATE_DT = SYS_EXTRACT_UTC(NOW())						    
	                WHERE	DEVICE_ID = v_row.DEVICE_ID;
	               
		           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
			       	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
			       	v_update_cnt2 := v_update_cnt2 + v_work_num;

	                --RAISE NOTICE 'i_sp_nm[%] update TB_BAS_DEVICE [%]건>>> DEVICE_ID[%] EMS_MODEL_NM[%] PCS_MODEL_NM[%] BMS_MODEL_NM[%]'
					--				, i_sp_nm, v_work_num, v_row.DEVICE_ID, v_ems_model_nm, v_pcs_model_nm, v_bms_model_nm;

                    -- 명령 전송 이후 TB_RAW_LOGIN 테이블에 로그인한 결과가 없으면 SKIP
            	END IF;
            
            -- 명령 전송 결과가 실패이면
            -- 결과 코드를 TB_PLF_DEVICE_FW_HIST 에 기록한다.
            -- 	2015.07.22 yngwie,  	CMD_REQ_STATUS_CD 에 진행상태코드 update
            ELSE
                v_result := FN_GET_PUSH_UPDATE_RESULT( v_row.RESULT_CD );
                v_ems_cd := FN_GET_SPLIT(v_result, 1, '|');
                v_pcs_cd := FN_GET_SPLIT(v_result, 2, '|');
                v_bms_cd := FN_GET_SPLIT(v_result, 3, '|');
                
               --RAISE NOTICE 'i_sp_nm[%] Fail! REQ_ID[%] DEVICE_ID[%]', i_sp_nm, v_row.REQ_ID, v_row.DEVICE_ID;

				UPDATE TB_PLF_DEVICE_FW_HIST
				SET EMS_UPDATE_ERR_CD = v_ems_cd
				    , PCS_UPDATE_ERR_CD = v_pcs_cd
				    , BMS_UPDATE_ERR_CD = v_bms_cd
				    , UPDATE_ID = ENC_VARCHAR_INS('SYSTEM', 11, 'SDIENCK', 'UPDATE_ID', 'TB_PLF_DEVICE_FW_HIST')
				    , UPDATE_DT = SYS_EXTRACT_UTC(NOW())
				    , CMD_REQ_STATUS_CD = '55'
				WHERE 	REQ_ID = v_row.REQ_ID
				    AND 	DEVICE_ID = v_row.DEVICE_ID;				   
				   
	           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		       	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		       	v_update_cnt1 := v_update_cnt1 + v_work_num;
		       
            END IF;
           
	   END LOOP;
		
       --RAISE NOTICE 'i_sp_nm[%] Update TB_PLF_DEVICE_FW_HIST[%]건, TB_BAS_DEVICE[%]건', i_sp_nm, v_update_cnt1, v_update_cnt2;
            
        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE NOTICE 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
    
                           
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_fw_update_result(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1156 (class 1255 OID 1895897)
-- Name: sp_get_err_list(); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_get_err_list()
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
  NAME:       SP_GET_ERR_LIST
  PURPOSE:

  REVISIONS:
  Ver        Date        Author           Description
  ---------  ----------  ---------------  ------------------------------------
  1.0        2014-07-25   yngwie       1. Created this function.
  2.0		 2019-10-07		박동환			postgres변환

  NOTES:  ESS 장애상황을 출력한다.

Input :

Usage :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_GET_ERR_LIST';
    v_log_seq 		NUMERIC := 0;		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';							-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    p_dt    			VARCHAR	:= ''; 
    v_row           	RECORD; 
   	v_sql				VARCHAR;
   
BEGIN

	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt 
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';

--	    RAISE NOTICE 'v_log_fn[%] v_log_seq[%] START[%]', v_log_fn, v_log_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

    	FOR v_row IN ( 
			SELECT	A.DEVICE_ID
						, A.DEVICE_TYPE_CD
						, DEC_VARCHAR_SEL(C.INSTL_ADDR , 10, 'SDIENCK', 'TB_BAS_DEVICE', 'INSTL_ADDR') AS ADDRESS
						, B.ALARM_TYPE_CD
						, B.ALARM_CD
						, B.ALARM_DESC
						, A.EVENT_DT
						, FN_TS_STR_TO_STR(A.EVENT_DT, 0, 'YYYYMMDDHH24MISS', 0, 'YYYY-MM-DD HH24:MI:SS') EVENT_DT_STR
						, ALARM_ID
						, ALARM_ID_SEQ
			FROM	 	TB_OPR_ALARM A
						, TB_PLF_ALARM_CODE B
						, TB_BAS_DEVICE C
			WHERE	A.NOTI_FLAG IS NULL
			   AND 	A.ALARM_TYPE_CD = B.ALARM_TYPE_CD
			   AND 	A.ALARM_CD = B.ALARM_CD
			   AND 	A.DEVICE_ID = C.DEVICE_ID
			   AND 	A.ALARM_CD in (
					   		'E601', 'E603', 
					   		'E901', 'E902', 'E903', 'E904', 'E905', 'E906',	'E907'
				 		)        
			ORDER BY A.DEVICE_ID, A.EVENT_DT

		) LOOP
--      		RAISE notice 'v_log_fn[%] Insert TB_OPR_ALARM_ERR_LIST : ALARM_ID[%] ALARM_ID_SEQ[%]',  v_log_fn, v_row.ALARM_ID, v_row.ALARM_ID_SEQ;
		
	   		INSERT INTO TB_OPR_ALARM_ERR_LIST(ALARM_ID, ALARM_ID_SEQ) 
	   		VALUES( v_row.ALARM_ID, v_row.ALARM_ID_SEQ);
	   	
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	
	   END LOOP;

	  /* 로그
	  IF v_rslt_cnt > 0 THEN
	  	RAISE notice 'v_log_fn[%] Insert TB_OPR_ALARM_ERR_LIST : [%]건',  v_log_fn, v_rslt_cnt;
	  ELSE
	  	RAISE notice 'v_log_fn[%] No New Alarm Records',  v_log_fn;
	  END IF;
	 */

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'v_log_fn[%] EXCEPTION !!! ', v_log_fn;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$$;


ALTER PROCEDURE wems.sp_get_err_list() OWNER TO wems;

--
-- TOC entry 1157 (class 1255 OID 1895899)
-- Name: sp_init_tb_raw_login(text); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_init_tb_raw_login(p_connectorid text DEFAULT NULL::text)
    LANGUAGE plpgsql
    AS $$
DECLARE
/******************************************************************************
   NAME:       SP_INIT_TB_RAW_LOGIN
   PURPOSE:

   REVISIONS:
   Ver        Date            Author           Description
   ---------  ----------------  ---------------  ------------------------------------
   2.0        2019-09-26   이재형         postgresql 변환
   
   NOTES:  

	Input : 

	Usage :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
    v_log_fn 			VARCHAR := 'SP_INIT_TB_RAW_LOGIN';
    v_log_seq 		NUMERIC := 0;		
    v_exe_mthd		VARCHAR := 'M';	-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3
	
    p_dt 				VARCHAR   := ' ' ;
   
    -- (2)업무처리 변수
    v_row 			RECORD;
BEGIN

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
	
--	    sqlCmd := CONCAT_WS('', 'INSERT INTO TB_RAW_LOGIN (DEVICE_ID, CREATE_TM, PRODUCT_MODEL_NM, RESPONSE_CD, CREATE_DT, DATA_SEQ, EMS_OUT_IPADDRESS, EMS_TM) VALUES ( :1, ', CHR(10), ' TO_CHAR( SYS_EXTRACT_UTC(systimestamp), ''YYYYMMDDHH24MISS''), ', CHR(10), ' ''INIT'', ''128'', SYS_EXTRACT_UTC(systimestamp), SQ_RAW_LOGIN.nextval , ''127.0.0.1'', ', CHR(10), ' TO_CHAR(SYS_EXTRACT_UTC(systimestamp), ''YYYYMMDDHH24MISS'') )');
	
	    FOR v_row IN
		    SELECT  device_id, create_dt, response_cd, connector_id
	        FROM 	(	SELECT 	device_id, create_dt, response_cd, connector_id
	        							, ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY create_dt DESC, create_tm DESC) AS rn
	              			FROM 	TB_RAW_LOGIN
	              		) AS a
	        WHERE 	rn = 1 
	        AND 		a.response_cd = '0' 
	        AND 		a.connector_id = CASE WHEN p_connectorid IS NOT NULL THEN p_connectorid ELSE a.connector_id END
	    LOOP
			INSERT INTO TB_RAW_LOGIN (DEVICE_ID, CREATE_TM, PRODUCT_MODEL_NM, RESPONSE_CD, CREATE_DT, DATA_SEQ, EMS_OUT_IPADDRESS, EMS_TM)
			VALUES ( v_row.DEVICE_ID, TO_CHAR( SYS_EXTRACT_UTC(now()), 'YYYYMMDDHH24MISS'),  'INIT', '128', SYS_EXTRACT_UTC(now()), nextval('wems.sq_raw_login') , '127.0.0.1',  TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYYMMDDHH24MISS') );
	
           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	       	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	    END LOOP;
	   
	     -- 실행성공 
	     v_rslt_cd := '1';
    
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = 'p_connectorid['||p_connectorid||'] '||v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
END;
$$;


ALTER PROCEDURE wems.sp_init_tb_raw_login(p_connectorid text) OWNER TO wems;

--
-- TOC entry 1158 (class 1255 OID 1895901)
-- Name: sp_log(double precision, text, text); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_log(p_jobseq double precision, p_jobname text, p_jobmsg text)
    LANGUAGE plpgsql
    AS $$
   
/******************************************************************************
   NAME:       SP_LOG
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-12   yngwie       Created this function.
   2.0		   2019-09-19  박동환         Postgresql 버젼적용

   NOTES:   PROCEDURE, PACKAGE 의 수행이력을 TB_SYS_JOB_HIST 테이블에 추가한다.

	Input : p_jobSeq  - 순번
	        p_jobName - Job Procedure Name
	        p_jobMsg - Job 수행 메세지

	Usage : CALL SP_LOG(nextval('wems.sq_sys_job_hist'), 'PKG_OPR_ESS.SP_AGGR_STATE', 'START');

******************************************************************************/
DECLARE
    v_err_msg         VARCHAR   := NULL;  			  -- SQL오류메시지내용
    v_err_msg1        VARCHAR   := NULL;  			  -- 메시지내용1
    v_err_msg2        VARCHAR   := NULL;  			  -- 메시지내용2
    v_err_msg3        VARCHAR   := NULL;  			  -- 메시지내용3
   
BEGIN
    
	INSERT INTO wems.tb_sys_job_hist (job_seq, job_name, job_msg)
    VALUES (p_jobSeq, p_jobName, aws_oracle_ext.substr(p_jobMsg, 1, 3200));
--    COMMIT;
   
    EXCEPTION
        WHEN others then
                GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT;

				v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
           
            INSERT INTO wems.tb_sys_job_hist (job_seq, job_name, job_msg)
            VALUES (nextval('wems.sq_sys_job_hist'), 'SP_LOG', v_err_msg);
 --           COMMIT;
END;
$$;


ALTER PROCEDURE wems.sp_log(p_jobseq double precision, p_jobname text, p_jobmsg text) OWNER TO wems;

--
-- TOC entry 1111 (class 1255 OID 1895902)
-- Name: sp_off_device(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_off_device(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_OFF_DEVICE
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-09-04   yngwie       1. Created this function.

   NOTES: TB_BAS_OFF_DEVICE 테이블의 WORK_FLAG 가 N 인 장비의 이력, 통계 등의 데이터를 삭제한다.
   
    Input : 
            
    Output : 
    
******************************************************************************/ 
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_row           	record; 
  
BEGIN
	-- 3시간마다 수행
	IF SUBSTR(i_exe_base, 9, 2)::NUMERIC % 3 != 0 THEN
		RETURN	'1';
	END IF ;


    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

	    --RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] START[%] i_exe_base[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base;

	   	UPDATE TB_BAS_OFF_DEVICE 
	   	SET 	UPDATE_DT = SYS_EXTRACT_UTC(NOW()) 
	  	WHERE WORK_FLAG = 'N'; 	--AND UPDATE_DT IS NULL';
	  
      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      	--RAISE notice 'i_sp_nm[%] UPDATE TB_BAS_OFF_DEVICE UNDER PROCESSING : [%]건', i_sp_nm, v_work_num;

    	FOR v_row IN ( 
			SELECT	DEVICE_ID
			          , DEVICE_TYPE_CD 
	       	FROM    	TB_BAS_OFF_DEVICE 
	       	WHERE	WORK_FLAG = 'N' 
	       	AND 		UPDATE_DT IS NOT NULL 
	  		LIMIT 100 

		) LOOP
      		--RAISE notice 'i_sp_nm[%] Delete ESS, DEVICE_ID[%] DEVICE_TYPE_CD[%]',  i_sp_nm, v_row.DEVICE_ID, v_row.DEVICE_TYPE_CD;
                
            -- 제어, 설정, 현황  이력 정보 삭제
            DELETE FROM TB_OPR_ESS_CONFIG_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_ESS_CONFIG_HIST : [%]',  i_sp_nm, v_work_num;
            
            -- 16.02.23 jihoon 추가
            DELETE FROM TB_OPR_ESS_CONFIG WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_ESS_CONFIG : [%]',  i_sp_nm, v_work_num;
                            
            -- 16.02.23 jihoon 추가
            DELETE FROM TB_BAS_ELPW_UNIT_PRICE WHERE ELPW_PROD_CD = v_row.DEVICE_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_BAS_ELPW_UNIT_PRICE : [%]',  i_sp_nm, v_work_num;

        
            DELETE FROM TB_PLF_DEVICE_FW_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_PLF_DEVICE_FW_HIST : [%]',  i_sp_nm, v_work_num;

            DELETE FROM TB_OPR_OPER_CTRL_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_OPER_CTRL_HIST : [%]',  i_sp_nm, v_work_num;
        
            DELETE FROM TB_OPR_CTRL_RESULT WHERE DEVICE_ID = v_row.DEVICE_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_CTRL_RESULT : [%]',  i_sp_nm, v_work_num;
        
            DELETE FROM TB_OPR_OPER_LOGFILE_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_OPER_LOGFILE_HIST : [%]',  i_sp_nm, v_work_num;
        
/** 2018-12-03
       임시적용 : 용량 문제로 인해 삭제로직 임시제거
                  (용량이 많아서 한시간 후 TBR-21003: Snapshot is too old. 오류 남.
                   TB_OPR_ESS_STATE_HIST테이블 용량 주기  6개월 이하 설정 후 주석 제거)
*/  
            DELETE FROM TB_OPR_ESS_STATE_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_ESS_STATE_HIST : [%]',  i_sp_nm, v_work_num;
                                              

/** 2015.11.12
        임시적용: 용량 문제로 인해 테이블 관리에서 삭제 하도록 수정.         
                v_sql := 'DELETE FROM TB_OPR_ALARM_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_OPR_ALARM_HIST : ' || v_cnt);
                
                v_sql := 'DELETE FROM TB_OPR_ESS_PREDICT_HIST WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_OPR_ESS_PREDICT_HIST : ' || v_cnt);
                
                -- 통계 정보 삭제 
                v_sql := 'DELETE FROM TB_STT_ESS_TM WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_ESS_TM : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_ESS_DD WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_ESS_DD : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_ESS_MM WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_ESS_MM : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_ESS_YY WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_ESS_YY : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_PREDICT_TM WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_PREDICT_TM : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_PREDICT_DD WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_PREDICT_DD : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_PREDICT_MM WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_PREDICT_MM : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_PREDICT_ADVICE_DD WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_PREDICT_ADVICE_DD : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_RANK_DD WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_RANK_DD : ' || v_cnt);
                
                v_sql := 'DELETE FROM TB_STT_RANK_MM WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_RANK_MM : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_STT_QUITY_WRTY_DD WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID, list_cur(i).DEVICE_TYPE_CD;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_STT_QUITY_WRTY_DD : ' || v_cnt);
                
                
                -- RAW 정보 삭제

                v_sql := 'DELETE FROM TB_RAW_ESS_INFO WHERE DEVICE_ID = v_row.DEVICE_ID;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_RAW_ESS_INFO : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_RAW_EMS_INFO WHERE DEVICE_ID = v_row.DEVICE_ID;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_RAW_EMS_INFO : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_RAW_PCS_INFO WHERE DEVICE_ID = v_row.DEVICE_ID;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_RAW_PCS_INFO : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_RAW_RAC_INFO WHERE DEVICE_ID = v_row.DEVICE_ID;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_RAW_RAC_INFO : ' || v_cnt);
            
                v_sql := 'DELETE FROM TB_RAW_LOGIN WHERE DEVICE_ID = v_row.DEVICE_ID;
                EXECUTE IMMEDIATE v_sql USING list_cur(i).DEVICE_ID;
                v_cnt := SQL%ROWCOUNT;
                COMMIT;
                SP_LOG(i_site_seq, i_sp_nm, 'Delete TB_RAW_LOGIN : ' || v_cnt);
    **/     
        
            -- 유지보수, 기타 정보 삭제
            DELETE FROM TB_OPR_MTNC_FILE 
            WHERE MTNC_SEQ IN ( 
                SELECT 	MTNC_SEQ 
               FROM 		TB_OPR_MTNC 
              where		DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD
            )  ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_MTNC_FILE : [%]',  i_sp_nm, v_work_num;
            
            DELETE FROM TB_OPR_MTNC WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_MTNC : [%]',  i_sp_nm, v_work_num;
        
            DELETE FROM TB_OPR_VOC_FILE 
            WHERE VOC_SEQ IN ( 
                SELECT 	VOC_SEQ 
               	FROM 	TB_OPR_VOC 
              	WHERE 	DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD
            )  ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_VOC_FILE : [%]',  i_sp_nm, v_work_num;
            
            DELETE FROM TB_OPR_VOC_REPL 
            WHERE VOC_SEQ IN ( 
                	SELECT 	VOC_SEQ 
                	FROM 	TB_OPR_VOC 
                	WHERE 	DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD
            ) ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_VOC_REPL : [%]',  i_sp_nm, v_work_num;
        
            DELETE FROM TB_OPR_VOC WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_OPR_VOC : [%]',  i_sp_nm, v_work_num;
           
      	
            UPDATE TB_BAS_OFF_DEVICE 
           	SET 	WORK_FLAG = 'Y', 
           			UPDATE_DT = SYS_EXTRACT_UTC(NOW()) 
           	WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Update TB_BAS_OFF_DEVICE WORK_FLAG : [%]',  i_sp_nm, v_work_num;


            INSERT INTO TB_BAS_OFF_DEVICE_HIST 
           	SELECT * 
          	FROM TB_BAS_OFF_DEVICE  
          	WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] INSERT INTO TB_BAS_OFF_DEVICE_HIST : [%]',  i_sp_nm, v_work_num;

            DELETE FROM TB_BAS_OFF_DEVICE WHERE DEVICE_ID = v_row.DEVICE_ID AND DEVICE_TYPE_CD = v_row.DEVICE_TYPE_CD;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
      		--RAISE notice 'i_sp_nm[%] Delete TB_BAS_OFF_DEVICE : [%]',  i_sp_nm, v_work_num;
	   END LOOP;
	   
 --     	RAISE notice 'i_sp_nm[%] End Loop !!! ', i_sp_nm;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_off_device(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1160 (class 1255 OID 1895904)
-- Name: sp_off_ess(text, text, text); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_off_ess(p_deviceid text, p_devicetypecd text, p_userid text)
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_OFF_ESS
   PURPOSE:

   REVISIONS:
   Ver        Date           Author           Description
   ---------  ----------------  ---------------  ------------------------------------
   1.0        2015-08-11   yngwie         Created this function.
   1.1        2019-09-30   이재형          convert postgresql
   
   NOTES:   장비 삭제시 관련 정보 (통계, 이력, 장애 등)를 모두 삭제한다.

	Input : p_deviceId VARCHAR TB_BAS_DEVICE.DEVICE_ID
	        p_deviceTypeCd VARCHAR TB_BAS_DEVICE.DEVICE_TYPE_CD
	        p_userId VARCHAR TB_BAS_USER.USER_ID

	Usage : exec SP_OFF_ESS('AR00460036Z199999999X', 'RES', 'installer1');

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
    v_log_fn 		VARCHAR := 'SP_OFF_ESS';
    v_log_seq 	NUMERIC := 0;	-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd	VARCHAR := 'M';							-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt		TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd		VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt		INTEGER   	:= 0;     						-- 적재수
    v_work_num	INTEGER   	:= 0;     						-- 처리건수
    v_err_msg	VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1	VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2	VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3	VARCHAR   	:= NULL;  					-- 메시지내용3
	
    p_dt 			VARCHAR   	:= ' ' ;
   
    -- (2)업무처리 변수
	v_cnt			INTEGER   	:= 0;     						-- 적재수
BEGIN

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
	
	    -- 형상, 요금 정보 삭제
	    DELETE FROM TB_BAS_DEVICE WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
	
	    DELETE FROM TB_BAS_USER_DEVICE WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
	   
	    DELETE FROM TB_BAS_API_USER_DEVICE WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
	
	    DELETE FROM TB_BAS_ELPW_UNIT_PRICE WHERE elpw_prod_cd = p_deviceId;
	   
	    DELETE FROM TB_BAS_CTL_GRP_MGT WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
	   
	    DELETE FROM TB_BAS_UPT_GRP_MGT WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
	   
--	    DELETE FROM TB_OPR_PRE_DIAG_EX_DEVICE WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
	
	    DELETE FROM TB_OPR_ESS_CONFIG WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
	
	    -- 현황 정보 삭제
	    DELETE FROM TB_OPR_ESS_STATE WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
	   
	    DELETE FROM TB_OPR_ESS_STATE_TL WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
	   
	    DELETE FROM TB_OPR_ALARM WHERE device_id = p_deviceId AND device_type_cd = p_deviceTypeCd;
	   
		v_rslt_cd := 'B';

		-- 삭제 대상 장비 정보에 추가
	    INSERT INTO TB_BAS_OFF_DEVICE (device_id, device_type_cd) VALUES (p_deviceId, p_deviceTypeCd);
	   
	    GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	    v_rslt_cnt := v_rslt_cnt + v_work_num;
	    
	    -- 실행성공
	    v_rslt_cd := '1';
    
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
END;
$$;


ALTER PROCEDURE wems.sp_off_ess(p_deviceid text, p_devicetypecd text, p_userid text) OWNER TO wems;

--
-- TOC entry 1069 (class 1255 OID 1895905)
-- Name: sp_off_user(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_off_user(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_OFF_USER
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-09-22   yngwie       1. Created this function.
   2.0        2019-09-25   박동환        Postgresql 변환

   NOTES: TB_BAS_USER 테이블의 LEAVE_DT(탈퇴일시) 컬럼을 일 2회 체크하여 다음과 같이 처리한다.
          1. 탈퇴일시가 24시간 이내이면 사용자가 남긴 흔적을 모두 삭제한다.
          2. 탈퇴일시가 24시간 이후이면 TB_BAS_USER 에서 사용자 정보를 삭제한다.
   
    Input : 
            
    Output : 
    
******************************************************************************/ 
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_row           	record; 
   
BEGIN
		
	-- 12시간마다 수행
	IF SUBSTR(i_exe_base, 9, 2) NOT IN ('10', '22') THEN
		RETURN	'1';
	END IF ;


	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

	    --RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] START[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');
--	    RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] START[%] i_exe_base[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base;

    	FOR v_row IN ( 
			SELECT 
			            USER_ID
			            , LEAVE_DT
			            , CASE WHEN EXTRACT(DAY FROM SYS_EXTRACT_UTC(NOW()) - LEAVE_DT) < 1 THEN 'N' ELSE 'Y' END OFF_FLAG
	        FROM    TB_BAS_USER 
	        WHERE 	LEAVE_DT IS NOT null
	        LIMIT     100

	    ) LOOP
--             CALL wems.SP_LOG(i_site_seq, i_sp_nm, 'Delete USER INFO Start : ' || v_row.USER_ID);
			 --RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete USER INFO Start : USER_ID[%]', i_sp_nm, i_site_seq, v_row.USER_ID;
        
            -- 1. 24시간이 지났으면 사용자 테이블에서 삭제한다.
            IF v_row.OFF_FLAG = 'Y' THEN
                DELETE FROM TB_BAS_USER WHERE USER_ID = v_row.USER_ID;
		      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
			 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_BAS_USER :[%]', i_sp_nm, i_site_seq, v_work_num;

                DELETE FROM TB_OPR_TERMS_HIST WHERE USER_ID = v_row.USER_ID;
		      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
			 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_TERMS_HIST :[%]', i_sp_nm, i_site_seq, v_work_num;

                DELETE FROM TB_BAS_USER_DEVICE WHERE USER_ID = v_row.USER_ID;
		      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
			 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_BAS_USER_DEVICE :[%]', i_sp_nm, i_site_seq, v_work_num;
            END IF;
            
            -- 2. 사용자가 남긴 흔적 삭제
            
            -- TB_OPR_CUST_QNA
            DELETE FROM TB_OPR_CUST_QNA_REPL 
            WHERE CUST_QNA_SEQ IN ( 
	                SELECT CUST_QNA_SEQ FROM TB_OPR_CUST_QNA WHERE CREATE_ID = v_row.USER_ID
            )    ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_CUST_QNA_REPL :[%]', i_sp_nm, i_site_seq, v_work_num;

            DELETE FROM TB_OPR_CUST_QNA_FILE 
            WHERE CUST_QNA_SEQ IN ( 
                SELECT CUST_QNA_SEQ FROM TB_OPR_CUST_QNA WHERE CREATE_ID = v_row.USER_ID
            )  ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_CUST_QNA_FILE :[%]', i_sp_nm, i_site_seq, v_work_num;
        
            DELETE FROM TB_OPR_CUST_QNA WHERE CREATE_ID = v_row.USER_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_CUST_QNA :[%]', i_sp_nm, i_site_seq, v_work_num;
            
            -- TB_OPR_PVCALC_HIST
            DELETE FROM TB_OPR_PVCALC_HIST WHERE USER_ID = v_row.USER_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_PVCALC_HIST :[%]', i_sp_nm, i_site_seq, v_work_num;
            
            -- TB_OPR_VOC
            DELETE FROM TB_OPR_VOC_FILE 
            WHERE VOC_SEQ IN ( 
                SELECT VOC_SEQ FROM TB_OPR_VOC WHERE CREATE_ID = v_row.USER_ID
            )  ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_VOC_FILE :[%]', i_sp_nm, i_site_seq, v_work_num;
            
            DELETE FROM TB_OPR_VOC_REPL 
            WHERE VOC_SEQ IN ( 
                SELECT VOC_SEQ FROM TB_OPR_VOC WHERE CREATE_ID = v_row.USER_ID
            )  ;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_VOC_REPL :[%]', i_sp_nm, i_site_seq, v_work_num;
        
           	DELETE FROM TB_OPR_VOC WHERE CREATE_ID = v_row.USER_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_OPR_VOC :[%]', i_sp_nm, i_site_seq, v_work_num;
            
            -- 3. 이력 정보 삭제
            
            -- TB_SYS_LOGIN_HIST
            DELETE FROM TB_SYS_LOGIN_HIST WHERE USER_ID = v_row.USER_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_SYS_LOGIN_HIST :[%]', i_sp_nm, i_site_seq, v_work_num;
            
            -- TB_SYS_EXEC_HIST
            DELETE FROM TB_SYS_EXEC_HIST WHERE USER_ID =v_row.USER_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_SYS_EXEC_HIST :[%]', i_sp_nm, i_site_seq, v_work_num;
            
            -- TB_SYS_SEC_HIST
           	DELETE FROM TB_SYS_SEC_HIST WHERE USER_ID =v_row.USER_ID;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		 	--RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] Delete TB_SYS_SEC_HIST :[%]', i_sp_nm, i_site_seq, v_work_num;

		END LOOP;
    	--RAISE NOTICE 'i_sp_nm[%] End Loop !!! ', i_sp_nm;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_off_user(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1070 (class 1255 OID 1895907)
-- Name: sp_oper_ctrl_result(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_oper_ctrl_result(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$

/******************************************************************************
   NAME:       SP_OPER_CTRL_RESULT
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-08-20   yngwie       1. Created this function.
   2.0        2019-10-02   박동환        Postgresql 변환

   NOTES:   긴급 제어 명령 수행 결과 처리.
  				(입력시간 무관하게 호출된 시간 기준 처리)
   
    Input : 
	        
	Output : 
	
	Usage : SELECT SP_OPER_CTRL_RESULT('SP_OPER_CTRL_RESULT', 0, '201910221237', 'M');
	
******************************************************************************/	
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
  
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
  		v_rslt_cd := 'A';

	    --RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] i_exe_base[%] START[%]', i_sp_nm, i_site_seq, i_exe_base, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

       	-- UI 에서 긴급제어 명령 전송 후 M2M 에서 그 결과를 TB_OPR_CTRL_RESULT 테이블에 insert 한 건들만 가져온다.
		-- 아직 M2M 에서 TB_OPR_CTRL_RESULT 테이블에 결과를 insert 하지 않은 건들은 처리 제외한다.
		
        -- 2015.07.22 yngwie, CMD_REQ_STATUS_CD 에 진행상태코드 update
	   	WITH R AS ( 
			SELECT
				A.REQ_ID
				, A.DEVICE_ID
				, A.CREATE_DT
				, B.RESULT_CD
			FROM
				TB_OPR_OPER_CTRL_HIST A
				, TB_OPR_CTRL_RESULT B
			WHERE
				A.RESPONSE_ERR_CD IS NULL
				AND A.REQ_ID = B.REQ_ID
				AND A.DEVICE_ID = B.DEVICE_ID
				AND B.REQ_TYPE_CD = 'M2M'
		)
		UPDATE TB_OPR_OPER_CTRL_HIST A
		SET 		 RESPONSE_ERR_CD = R.RESULT_CD,
					 CMD_REQ_STATUS_CD = CASE WHEN R.RESULT_CD = '0' THEN '51' ELSE '55' END
		FROM 	R
		WHERE   A.REQ_ID = R.REQ_ID
		    AND 	A.DEVICE_ID = R.DEVICE_ID ;

		GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
       	v_rslt_cnt := v_rslt_cnt + v_work_num;	

	  	--RAISE NOTICE 'i_sp_nm[%] Update TB_OPR_OPER_CTRL_HIST M2M Response Exist: [%]건', i_sp_nm, v_work_num;

	    -- 2015.08.26 yngwie, TB_OPR_CTRL_RESULT 에 같은 req_id 인 레코드가 10분 이상 존재하지 않으면
	    -- TB_OPR_OPER_CTRL_HIST.RESPONSE_ERR_CD = '255', CMD_REQ_STATUS_CD = '55' 로 update 한다.
	   	WITH R AS ( 
			SELECT	 A.REQ_ID
						, A.DEVICE_ID
						, A.CHK_FLAG
						, A.CNT
			FROM (
				SELECT	A.REQ_ID
						, A.DEVICE_ID
						, CASE WHEN B.CREATE_DT <  ( SYS_EXTRACT_UTC(NOW()) - INTERVAL '10 MINUTE' ) THEN 'Y' ELSE 'N' END CHK_FLAG
						, (SELECT COUNT(*) FROM TB_OPR_CTRL_RESULT C WHERE C.REQ_ID = A.REQ_ID) AS CNT
				FROM		 TB_OPR_OPER_CTRL_HIST A
							, TB_OPR_CTRL_RESULT B
				WHERE	A.RESPONSE_ERR_CD IS NULL
					AND 	A.REQ_ID = B.REQ_ID
					AND 	A.DEVICE_ID = B.DEVICE_ID
			) A
			WHERE	A.CHK_FLAG = 'Y'	AND 	A.CNT = 1
			GROUP BY	A.REQ_ID	, A.DEVICE_ID, A.CHK_FLAG, A.CNT
		) 
		UPDATE TB_OPR_OPER_CTRL_HIST A
		SET 		RESPONSE_ERR_CD = '255',
					CMD_REQ_STATUS_CD = '55'
		FROM 	R
		WHERE   A.REQ_ID = R.REQ_ID
		    AND 	A.DEVICE_ID = R.DEVICE_ID ;
		   
		GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
       	v_rslt_cnt := v_rslt_cnt + v_work_num;	

	  	--RAISE NOTICE 'i_sp_nm[%] Update TB_OPR_OPER_CTRL_HIST M2M Response Not Exist: [%]건', i_sp_nm, v_work_num;
            
        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE NOTICE 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );

   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_oper_ctrl_result(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1071 (class 1255 OID 1895909)
-- Name: sp_quity_wrty_dd(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_quity_wrty_dd(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_QUITY_WRTY_DD
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-04-10   yngwie       1. Created this function.
   2.0        2019-09-20   박동환        Postgresql 변환

   NOTES:   품질 보증을 위해 전일자의 특정 수집 항목들의 일 평균, 최소, 최대값 등을 저장한다.
            1일 주기 수행

    Input : i_exe_base VARCHAR 집계 대상 년월일

	Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   					-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     					-- 적재수
    v_work_num		INTEGER   := 0;     					-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_ems_pt_tbl 	VARCHAR   := NULL;  
    v_bms_pt_tbl 	VARCHAR   := NULL;  
   	v_sql 				VARCHAR   := NULL;  
   	v_cnt				INTEGER   := 0;  
   
BEGIN
	i_exe_base := SUBSTR(i_exe_base, 1, 8);

	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		 v_rslt_cd := 'A';

		v_ems_pt_tbl := CONCAT_WS('', 'pt_raw_ems_info_', i_exe_base);
	    v_bms_pt_tbl := CONCAT_WS('', 'pt_raw_bms_info_', i_exe_base);

--	   	RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] START[%] i_exe_base[%] v_ems_pt_tbl[%] v_bms_pt_tbl[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base, v_ems_pt_tbl, v_bms_pt_tbl;
      
        -- 해당 파티션 테이블이 있는지 확인	
		SELECT 	COUNT(child.relname)	-- 파티션명
		INTO 		v_cnt
		FROM 	pg_inherits
	    JOIN 		pg_class parent ON pg_inherits.inhparent = parent.oid
	    JOIN 		pg_class child	ON pg_inherits.inhrelid   = child.oid
	    JOIN 		pg_namespace nmsp_parent  ON nmsp_parent.oid = parent.relnamespace
	    JOIN 		pg_namespace nmsp_child    ON nmsp_child.oid   = child.relnamespace
		WHERE 	child.relname IN ( LOWER(v_ems_pt_tbl), LOWER(v_bms_pt_tbl) );

--	    RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] PARTITION TABLE EXISTS[%]', i_sp_nm, i_site_seq, CASE WHEN v_cnt = 2 THEN 'TRUE' ELSE 'FALSE' END;
	
        -- 해당 파티션 테이블이 있으면 수행한다.		
		IF v_cnt = 2 THEN
			v_rslt_cd := 'B';
		
			-- 기존 데이터가 있으면 삭제
			DELETE FROM TB_STT_QUITY_WRTY_DD WHERE COLEC_DD = i_exe_base;

			GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--	    	RAISE NOTICE 'i_sp_nm[%] delete TB_STT_QUITY_WRTY_DD Table : [%]건, i_exe_base[%]', i_sp_nm, v_work_num, i_exe_base;

     		v_rslt_cd := 'C';

	      	-- 신규 데이터 추가
	      	v_sql := FORMAT(	'
				WITH BMS AS ( 
					SELECT 	ROW_NUMBER() OVER (PARTITION BY DEVICE_ID ORDER BY COLEC_DT DESC) SEQ 
							, DEVICE_ID 
							, COL15 
							, COL17 
							, COL18 
							, COL19 
							, COL21 
							, COL22 
					FROM 	%s 
				) , EMS AS ( 
					SELECT 	DEVICE_ID 
							, TRUNC(MAX(COL281::NUMERIC), 1) AS EMSBOARD_TEMP_MAX_AVG 
							, TRUNC(MIN(COL281::NUMERIC), 1) AS EMSBOARD_TEMP_MIN_AVG
					FROM 	%s
					GROUP BY DEVICE_ID 
				) 
				INSERT INTO TB_STT_QUITY_WRTY_DD ( 
					COLEC_DD 
					, DEVICE_ID 
					, DEVICE_TYPE_CD 
					, CELL_MAX_VOL_AVG 
					, CELL_MIN_VOL_AVG 
					, RACK_CUR_AVG 
					, CELL_MAX_TEMP_AVG 
					, CELL_MIN_TEMP_AVG 
					, EMSBOARD_TEMP_MAX_AVG 
					, EMSBOARD_TEMP_MIN_AVG 
					, LAST_SOH 
				) 
				SELECT 
					 %s AS COLEC_DD
					, A.DEVICE_ID 
					, A.DEVICE_TYPE_CD 
					, TRUNC(AVG(B.COL18::NUMERIC), 3) AS CELL_MAX_VOL_AVG 
					, TRUNC(AVG(B.COL19::NUMERIC), 3) AS CELL_MIN_VOL_AVG 
					, TRUNC(AVG(B.COL17::NUMERIC), 1) AS RACK_CUR_AVG 
					, TRUNC(AVG(B.COL21::NUMERIC), 1) AS CELL_MAX_TEMP_AVG 
					, TRUNC(AVG(B.COL22::NUMERIC), 1) AS CELL_MIN_TEMP_AVG 
					, MAX(C.EMSBOARD_TEMP_MAX_AVG) AS EMSBOARD_TEMP_MAX_AVG 
					, MAX(C.EMSBOARD_TEMP_MIN_AVG) AS EMSBOARD_TEMP_MIN_AVG 
					, TRUNC(MAX((CASE WHEN D.COL15=''NA'' THEN NULL ELSE D.COL15 END)::NUMERIC), 1) AS LAST_SOH 
				FROM 	TB_BAS_DEVICE A 
						, BMS B 
						, BMS D	
						, EMS C 
				WHERE 	A.DEVICE_ID = B.DEVICE_ID 
				AND 	D.SEQ = 1 
				AND 	A.DEVICE_ID = D.DEVICE_ID 
				AND 	A.DEVICE_ID = C.DEVICE_ID 
				GROUP BY A.DEVICE_ID, A.DEVICE_TYPE_CD	;',
				v_bms_pt_tbl, v_ems_pt_tbl, CHR(39)||i_exe_base||CHR(39)
			);
			
			
	       	EXECUTE v_sql;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;		        
 --	       	RAISE NOTICE 'i_sp_nm[%] insert TB_STT_QUITY_WRTY_DD Table : [%]건, SQL[%]', i_sp_nm, v_work_num, v_sql;
 	       
	    END IF;
   
--    	RAISE NOTICE 'i_sp_nm[%] End Loop !!! ', i_sp_nm;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_quity_wrty_dd(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1072 (class 1255 OID 1895911)
-- Name: sp_rank_daily(text); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_rank_daily(p_dt text)
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_RANK_DAILY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-30   yngwie       Created this function.
   2.0        2019-09-24   박동환        Postgresql 변환

   NOTES:

            Source Table : TB_STT_ESS_DD
            Target Table : TB_STT_RANK_DD

    Input : p_dt VARCHAR 집계 대상 년월일

	Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_RANK_DAILY';
    v_log_seq 		NUMERIC := 0;		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';	-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
   
BEGIN
	SELECT SUBSTR(p_dt, 1, 8) into p_dt;

	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';

	    -- RAISE NOTICE 'v_log_fn[%] v_log_seq[%] START[%]', v_log_fn, v_log_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	   	--  일별 통계 테이블 데이터 중 p_dt 에 해당하는 레코드 Delete
       	DELETE FROM TB_STT_RANK_DD WHERE COLEC_DD = p_dt;
     
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       

--	    RAISE NOTICE 'v_log_fn[%] v_log_seq[%] DELETE TB_STT_RANK_DD Table : [%] p_dt[%]', v_log_fn, v_log_seq, v_work_num, p_dt;
	    
	    INSERT INTO TB_STT_RANK_DD ( 
		 COLEC_DD 
		 , UTC_OFFSET 
		 , RANK_TYPE_CD 
		 , DEVICE_ID 
		 , DEVICE_TYPE_CD 
		 , RANK_PER 
		 , RANK_NO 
		 , TOTAL_CNT 			
		 , CNTRY_CD 
		 , CREATE_DT 
		 ) 
/* 2015.04.09 건물 정보 폐기에 따른 수정
        SELECT '
        	A.COLEC_DD '
        	, A.UTC_OFFSET '
        	, K.RANK_TYPE_CD '
        	, A.DEVICE_ID '
        	, A.DEVICE_TYPE_CD '
        	, CASE  '
        	     WHEN K.RANK_TYPE_CD = ''RT01'' THEN TRUNC( A.RANK_RT01 / A.TOTAL_RT01 * 100 )  '
        	     WHEN K.RANK_TYPE_CD = ''RT02'' THEN TRUNC( A.RANK_RT02 / A.TOTAL_RT02 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT03'' THEN TRUNC( A.RANK_RT03 / A.TOTAL_RT03 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT04'' THEN TRUNC( A.RANK_RT04 / A.TOTAL_RT04 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT05'' THEN TRUNC( A.RANK_RT05 / A.TOTAL_RT05 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT06'' THEN TRUNC( A.RANK_RT06 / A.TOTAL_RT06 * 100 )   '
        	  ELSE NULL END AS RANK_PER '
		    , CASE '
		    	 WHEN K.RANK_TYPE_CD = ''RT01'' THEN A.RANK_RT01 '
		    	 WHEN K.RANK_TYPE_CD = ''RT02'' THEN A.RANK_RT02 '
		    	 WHEN K.RANK_TYPE_CD = ''RT03'' THEN A.RANK_RT03 '
		    	 WHEN K.RANK_TYPE_CD = ''RT04'' THEN A.RANK_RT04 '
		    	 WHEN K.RANK_TYPE_CD = ''RT05'' THEN A.RANK_RT05 '
		    	 WHEN K.RANK_TYPE_CD = ''RT06'' THEN A.RANK_RT06 '
		       ELSE NULL END AS RANK_NO '
		    , CASE '
		    	 WHEN K.RANK_TYPE_CD = ''RT01'' THEN A.TOTAL_RT01 '
		    	 WHEN K.RANK_TYPE_CD = ''RT02'' THEN A.TOTAL_RT02 '
		    	 WHEN K.RANK_TYPE_CD = ''RT03'' THEN A.TOTAL_RT03 '
		    	 WHEN K.RANK_TYPE_CD = ''RT04'' THEN A.TOTAL_RT04 '
		    	 WHEN K.RANK_TYPE_CD = ''RT05'' THEN A.TOTAL_RT05 '
		    	 WHEN K.RANK_TYPE_CD = ''RT06'' THEN A.TOTAL_RT06 '
		       ELSE NULL END AS TOTAL_CNT '
        	, A.CNTRY_CD '
        	, SYS_EXTRACT_UTC(SYSTIMESTAMP) AS CREATE_DT '
        FROM  '
        ( '
        	SELECT '
        		A.DEVICE_ID '
        		, A.DEVICE_TYPE_CD '
        		, S.COLEC_DD '
        		, S.UTC_OFFSET '
        		, C.CNTRY_CD '
        		, S.GRID_OB_PW_H '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET ORDER BY S.GRID_OB_PW_H) RANK_RT01 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT01 '
        	    , C.BLD_AREA_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.BLD_AREA_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT02 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.BLD_AREA_CD) TOTAL_RT02 '
        		, C.TOT_RSPP_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.TOT_RSPP_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT03 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.TOT_RSPP_CD) TOTAL_RT03 '
        		, C.RGR_RSPP_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.RGR_RSPP_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT04 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.RGR_RSPP_CD) TOTAL_RT04 '
        		, C.ELPW_PROD_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.ELPW_PROD_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT05 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.ELPW_PROD_CD) TOTAL_RT05 '
        	    , C.BLD_AREA_VAL '
        	    , TRUNC(S.GRID_OB_PW_H / C.BLD_AREA_VAL, 2) '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET ORDER BY TRUNC(S.GRID_OB_PW_H / C.BLD_AREA_VAL, 2) ) RANK_RT06 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT06 '
        	FROM '
        		( '
        			SELECT  '
        				ROW_NUMBER() OVER (PARTITION BY C.DEVICE_ID, C.DEVICE_TYPE_CD ORDER BY C.CREATE_DT DESC) SEQ '
        				, A.USER_ID '
        				, C.DEVICE_ID '
        				, C.DEVICE_TYPE_CD '
        			FROM '
        				TB_BAS_USER A '
        				, TB_BAS_USER_DEVICE B '
        				, TB_BAS_DEVICE C '
        			WHERE '
        				A.AGREE_FLAG = xx1.enc_varchar_ins(''Y'', 11, ''SDIENCK'', ''AGREE_FLAG'', ''TB_BAS_USER'') '
        				AND A.USER_ID = B.USER_ID '
        				AND B.DEVICE_ID = C.DEVICE_ID '
        				AND B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD '
        		) A '
        		, TB_BAS_BLD_DEVICE B '
        		, TB_BAS_BLD C '
        		, TB_STT_ESS_DD S '
        	WHERE A.SEQ = 1 '
        		AND S.COLEC_DD = :1 '
        		AND A.DEVICE_ID = B.DEVICE_ID '
        		AND A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD '
        		AND B.BLD_ID = C.BLD_ID '
        		AND A.DEVICE_ID = S.DEVICE_ID '
        		AND A.DEVICE_TYPE_CD = S.DEVICE_TYPE_CD '
        		AND C.BLD_AREA_CD IS NOT NULL '
        		AND C.TOT_RSPP_CD IS NOT NULL '
        		AND C.RGR_RSPP_CD IS NOT NULL '
        		AND C.BLD_AREA_VAL IS NOT NULL '
        ) A '
        , ( '
        	SELECT ''RT01'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT02'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT03'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT04'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT05'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT06'' RANK_TYPE_CD FROM DUAL '
        ) K '
*/
		SELECT 
			A.COLEC_DD 
			, A.UTC_OFFSET 
			, K.RANK_TYPE_CD 
			, A.DEVICE_ID 
			, A.DEVICE_TYPE_CD 
			, CASE  
			     WHEN K.RANK_TYPE_CD = 'RT01' THEN TRUNC( A.RANK_RT01 / A.TOTAL_RT01 * 100 ) 
			     WHEN K.RANK_TYPE_CD = 'RT02' THEN TRUNC( A.RANK_RT02 / A.TOTAL_RT02 * 100 ) 
			     WHEN K.RANK_TYPE_CD = 'RT05' THEN TRUNC( A.RANK_RT05 / A.TOTAL_RT05 * 100 ) 
			     WHEN K.RANK_TYPE_CD = 'RT06' THEN TRUNC( A.RANK_RT06 / A.TOTAL_RT06 * 100 ) 
			  ELSE NULL END AS RANK_PER 
		    , CASE 
		    	 WHEN K.RANK_TYPE_CD = 'RT01' THEN A.RANK_RT01 
		    	 WHEN K.RANK_TYPE_CD = 'RT02' THEN A.RANK_RT02 
		    	 WHEN K.RANK_TYPE_CD = 'RT05' THEN A.RANK_RT05 
		    	 WHEN K.RANK_TYPE_CD = 'RT06' THEN A.RANK_RT06 
		       ELSE NULL END AS RANK_NO 
		    , CASE 
		    	 WHEN K.RANK_TYPE_CD = 'RT01' THEN A.TOTAL_RT01 
		    	 WHEN K.RANK_TYPE_CD = 'RT02' THEN A.TOTAL_RT02 
		    	 WHEN K.RANK_TYPE_CD = 'RT05' THEN A.TOTAL_RT05 
		    	 WHEN K.RANK_TYPE_CD = 'RT06' THEN A.TOTAL_RT06 
		       ELSE NULL END AS TOTAL_CNT 
			, A.CNTRY_CD 
			, SYS_EXTRACT_UTC(now()) AS CREATE_DT 
		FROM  
		( 
			SELECT 
				A.DEVICE_ID 
				, A.DEVICE_TYPE_CD 
				, S.COLEC_DD 
				, S.UTC_OFFSET 
				, A.CNTRY_CD 
				, S.GRID_OB_PW_H 
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET ORDER BY S.GRID_OB_PW_H) RANK_RT01 
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT01 
			    , A.BLD_AREA_CD 
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.BLD_AREA_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT02 
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.BLD_AREA_CD) TOTAL_RT02 
				, A.ELPW_PROD_CD 
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.ELPW_PROD_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT05 
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.ELPW_PROD_CD) TOTAL_RT05 
			    , A.BLD_AREA_VAL 
			    , TRUNC(S.GRID_OB_PW_H / A.BLD_AREA_VAL, 2) 
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET ORDER BY TRUNC(S.GRID_OB_PW_H / A.BLD_AREA_VAL, 2) ) RANK_RT06 
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT06 
			FROM 
				( 
					SELECT  
						ROW_NUMBER() OVER (PARTITION BY C.DEVICE_ID, C.DEVICE_TYPE_CD ORDER BY C.CREATE_DT DESC) SEQ 
						, A.USER_ID 
						, A.BLD_AREA_VAL
						, A.BLD_AREA_CD
						, C.DEVICE_ID 
						, C.DEVICE_TYPE_CD
						, C.CNTRY_CD
						, C.ELPW_PROD_CD
					FROM 
						TB_BAS_USER A 
						, TB_BAS_USER_DEVICE B 
						, TB_BAS_DEVICE C 
					WHERE 
						A.AGREE_FLAG = ENC_VARCHAR_INS('Y', 11, 'SDIENCK', 'AGREE_FLAG', 'TB_BAS_USER')
						AND A.USER_ID = B.USER_ID
						AND B.DEVICE_ID = C.DEVICE_ID
						AND B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD 
				) A 
				, TB_STT_ESS_DD S
			WHERE A.SEQ = 1 
				AND S.COLEC_DD = p_dt 
				AND A.DEVICE_ID = S.DEVICE_ID 
				AND A.DEVICE_TYPE_CD = S.DEVICE_TYPE_CD 
				AND A.BLD_AREA_CD IS NOT NULL 
				AND A.BLD_AREA_VAL IS NOT NULL
		) A 
		, (
			SELECT 'RT01' RANK_TYPE_CD 
			UNION ALL
			SELECT 'RT02' RANK_TYPE_CD  
			UNION ALL 
			SELECT 'RT05' RANK_TYPE_CD 
			UNION ALL
			SELECT 'RT06' RANK_TYPE_CD 
		) K
	    ;
	    
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		
--		RAISE NOTICE 'v_log_fn[%] Insert TB_STT_RANK_DD Table [%]건', v_log_fn, v_work_num;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$$;


ALTER PROCEDURE wems.sp_rank_daily(p_dt text) OWNER TO wems;

--
-- TOC entry 1073 (class 1255 OID 1895913)
-- Name: sp_rank_daily(text, text); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_rank_daily(p_startdt text, p_enddt text)
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_RANK_DAILY
   PURPOSE:

   REVISIONS:
   Ver        Date            Author           Description
   ---------  ----------------  ---------------  ------------------------------------
   1.0        2014-06-30   yngwie       Created this function.
   2.0        2019-09-24   박동환        Postgresql 변환

   NOTES:
            Source Table : TB_STT_ESS_DD
            Target Table : TB_STT_RANK_DD

    Input : p_startdt VARCHAR 집계 대상 시작 년월일
             p_enddt VARCHAR 집계 대상 종료 년월일

	Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_RANK_DAILY';
    v_log_seq 		NUMERIC := 0;		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';	-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
   
BEGIN

	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_startdt 
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';

		SELECT substring(p_startdt, 1, 8) into p_startdt;
		SELECT substring(p_enddt, 1, 8)  into p_enddt;
--	    RAISE NOTICE 'v_log_fn[%] v_log_seq[%] START[%]', v_log_fn, v_log_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

		--  일별 통계 테이블 데이터 중 p_dt 에 해당하는 레코드 Delete
        DELETE FROM TB_STT_RANK_DD 
       	WHERE COLEC_DD BETWEEN p_startdt and p_enddt;
	   
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--	    RAISE NOTICE 'v_log_fn[%] v_log_seq[%] DELETE TB_STT_RANK_DD Table : [%]건, p_startdt[%] p_enddt[%]', v_log_fn, v_log_seq, v_work_num, p_startdt, p_enddt ;
    
		INSERT INTO TB_STT_RANK_DD ( 
		 COLEC_DD 
		 , UTC_OFFSET 
		 , RANK_TYPE_CD 
		 , DEVICE_ID 
		 , DEVICE_TYPE_CD 
		 , RANK_PER 
		 , RANK_NO 
		 , TOTAL_CNT 		
		 , CNTRY_CD 
		 , CREATE_DT 
		 ) 
/* -- 2015.04.08 yngwie 건물정보 폐기에 따른 수정 
        SELECT '
        	A.COLEC_DD '
        	, A.UTC_OFFSET '
        	, K.RANK_TYPE_CD '
        	, A.DEVICE_ID '
        	, A.DEVICE_TYPE_CD '
        	, CASE  '
        	     WHEN K.RANK_TYPE_CD = ''RT01'' THEN TRUNC( A.RANK_RT01 / A.TOTAL_RT01 * 100 )  '
        	     WHEN K.RANK_TYPE_CD = ''RT02'' THEN TRUNC( A.RANK_RT02 / A.TOTAL_RT02 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT03'' THEN TRUNC( A.RANK_RT03 / A.TOTAL_RT03 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT04'' THEN TRUNC( A.RANK_RT04 / A.TOTAL_RT04 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT05'' THEN TRUNC( A.RANK_RT05 / A.TOTAL_RT05 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT06'' THEN TRUNC( A.RANK_RT06 / A.TOTAL_RT06 * 100 )   '
        	  ELSE NULL END AS RANK_PER '
		    , CASE '
		    	 WHEN K.RANK_TYPE_CD = ''RT01'' THEN A.RANK_RT01 '
		    	 WHEN K.RANK_TYPE_CD = ''RT02'' THEN A.RANK_RT02 '
		    	 WHEN K.RANK_TYPE_CD = ''RT03'' THEN A.RANK_RT03 '
		    	 WHEN K.RANK_TYPE_CD = ''RT04'' THEN A.RANK_RT04 '
		    	 WHEN K.RANK_TYPE_CD = ''RT05'' THEN A.RANK_RT05 '
		    	 WHEN K.RANK_TYPE_CD = ''RT06'' THEN A.RANK_RT06 '
		       ELSE NULL END AS RANK_NO '
		    , CASE '
		    	 WHEN K.RANK_TYPE_CD = ''RT01'' THEN A.TOTAL_RT01 '
		    	 WHEN K.RANK_TYPE_CD = ''RT02'' THEN A.TOTAL_RT02 '
		    	 WHEN K.RANK_TYPE_CD = ''RT03'' THEN A.TOTAL_RT03 '
		    	 WHEN K.RANK_TYPE_CD = ''RT04'' THEN A.TOTAL_RT04 '
		    	 WHEN K.RANK_TYPE_CD = ''RT05'' THEN A.TOTAL_RT05 '
		    	 WHEN K.RANK_TYPE_CD = ''RT06'' THEN A.TOTAL_RT06 '
		       ELSE NULL END AS TOTAL_CNT '
        	, A.CNTRY_CD '
        	, SYS_EXTRACT_UTC(SYSTIMESTAMP) AS CREATE_DT '
        FROM  '
        ( '
        	SELECT '
        		A.DEVICE_ID '
        		, A.DEVICE_TYPE_CD '
        		, S.COLEC_DD '
        		, S.UTC_OFFSET '
        		, C.CNTRY_CD '
        		, S.GRID_OB_PW_H '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET ORDER BY S.GRID_OB_PW_H) RANK_RT01 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT01 '
        	    , C.BLD_AREA_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.BLD_AREA_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT02 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.BLD_AREA_CD) TOTAL_RT02 '
        		, C.TOT_RSPP_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.TOT_RSPP_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT03 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.TOT_RSPP_CD) TOTAL_RT03 '
        		, C.RGR_RSPP_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.RGR_RSPP_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT04 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.RGR_RSPP_CD) TOTAL_RT04 '
        		, C.ELPW_PROD_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.ELPW_PROD_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT05 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.ELPW_PROD_CD) TOTAL_RT05 '
        	    , C.BLD_AREA_VAL '
        	    , TRUNC(S.GRID_OB_PW_H / C.BLD_AREA_VAL, 2) '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET ORDER BY TRUNC(S.GRID_OB_PW_H / C.BLD_AREA_VAL, 2) ) RANK_RT06 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT06 '
        	FROM '
        		( '
        			SELECT  '
        				ROW_NUMBER() OVER (PARTITION BY C.DEVICE_ID, C.DEVICE_TYPE_CD ORDER BY C.CREATE_DT DESC) SEQ '
        				, A.USER_ID '
        				, C.DEVICE_ID '
        				, C.DEVICE_TYPE_CD '
        			FROM '
        				TB_BAS_USER A '
        				, TB_BAS_USER_DEVICE B '
        				, TB_BAS_DEVICE C '
        			WHERE '
        				A.AGREE_FLAG = xx1.enc_varchar_ins(''Y'', 11, ''SDIENCK'', ''AGREE_FLAG'', ''TB_BAS_USER'') '
        				AND A.USER_ID = B.USER_ID '
        				AND B.DEVICE_ID = C.DEVICE_ID '
        				AND B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD '
        		) A '
        		, TB_BAS_BLD_DEVICE B '
        		, TB_BAS_BLD C '
        		, TB_STT_ESS_DD S '
        	WHERE A.SEQ = 1 '
        		AND S.COLEC_DD BETWEEN :1 AND :2 '
        		AND A.DEVICE_ID = B.DEVICE_ID '
        		AND A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD '
        		AND B.BLD_ID = C.BLD_ID '
        		AND A.DEVICE_ID = S.DEVICE_ID '
        		AND A.DEVICE_TYPE_CD = S.DEVICE_TYPE_CD '
        		AND C.BLD_AREA_CD IS NOT NULL '
        		AND C.TOT_RSPP_CD IS NOT NULL '
        		AND C.RGR_RSPP_CD IS NOT NULL '
        		AND C.BLD_AREA_VAL IS NOT NULL '
        ) A '
        , ( '
        	SELECT ''RT01'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT02'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT03'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT04'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT05'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT06'' RANK_TYPE_CD FROM DUAL '
        ) K 
*/
		SELECT  
			A.COLEC_DD  
			, A.UTC_OFFSET  
			, K.RANK_TYPE_CD  
			, A.DEVICE_ID  
			, A.DEVICE_TYPE_CD  
			, CASE  
			     WHEN K.RANK_TYPE_CD = 'RT01' THEN TRUNC( A.RANK_RT01 / A.TOTAL_RT01 * 100 ) 
			     WHEN K.RANK_TYPE_CD = 'RT02' THEN TRUNC( A.RANK_RT02 / A.TOTAL_RT02 * 100 ) 
			     WHEN K.RANK_TYPE_CD = 'RT05' THEN TRUNC( A.RANK_RT05 / A.TOTAL_RT05 * 100 ) 
			     WHEN K.RANK_TYPE_CD = 'RT06' THEN TRUNC( A.RANK_RT06 / A.TOTAL_RT06 * 100 ) 
			  ELSE NULL END AS RANK_PER 
		    , CASE 
		    	 WHEN K.RANK_TYPE_CD = 'RT01' THEN A.RANK_RT01 
		    	 WHEN K.RANK_TYPE_CD = 'RT02' THEN A.RANK_RT02 
		    	 WHEN K.RANK_TYPE_CD = 'RT05' THEN A.RANK_RT05 
		    	 WHEN K.RANK_TYPE_CD = 'RT06' THEN A.RANK_RT06 
		       ELSE NULL END AS RANK_NO 
		    , CASE 
		    	 WHEN K.RANK_TYPE_CD = 'RT01' THEN A.TOTAL_RT01 
		    	 WHEN K.RANK_TYPE_CD = 'RT02' THEN A.TOTAL_RT02 
		    	 WHEN K.RANK_TYPE_CD = 'RT05' THEN A.TOTAL_RT05 
		    	 WHEN K.RANK_TYPE_CD = 'RT06' THEN A.TOTAL_RT06 
		       ELSE NULL END AS TOTAL_CNT 
			, A.CNTRY_CD 
			, SYS_EXTRACT_UTC(now()) AS CREATE_DT 
		FROM   
		(  
			SELECT  
				A.DEVICE_ID  
				, A.DEVICE_TYPE_CD  
				, S.COLEC_DD  
				, S.UTC_OFFSET  
				, A.CNTRY_CD  
				, S.GRID_OB_PW_H  
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET ORDER BY S.GRID_OB_PW_H) RANK_RT01  
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT01  
			    , A.BLD_AREA_CD  
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.BLD_AREA_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT02  
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.BLD_AREA_CD) TOTAL_RT02  
				, A.ELPW_PROD_CD  
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.ELPW_PROD_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT05  
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.ELPW_PROD_CD) TOTAL_RT05  
			    , A.BLD_AREA_VAL  
			    , TRUNC(S.GRID_OB_PW_H / A.BLD_AREA_VAL, 2)  
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET ORDER BY TRUNC(S.GRID_OB_PW_H / A.BLD_AREA_VAL, 2) ) RANK_RT06  
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT06  
			FROM  
				(  
					SELECT   
						ROW_NUMBER() OVER (PARTITION BY C.DEVICE_ID, C.DEVICE_TYPE_CD ORDER BY C.CREATE_DT DESC) SEQ  
						, A.USER_ID  
						, A.BLD_AREA_VAL 
						, A.BLD_AREA_CD				 
						, C.DEVICE_ID  
						, C.DEVICE_TYPE_CD  
						, C.CNTRY_CD 
						, C.ELPW_PROD_CD				 
					FROM  
						TB_BAS_USER A  
						, TB_BAS_USER_DEVICE B  
						, TB_BAS_DEVICE C  
					WHERE  
						A.AGREE_FLAG = ENC_VARCHAR_INS('Y', 11, 'SDIENCK', 'AGREE_FLAG', 'TB_BAS_USER') 
						AND A.USER_ID = B.USER_ID  
						AND B.DEVICE_ID = C.DEVICE_ID  
						AND B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD  
				) A  
				, TB_STT_ESS_DD S  
			WHERE A.SEQ = 1  
				AND S.COLEC_DD BETWEEN p_startdt AND p_enddt
				AND A.DEVICE_ID = S.DEVICE_ID  
				AND A.DEVICE_TYPE_CD = S.DEVICE_TYPE_CD  
				AND A.BLD_AREA_CD IS NOT NULL  
				AND A.BLD_AREA_VAL IS NOT NULL  
		) A 
		, (
			SELECT 'RT01' RANK_TYPE_CD 
			UNION ALL
			SELECT 'RT02' RANK_TYPE_CD  
			UNION ALL 
			SELECT 'RT05' RANK_TYPE_CD 
			UNION ALL
			SELECT 'RT06' RANK_TYPE_CD 
		) K
	    ;
	   
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--		RAISE NOTICE 'v_log_fn[%] Insert TB_STT_RANK_DD Table [%]건 p_startdt[%] p_enddt[%]', v_log_fn, v_work_num, p_startdt, p_enddt;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_startdt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$$;


ALTER PROCEDURE wems.sp_rank_daily(p_startdt text, p_enddt text) OWNER TO wems;

--
-- TOC entry 1074 (class 1255 OID 1895915)
-- Name: sp_rank_monthly(text, text); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_rank_monthly(p_startdt text, p_enddt text)
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_RANK_MONTHLY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-02   yngwie       1. Created this function.
   2.0        2019-09-24   박동환        Postgresql 변환

   NOTES:
            Source Table : TB_STT_ESS_MM
            Target Table : TB_STT_RANK_MM

    Input : p_startdt VARCHAR 집계 대상 시작 년월일
             p_enddt VARCHAR 집계 대상 종료 년월일

	Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_RANK_MONTHLY';
    v_log_seq 		NUMERIC := 0;	-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';							-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
   
BEGIN

	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_startdt 
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
  
		v_rslt_cd := 'A';

		SELECT substring(p_startdt, 1, 6) into p_startdt;
		SELECT substring(p_enddt, 1, 6)  into p_enddt;
--	    RAISE NOTICE 'v_log_fn[%] v_log_seq[%] START[%]', v_log_fn, v_log_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	   	--  월별 통계 테이블 데이터 중 p_dt 에 해당하는 레코드 Delete
	   	DELETE FROM TB_STT_RANK_MM 
	  	WHERE COLEC_MM BETWEEN p_startdt AND p_enddt;
	 
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--	    RAISE NOTICE 'v_log_fn[%] v_log_seq[%] DELETE TB_STT_RANK_MM Table : [%]건, p_startdt[%] p_enddt[%]', v_log_fn, v_log_seq, v_work_num, p_startdt, p_enddt ;

	   	INSERT INTO TB_STT_RANK_MM ( 
			   COLEC_MM 
			 , UTC_OFFSET 
			 , RANK_TYPE_CD 
			 , DEVICE_ID 
			 , DEVICE_TYPE_CD 
			 , RANK_PER 
			 , RANK_NO 
			 , TOTAL_CNT 
			 , CNTRY_CD 
			 , CREATE_DT 
		 ) 
/* -- 2015.04.08 yngwie 건물정보 폐기에 따른 수정
        SELECT '
        	A.COLEC_MM '
        	, A.UTC_OFFSET '
        	, K.RANK_TYPE_CD '
        	, A.DEVICE_ID '
        	, A.DEVICE_TYPE_CD '
        	, CASE  '
        	     WHEN K.RANK_TYPE_CD = ''RT11'' THEN TRUNC( A.RANK_RT01 / A.TOTAL_RT01 * 100 )  '
        	     WHEN K.RANK_TYPE_CD = ''RT12'' THEN TRUNC( A.RANK_RT02 / A.TOTAL_RT02 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT13'' THEN TRUNC( A.RANK_RT03 / A.TOTAL_RT03 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT14'' THEN TRUNC( A.RANK_RT04 / A.TOTAL_RT04 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT15'' THEN TRUNC( A.RANK_RT05 / A.TOTAL_RT05 * 100 ) '
        	     WHEN K.RANK_TYPE_CD = ''RT16'' THEN TRUNC( A.RANK_RT06 / A.TOTAL_RT06 * 100 )   '
        	  ELSE NULL END AS RANK_PER '
		    , CASE '
		    	 WHEN K.RANK_TYPE_CD = ''RT11'' THEN A.RANK_RT01 '
		    	 WHEN K.RANK_TYPE_CD = ''RT12'' THEN A.RANK_RT02 '
		    	 WHEN K.RANK_TYPE_CD = ''RT13'' THEN A.RANK_RT03 '
		    	 WHEN K.RANK_TYPE_CD = ''RT14'' THEN A.RANK_RT04 '
		    	 WHEN K.RANK_TYPE_CD = ''RT15'' THEN A.RANK_RT05 '
		    	 WHEN K.RANK_TYPE_CD = ''RT16'' THEN A.RANK_RT06 '
		       ELSE NULL END AS RANK_NO '
		    , CASE '
		    	 WHEN K.RANK_TYPE_CD = ''RT11'' THEN A.TOTAL_RT01 '
		    	 WHEN K.RANK_TYPE_CD = ''RT12'' THEN A.TOTAL_RT02 '
		    	 WHEN K.RANK_TYPE_CD = ''RT13'' THEN A.TOTAL_RT03 '
		    	 WHEN K.RANK_TYPE_CD = ''RT14'' THEN A.TOTAL_RT04 '
		    	 WHEN K.RANK_TYPE_CD = ''RT15'' THEN A.TOTAL_RT05 '
		    	 WHEN K.RANK_TYPE_CD = ''RT16'' THEN A.TOTAL_RT06 '
		       ELSE NULL END AS TOTAL_CNT '
        	, A.CNTRY_CD '
        	, SYS_EXTRACT_UTC(SYSTIMESTAMP) AS CREATE_DT '
        FROM  '
        ( '
        	SELECT '
        		A.DEVICE_ID '
        		, A.DEVICE_TYPE_CD '
        		, S.COLEC_MM '
        		, S.UTC_OFFSET '
        		, C.CNTRY_CD '
        		, S.GRID_OB_PW_H '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET ORDER BY S.GRID_OB_PW_H) RANK_RT01 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT01 '
        	    , C.BLD_AREA_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.BLD_AREA_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT02 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.BLD_AREA_CD) TOTAL_RT02 '
        		, C.TOT_RSPP_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.TOT_RSPP_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT03 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.TOT_RSPP_CD) TOTAL_RT03 '
        		, C.RGR_RSPP_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.RGR_RSPP_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT04 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.RGR_RSPP_CD) TOTAL_RT04 '
        		, C.ELPW_PROD_CD '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.ELPW_PROD_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT05 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.ELPW_PROD_CD) TOTAL_RT05 '
        	    , C.BLD_AREA_VAL '
        	    , TRUNC(S.GRID_OB_PW_H / C.BLD_AREA_VAL, 2) '
        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET ORDER BY TRUNC(S.GRID_OB_PW_H / C.BLD_AREA_VAL, 2) ) RANK_RT06 '
        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT06 '
        	FROM '
        		( '
        			SELECT  '
        				ROW_NUMBER() OVER (PARTITION BY C.DEVICE_ID, C.DEVICE_TYPE_CD ORDER BY C.CREATE_DT DESC) SEQ '
        				, A.USER_ID '
        				, C.DEVICE_ID '
        				, C.DEVICE_TYPE_CD '
        			FROM '
        				TB_BAS_USER A '
        				, TB_BAS_USER_DEVICE B '
        				, TB_BAS_DEVICE C '
        			WHERE '
        				A.AGREE_FLAG = xx1.enc_varchar_ins(''Y'', 11, ''SDIENCK'', ''AGREE_FLAG'', ''TB_BAS_USER'') '
        				AND A.USER_ID = B.USER_ID '
        				AND B.DEVICE_ID = C.DEVICE_ID '
        				AND B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD '
        		) A '
        		, TB_BAS_BLD_DEVICE B '
        		, TB_BAS_BLD C '
        		, TB_STT_ESS_MM S '
        	WHERE A.SEQ = 1 '
        		AND S.COLEC_MM BETWEEN :1 AND :2 '
        		AND A.DEVICE_ID = B.DEVICE_ID '
        		AND A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD '
        		AND B.BLD_ID = C.BLD_ID '
        		AND A.DEVICE_ID = S.DEVICE_ID '
        		AND A.DEVICE_TYPE_CD = S.DEVICE_TYPE_CD '
        ) A '
        , ( '
        	SELECT ''RT11'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT12'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT13'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT14'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT15'' RANK_TYPE_CD FROM DUAL '
        	UNION ALL '
        	SELECT ''RT16'' RANK_TYPE_CD FROM DUAL '
        ) K '
*/
		SELECT  
			A.COLEC_MM  
			, A.UTC_OFFSET  
			, K.RANK_TYPE_CD  
			, A.DEVICE_ID  
			, A.DEVICE_TYPE_CD  
			, CASE   
			     WHEN K.RANK_TYPE_CD = 'RT11' THEN TRUNC( A.RANK_RT01 / A.TOTAL_RT01 * 100 )   
			     WHEN K.RANK_TYPE_CD = 'RT12' THEN TRUNC( A.RANK_RT02 / A.TOTAL_RT02 * 100 )  
			     WHEN K.RANK_TYPE_CD = 'RT15' THEN TRUNC( A.RANK_RT05 / A.TOTAL_RT05 * 100 )  
			     WHEN K.RANK_TYPE_CD = 'RT16' THEN TRUNC( A.RANK_RT06 / A.TOTAL_RT06 * 100 )    
			  ELSE NULL END AS RANK_PER  
		    , CASE  
		    	 WHEN K.RANK_TYPE_CD = 'RT11' THEN A.RANK_RT01  
		    	 WHEN K.RANK_TYPE_CD = 'RT12' THEN A.RANK_RT02  
		    	 WHEN K.RANK_TYPE_CD = 'RT15' THEN A.RANK_RT05  
		    	 WHEN K.RANK_TYPE_CD = 'RT16' THEN A.RANK_RT06  
		       ELSE NULL END AS RANK_NO 
		    , CASE  
		    	 WHEN K.RANK_TYPE_CD = 'RT11' THEN A.TOTAL_RT01  
		    	 WHEN K.RANK_TYPE_CD = 'RT12' THEN A.TOTAL_RT02  
		    	 WHEN K.RANK_TYPE_CD = 'RT15' THEN A.TOTAL_RT05  
		    	 WHEN K.RANK_TYPE_CD = 'RT16' THEN A.TOTAL_RT06  
		       ELSE NULL END AS TOTAL_CNT  
			, A.CNTRY_CD  
			, SYS_EXTRACT_UTC(NOW()) AS CREATE_DT  
		FROM   
		(  
			SELECT  
				A.DEVICE_ID  
				, A.DEVICE_TYPE_CD  
				, S.COLEC_MM  
				, S.UTC_OFFSET  
				, A.CNTRY_CD  
				, S.GRID_OB_PW_H  
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET ORDER BY S.GRID_OB_PW_H) RANK_RT01  
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT01  
			    , A.BLD_AREA_CD  
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.BLD_AREA_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT02  
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.BLD_AREA_CD) TOTAL_RT02  
				, A.ELPW_PROD_CD  
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.ELPW_PROD_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT05  
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.ELPW_PROD_CD) TOTAL_RT05  
			    , A.BLD_AREA_VAL  
			    , TRUNC(S.GRID_OB_PW_H / A.BLD_AREA_VAL, 2)  
				, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET ORDER BY TRUNC(S.GRID_OB_PW_H / A.BLD_AREA_VAL, 2) ) RANK_RT06  
				, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT06  
			FROM  
				(  
					SELECT   
						ROW_NUMBER() OVER (PARTITION BY C.DEVICE_ID, C.DEVICE_TYPE_CD ORDER BY C.CREATE_DT DESC) SEQ  
						, A.USER_ID  
						, A.BLD_AREA_VAL 
						, A.BLD_AREA_CD				 
						, C.DEVICE_ID  
						, C.DEVICE_TYPE_CD  
						, C.CNTRY_CD 
						, C.ELPW_PROD_CD 
					FROM  
						TB_BAS_USER A  
						, TB_BAS_USER_DEVICE B  
						, TB_BAS_DEVICE C  
					WHERE  
						A.AGREE_FLAG = ENC_VARCHAR_INS('Y', 11, 'SDIENCK', 'AGREE_FLAG', 'TB_BAS_USER')
						AND A.USER_ID = B.USER_ID  
						AND B.DEVICE_ID = C.DEVICE_ID  
						AND B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD  
				) A  
				, TB_STT_ESS_MM S  
			WHERE A.SEQ = 1  
				AND S.COLEC_MM BETWEEN p_startdt AND p_enddt
				AND A.DEVICE_ID = S.DEVICE_ID  
				AND A.DEVICE_TYPE_CD = S.DEVICE_TYPE_CD  
		) A  
		, (  
			SELECT 'RT11' RANK_TYPE_CD
			UNION ALL  
			SELECT 'RT12' RANK_TYPE_CD
			UNION ALL  
			SELECT 'RT15' RANK_TYPE_CD
			UNION ALL  
			SELECT 'RT16' RANK_TYPE_CD
		) K 
	    ;
	    
       	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--		RAISE NOTICE 'v_log_fn[%] Insert TB_STT_RANK_MM Table [%]건 p_startdt[%] p_enddt[%]', v_log_fn, v_work_num, p_startdt, p_enddt;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_startdt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$$;


ALTER PROCEDURE wems.sp_rank_monthly(p_startdt text, p_enddt text) OWNER TO wems;

--
-- TOC entry 1075 (class 1255 OID 1895917)
-- Name: sp_rank_monthly(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_rank_monthly(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_RANK_MONTHLY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-02   yngwie       1. Created this function.
   2.0        2019-09-20   박동환        Postgresql 변환

   NOTES:
            Source Table : TB_STT_ESS_MM
            Target Table : TB_STT_RANK_MM

    Input : i_exe_base VARCHAR 집계 대상 년월일

	Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_last_dt			TIMESTAMP := NULL;  					-- 최종작업시간
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
   
BEGIN
	i_exe_base := SUBSTR(i_exe_base, 1, 6);
	
    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
  		v_rslt_cd := 'A';

	    --RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] START[%] i_exe_base[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base;

   		--  월별 통계 테이블 데이터 중 i_exe_base 에 해당하는 레코드 Delete
        DELETE FROM TB_STT_RANK_MM
		WHERE COLEC_MM = i_exe_base;
   
      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	
     	--RAISE notice 'i_sp_nm[%] DELETE TB_STT_RANK_MM Table : [%]건, i_exe_base[%]', i_sp_nm, v_work_num, i_exe_base;

        INSERT INTO TB_STT_RANK_MM (
			 COLEC_MM	, UTC_OFFSET, RANK_TYPE_CD, DEVICE_ID, DEVICE_TYPE_CD	, RANK_PER, RANK_NO, TOTAL_CNT, CNTRY_CD	, CREATE_DT 
			) 
	/* -- 2015.04.08 yngwie 건물정보 폐기에 따른 수정
	        SELECT 
	        	A.COLEC_MM 
	        	, A.UTC_OFFSET 
	        	, K.RANK_TYPE_CD 
	        	, A.DEVICE_ID 
	        	, A.DEVICE_TYPE_CD 
	        	, CASE  
	        	     WHEN K.RANK_TYPE_CD = 'RT11' THEN TRUNC( A.RANK_RT01 / A.TOTAL_RT01 * 100 ) 
	        	     WHEN K.RANK_TYPE_CD = 'RT12' THEN TRUNC( A.RANK_RT02 / A.TOTAL_RT02 * 100 ) 
	        	     WHEN K.RANK_TYPE_CD = 'RT13' THEN TRUNC( A.RANK_RT03 / A.TOTAL_RT03 * 100 ) 
	        	     WHEN K.RANK_TYPE_CD = 'RT14' THEN TRUNC( A.RANK_RT04 / A.TOTAL_RT04 * 100 ) 
	        	     WHEN K.RANK_TYPE_CD = 'RT15' THEN TRUNC( A.RANK_RT05 / A.TOTAL_RT05 * 100 ) 
	        	     WHEN K.RANK_TYPE_CD = 'RT16' THEN TRUNC( A.RANK_RT06 / A.TOTAL_RT06 * 100 ) '
	        	  ELSE NULL END AS RANK_PER 
			    , CASE 
			    	 WHEN K.RANK_TYPE_CD = 'RT11' THEN A.RANK_RT01 
			    	 WHEN K.RANK_TYPE_CD = 'RT12' THEN A.RANK_RT02 
			    	 WHEN K.RANK_TYPE_CD = 'RT13' THEN A.RANK_RT03 
			    	 WHEN K.RANK_TYPE_CD = 'RT14' THEN A.RANK_RT04 
			    	 WHEN K.RANK_TYPE_CD = 'RT15' THEN A.RANK_RT05 
			    	 WHEN K.RANK_TYPE_CD = 'RT16' THEN A.RANK_RT06 
			       ELSE NULL END AS RANK_NO 
			    , CASE 
			    	 WHEN K.RANK_TYPE_CD = 'RT11' THEN A.TOTAL_RT01 
			    	 WHEN K.RANK_TYPE_CD = 'RT12' THEN A.TOTAL_RT02 
			    	 WHEN K.RANK_TYPE_CD = 'RT13' THEN A.TOTAL_RT03 
			    	 WHEN K.RANK_TYPE_CD = 'RT14' THEN A.TOTAL_RT04 
			    	 WHEN K.RANK_TYPE_CD = 'RT15' THEN A.TOTAL_RT05 
			    	 WHEN K.RANK_TYPE_CD = 'RT16' THEN A.TOTAL_RT06 
			       ELSE NULL END AS TOTAL_CNT 
	        	, A.CNTRY_CD 
	        	, SYS_EXTRACT_UTC(NOW()) AS CREATE_DT 
	        FROM  
	        ( 
	        	SELECT 
	        		A.DEVICE_ID 
	        		, A.DEVICE_TYPE_CD 
	        		, S.COLEC_MM 
	        		, S.UTC_OFFSET 
	        		, C.CNTRY_CD 
	        		, S.GRID_OB_PW_H 
	        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET ORDER BY S.GRID_OB_PW_H) RANK_RT01 
	        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT01 
	        	    , C.BLD_AREA_CD 
	        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.BLD_AREA_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT02 
	        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.BLD_AREA_CD) TOTAL_RT02 
	        		, C.TOT_RSPP_CD 
	        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.TOT_RSPP_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT03 
	        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.TOT_RSPP_CD) TOTAL_RT03 
	        		, C.RGR_RSPP_CD 
	        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.RGR_RSPP_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT04 
	        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.RGR_RSPP_CD) TOTAL_RT04 
	        		, C.ELPW_PROD_CD 
	        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.ELPW_PROD_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT05 
	        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET, C.ELPW_PROD_CD) TOTAL_RT05 
	        	    , C.BLD_AREA_VAL 
	        	    , TRUNC(S.GRID_OB_PW_H / C.BLD_AREA_VAL, 2) 
	        		, DENSE_RANK() OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET ORDER BY TRUNC(S.GRID_OB_PW_H / C.BLD_AREA_VAL, 2) ) RANK_RT06 
	        		, COUNT(*) OVER (PARTITION BY C.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT06 
	        	FROM 
	        		( 
	        			SELECT  
	        				ROW_NUMBER() OVER (PARTITION BY C.DEVICE_ID, C.DEVICE_TYPE_CD ORDER BY C.CREATE_DT DESC) SEQ 
	        				, A.USER_ID 
	        				, C.DEVICE_ID 
	        				, C.DEVICE_TYPE_CD 
	        			FROM 
	        				TB_BAS_USER A 
	        				, TB_BAS_USER_DEVICE B 
	        				, TB_BAS_DEVICE C 
	        			WHERE 
	        				A.AGREE_FLAG = ENC_VARCHAR_INS('Y', 11, 'SDIENCK', 'AGREE_FLAG', 'TB_BAS_USER')
	        				AND A.USER_ID = B.USER_ID 
	        				AND B.DEVICE_ID = C.DEVICE_ID
	        				AND B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD 
	        		) A 
	        		, TB_BAS_BLD_DEVICE B 
	        		, TB_BAS_BLD C 
	        		, TB_STT_ESS_MM S 
	        	WHERE A.SEQ = 1 
	        		AND S.COLEC_MM = :1 
	        		AND A.DEVICE_ID = B.DEVICE_ID 
	        		AND A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD 
	        		AND B.BLD_ID = C.BLD_ID 
	        		AND A.DEVICE_ID = S.DEVICE_ID 
	        		AND A.DEVICE_TYPE_CD = S.DEVICE_TYPE_CD 
	        ) A 
	        , ( 
	        	SELECT 'RT11' RANK_TYPE_CD FROM DUAL 
	        	UNION ALL 
	        	SELECT 'RT12' RANK_TYPE_CD FROM DUAL 
	        	UNION ALL '
	        	SELECT 'RT13' RANK_TYPE_CD FROM DUAL
	        	UNION ALL 
	        	SELECT 'RT14' RANK_TYPE_CD FROM DUAL 
	        	UNION ALL '
	        	SELECT 'RT15' RANK_TYPE_CD FROM DUAL
	        	UNION ALL 
	        	SELECT 'RT16' RANK_TYPE_CD FROM DUAL
	        ) K 
	*/
			SELECT  
				A.COLEC_MM  
				, A.UTC_OFFSET  
				, K.RANK_TYPE_CD  
				, A.DEVICE_ID  
				, A.DEVICE_TYPE_CD  
				, CASE   
				     WHEN K.RANK_TYPE_CD = 'RT11' THEN TRUNC( A.RANK_RT01 / A.TOTAL_RT01 * 100 ) 
				     WHEN K.RANK_TYPE_CD = 'RT12' THEN TRUNC( A.RANK_RT02 / A.TOTAL_RT02 * 100 ) 
				     WHEN K.RANK_TYPE_CD = 'RT15' THEN TRUNC( A.RANK_RT05 / A.TOTAL_RT05 * 100 ) 
				     WHEN K.RANK_TYPE_CD = 'RT16' THEN TRUNC( A.RANK_RT06 / A.TOTAL_RT06 * 100 ) 
				  ELSE NULL END AS RANK_PER
			    , CASE 
			    	 WHEN K.RANK_TYPE_CD = 'RT11' THEN A.RANK_RT01  
			    	 WHEN K.RANK_TYPE_CD = 'RT12' THEN A.RANK_RT02  
			    	 WHEN K.RANK_TYPE_CD = 'RT15' THEN A.RANK_RT05  
			    	 WHEN K.RANK_TYPE_CD = 'RT16' THEN A.RANK_RT06  
			       ELSE NULL END AS RANK_NO
			    , CASE
			    	 WHEN K.RANK_TYPE_CD = 'RT11' THEN A.TOTAL_RT01 
			    	 WHEN K.RANK_TYPE_CD = 'RT12' THEN A.TOTAL_RT02 
			    	 WHEN K.RANK_TYPE_CD = 'RT15' THEN A.TOTAL_RT05 
			    	 WHEN K.RANK_TYPE_CD = 'RT16' THEN A.TOTAL_RT06 
			       ELSE NULL END AS TOTAL_CNT  
				, A.CNTRY_CD  
				, SYS_EXTRACT_UTC(NOW()) AS CREATE_DT  
			FROM   (  
				SELECT  
					A.DEVICE_ID  
					, A.DEVICE_TYPE_CD  
					, S.COLEC_MM  
					, S.UTC_OFFSET  
					, A.CNTRY_CD  
					, S.GRID_OB_PW_H  
					, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET ORDER BY S.GRID_OB_PW_H) RANK_RT01  
					, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT01  
				    , A.BLD_AREA_CD  
					, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.BLD_AREA_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT02  
					, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.BLD_AREA_CD) TOTAL_RT02  
					, A.ELPW_PROD_CD  
					, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.ELPW_PROD_CD  ORDER BY S.GRID_OB_PW_H) RANK_RT05  
					, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET, A.ELPW_PROD_CD) TOTAL_RT05  
				    , A.BLD_AREA_VAL  
				    , TRUNC(S.GRID_OB_PW_H / A.BLD_AREA_VAL, 2)  
					, DENSE_RANK() OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET ORDER BY TRUNC(S.GRID_OB_PW_H / A.BLD_AREA_VAL, 2) ) RANK_RT06  
					, COUNT(*) OVER (PARTITION BY A.CNTRY_CD, S.UTC_OFFSET) TOTAL_RT06  
				FROM  (  
						SELECT   
							ROW_NUMBER() OVER (PARTITION BY C.DEVICE_ID, C.DEVICE_TYPE_CD ORDER BY C.CREATE_DT DESC) SEQ  
							, A.USER_ID  
							, A.BLD_AREA_VAL 
							, A.BLD_AREA_CD 
							, C.DEVICE_ID  
							, C.DEVICE_TYPE_CD 
							, C.CNTRY_CD 
							, C.ELPW_PROD_CD 
						FROM  
							TB_BAS_USER A  
							, TB_BAS_USER_DEVICE B  
							, TB_BAS_DEVICE C  
						WHERE  
							A.AGREE_FLAG = ENC_VARCHAR_INS('Y', 11, 'SDIENCK', 'AGREE_FLAG', 'TB_BAS_USER')
							AND A.USER_ID = B.USER_ID  
							AND B.DEVICE_ID = C.DEVICE_ID  
							AND B.DEVICE_TYPE_CD = C.DEVICE_TYPE_CD  
					) A  
					, TB_STT_ESS_MM S  
				WHERE A.SEQ = 1  
					AND S.COLEC_MM = i_exe_base  
					AND A.DEVICE_ID = S.DEVICE_ID  
					AND A.DEVICE_TYPE_CD = S.DEVICE_TYPE_CD  
			) A	, (  
				SELECT 'RT11' RANK_TYPE_CD  
				UNION ALL  
				SELECT 'RT12' RANK_TYPE_CD  
				UNION ALL  
				SELECT 'RT15' RANK_TYPE_CD  
				UNION ALL  
				SELECT 'RT16' RANK_TYPE_CD  
			) K 
		    ;
	    
   
     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;
      	--RAISE notice 'i_sp_nm[%] i_site_seq[%] Insert TB_STT_RANK_MM Table [%]건, i_exe_base[%]', i_sp_nm, i_site_seq, v_work_num, i_exe_base;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS then
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
        
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_rank_monthly(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1076 (class 1255 OID 1895919)
-- Name: sp_req_logfile_result(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_req_logfile_result(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_REQ_LOGFILE_RESULT
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-08-26   yngwie       1. Created this function.
   2.0        2019-10-02   박동환        Postgresql 변환

   NOTES:   로그파일 요청 명령 처리
  				(입력시간 무관하게 호출된 시간 기준 처리)
   
    Input : 
	        
	Output : 

	Usage : SELECT SP_REQ_LOGFILE_RESULT('SP_REQ_LOGFILE_RESULT', 0, '20191024123500', 'M')
		
******************************************************************************/	
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
  
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

	    --RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] START[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	    -- UI 에서 로그파일 요청 명령 전송 후 M2M 에서 그 결과를 TB_OPR_CTRL_RESULT 테이블에 insert 한 건들만 가져온다.
	    -- ESS 응답 대기중인 건만 찾는다 (CMD_REQ_STATUS_CD = '50')
	    
	   	WITH R AS ( 
			SELECT	A.REQ_ID
						, A.DEVICE_ID
						, A.CREATE_DT
						, A.RESULT_CD
			FROM		TB_OPR_CTRL_RESULT A
						, TB_OPR_OPER_LOGFILE_HIST B
			WHERE	A.CMD_TYPE_CD = '25'
				AND 	A.REQ_ID = B.REQ_ID
				AND 	A.REQ_TYPE_CD = 'M2M'
				AND 	B.CMD_REQ_STATUS_CD = '50'
			
		)
		UPDATE TB_OPR_OPER_LOGFILE_HIST A
		SET 		CMD_REQ_STATUS_CD = CASE WHEN R.RESULT_CD = '0' THEN '51' ELSE '55' END
		FROM		R
		WHERE   A.REQ_ID = R.REQ_ID
		    AND 	A.DEVICE_ID = R.DEVICE_ID
		;

		GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
       	v_rslt_cnt := v_rslt_cnt + v_work_num;	
	  	--RAISE NOTICE 'i_sp_nm[%] Update TB_OPR_OPER_LOGFILE_HIST[%]건', i_sp_nm, v_work_num;

	    -- TB_OPR_CTRL_RESULT 에 같은 req_id 인 레코드가 10분 이상 존재하지 않으면
	    -- TB_OPR_OPER_LOGFILE_HIST.CMD_REQ_STATUS_CD = '55' 로 update 한다.
	   	WITH R AS ( 
			SELECT	 A.REQ_ID
						, A.DEVICE_ID
						, A.CHK_FLAG
						, A.CNT
			FROM (
				SELECT	 A.REQ_ID
							, A.DEVICE_ID
							, A.CREATE_DT
							, CASE WHEN A.CREATE_DT < ( SYS_EXTRACT_UTC(NOW()) - interval '10 MINUTE' ) THEN 'Y' ELSE 'N' END CHK_FLAG
							, (SELECT COUNT(*) FROM TB_OPR_CTRL_RESULT C WHERE C.REQ_ID = A.REQ_ID) AS CNT
				FROM		 TB_OPR_CTRL_RESULT A
							, TB_OPR_OPER_LOGFILE_HIST B
				WHERE	A.REQ_ID = B.REQ_ID
					AND 	B.CMD_REQ_STATUS_CD = '50'
			) A
			WHERE	A.CHK_FLAG = 'Y' AND 	A.CNT = 1
			GROUP BY	A.REQ_ID	, A.DEVICE_ID, A.CHK_FLAG, A.CNT
		) 
		UPDATE TB_OPR_OPER_LOGFILE_HIST A
		SET 		CMD_REQ_STATUS_CD = '55'
		FROM 	R
		WHERE   A.REQ_ID = R.REQ_ID
		    AND 	A.DEVICE_ID = R.DEVICE_ID
		;

		GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
       	v_rslt_cnt := v_rslt_cnt + v_work_num;	
	  	--RAISE NOTICE 'i_sp_nm[%] Update TB_OPR_OPER_LOGFILE_HIST for long term Fail [%]건', i_sp_nm, v_work_num;
            
        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE NOTICE 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_req_logfile_result(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1161 (class 1255 OID 1895921)
-- Name: sp_set_err_list_flag(); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_set_err_list_flag()
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_SET_ERR_LIST_FLAG
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-29   yngwie       Created this function.
   1.1        2019-10-01   이재형        Convert postgresql   

   NOTES:  SP_GET_ERRLIST 로 출력한 장애상황 목록을 임시테이블에서 삭제한다.

	Input :

	Usage :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_SET_ERR_LIST_FLAG';
    v_log_seq 		NUMERIC := 0;		--	nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';	-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_last_dt			TIMESTAMP := NULL;  					-- 최종작업시간
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3

    p_dt				VARCHAR   	:= ' ';	
   
    -- (2)업무처리 변수
   
BEGIN
    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
	
	    UPDATE TB_OPR_ALARM 
	    SET 		NOTI_FLAG = 'Y' 
	    WHERE 	( ALARM_ID, ALARM_ID_SEQ ) 
	    			IN (	
	    					SELECT 	ALARM_ID, ALARM_ID_SEQ 
	   			    		FROM 	TB_OPR_ALARM_ERR_LIST 
	   			    	) ;
	
	    TRUNCATE TABLE TB_OPR_ALARM_ERR_LIST;
   
     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
       
--      	RAISE notice 'v_log_fn[%] v_log_seq[%] Insert TB_STT_RANK_MM Table [%]건, p_dt[%]', v_log_fn, v_log_seq, v_work_num, p_dt;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS then
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$$;


ALTER PROCEDURE wems.sp_set_err_list_flag() OWNER TO wems;

--
-- TOC entry 1077 (class 1255 OID 1895922)
-- Name: sp_stats_dd(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_stats_dd(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_STATS_DD
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-26   yngwie  Created this function.
   2.0        2019-09-20   박동환   Postgresql 변환
   2.1        2019-11-27   이재형   성능개선을 위한 쿼리 변환

   NOTES:   시간별 통계 데이터를 가지고 UTC_OFFSET 별로 일 통계 데이터를 생성
               1시간 주기 누적. (SP_STATS_TM에서 호출됨)

            Source Table : TB_STT_ESS_TM
            Target Table : TB_STT_ESS_DD

    Input : i_exe_base 집계 대상 년월일(YYYYMMDD)

	Output :

	Usage : SELECT SP_STATS_DD('SP_STATS_DD', 0, '20191025', 'M');

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_from			VARCHAR   := NULL; 
    v_to				VARCHAR   := NULL; 
    v_tm              VARCHAR   := NULL; 
    v_row           	record; 
  	v_sql 				VARCHAR  := NULL; 
   
BEGIN
	i_exe_base := SUBSTR(i_exe_base, 1, 8);

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';
	
--	    RAISE NOTICE 'i_sp_nm[%] exe_base[%]', i_sp_nm, i_exe_base;

    	FOR v_row IN ( 
		    SELECT  UTC_OFFSET  
		    FROM (  
		    	SELECT	0 AS UTC_OFFSET  
		    	
		    	UNION ALL    
		    	SELECT  UTC_OFFSET  
		    	FROM   TB_BAS_CITY  
		    	WHERE  UTC_OFFSET IS NOT NULL  
		    	
		    	UNION ALL  
		    	SELECT  UTC_OFFSET + 1 
		    	FROM  TB_BAS_CITY  
		    	WHERE UTC_OFFSET IS NOT NULL  
		    )  T
		    GROUP BY UTC_OFFSET 

		) LOOP

	        -- 추가할 데이터의 시작, 종료일시를 구한다.
			v_tm 		:= FN_TS_STR_TO_STR( i_exe_base||'12', v_row.utc_offset, 'YYYYMMDDHH24', 0, 'YYYYMMDDHH24');	
	        v_from 	:= FN_TS_STR_TO_STR( i_exe_base||'00', v_row.utc_offset, 'YYYYMMDDHH24', 0, 'YYYYMMDDHH24');
	        v_to 		:= FN_TS_STR_TO_STR( i_exe_base||'23', v_row.utc_offset, 'YYYYMMDDHH24', 0, 'YYYYMMDDHH24');
       
    		-- 시간별 통계 데이터를 SUM 해서 일별 통계 테이블에 Merge
    		v_sql := FORMAT('
    		WITH AR AS ( 
					SELECT   DEVICE_ID, DEVICE_TYPE_CD
							, OUTLET_PW  
							, PV_V1, PV_I1, PV_PW1, PV_V2	, PV_I2, PV_PW2  
							, INVERTER_V, INVERTER_I, INVERTER_PW  
							, DC_LINK_V
							, G_RLY_CNT	, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT  
							, RACK_V, RACK_I  
							, CELL_MAX_V, CELL_MIN_V, CELL_AVG_V	, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T 
							, PV_PW, CONS_PW, GRID_PW, PCS_PW, PCS_TGT_PW 
							, BT_SOH	, BT_SOC , BT_PW
					FROM (  
						SELECT	  A.*  
									, ROW_NUMBER() OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM) AS RNUM  
						FROM 	TB_STT_ESS_TM A 
						WHERE	COLEC_TM BETWEEN ''%s'' AND ''%s'' 
					)  Z
					WHERE RNUM = 1  
			), A AS (
				SELECT  A.DEVICE_ID    
						, A.DEVICE_TYPE_CD
						, SUM(A.PV_PW_H) AS PV_PW_H     
						, TRUNC(SUM(A.PV_PW_PRICE), 2) AS PV_PW_PRICE     
						, TRUNC(SUM(A.PV_PW_CO2), 2) AS PV_PW_CO2  
						, SUM(A.CONS_PW_H) AS CONS_PW_H   
						, TRUNC(SUM(A.CONS_PW_PRICE), 2) AS CONS_PW_PRICE   
						, TRUNC(SUM(A.CONS_PW_CO2), 2) AS CONS_PW_CO2     
						, SUM(A.CONS_GRID_PW_H) AS CONS_GRID_PW_H  
						, TRUNC(SUM(A.CONS_GRID_PW_PRICE), 2) AS CONS_GRID_PW_PRICE     
						, TRUNC(SUM(A.CONS_GRID_PW_CO2), 2) AS CONS_GRID_PW_CO2   
						, SUM(A.CONS_PV_PW_H) AS CONS_PV_PW_H    
						, TRUNC(SUM(A.CONS_PV_PW_PRICE), 2) AS CONS_PV_PW_PRICE   
						, TRUNC(SUM(A.CONS_PV_PW_CO2), 2) AS CONS_PV_PW_CO2  
						, SUM(A.CONS_BT_PW_H) AS CONS_BT_PW_H    
						, TRUNC(SUM(A.CONS_BT_PW_PRICE), 2) AS CONS_BT_PW_PRICE   
						, TRUNC(SUM(A.CONS_BT_PW_CO2), 2) AS CONS_BT_PW_CO2  
						, SUM(A.BT_CHRG_PW_H) AS BT_CHRG_PW_H    
						, TRUNC(SUM(A.BT_CHRG_PW_PRICE), 2) AS BT_CHRG_PW_PRICE   
						, TRUNC(SUM(A.BT_CHRG_PW_CO2), 2) AS BT_CHRG_PW_CO2  
						, SUM(A.BT_DCHRG_PW_H) AS BT_DCHRG_PW_H   
						, TRUNC(SUM(A.BT_DCHRG_PW_PRICE), 2) AS BT_DCHRG_PW_PRICE  
						, TRUNC(SUM(A.BT_DCHRG_PW_CO2), 2) AS BT_DCHRG_PW_CO2  
						, SUM(A.GRID_OB_PW_H) AS GRID_OB_PW_H    
						, TRUNC(SUM(A.GRID_OB_PW_PRICE), 2) AS GRID_OB_PW_PRICE   
						, TRUNC(SUM(A.GRID_OB_PW_CO2), 2) AS GRID_OB_PW_CO2  
						, SUM(A.GRID_TR_PW_H) AS GRID_TR_PW_H    
						, TRUNC(SUM(A.GRID_TR_PW_PRICE), 2) AS GRID_TR_PW_PRICE   
						, TRUNC(SUM(A.GRID_TR_PW_CO2), 2) AS GRID_TR_PW_CO2  
						, SUM(A.GRID_TR_PV_PW_H) AS GRID_TR_PV_PW_H  
						, TRUNC(SUM(A.GRID_TR_PV_PW_PRICE), 2) AS GRID_TR_PV_PW_PRICE    
						, TRUNC(SUM(A.GRID_TR_PV_PW_CO2), 2) AS GRID_TR_PV_PW_CO2  
						, SUM(A.GRID_TR_BT_PW_H) AS GRID_TR_BT_PW_H  
						, TRUNC(SUM(A.GRID_TR_BT_PW_PRICE), 2) AS GRID_TR_BT_PW_PRICE    
						, TRUNC(SUM(A.GRID_TR_BT_PW_CO2), 2) AS GRID_TR_BT_PW_CO2    
						, SUM(A.OUTLET_PW_H) AS OUTLET_PW_H    
						, TRUNC(SUM(A.OUTLET_PW_PRICE), 2) AS OUTLET_PW_PRICE   
						, TRUNC(SUM(A.OUTLET_PW_CO2), 2) AS OUTLET_PW_CO2 
						, SUM(A.PCS_FD_PW_H)			AS PCS_FD_PW_H  
						, TRUNC(SUM(A.PCS_FD_PW_PRICE), 2)		AS PCS_FD_PW_PRICE  
						, TRUNC(SUM(A.PCS_FD_PW_CO2), 2)			AS PCS_FD_PW_CO2  
						, SUM(A.PCS_PCH_PW_H)			AS PCS_PCH_PW_H  
						, TRUNC(SUM(A.PCS_PCH_PW_PRICE), 2)		AS PCS_PCH_PW_PRICE  
						, TRUNC(SUM(A.PCS_PCH_PW_CO2), 2)		AS PCS_PCH_PW_CO2  
				FROM TB_STT_ESS_TM A
--				JOIN AR ON A.DEVICE_ID = AR.DEVICE_ID AND A.DEVICE_TYPE_CD = AR.DEVICE_TYPE_CD
				WHERE A.COLEC_TM BETWEEN ''%s'' AND ''%s''
				GROUP BY A.DEVICE_ID, A.DEVICE_TYPE_CD 
			)
			, B AS (
				SELECT 
					 ''%s'' AS COLEC_DD    
					, %s AS UTC_OFFSET   
					, A.DEVICE_ID, A.DEVICE_TYPE_CD
					, PV_PW_H, PV_PW_PRICE, PV_PW_CO2  
					, CONS_PW_H, CONS_PW_PRICE	, CONS_PW_CO2     
					, CONS_GRID_PW_H, CONS_GRID_PW_PRICE, CONS_GRID_PW_CO2   
					, CONS_PV_PW_H, CONS_PV_PW_PRICE, CONS_PV_PW_CO2  
					, CONS_BT_PW_H, CONS_BT_PW_PRICE, CONS_BT_PW_CO2  
					, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2  
					, BT_DCHRG_PW_H	, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2  
					, GRID_OB_PW_H, GRID_OB_PW_PRICE, GRID_OB_PW_CO2  
					, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2  
					, GRID_TR_PV_PW_H, GRID_TR_PV_PW_PRICE	, GRID_TR_PV_PW_CO2  
					, GRID_TR_BT_PW_H, GRID_TR_BT_PW_PRICE	, GRID_TR_BT_PW_CO2    
					, OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2 
					, PCS_FD_PW_H	, PCS_FD_PW_PRICE, PCS_FD_PW_CO2  
					, PCS_PCH_PW_H, PCS_PCH_PW_PRICE	, PCS_PCH_PW_CO2  
					, OUTLET_PW  
					, PV_V1, PV_I1, PV_PW1, PV_V2	, PV_I2, PV_PW2  
					, INVERTER_V, INVERTER_I, INVERTER_PW	, DC_LINK_V  
					, G_RLY_CNT	, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT  
					, RACK_V	, RACK_I  
					, CELL_MAX_V, CELL_MIN_V, CELL_AVG_V	, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T 
					, PV_PW, CONS_PW, BT_PW, BT_SOH  
					, GRID_PW, PCS_PW, PCS_TGT_PW, BT_SOC
					, SYS_EXTRACT_UTC(NOW()) AS CREATE_DT  
				FROM  TB_BAS_DEVICE B
				join AR  on B.DEVICE_ID  = AR.DEVICE_ID and B.DEVICE_TYPE_CD = AR.DEVICE_TYPE_CD
				join  A on AR.DEVICE_ID  = A.DEVICE_ID and AR.DEVICE_TYPE_CD = A.DEVICE_TYPE_CD
			), U AS (
				UPDATE TB_STT_ESS_DD dd
				SET  
					 PV_PW_H              		= B.PV_PW_H               
					, PV_PW_PRICE          	= B.PV_PW_PRICE           
					, PV_PW_CO2            	= B.PV_PW_CO2             
					, CONS_PW_H            	= B.CONS_PW_H             
					, CONS_PW_PRICE        	= B.CONS_PW_PRICE         
					, CONS_PW_CO2          	= B.CONS_PW_CO2           
					, CONS_GRID_PW_H       = B.CONS_GRID_PW_H        
					, CONS_GRID_PW_PRICE  = B.CONS_GRID_PW_PRICE    
					, CONS_GRID_PW_CO2    = B.CONS_GRID_PW_CO2      
					, CONS_PV_PW_H         	= B.CONS_PV_PW_H          
					, CONS_PV_PW_PRICE     = B.CONS_PV_PW_PRICE      
					, CONS_PV_PW_CO2       = B.CONS_PV_PW_CO2        
					, CONS_BT_PW_H         	= B.CONS_BT_PW_H          
					, CONS_BT_PW_PRICE     	= B.CONS_BT_PW_PRICE      
					, CONS_BT_PW_CO2       = B.CONS_BT_PW_CO2        
					, BT_CHRG_PW_H         	= B.BT_CHRG_PW_H          
					, BT_CHRG_PW_PRICE     = B.BT_CHRG_PW_PRICE      
					, BT_CHRG_PW_CO2       = B.BT_CHRG_PW_CO2        
					, BT_DCHRG_PW_H        = B.BT_DCHRG_PW_H         
					, BT_DCHRG_PW_PRICE   = B.BT_DCHRG_PW_PRICE     
					, BT_DCHRG_PW_CO2     = B.BT_DCHRG_PW_CO2       
					, GRID_OB_PW_H         	= B.GRID_OB_PW_H          
					, GRID_OB_PW_PRICE     	= B.GRID_OB_PW_PRICE      
					, GRID_OB_PW_CO2       	= B.GRID_OB_PW_CO2        
					, GRID_TR_PW_H         	= B.GRID_TR_PW_H          
					, GRID_TR_PW_PRICE     	= B.GRID_TR_PW_PRICE      
					, GRID_TR_PW_CO2       	= B.GRID_TR_PW_CO2        
					, GRID_TR_PV_PW_H      	= B.GRID_TR_PV_PW_H       
					, GRID_TR_PV_PW_PRICE	= B.GRID_TR_PV_PW_PRICE   
					, GRID_TR_PV_PW_CO2   = B.GRID_TR_PV_PW_CO2     
					, GRID_TR_BT_PW_H      	= B.GRID_TR_BT_PW_H       
					, GRID_TR_BT_PW_PRICE  = B.GRID_TR_BT_PW_PRICE   
					, GRID_TR_BT_PW_CO2	= B.GRID_TR_BT_PW_CO2   
				    , BT_SOC               		= B.BT_SOC  
				    , OUTLET_PW_H          	= B.OUTLET_PW_H   
				    , OUTLET_PW_PRICE      	= B.OUTLET_PW_PRICE   
				    , OUTLET_PW_CO2        	= B.OUTLET_PW_CO2   
					, CREATE_DT            		= B.CREATE_DT    
					, PV_PW 						= B.PV_PW  
					, CONS_PW 					= B.CONS_PW  
					, BT_PW 						= B.BT_PW  
					, BT_SOH 					= B.BT_SOH  
					, GRID_PW 					= B.GRID_PW 	 
					, PCS_PW 					= B.PCS_PW  
					, PCS_TGT_PW 				= B.PCS_TGT_PW  
					, PCS_FD_PW_H				= B.PCS_FD_PW_H  
					, PCS_FD_PW_PRICE 		= B.PCS_FD_PW_PRICE  
					, PCS_FD_PW_CO2 			= B.PCS_FD_PW_CO2  
					, PCS_PCH_PW_H 			= B.PCS_PCH_PW_H  
					, PCS_PCH_PW_PRICE 		= B.PCS_PCH_PW_PRICE  
					, PCS_PCH_PW_CO2 		= B.PCS_PCH_PW_CO2  
					, OUTLET_PW 				= B.OUTLET_PW  
					, PV_V1 						= B.PV_V1  
					, PV_I1 						= B.PV_I1  
					, PV_PW1 					= B.PV_PW1  
					, PV_V2 						= B.PV_V2  
					, PV_I2 						= B.PV_I2  
					, PV_PW2 					= B.PV_PW2  
					, INVERTER_V 				= B.INVERTER_V  
					, INVERTER_I 				= B.INVERTER_I  
					, INVERTER_PW 			= B.INVERTER_PW  
					, DC_LINK_V 				= B.DC_LINK_V  
					, G_RLY_CNT 				= B.G_RLY_CNT  
					, BAT_RLY_CNT 				= B.BAT_RLY_CNT  
					, GRID_TO_GRID_RLY_CNT= B.GRID_TO_GRID_RLY_CNT  
					, BAT_PCHRG_RLY_CNT 	= B.BAT_PCHRG_RLY_CNT  
					, RACK_V 					= B.RACK_V  
					, RACK_I 						= B.RACK_I  
					, CELL_MAX_V 				= B.CELL_MAX_V  
					, CELL_MIN_V 				= B.CELL_MIN_V  
					, CELL_AVG_V 				= B.CELL_AVG_V  
					, CELL_MAX_T 				= B.CELL_MAX_T  
					, CELL_MIN_T 				= B.CELL_MIN_T  
					, CELL_AVG_T 				= B.CELL_AVG_T 
				FROM 	B
				WHERE 	dd.COLEC_DD 			= B.COLEC_DD
				AND	 	dd.UTC_OFFSET			= B.UTC_OFFSET
				AND		dd.DEVICE_ID			= B.DEVICE_ID
				AND		dd.DEVICE_TYPE_CD	= B.DEVICE_TYPE_CD
			    RETURNING dd.*
			)			
	       	INSERT INTO TB_STT_ESS_DD (  
				 COLEC_DD, UTC_OFFSET, DEVICE_ID, DEVICE_TYPE_CD   
				, PV_PW_H, PV_PW_PRICE, PV_PW_CO2             
				, CONS_PW_H, CONS_PW_PRICE	, CONS_PW_CO2, CONS_GRID_PW_H, CONS_GRID_PW_PRICE, CONS_GRID_PW_CO2
				, CONS_PV_PW_H, CONS_PV_PW_PRICE, CONS_PV_PW_CO2, CONS_BT_PW_H	, CONS_BT_PW_PRICE, CONS_BT_PW_CO2        
				, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2, BT_DCHRG_PW_H, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2       
				, GRID_OB_PW_H, GRID_OB_PW_PRICE	, GRID_OB_PW_CO2
				, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2
				, GRID_TR_PV_PW_H, GRID_TR_PV_PW_PRICE	, GRID_TR_PV_PW_CO2     
				, GRID_TR_BT_PW_H, GRID_TR_BT_PW_PRICE	, GRID_TR_BT_PW_CO2   
			    , BT_SOC
			    , OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2  
				, CREATE_DT    
				, PV_PW, CONS_PW, BT_PW, BT_SOH, GRID_PW
				, PCS_PW, PCS_TGT_PW, PCS_FD_PW_H, PCS_FD_PW_PRICE, PCS_FD_PW_CO2	, PCS_PCH_PW_H, PCS_PCH_PW_PRICE, PCS_PCH_PW_CO2 		 
				, OUTLET_PW
				, PV_V1, PV_I1, PV_PW1, PV_V2	, PV_I2, PV_PW2 				 
				, INVERTER_V, INVERTER_I, INVERTER_PW
				, DC_LINK_V
				, G_RLY_CNT, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT 	 
				, RACK_V, RACK_I 				 
				, CELL_MAX_V, CELL_MIN_V, CELL_AVG_V	, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T 			 
			) 
			SELECT
				 COLEC_DD, UTC_OFFSET, DEVICE_ID, DEVICE_TYPE_CD   
				, PV_PW_H, PV_PW_PRICE, PV_PW_CO2             
				, CONS_PW_H, CONS_PW_PRICE	, CONS_PW_CO2, CONS_GRID_PW_H, CONS_GRID_PW_PRICE, CONS_GRID_PW_CO2
				, CONS_PV_PW_H, CONS_PV_PW_PRICE, CONS_PV_PW_CO2, CONS_BT_PW_H	, CONS_BT_PW_PRICE, CONS_BT_PW_CO2        
				, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2, BT_DCHRG_PW_H, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2       
				, GRID_OB_PW_H, GRID_OB_PW_PRICE	, GRID_OB_PW_CO2
				, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2
				, GRID_TR_PV_PW_H, GRID_TR_PV_PW_PRICE	, GRID_TR_PV_PW_CO2     
				, GRID_TR_BT_PW_H, GRID_TR_BT_PW_PRICE	, GRID_TR_BT_PW_CO2   
			    , BT_SOC
			    , OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2  
				, CREATE_DT    
				, PV_PW, CONS_PW, BT_PW, BT_SOH, GRID_PW
				, PCS_PW, PCS_TGT_PW, PCS_FD_PW_H, PCS_FD_PW_PRICE, PCS_FD_PW_CO2	, PCS_PCH_PW_H, PCS_PCH_PW_PRICE, PCS_PCH_PW_CO2 		 
				, OUTLET_PW
				, PV_V1, PV_I1, PV_PW1, PV_V2	, PV_I2, PV_PW2 				 
				, INVERTER_V, INVERTER_I, INVERTER_PW
				, DC_LINK_V
				, G_RLY_CNT, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT 	 
				, RACK_V, RACK_I 				 
				, CELL_MAX_V, CELL_MIN_V, CELL_AVG_V	, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T 			 
			FROM B
			WHERE NOT EXISTS (  SELECT 1 
			    FROM 	U 
			    WHERE 	U.COLEC_DD 		= B.COLEC_DD
			    AND		U.UTC_OFFSET 		= B.UTC_OFFSET
			    AND		U.DEVICE_ID 		= B.DEVICE_ID
			    AND		U.DEVICE_TYPE_CD= B.DEVICE_TYPE_CD
			 ) ;'
    		, v_tm, v_to, v_from, v_to, i_exe_base, v_row.utc_offset);
    
--	        RAISE NOTICE 'i_sp_nm[%] UTC_OFFSET[%] 시작[%] 종료[%] v_tm[%] SQL[%]', i_sp_nm, v_row.utc_offset, v_from, v_to, v_tm, v_sql;
    		EXECUTE v_sql;
	       
	        GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--	      	RAISE notice 'i_sp_nm[%] Merge TB_STT_ESS_DD Table [%]건', i_sp_nm, v_work_num;

	  END LOOP;

 	    ------------------------------------------------
		-- 일별 순위 통계 집계 프로시저 수행
		------------------------------------------------
        CALL SP_RANK_DAILY(i_exe_base);

--	 RAISE notice 'i_sp_nm[%] End Loop !!! ', i_sp_nm;

	  -- 실행성공
      v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS then
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_stats_dd(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1078 (class 1255 OID 1895924)
-- Name: sp_stats_mm(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_stats_mm(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_STATS_MM
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-26   yngwie       Created this function.
   2.0        2019-09-20   박동환        Postgresql 변환

   NOTES:   시간별 통계 데이터를 가지고 월별 통계 데이터를 생성한다.
            1일 주기 갱신, 월 변경시 누적. (매일 0시 5분에 전일까지의 집계 수행)

            Source Table : TB_STT_ESS_TM
            Target Table : TB_STT_ESS_MM

    Input : i_exe_base VARCHAR 집계 대상 년월

	Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_from			VARCHAR   := NULL; 
    v_to				VARCHAR   := NULL; 
    v_row           	record; 
  	v_sql 				VARCHAR  	:= NULL; 
   
BEGIN
	i_exe_base := SUBSTR(i_exe_base, 1, 6);

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
  
		v_rslt_cd := 'A';
--	    RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] START[%] i_exe_base[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base;

    	FOR v_row IN ( 
		    SELECT  UTC_OFFSET  
		    FROM (  
		    	SELECT	0 AS UTC_OFFSET  
		    	
		    	UNION ALL    
		    	SELECT  UTC_OFFSET  
		    	FROM   TB_BAS_CITY  
		    	WHERE  UTC_OFFSET IS NOT NULL  
		    	
		    	UNION ALL  
		    	SELECT  UTC_OFFSET + 1 
		    	FROM  TB_BAS_CITY  
		    	WHERE UTC_OFFSET IS NOT NULL  
		    )  T
		    GROUP by UTC_OFFSET 
		    
		) LOOP
            -- 추가할 데이터의 시작, 종료일시를 구한다.
            v_from := FN_TS_STR_TO_STR ( i_exe_base || '0100', v_row.utc_offset, 'YYYYMMDDHH24', 0, 'YYYYMMDDHH24' );
            v_to    := FN_TS_STR_TO_STR ( TO_CHAR(TO_TIMESTAMP(i_exe_base, 'YYYYMM')+INTERVAL '1 MONTH -1 DAY', 'YYYYMMDD23'), v_row.utc_offset, 'YYYYMMDDHH24', 0, 'YYYYMMDDHH24' );
	       
--		    RAISE NOTICE 'i_sp_nm:[%] LOOP utc_offset[%]', i_sp_nm, v_row.utc_offset;
		   
		   
    		-- 시간별 통계 데이터를 SUM 해서 월별 통계 테이블에 Merge
   		  	v_sql := FORMAT(' 
			WITH AR AS ( 
				SELECT  	* 
				FROM (  
					SELECT 	  DEVICE_ID
								, DEVICE_TYPE_CD
								, BT_SOC  
								, ROW_NUMBER() OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM DESC) AS RNUM  
					FROM 	TB_STT_ESS_TM
					WHERE   COLEC_TM BETWEEN ''%s'' AND ''%s'' 
				)  Z
				WHERE RNUM = 1  
			), A AS ( 
				SELECT 
					 A.DEVICE_ID    
					, A.DEVICE_TYPE_CD    
					, SUM(A.PV_PW_H) AS PV_PW_H     
					, TRUNC(SUM(A.PV_PW_PRICE), 2) AS PV_PW_PRICE     
					, TRUNC(SUM(A.PV_PW_CO2), 2) AS PV_PW_CO2  
					, SUM(A.CONS_PW_H) AS CONS_PW_H   
					, TRUNC(SUM(A.CONS_PW_PRICE), 2) AS CONS_PW_PRICE   
					, TRUNC(SUM(A.CONS_PW_CO2), 2) AS CONS_PW_CO2     
					, SUM(A.CONS_GRID_PW_H) AS CONS_GRID_PW_H  
					, TRUNC(SUM(A.CONS_GRID_PW_PRICE), 2) AS CONS_GRID_PW_PRICE     
					, TRUNC(SUM(A.CONS_GRID_PW_CO2), 2) AS CONS_GRID_PW_CO2   
					, SUM(A.CONS_PV_PW_H) AS CONS_PV_PW_H    
					, TRUNC(SUM(A.CONS_PV_PW_PRICE), 2) AS CONS_PV_PW_PRICE   
					, TRUNC(SUM(A.CONS_PV_PW_CO2), 2) AS CONS_PV_PW_CO2  
					, SUM(A.CONS_BT_PW_H) AS CONS_BT_PW_H    
					, TRUNC(SUM(A.CONS_BT_PW_PRICE), 2) AS CONS_BT_PW_PRICE   
					, TRUNC(SUM(A.CONS_BT_PW_CO2), 2) AS CONS_BT_PW_CO2  
					, SUM(A.BT_CHRG_PW_H) AS BT_CHRG_PW_H    
					, TRUNC(SUM(A.BT_CHRG_PW_PRICE), 2) AS BT_CHRG_PW_PRICE   
					, TRUNC(SUM(A.BT_CHRG_PW_CO2), 2) AS BT_CHRG_PW_CO2  
					, SUM(A.BT_DCHRG_PW_H) AS BT_DCHRG_PW_H   
					, TRUNC(SUM(A.BT_DCHRG_PW_PRICE), 2) AS BT_DCHRG_PW_PRICE  
					, TRUNC(SUM(A.BT_DCHRG_PW_CO2), 2) AS BT_DCHRG_PW_CO2  
					, SUM(A.GRID_OB_PW_H) AS GRID_OB_PW_H    
					, TRUNC(SUM(A.GRID_OB_PW_PRICE), 2) AS GRID_OB_PW_PRICE   
					, TRUNC(SUM(A.GRID_OB_PW_CO2), 2) AS GRID_OB_PW_CO2  
					, SUM(A.GRID_TR_PW_H) AS GRID_TR_PW_H    
					, TRUNC(SUM(A.GRID_TR_PW_PRICE), 2) AS GRID_TR_PW_PRICE   
					, TRUNC(SUM(A.GRID_TR_PW_CO2), 2) AS GRID_TR_PW_CO2  
					, SUM(A.GRID_TR_PV_PW_H) AS GRID_TR_PV_PW_H  
					, TRUNC(SUM(A.GRID_TR_PV_PW_PRICE), 2) AS GRID_TR_PV_PW_PRICE    
					, TRUNC(SUM(A.GRID_TR_PV_PW_CO2), 2) AS GRID_TR_PV_PW_CO2  
					, SUM(A.GRID_TR_BT_PW_H) AS GRID_TR_BT_PW_H  
					, TRUNC(SUM(A.GRID_TR_BT_PW_PRICE), 2) AS GRID_TR_BT_PW_PRICE    
					, TRUNC(SUM(A.GRID_TR_BT_PW_CO2), 2) AS GRID_TR_BT_PW_CO2    
					, SUM(A.OUTLET_PW_H) AS OUTLET_PW_H    
					, TRUNC(SUM(A.OUTLET_PW_PRICE), 2) AS OUTLET_PW_PRICE   
					, TRUNC(SUM(A.OUTLET_PW_CO2), 2) AS OUTLET_PW_CO2 
					, SUM(A.PCS_FD_PW_H)			AS PCS_FD_PW_H  
					, TRUNC(SUM(A.PCS_FD_PW_PRICE), 2)		AS PCS_FD_PW_PRICE  
					, TRUNC(SUM(A.PCS_FD_PW_CO2), 2)			AS PCS_FD_PW_CO2  
					, SUM(A.PCS_PCH_PW_H)			AS PCS_PCH_PW_H  
					, TRUNC(SUM(A.PCS_PCH_PW_PRICE), 2)		AS PCS_PCH_PW_PRICE  
					, TRUNC(SUM(A.PCS_PCH_PW_CO2), 2)		AS PCS_PCH_PW_CO2  			
				FROM 	TB_STT_ESS_TM A    
				WHERE	A.COLEC_TM BETWEEN ''%s'' AND ''%s'' 
				GROUP BY A.DEVICE_ID, A.DEVICE_TYPE_CD   

			), B as ( 
				SELECT 
					 ''%s'' AS COLEC_MM    
					, %s AS UTC_OFFSET   
					, A.DEVICE_ID    
					, A.DEVICE_TYPE_CD 
					, A.PV_PW_H, A.PV_PW_PRICE, A.PV_PW_CO2, A.CONS_PW_H   
					, A.CONS_PW_PRICE, A.CONS_PW_CO2, A.CONS_GRID_PW_H, A.CONS_GRID_PW_PRICE, A.CONS_GRID_PW_CO2
					, A.CONS_PV_PW_H, A.CONS_PV_PW_PRICE, A.CONS_PV_PW_CO2
					, A.CONS_BT_PW_H, A.CONS_BT_PW_PRICE, A.CONS_BT_PW_CO2
					, A.BT_CHRG_PW_H, A.BT_CHRG_PW_PRICE, A.BT_CHRG_PW_CO2
					, A.BT_DCHRG_PW_H, A.BT_DCHRG_PW_PRICE, A.BT_DCHRG_PW_CO2
					, A.GRID_OB_PW_H, A.GRID_OB_PW_PRICE, A.GRID_OB_PW_CO2
					, A.GRID_TR_PW_H, A.GRID_TR_PW_PRICE, A.GRID_TR_PW_CO2
					, A.GRID_TR_PV_PW_H, A.GRID_TR_PV_PW_PRICE, A.GRID_TR_PV_PW_CO2  
					, A.GRID_TR_BT_PW_H, A.GRID_TR_BT_PW_PRICE, A.GRID_TR_BT_PW_CO2
--				    , MAX(AR.BT_SOC) AS BT_SOC  
				    , AR.BT_SOC  
					, A.OUTLET_PW_H, A.OUTLET_PW_PRICE, A.OUTLET_PW_CO2 
					, SYS_EXTRACT_UTC(NOW()) AS CREATE_DT  
					, A.PCS_FD_PW_H, A.PCS_FD_PW_PRICE, A.PCS_FD_PW_CO2, A.PCS_PCH_PW_H, A.PCS_PCH_PW_PRICE, A.PCS_PCH_PW_CO2  			
				FROM  A    
						, TB_BAS_DEVICE B 
						, AR 
				WHERE  A.DEVICE_ID 			= B.DEVICE_ID   
					AND A.DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD   
					AND A.DEVICE_ID 			= AR.DEVICE_ID    
					AND A.DEVICE_TYPE_CD 	= AR.DEVICE_TYPE_CD 
--				GROUP BY A.DEVICE_ID, A.DEVICE_TYPE_CD   

	      ), U AS (
			UPDATE TB_STT_ESS_MM mm
			SET  
				  PV_PW_H              = B.PV_PW_H               
				, PV_PW_PRICE          = B.PV_PW_PRICE           
				, PV_PW_CO2            = B.PV_PW_CO2             
				, CONS_PW_H            = B.CONS_PW_H             
				, CONS_PW_PRICE        = B.CONS_PW_PRICE         
				, CONS_PW_CO2          = B.CONS_PW_CO2           
				, CONS_GRID_PW_H       = B.CONS_GRID_PW_H        
				, CONS_GRID_PW_PRICE   = B.CONS_GRID_PW_PRICE    
				, CONS_GRID_PW_CO2     = B.CONS_GRID_PW_CO2      
				, CONS_PV_PW_H         = B.CONS_PV_PW_H          
				, CONS_PV_PW_PRICE     = B.CONS_PV_PW_PRICE      
				, CONS_PV_PW_CO2       = B.CONS_PV_PW_CO2        
				, CONS_BT_PW_H         = B.CONS_BT_PW_H          
				, CONS_BT_PW_PRICE     = B.CONS_BT_PW_PRICE      
				, CONS_BT_PW_CO2       = B.CONS_BT_PW_CO2        
				, BT_CHRG_PW_H         = B.BT_CHRG_PW_H          
				, BT_CHRG_PW_PRICE     = B.BT_CHRG_PW_PRICE      
				, BT_CHRG_PW_CO2       = B.BT_CHRG_PW_CO2        
				, BT_DCHRG_PW_H        = B.BT_DCHRG_PW_H         
				, BT_DCHRG_PW_PRICE    = B.BT_DCHRG_PW_PRICE     
				, BT_DCHRG_PW_CO2      = B.BT_DCHRG_PW_CO2       
				, GRID_OB_PW_H         = B.GRID_OB_PW_H          
				, GRID_OB_PW_PRICE     = B.GRID_OB_PW_PRICE      
				, GRID_OB_PW_CO2       = B.GRID_OB_PW_CO2        
				, GRID_TR_PW_H         = B.GRID_TR_PW_H          
				, GRID_TR_PW_PRICE     = B.GRID_TR_PW_PRICE      
				, GRID_TR_PW_CO2       = B.GRID_TR_PW_CO2        
				, GRID_TR_PV_PW_H      = B.GRID_TR_PV_PW_H       
				, GRID_TR_PV_PW_PRICE  = B.GRID_TR_PV_PW_PRICE   
				, GRID_TR_PV_PW_CO2    = B.GRID_TR_PV_PW_CO2     
				, GRID_TR_BT_PW_H      = B.GRID_TR_BT_PW_H       
				, GRID_TR_BT_PW_PRICE  = B.GRID_TR_BT_PW_PRICE   
				, GRID_TR_BT_PW_CO2    = B.GRID_TR_BT_PW_CO2   
			    , BT_SOC               		= B.BT_SOC  
			    , OUTLET_PW_H          	= B.OUTLET_PW_H   
			    , OUTLET_PW_PRICE      = B.OUTLET_PW_PRICE   
			    , OUTLET_PW_CO2        = B.OUTLET_PW_CO2   
				, CREATE_DT            		= B.CREATE_DT    
				, PCS_FD_PW_H				= B.PCS_FD_PW_H  
				, PCS_FD_PW_PRICE 		= B.PCS_FD_PW_PRICE  
				, PCS_FD_PW_CO2 			= B.PCS_FD_PW_CO2  
				, PCS_PCH_PW_H 			= B.PCS_PCH_PW_H  
				, PCS_PCH_PW_PRICE 		= B.PCS_PCH_PW_PRICE  
				, PCS_PCH_PW_CO2 			= B.PCS_PCH_PW_CO2  			
	      	FROM 	B
			WHERE	mm.COLEC_MM 		= B.COLEC_MM  
				AND 	mm.UTC_OFFSET 		= B.UTC_OFFSET  
				AND 	mm.DEVICE_ID			= B.DEVICE_ID  
				AND 	mm.DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
				RETURNING mm.*
	      )
		INSERT INTO TB_STT_ESS_MM (  
				 COLEC_MM	, UTC_OFFSET, DEVICE_ID	, DEVICE_TYPE_CD   
				, PV_PW_H, PV_PW_PRICE, PV_PW_CO2
				, CONS_PW_H, CONS_PW_PRICE	, CONS_PW_CO2, CONS_GRID_PW_H
				, CONS_GRID_PW_PRICE, CONS_GRID_PW_CO2
				, CONS_PV_PW_H, CONS_PV_PW_PRICE, CONS_PV_PW_CO2        
				, CONS_BT_PW_H, CONS_BT_PW_PRICE, CONS_BT_PW_CO2        
				, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2        
				, BT_DCHRG_PW_H	, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2       
				, GRID_OB_PW_H, GRID_OB_PW_PRICE	, GRID_OB_PW_CO2        
				, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2        
				, GRID_TR_PV_PW_H, GRID_TR_PV_PW_PRICE	, GRID_TR_PV_PW_CO2     
				, GRID_TR_BT_PW_H, GRID_TR_BT_PW_PRICE	, GRID_TR_BT_PW_CO2   
			    , BT_SOC   
			    , OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2  
				, CREATE_DT    
				, PCS_FD_PW_H, PCS_FD_PW_PRICE	, PCS_FD_PW_CO2 		 
				, PCS_PCH_PW_H, PCS_PCH_PW_PRICE, PCS_PCH_PW_CO2 		 			
			)
			SELECT  
				 B.COLEC_MM, B.UTC_OFFSET, B.DEVICE_ID, B.DEVICE_TYPE_CD   
				, B.PV_PW_H	, B.PV_PW_PRICE, B.PV_PW_CO2             
				, B.CONS_PW_H	, B.CONS_PW_PRICE, B.CONS_PW_CO2           
				, B.CONS_GRID_PW_H, B.CONS_GRID_PW_PRICE, B.CONS_GRID_PW_CO2      
				, B.CONS_PV_PW_H, B.CONS_PV_PW_PRICE, B.CONS_PV_PW_CO2        
				, B.CONS_BT_PW_H	, B.CONS_BT_PW_PRICE, B.CONS_BT_PW_CO2        
				, B.BT_CHRG_PW_H	, B.BT_CHRG_PW_PRICE, B.BT_CHRG_PW_CO2        
				, B.BT_DCHRG_PW_H, B.BT_DCHRG_PW_PRICE, B.BT_DCHRG_PW_CO2       
				, B.GRID_OB_PW_H	, B.GRID_OB_PW_PRICE, B.GRID_OB_PW_CO2        
				, B.GRID_TR_PW_H, B.GRID_TR_PW_PRICE, B.GRID_TR_PW_CO2        
				, B.GRID_TR_PV_PW_H	, B.GRID_TR_PV_PW_PRICE, B.GRID_TR_PV_PW_CO2     
				, B.GRID_TR_BT_PW_H	, B.GRID_TR_BT_PW_PRICE, B.GRID_TR_BT_PW_CO2   
			    , B.BT_SOC   
			    , B.OUTLET_PW_H, B.OUTLET_PW_PRICE, B.OUTLET_PW_CO2  
				, B.CREATE_DT  
				, B.PCS_FD_PW_H, B.PCS_FD_PW_PRICE, B.PCS_FD_PW_CO2 		 
				, B.PCS_PCH_PW_H	, B.PCS_PCH_PW_PRICE, B.PCS_PCH_PW_CO2 	
			FROM B
			WHERE NOT EXISTS (  
				SELECT 	1 
			    FROM 	U 
				WHERE	COLEC_MM 			= B.COLEC_MM  
					AND 	UTC_OFFSET 		= B.UTC_OFFSET  
					AND 	DEVICE_ID			= B.DEVICE_ID  
					AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
			  ) ;', v_from, v_to, v_from, v_to, i_exe_base, v_row.utc_offset);
			 
--	      	RAISE NOTICE 'i_sp_nm[%] Merge TB_STT_ESS_MM Table [%]건 UTC_OFFSET[%] 시작[%] 종료[%] SQL[%]', i_sp_nm, v_work_num, v_row.utc_offset, v_from, v_to, v_sql;

			EXECUTE v_sql;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       


	   END LOOP;
	   
	    ------------------------------------------------
		-- 월별 순위 통계 집계 프로시저 수행
		------------------------------------------------
        PERFORM SP_RANK_MONTHLY('SP_RANK_MONTHLY', 0, i_exe_base, i_exe_mthd);

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_stats_mm(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1079 (class 1255 OID 1895926)
-- Name: sp_stats_tm(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_stats_tm(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_STATS_TM
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-20   yngwie  	Created this function.
   2.0        2019-09-20   박동환    Postgresql 변환
   2.1        2019-10-11   이재형    성능개선을 위해 파티션테이블 조회 방법수정
 
   NOTES:   5분주기로 수집된 데이터를 가지고 시간별 통계 데이터를 생성한다.
              매시 2분에 전시각 데이터 집계 수행.

            Source Table : TB_OPR_ESS_STATE_HIST
            Target Table : TB_STT_ESS_TM

    Input :

	Output :

		Usage : SELECT SP_STATS_TM('SP_STATS_TM', 0, '2019102519', 'M');

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_prev_dt			VARCHAR   := NULL; 
    v_next_dt			VARCHAR   := NULL; 
   
   
BEGIN
	i_exe_base := SUBSTR(i_exe_base, 1, 10);

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
	
--		RAISE notice 'i_sp_nm[%], i_site_seq[%], START[%], i_exe_base[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base;
		
		-- 집계대상시각(i_exe_base)의 이전시각을 구한다.
		SELECT TO_CHAR( TO_TIMESTAMP(i_exe_base, 'YYYYMMDDHH24')-interval '1 hour', 'YYYYMMDDHH24')
		into	v_prev_dt;
	
		SELECT TO_CHAR( TO_TIMESTAMP(i_exe_base, 'YYYYMMDDHH24') + interval '1 hour', 'YYYYMMDDHH24')
		into	v_next_dt;
		   
--		RAISE notice 'i_sp_nm[%], i_site_seq[%], i_exe_base[%], v_prev_dt[%]'	, i_sp_nm, i_site_seq, i_exe_base, v_prev_dt;

		-- 시간별 통계 테이블 데이터 중 i_exe_base 에 해당하는 레코드 Delete
		DELETE FROM TB_STT_ESS_TM WHERE COLEC_TM = i_exe_base;
		  
        GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;
--		RAISE notice 'i_sp_nm[%], i_site_seq[%], DELETE TB_STT_ESS_TM Table :[%], 삭제건수[%]', i_sp_nm, i_site_seq, i_exe_base, v_work_num;

   		v_rslt_cd := 'B';

        WITH T AS (
			SELECT  
			    DEVICE_ID 
			    , DEVICE_TYPE_CD 
			    , SUBSTR(COLEC_DT, 1, 10)  AS COLEC_TM 
			    , PV_PW_H  
			    , PV_PW_PRICE 
			    , PV_PW_CO2   
			    , CONS_PW_H     
			    , CONS_PW_PRICE 
			    , CONS_PW_CO2   
			    , BT_CHRG_PW_H      
			    , BT_CHRG_PW_PRICE  
			    , BT_CHRG_PW_CO2    
			    , BT_DCHRG_PW_H     
			    , BT_DCHRG_PW_PRICE 
			    , BT_DCHRG_PW_CO2   
			    , GRID_OB_PW_H         
			    , GRID_OB_PW_PRICE     
			    , GRID_OB_PW_CO2       
			    , GRID_TR_PW_H      
			    , GRID_TR_PW_PRICE  
			    , GRID_TR_PW_CO2        
			-- [ 2015.05.13 yngwie OUTLET_PW 추가
			    , BT_SOC        
			    , OUTLET_PW_H        
			    , OUTLET_PW_PRICE        
			    , OUTLET_PW_CO2        
			-- ]
			-- [ 2015.07.06 yngwie 수집 컬럼 추가
			    , PV_PW 
			    , CONS_PW 
			    , BT_PW 
			    , BT_SOH 
			    , GRID_PW 
			    , PCS_PW 
			    , PCS_TGT_PW 
			    , PCS_FD_PW_H 
			    , PCS_PCH_PW_H 
			    , OUTLET_PW 
			    , PV_V1 
			    , PV_I1 
			    , PV_PW1 
			    , PV_V2 
			    , PV_I2 
			    , PV_PW2 
			    , INVERTER_V 
			    , INVERTER_I 
			    , INVERTER_PW 
			    , DC_LINK_V 
			    , G_RLY_CNT 
			    , BAT_RLY_CNT 
			    , GRID_TO_GRID_RLY_CNT 
			    , BAT_PCHRG_RLY_CNT 
			    , RACK_V 
			    , RACK_I 
			    , CELL_MAX_V 
			    , CELL_MIN_V 
			    , CELL_AVG_V 
			    , CELL_MAX_T 
			    , CELL_MIN_T 
			    , CELL_AVG_T 	
			    , SMTR_TR_CNTER 
			    , SMTR_OB_CNTER 
			FROM TB_OPR_ESS_STATE_HIST 
			WHERE (COLEC_DT, DEVICE_ID) IN (  
				      SELECT 	MAX(COLEC_DT) AS COLEC_DT, DEVICE_ID                  
				      FROM 	TB_OPR_ESS_STATE_HIST         
				      -- 2019-10-11 파티션 테이블 조회 수정, 이재형
				      WHERE COLEC_DT >= i_exe_base AND COLEC_DT < v_next_dt
				      GROUP BY DEVICE_ID
				      
				      UNION ALL             
				      
				      SELECT 	MAX(COLEC_DT) AS COLEC_DT, DEVICE_ID                  
				      FROM 	TB_OPR_ESS_STATE_HIST         
				      WHERE	COLEC_DT BETWEEN SUBSTR(v_prev_dt, 1, 8)  || '000000' and v_prev_dt || '5959'   
				      GROUP BY  DEVICE_ID   
					)  
		), B AS (
			SELECT 
				DEVICE_ID 
				, DEVICE_TYPE_CD 
				, COLEC_TM 
				, PV_PW_H           
				, COALESCE(LAG(PV_PW_H , 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_PV_PW_H 
				, CONS_PW_H         
				, COALESCE(LAG(CONS_PW_H , 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_CONS_PW_H 
				, BT_CHRG_PW_H      
				, COALESCE(LAG(BT_CHRG_PW_H, 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_BT_CHRG_PW_H 
				, BT_DCHRG_PW_H     
				, COALESCE(LAG(BT_DCHRG_PW_H, 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_BT_DCHRG_PW_H 
				, GRID_OB_PW_H      
				, COALESCE(LAG(GRID_OB_PW_H, 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_GRID_OB_PW_H 
				, GRID_TR_PW_H      
				, COALESCE(LAG(GRID_TR_PW_H, 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_GRID_TR_PW_H 
		-- [ 2015.05.13 yngwie OUTLET_PW 추가
				, BT_SOC      
				, OUTLET_PW_H      
				, COALESCE(LAG(OUTLET_PW_H , 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_OUTLET_PW_H 
		-- ]
		-- [ 2015.07.06 yngwie 수집 컬럼 추가
				, PV_PW 
				, CONS_PW 
				, BT_PW 
				, BT_SOH 
				, GRID_PW 
				, PCS_PW 
				, PCS_TGT_PW 
				, PCS_FD_PW_H 
				, COALESCE(LAG(PCS_FD_PW_H , 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_PCS_FD_PW_H 
				, PCS_PCH_PW_H 
				, COALESCE(LAG(PCS_PCH_PW_H , 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_PCS_PCH_PW_H 
				, OUTLET_PW 
				, PV_V1 
				, PV_I1 
				, PV_PW1 
				, PV_V2 
				, PV_I2 
				, PV_PW2 
				, INVERTER_V 
				, INVERTER_I 
				, INVERTER_PW 
				, DC_LINK_V 
				, G_RLY_CNT 
				, BAT_RLY_CNT 
				, GRID_TO_GRID_RLY_CNT 
				, BAT_PCHRG_RLY_CNT 
				, RACK_V 
				, RACK_I 
				, CELL_MAX_V 
				, CELL_MIN_V 
				, CELL_AVG_V 
				, CELL_MAX_T 
				, CELL_MIN_T 
				, CELL_AVG_T 	
				, SMTR_TR_CNTER 
				, SMTR_OB_CNTER 		
		-- ]
			FROM T
		), A as (
			SELECT 
				COLEC_TM           
				, DEVICE_ID          
				, DEVICE_TYPE_CD     
				, CASE WHEN PV_PW_H - PREV_PV_PW_H < 0 THEN PV_PW_H ELSE PV_PW_H - PREV_PV_PW_H END PV_PW_H 
				, NULL AS PV_PW_PRICE        
				, NULL AS PV_PW_CO2          
				, CASE WHEN CONS_PW_H - PREV_CONS_PW_H < 0 THEN CONS_PW_H ELSE CONS_PW_H - PREV_CONS_PW_H END CONS_PW_H 
				, NULL AS CONS_PW_PRICE      
				, NULL AS CONS_PW_CO2    
				, NULL AS CONS_GRID_PW_H  
				, NULL AS CONS_GRID_PW_PRICE 
				, NULL AS CONS_GRID_PW_CO2   
				, NULL AS CONS_PV_PW_H       
				, NULL AS CONS_PV_PW_PRICE  
				, NULL AS CONS_PV_PW_CO2     
				, NULL AS CONS_BT_PW_H       
				, NULL AS CONS_BT_PW_PRICE   
				, NULL AS CONS_BT_PW_CO2     
				, CASE WHEN BT_CHRG_PW_H - PREV_BT_CHRG_PW_H < 0 THEN BT_CHRG_PW_H ELSE BT_CHRG_PW_H - PREV_BT_CHRG_PW_H END BT_CHRG_PW_H 
				, NULL AS BT_CHRG_PW_PRICE   
				, NULL AS BT_CHRG_PW_CO2     
				, CASE WHEN BT_DCHRG_PW_H - PREV_BT_DCHRG_PW_H < 0 THEN BT_DCHRG_PW_H ELSE BT_DCHRG_PW_H - PREV_BT_DCHRG_PW_H END BT_DCHRG_PW_H 
				, NULL AS BT_DCHRG_PW_PRICE  
				, NULL AS BT_DCHRG_PW_CO2    
				, CASE WHEN GRID_OB_PW_H - PREV_GRID_OB_PW_H < 0 THEN GRID_OB_PW_H ELSE GRID_OB_PW_H - PREV_GRID_OB_PW_H END GRID_OB_PW_H 
				, NULL AS GRID_OB_PW_PRICE   
				, NULL AS GRID_OB_PW_CO2    
				, CASE WHEN GRID_TR_PW_H - PREV_GRID_TR_PW_H < 0 THEN GRID_TR_PW_H ELSE GRID_TR_PW_H - PREV_GRID_TR_PW_H END GRID_TR_PW_H 
				, NULL AS GRID_TR_PW_PRICE   
				, NULL AS GRID_TR_PW_CO2    
				, NULL AS GRID_TR_PV_PW_H 
				, NULL AS GRID_TR_PV_PW_PRICE 
				, NULL AS GRID_TR_PV_PW_CO2 
				, NULL AS GRID_TR_BT_PW_H 
				, NULL AS GRID_TR_BT_PW_PRICE 
				, NULL AS GRID_TR_BT_PW_CO2 
			 , FN_GET_DIV_GRID_TR( 
			      CASE WHEN PV_PW_H - PREV_PV_PW_H < 0 THEN PV_PW_H ELSE PV_PW_H - PREV_PV_PW_H END 
			     , CASE WHEN BT_DCHRG_PW_H - PREV_BT_DCHRG_PW_H < 0 THEN BT_DCHRG_PW_H ELSE BT_DCHRG_PW_H - PREV_BT_DCHRG_PW_H END 
			     , CASE WHEN GRID_TR_PW_H - PREV_GRID_TR_PW_H < 0 THEN GRID_TR_PW_H ELSE GRID_TR_PW_H - PREV_GRID_TR_PW_H END 
			   ) DIV_GRID_TR 
			 , FN_GET_DIV_CONS ( 
			     CASE WHEN PV_PW_H - PREV_PV_PW_H < 0 THEN PV_PW_H ELSE PV_PW_H - PREV_PV_PW_H END 
			     , CASE WHEN BT_DCHRG_PW_H - PREV_BT_DCHRG_PW_H < 0 THEN BT_DCHRG_PW_H ELSE BT_DCHRG_PW_H - PREV_BT_DCHRG_PW_H END 
			     , CASE WHEN GRID_OB_PW_H - PREV_GRID_OB_PW_H < 0 THEN GRID_OB_PW_H ELSE GRID_OB_PW_H - PREV_GRID_OB_PW_H END 
			     , CASE WHEN CONS_PW_H - PREV_CONS_PW_H < 0 THEN CONS_PW_H ELSE CONS_PW_H - PREV_CONS_PW_H END 
			   ) DIV_CONS 
			-- [ 2015.05.13 yngwie OUTLET_PW 추가
			   , BT_SOC 
			   , CASE WHEN OUTLET_PW_H - PREV_OUTLET_PW_H < 0 THEN OUTLET_PW_H ELSE OUTLET_PW_H - PREV_OUTLET_PW_H END AS OUTLET_PW_H 
			   , NULL AS OUTLET_PW_PRICE 
			   , NULL AS OUTLET_PW_CO2 
			-- ]
				, SYS_EXTRACT_UTC(NOW()) AS CREATE_DT 
			-- [ 2015.07.06 yngwie 수집 컬럼 추가
				, PV_PW 
				, CONS_PW 
				, BT_PW 
				, BT_SOH 
				, GRID_PW 
				, PCS_PW 
				, PCS_TGT_PW 
			    , CASE WHEN PCS_FD_PW_H - PREV_PCS_FD_PW_H < 0 THEN PCS_FD_PW_H ELSE PCS_FD_PW_H - PREV_PCS_FD_PW_H END AS PCS_FD_PW_H 
			    , NULL AS PCS_FD_PW_PRICE 
			    , NULL AS PCS_FD_PW_CO2 
			    , CASE WHEN PCS_PCH_PW_H - PREV_PCS_PCH_PW_H < 0 THEN PCS_PCH_PW_H ELSE PCS_PCH_PW_H - PREV_PCS_PCH_PW_H END AS PCS_PCH_PW_H 
			    , NULL AS PCS_PCH_PW_PRICE 
			    , NULL AS PCS_PCH_PW_CO2 
				, OUTLET_PW 
				, PV_V1 
				, PV_I1 
				, PV_PW1 
				, PV_V2 
				, PV_I2 
				, PV_PW2 
				, INVERTER_V 
				, INVERTER_I 
				, INVERTER_PW 
				, DC_LINK_V 
				, G_RLY_CNT 
				, BAT_RLY_CNT 
				, GRID_TO_GRID_RLY_CNT 
				, BAT_PCHRG_RLY_CNT 
				, RACK_V 
				, RACK_I 
				, CELL_MAX_V 
				, CELL_MIN_V 
				, CELL_AVG_V 
				, CELL_MAX_T 
				, CELL_MIN_T 
				, CELL_AVG_T 
				, SMTR_TR_CNTER 
				, SMTR_OB_CNTER 			
			-- ]
			FROM  B
			WHERE COLEC_TM = i_exe_base
		)
        --  각 전력량의 요금, CO2 와 부하를 PV, GRID, BT 로 나눈 결과를 계산하여 시간별 통계 테이블에 Insert
        INSERT INTO TB_STT_ESS_TM (
			COLEC_TM 
			, DEVICE_ID 
			, DEVICE_TYPE_CD 
			, PV_PW_H 
			, PV_PW_PRICE 
			, PV_PW_CO2 
			, CONS_PW_H 
			, CONS_PW_PRICE 
			, CONS_PW_CO2 
			, CONS_GRID_PW_H 
			, CONS_GRID_PW_PRICE 
			, CONS_GRID_PW_CO2 
			, CONS_PV_PW_H 
			, CONS_PV_PW_PRICE 
			, CONS_PV_PW_CO2 
			, CONS_BT_PW_H 
			, CONS_BT_PW_PRICE 
			, CONS_BT_PW_CO2 
			, BT_CHRG_PW_H 
			, BT_CHRG_PW_PRICE 
			, BT_CHRG_PW_CO2 
			, BT_DCHRG_PW_H 
			, BT_DCHRG_PW_PRICE 
			, BT_DCHRG_PW_CO2 
			, GRID_OB_PW_H 
			, GRID_OB_PW_PRICE 
			, GRID_OB_PW_CO2 
			, GRID_TR_PW_H 
			, GRID_TR_PW_PRICE 
			, GRID_TR_PW_CO2 
			, GRID_TR_PV_PW_H 
			, GRID_TR_PV_PW_PRICE 
			, GRID_TR_PV_PW_CO2 
			, GRID_TR_BT_PW_H 
			, GRID_TR_BT_PW_PRICE 
			, GRID_TR_BT_PW_CO2 
			-- [ 2015.05.13 yngwie OUTLET_PW 추가
			, BT_SOC 
			, OUTLET_PW_H 
			, OUTLET_PW_PRICE 
			, OUTLET_PW_CO2 
			
			, CREATE_DT      
			-- [ 2015.07.06 yngwie 수집 컬럼 추가
			, PV_PW 
			, CONS_PW 
			, BT_PW 
			, BT_SOH 
			, GRID_PW 
			, PCS_PW 
			, PCS_TGT_PW 
			, PCS_FD_PW_H 
			, PCS_FD_PW_PRICE 
			, PCS_FD_PW_CO2 
			, PCS_PCH_PW_H 
			, PCS_PCH_PW_PRICE 
			, PCS_PCH_PW_CO2 
			, OUTLET_PW 
			, PV_V1 
			, PV_I1 
			, PV_PW1 
			, PV_V2 
			, PV_I2 
			, PV_PW2 
			, INVERTER_V 
			, INVERTER_I 
			, INVERTER_PW 
			, DC_LINK_V 
			, G_RLY_CNT 
			, BAT_RLY_CNT 
			, GRID_TO_GRID_RLY_CNT 
			, BAT_PCHRG_RLY_CNT 
			, RACK_V 
			, RACK_I 
			, CELL_MAX_V 
			, CELL_MIN_V 
			, CELL_AVG_V 
			, CELL_MAX_T 
			, CELL_MIN_T 
			, CELL_AVG_T 
			, SMTR_TR_CNTER 
			, SMTR_OB_CNTER 				
			-- ]		   
        ) 
        SELECT 
        	A.COLEC_TM 
        	, A.DEVICE_ID 
        	, A.DEVICE_TYPE_CD 
        	, A.PV_PW_H 
        	, TRUNC(A.PV_PW_H::NUMERIC * ET.UNIT_PRICE * .001, 2) AS PV_PW_PRICE 
        	, TRUNC(A.PV_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS PV_PW_CO2 
        	, A.CONS_PW_H 
        	, TRUNC(A.CONS_PW_H::NUMERIC * E.UNIT_PRICE * .001, 2) AS CONS_PW_PRICE 
        	, TRUNC(A.CONS_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS CONS_PW_CO2 
        	, FN_GET_SPLIT(DIV_CONS, 3, '|')::NUMERIC AS CONS_GRID_PW_H 
        	, TRUNC(FN_GET_SPLIT(DIV_CONS, 3, '|')::NUMERIC * E.UNIT_PRICE * .001, 2) AS CONS_GRID_PW_PRICE 
        	, TRUNC(FN_GET_SPLIT(DIV_CONS, 3, '|')::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS CONS_GRID_PW_CO2 
        	, FN_GET_SPLIT(DIV_CONS, 1, '|')::NUMERIC AS CONS_PV_PW_H 
        	, TRUNC(FN_GET_SPLIT(DIV_CONS, 1, '|')::NUMERIC * E.UNIT_PRICE * .001, 2) AS CONS_PV_PW_PRICE 
        	, TRUNC(FN_GET_SPLIT(DIV_CONS, 1, '|')::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS CONS_PV_PW_CO2 
        	, FN_GET_SPLIT(DIV_CONS, 2, '|')::NUMERIC AS CONS_BT_PW_H 
        	, TRUNC(FN_GET_SPLIT(DIV_CONS, 2, '|')::NUMERIC * E.UNIT_PRICE * .001, 2) AS CONS_BT_PW_PRICE 
        	, TRUNC(FN_GET_SPLIT(DIV_CONS, 2, '|')::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS CONS_BT_PW_CO2 
        	, A.BT_CHRG_PW_H 
        	, TRUNC(A.BT_CHRG_PW_H::NUMERIC * ET.UNIT_PRICE * .001, 2) AS BT_CHRG_PW_PRICE 
        	, TRUNC(A.BT_CHRG_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS BT_CHRG_PW_CO2 
        	, A.BT_DCHRG_PW_H 
        	, TRUNC(A.BT_DCHRG_PW_H::NUMERIC * ET.UNIT_PRICE * .001, 2) AS BT_DCHRG_PW_PRICE 
        	, TRUNC(A.BT_DCHRG_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS BT_DCHRG_PW_CO2 
        	, A.GRID_OB_PW_H 
        	, TRUNC(A.GRID_OB_PW_H::NUMERIC * E.UNIT_PRICE * .001, 2) AS GRID_OB_PW_PRICE 
        	, TRUNC(A.GRID_OB_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS GRID_OB_PW_CO2 
        	, A.GRID_TR_PW_H 
        	, TRUNC(A.GRID_TR_PW_H::NUMERIC * ET.UNIT_PRICE * .001, 2) AS GRID_TR_PW_PRICE 
        	, TRUNC(A.GRID_TR_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS GRID_TR_PW_CO2 
        	, FN_GET_SPLIT(DIV_GRID_TR, 1, '|')::NUMERIC AS GRID_TR_PV_PW_H 
        	, TRUNC(FN_GET_SPLIT(DIV_GRID_TR, 1, '|')::NUMERIC * ET.UNIT_PRICE * .001, 2) AS GRID_TR_PV_PW_PRICE 
        	, TRUNC(FN_GET_SPLIT(DIV_GRID_TR, 1, '|')::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS GRID_TR_PV_PW_CO2 
        	, FN_GET_SPLIT(DIV_GRID_TR, 2, '|')::NUMERIC AS GRID_TR_BT_PW_H 
        	, TRUNC(FN_GET_SPLIT(DIV_GRID_TR, 2, '|')::NUMERIC * ET.UNIT_PRICE * .001, 2) AS GRID_TR_BT_PW_PRICE 
        	, TRUNC(FN_GET_SPLIT(DIV_GRID_TR, 2, '|')::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS GRID_TR_BT_PW_CO2 
		-- [ 2015.05.13 yngwie OUTLET_PW 추가
			, A.BT_SOC 
        	, A.OUTLET_PW_H 
        	, TRUNC(A.OUTLET_PW_H::NUMERIC * E.UNIT_PRICE * .001, 2) AS OUTLET_PW_PRICE 
        	, TRUNC(A.OUTLET_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS OUTLET_PW_CO2 
        -- ]
        	, SYS_EXTRACT_UTC(NOW()) AS CREATE_DT  
		-- [ 2015.07.06 yngwie 수집 컬럼 추가
			, A.PV_PW 
			, A.CONS_PW 
			, A.BT_PW 
			, A.BT_SOH 
			, A.GRID_PW 
			, A.PCS_PW 
			, A.PCS_TGT_PW 
			, A.PCS_FD_PW_H 
        	, TRUNC(A.PCS_FD_PW_H::NUMERIC * ET.UNIT_PRICE * .001, 2) AS PCS_FD_PW_PRICE 
        	, TRUNC(A.PCS_FD_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS PCS_FD_PW_CO2 
			, A.PCS_PCH_PW_H 
        	, TRUNC(A.PCS_PCH_PW_H::NUMERIC * E.UNIT_PRICE * .001, 2) AS PCS_PCH_PW_PRICE 
        	, TRUNC(A.PCS_PCH_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS PCS_PCH_PW_CO2 
			, A.OUTLET_PW 
			, A.PV_V1 
			, A.PV_I1 
			, A.PV_PW1 
			, A.PV_V2 
			, A.PV_I2 
			, A.PV_PW2 
			, A.INVERTER_V 
			, A.INVERTER_I 
			, A.INVERTER_PW 
			, A.DC_LINK_V 
			, A.G_RLY_CNT 
			, A.BAT_RLY_CNT 
			, A.GRID_TO_GRID_RLY_CNT 
			, A.BAT_PCHRG_RLY_CNT 
			, A.RACK_V 
			, A.RACK_I 
			, A.CELL_MAX_V 
			, A.CELL_MIN_V 
			, A.CELL_AVG_V 
			, A.CELL_MAX_T 
			, A.CELL_MIN_T 
			, A.CELL_AVG_T 	
			, A.SMTR_TR_CNTER 
			, A.SMTR_OB_CNTER 		
		-- ]        
        FROM		  A 
		        	, TB_BAS_DEVICE B 
		        	, TB_PLF_CARB_EMI_QUITY D 
		        	, TB_BAS_ELPW_UNIT_PRICE E 
		        	, TB_BAS_ELPW_UNIT_PRICE ET 
        WHERE 	A.DEVICE_ID = B.DEVICE_ID 
        	AND 	B.CNTRY_CD = D.CNTRY_CD 
        	AND 	D.USE_FLAG = 'Y' 
        	AND 	B.ELPW_PROD_CD = E.ELPW_PROD_CD 
        	AND 	E.IO_FLAG = 'O' 
        	AND 	E.USE_FLAG = 'Y' 
        -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가
			AND 	E.WKDAY_FLAG = FN_GET_WKDAY_FLAG(SUBSTR(COLEC_TM, 1, 8))  
		-- 2015.07.31 요금제 년월 변경
        --	AND 	SUBSTR(COLEC_TM, 1, 6) = E.UNIT_YM 
        	AND 	E.UNIT_YM ='000000' 
        	AND 	SUBSTR(COLEC_TM, 9, 2) = E.UNIT_HH 
        	AND 	B.ELPW_PROD_CD = ET.ELPW_PROD_CD 
        	AND 	ET.IO_FLAG = 'T' 
        	AND 	ET.USE_FLAG = 'Y' 
        -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가, T 는  1 만 있음
		-- 2015.08.06 modified by yngwie T 도 주말 0 추가
		--	AND 	ET.WKDAY_FLAG = '1'   
			AND 	ET.WKDAY_FLAG = FN_GET_WKDAY_FLAG(SUBSTR(COLEC_TM, 1, 8)) 
		-- 2015.07.31 요금제 년월 변경
        --	AND 	SUBSTR(COLEC_TM, 1, 6) = ET.UNIT_YM 
        	AND 	ET.UNIT_YM = '000000' 
        	AND 	SUBSTR(COLEC_TM, 9, 2) = ET.UNIT_HH 
        ;
        
        GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;
--		RAISE notice 'i_sp_nm[%], i_site_seq[%], Insert TB_STT_ESS_TM Table 입력건수[%]'	, i_sp_nm, i_site_seq, v_work_num;

   		v_rslt_cd := 'C';

	    -------------------------------------------------
	    --- 일별 통계 집계 프로시저 수행 		
	    -------------------------------------------------
--		RAISE notice 'i_sp_nm[%], i_site_seq[%], Call SP_STATS_DD 입력일자[%]', i_sp_nm, i_site_seq, substring(i_exe_base, 1, 8);
   		PERFORM SP_STATS_DD('SP_STATS_DD', 0, SUBSTR(i_exe_base, 1, 8), i_exe_mthd);

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_stats_tm(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1080 (class 1255 OID 1895928)
-- Name: sp_stats_yy(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_stats_yy(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_STATS_YY
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-26   yngwie       1. Created this function.
   2.0        2019-09-20   박동환        Postgresql 변환

   NOTES:   시간별 통계 데이터를 가지고 년간 통계 데이터를 생성한다.
            PKG_OPR_ESS.SP_AGGR_STATE 가 끝난 후에 실행 (5분 주기).

            Source Table : TB_STT_ESS_TM
            Target Table : TB_STT_ESS_YY

    Input : i_exe_base VARCHAR 집계 대상 년

	Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_from			VARCHAR   := NULL; 
    v_to				VARCHAR   := NULL; 
    v_row           	record; 
   
BEGIN
	SELECT SUBSTR(i_exe_base, 1, 4) INTO i_exe_base;

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN  
		v_rslt_cd := 'A';
--	    RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] START[%] i_exe_base[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base;

    	FOR v_row IN ( 
    		-- Tibero에선 m_offset_sql
		    SELECT  UTC_OFFSET  
		    FROM (  
		    	SELECT	0 AS UTC_OFFSET  
		    	
		    	UNION ALL    
		    	SELECT  UTC_OFFSET  
		    	FROM   TB_BAS_CITY  
		    	WHERE  UTC_OFFSET IS NOT NULL  
		    	
		    	UNION ALL  
		    	SELECT  UTC_OFFSET + 1 
		    	FROM  TB_BAS_CITY  
		    	WHERE UTC_OFFSET IS NOT NULL  
		    )  T
		    GROUP by UTC_OFFSET 
		    
		) LOOP

	        -- 추가할 데이터의 시작, 종료일시를 구한다.
	        v_from := wems.fn_ts_str_to_str(CONCAT_WS('', i_exe_base, '010100'), v_row.utc_offset, 'YYYYMMDDHH24'::TEXT, 0, 'YYYYMMDDHH24'::TEXT);
	        v_to := wems.fn_ts_str_to_str(CONCAT_WS('', i_exe_base, '123123'), v_row.utc_offset, 'YYYYMMDDHH24'::TEXT, 0, 'YYYYMMDDHH24'::TEXT);
--	      	RAISE notice 'i_sp_nm[%] i_site_seq[%] Merge TB_STT_ESS_MM Table UTC_OFFSET[%] 시작[%] 종료[%]', i_sp_nm, i_site_seq, v_row.utc_offset, v_from, v_to;
	       
    		-- 시간별 통계 데이터를 SUM 해서 연간 통계 테이블에 Merge      
    		WITH AR AS ( 
				SELECT  * 
				FROM (  
					SELECT	A.*  
								, ROW_NUMBER() OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM DESC) AS RNUM  
					FROM  TB_STT_ESS_TM A 
					WHERE COLEC_TM BETWEEN v_from AND v_to
				)  z
				WHERE RNUM = 1  
			) , B AS (
				SELECT 
					  i_exe_base AS COLEC_YY    
					, v_row.utc_offset AS UTC_OFFSET   
					, A.DEVICE_ID    
					, A.DEVICE_TYPE_CD    
					, SUM(A.PV_PW_H) AS PV_PW_H     
					, TRUNC(SUM(A.PV_PW_PRICE), 2) AS PV_PW_PRICE     
					, TRUNC(SUM(A.PV_PW_CO2), 2) AS PV_PW_CO2  
					, SUM(A.CONS_PW_H) AS CONS_PW_H   
					, TRUNC(SUM(A.CONS_PW_PRICE), 2) AS CONS_PW_PRICE   
					, TRUNC(SUM(A.CONS_PW_CO2), 2) AS CONS_PW_CO2     
					, SUM(A.CONS_GRID_PW_H) AS CONS_GRID_PW_H  
					, TRUNC(SUM(A.CONS_GRID_PW_PRICE), 2) AS CONS_GRID_PW_PRICE     
					, TRUNC(SUM(A.CONS_GRID_PW_CO2), 2) AS CONS_GRID_PW_CO2   
					, SUM(A.CONS_PV_PW_H) AS CONS_PV_PW_H    
					, TRUNC(SUM(A.CONS_PV_PW_PRICE), 2) AS CONS_PV_PW_PRICE   
					, TRUNC(SUM(A.CONS_PV_PW_CO2), 2) AS CONS_PV_PW_CO2  
					, SUM(A.CONS_BT_PW_H) AS CONS_BT_PW_H    
					, TRUNC(SUM(A.CONS_BT_PW_PRICE), 2) AS CONS_BT_PW_PRICE   
					, TRUNC(SUM(A.CONS_BT_PW_CO2), 2) AS CONS_BT_PW_CO2  
					, SUM(A.BT_CHRG_PW_H) AS BT_CHRG_PW_H    
					, TRUNC(SUM(A.BT_CHRG_PW_PRICE), 2) AS BT_CHRG_PW_PRICE   
					, TRUNC(SUM(A.BT_CHRG_PW_CO2), 2) AS BT_CHRG_PW_CO2  
					, SUM(A.BT_DCHRG_PW_H) AS BT_DCHRG_PW_H   
					, TRUNC(SUM(A.BT_DCHRG_PW_PRICE), 2) AS BT_DCHRG_PW_PRICE  
					, TRUNC(SUM(A.BT_DCHRG_PW_CO2), 2) AS BT_DCHRG_PW_CO2  
					, SUM(A.GRID_OB_PW_H) AS GRID_OB_PW_H    
					, TRUNC(SUM(A.GRID_OB_PW_PRICE), 2) AS GRID_OB_PW_PRICE   
					, TRUNC(SUM(A.GRID_OB_PW_CO2), 2) AS GRID_OB_PW_CO2  
					, SUM(A.GRID_TR_PW_H) AS GRID_TR_PW_H    
					, TRUNC(SUM(A.GRID_TR_PW_PRICE), 2) AS GRID_TR_PW_PRICE   
					, TRUNC(SUM(A.GRID_TR_PW_CO2), 2) AS GRID_TR_PW_CO2  
					, SUM(A.GRID_TR_PV_PW_H) AS GRID_TR_PV_PW_H  
					, TRUNC(SUM(A.GRID_TR_PV_PW_PRICE), 2) AS GRID_TR_PV_PW_PRICE    
					, TRUNC(SUM(A.GRID_TR_PV_PW_CO2), 2) AS GRID_TR_PV_PW_CO2  
					, SUM(A.GRID_TR_BT_PW_H) AS GRID_TR_BT_PW_H  
					, TRUNC(SUM(A.GRID_TR_BT_PW_PRICE), 2) AS GRID_TR_BT_PW_PRICE    
					, TRUNC(SUM(A.GRID_TR_BT_PW_CO2), 2) AS GRID_TR_BT_PW_CO2    
				    , MAX(AR.BT_SOC) AS BT_SOC  
					, SUM(A.OUTLET_PW_H) AS OUTLET_PW_H    
					, TRUNC(SUM(A.OUTLET_PW_PRICE), 2) AS OUTLET_PW_PRICE   
					, TRUNC(SUM(A.OUTLET_PW_CO2), 2) AS OUTLET_PW_CO2 
					, SYS_EXTRACT_UTC(NOW()) AS CREATE_DT  
					, SUM(A.PCS_FD_PW_H)			AS PCS_FD_PW_H  
					, TRUNC(SUM(A.PCS_FD_PW_PRICE), 2)		AS PCS_FD_PW_PRICE  
					, TRUNC(SUM(A.PCS_FD_PW_CO2), 2)			AS PCS_FD_PW_CO2
					, SUM(A.PCS_PCH_PW_H)			AS PCS_PCH_PW_H
					, TRUNC(SUM(A.PCS_PCH_PW_PRICE), 2)		AS PCS_PCH_PW_PRICE
					, TRUNC(SUM(A.PCS_PCH_PW_CO2), 2)		AS PCS_PCH_PW_CO2
				FROM 
					 TB_STT_ESS_TM A    
					, TB_BAS_DEVICE B -- 살아있는 장비만 조회하기 위해
					, AR 
				WHERE 	A.COLEC_TM BETWEEN v_from AND v_to 
				AND 		A.DEVICE_ID = B.DEVICE_ID   
				AND 		A.DEVICE_TYPE_CD = B.DEVICE_TYPE_CD   
				AND 		A.DEVICE_ID = AR.DEVICE_ID    
				AND 		A.DEVICE_TYPE_CD = AR.DEVICE_TYPE_CD 
				GROUP BY A.DEVICE_ID, A.DEVICE_TYPE_CD   
			), U as (
				UPDATE TB_STT_ESS_YY yy
				SET  
					 PV_PW_H              = B.PV_PW_H               
					, PV_PW_PRICE          = B.PV_PW_PRICE           
					, PV_PW_CO2            = B.PV_PW_CO2             
					, CONS_PW_H            = B.CONS_PW_H             
					, CONS_PW_PRICE        = B.CONS_PW_PRICE         
					, CONS_PW_CO2          = B.CONS_PW_CO2           
					, CONS_GRID_PW_H       = B.CONS_GRID_PW_H        
					, CONS_GRID_PW_PRICE   = B.CONS_GRID_PW_PRICE    
					, CONS_GRID_PW_CO2     = B.CONS_GRID_PW_CO2      
					, CONS_PV_PW_H         = B.CONS_PV_PW_H          
					, CONS_PV_PW_PRICE     = B.CONS_PV_PW_PRICE      
					, CONS_PV_PW_CO2       = B.CONS_PV_PW_CO2        
					, CONS_BT_PW_H         = B.CONS_BT_PW_H          
					, CONS_BT_PW_PRICE     = B.CONS_BT_PW_PRICE      
					, CONS_BT_PW_CO2       = B.CONS_BT_PW_CO2        
					, BT_CHRG_PW_H         = B.BT_CHRG_PW_H          
					, BT_CHRG_PW_PRICE     = B.BT_CHRG_PW_PRICE      
					, BT_CHRG_PW_CO2       = B.BT_CHRG_PW_CO2        
					, BT_DCHRG_PW_H        = B.BT_DCHRG_PW_H         
					, BT_DCHRG_PW_PRICE    = B.BT_DCHRG_PW_PRICE     
					, BT_DCHRG_PW_CO2      = B.BT_DCHRG_PW_CO2       
					, GRID_OB_PW_H         	= B.GRID_OB_PW_H          
					, GRID_OB_PW_PRICE     	= B.GRID_OB_PW_PRICE      
					, GRID_OB_PW_CO2       	= B.GRID_OB_PW_CO2        
					, GRID_TR_PW_H         	= B.GRID_TR_PW_H          
					, GRID_TR_PW_PRICE     	= B.GRID_TR_PW_PRICE      
					, GRID_TR_PW_CO2       	= B.GRID_TR_PW_CO2        
					, GRID_TR_PV_PW_H      	= B.GRID_TR_PV_PW_H       
					, GRID_TR_PV_PW_PRICE  = B.GRID_TR_PV_PW_PRICE   
					, GRID_TR_PV_PW_CO2   = B.GRID_TR_PV_PW_CO2     
					, GRID_TR_BT_PW_H      	= B.GRID_TR_BT_PW_H       
					, GRID_TR_BT_PW_PRICE  = B.GRID_TR_BT_PW_PRICE   
					, GRID_TR_BT_PW_CO2    = B.GRID_TR_BT_PW_CO2   
				    , BT_SOC               		= B.BT_SOC  
				    , OUTLET_PW_H          	= B.OUTLET_PW_H   
				    , OUTLET_PW_PRICE      	= B.OUTLET_PW_PRICE   
				    , OUTLET_PW_CO2        	= B.OUTLET_PW_CO2   
					, CREATE_DT            		= B.CREATE_DT    
					, PCS_FD_PW_H				= B.PCS_FD_PW_H  
					, PCS_FD_PW_PRICE 		= B.PCS_FD_PW_PRICE  
					, PCS_FD_PW_CO2 			= B.PCS_FD_PW_CO2  
					, PCS_PCH_PW_H 			= B.PCS_PCH_PW_H  
					, PCS_PCH_PW_PRICE 		= B.PCS_PCH_PW_PRICE  
					, PCS_PCH_PW_CO2 		= B.PCS_PCH_PW_CO2  	
		      	FROM 	B
				WHERE 	yy.COLEC_YY 			= B.COLEC_YY  
				AND 		yy.UTC_OFFSET 		= B.UTC_OFFSET  
				AND 		yy.DEVICE_ID 			= B.DEVICE_ID  
				AND 		yy.DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
				RETURNING yy.*
			)
			INSERT into TB_STT_ESS_YY (  
				  COLEC_YY  
				, UTC_OFFSET      
				, DEVICE_ID   
				, DEVICE_TYPE_CD   
				, PV_PW_H               
				, PV_PW_PRICE           
				, PV_PW_CO2             
				, CONS_PW_H             
				, CONS_PW_PRICE         
				, CONS_PW_CO2           
				, CONS_GRID_PW_H        
				, CONS_GRID_PW_PRICE    
				, CONS_GRID_PW_CO2      
				, CONS_PV_PW_H          
				, CONS_PV_PW_PRICE      
				, CONS_PV_PW_CO2        
				, CONS_BT_PW_H          
				, CONS_BT_PW_PRICE      
				, CONS_BT_PW_CO2        
				, BT_CHRG_PW_H          
				, BT_CHRG_PW_PRICE      
				, BT_CHRG_PW_CO2        
				, BT_DCHRG_PW_H         
				, BT_DCHRG_PW_PRICE     
				, BT_DCHRG_PW_CO2       
				, GRID_OB_PW_H          
				, GRID_OB_PW_PRICE      
				, GRID_OB_PW_CO2        
				, GRID_TR_PW_H          
				, GRID_TR_PW_PRICE      
				, GRID_TR_PW_CO2        
				, GRID_TR_PV_PW_H       
				, GRID_TR_PV_PW_PRICE   
				, GRID_TR_PV_PW_CO2     
				, GRID_TR_BT_PW_H       
				, GRID_TR_BT_PW_PRICE   
				, GRID_TR_BT_PW_CO2   
			    , BT_SOC   
			    , OUTLET_PW_H   
			    , OUTLET_PW_PRICE  
			    , OUTLET_PW_CO2  
				, CREATE_DT    
				, PCS_FD_PW_H			 
				, PCS_FD_PW_PRICE 	 
				, PCS_FD_PW_CO2 		 
				, PCS_PCH_PW_H 		 
				, PCS_PCH_PW_PRICE 	 
				, PCS_PCH_PW_CO2 		 			
			) 
			SELECT   
				  B.COLEC_YY  
				, B.UTC_OFFSET      
				, B.DEVICE_ID   
				, B.DEVICE_TYPE_CD   
				, B.PV_PW_H               
				, B.PV_PW_PRICE           
				, B.PV_PW_CO2             
				, B.CONS_PW_H             
				, B.CONS_PW_PRICE         
				, B.CONS_PW_CO2           
				, B.CONS_GRID_PW_H        
				, B.CONS_GRID_PW_PRICE    
				, B.CONS_GRID_PW_CO2      
				, B.CONS_PV_PW_H          
				, B.CONS_PV_PW_PRICE      
				, B.CONS_PV_PW_CO2        
				, B.CONS_BT_PW_H          
				, B.CONS_BT_PW_PRICE      
				, B.CONS_BT_PW_CO2        
				, B.BT_CHRG_PW_H          
				, B.BT_CHRG_PW_PRICE      
				, B.BT_CHRG_PW_CO2        
				, B.BT_DCHRG_PW_H         
				, B.BT_DCHRG_PW_PRICE     
				, B.BT_DCHRG_PW_CO2       
				, B.GRID_OB_PW_H          
				, B.GRID_OB_PW_PRICE      
				, B.GRID_OB_PW_CO2        
				, B.GRID_TR_PW_H          
				, B.GRID_TR_PW_PRICE      
				, B.GRID_TR_PW_CO2        
				, B.GRID_TR_PV_PW_H       
				, B.GRID_TR_PV_PW_PRICE   
				, B.GRID_TR_PV_PW_CO2     
				, B.GRID_TR_BT_PW_H       
				, B.GRID_TR_BT_PW_PRICE   
				, B.GRID_TR_BT_PW_CO2   
			    , B.BT_SOC   
			    , B.OUTLET_PW_H   
			    , B.OUTLET_PW_PRICE  
			    , B.OUTLET_PW_CO2  
				, B.CREATE_DT  
				, B.PCS_FD_PW_H			 
				, B.PCS_FD_PW_PRICE 	 
				, B.PCS_FD_PW_CO2 		 
				, B.PCS_PCH_PW_H 		 
				, B.PCS_PCH_PW_PRICE 	 
				, B.PCS_PCH_PW_CO2	
			FROM 	B
			WHERE NOT EXISTS (  SELECT 1 
			    FROM 	U 
				WHERE	COLEC_YY 			= B.COLEC_YY  
					AND 	UTC_OFFSET 		= B.UTC_OFFSET  
					AND 	DEVICE_ID			= B.DEVICE_ID  
					AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
			  )
			 ;
    
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
           
--	      	RAISE notice 'i_sp_nm[%] Merge TB_STT_ESS_YY Table [%]건, UTC_OFFSET[%] 시작[%] 종료[%]', i_sp_nm, v_work_num, v_row.utc_offset, v_from, v_to;
    	
    	END LOOP;
   		
   		-- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS then
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_stats_yy(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1081 (class 1255 OID 1895930)
-- Name: sp_sumup_stat(text); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_sumup_stat(p_dt text)
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_SUMUP_STAT
   PURPOSE:

   REVISIONS:
   Ver        Date            Author           Description
   ---------  ----------------  ---------------  ------------------------------------
   1.0        2014-07-08   yngwie        Created this function.
   1.1        2019-10-01   이재형         convert postgresql

   NOTES:   통계 재집계용

	Input : p_dt VARCHAR 재집계 시작일

	Usage : exec SP_SUMUP_STAT('2014/08/20');

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_SUMUP_STAT';
    v_log_seq 		NUMERIC := 0;		-- nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'M';	-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_last_dt			TIMESTAMP := NULL;  					-- 최종작업시간
    v_rslt_cd			VARCHAR   	:= '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   	:= 0;     						-- 적재수
    v_work_num		INTEGER   	:= 0;     						-- 처리건수
    v_err_msg		VARCHAR   	:= NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   	:= NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   	:= NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   	:= NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_date 			RECORD;
    v_startdt 			CHARACTER VARYING(20);  
   
BEGIN
    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';

	    --시작일자
	    v_startdt := p_dt;
	
	   -- 시간별 통계 생성
		for v_date in 
	   		select to_char(generate_series(v_startdt, now()- interval '1 day', '1 hour'),'YYYYMMDDHH24') tm
	   	loop
	   		PERFORM SP_STATS_TM(v_date.tm);
	     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   	end loop;
	   
	   	-- 일별 통계 생성
	   	for v_date in 
	   		select to_char(generate_series(v_startdt, now()- interval '1 day', '1 day'),'YYYYMMDD') tm
	   	loop
	   		PERFORM  SP_STATS_DD(v_date.tm);
	     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   	end loop;  
	  
	   -- 월간 통계 생성
	   	for v_date in 
	   		select to_char(generate_series(v_startdt, now(), '1 month'),'YYYYMM') tm
	   	loop
	   		PERFORM  SP_STATS_MM(v_date.tm);
	     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   	end loop; 
	  
	   	for v_date in 
	   		select to_char(generate_series(v_startdt, now(), '1 year'),'YYYY') tm
	   	loop
	   		PERFORM  SP_STATS_YY(v_date.tm);
	     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   	end loop; 
	
	  /* 예측 업무 배제 - QCELLS 
	    -- 시간별 예측통계 생성
	   for v_date in select to_char(generate_series(v_startdt, now()- interval '1 day', '1 hour'),'YYYYMMDDHH24') tm
	   loop
	   		perform  wems.sp_predict_tm(v_date.tm);
	   end loop;
	
	   -- 일별 예측통계 생성
	   for v_date in select to_char(generate_series(v_startdt, now()- interval '1 day', '1 day'),'YYYYMMDD') tm
	   loop
	   		perform  wems.sp_predict_dd(v_date.tm);
	   end loop;  
	
	   -- 월간 예측통계 생성
	   for v_date in select to_char(generate_series(v_startdt, now(), '1 month'),'YYYYMM') tm
	   loop
	   		perform  wems.sp_predict_mm(v_date.tm);
	   end loop; 
	   */
	
	   	-- 일별 순위통계 생성 
	   	for v_date in 
	   		select to_char(generate_series(v_startdt, now()- interval '1 day', '1 day'),'YYYYMMDD') tm
	   	loop
	   		PERFORM SP_RANK_DAILY(v_date.tm);
	     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   	end loop;
	
	   	-- 월별 순위통계 생성
	   	for v_date in 
	   		select to_char(generate_series(v_startdt, now(), '1 month'),'YYYYMM') tm
	   	loop
	   		PERFORM SP_RANK_MONTHLY('SP_RANK_MONTHLY', 0, v_date.tm, 'M');
	     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	   	end loop;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS then
--          	RAISE notice 'v_log_fn[%] EXCEPTION !!! ', v_log_fn;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/


    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$$;


ALTER PROCEDURE wems.sp_sumup_stat(p_dt text) OWNER TO wems;

--
-- TOC entry 1082 (class 1255 OID 1895932)
-- Name: sp_sys_add_partition(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_sys_add_partition(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_SYS_ADD_PARTITION
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-09-15   yngwie       1. Created this function.
   2.0        2019-09-24   박동환        Postgresql 변환

   NOTES:   일/월별 파티션 자동 생성
              매일 GMT 01시에 수행.
   
    Input : 
	        
	Output : 
	
******************************************************************************/	
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
   -- 미리 생성할 일수
--    v_daily_size 		INTEGER := 14;
--    v_monthly_size INTEGER := 2;
   	
    v_row           	record; 
   	v_len				INTEGER := 0;
   	v_tz_offset		NUMERIC := TO_CHAR(NOW() - TIMEZONE('UTC', NOW()), 'HH24')::NUMERIC;
   	v_sql				VARCHAR   := NULL;
   	v_nextDateStr	VARCHAR   := NULL; 
   	v_start_value	VARCHAR   := NULL;
   	v_end_value		VARCHAR   := NULL;
  
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

--	    RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] tz_offset[%] START[%]', i_sp_nm, i_site_seq, v_tz_offset, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	    FOR v_row IN ( 
			WITH PLF AS (
				SELECT M.TABLE_NM
						, M.PARTITION_TYPE
						, M.STD_COL_NM
						, M.STD_COL_TYPE
						, 'PK'||SUBSTRING(M.TABLE_NM, 3, LENGTH(M.TABLE_NM)-2) INDEX_NM
				   		, MAX(CHILD.RELNAME)		LAST_PARTITION
					FROM 	PG_INHERITS
				    JOIN 		PG_CLASS PARENT ON PG_INHERITS.INHPARENT = PARENT.OID
				    JOIN 		PG_CLASS CHILD   ON PG_INHERITS.INHRELID   = CHILD.OID
				    JOIN 		TB_PLF_TBL_META M ON (PARENT.RELNAME= LOWER(M.TABLE_NM)  )
				    WHERE	M.PARTITION_TYPE IS NOT NULL
				    GROUP BY  M.TABLE_NM, M.PARTITION_TYPE, M.STD_COL_NM	, M.STD_COL_TYPE
			), BAS AS (
				SELECT 	*
							, STRING_TO_ARRAY(LAST_PARTITION, '_') RELARR
				FROM 	PLF
			), PT AS (
				SELECT *
						, RELARR[ARRAY_UPPER(RELARR,1)] 					  LAST_PT_DATE_STR
						, CHAR_LENGTH(RELARR[ARRAY_UPPER(RELARR,1)]) FMNUM
						, CASE WHEN PARTITION_TYPE = 'M' THEN 
									AGE( NOW()+INTERVAL '2 MONTH', TO_TIMESTAMP(RELARR[ARRAY_UPPER(RELARR,1)] , 'YYYYMM'))
							  	 ELSE 
							  	 	AGE( NOW()+INTERVAL '14 DAY', TO_TIMESTAMP(RELARR[ARRAY_UPPER(RELARR,1)], 'YYYYMMDD'))
						  		END AGES
				FROM BAS
			)
			SELECT TABLE_NM as TABLE_NAME
					, LAST_PARTITION
					, LAST_PT_DATE_STR
					, CASE WHEN PARTITION_TYPE = 'M' THEN 
										EXTRACT(YEAR FROM AGES)*12 + EXTRACT(MONTH FROM AGES)
						  	 ELSE
						  	 			EXTRACT(DAY FROM NOW()+INTERVAL '14 DAY' - TO_TIMESTAMP(LAST_PT_DATE_STR, 'YYYYMMDD'))
							 END DIFF_SIZE
					, CASE WHEN FMNUM = 4 THEN 'YYYY' 
						  WHEN FMNUM = 6 THEN 'YYYYMM' 
						  WHEN FMNUM = 8 THEN 'YYYYMMDD' 
						  WHEN FMNUM = 10 THEN 'YYYYMMDDHH24'
						  ELSE  ''
				    END AS NEXT_DATE_FMT
					, FN_LONG2CHAR(LAST_PARTITION) as BOUND
					, PARTITION_TYPE
					, STD_COL_NM
					, STD_COL_TYPE
					, INDEX_NM
			 FROM PT
			 
		) LOOP
			v_len := LENGTH(v_row.BOUND) - 2;
			IF v_row.DIFF_SIZE > 0 THEN
	            	--  2015.03.30 yngwie
	            	-- 가장 최근에 만들어진 파티션의 날짜(LAST_PT_DATE_STR) 부터 파티션이 생성되어야 하는 날짜 (v_max...Str) 까지 loop 를 돌며 파티션을 생성한다.

	            	FOR v_loopCnt IN 1..v_row.DIFF_SIZE LOOP
--						RAISE notice 'i_sp_nm[%] v_loopCnt{%] TABLE_NAME[%] LAST_PT_DATE_STR[%] NEXT_DATE_FMT[%] tz_offset[%] DIFF_SIZE[%]', 
--											i_sp_nm, v_loopCnt, v_row.TABLE_NAME, v_row.LAST_PT_DATE_STR, v_row.NEXT_DATE_FMT, v_tz_offset, v_row.DIFF_SIZE ;

						-- 다음 번 파티션을 만들 날짜 문자열을 생성한다.
	            		IF v_row.PARTITION_TYPE = 'D' THEN
	            			v_nextDateStr := TO_CHAR(TO_DATE(v_row.LAST_PT_DATE_STR, v_row.NEXT_DATE_FMT) + concat(v_loopCnt,' DAY')::INTERVAL, v_row.NEXT_DATE_FMT);
	            		ELSE
	            			v_nextDateStr := TO_CHAR(TO_DATE(v_row.LAST_PT_DATE_STR, v_row.NEXT_DATE_FMT) + concat(v_loopCnt,' MONTH')::INTERVAL, v_row.NEXT_DATE_FMT);
	            		END IF;

		--				RAISE notice 'i_sp_nm[%] v_loopCnt{%] TABLE_NAME[%] LAST_PT_DATE_STR[%] NEXT_DATE_FMT[%] tz_offset[%]', i_sp_nm, v_loopCnt, v_row.TABLE_NAME, v_row.LAST_PT_DATE_STR, v_row.NEXT_DATE_FMT, v_tz_offset ;

		                IF v_row.STD_COL_TYPE = 'VARCHAR' THEN

	--		                RAISE notice 'i_sp_nm[%] v_loopCnt{%] STD_COL_TYPE[%]', i_sp_nm, v_loopCnt, v_row.STD_COL_TYPE ;

		                    IF v_row.PARTITION_TYPE = 'D' THEN
--				                RAISE notice 'i_sp_nm[%] v_loopCnt{%] D >>> v_nextDateStr[%]', i_sp_nm, v_loopCnt, v_nextDateStr ;
				               
		                        IF v_len = 8 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 DAY'::INTERVAL, 'YYYYMMDD');
		                        ELSIF v_len = 10 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 DAY'::INTERVAL, 'YYYYMMDDHH24');
		                        ELSIF v_len = 14 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 DAY'::INTERVAL, 'YYYYMMDDHH24MISS');
		                        END IF;
		                    ELSIF v_row.PARTITION_TYPE = 'M' THEN
	--			                RAISE notice 'i_sp_nm[%] v_loopCnt{%] M >>> v_nextDateStr[%]', i_sp_nm, v_loopCnt, v_nextDateStr ;
				               
		                        IF v_len = 6 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 MONTH'::INTERVAL, 'YYYYMM');
		                        ELSIF v_len = 8 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 MONTH'::INTERVAL, 'YYYYMMDD');
		                        ELSIF v_len = 10 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 MONTH'::INTERVAL, 'YYYYMMDDHH24');
		                        ELSIF v_len = 14 THEN
		                            v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 MONTH'::INTERVAL, 'YYYYMMDDHH24MISS');
		                        END IF;
		                    END IF;

		                ELSIF v_row.STD_COL_TYPE = 'TIMESTAMP' then
			               IF v_row.PARTITION_TYPE = 'D' THEN
   		                       v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 DAY'::INTERVAL, 'YYYY-MM-DD HH24:MI:SS');
				               v_nextDateStr := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT), 'YYYY-MM-DD');
		                    ELSIF v_row.PARTITION_TYPE = 'M' THEN
			                   --  ('2019-09-01 00:00:00') TO ('2019-10-01 00:00:00')
   		                       v_end_value := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT) + '1 MONTH'::INTERVAL, 'YYYY-MM-DD HH24:MI:SS');
				               v_nextDateStr := TO_CHAR(TO_DATE(v_nextDateStr, v_row.NEXT_DATE_FMT), 'YYYY-MM');
		                    END IF;
		                   
--			                RAISE notice 'i_sp_nm[%] TIMESTAMP >>> v_tz_offset{%] v_nextDateStr[%]  v_end_value[%]', i_sp_nm, v_tz_offset, v_nextDateStr, v_end_value ;
		                END IF;
		            
		               	v_start_value := v_nextDateStr;
		               	IF LENGTH(v_start_value) < LENGTH(v_end_value) THEN
			               v_start_value := v_start_value || SUBSTRING(v_end_value, LENGTH(v_start_value)+1);
						END IF;
					
						/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
						Oracle Style 불가 : ALTER TABLE TB_STT_ESS_TM ATTACH PARTITION PT_STT_ESS_TM_20190829 VALUES LESS THAN ('2019083000');
						
			           	CREATE TABLE tb_opr_ess_state_hist_pt_opr_ess_state_hist_20190827 PARTITION OF tb_opr_ess_state_hist FOR VALUES FROM ('20190827') TO ('20190828');							
						--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
	--	            	RAISE notice 'i_sp_nm[%] v_loopCnt{%] M >>> TABLE_NAME[%] nextDateStr[%] value_date[%]', i_sp_nm, v_loopCnt, v_row.TABLE_NAME, v_nextDateStr, v_end_value ;
						v_sql := FORMAT(	'CREATE TABLE PT_%s_%s PARTITION OF %s FOR VALUES FROM(%s) TO(%s);'
									, REPLACE(v_row.TABLE_NAME, 'TB_', '')
									, REPLACE(v_nextDateStr, '-', '')
									, v_row.TABLE_NAME
									, CHR(39)||v_start_value||CHR(39)
									, CHR(39)||v_end_value||CHR(39)
								);
						
						--RAISE notice 'i_sp_nm[%] ADD Partition :  SQL[%]', i_sp_nm, v_sql;
						EXECUTE v_sql;		            
			           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
				       	v_rslt_cnt := v_rslt_cnt + v_work_num;	
		
	            	END LOOP;
	            END IF;
	   END LOOP;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   	/***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_sys_add_partition(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1162 (class 1255 OID 1895934)
-- Name: sp_sys_remove_old_records(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_sys_remove_old_records(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
  NAME:       SP_SYS_REMOVE_OLD_RECORDS
  PURPOSE:

  REVISIONS:
  Ver        Date        Author           Description
  ---------  ----------  ---------------  ------------------------------------
  1.0        2014-09-16   yngwie       1. Created this function.
  2.0        2019-10-01   박동환        Postgresql 변환

  NOTES:   메타테이블(TB_PLF_TBL_META) 기준으로 보관기간 경과 레코드 삭제

   Input :

Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_row           	record; 
   	v_sql				VARCHAR   := NULL;
  
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

	    --RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] START[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	    FOR v_row IN ( 
			SELECT
				A.TABLE_NM
				, A.STD_COL_NM
				, A.STD_COL_TYPE
				, COALESCE(B.CHARACTER_MAXIMUM_LENGTH, 17) AS DATA_LENGTH
				, FN_GET_DATEFORMAT(COALESCE(B.CHARACTER_MAXIMUM_LENGTH, 17)) AS DATE_FMT
				, A.DEL_CYCLE
			FROM
				TB_PLF_TBL_META A
				, INFORMATION_SCHEMA.COLUMNS B
			WHERE	A.DEL_FLAG = 'Y'
				AND 	A.DEL_CYCLE > 0
				AND 	A.TABLE_NM = UPPER(B.TABLE_NAME)
				AND 	A.STD_COL_NM = UPPER(B.COLUMN_NAME)
				AND 	PARTITION_TYPE IS null
		--		AND 	table_schema='wems'
		--	order by TABLE_NM;
			 
		) LOOP
           IF v_row.STD_COL_TYPE = 'VARCHAR' then
               -- DELETE FROM TB_SYS_EXEC_HIST WHERE CREATE_DT < TO_CHAR(DATE_TRUNC(''DAY'', SYS_EXTRACT_UTC(NOW()) - 1 * '30 DAY'::INTERVAL), 'YYYYMMDDHH24MISS') ;
              	v_sql := FORMAT(	'DELETE FROM %s WHERE %s < TO_CHAR(DATE_TRUNC(''DAY'', SYS_EXTRACT_UTC(NOW()) - %s * ''30 DAY''::INTERVAL), %s) ;'
              				, v_row.TABLE_NM
              				, v_row.STD_COL_NM
              				, v_row.DEL_CYCLE
							, CHR(39)||v_row.DATE_FMT||CHR(39)
						); 
               
            ELSIF v_row.STD_COL_TYPE = 'TIMESTAMP' THEN
               -- DELETE FROM TB_SYS_EXEC_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
              	v_sql := FORMAT(	'DELETE FROM %s WHERE %s < DATE_TRUNC(''DAY'', SYS_EXTRACT_UTC(NOW()) - %s * ''30 DAY''::INTERVAL) ;'
              				, v_row.TABLE_NM
              				, v_row.STD_COL_NM
              				, v_row.DEL_CYCLE
						); 
            END IF;

--	       	RAISE NOTICE 'i_sp_nm[%] REMOVE old records[%]', i_sp_nm, v_sql;
	       
	       /*
	        SELECT * FROM TB_RAW_WEATHER WHERE TIME_FROM < TO_CHAR(DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 1 * '30 DAY'::INTERVAL), 'YYYYMMDDHH24MISS') ;
			SELECT * FROM TB_RAW_WEATHER_REQHIST WHERE CREATE_TM < TO_CHAR(DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 1 * '30 DAY'::INTERVAL), 'YYYYMMDDHH24MISS') ;
			SELECT * FROM TB_STT_ESS_YY WHERE COLEC_YY < TO_CHAR(DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 24 * '30 DAY'::INTERVAL), 'YYYY') ;
			SELECT * FROM TB_STT_QUITY_WRTY_DD WHERE COLEC_DD < TO_CHAR(DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 36 * '30 DAY'::INTERVAL), 'YYYYMMDD') ;
			SELECT * FROM TB_OPR_ESS_PREDICT_HIST WHERE COLEC_DT < TO_CHAR(DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 3 * '30 DAY'::INTERVAL), 'YYYYMMDDHH24MISS') ;
			SELECT * FROM TB_RAW_CURR_WEATHER WHERE COLEC_DT < TO_CHAR(DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 1 * '30 DAY'::INTERVAL), 'YYYYMMDDHH24MISS') ;
			
			SELECT * FROM TB_SYS_EXEC_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_SYS_LOGIN_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 6 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_SYS_SEC_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_SYS_SYSTEM_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 3 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_BAS_UPT_GRP_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_OPR_CTRL_RESULT WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_OPR_ESS_CONFIG_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_OPR_OPER_CTRL_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_OPR_OPER_LOGFILE_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
			SELECT * FROM TB_OPR_PVCALC_HIST WHERE CREATE_DT < DATE_TRUNC('DAY', SYS_EXTRACT_UTC(NOW()) - 12 * '30 DAY'::INTERVAL) ;
	        */
      
	       	EXECUTE v_sql;
           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	       	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
	       	--RAISE NOTICE 'i_sp_nm[%] [%] Table REMOVE old records[%] SQL[%]', i_sp_nm, v_row.TABLE_NM, v_work_num, v_sql;
	 
	   	END LOOP;

		-- 실행성공
       	v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_sys_remove_old_records(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1112 (class 1255 OID 1895936)
-- Name: sp_sys_remove_partition(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_sys_remove_partition(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_SYS_REMOVE_PARTITION
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-03-17   yngwie       1. Created this function.
   2.0        2019-09-24   박동환        Postgresql 변환

   NOTES:   TB_PLF_TBL_META의 DEL_CYCLE 이전 파티션 삭제
   
    Input : 
	        
	Output : 
	
******************************************************************************/	
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_row           	record; 
   	v_loopCnt		INTEGER := 0;
   	v_nextDateStr	VARCHAR   := NULL; 
   	v_sql				VARCHAR   := NULL;
  
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';

--	    RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] START[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	    FOR v_row IN ( 
			WITH PLF AS (
				SELECT M.TABLE_NM    as TABLE_NAME
				   		, MIN(CHILD.RELNAME)		OLD_PARTITION
						, M.PARTITION_TYPE
						, M.DEL_CYCLE
					FROM 	PG_INHERITS
				    JOIN 		PG_CLASS PARENT ON PG_INHERITS.INHPARENT = PARENT.OID
				    JOIN 		PG_CLASS CHILD   ON PG_INHERITS.INHRELID   = CHILD.OID
				    JOIN 		TB_PLF_TBL_META M ON (PARENT.RELNAME= LOWER(M.TABLE_NM)  )
				    WHERE	M.PARTITION_TYPE IN ('D', 'M')
				    AND		M.DEL_CYCLE > 0
					GROUP BY  M.TABLE_NM, M.PARTITION_TYPE, M.DEL_CYCLE
			), BAS AS (
				SELECT 	*
							, STRING_TO_ARRAY(OLD_PARTITION, '_') RELARR
				FROM 	PLF
			), PT AS (
				SELECT *
						, RELARR[ARRAY_UPPER(RELARR,1)] 	OLD_PT_DATE_STR
						, CASE WHEN PARTITION_TYPE = 'M' THEN 
											TO_CHAR(SYS_EXTRACT_UTC(NOW())- concat(DEL_CYCLE,' MONTH')::INTERVAL, 'YYYYMM')
						       	 WHEN PARTITION_TYPE = 'D' THEN 
						       	 			TO_CHAR(SYS_EXTRACT_UTC(NOW())- concat(DEL_CYCLE,' DAY')::INTERVAL, 'YYYYMMDD')
						         ELSE NULL
						   END AS MIN_DATE_STR
				FROM BAS
			)
			SELECT 	
						TABLE_NAME
						, OLD_PARTITION
						, OLD_PT_DATE_STR
						, MIN_DATE_STR
						, PARTITION_TYPE
						, CASE WHEN PARTITION_TYPE = 'M' THEN 
										(DATE_PART('YEAR',     TO_DATE(MIN_DATE_STR, 'YYYYMM')) - DATE_PART('YEAR',     TO_DATE(OLD_PT_DATE_STR, 'YYYYMM'))
										) * 12 +
										(DATE_PART('MONTH', TO_DATE(MIN_DATE_STR, 'YYYYMM')) - DATE_PART('MONTH', TO_DATE(OLD_PT_DATE_STR, 'YYYYMM')))
						  		ELSE 	 
						  				 EXTRACT(DAY FROM TO_TIMESTAMP(MIN_DATE_STR, 'YYYYMMDD')-TO_TIMESTAMP(OLD_PT_DATE_STR, 'YYYYMMDD'))
						  END DIFF_SIZE
						, DEL_CYCLE
			FROM PT
			
		) LOOP
            IF v_row.DIFF_SIZE > 0 THEN

            	--	2015.03.31 yngwie, 파티션 보관주기 이전의 파티션들은 모두 drop 한다.
            	FOR v_loopCnt IN 0..v_row.DIFF_SIZE-1 LOOP

            		-- 다음 번 삭제할 파티션 날짜 문자열을 생성한다.
            		IF v_row.PARTITION_TYPE = 'D' THEN
            			v_nextDateStr := TO_CHAR(TO_DATE(v_row.OLD_PT_DATE_STR, 'YYYYMMDD') + concat(v_loopCnt,' DAY')::INTERVAL, 'YYYYMMDD');
            		ELSE
            			v_nextDateStr := TO_CHAR(TO_DATE(v_row.OLD_PT_DATE_STR, 'YYYYMMDD') + concat(v_loopCnt,' MONTH')::INTERVAL, 'YYYYMM');
            		END IF;
            	
                    -- ex) DROP TABLE PT_STT_ESS_DD_20170812 CASCADE;
	                v_sql := FORMAT('DROP TABLE PT_%s_%s CASCADE;' 
	                	                , REPLACE(v_row.TABLE_NAME, 'TB_', '')
	                					, v_nextDateStr
	                					) ;
	                				
					--RAISE notice 'i_sp_nm[%] REMOVE Partition :  SQL[%]', i_sp_nm, v_sql;
	                EXECUTE v_sql;		            
	                GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
				    v_rslt_cnt := v_rslt_cnt + v_work_num;	       

	            END LOOP;            
            END IF;
           
	   END LOOP;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$$;


ALTER FUNCTION wems.sp_sys_remove_partition(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1084 (class 1255 OID 1895938)
-- Name: sp_sys_rename_partition(); Type: PROCEDURE; Schema: wems; Owner: wems
--

CREATE PROCEDURE wems.sp_sys_rename_partition()
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_SYS_RENAME_PARTITION
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2019-09-30   박동환        최초작성

   NOTES:   파티션명 일괄변경
   
    Input : 
	        
	Output : 
	
******************************************************************************/	
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_log_fn 			VARCHAR := 'SP_SYS_RENAME_PARTITION';
    v_log_seq 		NUMERIC 	:= nextval('wems.sq_sys_job_hist');
    v_exe_mthd		VARCHAR := 'A';							-- 실행방식( A:자동(job수행), M:수동 )

	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    p_dt				VARCHAR   := '';

   -- 미리 생성할 일수
--    v_daily_size 		INTEGER := 14;
--    v_monthly_size INTEGER := 2;
   	
    v_row           	record; 
   	v_sql				VARCHAR   := NULL;
  
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
  
		v_rslt_cd := 'A';

	    --RAISE NOTICE 'v_log_fn:[%] v_log_seq[%] START[%]', v_log_fn, v_log_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	    FOR v_row IN ( 
			SELECT 	M.TABLE_NM
						, CHILD.RELNAME as PARTION_NM
						, TO_REGCLASS( REPLACE( LOWER(CHILD.RELNAME), LOWER(TABLE_NM||'_'), '') ) as EXIST_NM
			FROM 	PG_INHERITS
		    JOIN 		PG_CLASS PARENT ON PG_INHERITS.INHPARENT = PARENT.OID
		    JOIN 		PG_CLASS CHILD   ON PG_INHERITS.INHRELID   = CHILD.OID
		    JOIN 		TB_PLF_TBL_META M ON (PARENT.RELNAME= LOWER(M.TABLE_NM)  )
		    WHERE	M.PARTITION_TYPE IS NOT null
				    
		) loop
			-- ALTER TABLE tb_sys_sp_exe_hst OWNER TO wems
--			v_sql := 'ALTER TABLE ' || v_row.PARTION_NM || ' OWNER TO wems;';
--			RAISE notice 'v_log_fn[%] Change Owner Partition Table :  SQL[%]', v_log_fn, v_sql;
--			EXECUTE v_sql;		            

			if v_row.EXIST_NM is null THEN
--				ALTER TABLE tb_stt_rank_mm_pt_stt_rank_mm_201902 RENAME TO pt_stt_rank_mm_201902;
				v_sql := 'ALTER TABLE ' || v_row.PARTION_NM || ' RENAME TO ' || REPLACE( LOWER(v_row.PARTION_NM), LOWER(v_row.TABLE_NM||'_'), '')||';';

				--RAISE notice 'v_log_fn[%] Rename Partition Table :  SQL[%]', v_log_fn, v_sql;
				EXECUTE v_sql;		            
			end if;
			
           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	       	v_rslt_cnt := v_rslt_cnt + v_work_num;	       

		END LOOP;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'v_log_fn[%] EXCEPTION !!! ', v_log_fn;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( v_log_fn
                            , v_log_seq
                            , p_dt
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
END;
$$;


ALTER PROCEDURE wems.sp_sys_rename_partition() OWNER TO wems;

--
-- TOC entry 1113 (class 1255 OID 1895940)
-- Name: sp_unlock_user(character varying, numeric, character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sp_unlock_user(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
/******************************************************************************
   NAME:       SP_UNLOCK_USER
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-07-08   yngwie       Created this function.
   2.0        2019-10-01   이재형        convert postgresql
   
   NOTES:   패스워드 5회 입력 오류로 잠긴계정을 일정시간 후 잠금해제
   				(입력시간 무관하게 호출된 시간 기준 처리)
	Input :

	Usage : SELECT SP_UNLOCK_USER('SP_UNLOCK_USER', 0, '20191024', 'M')

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_last_dt			TIMESTAMP := NULL;  					-- 최종작업시간
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
	v_record       	record;
	v_timeout 		numeric;

BEGIN
    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
  
		v_rslt_cd := 'A';

	    SELECT   CAST(CODE AS NUMERIC)
		INTO 		v_timeout 
		FROM 	TB_PLF_CODE_INFO     
		WHERE 	GRP_CD = 'ACC_LOCK_TIME_CD';
	
	 	FOR v_record IN
				SELECT USER_ID, LOCK_START_DT 
				FROM TB_BAS_USER 
				WHERE LOCK_FLAG = ENC_VARCHAR_INS('Y', 12, 'SDIENCK', 'TB_BAS_USER', 'LOCK_FLAG') 
		LOOP
			IF v_record.LOCK_START_DT+ (v_timeout||' MINUTE')::interval  < SYS_EXTRACT_UTC(now()) THEN
				
				UPDATE TB_BAS_USER  
	            SET  LOCK_FLAG = NULL    
	           		, LOCK_START_DT = NULL      
	           		, LAST_UNLOCK_DT = SYS_EXTRACT_UTC(now())
	           		, UPDATE_ID = ENC_VARCHAR_INS('SYSTEM', 12, 'SDIENCK', 'TB_BAS_USER', 'UPDATE_ID')      
	           		, UPDATE_DT = SYS_EXTRACT_UTC(now()) 
	           	WHERE USER_ID = v_record.USER_ID;	
			
			END IF;
		END LOOP;

     	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;	       

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS then
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   	/***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;


$$;


ALTER FUNCTION wems.sp_unlock_user(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) OWNER TO wems;

--
-- TOC entry 1163 (class 1255 OID 1893553)
-- Name: sys_extract_utc(time without time zone); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sys_extract_utc(val time without time zone) RETURNS text
    LANGUAGE plpgsql
    AS $$
	DECLARE
		v_time  VARCHAR;
		v_result	VARCHAR(10000);	  
	BEGIN
		select to_char(val, 'YYYY-MM-DD HH24:MI:SS') into v_time;
--		SELECT timestamp with time zone v_time AT TIME ZONE 'UTC'  into v_result ;
		return v_time;
	end;
$$;


ALTER FUNCTION wems.sys_extract_utc(val time without time zone) OWNER TO wems;

--
-- TOC entry 1087 (class 1255 OID 1893554)
-- Name: sys_extract_utc(timestamp with time zone); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sys_extract_utc(val timestamp with time zone) RETURNS timestamp with time zone
    LANGUAGE plpgsql
    AS $$
	DECLARE
		v_time  VARCHAR;
		v_result	timestamptz;	  
	BEGIN
		select (val at time zone 'UTC')::timestamptz into v_result ;
		return v_result;
	end;
$$;


ALTER FUNCTION wems.sys_extract_utc(val timestamp with time zone) OWNER TO wems;

--
-- TOC entry 1164 (class 1255 OID 1893555)
-- Name: sys_extract_utcnow(); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.sys_extract_utcnow() RETURNS timestamp with time zone
    LANGUAGE plpgsql IMMUTABLE
    AS $$
	DECLARE
		v_result	timestamptz;	  
	BEGIN
		select SYS_EXTRACT_UTC(NOW()) into v_result ;
		return v_result;
	end;
$$;


ALTER FUNCTION wems.sys_extract_utcnow() OWNER TO wems;


--
-- TOC entry 1095 (class 1255 OID 1895942)
-- Name: to_char(integer); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.to_char(param integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
	DECLARE
		v_result	VARCHAR(10000);	
	
	BEGIN
		select param::VARCHAR into v_result;
		RETURN v_result;
	END;
$$;


ALTER FUNCTION wems.to_char(param integer) OWNER TO wems;

--
-- TOC entry 1114 (class 1255 OID 1895943)
-- Name: to_char(numeric); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.to_char(param numeric) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
	DECLARE
		v_result	VARCHAR(10000);	
	
	BEGIN
		select param::VARCHAR into v_result;
		RETURN v_result;
	END;
$$;


ALTER FUNCTION wems.to_char(param numeric) OWNER TO wems;

--
-- TOC entry 1159 (class 1255 OID 1895944)
-- Name: to_char(character varying, character varying); Type: FUNCTION; Schema: wems; Owner: wems
--

CREATE FUNCTION wems.to_char(param character varying, fmt character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
	DECLARE
		v_result	VARCHAR(10000);	
	
	BEGIN
		select to_char(param::numeric ,fmt) into v_result;
		RETURN v_result;
	END;
$$;


ALTER FUNCTION wems.to_char(param character varying, fmt character varying) OWNER TO wems;


/*------------------------------------------------------------------------------
 한솔DB 데이타 이관 후에 아래의 변경된 테이블에 대한 처리를 해야 합니다.

1. TB_OPR_ALARM 
2. TB_BAS_MODEL_ALARM_CODE_MAP 
3. TB_PLF_CODE_DESC_TRANS
------------------------------------------------------------------------------*/

alter table TB_OPR_ALARM add column send_dt timestamp;
alter table TB_BAS_MODEL_ALARM_CODE_MAP add column repeat_flag varchar(1);
alter table TB_BAS_MODEL_ALARM_CODE_MAP add column repeat_interval varchar(4);
delete from wems.tb_plf_code_desc_trans;
alter table wems.tb_plf_code_desc_trans add column model_type varchar(10);
alter table wems.tb_plf_code_desc_trans drop  CONSTRAINT  pk_plf_code_desc_trans;
alter table wems.tb_plf_code_desc_trans add CONSTRAINT pk_plf_code_desc_trans PRIMARY KEY (grp_cd, code, lang_cd, model_type);  


/*------------------------------------------------------------------------------
 아래의 4개 테이블은 한솔에서 받은 CSV를 사용하지 않고 
 S3의 rdsmigration-bucket 폴더의 파일로 import해야 함

1. BATCH_INF 
2. TB_BAS_TIMEZONE
3. TB_PLF_CODE_DESC_TRANS
4. TB_PLF_TBL_META
------------------------------------------------------------------------------*/
TRUNCATE TABLE wems.batch_inf;
SELECT aws_s3.table_import_from_s3(
   	'wems.batch_inf',
	'sys_nm, sp_nm, cycl_cd, cycl_time, exe_scdul, ord, use_yn, sp_desc, cretr, cret_dt',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'BATCH_INF.csv', 'eu-central-1')
);


TRUNCATE TABLE wems.tb_bas_timezone;
SELECT aws_s3.table_import_from_s3(
   	'wems.tb_bas_timezone',
	'timezone_id, utc_offset, dst_start_exp, dst_end_exp, dst_use_flag, create_dt',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'TB_BAS_TIMEZONE.csv', 'eu-central-1')
);

TRUNCATE TABLE wems.tb_plf_code_desc_trans;
SELECT aws_s3.table_import_from_s3(
   	'wems.tb_plf_code_desc_trans',
	'grp_cd, code, lang_cd, desc_trans, model_type',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'TB_PLF_CODE_DESC_TRANS.csv', 'eu-central-1')
);


TRUNCATE TABLE wems.tb_plf_tbl_meta;
SELECT aws_s3.table_import_from_s3(
   	'wems.tb_plf_tbl_meta',
	'sub_area, table_nm, table_type, arcv_flag, arcv_period, fst_load_dt, load_type, partition_type, std_col_nm, std_col_type, del_flag, del_cycle, create_dt',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'TB_PLF_TBL_META.csv', 'eu-central-1')
);

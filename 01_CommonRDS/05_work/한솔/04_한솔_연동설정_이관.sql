
-- 1
TRUNCATE TABLE wems.dc_cmd_session_ident;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_cmd_session_ident',
	'id, maxcmdqueue, description, priority, logmode, ackmode, max_session_cnt',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_CMD_SESSION_IDENT.csv', 'ap-northeast-2')
);

-- 8
TRUNCATE TABLE wems.dc_cnf_connection;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_cnf_connection',
	'sequence, connectorid, agentequipid, protocoltype, status, commandflag, description, name, agentportno, porttype, userid, password',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_CNF_CONNECTION.csv', 'ap-northeast-2')
);

-- 8 
TRUNCATE TABLE wems.dc_cnf_connector;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_cnf_connector',
	'id, gatewayid, ruleid, status, description, junctiontype, cmdresponsetype, logcycle, create_date, modify_date, last_action_date, last_action, last_action_desc, name',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_CNF_CONNECTOR.csv', 'ap-northeast-2')
);

-- 0 
TRUNCATE TABLE wems.dc_cnf_event_consumer;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_cnf_event_consumer',
	'id, dbuserid, dbpassword, dbtns, hostname, timemode, listenport, status, logmode, ipaddress, bypasslistenport, loadinginterval, handlermode, targetinfo, logcycle, runmode, managerid, subopermode',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_CNF_EVENT_CONSUMER.csv', 'ap-northeast-2')
);

-- 1
TRUNCATE TABLE wems.dc_cnf_manager;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_cnf_manager',
	'id, ip, status, sockmgrport, description, name',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_CNF_MANAGER.csv', 'ap-northeast-2')
);

-- 0
TRUNCATE TABLE wems.dc_cnf_mapping;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_cnf_mapping',
	'agentequipid, agentportno, bscno, aciiheader',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_CNF_MAPPING.csv', 'ap-northeast-2')
);

-- 1
TRUNCATE TABLE wems.dc_cnf_server;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_cnf_server',
	'ip, guiport, cmdport, netfinderport, logport, netfinderip, sockmgrport',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_CNF_SERVER.csv', 'ap-northeast-2')
);

-- 0
TRUNCATE TABLE wems.dc_cnf_sub_proc;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_cnf_sub_proc',
	'id, id_str, parent, parentid, ipaddress, hostname, status, logcycle, description, bin_name, args',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_CNF_SUB_PROC.csv', 'ap-northeast-2')
);

-- 0 
TRUNCATE TABLE wems.dc_code_porttype;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_code_porttype',
	'porttype, description, extflag, section, cmdflag',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_CODE_PORTTYPE.csv', 'ap-northeast-2')
);

-- 0
TRUNCATE TABLE wems.dc_code_protocoltype;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_code_protocoltype',
	'protocoltype, description, need_portno, need_password, need_userid, extflag, porttype_able, need_ip',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_CODE_PROTOCOLTYPE.csv', 'ap-northeast-2')
);

-- 0 
TRUNCATE TABLE wems.dc_rul_identification;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_rul_identification',
	'id, ruleid, parentid, parsingruleid, idstring, outputflag, parsingflag, rawdataflag, update_user, update_date, xmlflag, editoruseflag, default_ident_flag',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_RUL_IDENTIFICATION.csv', 'ap-northeast-2')
);

-- 0 
TRUNCATE TABLE wems.dc_rul_parsing_template;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_rul_parsing_template',
	'id, parsingtemplategrpid, eventid, sequence, parsettype, headersize, datasize, skipline, nextflag, equipflag, consumers, ruleid, udpate_user, update_date, xmlkeyelementtag, xmlrootelementtag, xmlrootelementcdatatag, xmlrootelementpcdata, xmlrootpcdatakind, atomresulthideflag, eofhideflag',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_RUL_PARSING_TEMPLATE.csv', 'ap-northeast-2')
);

-- 0
TRUNCATE TABLE wems.dc_rul_parsing_template_grp;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_rul_parsing_template_grp',
	'id, identificationid, ruleid, update_user, update_date',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_RUL_PARSING_TEMPLATE_GRP.csv', 'ap-northeast-2')
);

-- 0 
TRUNCATE TABLE wems.dc_rul_rawdata;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_rul_rawdata',
	'ruleid, id, rawdata',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_RUL_RAWDATA.csv', 'ap-northeast-2')
);

-- 0
TRUNCATE TABLE wems.dc_rul_rule;
SELECT aws_s3.table_import_from_s3(
   	'wems.dc_rul_rule',
	'id, generationcode, vendercode, version, identificationtype, linedecimal, update_user, update_date',
   	'DELIMITER ''|'' NULL ''~'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('seoul-rds-migration-bucket', 'hansol_meta/DC_RUL_RULE.csv', 'ap-northeast-2')
);

기준/통계 데이터 모두 s3에 .csv(migration 하려고 하는 데이터)를 업로드 해 두고,
아래와 같은 import 구문을 통해서, rds 테이블에 data 를 밀어 넣으면 되겠음.

SELECT aws_s3.table_import_from_s3(
   	'wems.tb_raw_bms_info',
	'device_id, colec_dt, done_flag, col0, col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16, col17, col18, col19, col20, col21, col22, col23, col24, col25, col26, col27, col28, col29, col30, col31, col32, col33, col34, col35, col36, col37, col38, col39, col40, create_dt, handle_flag',
   	'DELIMITER ''|'' NULL ''\N'' quote ''^'' CSV HEADER',
   aws_commons.create_s3_uri('rdsmigration-bucket', 'TB_RAW_BMS_INFO_98.csv', 'eu-central-1')
);

s3에 data 를 올리고, rds에서 s3의 데이터를 땡겨오는 상세 방법은 아래 Confluence 링크에서 확인이 가능함
(http://147.46.136.48:8094/hconf/x/HwIh)
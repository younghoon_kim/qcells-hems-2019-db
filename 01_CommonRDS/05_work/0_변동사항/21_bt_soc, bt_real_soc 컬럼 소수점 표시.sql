alter table tb_opr_ess_state alter column bt_soc type numeric(5,1) using bt_soc::numeric(5,1);
alter table tb_opr_ess_state alter column bt_real_soc type numeric(5,1) using bt_real_soc::numeric(5,1);


alter table tb_opr_ess_state_tmp alter column bt_soc type numeric(5,1) using bt_soc::numeric(5,1);
alter table tb_opr_ess_state_tmp alter column bt_real_soc type numeric(5,1) using bt_real_soc::numeric(5,1);

alter table tb_opr_ess_state_hist alter column bt_soc type numeric(5,1) using bt_soc::numeric(5,1);
alter table tb_opr_ess_state_hist alter column bt_real_soc type numeric(5,1) using bt_real_soc::numeric(5,1);

-- alter table tb_opr_ess_state_hist_tmp alter column bt_soc type numeric(5,1) using bt_soc::numeric(5,1);
-- alter table tb_opr_ess_state_hist_tmp alter column bt_real_soc type numeric(5,1) using bt_real_soc::numeric(5,1);

alter table tb_stt_ess_tm alter column bt_soc type numeric(5,1) using bt_soc::numeric(5,1);
alter table tb_stt_ess_tm alter column bt_real_soc type numeric(5,1) using bt_real_soc::numeric(5,1);

-- alter table tb_stt_ess_tm_tmp alter column bt_soc type numeric(5,1) using bt_soc::numeric(5,1);
-- alter table tb_stt_ess_tm_tmp alter column bt_real_soc type numeric(5,1) using bt_real_soc::numeric(5,1);

alter table tb_stt_ess_dd alter column bt_soc type numeric(5,1) using bt_soc::numeric(5,1);
alter table tb_stt_ess_dd alter column bt_real_soc type numeric(5,1) using bt_real_soc::numeric(5,1);

alter table tb_stt_ess_mm alter column bt_soc type numeric(5,1) using bt_soc::numeric(5,1);
alter table tb_stt_ess_mm alter column bt_real_soc type numeric(5,1) using bt_real_soc::numeric(5,1);

alter table tb_stt_ess_yy alter column bt_soc type numeric(5,1) using bt_soc::numeric(5,1);
-- alter table tb_stt_ess_yy alter column bt_real_soc type numeric(5,1) using bt_real_soc::numeric(5,1);





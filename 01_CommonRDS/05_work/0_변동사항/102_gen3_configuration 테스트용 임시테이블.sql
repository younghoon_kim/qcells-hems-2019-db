create table tb_test_configuration_gens3_temp
(
    uid                                                                   varchar,
    batterybackupsoc                                                      integer,
    energypolicy                                                          integer,
    inverter_inverterlimit                                                integer,
    inverter_gridtargetfrequency                                          double precision,
    pv_pvtype                                                             integer,
    pv_pvstringcount                                                      integer,
    pv_pv1stringpower                                                     integer,
    pv_pv2stringpower                                                     integer,
    pv_pv3stringpower                                                     integer,
    pv_pv4stringpower                                                     integer,
    pv_pv5stringpower                                                     integer,
    pv_feedinlimit                                                        integer,
    meter_metermodel                                                      integer,
    meter_meterconnection                                                 integer,
    meter_tcpipaddress                                                    varchar,
    meter_tcpport                                                         integer,
    meter_rtubaudrate                                                     integer,
    meter_rtuparity                                                       varchar,
    meter_rtudatabits                                                     integer,
    meter_rtustopbits                                                     integer,
    battery_operatingrackcount                                            integer,
    battery_connectedrackcount                                            integer,
    externalcontrol_gatewayconnection                                     integer,
    pcs_externalems_pcsconnection                                         integer,
    hysteresis_low                                                        integer,
    hysteresis_high                                                       integer,
    batteryusersoc_min                                                    integer,
    batteryusersoc_max                                                    integer,
    debuginfo_processmgr                                                  integer,
    debuginfo_systemlog                                                   integer,
    debuginfo_fota                                                        integer,
    debuginfo_powercontrol                                                integer,
    debuginfo_algorithmmgr                                                integer,
    debuginfo_essmgr                                                      integer,
    debuginfo_dcsourcemgr                                                 integer,
    debuginfo_cloudmgr                                                    integer,
    debuginfo_metermgr                                                    integer,
    debuginfo_gatewaymgr                                                  integer,
    debuginfo_datamgr                                                     integer,
    debuginfo_dbmgr                                                       integer,
    debuginfo_webengine                                                   integer,
    gridcontrol_overvoltage_frtflag                                       integer,
    gridcontrol_overvoltage_level1                                        double precision,
    gridcontrol_overvoltage_time1                                         integer,
    gridcontrol_overvoltage_level2                                        double precision,
    gridcontrol_overvoltage_time2                                         integer,
    gridcontrol_overvoltage_level3                                        double precision,
    gridcontrol_overvoltage_time3                                         integer,
    gridcontrol_overvoltage_level4                                        double precision,
    gridcontrol_overvoltage_time4                                         integer,
    gridcontrol_overvoltage_releaselevel                                  double precision,
    gridcontrol_overvoltage_releasetime                                   integer,
    gridcontrol_tenminvoltage_level                                       double precision,
    gridcontrol_undervoltage_level1                                       double precision,
    gridcontrol_undervoltage_time1                                        integer,
    gridcontrol_undervoltage_level2                                       double precision,
    gridcontrol_undervoltage_time2                                        integer,
    gridcontrol_undervoltage_level3                                       double precision,
    gridcontrol_undervoltage_time3                                        integer,
    gridcontrol_undervoltage_level4                                       double precision,
    gridcontrol_undervoltage_time4                                        integer,
    gridcontrol_undervoltage_releaselevel                                 double precision,
    gridcontrol_undervoltage_releasetime                                  integer,
    gridcontrol_overfrequency_level1                                      double precision,
    gridcontrol_overfrequency_time1                                       integer,
    gridcontrol_overfrequency_level2                                      double precision,
    gridcontrol_overfrequency_time2                                       integer,
    gridcontrol_overfrequency_releaselevel                                double precision,
    gridcontrol_overfrequency_releasetime                                 integer,
    gridcontrol_underfrequency_level1                                     double precision,
    gridcontrol_underfrequency_time1                                      integer,
    gridcontrol_underfrequency_level2                                     double precision,
    gridcontrol_underfrequency_time2                                      integer,
    gridcontrol_underfrequency_releaselevel                               double precision,
    gridcontrol_underfrequency_releasetime                                integer,
    gridcontrolactive_activepowersetpoint_setpointflag                    integer,
    gridcontrolactive_activepowersetpoint_setpointvalue                   integer,
    gridcontrolactive_activepowerfrequency_frequencyflag                  integer,
    gridcontrolactive_activepowerfrequency_slopeselect                    integer,
    gridcontrolactive_activepowerfrequency_slfrequency                    double precision,
    gridcontrolactive_activepowerfrequency_shfrequency                    double precision,
    gridcontrolactive_activepowerfrequency_x1frequency                    double precision,
    gridcontrolactive_activepowerfrequency_y1frequency                    double precision,
    gridcontrolactive_activepowerfrequency_x2frequency                    double precision,
    gridcontrolactive_activepowerfrequency_y2frequency                    double precision,
    gridcontrolactive_activepowerfrequency_x3frequency                    double precision,
    gridcontrolactive_activepowerfrequency_y3frequency                    double precision,
    gridcontrolactive_activepowerfrequency_x4frequency                    double precision,
    gridcontrolactive_activepowerfrequency_y4frequency                    double precision,
    gridcontrolactive_activepowerfrequency_responsetime                   integer,
    gridcontrolactive_activepowerfrequency_of_prefselect                  integer,
    gridcontrolactive_activepowerfrequency_of_endingfrequency             double precision,
    gridcontrolactive_activepowerfrequency_of_endingtime                  integer,
    gridcontrolactive_activepowerfrequency_uf_prefselect                  integer,
    gridcontrolactive_activepowerfrequency_uf_endingfrequency             double precision,
    gridcontrolactive_activepowerfrequency_uf_endingtime                  integer,
    gridcontrolactive_activepowervoltage_voltageflag                      integer,
    gridcontrolactive_activepowervoltage_curveselect                      integer,
    gridcontrolactive_activepowervoltage_x1voltage                        double precision,
    gridcontrolactive_activepowervoltage_y1power                          double precision,
    gridcontrolactive_activepowervoltage_x2voltage                        double precision,
    gridcontrolactive_activepowervoltage_y2power                          double precision,
    gridcontrolactive_activepowervoltage_x3voltage                        double precision,
    gridcontrolactive_activepowervoltage_y3power                          double precision,
    gridcontrolactive_activepowervoltage_x4voltage                        double precision,
    gridcontrolactive_activepowervoltage_y4power                          double precision,
    gridcontrolactive_activepowervoltage_responsetime                     integer,
    gridcontrolreactive_pt1filterbehaviortau_tauvalue                     integer,
    gridcontrolreactive_reactivepowercospi_setpoint_cospi_setpointflag    integer,
    gridcontrolreactive_reactivepowercospi_setpoint_cospi_setpointexcited integer,
    gridcontrolreactive_reactivepowercospi_setpoint_cospi_setpointvalue   double precision,
    gridcontrolreactive_reactivepowercospi_setpoint_responsetime          integer,
    gridcontrolreactive_reactivepowercospi_p_cospi_p_flag                 integer,
    gridcontrolreactive_reactivepowercospi_p_cospi_p_excited              integer,
    gridcontrolreactive_reactivepowercospi_p_gen_x1power                  double precision,
    gridcontrolreactive_reactivepowercospi_p_gen_y1powerfactor            double precision,
    gridcontrolreactive_reactivepowercospi_p_gen_x2power                  double precision,
    gridcontrolreactive_reactivepowercospi_p_gen_y2powerfactor            double precision,
    gridcontrolreactive_reactivepowercospi_p_gen_x3power                  double precision,
    gridcontrolreactive_reactivepowercospi_p_gen_y3powerfactor            double precision,
    gridcontrolreactive_reactivepowercospi_p_load_x1power                 double precision,
    gridcontrolreactive_reactivepowercospi_p_load_y1powerfactor           double precision,
    gridcontrolreactive_reactivepowercospi_p_load_x2power                 double precision,
    gridcontrolreactive_reactivepowercospi_p_load_y2powerfactor           double precision,
    gridcontrolreactive_reactivepowercospi_p_load_x3power                 double precision,
    gridcontrolreactive_reactivepowercospi_p_load_y3powerfactor           double precision,
    gridcontrolreactive_reactivepowercospi_p_responsetime                 integer,
    gridcontrolreactive_reactivepower_q_setpoint_q_setpointflag           integer,
    gridcontrolreactive_reactivepower_q_setpoint_q_setpointexcited        integer,
    gridcontrolreactive_reactivepower_q_setpoint_q_setpointvalue          double precision,
    gridcontrolreactive_reactivepower_q_setpoint_responsetime             integer,
    gridcontrolreactive_reactivepower_q_u_q_u_flag                        integer,
    gridcontrolreactive_reactivepower_q_u_q_u_curveselect                 integer,
    gridcontrolreactive_reactivepower_q_u_fixedvrefselect_flag            integer,
    gridcontrolreactive_reactivepower_q_u_vrefvoltage                     double precision,
    gridcontrolreactive_reactivepower_q_u_autovrefavgtime                 integer,
    gridcontrolreactive_reactivepower_q_u_x1voltage                       double precision,
    gridcontrolreactive_reactivepower_q_u_y1reactive                      double precision,
    gridcontrolreactive_reactivepower_q_u_x2voltage                       double precision,
    gridcontrolreactive_reactivepower_q_u_y2reactive                      double precision,
    gridcontrolreactive_reactivepower_q_u_x3voltage                       double precision,
    gridcontrolreactive_reactivepower_q_u_y3reactive                      double precision,
    gridcontrolreactive_reactivepower_q_u_x4voltage                       double precision,
    gridcontrolreactive_reactivepower_q_u_y4reactive                      double precision,
    gridcontrolreactive_reactivepower_q_u_responsetime                    integer,
    gridcontrolreactive_reactivepower_q_p_q_p_flag                        integer,
    gridcontrolreactive_reactivepower_q_p_x1gen                           double precision,
    gridcontrolreactive_reactivepower_q_p_y1gen                           double precision,
    gridcontrolreactive_reactivepower_q_p_x2gen                           double precision,
    gridcontrolreactive_reactivepower_q_p_y2gen                           double precision,
    gridcontrolreactive_reactivepower_q_p_x3gen                           double precision,
    gridcontrolreactive_reactivepower_q_p_y3gen                           double precision,
    gridcontrolreactive_reactivepower_q_p_x1load                          double precision,
    gridcontrolreactive_reactivepower_q_p_y1load                          double precision,
    gridcontrolreactive_reactivepower_q_p_x2load                          double precision,
    gridcontrolreactive_reactivepower_q_p_y2load                          double precision,
    gridcontrolreactive_reactivepower_q_p_x3load                          double precision,
    gridcontrolreactive_reactivepower_q_p_y3load                          double precision,
    gridcontrolreactive_reactivepower_q_p_responsetime                    integer,
    dcinjectioncontrol_enable_flag                                        integer,
    dcinjectioncontrol_fault_level1                                       double precision,
    dcinjectioncontrol_fault_time1                                        integer,
    dcinjectioncontrol_fault_level2                                       double precision,
    dcinjectioncontrol_fault_time2                                        integer,
    dcinjectioncontrol_control_enable                                     integer,
    dcinjectioncontrol_control_level                                      double precision,
    rcmucontrol_sudden_enable_flag                                        integer,
    rcmucontrol_sudden_level1                                             integer,
    rcmucontrol_sudden_time1                                              integer,
    rcmucontrol_sudden_level2                                             integer,
    rcmucontrol_sudden_time2                                              integer,
    rcmucontrol_sudden_level3                                             integer,
    rcmucontrol_sudden_time3                                              integer,
    rcmucontrol_continuous_enable_flag                                    integer,
    rcmucontrol_continuous_level                                          integer,
    rcmucontrol_continuous_time                                           integer,
    pvinsulationcontrol_enable_flag                                       integer,
    pvinsulationcontrol_faultlevel                                        double precision,
    pvinsulationcontrol_checkcnt                                          integer,
    pvinsulationcontrol_retrytime                                         integer,
    antiislandingcontrol_enable_flag                                      integer,
    antiislandingcontrol_antiislandinggain                                integer,
    gradientcontrol_enable_flag                                           integer,
    gradientcontrol_energy_source_flag                                    integer,
    gradientcontrol_timelevel                                             integer,
    gradientcontrol_activepowersetpointgradient_                          double precision,
    deratingcontrol_enable_flag                                           integer,
    deratingcontrol_starttemp                                             double precision,
    deratingcontrol_value                                                 double precision,
    feedinrelay_feed_in_relay_enable_flag                                 integer,
    feedinrelay_relay_1_attach_level                                      integer,
    feedinrelay_relay_1_detach_level                                      integer,
    feedinrelay_relay_2_attach_level                                      integer,
    feedinrelay_relay_2_detach_level                                      integer,
    feedinrelay_relay_3_attach_level                                      integer,
    feedinrelay_relay_3_detach_level                                      integer,
    feedinrelay_relay_4_attach_level                                      integer,
    feedinrelay_relay_4_detach_level                                      integer,
    reconnectiontime_time                                                 integer,
    taucoefficient                                                        double precision,
    create_dt                                                             timestamptz default sys_extract_utc(now()) not null

);

comment on column tb_test_configuration_gens3_temp.uid is 'EMS의 Serial Number';
comment on column tb_test_configuration_gens3_temp.batterybackupsoc is '0%(default),25%,50%,75%,100%';
comment on column tb_test_configuration_gens3_temp.energypolicy is '1: Self consumption
2. Zero Export Mode
3. Time-based Mode
4. External Generation Mode';
comment on column tb_test_configuration_gens3_temp.inverter_inverterlimit is '정수값, 3600, 4600, 5000, 8000 모델별로 상이';
comment on column tb_test_configuration_gens3_temp.inverter_gridtargetfrequency is '50.00 or 60.00 [Hz]';
comment on column tb_test_configuration_gens3_temp.pv_pvtype is '0: String Inverter
1: Micro Inverter
2: Power Optimizers';
comment on column tb_test_configuration_gens3_temp.pv_pvstringcount is '0 ~ 5';
comment on column tb_test_configuration_gens3_temp.pv_pv1stringpower is '0 ~ 5000W';
comment on column tb_test_configuration_gens3_temp.pv_pv2stringpower is '0 ~ 5000W';
comment on column tb_test_configuration_gens3_temp.pv_pv3stringpower is '0 ~ 5000W';
comment on column tb_test_configuration_gens3_temp.pv_pv4stringpower is '0 ~ 5000W';
comment on column tb_test_configuration_gens3_temp.pv_pv5stringpower is '0 ~ 5000W';
comment on column tb_test_configuration_gens3_temp.pv_feedinlimit is '-10 ~ 100 %';
comment on column tb_test_configuration_gens3_temp.meter_metermodel is '0:없음
1:EM112 (CarloGavazzi)
2:EM24 (CarloGavazzi)
99:Custom';
comment on column tb_test_configuration_gens3_temp.meter_meterconnection is '0: Disable
1: Modbus TCP
2: Modbus RTU';
comment on column tb_test_configuration_gens3_temp.meter_tcpipaddress is '';
comment on column tb_test_configuration_gens3_temp.meter_tcpport is '0 ~ 65535';
comment on column tb_test_configuration_gens3_temp.meter_rtubaudrate is '4600, 9600, 19200, 38400, 57600, 115200';
comment on column tb_test_configuration_gens3_temp.meter_rtuparity is 'N or E or O';
comment on column tb_test_configuration_gens3_temp.meter_rtudatabits is '5 ~ 8';
comment on column tb_test_configuration_gens3_temp.meter_rtustopbits is '1 ~ 2';
comment on column tb_test_configuration_gens3_temp.battery_operatingrackcount is '0 ~ 3';
comment on column tb_test_configuration_gens3_temp.battery_connectedrackcount is '0 ~ 3';
comment on column tb_test_configuration_gens3_temp.externalcontrol_gatewayconnection is '0: Disable
1: Modbus TCP
2: Modbus RTU';
comment on column tb_test_configuration_gens3_temp.pcs_externalems_pcsconnection is '0: Disable
1: CAN (QCELL PCS)
2: Modbus TCP (External EMS)
3: Modbus RTU (External EMS)';
comment on column tb_test_configuration_gens3_temp.hysteresis_low is 'Battery SOC가 5% 이하일 때 설정한 Low hysteresis SOC (10%,15%(default),20%,25%) 까지 방전하는 것이 불가능하다';
comment on column tb_test_configuration_gens3_temp.hysteresis_high is 'Battery SOC가 95% 이상일 때 설정한 High hysteresis SOC (90%(default),85%,80%) 까지 충전하는 것이 불가능하다';
comment on column tb_test_configuration_gens3_temp.batteryusersoc_min is '해당 값은 실제 Battery SOC값이며 설정된 값이 User SOC의 0%임';
comment on column tb_test_configuration_gens3_temp.batteryusersoc_max is '해당 값은 실제 Battery SOC값이며 설정된 값이 User SOC의 100%임';
comment on column tb_test_configuration_gens3_temp.debuginfo_processmgr is '0: Debug Level
1: Info Level
2: Warning Level
3: Error Level';
comment on column tb_test_configuration_gens3_temp.debuginfo_systemlog is '0: Debug Level
1: Info Level
2: Warning Level
3: Error Level';
comment on column tb_test_configuration_gens3_temp.debuginfo_fota is '0: Debug Level
1: Info Level
2: Warning Level
3: Error Level';
comment on column tb_test_configuration_gens3_temp.debuginfo_powercontrol is '0: Debug Level
1: Info Level
2: Warning Level
3: Error Level';
comment on column tb_test_configuration_gens3_temp.debuginfo_algorithmmgr is '0: Debug Level
1: Info Level
2: Warning Level
3: Error Level';
comment on column tb_test_configuration_gens3_temp.debuginfo_essmgr is '0: Debug Level
1: Info Level
2: Warning Level
3: Error Level';
comment on column tb_test_configuration_gens3_temp.debuginfo_dcsourcemgr is '0: Debug Level
1: Info Level
2: Warning Level
3: Error Level';
comment on column tb_test_configuration_gens3_temp.debuginfo_cloudmgr is '0: Debug Level
1: Info Level
2: Warning Level
3: Error Level';
comment on column tb_test_configuration_gens3_temp.debuginfo_metermgr is '0: Debug Level
1: Info Level
2: Warning Level
3: Error Level';
comment on column tb_test_configuration_gens3_temp.debuginfo_gatewaymgr is '0: Debug Level
1: Info Level
2: Warning Level
3: Error Level';
comment on column tb_test_configuration_gens3_temp.debuginfo_datamgr is '0: Debug Level
1: Info Level
2: Warning Level
3: Error Level';
comment on column tb_test_configuration_gens3_temp.debuginfo_dbmgr is '0: Debug Level
1: Info Level
2: Warning Level
4: Error Level';
comment on column tb_test_configuration_gens3_temp.debuginfo_webengine is '0: Debug Level
1: Info Level
2: Warning Level
5: Error Level';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overvoltage_frtflag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overvoltage_level1 is '100.0 ~ 350.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overvoltage_time1 is '0 ~ 65535 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overvoltage_level2 is '100.0 ~ 350.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overvoltage_time2 is '0 ~ 65535 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overvoltage_level3 is '100.0 ~ 350.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overvoltage_time3 is '0 ~ 65535 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overvoltage_level4 is '100.0 ~ 350.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overvoltage_time4 is '0 ~ 65535 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overvoltage_releaselevel is '100.0 ~ 350.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overvoltage_releasetime is '0 ~ 65535 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_tenminvoltage_level is '100.0 ~ 350.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_undervoltage_level1 is '0.0 ~ 240.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_undervoltage_time1 is '0 ~ 65535 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_undervoltage_level2 is '0.0 ~ 240.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_undervoltage_time2 is '0 ~ 65535 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_undervoltage_level3 is '0.0 ~ 240.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_undervoltage_time3 is '0 ~ 65535 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_undervoltage_level4 is '0.0 ~ 240.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_undervoltage_time4 is '0 ~ 65535 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_undervoltage_releaselevel is '0.0 ~ 240.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_undervoltage_releasetime is '0 ~ 65535 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overfrequency_level1 is '50.000 ~ 65.000 [Hz]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overfrequency_time1 is '0 ~ 100000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overfrequency_level2 is '50.000 ~ 65.000 [Hz]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overfrequency_time2 is '0 ~ 100000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overfrequency_releaselevel is '50.000 ~ 65.000 [Hz]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_overfrequency_releasetime is '0 ~ 65535 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_underfrequency_level1 is '45.000 ~ 60.000 [Hz]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_underfrequency_time1 is '0 ~ 100000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_underfrequency_level2 is '45.000 ~ 60.000 [Hz]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_underfrequency_time2 is '0 ~ 100000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_underfrequency_releaselevel is '45.000 ~ 60.000 [Hz]';
comment on column tb_test_configuration_gens3_temp.gridcontrol_underfrequency_releasetime is '0 ~ 65535 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowersetpoint_setpointflag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowersetpoint_setpointvalue is '0.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_frequencyflag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_slopeselect is '0: Curve, 1: Slope';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_slfrequency is '-100.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_shfrequency is '-100.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_x1frequency is '45.000 ~ 60.000 [Hz]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_y1frequency is '-100.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_x2frequency is '45.000 ~ 60.000 [Hz]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_y2frequency is '-100.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_x3frequency is '50.000 ~ 65.000 [Hz]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_y3frequency is '-100.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_x4frequency is '50.000 ~ 65.000 [Hz]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_y4frequency is '-100.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_responsetime is '0 ~ 60000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_of_prefselect is '0: Pref, 1: Pmax';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_of_endingfrequency is '50.000 ~ 65.000 [Hz]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_of_endingtime is '0 ~ 60000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_uf_prefselect is '0: Pref, 1: Pmax';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_uf_endingfrequency is '45.000 ~ 60.000 [Hz]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowerfrequency_uf_endingtime is '0 ~ 60000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowervoltage_voltageflag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowervoltage_curveselect is '0: Curve, 1: Target';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowervoltage_x1voltage is '0.0 ~ 240.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowervoltage_y1power is '-100.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowervoltage_x2voltage is '0.0 ~ 240.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowervoltage_y2power is '-100.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowervoltage_x3voltage is '100.0 ~ 350.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowervoltage_y3power is '-100.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowervoltage_x4voltage is '100.0 ~ 350.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowervoltage_y4power is '-100.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolactive_activepowervoltage_responsetime is '0 ~ 60000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_pt1filterbehaviortau_tauvalue is '0 ~ 60000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_setpoint_cospi_setpointflag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_setpoint_cospi_setpointexcited is '0: Under, 1: Over';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_setpoint_cospi_setpointvalue is '0.00 ~ 1.00 [역률]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_setpoint_responsetime is '0 ~ 60000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_cospi_p_flag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_cospi_p_excited is '0: Under, 1: Over';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_gen_x1power is '0.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_gen_y1powerfactor is '0.00 ~ 1.00 [역률]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_gen_x2power is '0.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_gen_y2powerfactor is '0.00 ~ 1.00 [역률]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_gen_x3power is '0.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_gen_y3powerfactor is '0.00 ~ 1.00 [역률]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_load_x1power is '0.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_load_y1powerfactor is '0.00 ~ 1.00 [역률]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_load_x2power is '0.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_load_y2powerfactor is '0.00 ~ 1.00 [역률]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_load_x3power is '0.0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_load_y3powerfactor is '0.00 ~ 1.00 [역률]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepowercospi_p_responsetime is '0 ~ 60000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_setpoint_q_setpointflag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_setpoint_q_setpointexcited is '0: Under, 1: Over';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_setpoint_q_setpointvalue is '0.00 ~ 60.00 [%(Q/S)]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_setpoint_responsetime is '0 ~ 60000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_q_u_flag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_q_u_curveselect is '0: Curve, 1: Target';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_fixedvrefselect_flag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_vrefvoltage is '0 ~ 300.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_autovrefavgtime is '0 ~ 50000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_x1voltage is '0.0 ~ 240.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_y1reactive is '-60.00 ~ 60.00 [%(Q/S)]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_x2voltage is '0.0 ~ 240.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_y2reactive is '-60.00 ~ 60.00 [%(Q/S)]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_x3voltage is '200.0 ~ 350.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_y3reactive is '-60.00 ~ 60.00 [%(Q/S)]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_x4voltage is '200.0 ~ 350.0 [V]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_y4reactive is '-60.00 ~ 60.00 [%(Q/S)]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_u_responsetime is '0 ~ 60000 [msec]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_q_p_flag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_x1gen is '0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_y1gen is '-60.00 ~ 60.00 [%(Q/S)]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_x2gen is '0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_y2gen is '-60.00 ~ 60.00 [%(Q/S)]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_x3gen is '0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_y3gen is '-60.00 ~ 60.00 [%(Q/S)]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_x1load is '0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_y1load is '-60.00 ~ 60.00 [%(Q/S)]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_x2load is '0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_y2load is '-60.00 ~ 60.00 [%(Q/S)]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_x3load is '0 ~ 100.0 [%]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_y3load is '-60.00 ~ 60.00 [%(Q/S)]';
comment on column tb_test_configuration_gens3_temp.gridcontrolreactive_reactivepower_q_p_responsetime is '0 ~ 60000 [msec]';
comment on column tb_test_configuration_gens3_temp.dcinjectioncontrol_enable_flag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.dcinjectioncontrol_fault_level1 is '0.00 ~ 1.00 [A]';
comment on column tb_test_configuration_gens3_temp.dcinjectioncontrol_fault_time1 is '0 ~ 10000 [msec]';
comment on column tb_test_configuration_gens3_temp.dcinjectioncontrol_fault_level2 is '0.00 ~ 1.00 [A]';
comment on column tb_test_configuration_gens3_temp.dcinjectioncontrol_fault_time2 is '0 ~ 10000 [msec]';
comment on column tb_test_configuration_gens3_temp.dcinjectioncontrol_control_enable is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.dcinjectioncontrol_control_level is '0.00 ~ 1.00 [A]';
comment on column tb_test_configuration_gens3_temp.rcmucontrol_sudden_enable_flag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.rcmucontrol_sudden_level1 is '0 ~ 500 [mA]';
comment on column tb_test_configuration_gens3_temp.rcmucontrol_sudden_time1 is '0 ~ 500 [msec]';
comment on column tb_test_configuration_gens3_temp.rcmucontrol_sudden_level2 is '0 ~ 500 [mA]';
comment on column tb_test_configuration_gens3_temp.rcmucontrol_sudden_time2 is '0 ~ 500 [msec]';
comment on column tb_test_configuration_gens3_temp.rcmucontrol_sudden_level3 is '0 ~ 500 [mA]';
comment on column tb_test_configuration_gens3_temp.rcmucontrol_sudden_time3 is '0 ~ 500 [msec]';
comment on column tb_test_configuration_gens3_temp.rcmucontrol_continuous_enable_flag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.rcmucontrol_continuous_level is '0 ~ 500 [mA]';
comment on column tb_test_configuration_gens3_temp.rcmucontrol_continuous_time is '0 ~ 500 [msec]';
comment on column tb_test_configuration_gens3_temp.pvinsulationcontrol_enable_flag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.pvinsulationcontrol_faultlevel is '0.0 ~ 1000.0 [KOhm]';
comment on column tb_test_configuration_gens3_temp.pvinsulationcontrol_checkcnt is '1 ~ 100 [Cnt]';
comment on column tb_test_configuration_gens3_temp.pvinsulationcontrol_retrytime is '0 ~ 100 [Min]';
comment on column tb_test_configuration_gens3_temp.antiislandingcontrol_enable_flag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.antiislandingcontrol_antiislandinggain is '0 ~ 100000';
comment on column tb_test_configuration_gens3_temp.gradientcontrol_enable_flag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.gradientcontrol_energy_source_flag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.gradientcontrol_timelevel is '60 ~ 1200 [sec]';
comment on column tb_test_configuration_gens3_temp.gradientcontrol_activepowersetpointgradient_ is '0.00 ~ 100.00 [Pmax/sec]';
comment on column tb_test_configuration_gens3_temp.deratingcontrol_enable_flag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.deratingcontrol_starttemp is '0.0 ~ 150.0 [℃]';
comment on column tb_test_configuration_gens3_temp.deratingcontrol_value is '0.0 ~ 60.0 [%]';
comment on column tb_test_configuration_gens3_temp.feedinrelay_feed_in_relay_enable_flag is '0: Disable, 1: Enable';
comment on column tb_test_configuration_gens3_temp.feedinrelay_relay_1_attach_level is '0 ~ 10000 [W]';
comment on column tb_test_configuration_gens3_temp.feedinrelay_relay_1_detach_level is '0 ~ 10000 [W]';
comment on column tb_test_configuration_gens3_temp.feedinrelay_relay_2_attach_level is '0 ~ 10000 [W]';
comment on column tb_test_configuration_gens3_temp.feedinrelay_relay_2_detach_level is '0 ~ 10000 [W]';
comment on column tb_test_configuration_gens3_temp.feedinrelay_relay_3_attach_level is '0 ~ 10000 [W]';
comment on column tb_test_configuration_gens3_temp.feedinrelay_relay_3_detach_level is '0 ~ 10000 [W]';
comment on column tb_test_configuration_gens3_temp.feedinrelay_relay_4_attach_level is '0 ~ 10000 [W]';
comment on column tb_test_configuration_gens3_temp.feedinrelay_relay_4_detach_level is '0 ~ 10000 [W]';
comment on column tb_test_configuration_gens3_temp.reconnectiontime_time is '0 ~ 65535 [sec]';
comment on column tb_test_configuration_gens3_temp.taucoefficient is '0.0 ~ 6.0 [Tau]';
comment on column tb_test_configuration_gens3_temp.create_dt is 'HeatPump관련 정보 (추후 더 많은 정보 추가 예정)';
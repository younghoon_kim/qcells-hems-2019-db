CREATE OR REPLACE FUNCTION wems.sp_check_ess_login_status(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/* *****************************************************************************
   NAME:       SP_CHECK_ESS_LOGIN_STATUS
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-09-11   yngwie     	Created this function.
   2.0        2019-09-25   이재형		postgresql 변환
   2.1        2019-12-12   박동환		성능개선
   2.2        2020-01-23   박동환		성능개선(where문의 SYS_EXTRACT_UTC(now())를 상수화)
   2.3		  2020-02-21   김현덕		Offline 상태이지만 데이터가 올라오는 장비의 상태를 바꿔주는 부분의 오류수정
   2.4        2020-04-17   박동환		성능개선(where문의 시간계산을 상수화)
   2.5        2020-05-27   서정우		1주일 이전에 로그인한 경우 아래 프로시져로 logout 처리된후 login이 처리 되지 않는현상 개선
   2.6        2020-06-08   서정우		m2m에서 logout처리후, 바로 프로시져에서 login 처리하는 현상 개선
   2.7        2020-06-12   김현덕		쿼리 오류 수정(column "tb_raw_login.device_id" must appear in the GROUP BY clause or be used in an aggregate function_|~_|~)
   2.8        2020-01-12   김영훈        Monitoring 데이터 테이블 삭제에 따라 tb_raw_ems_info 테이블 대신 tb_opr_ess_state 테이블의 colec_dt를 온/오프라인 판단에 사용.

   NOTES:   TB_RAW_EMS_INFO의 수집상황을 판단해 ESS의 LOGIN 상태를 TB_RAW_LOGIN에 기록
  				(입력시간 무관하게 호출된 시간 기준 처리)
   -> 변경:
             TB_OPR_ESS_STATE의 수집상황을 판단해 ESS의 LOGIN 상태를 TB_RAW_LOGIN에 기록
  				(입력시간 무관하게 호출된 시간 기준 처리)
	Input :

	Usage :	SELECT SP_CHECK_ESS_LOGIN_STATUS('SP_CHECK_ESS_LOGIN_STATUS', 0, '20191024123000', 'M')

******************************************************************************/

DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd		VARCHAR   := '0';   			-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt		INTEGER   := 0;     			-- 적재수
    v_work_num		INTEGER   := 0;     			-- 처리건수
    v_err_msg		VARCHAR   := NULL;  			-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  			-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  			-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  			-- 메시지내용3

    -- (2)업무처리 변수
	v_cnt			INTEGER  	:= 0;   			-- 적재수
    v_baseTm   		TIMESTAMPTZ;

BEGIN

	-- 2분마다 실행
	IF SUBSTR(i_exe_base, 11, 2)::NUMERIC % 2 = 1 THEN
		RETURN '1';
	END IF;

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';

	   	v_baseTm := SYS_EXTRACT_UTC(now());


		WITH CT AS (
			SELECT SYS_EXTRACT_UTC(now()) tm
		), A AS (
			SELECT *
				,DATE_PART('MINUTE', (v_baseTm - TO_TIMESTAMP(create_tm, 'YYYYMMDDHH24MISS')) ) LOGIN_GAP_MIN	--M2M에서 tb_raw_login에 로그인 정보를 직접 넣어주도록 변경되어서 create_dt와 create_tm의 유의미한 차이는 없음
			FROM(
				SELECT *
				FROM (
					SELECT  *
						, ROW_NUMBER() OVER (PARTITION BY DEVICE_ID ORDER BY CREATE_DT DESC, CREATE_TM DESC, DATA_SEQ DESC) SEQ
					FROM  TB_RAW_LOGIN
					CROSS JOIN CT
				) Z
				WHERE SEQ = 1
			) YY
	   ), norLogin AS(
			SELECT *
			FROM (
				SELECT  *, ROW_NUMBER() OVER (PARTITION BY DEVICE_ID ORDER BY CREATE_DT DESC, CREATE_TM DESC, DATA_SEQ DESC) SEQ
				FROM   	TB_RAW_LOGIN
				JOIN CT ON 1=1
				WHERE response_cd='0' AND connector_id IS NOT NULL
			) Z
			WHERE SEQ = 1
	   ), B AS (
			SELECT 	 B.DEVICE_ID
					, DATE_PART('MINUTE', (v_baseTm - TO_TIMESTAMP(MAX(B.COLEC_DT), 'YYYYMMDDHH24MISS')) ) GAP_MIN
			FROM 	TB_OPR_ESS_STATE B, A
--			WHERE 	B.COLEC_DT > TO_CHAR(A.tm - INTERVAL '1 hour', 'YYYYMMDDHH24MISS')
--			AND 	B.COLEC_DT <= TO_CHAR(A.tm, 'YYYYMMDDHH24MISS')t
			WHERE 	B.COLEC_DT > TO_CHAR(v_baseTm - INTERVAL '1 hour', 'YYYYMMDDHH24MISS')
			AND 	B.COLEC_DT <= TO_CHAR(v_baseTm, 'YYYYMMDDHH24MISS')
			AND		A.DEVICE_ID =  B.DEVICE_ID
			GROUP BY B.DEVICE_ID, v_baseTm

	   ), C AS (
	       SELECT
					CASE
						WHEN A.RESPONSE_CD = '0' AND B.GAP_MIN > 7 THEN 'ABNORMAL'
						WHEN A.RESPONSE_CD = '128' AND B.GAP_MIN BETWEEN 0 AND 6 AND  A.LOGIN_GAP_MIN > 7 THEN 'NORMAL'
						ELSE NULL
					END STATUS
					, A.*
		   FROM  A, B
		   WHERE A.DEVICE_ID = B.DEVICE_ID

	   ), sts as (
	   		SELECT  	STATUS,
						SEQ,
						DEVICE_ID,
						CREATE_TM,
						PRODUCT_MODEL_NM,
						PASSWORD  ,
						EMS_MODEL_NM,
						PCS_MODEL_NM,
						BMS_MODEL_NM,
						EMS_VER ,
						PCS_VER ,
						BMS_VER ,
						EMS_UPDATE_DT,
						EMS_UPDATE_ERR_CD,
						PCS_UPDATE_DT,
						PCS_UPDATE_ERR_CD,
						BMS_UPDATE_DT,
						BMS_UPDATE_ERR_CD,
						RESPONSE_CD,
						EMS_IPADDRESS,
						EMS_OUT_IPADDRESS,
						CONNECTOR_ID,
						EMS_TM,
						DATA_SEQ,
						CREATE_DT,
						tm
		    FROM C
		    WHERE  	STATUS IN ('ABNORMAL','NORMAL')

	   ), abn AS (
			 INSERT INTO TB_RAW_LOGIN (
			 	DEVICE_ID
			 	, CREATE_TM
			 	, PRODUCT_MODEL_NM
			 	, RESPONSE_CD
			 	, CREATE_DT
			 	, DATA_SEQ
			 	, EMS_IPADDRESS
			 	, EMS_OUT_IPADDRESS
			 	, EMS_TM
			 	, EMS_MODEL_NM
			 	, PCS_MODEL_NM
			 	, BMS_MODEL_NM
			 	, EMS_VER
			 	, PCS_VER
			 	, BMS_VER
			 	, EMS_UPDATE_DT
			 	, EMS_UPDATE_ERR_CD
			 	, PCS_UPDATE_DT
			 	, PCS_UPDATE_ERR_CD
			 	, BMS_UPDATE_DT
			 	, BMS_UPDATE_ERR_CD
			 )
			 SELECT
			 	DEVICE_ID
			 	, TO_CHAR( tm, 'YYYYMMDDHH24MISS') AS CREATE_TM
			 	, PRODUCT_MODEL_NM
			 	, '128' AS RESPONSE_CD
			 	, tm AS CREATE_DT
			 	, nextval('SQ_RAW_LOGIN') AS DATA_SEQ
			 	, 'BY SP_CHECK_ESS_LOGIN' AS EMS_IPADDRESS
			 	, '127.0.0.1' AS EMS_OUT_IPADDRESS
			 	, TO_CHAR(tm, 'YYYYMMDDHH24MISS') AS EMS_TM
			 	, EMS_MODEL_NM
			 	, PCS_MODEL_NM
			 	, BMS_MODEL_NM
			 	, EMS_VER
			 	, PCS_VER
			 	, BMS_VER
			 	, EMS_UPDATE_DT
			 	, EMS_UPDATE_ERR_CD
			 	, PCS_UPDATE_DT
			 	, PCS_UPDATE_ERR_CD
			 	, BMS_UPDATE_DT
			 	, BMS_UPDATE_ERR_CD
			 FROM	 	sts
			 WHERE	 	STATUS = 'ABNORMAL'

			 RETURNING *
	   ), nor AS (
	         INSERT INTO TB_RAW_LOGIN (
			 	DEVICE_ID
			 	, CREATE_TM
			 	, PRODUCT_MODEL_NM
			 	, PASSWORD
			 	, EMS_MODEL_NM
			 	, PCS_MODEL_NM
			 	, BMS_MODEL_NM
			 	, EMS_VER
			 	, PCS_VER
			 	, BMS_VER
			 	, EMS_UPDATE_DT
			 	, EMS_UPDATE_ERR_CD
			 	, PCS_UPDATE_DT
			 	, PCS_UPDATE_ERR_CD
			 	, BMS_UPDATE_DT
			 	, BMS_UPDATE_ERR_CD
			 	, RESPONSE_CD
			 	, EMS_IPADDRESS
			 	, EMS_OUT_IPADDRESS
			 	, CONNECTOR_ID
			 	, EMS_TM
			 	, DATA_SEQ
			 )
			 SELECT
			 	DEVICE_ID
			 	, TO_CHAR(tm, 'YYYYMMDDHH24MISS') AS CREATE_TM
			 	, PRODUCT_MODEL_NM
			 	, PASSWORD
			     , EMS_MODEL_NM
			 	, PCS_MODEL_NM
			 	, BMS_MODEL_NM
			 	, EMS_VER
			 	, PCS_VER
			 	, BMS_VER
			 	, EMS_UPDATE_DT
			 	, EMS_UPDATE_ERR_CD
			 	, PCS_UPDATE_DT
			 	, PCS_UPDATE_ERR_CD
			 	, BMS_UPDATE_DT
			 	, BMS_UPDATE_ERR_CD
			 	, '0' AS RESPONSE_CD
			     , 'BY SP_CHECK_ESS_LOGIN' AS EMS_IPADDRESS
			 	, EMS_OUT_IPADDRESS
			 	, CONNECTOR_ID
			 	, TO_CHAR(tm, 'YYYYMMDDHH24MISS') AS EMS_TM
			 	, nextval('SQ_RAW_LOGIN') AS DATA_SEQ
			 FROM (
			 	SELECT	norLogin.*
			 	FROM	norLogin, sts
			 	WHERE  sts.STATUS = 'NORMAL'
					AND sts.DEVICE_ID = norLogin.DEVICE_ID
			 		--AND norLogin.CREATE_TM > TO_CHAR(sts.tm - interval '7 day', 'YYYYMMDDHH24MISS')
					AND norLogin.CREATE_TM <= TO_CHAR(sts.tm, 'YYYYMMDDHH24MISS')
			 ) tmpA

			 RETURNING *
		)
		SELECT  SUM(CNT)
		FROM (
			SELECT count(*) CNT FROM abn
			UNION ALL
			SELECT count(*) CNT FROM nor
			) Z
		INTO v_work_num;


	     v_rslt_cnt := v_rslt_cnt + v_work_num;


	     -- 실행성공
	     v_rslt_cd := '1';

    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;

   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
   	return v_rslt_cd;

END;
$function$
;

create or replace function fn_count_table(i_schemaname character varying) returns text
	language plpgsql
as $$
/******************************************************************************
      NAME:       fn_count_table
      PURPOSE:
    
      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        2020-04-27  이재형                       최초생성   
      
      NOTES:   테이블별 COUNT를 하여 이관 문제 여부 파악한다.
                       < INPUT >
                        i_schemaname 스미카명
    
                       < OUTPUT >
                        'error' 데이터 이관 문제( count 0 )
                        'ok' 데이터 정상 이관
    *****************************************************************************/
-- 수동 실행 스크립트
-- SELECT fn_count_table('wems');
declare
	v_tname 			varchar;
	v_cnt	            numeric;
	v_text              text;
    v_row           	record; 
    v_err_cnt			numeric :=0 ;
begin
	for v_row in (
			with partition_parents as (
			    select relnamespace::regnamespace::text as schema_name,
			           relname as table_name,
			           'partition_parent' as info
			      from pg_class
			     where relkind = 'p'
			     and relnamespace::regnamespace::text = i_schemaname)
			, unpartitioned_tables as (     
			    select relnamespace::regnamespace::text as schema_name,
			           relname as table_name,
			           'unpartitioned_table' as info
			      from pg_class
			     where relkind = 'r'
			     and not relispartition
			     and relnamespace::regnamespace::text = i_schemaname
			     ) -- Regular table
			select table_name 
			from (
			select table_name from partition_parents
			union
			select table_name from unpartitioned_tables
			order by table_name
			) list
			where list.table_name 
			in (
				'batch_inf'
				,'dc_cmd_session_ident'
				,'dc_cnf_connection'
				,'dc_cnf_connector'
				,'dc_cnf_manager'
				,'dc_cnf_server'
				,'tb_bas_api_user'
				,'tb_bas_api_user_device'
				,'tb_bas_city'
				,'tb_bas_cntry'
				,'tb_bas_ctl_grp'
				,'tb_bas_ctl_grp_mgt'
				,'tb_bas_device'
				,'tb_bas_elpw_unit_price'
				,'tb_bas_mkr'
				,'tb_bas_model_alarm_code_map'
				,'tb_bas_off_device_hist'
				,'tb_bas_oprtr_device'
				,'tb_bas_ptnr'
				,'tb_bas_svc_cntr'
				,'tb_bas_timezone'
				,'tb_bas_upt_grp'
				,'tb_bas_upt_grp_hist'
				,'tb_bas_upt_grp_mgt'
				,'tb_bas_user'
				,'tb_bas_user_device'
				,'tb_bas_user_hash'
				,'tb_bas_utility_installer'
				,'tb_opr_act'
				,'tb_opr_alarm'
				,'tb_opr_alarm_hist'
				,'tb_opr_ctrl_result'
				,'tb_opr_cust_qna'
				,'tb_opr_cust_qna_file'
				,'tb_opr_cust_qna_repl'
				,'tb_opr_dnld'
				,'tb_opr_ess_config'
				,'tb_opr_ess_config_hist'
				,'tb_opr_ess_config_mon'
				,'tb_opr_ess_config_mon_hist'
				,'tb_opr_ess_state'
				,'tb_opr_ess_state_hist'
				,'tb_opr_ess_state_tl'
				,'tb_opr_faq'
				,'tb_opr_instlr_ess_state'
				,'tb_opr_mng_file'
				,'tb_opr_mtnc'
				,'tb_opr_oper_ctrl'
				,'tb_opr_oper_ctrl_hist'
				,'tb_opr_oper_logfile_hist'
				,'tb_opr_pvcalc_ess_info'
				,'tb_opr_pvcalc_hist'
				,'tb_opr_pvcalc_pv_info'
				,'tb_opr_terms_hist'
				,'tb_opr_voc'
				,'tb_opr_voc_repl'
				,'tb_opr_was_state'
				,'tb_plf_alarm_code'
				,'tb_plf_carb_emi_quity'
				,'tb_plf_code_desc_trans'
				,'tb_plf_code_grp'
				,'tb_plf_code_info'
				,'tb_plf_daq_type'
				,'tb_plf_device_fw_hist'
				,'tb_plf_elpw_prod'
				,'tb_plf_firmware_info'
				,'tb_plf_firmware_info_hist'
				,'tb_plf_menu'
				,'tb_plf_menu_auth'
				,'tb_plf_notice'
				,'tb_plf_num_list'
				,'tb_plf_popup'
				,'tb_plf_tbl_meta'
				,'tb_raw_bms_info'
				,'tb_raw_ems_info'
				,'tb_raw_ess_info'
				,'tb_raw_login'
				,'tb_raw_pcs_info'
				,'tb_stt_ess_dd'
				,'tb_stt_ess_mm'
				,'tb_stt_ess_tm'
				,'tb_stt_ess_yy'
				,'tb_stt_quity_wrty_dd'
				,'tb_stt_rank_dd'
				,'tb_stt_rank_mm'
				,'tb_sys_login_hist'
				,'tb_sys_sec_hist'
				,'tb_sys_sp_exe_hst'
				,'tb_sys_system_hist'
				,'tbd_eqp_hosthw'
			)	
	)
	 loop
	     v_tname := v_row.table_name; 
		 execute 'select count(*)  from '|| v_row.table_name  into v_cnt;
		 
	     if v_cnt = 0 then
	        raise notice ' table name[%], count[%]',v_tname,v_cnt;  
	        v_err_cnt := v_err_cnt+1;
	     end if;

	 end loop;
	
	if v_err_cnt > 0 then
	  return 'error';
	else
	  return 'ok';
	end if;
end;
$$ 

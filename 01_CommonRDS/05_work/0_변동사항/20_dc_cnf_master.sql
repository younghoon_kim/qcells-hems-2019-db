-- Drop table
DROP TABLE IF EXISTS dc_cnf_master;

-- Create table
create table dc_cnf_master
(
	ip varchar(100) not null
		constraint dc_cnf_master_pk
			primary key,
	cmdport numeric(8) not null,
	hcport numeric(8) not null,
	status integer default 1 not null,
	front_tgt_yn varchar(1) default 'N'::character varying not null,
	lnc_yn varchar(1) default 'N'::character varying not null,
	use_yn varchar(1) default 'Y'::character varying not null,
	lst_dt timestamp with time zone default sys_extract_utc(now())
);

comment on table dc_cnf_master is 'DAQ Master 정보';
comment on column dc_cnf_master.ip is 'ipaddress or 도메인';
comment on column dc_cnf_master.cmdport is '제어명령 포트';
comment on column dc_cnf_master.hcport is 'health check 포트';
comment on column dc_cnf_master.status is '상태정보 1:정상 2:비정상';
comment on column dc_cnf_master.front_tgt_yn is 'Web 이 바라보는 Master 인지의 여부';
comment on column dc_cnf_master.lnc_yn is 'Load Balancer 유무(이중화시에 Master를 LB Group 으로 묶어야 함)'
comment on column dc_cnf_master.use_yn is 'Master 사용 유무';
comment on column dc_cnf_master.lst_dt is '최종 수정 일시';

alter table dc_cnf_master owner to wems;

-- Insert data
-- INSERT INTO wems.dc_cnf_master (ip, cmdport, hcport, status, front_tgt_yn, lnc_yn, use_yn, lst_dt) VALUES ('11.1.40.200', 3436, 3435, 2, 'N', 'N', 'N', '2020-06-30 01:20:51.886622');
INSERT INTO wems.dc_cnf_master (ip, cmdport, hcport, status, front_tgt_yn, lnc_yn, use_yn, lst_dt) VALUES ('11.1.27.230', 3436, 3435, 1, 'Y', 'N', 'Y', '2020-08-13 03:09:28.864062');
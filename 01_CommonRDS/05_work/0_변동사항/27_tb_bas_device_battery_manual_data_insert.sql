with join_site_id_tb_bas_device as (
    select t2.site_id, t1.device_id, t1.product_model_nm, battery1, battery2, battery3 , t1.create_dt
    from tb_bas_device as t1
             join tb_bas_super_device as t2
                  on t1.device_id = t2.device_id)
insert into tb_bas_device_battery_manual (site_id, device_type_cd, prn_device_id, battery1, battery2, battery3, create_dt)
(
select site_id, 'BAT' as device_type_cd, device_id as prn_device_id,
       battery1, battery2, battery3, create_dt
from join_site_id_tb_bas_device)
-- Drop table

-- DROP TABLE wems.tb_stt_ess_mm;

CREATE TABLE wems.tb_stt_ess_mm (
	colec_mm varchar(6) NOT NULL, -- 집계월
	utc_offset numeric(4,2) NOT NULL, -- UTC Offset
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	pv_pw_h numeric(18,2) NULL, -- PV전력량
	pv_pw_price numeric(22,4) NULL, -- PV전력요금
	pv_pw_co2 numeric(20,6) NULL, -- PV탄소배출량
	cons_pw_h numeric(18,2) NULL, -- 가정내소비전력량-TOTAL
	cons_pw_price numeric(22,4) NULL, -- 가정내소비전력요금-TOTAL
	cons_pw_co2 numeric(20,6) NULL, -- 가정내소비전력탄소배출량-TOTAL
	cons_grid_pw_h numeric(18,2) NULL, -- 가정내소비전력량-GRID
	cons_grid_pw_price numeric(22,4) NULL, -- 가정내소비전력요금-GRID
	cons_grid_pw_co2 numeric(20,6) NULL, -- 가정내소비전력탄소배출량-GRID
	cons_pv_pw_h numeric(18,2) NULL, -- 가정내소비전력량-PV
	cons_pv_pw_price numeric(22,4) NULL, -- 가정내소비전력요금-PV
	cons_pv_pw_co2 numeric(20,6) NULL, -- 가정내소비전력탄소배출량-PV
	cons_bt_pw_h numeric(18,2) NULL, -- 가정내소비전력량-배터리
	cons_bt_pw_price numeric(22,4) NULL, -- 가정내소비전력요금-배터리
	cons_bt_pw_co2 numeric(20,6) NULL, -- 가정내소비전력탄소배출량-배터리
	bt_chrg_pw_h numeric(18,2) NULL, -- 배터리전력량-충전
	bt_chrg_pw_price numeric(22,4) NULL, -- 배터리전력요금-충전
	bt_chrg_pw_co2 numeric(20,6) NULL, -- 배터리탄소배출량-충전
	bt_dchrg_pw_h numeric(18,2) NULL, -- 배터리전력량-방전
	bt_dchrg_pw_price numeric(22,4) NULL, -- 배터리전력요금-방전
	bt_dchrg_pw_co2 numeric(20,6) NULL, -- 배터리탄소배출량-방전
	grid_ob_pw_h numeric(18,2) NULL, -- Grid전력량-수전
	grid_ob_pw_price numeric(22,4) NULL, -- Grid전력요금-수전
	grid_ob_pw_co2 numeric(20,6) NULL, -- Grid탄소배출량-수전
	grid_tr_pw_h numeric(18,2) NULL, -- Grid 전력량 - 매전 Total
	grid_tr_pw_price numeric(22,4) NULL, -- Grid 전력요금 - 매전 Total
	grid_tr_pw_co2 numeric(20,6) NULL, -- Grid 탄소배출량 - 매전 Total
	grid_tr_pv_pw_h numeric(18,2) NULL, -- Grid 전력량 - 매전 중 PV
	grid_tr_pv_pw_price numeric(22,4) NULL, -- Grid 전력요금 - 매전 중 PV
	grid_tr_pv_pw_co2 numeric(20,6) NULL, -- Grid 탄소배출량 - 매전 중 PV
	grid_tr_bt_pw_h numeric(18,2) NULL, -- Grid 전력량 - 매전 중 배터리
	grid_tr_bt_pw_price numeric(22,4) NULL, -- Grid 전력요금 - 매전 중 배터리
	grid_tr_bt_pw_co2 numeric(20,6) NULL, -- Grid 탄소배출량 - 매전 중 배터리
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 생성일시
	bt_soc numeric(3) NULL, -- 배터리충전량백분율(SOC) 평균
	outlet_pw_h numeric(18,2) NULL, -- Power Outlet 전력량
	outlet_pw_price numeric(18,2) NULL, -- Power Outlet 전력요금
	outlet_pw_co2 numeric(18,2) NULL, -- Power Outlet 탄소배출량
	pcs_fd_pw_h numeric(18,2) NULL, -- PCS 인버터 전력 발전량
	pcs_fd_pw_price numeric(18,2) NULL, -- PCS 인버터 전력 발전요금
	pcs_fd_pw_co2 numeric(18,2) NULL, -- PCS 인버터 전력 발전 탄소배출량
	pcs_pch_pw_h numeric(18,2) NULL, -- PCS 인버터 전력 사용량
	pcs_pch_pw_price numeric(18,2) NULL, -- PCS 인버터 전력 사용요금
	pcs_pch_pw_co2 numeric(18,2) NULL, -- PCS 인버터 전력 사용 탄소배출량
	bt_real_soc numeric(3) NULL -- real 배터리충전량백분율(SOC) 평균
)
PARTITION BY RANGE (colec_mm);
CREATE UNIQUE INDEX pk_stt_ess_mm ON ONLY wems.tb_stt_ess_mm USING btree (colec_mm, utc_offset, device_type_cd, device_id);
COMMENT ON TABLE wems.tb_stt_ess_mm IS '월별 ESS 통계데이터 정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_stt_ess_mm.colec_mm IS '집계월';
COMMENT ON COLUMN wems.tb_stt_ess_mm.utc_offset IS 'UTC Offset';
COMMENT ON COLUMN wems.tb_stt_ess_mm.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_stt_ess_mm.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_stt_ess_mm.pv_pw_h IS 'PV전력량';
COMMENT ON COLUMN wems.tb_stt_ess_mm.pv_pw_price IS 'PV전력요금';
COMMENT ON COLUMN wems.tb_stt_ess_mm.pv_pw_co2 IS 'PV탄소배출량';
COMMENT ON COLUMN wems.tb_stt_ess_mm.cons_pw_h IS '가정내소비전력량-TOTAL';
COMMENT ON COLUMN wems.tb_stt_ess_mm.cons_pw_price IS '가정내소비전력요금-TOTAL';
COMMENT ON COLUMN wems.tb_stt_ess_mm.cons_pw_co2 IS '가정내소비전력탄소배출량-TOTAL';
COMMENT ON COLUMN wems.tb_stt_ess_mm.cons_grid_pw_h IS '가정내소비전력량-GRID';
COMMENT ON COLUMN wems.tb_stt_ess_mm.cons_grid_pw_price IS '가정내소비전력요금-GRID';
COMMENT ON COLUMN wems.tb_stt_ess_mm.cons_grid_pw_co2 IS '가정내소비전력탄소배출량-GRID';
COMMENT ON COLUMN wems.tb_stt_ess_mm.cons_pv_pw_h IS '가정내소비전력량-PV';
COMMENT ON COLUMN wems.tb_stt_ess_mm.cons_pv_pw_price IS '가정내소비전력요금-PV';
COMMENT ON COLUMN wems.tb_stt_ess_mm.cons_pv_pw_co2 IS '가정내소비전력탄소배출량-PV';
COMMENT ON COLUMN wems.tb_stt_ess_mm.cons_bt_pw_h IS '가정내소비전력량-배터리';
COMMENT ON COLUMN wems.tb_stt_ess_mm.cons_bt_pw_price IS '가정내소비전력요금-배터리';
COMMENT ON COLUMN wems.tb_stt_ess_mm.cons_bt_pw_co2 IS '가정내소비전력탄소배출량-배터리';
COMMENT ON COLUMN wems.tb_stt_ess_mm.bt_chrg_pw_h IS '배터리전력량-충전';
COMMENT ON COLUMN wems.tb_stt_ess_mm.bt_chrg_pw_price IS '배터리전력요금-충전';
COMMENT ON COLUMN wems.tb_stt_ess_mm.bt_chrg_pw_co2 IS '배터리탄소배출량-충전';
COMMENT ON COLUMN wems.tb_stt_ess_mm.bt_dchrg_pw_h IS '배터리전력량-방전';
COMMENT ON COLUMN wems.tb_stt_ess_mm.bt_dchrg_pw_price IS '배터리전력요금-방전';
COMMENT ON COLUMN wems.tb_stt_ess_mm.bt_dchrg_pw_co2 IS '배터리탄소배출량-방전';
COMMENT ON COLUMN wems.tb_stt_ess_mm.grid_ob_pw_h IS 'Grid전력량-수전';
COMMENT ON COLUMN wems.tb_stt_ess_mm.grid_ob_pw_price IS 'Grid전력요금-수전';
COMMENT ON COLUMN wems.tb_stt_ess_mm.grid_ob_pw_co2 IS 'Grid탄소배출량-수전';
COMMENT ON COLUMN wems.tb_stt_ess_mm.grid_tr_pw_h IS 'Grid 전력량 - 매전 Total';
COMMENT ON COLUMN wems.tb_stt_ess_mm.grid_tr_pw_price IS 'Grid 전력요금 - 매전 Total';
COMMENT ON COLUMN wems.tb_stt_ess_mm.grid_tr_pw_co2 IS 'Grid 탄소배출량 - 매전 Total';
COMMENT ON COLUMN wems.tb_stt_ess_mm.grid_tr_pv_pw_h IS 'Grid 전력량 - 매전 중 PV';
COMMENT ON COLUMN wems.tb_stt_ess_mm.grid_tr_pv_pw_price IS 'Grid 전력요금 - 매전 중 PV';
COMMENT ON COLUMN wems.tb_stt_ess_mm.grid_tr_pv_pw_co2 IS 'Grid 탄소배출량 - 매전 중 PV';
COMMENT ON COLUMN wems.tb_stt_ess_mm.grid_tr_bt_pw_h IS 'Grid 전력량 - 매전 중 배터리';
COMMENT ON COLUMN wems.tb_stt_ess_mm.grid_tr_bt_pw_price IS 'Grid 전력요금 - 매전 중 배터리';
COMMENT ON COLUMN wems.tb_stt_ess_mm.grid_tr_bt_pw_co2 IS 'Grid 탄소배출량 - 매전 중 배터리';
COMMENT ON COLUMN wems.tb_stt_ess_mm.create_dt IS '생성일시';
COMMENT ON COLUMN wems.tb_stt_ess_mm.bt_soc IS '배터리충전량백분율(SOC) 평균';
COMMENT ON COLUMN wems.tb_stt_ess_mm.outlet_pw_h IS 'Power Outlet 전력량';
COMMENT ON COLUMN wems.tb_stt_ess_mm.outlet_pw_price IS 'Power Outlet 전력요금';
COMMENT ON COLUMN wems.tb_stt_ess_mm.outlet_pw_co2 IS 'Power Outlet 탄소배출량';
COMMENT ON COLUMN wems.tb_stt_ess_mm.pcs_fd_pw_h IS 'PCS 인버터 전력 발전량';
COMMENT ON COLUMN wems.tb_stt_ess_mm.pcs_fd_pw_price IS 'PCS 인버터 전력 발전요금';
COMMENT ON COLUMN wems.tb_stt_ess_mm.pcs_fd_pw_co2 IS 'PCS 인버터 전력 발전 탄소배출량';
COMMENT ON COLUMN wems.tb_stt_ess_mm.pcs_pch_pw_h IS 'PCS 인버터 전력 사용량';
COMMENT ON COLUMN wems.tb_stt_ess_mm.pcs_pch_pw_price IS 'PCS 인버터 전력 사용요금';
COMMENT ON COLUMN wems.tb_stt_ess_mm.pcs_pch_pw_co2 IS 'PCS 인버터 전력 사용 탄소배출량';
COMMENT ON COLUMN wems.tb_stt_ess_mm.bt_real_soc IS 'real 배터리충전량백분율(SOC) 평균';

-- Permissions

ALTER TABLE wems.tb_stt_ess_mm OWNER TO wems;
GRANT ALL ON TABLE wems.tb_stt_ess_mm TO wems;

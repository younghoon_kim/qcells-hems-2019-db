--table에 컬럼 추가.
alter table wems.tb_opr_ess_state
	add is_connect boolean default false;

comment on column wems.tb_opr_ess_state.is_connect is '연결/미연결 상태 저장';

alter table wems.tb_opr_ess_state_tmp
	add is_connect boolean default false;

comment on column wems.tb_opr_ess_state_tmp.is_connect is '연결/미연결 상태 저장';

alter table wems.tb_opr_ess_state_hist
	add is_connect boolean default false;

comment on column wems.tb_opr_ess_state_hist.is_connect is '연결/미연결 상태 저장';

-- alter table wems.tb_opr_ess_state_hist_tmp
-- 	add is_connect boolean default false;
--
-- comment on column wems.tb_opr_ess_state_hist_tmp.is_connect is '연결/미연결 상태 저장';


select * from tb_opr_ess_state;

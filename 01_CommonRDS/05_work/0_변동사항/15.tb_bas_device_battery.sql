create table tb_bas_device_battery
(
    id        integer     not null,
    device_id varchar,
--     product_model_nm          varchar,  --33j
    module    jsonb,
    capacity  varchar(64) not null,
    create_dt timestamp with time zone default now(),
    constraint tb_bas_device_battery_pk
        primary key (site_id, id, prn_device_id)
) INHERITS (tb_bas_super_device);

-- alter table tb_bas_device_battery
--     owner to wems;

--2020.11.30 younghoon.kim Unique index 불필요 처리
/*
create unique index tb_bas_device_battery_module_uindex
    on tb_bas_device_battery (module);

create unique index tb_bas_device_battery_device_id_uindex
    on tb_bas_device_battery (device_id);
 */

comment on table tb_bas_device_battery is '베터리 정보를 저장';
comment on column tb_bas_device_battery.id is 'battery_id';
comment on column tb_bas_device_battery.device_id is '장비ID';
comment on column tb_bas_device_battery.module is 'module정보,id,serial number 등';
comment on column tb_bas_device_battery.capacity is '용량';
comment on column tb_bas_device_battery.create_dt is '등록일시';

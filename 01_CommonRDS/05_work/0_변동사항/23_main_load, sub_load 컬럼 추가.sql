-- main_load power와 sub_load power를 저장하도록 컬럼 추가.
-- tb_opr_ess_state
alter table tb_opr_ess_state add load_main_pw numeric(18,2);
alter table tb_opr_ess_state add load_sub_pw numeric(18,2);


comment on column tb_opr_ess_state.load_main_pw is 'Load 중 main_load의 power';
comment on column tb_opr_ess_state.load_sub_pw is 'Load 중 sub_load의 power';

--tb_opr_ess_state_tmp
alter table tb_opr_ess_state_tmp add load_main_pw numeric(18,2);
alter table tb_opr_ess_state_tmp add load_sub_pw numeric(18,2);

comment on column tb_opr_ess_state_tmp.load_main_pw is 'Load 중 main_load의 power';
comment on column tb_opr_ess_state_tmp.load_sub_pw is 'Load 중 sub_load의 power';

-- tb_opr_ess_state_hist
alter table tb_opr_ess_state_hist add load_main_pw numeric(18,2);
alter table tb_opr_ess_state_hist add load_sub_pw numeric(18,2);

comment on column tb_opr_ess_state_hist.load_main_pw is 'Load 중 main_load의 power';
comment on column tb_opr_ess_state_hist.load_sub_pw is 'Load 중 sub_load의 power';

-- tb_stt_ess_tm
alter table tb_stt_ess_tm add load_main_pw numeric(18,2);
alter table tb_stt_ess_tm add load_sub_pw numeric(18,2);

comment on column tb_stt_ess_tm.load_main_pw is 'Load 중 main_load의 power';
comment on column tb_stt_ess_tm.load_sub_pw is 'Load 중 sub_load의 power';

-- tb_stt_ess_dd
alter table tb_stt_ess_dd add load_main_pw numeric(18,2);
alter table tb_stt_ess_dd add load_sub_pw numeric(18,2);

comment on column tb_stt_ess_dd.load_main_pw is 'Load 중 main_load의 power';
comment on column tb_stt_ess_dd.load_sub_pw is 'Load 중 sub_load의 power';



-- load_main_pw_h, load_sub_pw_h 추가
-- tb_opr_ess_state
alter table tb_opr_ess_state add load_main_pw_h numeric(18,2);
alter table tb_opr_ess_state add load_sub_pw_h numeric(18,2);


comment on column tb_opr_ess_state.load_main_pw_h is 'Load 중 main_load의 Electrical energy';
comment on column tb_opr_ess_state.load_sub_pw_h is 'Load 중 sub_load의 Electrical energy';

--tb_opr_ess_state_tmp
alter table tb_opr_ess_state_tmp add load_main_pw_h numeric(18,2);
alter table tb_opr_ess_state_tmp add load_sub_pw_h numeric(18,2);

comment on column tb_opr_ess_state_tmp.load_main_pw_h is 'Load 중 main_load의 Electrical energy';
comment on column tb_opr_ess_state_tmp.load_sub_pw_h is 'Load 중 sub_load의 Electrical energy';

-- tb_opr_ess_state_hist
alter table tb_opr_ess_state_hist add load_main_pw_h numeric(18,2);
alter table tb_opr_ess_state_hist add load_sub_pw_h numeric(18,2);

comment on column tb_opr_ess_state_hist.load_main_pw_h is 'Load 중 main_load의 Electrical energy';
comment on column tb_opr_ess_state_hist.load_sub_pw_h is 'Load 중 sub_load의 Electrical energy';

-- tb_stt_ess_tm
alter table tb_stt_ess_tm add load_main_pw_h numeric(18,2);
alter table tb_stt_ess_tm add load_sub_pw_h numeric(18,2);

comment on column tb_stt_ess_tm.load_main_pw_h is 'Load 중 main_load의 Electrical energy';
comment on column tb_stt_ess_tm.load_sub_pw_h is 'Load 중 sub_load의 Electrical energy';

-- tb_stt_ess_dd
alter table tb_stt_ess_dd add load_main_pw_h numeric(18,2);
alter table tb_stt_ess_dd add load_sub_pw_h numeric(18,2);

comment on column tb_stt_ess_dd.load_main_pw_h is 'Load 중 main_load의 Electrical energy';
comment on column tb_stt_ess_dd.load_sub_pw_h is 'Load 중 sub_load의 Electrical energy';

-- tb_stt_ess_mm
alter table tb_stt_ess_mm add load_main_pw_h numeric(18,2);
alter table tb_stt_ess_mm add load_sub_pw_h numeric(18,2);

comment on column tb_stt_ess_mm.load_main_pw_h is 'Load 중 main_load의 Electrical energy';
comment on column tb_stt_ess_mm.load_sub_pw_h is 'Load 중 sub_load의 Electrical energy';


--battery rack의 총 개수 정보 추가
alter table tb_bas_device_battery
    add total_rack_cnt integer;
comment on column tb_bas_device_battery.total_rack_cnt is 'site내 연결된 총 rack의 수';


alter table tb_bas_device_battery_hist
    add total_rack_cnt integer;

comment on column tb_bas_device_battery_hist.total_rack_cnt is 'site내 연결된 총 rack의 수';


--profile code 추가
alter table tb_opr_ess_config_mon_v2
add base_profile_cd integer;

comment on column tb_opr_ess_config_mon_v2.base_profile_cd is 'base가 되는 profile의 country_code';

alter table tb_opr_ess_config_mon_hist_v2
add base_profile_cd integer;

comment on column tb_opr_ess_config_mon_hist_v2.base_profile_cd is 'base가 되는 profile의 country_code';

alter table tb_opr_ess_profile
add base_profile_cd integer;

comment on column tb_opr_ess_profile.base_profile_cd is 'base가 되는 profile의 country_code';

alter table tb_opr_ess_config_set_v2
add base_profile_cd integer;

comment on column tb_opr_ess_config_set_v2.base_profile_cd is 'base가 되는 profile의 country_code';

alter table tb_opr_ess_config_set_hist_v2
add base_profile_cd integer;

comment on column tb_opr_ess_config_set_hist_v2.base_profile_cd is 'base가 되는 profile의 country_code';


-- Drop table

-- DROP TABLE wems.tb_opr_ess_state_hist;

CREATE TABLE wems.tb_opr_ess_state_hist (
	device_id varchar(50) NOT NULL, -- 장비ID
	device_type_cd varchar(4) NOT NULL, -- 장비유형코드
	colec_dt varchar(14) NOT NULL, -- 수집시간
	oper_stus_cd varchar(4) NULL, -- 운전상태코드
	pv_pw numeric(18,2) NULL, -- PV현재 발전 전력
	pv_pw_h numeric(18,2) NULL, -- PV일일누적 발전 전력양
	pv_pw_price numeric(22,4) NULL, -- PV일일누적 발전 전력요금
	pv_pw_co2 numeric(18,2) NULL, -- PV일일누적탄소배출량
	pv_stus_cd varchar(4) NULL, -- PV상태코드
	cons_pw numeric(18,2) NULL, -- 부하현재전력
	cons_pw_h numeric(18,2) NULL, -- 부하일일누적전력량
	cons_pw_price numeric(22,4) NULL, -- 부하일일누적전력요금
	cons_pw_co2 numeric(20,6) NULL, -- 부하일일누적탄소배출량
	bt_pw numeric(18,2) NULL, -- 배터리현재전력
	bt_chrg_pw_h numeric(18,2) NULL, -- 배터리일일누적충전량
	bt_chrg_pw_price numeric(22,4) NULL, -- 배터리일일누적충전요금
	bt_chrg_pw_co2 numeric(20,6) NULL, -- 배터리일일누적충전 탄소배출량
	bt_dchrg_pw_h numeric(18,2) NULL, -- 배터리일일누적방전량
	bt_dchrg_pw_price numeric(22,4) NULL, -- 배터리일일누적방전요금
	bt_dchrg_pw_co2 numeric(20,6) NULL, -- 배터리일일누적방전 탄소배출량
	bt_soc numeric(3) NULL, -- 배터리충전량백분율(SOC)
	bt_soh numeric(10) NULL, -- 배터리잔존수명(SOH)
	bt_stus_cd varchar(4) NULL, -- 배터리상태코드
	grid_pw numeric(18,2) NULL, -- GRID현재전력
	grid_ob_pw_h numeric(18,2) NULL, -- GRID일일누적소비전력량
	grid_ob_pw_price numeric(22,4) NULL, -- GRID일일누적소비전력요금
	grid_ob_pw_co2 numeric(20,6) NULL, -- GRID일일누적 소비전력 탄소배출량
	grid_tr_pw_h numeric(18,2) NULL, -- GRID일일누적매전량
	grid_tr_pw_price numeric(22,4) NULL, -- GRID일일누적매전요금
	grid_tr_pw_co2 numeric(20,6) NULL, -- GRID일일누적매전 탄소배출량
	grid_stus_cd varchar(4) NULL, -- GRID상태코드
	pcs_pw numeric(18,2) NULL, -- PCS 인버터 파워
	pcs_fd_pw_h numeric(18,2) NULL, -- PCS 인버터 전력 발전량
	pcs_pch_pw_h numeric(18,2) NULL, -- PCS 인버터 전력 사용량
	ems_opmode varchar(4) NULL, -- EMS 운영모드
	pcs_opmode1 varchar(4) NULL, -- PCS 운전모드
	pcs_opmode2 varchar(4) NULL, -- PCS 운전모드2
	pcs_tgt_pw numeric(18,2) NULL, -- PCS 타겟파워
	feed_in_limit varchar(4) NULL, -- Feed In Limit
	create_dt timestamptz NOT NULL DEFAULT wems.sys_extract_utc(now()), -- 등록일시
	max_inverter_pw_cd varchar(8) NULL,
	outlet_pw numeric(18,2) NULL, -- Power Outlet 전력
	outlet_pw_h numeric(18,2) NULL, -- Power Outlet 전력량
	outlet_pw_price numeric(18,2) NULL, -- Power Outlet 전력요금
	outlet_pw_co2 numeric(18,2) NULL, -- Power Outlet 탄소배출량
	pcs_comm_err_rate numeric(3) NULL, -- PCS 통신 에러율
	smtr_comm_err_rate numeric(3) NULL, -- 스마트미터 통신 에러율
	m2m_comm_err_rate numeric(3) NULL, -- M2M 통신 에러율
	pcs_flag0 varchar(8) NULL, -- PCS 운전상태 0
	pcs_flag1 varchar(8) NULL, -- PCS 운전상태 1
	pcs_flag2 varchar(8) NULL, -- PCS 운전상태 2
	pcs_opmode_cmd1 varchar(1) NULL, -- PCS 제어 컴맨드 1
	pcs_opmode_cmd2 varchar(1) NULL, -- PCS 제어 컴맨드 2
	pv_v1 numeric(5,1) NULL, -- PV 전압1
	pv_i1 numeric(4,1) NULL, -- PV 전류1
	pv_pw1 numeric(5) NULL, -- PV 파워1
	pv_v2 numeric(5,1) NULL, -- PV 전압2
	pv_i2 numeric(4,1) NULL, -- PV 전류2
	pv_pw2 numeric(5) NULL, -- PV 파워2
	inverter_v numeric(5,1) NULL, -- 인버터 전압
	inverter_i numeric(4,1) NULL, -- 인버터 전류
	inverter_pw numeric(5) NULL, -- 인버터 파워
	dc_link_v numeric(5,1) NULL, -- DC Link 전압
	g_rly_cnt float8 NULL, -- Relay Count (G-relay)
	bat_rly_cnt float8 NULL, -- Relay Count (BAT-relay)
	grid_to_grid_rly_cnt float8 NULL, -- Relay Count (Grid to Grid relay)
	bat_pchrg_rly_cnt float8 NULL, -- Relay Count (BAT-Precharge-relay)
	bms_flag0 varchar(8) NULL, -- BMS 운전상태
	bms_diag0 varchar(8) NULL, -- BMS 진단상태
	rack_v numeric(5,1) NULL, -- 랙 전압
	rack_i numeric(4,1) NULL, -- 랙 전류
	cell_max_v numeric(4,3) NULL, -- 셀 최대 전압
	cell_min_v numeric(4,3) NULL, -- 셀 최소 전압
	cell_avg_v numeric(4,3) NULL, -- 셀 평균 전압
	cell_max_t numeric(4,1) NULL, -- 셀 최대 온도
	cell_min_t numeric(4,1) NULL, -- 셀 최소 온도
	cell_avg_t numeric(4,1) NULL, -- 셀 평균 온도
	tray_cnt float8 NULL, -- 트레이 수량
	smtr_tp_cd varchar(4) NULL, -- 설치 스마트미터 기종 코드
	smtr_pulse_cnt float8 NULL, -- 설치 스마트미터 펄스카운트
	smtr_modl_cd varchar(4) NULL, -- 스마트미터 모델 코드
	pv_max_pwr1 float8 NULL, -- 설치 PV 최대 전력량1
	pv_max_pwr2 float8 NULL, -- 설치 PV 최대 전력량2
	instl_rgn varchar(10) NULL, -- 설치 지역
	mmtr_slv_addr float8 NULL, -- Slave address for Modebus Meter
	basicmode_cd varchar(4) NULL,
	sys_st_cd varchar(4) NULL, -- 시스템 상태 코드
	bt_chrg_st_cd varchar(4) NULL, -- 배터리 충전 가능여부 코드
	bt_dchrg_st_cd varchar(4) NULL, -- 배터리 방전 가능여부 코드
	pv_st_cd varchar(4) NULL, -- 인버터 방전(PV 발전 가능) 가능여부 코드
	grid_st_cd varchar(4) NULL, -- 그리드 상태 코드
	smtr_st_cd varchar(4) NULL, -- 스마트 미터(에너지 미터) 상태 코드
	smtr_tr_cnter float8 NULL, -- 스마트미터 총 사용량
	smtr_ob_cnter float8 NULL, -- 스마트미터 총 구매량
	grid_code0 varchar(50) NULL,
	grid_code1 varchar(50) NULL,
	grid_code2 varchar(50) NULL,
	pcon_bat_targetpower varchar(50) NULL,
	grid_code3 varchar(50) NULL,
	bdc_module_code0 varchar(50) NULL,
	bdc_module_code1 varchar(50) NULL,
	bdc_module_code2 varchar(50) NULL,
	bdc_module_code3 varchar(50) NULL,
	fault5_realtime_year varchar(50) NULL,
	fault5_realtime_month varchar(50) NULL,
	fault5_day varchar(50) NULL,
	fault5_hour varchar(50) NULL,
	fault5_minute varchar(50) NULL,
	fault5_second varchar(50) NULL,
	fault5_datahigh varchar(50) NULL,
	fault5_datalow varchar(50) NULL,
	fault6_realtime_year varchar(50) NULL,
	fault6_realtime_month varchar(50) NULL,
	fault6_realtime_day varchar(50) NULL,
	fault6_realtime_hour varchar(50) NULL,
	fault6_realtime_minute varchar(50) NULL,
	fault6_realtime_second varchar(50) NULL,
	fault6_datahigh varchar(50) NULL,
	fault6_datalow varchar(50) NULL,
	fault7_realtime_year varchar(50) NULL,
	fault7_realtime_month varchar(50) NULL,
	fault7_realtime_day varchar(50) NULL,
	fault7_realtime_hour varchar(50) NULL,
	fault7_realtime_minute varchar(50) NULL,
	fault7_realtime_second varchar(50) NULL,
	fault7_datahigh varchar(50) NULL,
	fault7_datalow varchar(50) NULL,
	fault8_realtime_year varchar(50) NULL,
	fault8_realtime_month varchar(50) NULL,
	fault8_realtime_day varchar(50) NULL,
	fault8_realtime_hour varchar(50) NULL,
	fault8_realtime_minute varchar(50) NULL,
	fault8_realtime_second varchar(50) NULL,
	fault8_datahigh varchar(50) NULL,
	fault8_datalow varchar(50) NULL,
	fault9_realtime_year varchar(50) NULL,
	fault9_realtime_month varchar(50) NULL,
	fault9_realtime_day varchar(50) NULL,
	fault9_realtime_hour varchar(50) NULL,
	fault9_realtime_minute varchar(50) NULL,
	fault9_realtime_second varchar(50) NULL,
	fault9_datahigh varchar(50) NULL,
	fault9_datalow varchar(50) NULL,
    is_connect boolean default false,
    bt_real_soc numeric(3) NULL
)
PARTITION BY RANGE (colec_dt);
CREATE UNIQUE INDEX pk_opr_ess_state_hist ON ONLY wems.tb_opr_ess_state_hist USING btree (colec_dt, device_id, device_type_cd);
COMMENT ON TABLE wems.tb_opr_ess_state_hist IS 'ESS의 상태이력정보를 관리한다.';

-- Column comments

COMMENT ON COLUMN wems.tb_opr_ess_state_hist.device_id IS '장비ID';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.colec_dt IS '수집시간';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.oper_stus_cd IS '운전상태코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_pw IS 'PV현재 발전 전력';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_pw_h IS 'PV일일누적 발전 전력양';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_pw_price IS 'PV일일누적 발전 전력요금';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_pw_co2 IS 'PV일일누적탄소배출량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_stus_cd IS 'PV상태코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.cons_pw IS '부하현재전력';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.cons_pw_h IS '부하일일누적전력량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.cons_pw_price IS '부하일일누적전력요금';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.cons_pw_co2 IS '부하일일누적탄소배출량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bt_pw IS '배터리현재전력';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bt_chrg_pw_h IS '배터리일일누적충전량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bt_chrg_pw_price IS '배터리일일누적충전요금';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bt_chrg_pw_co2 IS '배터리일일누적충전 탄소배출량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bt_dchrg_pw_h IS '배터리일일누적방전량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bt_dchrg_pw_price IS '배터리일일누적방전요금';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bt_dchrg_pw_co2 IS '배터리일일누적방전 탄소배출량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bt_soc IS '배터리충전량백분율(SOC)';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bt_soh IS '배터리잔존수명(SOH)';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bt_stus_cd IS '배터리상태코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.grid_pw IS 'GRID현재전력';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.grid_ob_pw_h IS 'GRID일일누적소비전력량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.grid_ob_pw_price IS 'GRID일일누적소비전력요금';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.grid_ob_pw_co2 IS 'GRID일일누적 소비전력 탄소배출량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.grid_tr_pw_h IS 'GRID일일누적매전량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.grid_tr_pw_price IS 'GRID일일누적매전요금';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.grid_tr_pw_co2 IS 'GRID일일누적매전 탄소배출량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.grid_stus_cd IS 'GRID상태코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pcs_pw IS 'PCS 인버터 파워';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pcs_fd_pw_h IS 'PCS 인버터 전력 발전량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pcs_pch_pw_h IS 'PCS 인버터 전력 사용량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.ems_opmode IS 'EMS 운영모드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pcs_opmode1 IS 'PCS 운전모드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pcs_opmode2 IS 'PCS 운전모드2';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pcs_tgt_pw IS 'PCS 타겟파워';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.feed_in_limit IS 'Feed In Limit';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.create_dt IS '등록일시';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.outlet_pw IS 'Power Outlet 전력';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.outlet_pw_h IS 'Power Outlet 전력량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.outlet_pw_price IS 'Power Outlet 전력요금';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.outlet_pw_co2 IS 'Power Outlet 탄소배출량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pcs_comm_err_rate IS 'PCS 통신 에러율';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.smtr_comm_err_rate IS '스마트미터 통신 에러율';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.m2m_comm_err_rate IS 'M2M 통신 에러율';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pcs_flag0 IS 'PCS 운전상태 0';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pcs_flag1 IS 'PCS 운전상태 1';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pcs_flag2 IS 'PCS 운전상태 2';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pcs_opmode_cmd1 IS 'PCS 제어 컴맨드 1';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pcs_opmode_cmd2 IS 'PCS 제어 컴맨드 2';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_v1 IS 'PV 전압1';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_i1 IS 'PV 전류1';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_pw1 IS 'PV 파워1';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_v2 IS 'PV 전압2';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_i2 IS 'PV 전류2';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_pw2 IS 'PV 파워2';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.inverter_v IS '인버터 전압';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.inverter_i IS '인버터 전류';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.inverter_pw IS '인버터 파워';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.dc_link_v IS 'DC Link 전압';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.g_rly_cnt IS 'Relay Count (G-relay)';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bat_rly_cnt IS 'Relay Count (BAT-relay)';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.grid_to_grid_rly_cnt IS 'Relay Count (Grid to Grid relay)';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bat_pchrg_rly_cnt IS 'Relay Count (BAT-Precharge-relay)';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bms_flag0 IS 'BMS 운전상태';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bms_diag0 IS 'BMS 진단상태';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.rack_v IS '랙 전압';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.rack_i IS '랙 전류';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.cell_max_v IS '셀 최대 전압';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.cell_min_v IS '셀 최소 전압';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.cell_avg_v IS '셀 평균 전압';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.cell_max_t IS '셀 최대 온도';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.cell_min_t IS '셀 최소 온도';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.cell_avg_t IS '셀 평균 온도';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.tray_cnt IS '트레이 수량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.smtr_tp_cd IS '설치 스마트미터 기종 코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.smtr_pulse_cnt IS '설치 스마트미터 펄스카운트';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.smtr_modl_cd IS '스마트미터 모델 코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_max_pwr1 IS '설치 PV 최대 전력량1';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_max_pwr2 IS '설치 PV 최대 전력량2';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.instl_rgn IS '설치 지역';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.mmtr_slv_addr IS 'Slave address for Modebus Meter';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.sys_st_cd IS '시스템 상태 코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bt_chrg_st_cd IS '배터리 충전 가능여부 코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.bt_dchrg_st_cd IS '배터리 방전 가능여부 코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.pv_st_cd IS '인버터 방전(PV 발전 가능) 가능여부 코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.grid_st_cd IS '그리드 상태 코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.smtr_st_cd IS '스마트 미터(에너지 미터) 상태 코드';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.smtr_tr_cnter IS '스마트미터 총 사용량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.smtr_ob_cnter IS '스마트미터 총 구매량';
COMMENT ON COLUMN wems.tb_opr_ess_state_hist.is_connect IS '연결/미연결 상태 저장';


-- Permissions

ALTER TABLE wems.tb_opr_ess_state_hist OWNER TO wems;
GRANT ALL ON TABLE wems.tb_opr_ess_state_hist TO wems;

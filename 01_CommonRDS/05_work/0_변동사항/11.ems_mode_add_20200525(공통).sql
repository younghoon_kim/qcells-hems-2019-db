insert into tb_plf_code_info (grp_cd ,code, code_nm,ref_4,cd_desc,ord_seq)
values ('EMS_RUN_MODE_CD', 'A', 'PEM', 'code.emsrunmodecd.10', 'PMS External Mode',11);

insert into tb_plf_code_info (grp_cd ,code, code_nm,ref_4,cd_desc,ord_seq)
values ('EMS_RUN_MODE_CD',  'C', 'BFC', 'code.emsrunmodecd.12','Battery Forced Charge',12);

insert into tb_plf_code_info (grp_cd ,code, code_nm,ref_4,cd_desc,ord_seq)
values ('EMS_RUN_MODE_CD', 'D', 'BFD', 'code.emsrunmodecd.13', 'Battery Forced Discharge',13);

update tb_plf_code_info set
ord_seq = 14
where grp_cd = 'EMS_RUN_MODE_CD' and code = 'X';

update tb_plf_code_info set
ord_seq = 10
where grp_cd = 'EMS_RUN_MODE_CD' and code = '9';
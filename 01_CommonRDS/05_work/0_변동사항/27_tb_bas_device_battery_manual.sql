
-- auto-generated definition
create table tb_bas_device_battery_manual
(
    site_id        integer not null,
    device_type_cd varchar,
    prn_device_id  varchar,
    battery1       varchar(128),
    battery2       varchar(128),
    battery3       varchar(128),
    update_dt      timestamp with time zone,
    create_dt      timestamp with time zone default now(),

    constraint tb_bas_device_battery_manual_pk
        primary key (site_id, prn_device_id)
);


comment on table tb_bas_device_battery_manual is '베터리 정보를 저장_수기입력';

comment on column tb_bas_device_battery_manual.site_id is 'site_id';

comment on column tb_bas_device_battery_manual.device_type_cd is '장비유형코드';

comment on column tb_bas_device_battery_manual.prn_device_id is '상위장비ID';

comment on column tb_bas_device_battery_manual.battery1 is '배터리1 시리얼번호';

comment on column tb_bas_device_battery_manual.battery2 is '배터리2 시리얼번호';

comment on column tb_bas_device_battery_manual.battery3 is '배터리3 시리얼번호';

comment on column tb_bas_device_battery_manual.update_dt is '업데이트 일시';

comment on column tb_bas_device_battery_manual.create_dt is '등록일시';


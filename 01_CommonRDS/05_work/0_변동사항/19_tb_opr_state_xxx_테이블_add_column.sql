--table에 컬럼 추가.
alter table wems.tb_opr_ess_state
	add bt_real_soc numeric(3) NULL;

comment on column wems.tb_opr_ess_state.bt_real_soc is '실제 SOC, user_soc와는 다른 개념';

alter table wems.tb_opr_ess_state_tmp
	add bt_real_soc numeric(3) NULL;

comment on column wems.tb_opr_ess_state_tmp.bt_real_soc is '실제 SOC, user_soc와는 다른 개념';

alter table wems.tb_opr_ess_state_hist
	add bt_real_soc numeric(3) NULL;

comment on column wems.tb_opr_ess_state_hist.bt_real_soc is '실제 SOC, user_soc와는 다른 개념';

-- alter table wems.tb_opr_ess_state_hist_tmp
-- 	add is_connect boolean default false;
--
-- comment on column wems.tb_opr_ess_state_hist_tmp.is_connect is '연결/미연결 상태 저장';

alter table wems.tb_stt_ess_dd
	add bt_real_soc numeric(3) NULL;
comment on column wems.tb_stt_ess_dd.bt_real_soc is '실제 SOC, user_soc와는 다른 개념';
alter table wems.tb_stt_ess_mm
	add bt_real_soc numeric(3) NULL;
comment on column wems.tb_stt_ess_mm.bt_real_soc is '실제 SOC, user_soc와는 다른 개념';
alter table wems.tb_stt_ess_tm
	add bt_real_soc numeric(3) NULL;
comment on column wems.tb_stt_ess_tm.bt_real_soc is '실제 SOC, user_soc와는 다른 개념';


select * from tb_opr_ess_state;

CREATE OR REPLACE FUNCTION sp_fw_update_result(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       SP_FW_UPDATE_RESULT
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-08-20   yngwie       Created this function.
   2.0        2019-10-02   박동환        Postgresql 변환
   2.1        2020-07-24   서정우        site_id 적용으로 인해서 tb_bas_device -> tb_bas_device_pms 로 변경

   NOTES:   펌웨어 업데이트 명령 처리
  				(입력시간 무관하게 호출된 시간 기준 처리)
   
    Input : 
	        
	Output : 
	
	Usage : SELECT SP_FW_UPDATE_RESULT('SP_FW_UPDATE_RESULT', 0, '201910221237', 'M');

*******************************************************************************/	
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_row           			record; 
   
	v_ems_update_err_cd	VARCHAR   := '';  
	v_pcs_update_err_cd   VARCHAR   := '';  
	v_bms_update_err_cd  VARCHAR   := '';  
	v_ems_model_nm      VARCHAR   := '';  
	v_bms_model_nm      VARCHAR   := '';  
	v_pcs_model_nm       VARCHAR   := '';  
	v_ems_ver               VARCHAR   := '';  
	v_bms_ver               VARCHAR   := '';  
	v_pcs_ver                	VARCHAR   := '';  
	v_ems_update_dt       VARCHAR   := '';  
	v_device_id       		VARCHAR   := '';  

	v_result       			VARCHAR   := '';  
	v_ems_cd       			VARCHAR   := '';   
	v_pcs_cd       			VARCHAR   := '';   
	v_bms_cd       			VARCHAR   := '';  
    v_update_cnt1			INTEGER   := 0;     						-- 적재수
    v_update_cnt2			INTEGER   := 0;     						-- 적재수
  
BEGIN
	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
  
		v_rslt_cd := 'A';

	    --RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] i_exe_base[%] START[%]', i_sp_nm, i_site_seq, i_exe_base, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS');

	   	FOR v_row IN ( 
			SELECT
				A.REQ_ID
				, A.DEVICE_ID
				, A.CREATE_DT
				, B.RESULT_CD
			FROM
				TB_PLF_DEVICE_FW_HIST A
				, TB_OPR_CTRL_RESULT B
			WHERE
				A.EMS_UPDATE_ERR_CD IS NULL
				AND A.PCS_UPDATE_ERR_CD IS NULL
				AND A.BMS_UPDATE_ERR_CD IS NULL
				AND A.REQ_ID = B.REQ_ID
				AND A.DEVICE_ID = B.DEVICE_ID
				AND B.REQ_TYPE_CD = 'M2M'
		) LOOP
			-- 푸시 업데이트 응답 처리
            IF v_row.RESULT_CD = '0' THEN 
                -- TB_RAW_LOGIN 테이블에서 대상 레코드보다 CREATE_DT 가 이후인 건들 중 가장 최근인 건을 가져온다.
                v_device_id := ''; 

				SELECT /*+ INDEX_DESC(A PK_RAW_LOGIN) */
							  EMS_UPDATE_ERR_CD
							, PCS_UPDATE_ERR_CD
							, BMS_UPDATE_ERR_CD
							, EMS_MODEL_NM
							, BMS_MODEL_NM
							, PCS_MODEL_NM
							, EMS_VER
							, BMS_VER
							, PCS_VER
							, EMS_UPDATE_DT
							, DEVICE_ID
				INTO 		  v_ems_update_err_cd
							, v_pcs_update_err_cd
							, v_bms_update_err_cd
							, v_ems_model_nm
							, v_bms_model_nm
							, v_pcs_model_nm
							, v_ems_ver
							, v_bms_ver
							, v_pcs_ver
							, v_ems_update_dt
							, v_device_id
				FROM		TB_RAW_LOGIN A
				WHERE	DEVICE_ID = v_row.DEVICE_ID
				AND 		RESPONSE_CD = '0'
				AND 		CREATE_DT > v_row.CREATE_DT
				ORDER BY CREATE_DT DESC
				LIMIT 1;
				
                --RAISE NOTICE 'i_sp_nm[%] select TB_RAW_LOGIN [%]건>>> REQ_ID{%] DEVICE_ID[%] EMS_UPDATE_ERR_CD[%] PCS_UPDATE_ERR_CD[%] BMS_UPDATE_ERR_CD[%]'
				--				, i_sp_nm, v_work_num, v_row.REQ_ID, v_row.DEVICE_ID, v_ems_update_err_cd, v_pcs_update_err_cd, v_bms_update_err_cd;
								
				-- 위 조회결과 레코드가 1건이 있으면 TB_PLF_DEVICE_FW_HIST 테이블의 해당 레코드에 업데이트 결과를 쓴다.
				-- 결과가 없으면 SKIP
				IF v_device_id != '' THEN
		            -- 	2015.07.22 yngwie          	CMD_REQ_STATUS_CD 에 진행상태코드 update
					UPDATE TB_PLF_DEVICE_FW_HIST
					SET EMS_UPDATE_ERR_CD = v_ems_update_err_cd
					    , PCS_UPDATE_ERR_CD = v_pcs_update_err_cd
					    , BMS_UPDATE_ERR_CD = v_bms_update_err_cd
					    , UPDATE_ID = ENC_VARCHAR_INS('SYSTEM', 11, 'SDIENCK', 'UPDATE_ID', 'TB_PLF_DEVICE_FW_HIST')
					    , UPDATE_DT = SYS_EXTRACT_UTC(NOW())
					    , CMD_REQ_STATUS_CD = '51'
					WHERE 	REQ_ID = v_row.REQ_ID
					    AND 	DEVICE_ID = v_row.DEVICE_ID;
					   
		           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
			       	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
			       	v_update_cnt1 := v_update_cnt1 + v_work_num;

	                --RAISE NOTICE 'i_sp_nm[%] update TB_PLF_DEVICE_FW_HIST [%]건>>> REQ_ID{%] DEVICE_ID[%] EMS_UPDATE_ERR_CD[%] PCS_UPDATE_ERR_CD[%] BMS_UPDATE_ERR_CD[%]'
					--				, i_sp_nm, v_work_num, v_row.REQ_ID, v_row.DEVICE_ID, v_ems_update_err_cd, v_pcs_update_err_cd, v_bms_update_err_cd;
	                
	                -- 장비 테이블에 모델정보와 버전을 업데이트한다.
					UPDATE TB_BAS_DEVICE_PMS
					SET EMS_MODEL_NM = v_ems_model_nm
					    , PCS_MODEL_NM = v_pcs_model_nm
					    , BMS_MODEL_NM = v_bms_model_nm
					    , EMS_VER = v_ems_ver
					    , PCS_VER = v_pcs_ver
					    , BMS_VER = v_bms_ver
						, EMS_UPDATE_DT = v_ems_update_dt
					    , UPDATE_ID = ENC_VARCHAR_INS('SYSTEM', 11, 'SDIENCK', 'UPDATE_ID', 'TB_BAS_DEVICE_PMS')
					    , UPDATE_DT = SYS_EXTRACT_UTC(NOW())						    
	                WHERE	DEVICE_ID = v_row.DEVICE_ID;
	               
		           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
			       	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
			       	v_update_cnt2 := v_update_cnt2 + v_work_num;

	                --RAISE NOTICE 'i_sp_nm[%] update TB_BAS_DEVICE_PMS [%]건>>> DEVICE_ID[%] EMS_MODEL_NM[%] PCS_MODEL_NM[%] BMS_MODEL_NM[%]'
					--				, i_sp_nm, v_work_num, v_row.DEVICE_ID, v_ems_model_nm, v_pcs_model_nm, v_bms_model_nm;

                    -- 명령 전송 이후 TB_RAW_LOGIN 테이블에 로그인한 결과가 없으면 SKIP
            	END IF;
            
            -- 명령 전송 결과가 실패이면
            -- 결과 코드를 TB_PLF_DEVICE_FW_HIST 에 기록한다.
            -- 	2015.07.22 yngwie,  	CMD_REQ_STATUS_CD 에 진행상태코드 update
            ELSE
                v_result := FN_GET_PUSH_UPDATE_RESULT( v_row.RESULT_CD );
                v_ems_cd := FN_GET_SPLIT(v_result, 1, '|');
                v_pcs_cd := FN_GET_SPLIT(v_result, 2, '|');
                v_bms_cd := FN_GET_SPLIT(v_result, 3, '|');
                
               --RAISE NOTICE 'i_sp_nm[%] Fail! REQ_ID[%] DEVICE_ID[%]', i_sp_nm, v_row.REQ_ID, v_row.DEVICE_ID;

				UPDATE TB_PLF_DEVICE_FW_HIST
				SET EMS_UPDATE_ERR_CD = v_ems_cd
				    , PCS_UPDATE_ERR_CD = v_pcs_cd
				    , BMS_UPDATE_ERR_CD = v_bms_cd
				    , UPDATE_ID = ENC_VARCHAR_INS('SYSTEM', 11, 'SDIENCK', 'UPDATE_ID', 'TB_PLF_DEVICE_FW_HIST')
				    , UPDATE_DT = SYS_EXTRACT_UTC(NOW())
				    , CMD_REQ_STATUS_CD = '55'
				WHERE 	REQ_ID = v_row.REQ_ID
				    AND 	DEVICE_ID = v_row.DEVICE_ID;				   
				   
	           	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
		       	v_rslt_cnt := v_rslt_cnt + v_work_num;	       
		       	v_update_cnt1 := v_update_cnt1 + v_work_num;
		       
            END IF;
           
	   END LOOP;
		
       --RAISE NOTICE 'i_sp_nm[%] Update TB_PLF_DEVICE_FW_HIST[%]건, TB_BAS_DEVICE_PMS[%]건', i_sp_nm, v_update_cnt1, v_update_cnt2;
            
        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE NOTICE 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
    
                           
   	return v_rslt_cd;

END;
$function$
;

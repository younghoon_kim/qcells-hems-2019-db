create table tb_bas_device_battery_hist
(
    site_id          varchar     not null,
    id               integer     not null,
    device_id        varchar,
    product_model_nm varchar,
    device_type_cd   varchar, --ex) PMS, Battery 등등
    capacity         varchar(64) not null,
    prn_device_id    varchar, --상위 디바이스 S/N
    module           jsonb,
    create_dt        timestamp with time zone
);

create index tb_bas_device_battery_hist_product_model_nm_index
    on tb_bas_device_battery_hist (product_model_nm);

create index tb_bas_device_battery_hist_device_id_index
    on tb_bas_device_battery_hist (device_id);

create index tb_bas_device_battery_hist_site_id_index
    on tb_bas_device_battery_hist (site_id);

comment on table tb_bas_device_battery_hist is '베터리 정보의 변경사항을 이력을 저장';
comment on column tb_bas_device_battery_hist.site_id is 'site_id';
comment on column tb_bas_device_battery_hist.id is 'battery_id';
comment on column tb_bas_device_battery_hist.device_id is '장비ID';
comment on column tb_bas_device_battery_hist.product_model_nm is '제품모델명';
comment on column tb_bas_device_battery_hist.device_type_cd is '장비유형코드';
comment on column tb_bas_device_battery_hist.capacity is '용량';
comment on column tb_bas_device_battery_hist.prn_device_id is 'prn_device_id';
comment on column tb_bas_device_battery_hist.module is 'module정보,id,serial number 등';
comment on column tb_bas_device_battery_hist.create_dt is '변경일시';



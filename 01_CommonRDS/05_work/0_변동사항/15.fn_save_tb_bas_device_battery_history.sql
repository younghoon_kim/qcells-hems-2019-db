create or replace function fn_save_tb_bas_device_battery_history() returns trigger AS
$save_tb_bas_device_battery_history$
declare
    count int;
begin
    -- 베터리 정보 변경이력을 관리
    -- tb_bas_device_battery에 update 또는 insert시 tb_bas_device_battery_hist에 insert됨
    -- todo: implement
    if (TG_OP = 'INSERT') then
        insert into tb_bas_device_battery_hist(site_id, id, device_id, product_model_nm, module, device_type_cd,
                                               capacity, prn_device_id, create_dt)
        values (new.site_id, new.id, new.device_id, new.product_model_nm, new.module, new.device_type_cd, new.capacity,
                new.prn_device_id, new.create_dt);
        raise notice 'Insert : % : % : % : % : % : % : %: % :%'
            ,new.site_id, new.id, new.device_id, new.product_model_nm, new.module, new.device_type_cd,new.capacity,new.prn_device_id, new.create_dt;
        return new;
    elsif (TG_OP = 'UPDATE') then
        insert into tb_bas_device_battery_hist(site_id, id, device_id, product_model_nm, module, device_type_cd,
                                               capacity, prn_device_id, create_dt)
        values (new.site_id, new.id, new.device_id, new.product_model_nm, new.module, new.device_type_cd, new.capacity,
                new.prn_device_id, new.create_dt);
        raise notice 'Insert : % : % : % : % : % : % : %: % :%'
            ,new.site_id, new.id, new.device_id, new.product_model_nm, new.module, new.device_type_cd,new.capacity,new.prn_device_id, new.create_dt;
        return new;
    end if;
    return null;
end;
$save_tb_bas_device_battery_history$ language plpgsql;


-- trigger 생성
create trigger save_tb_bas_battery_product_history
    after insert or update
    on tb_bas_device_battery
    for each row
execute procedure fn_save_tb_bas_device_battery_history();
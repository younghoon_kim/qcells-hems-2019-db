COMMENT ON COLUMN tb_plf_tbl_meta.del_flag IS '일정 기간 후 삭제/유지/보관 여부를 Y/M/N로 나타낸다.Y=데이터삭제&파티션삭제 M=데이터삭제&파티션남김 N=데이터남김';

UPDATE tb_plf_tbl_meta SET del_flag='M' WHERE table_nm='TB_OPR_ALARM_HIST' AND sub_area='OPR';
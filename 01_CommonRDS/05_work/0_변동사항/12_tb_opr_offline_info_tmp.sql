-- auto-generated definition
create table tb_opr_offline_info_tmp
(
    device_id varchar(50) not null,
    start_tm  varchar(14) not null,
    end_tm    varchar(14),
    type      varchar(4),
    constraint tb_opr_offline_info_tmp_pk
        primary key (device_id, start_tm)
);

comment on table tb_opr_offline_info_tmp is '네트워크 단절 정보 임시 저장 테이블';

alter table tb_opr_offline_info_tmp
    owner to wems;


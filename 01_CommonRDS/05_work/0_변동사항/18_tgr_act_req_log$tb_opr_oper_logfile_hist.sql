--trigger 생성
--tb_opr_oper_logfile_hist 에서 생성하는 트리거
--1종류 tgr_act_req_log$tb_opr_oper_logfile_hist

CREATE OR REPLACE FUNCTION "tgr_act_req_log$tb_opr_oper_logfile_hist"() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE

BEGIN
    --  M2M 에서 ESS 으로 요청이 처리 된 후에 기록한다.
    IF new.cmd_req_status_cd IN ('51', '55') THEN
        INSERT INTO tb_opr_act (act_seq, act_type_cd, device_id, device_type_cd, ref_seq, ref_type_cd, ref_desc, ref_user_id, create_dt)
        VALUES (nextval('sq_opr_act'), '50', new.device_id, new.device_type_cd, new.req_id, new.logfile_type_cd, new.logfile_nm
        			, (select instl_user_id FROM tb_bas_device_pms WHERE device_id = new.device_id)
        			, new.create_dt);
    END IF;

    RETURN NULL;

    EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END;
$$;

-- Drop table
DROP TABLE IF EXISTS dc_cnf_ext_svc;

-- Create table
create table dc_cnf_ext_svc
(
	svc_seq bigserial not null
		constraint dc_cnf_ext_svc_pk
			primary key,
	svc_nm varchar(20) not null,
	svc_desc varchar(1024),
	svc_ip varchar(20) not null,
	svc_port integer default 22,
	svc_base_path varchar(200),
	svc_file_nm varchar(60) not null,
	svc_start_prog varchar(60) not null,
	svc_stop_prog varchar(60) not null,
	svc_restart_prog varchar(60) not null,
	wrk_mng_id varchar(40),
	acct_id varchar(60),
	acct_pw varchar(150),
	watch_tgt_yn varchar(1) default 'Y'::character varying,
	svc_state integer default 1,
	evt_type_cd varchar(5) not null,
	auto_restart_yn varchar(1) default 'Y'::character varying not null,
	use_yn varchar(1) default 'Y'::character varying not null,
	reg_dt timestamp with time zone default sys_extract_utc(now()) not null,
	lst_dt timestamp with time zone default sys_extract_utc(now()) not null
);

COMMENT ON TABLE dc_cnf_ext_svc IS '관리 대상이 되는 서비스 프로세스 정보';
COMMENT ON COLUMN dc_cnf_ext_svc.svc_seq IS '일련번호';
COMMENT ON COLUMN dc_cnf_ext_svc.svc_nm IS '서비스 명';
COMMENT ON COLUMN dc_cnf_ext_svc.svc_desc IS '서비스 설명';
COMMENT ON COLUMN dc_cnf_ext_svc.svc_ip IS '서비스 아이피';
COMMENT ON COLUMN dc_cnf_ext_svc.svc_port IS '서비스 포트';
COMMENT ON COLUMN dc_cnf_ext_svc.svc_base_path IS '서비스 기본 경로';
COMMENT ON COLUMN dc_cnf_ext_svc.svc_file_nm IS '서비스 파일 명';
COMMENT ON COLUMN dc_cnf_ext_svc.svc_start_prog IS '서비스 실행 프로그램(shell)';
COMMENT ON COLUMN dc_cnf_ext_svc.svc_stop_prog IS '서비스 중지 프로그램(shell)';
COMMENT ON COLUMN dc_cnf_ext_svc.svc_restart_prog IS '서비스 재실행 프로그램(shell)';
COMMENT ON COLUMN dc_cnf_ext_svc.wrk_mng_id IS 'worker gateway id (dc_cnf_manager 의 id
worker 체크시 사용하기 위함)';
COMMENT ON COLUMN dc_cnf_ext_svc.acct_id IS '계정 아이디';
COMMENT ON COLUMN dc_cnf_ext_svc.acct_pw IS '계정 패스워드';
COMMENT ON COLUMN dc_cnf_ext_svc.watch_tgt_yn IS '감시 대상 여부';
COMMENT ON COLUMN dc_cnf_ext_svc.svc_state IS '서비스 상태 (1:정상 2:비정상)';
COMMENT ON COLUMN dc_cnf_ext_svc.evt_type_cd IS '이벤트 타입 코드';
COMMENT ON COLUMN dc_cnf_ext_svc.auto_restart_yn IS '자동 재시작 여부';
COMMENT ON COLUMN dc_cnf_ext_svc.use_yn IS '사용유무';
COMMENT ON COLUMN dc_cnf_ext_svc.reg_dt IS '등록일시';
COMMENT ON COLUMN dc_cnf_ext_svc.lst_dt IS '최종일시';
alter table dc_cnf_ext_svc owner to wems;

-- sequence 생성
-- create sequence dc_cnf_ext_svc_svc_seq_seq;

-- data
INSERT INTO wems.dc_cnf_ext_svc (svc_nm, svc_desc, svc_ip, svc_port, svc_base_path, svc_file_nm, svc_start_prog, svc_stop_prog, svc_restart_prog, wrk_mng_id, acct_id, acct_pw, watch_tgt_yn, svc_state, evt_type_cd, auto_restart_yn, use_yn, reg_dt, lst_dt) VALUES ('daqworker1', '', '11.0.30.102', 22, '/home/bems/daq', 'daqworker.jar', 'daqworkerMg-start.sh', 'daqworkerMg-stop.sh', 'daqworkerMg-restart.sh', 'WKM1', 'bems', 'f3c135d8b192173f91899e2e115101a8', 'Y', 1, 'ETWKR', 'Y', 'Y', '2020-06-28 20:32:43.917575', '2020-08-12 07:57:03.808126');
INSERT INTO wems.dc_cnf_ext_svc (svc_nm, svc_desc, svc_ip, svc_port, svc_base_path, svc_file_nm, svc_start_prog, svc_stop_prog, svc_restart_prog, wrk_mng_id, acct_id, acct_pw, watch_tgt_yn, svc_state, evt_type_cd, auto_restart_yn, use_yn, reg_dt, lst_dt) VALUES ('daqetl1', null, '11.1.27.230', 22, '/home/bems/daq', 'daqetl.jar', 'daqetl-start.sh', 'daqetl-stop.sh', 'daqetl-restart.sh', null, 'bems', 'f3c135d8b192173f91899e2e115101a8', 'Y', 1, 'ETETL', 'Y', 'Y', '2020-06-28 20:32:43.917575', '2020-08-12 07:57:05.293156');
INSERT INTO wems.dc_cnf_ext_svc (svc_nm, svc_desc, svc_ip, svc_port, svc_base_path, svc_file_nm, svc_start_prog, svc_stop_prog, svc_restart_prog, wrk_mng_id, acct_id, acct_pw, watch_tgt_yn, svc_state, evt_type_cd, auto_restart_yn, use_yn, reg_dt, lst_dt) VALUES ('daqdbs1', null, '11.1.27.230', 22, '/home/bems/daq', 'daqdbs.jar', 'daqdbs-start.sh', 'daqdbs-stop.sh', 'daqdbs-restart.sh', null, 'bems', 'f3c135d8b192173f91899e2e115101a8', 'Y', 1, 'ETDBS', 'Y', 'Y', '2020-06-28 20:32:43.917575', '2020-08-12 07:57:06.757837');
INSERT INTO wems.dc_cnf_ext_svc (svc_nm, svc_desc, svc_ip, svc_port, svc_base_path, svc_file_nm, svc_start_prog, svc_stop_prog, svc_restart_prog, wrk_mng_id, acct_id, acct_pw, watch_tgt_yn, svc_state, evt_type_cd, auto_restart_yn, use_yn, reg_dt, lst_dt) VALUES ('daqmaster1', null, '11.1.27.230', 22, '/home/bems/daq', 'daqmaster.jar', 'daqmaster-start.sh', 'daqmaster-stop.sh', 'daqmaster-restart.sh', null, 'bems', 'f3c135d8b192173f91899e2e115101a8', 'Y', 1, 'ETMSR', 'Y', 'Y', '2020-06-28 20:32:43.917575', '2020-08-12 07:57:08.217423');
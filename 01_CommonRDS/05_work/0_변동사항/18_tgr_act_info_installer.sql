--trigger 생성
--tb_bas_device 에서 생성하는 트리거 2종류
-- tgr_act_instl , tgr_device_warranty
--tgr_act_instl를 대체 함.
-- tgr_device_warranty는 워런티 날짜를 갱신하는 것으로 보이며 필요하지 않다고 판단되어 구현하지 않음.

-- Table Triggers

--DROP TRIGGER tgr_act_info_installer ON tb_bas_device_pms;
create or replace function tgr_act_info_installer() returns trigger AS
$tgr_act_info_installer$
declare
begin
    -- todo: implement
    INSERT INTO wems.tb_opr_act ( act_seq, act_type_cd, device_id, device_type_cd
        -- , REF_DESC
                                , ref_user_id, create_dt)
    VALUES ( nextval('sq_opr_act'), '30', new.device_id, new.device_type_cd
               -- , :NEW.INSTL_DT
           , new.instl_user_id, new.create_dt);

    RETURN NULL;

EXCEPTION
    WHEN others THEN
        RETURN NULL;
end;
$tgr_act_info_installer$ language plpgsql;

--trigger 생성
create trigger tgr_act_info_installer
    after insert
    on tb_bas_device_pms for each row
execute procedure tgr_act_info_installer();



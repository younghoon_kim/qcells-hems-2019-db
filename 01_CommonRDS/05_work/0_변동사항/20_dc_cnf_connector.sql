-- Drop table
DROP TABLE IF EXISTS dc_cnf_connector;

-- Create table
create table dc_cnf_connector
(
	id varchar(80) not null
		constraint dc_cnf_connector_pk
			primary key,
	gatewayid varchar(40) not null,
	status numeric(1),
	description varchar(500),
	modify_date timestamp(6) with time zone,
	last_action_date timestamp(6) with time zone,
	last_action varchar(50),
	state_fail_cnt integer default 0,
	use_yn varchar(1) default 'Y'::character varying,
	stop_posbl_yn varchar(1) default 'Y'::character varying,
	port numeric(8) default 0,
	prtcl_type varchar(60)
);

COMMENT ON TABLE DC_CNF_CONNECTOR IS 'Worker(Connector)의 상세 정보';
COMMENT ON COLUMN DC_CNF_CONNECTOR.id IS 'worker 아이디';
COMMENT ON COLUMN DC_CNF_CONNECTOR.gatewayid IS 'worker Manager 아이디';
COMMENT ON COLUMN DC_CNF_CONNECTOR.status IS '사용여부 1:사용 2:미사용';
COMMENT ON COLUMN DC_CNF_CONNECTOR.description IS '설명';
COMMENT ON COLUMN DC_CNF_CONNECTOR.modify_date IS '수정일시';
COMMENT ON COLUMN DC_CNF_CONNECTOR.last_action_date IS 'worker 최종 기동 일시';
COMMENT ON COLUMN DC_CNF_CONNECTOR.last_action IS '최종 작업';
COMMENT ON COLUMN DC_CNF_CONNECTOR.state_fail_cnt IS '실패 건수';
COMMENT ON COLUMN DC_CNF_CONNECTOR.use_yn IS '사용여부 ( 1:사용 2:미사용 )';
COMMENT ON COLUMN DC_CNF_CONNECTOR.stop_posbl_yn IS '중지 가능 여부';
COMMENT ON COLUMN DC_CNF_CONNECTOR.port IS '사용포트';
COMMENT ON COLUMN DC_CNF_CONNECTOR.prtcl_type IS '사용 프로토콜 타입';

alter table dc_cnf_connector owner to wems;


-- data
INSERT INTO wems.dc_cnf_connector (id, gatewayid, status, description, modify_date, last_action_date, last_action, state_fail_cnt, use_yn, stop_posbl_yn, port, prtcl_type) VALUES ('ESS_XML1', 'WKM1', 1, null, '2020-08-13 03:09:32.429804', '2020-08-13 02:24:04.859979', 'START', 0, 'Y', 'Y', 12001, 'TCP');
INSERT INTO wems.dc_cnf_connector (id, gatewayid, status, description, modify_date, last_action_date, last_action, state_fail_cnt, use_yn, stop_posbl_yn, port, prtcl_type) VALUES ('ESS_XML4', 'WKM1', 1, null, '2020-07-14 07:15:52.041757', '2020-07-31 07:33:25.465241', 'START', 0, 'N', 'Y', 12004, 'TCP');
INSERT INTO wems.dc_cnf_connector (id, gatewayid, status, description, modify_date, last_action_date, last_action, state_fail_cnt, use_yn, stop_posbl_yn, port, prtcl_type) VALUES ('ESS_XML3', 'WKM1', 1, null, '2020-07-14 07:15:48.079504', '2020-08-25 06:36:14.695500', 'START', 0, 'N', 'Y', 12003, 'TCP');
INSERT INTO wems.dc_cnf_connector (id, gatewayid, status, description, modify_date, last_action_date, last_action, state_fail_cnt, use_yn, stop_posbl_yn, port, prtcl_type) VALUES ('ESS_XML2', 'WKM1', 1, null, '2020-07-14 07:15:44.106897', '2020-08-24 05:48:57.306030', 'START', 0, 'Y', 'Y', 12002, 'TCP');
INSERT INTO wems.dc_cnf_connector (id, gatewayid, status, description, modify_date, last_action_date, last_action, state_fail_cnt, use_yn, stop_posbl_yn, port, prtcl_type) VALUES ('ESS_XML21', 'WKM2', 2, null, '2020-06-29 08:02:39.403340', '2020-06-11 06:13:45.340015', 'STOP', 0, 'N', 'Y', 12001, 'TCP');
INSERT INTO wems.dc_cnf_connector (id, gatewayid, status, description, modify_date, last_action_date, last_action, state_fail_cnt, use_yn, stop_posbl_yn, port, prtcl_type) VALUES ('ESS_XML22', 'WKM2', 2, null, '2020-06-29 08:02:43.124247', '2020-06-11 06:13:44.944302', 'STOP', 0, 'N', 'Y', 12002, 'TCP');
INSERT INTO wems.dc_cnf_connector (id, gatewayid, status, description, modify_date, last_action_date, last_action, state_fail_cnt, use_yn, stop_posbl_yn, port, prtcl_type) VALUES ('ESS_XML23', 'WKM2', 2, null, '2020-06-29 08:02:46.838631', '2020-06-11 06:13:44.708467', 'STOP', 0, 'N', 'Y', 12003, 'TCP');
INSERT INTO wems.dc_cnf_connector (id, gatewayid, status, description, modify_date, last_action_date, last_action, state_fail_cnt, use_yn, stop_posbl_yn, port, prtcl_type) VALUES ('ESS_XML24', 'WKM2', 2, null, '2020-06-29 08:02:50.558995', '2020-06-11 06:13:46.711503', 'STOP', 0, 'N', 'Y', 12004, 'TCP');
create table tb_test_deviceinfo_gens3_temp
(
    uid                                varchar,
    pvinfo_pvtype                      integer,
    pvinfo_stringinverter              integer,
    pvinfo_microinverter               integer,
    pvinfo_poweroptimizers             integer,
    batteryinfo_rackcount              integer,
    batteryinfo_rackid                 varchar,
    batteryinfo_modelname              varchar,
    batteryinfo_serialnumber           varchar,
    batteryinfo_capacity               integer,
    batteryinfo_module                 JSONB,
    meterinfo_modelname                varchar,
    meterinfo_connectiontype           integer,
    externalcontrolinfo_modelname      varchar,
    externalcontrolinfo_connectiontype integer,
    evchargeinfo                       varchar,
    heatpumpinfo                       varchar,
    create_dt                          timestamptz default sys_extract_utc(now()) not null
);

comment on column tb_test_deviceinfo_gens3_temp.uid is 'EMS의 Serial Number';
comment on column tb_test_deviceinfo_gens3_temp.pvinfo_pvtype is '0: String Inverter
1: Micro Inverter
2: Power Optimizers';
comment on column tb_test_deviceinfo_gens3_temp.pvinfo_stringinverter is '현재 올라갈 필요 없는 값이며 Protocol만 유지';
comment on column tb_test_deviceinfo_gens3_temp.pvinfo_microinverter is '현재 올라갈 필요 없는 값이며 Protocol만 유지';
comment on column tb_test_deviceinfo_gens3_temp.pvinfo_poweroptimizers is '현재 올라갈 필요 없는 값이며 Protocol만 유지';
comment on column tb_test_deviceinfo_gens3_temp.batteryinfo_rackcount is 'Rack 개수 정보 (구동되는 Rack 기준)';
comment on column tb_test_deviceinfo_gens3_temp.batteryinfo_rackid is 'Battery Rack에 대한 ID (1~3과 같은 일련의 숫자, 값은 정해지지 않음)';
comment on column tb_test_deviceinfo_gens3_temp.batteryinfo_modelname is '33J/41J/50E';
comment on column tb_test_deviceinfo_gens3_temp.batteryinfo_serialnumber is 'Q.SAVE Battery Rack에 대한 Serial Number';
comment on column tb_test_deviceinfo_gens3_temp.batteryinfo_capacity is 'Battery Rack의 용량';
comment on column tb_test_deviceinfo_gens3_temp.batteryinfo_module is '1개의 Rack안에 최대 5개까지의 Module이 들어있음';
comment on column tb_test_deviceinfo_gens3_temp.meterinfo_modelname is 'Meter Model Name
- EM112
- EM24
- 기타 model name';
comment on column tb_test_deviceinfo_gens3_temp.meterinfo_connectiontype is '0: None
1: Modbus TCP
2: Modbus RTU';
comment on column tb_test_deviceinfo_gens3_temp.externalcontrolinfo_modelname is '3rd party Gateway 관련 정보';
comment on column tb_test_deviceinfo_gens3_temp.externalcontrolinfo_connectiontype is 'Gateway Model Name
- Energeybase XX
- SwichDin XX
- 기타 model name';
comment on column tb_test_deviceinfo_gens3_temp.evchargeinfo is '0: None
1: Modbus TCP
2: Modbus RTU';
comment on column tb_test_deviceinfo_gens3_temp.heatpumpinfo is 'EV Charger관련 정보 (추후 더 많은 정보 추가 예정)';
comment on column tb_test_deviceinfo_gens3_temp.create_dt is 'HeatPump관련 정보 (추후 더 많은 정보 추가 예정)';
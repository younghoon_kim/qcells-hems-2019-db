-- Drop table
DROP TABLE IF EXISTS dc_cnf_code;

-- Create table
create table dc_cnf_code
(
	cd_seq serial not null
		constraint dc_cnf_code_pkey
			primary key,
	cd_grp varchar(4) not null,
	grp_flag boolean default false not null,
	cd_val varchar(30) not null,
	cd_nm varchar(256) not null,
	cd_desc varchar(256),
	cd_ord numeric(2) default 0 not null,
	use_yn varchar(1) default 'Y'::character varying not null,
	lst_user varchar(60) default 'system'::character varying not null,
	lst_dt timestamp with time zone default sys_extract_utc(now()) not null,
	ref_1 varchar(256),
	ref_2 varchar(256),
	ref_3 varchar(256),
	ref_4 varchar(256),
	ref_5 varchar(256),
	ref_6 varchar(256),
	ref_7 varchar(256),
	ref_8 varchar(256),
	ref_9 varchar(256),
	constraint dc_cnf_code_cd_grp_grp_flag_cd_val_uindex
		unique (cd_grp, grp_flag, cd_val)
);

comment on table dc_cnf_code is 'DAQ 코드정보';

comment on column dc_cnf_code.cd_seq is '코드 일련번호';

comment on column dc_cnf_code.cd_grp is '코드 그룹';

comment on column dc_cnf_code.grp_flag is '코드가 그룹인지 여부 false:상세데이터 true:그룹';

comment on column dc_cnf_code.cd_val is '코드값';

comment on column dc_cnf_code.cd_nm is '코드명';

comment on column dc_cnf_code.cd_desc is '코드설명';

comment on column dc_cnf_code.cd_ord is '코드 순서';

comment on column dc_cnf_code.use_yn is '사용유무';

comment on column dc_cnf_code.lst_user is '최종사용자';

comment on column dc_cnf_code.lst_dt is '최종일시';

alter table dc_cnf_code owner to wems;

-- sequence 생성
-- create sequence dc_cnf_code_cd_seq_seq
--     as integer;


-- Insert data
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('1101', true, 'master status 코드 정의', 'master status 코드 정의', 'dc_cnf_server.status 코드 정의', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('1101', false, '1', 'alive', 'master 상태가 정상', 1, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('1101', false, '2', 'fail', 'master 상태가 비정상', 2, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('1201', true, 'worker status 코드 정의', 'worker status 코드 정의', 'dc_cnf_connector.status 코드 정의', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('1201', false, '1', 'alive', 'worker 상태가 정상', 1, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('1201', false, '2', 'fail', 'worker 상태가 비정상', 2, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('1202', true, 'worker last_action 정의', 'worker last_action 정의', 'dc_cnf_connector.last_action 정의', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('1202', false, 'START', 'alive', 'worker 동작중인 상태(string)', 1, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('1202', false, 'STOP', 'fail', 'worker 중지된 상태(string)', 2, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4001', true, 'DAQ Event Type Code', 'DAQ Event Type Code', 'DAQ 의 이벤트 타입코드를 정의', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4001', false, 'ETMSR', 'MASTER', 'master에서 발생하는 이벤트', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4001', false, 'ETWKR', 'WORKER', 'worker에서 발생하는 이벤트', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4001', false, 'ETAPI', 'API', 'api에서 발생하는 이벤트', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4001', false, 'ETDBS', 'WORKER DBS', 'worker dbs에서 발생하는 이벤트', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4001', false, 'ETETL', 'WORKER ETL', 'worker etl에서 발생하는 이벤트', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4010', true, 'DAQ Event nofi flag', 'DAQ Event nofi flag', 'dc_cnf_evt.nofi_flag값 정의 ', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4010', false, '0', '이벤트 알림 미전송', '이벤트 알림 미전송(이벤트가 등록된 상태)', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4010', false, '90', '이벤트 알림 전송', '이벤트 알림 전송 완료', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4010', false, '10', '이벤트 알림 실패', '이벤트 알림 전송 실패', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4011', true, 'DAQ Event nofi code', 'DAQ Event nofi code 및 message', 'dc_cnf_evt.cd_err_cd, cd_err_msg 정의 ', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4011', false, 'E0000', 'success', '이벤트 알림 전송 성공', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4011', false, 'E1000', 'send fail', '이벤트 알림 전송 실패', 0, 'Y', 'system', '2020-05-27 07:41:38.341066', null, null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4000', true, 'DAQ Event Code', 'DAQ Event Code', 'DAQ 의 이벤트 코드를 정의', 0, 'Y', 'system', '2020-05-27 07:41:41.445328', 'send only once (Y,N)', null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4000', false, 'WE100', 'ESS 핸드쉐이크 오류', 'worker와 ESS장비간 핸드쉐이크 오류', 0, 'Y', 'system', '2020-05-27 07:41:41.445328', 'N', null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4000', false, 'WE400', '장비 데이터 미수신', 'Worker에서 일정시간동안 데이터 미수신 상태인 경우 발생', 0, 'Y', 'system', '2020-05-27 07:41:41.445328', 'N', null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4000', false, 'WE410', '장비에서 데이터는 수신되나 kafka로 전송이 안될시', '장비에서 데이터는 수신되나 kafka로 전송이 안될시', 0, 'Y', 'system', '2020-05-27 07:41:41.445328', 'N', null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ('4000', false, 'WE510', 'file 생성 오류 발생', 'daqetl 에서 file 생성시 오류 발생', 0, 'Y', 'system', '2020-05-27 07:41:41.445328', 'N', null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ( '4000', false, 'WE550', 'DB 적재시 오류 발생(일반)', 'daqdbs 에서 DB 적재시 일반적인 오류 발생', 0, 'Y', 'system', '2020-05-27 07:41:41.445328', 'N', null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ( '4000', false, 'WE551', 'DB 적재시 중복 오류(duplicate)', 'daqdbs 에서 DB 적재시 동일한 키값이 있어 오류 발생', 0, 'Y', 'system', '2020-05-27 07:41:41.445328', 'N', null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ( '4000', false, 'HC100', 'health check 응답없음', 'health check 응답 없음', 0, 'Y', 'system', '2020-05-27 07:41:41.445328', 'N', null, null, null, null, null, null, null, null);
INSERT INTO wems.dc_cnf_code (cd_grp, grp_flag, cd_val, cd_nm, cd_desc, cd_ord, use_yn, lst_user, lst_dt, ref_1, ref_2, ref_3, ref_4, ref_5, ref_6, ref_7, ref_8, ref_9) VALUES ( '4000', false, 'PC100', 'process check 응답없음', 'process check 응답 없음', 0, 'Y', 'system', '2020-05-27 07:41:41.445328', 'Y', null, null, null, null, null, null, null, null);
;
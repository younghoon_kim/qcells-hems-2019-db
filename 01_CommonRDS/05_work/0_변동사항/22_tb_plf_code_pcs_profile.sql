-- auto-generated definition
create table tb_plf_code_pcs_profile
(
    base_profile_name varchar(255),
    base_profile_cd   integer not null
        constraint tb_plf_code_pcs_profile_pk
            primary key
);

comment on table tb_plf_code_pcs_profile is 'base_profile의 code와 인증이름을 mapping';

comment on column tb_plf_code_pcs_profile.base_profile_name is 'base_profile의 인증 이름을 의미';

comment on column tb_plf_code_pcs_profile.base_profile_cd is 'base_profile_code, 프로토콜문서에서 county_code를 의미함.';

alter table tb_plf_code_pcs_profile
    owner to wems;


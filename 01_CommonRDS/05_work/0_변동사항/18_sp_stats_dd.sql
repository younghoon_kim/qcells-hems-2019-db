CREATE OR REPLACE FUNCTION sp_stats_dd(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       SP_STATS_DD
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-26   yngwie  Created this function.
   2.0        2019-09-20   박동환   Postgresql 변환
   2.1        2019-11-27   이재형   성능개선을 위한 쿼리 변환
   2.2        2020-07-24   서정우   1. site_id추가로 인해 tb_bas_device -> tb_bas_device_pms 를 사용하도록 수정
                                    2. sp_rank_daily 주석 처리

   NOTES:   시간별 통계 데이터를 가지고 UTC_OFFSET 별로 일 통계 데이터를 생성
               1시간 주기 누적. (SP_STATS_TM에서 호출됨)

            Source Table : TB_STT_ESS_TM
            Target Table : TB_STT_ESS_DD

    Input : i_exe_base 집계 대상 년월일(YYYYMMDD)

	Output :

	Usage : SELECT SP_STATS_DD('SP_STATS_DD', 0, '20191025', 'M');

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_from			VARCHAR   := NULL; 
    v_to				VARCHAR   := NULL; 
    v_tm              VARCHAR   := NULL; 
    v_row           	record; 
  	v_sql 				VARCHAR  := NULL; 
   
BEGIN
	i_exe_base := SUBSTR(i_exe_base, 1, 8);

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		v_rslt_cd := 'A';
	
--	    RAISE NOTICE 'i_sp_nm[%] exe_base[%]', i_sp_nm, i_exe_base;

    	FOR v_row IN ( 
		    SELECT  UTC_OFFSET  
		    FROM (  
		    	SELECT	0 AS UTC_OFFSET  
		    	
		    	UNION ALL    
		    	SELECT  UTC_OFFSET  
		    	FROM   TB_BAS_CITY  
		    	WHERE  UTC_OFFSET IS NOT NULL  
		    	
		    	UNION ALL  
		    	SELECT  UTC_OFFSET + 1 
		    	FROM  TB_BAS_CITY  
		    	WHERE UTC_OFFSET IS NOT NULL  
		    )  T
		    GROUP BY UTC_OFFSET 

		) LOOP

	        -- 추가할 데이터의 시작, 종료일시를 구한다.
			v_tm 		:= FN_TS_STR_TO_STR( i_exe_base||'12', v_row.utc_offset, 'YYYYMMDDHH24', 0, 'YYYYMMDDHH24');	
	        v_from 	:= FN_TS_STR_TO_STR( i_exe_base||'00', v_row.utc_offset, 'YYYYMMDDHH24', 0, 'YYYYMMDDHH24');
	        v_to 		:= FN_TS_STR_TO_STR( i_exe_base||'23', v_row.utc_offset, 'YYYYMMDDHH24', 0, 'YYYYMMDDHH24');
       
    		-- 시간별 통계 데이터를 SUM 해서 일별 통계 테이블에 Merge
    		v_sql := FORMAT('
    		WITH AR AS ( 
					SELECT   DEVICE_ID, DEVICE_TYPE_CD
							, OUTLET_PW  
							, PV_V1, PV_I1, PV_PW1, PV_V2	, PV_I2, PV_PW2  
							, INVERTER_V, INVERTER_I, INVERTER_PW  
							, DC_LINK_V
							, G_RLY_CNT	, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT  
							, RACK_V, RACK_I  
							, CELL_MAX_V, CELL_MIN_V, CELL_AVG_V	, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T 
							, PV_PW, CONS_PW, GRID_PW, PCS_PW, PCS_TGT_PW 
							, BT_SOH	, BT_SOC , BT_PW
					FROM (  
						SELECT	  A.*  
									, ROW_NUMBER() OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM) AS RNUM  
						FROM 	TB_STT_ESS_TM A 
						WHERE	COLEC_TM BETWEEN ''%s'' AND ''%s'' 
					)  Z
					WHERE RNUM = 1  
			), A AS (
				SELECT  A.DEVICE_ID    
						, A.DEVICE_TYPE_CD
						, SUM(A.PV_PW_H) AS PV_PW_H     
						, TRUNC(SUM(A.PV_PW_PRICE), 2) AS PV_PW_PRICE     
						, TRUNC(SUM(A.PV_PW_CO2), 2) AS PV_PW_CO2  
						, SUM(A.CONS_PW_H) AS CONS_PW_H   
						, TRUNC(SUM(A.CONS_PW_PRICE), 2) AS CONS_PW_PRICE   
						, TRUNC(SUM(A.CONS_PW_CO2), 2) AS CONS_PW_CO2     
						, SUM(A.CONS_GRID_PW_H) AS CONS_GRID_PW_H  
						, TRUNC(SUM(A.CONS_GRID_PW_PRICE), 2) AS CONS_GRID_PW_PRICE     
						, TRUNC(SUM(A.CONS_GRID_PW_CO2), 2) AS CONS_GRID_PW_CO2   
						, SUM(A.CONS_PV_PW_H) AS CONS_PV_PW_H    
						, TRUNC(SUM(A.CONS_PV_PW_PRICE), 2) AS CONS_PV_PW_PRICE   
						, TRUNC(SUM(A.CONS_PV_PW_CO2), 2) AS CONS_PV_PW_CO2  
						, SUM(A.CONS_BT_PW_H) AS CONS_BT_PW_H    
						, TRUNC(SUM(A.CONS_BT_PW_PRICE), 2) AS CONS_BT_PW_PRICE   
						, TRUNC(SUM(A.CONS_BT_PW_CO2), 2) AS CONS_BT_PW_CO2  
						, SUM(A.BT_CHRG_PW_H) AS BT_CHRG_PW_H    
						, TRUNC(SUM(A.BT_CHRG_PW_PRICE), 2) AS BT_CHRG_PW_PRICE   
						, TRUNC(SUM(A.BT_CHRG_PW_CO2), 2) AS BT_CHRG_PW_CO2  
						, SUM(A.BT_DCHRG_PW_H) AS BT_DCHRG_PW_H   
						, TRUNC(SUM(A.BT_DCHRG_PW_PRICE), 2) AS BT_DCHRG_PW_PRICE  
						, TRUNC(SUM(A.BT_DCHRG_PW_CO2), 2) AS BT_DCHRG_PW_CO2  
						, SUM(A.GRID_OB_PW_H) AS GRID_OB_PW_H    
						, TRUNC(SUM(A.GRID_OB_PW_PRICE), 2) AS GRID_OB_PW_PRICE   
						, TRUNC(SUM(A.GRID_OB_PW_CO2), 2) AS GRID_OB_PW_CO2  
						, SUM(A.GRID_TR_PW_H) AS GRID_TR_PW_H    
						, TRUNC(SUM(A.GRID_TR_PW_PRICE), 2) AS GRID_TR_PW_PRICE   
						, TRUNC(SUM(A.GRID_TR_PW_CO2), 2) AS GRID_TR_PW_CO2  
						, SUM(A.GRID_TR_PV_PW_H) AS GRID_TR_PV_PW_H  
						, TRUNC(SUM(A.GRID_TR_PV_PW_PRICE), 2) AS GRID_TR_PV_PW_PRICE    
						, TRUNC(SUM(A.GRID_TR_PV_PW_CO2), 2) AS GRID_TR_PV_PW_CO2  
						, SUM(A.GRID_TR_BT_PW_H) AS GRID_TR_BT_PW_H  
						, TRUNC(SUM(A.GRID_TR_BT_PW_PRICE), 2) AS GRID_TR_BT_PW_PRICE    
						, TRUNC(SUM(A.GRID_TR_BT_PW_CO2), 2) AS GRID_TR_BT_PW_CO2    
						, SUM(A.OUTLET_PW_H) AS OUTLET_PW_H    
						, TRUNC(SUM(A.OUTLET_PW_PRICE), 2) AS OUTLET_PW_PRICE   
						, TRUNC(SUM(A.OUTLET_PW_CO2), 2) AS OUTLET_PW_CO2 
						, SUM(A.PCS_FD_PW_H)			AS PCS_FD_PW_H  
						, TRUNC(SUM(A.PCS_FD_PW_PRICE), 2)		AS PCS_FD_PW_PRICE  
						, TRUNC(SUM(A.PCS_FD_PW_CO2), 2)			AS PCS_FD_PW_CO2  
						, SUM(A.PCS_PCH_PW_H)			AS PCS_PCH_PW_H  
						, TRUNC(SUM(A.PCS_PCH_PW_PRICE), 2)		AS PCS_PCH_PW_PRICE  
						, TRUNC(SUM(A.PCS_PCH_PW_CO2), 2)		AS PCS_PCH_PW_CO2  
				FROM TB_STT_ESS_TM A
--				JOIN AR ON A.DEVICE_ID = AR.DEVICE_ID AND A.DEVICE_TYPE_CD = AR.DEVICE_TYPE_CD
				WHERE A.COLEC_TM BETWEEN ''%s'' AND ''%s''
				GROUP BY A.DEVICE_ID, A.DEVICE_TYPE_CD 
			)
			, B AS (
				SELECT 
					 ''%s'' AS COLEC_DD    
					, %s AS UTC_OFFSET   
					, A.DEVICE_ID, A.DEVICE_TYPE_CD
					, PV_PW_H, PV_PW_PRICE, PV_PW_CO2  
					, CONS_PW_H, CONS_PW_PRICE	, CONS_PW_CO2     
					, CONS_GRID_PW_H, CONS_GRID_PW_PRICE, CONS_GRID_PW_CO2   
					, CONS_PV_PW_H, CONS_PV_PW_PRICE, CONS_PV_PW_CO2  
					, CONS_BT_PW_H, CONS_BT_PW_PRICE, CONS_BT_PW_CO2  
					, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2  
					, BT_DCHRG_PW_H	, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2  
					, GRID_OB_PW_H, GRID_OB_PW_PRICE, GRID_OB_PW_CO2  
					, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2  
					, GRID_TR_PV_PW_H, GRID_TR_PV_PW_PRICE	, GRID_TR_PV_PW_CO2  
					, GRID_TR_BT_PW_H, GRID_TR_BT_PW_PRICE	, GRID_TR_BT_PW_CO2    
					, OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2 
					, PCS_FD_PW_H	, PCS_FD_PW_PRICE, PCS_FD_PW_CO2  
					, PCS_PCH_PW_H, PCS_PCH_PW_PRICE	, PCS_PCH_PW_CO2  
					, OUTLET_PW  
					, PV_V1, PV_I1, PV_PW1, PV_V2	, PV_I2, PV_PW2  
					, INVERTER_V, INVERTER_I, INVERTER_PW	, DC_LINK_V  
					, G_RLY_CNT	, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT  
					, RACK_V	, RACK_I  
					, CELL_MAX_V, CELL_MIN_V, CELL_AVG_V	, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T 
					, PV_PW, CONS_PW, BT_PW, BT_SOH  
					, GRID_PW, PCS_PW, PCS_TGT_PW, BT_SOC
					, SYS_EXTRACT_UTC(NOW()) AS CREATE_DT  
				FROM  TB_BAS_DEVICE_PMS B
				join AR  on B.DEVICE_ID  = AR.DEVICE_ID and B.DEVICE_TYPE_CD = AR.DEVICE_TYPE_CD
				join  A on AR.DEVICE_ID  = A.DEVICE_ID and AR.DEVICE_TYPE_CD = A.DEVICE_TYPE_CD
			), U AS (
				UPDATE TB_STT_ESS_DD dd
				SET  
					 PV_PW_H              		= B.PV_PW_H               
					, PV_PW_PRICE          	= B.PV_PW_PRICE           
					, PV_PW_CO2            	= B.PV_PW_CO2             
					, CONS_PW_H            	= B.CONS_PW_H             
					, CONS_PW_PRICE        	= B.CONS_PW_PRICE         
					, CONS_PW_CO2          	= B.CONS_PW_CO2           
					, CONS_GRID_PW_H       = B.CONS_GRID_PW_H        
					, CONS_GRID_PW_PRICE  = B.CONS_GRID_PW_PRICE    
					, CONS_GRID_PW_CO2    = B.CONS_GRID_PW_CO2      
					, CONS_PV_PW_H         	= B.CONS_PV_PW_H          
					, CONS_PV_PW_PRICE     = B.CONS_PV_PW_PRICE      
					, CONS_PV_PW_CO2       = B.CONS_PV_PW_CO2        
					, CONS_BT_PW_H         	= B.CONS_BT_PW_H          
					, CONS_BT_PW_PRICE     	= B.CONS_BT_PW_PRICE      
					, CONS_BT_PW_CO2       = B.CONS_BT_PW_CO2        
					, BT_CHRG_PW_H         	= B.BT_CHRG_PW_H          
					, BT_CHRG_PW_PRICE     = B.BT_CHRG_PW_PRICE      
					, BT_CHRG_PW_CO2       = B.BT_CHRG_PW_CO2        
					, BT_DCHRG_PW_H        = B.BT_DCHRG_PW_H         
					, BT_DCHRG_PW_PRICE   = B.BT_DCHRG_PW_PRICE     
					, BT_DCHRG_PW_CO2     = B.BT_DCHRG_PW_CO2       
					, GRID_OB_PW_H         	= B.GRID_OB_PW_H          
					, GRID_OB_PW_PRICE     	= B.GRID_OB_PW_PRICE      
					, GRID_OB_PW_CO2       	= B.GRID_OB_PW_CO2        
					, GRID_TR_PW_H         	= B.GRID_TR_PW_H          
					, GRID_TR_PW_PRICE     	= B.GRID_TR_PW_PRICE      
					, GRID_TR_PW_CO2       	= B.GRID_TR_PW_CO2        
					, GRID_TR_PV_PW_H      	= B.GRID_TR_PV_PW_H       
					, GRID_TR_PV_PW_PRICE	= B.GRID_TR_PV_PW_PRICE   
					, GRID_TR_PV_PW_CO2   = B.GRID_TR_PV_PW_CO2     
					, GRID_TR_BT_PW_H      	= B.GRID_TR_BT_PW_H       
					, GRID_TR_BT_PW_PRICE  = B.GRID_TR_BT_PW_PRICE   
					, GRID_TR_BT_PW_CO2	= B.GRID_TR_BT_PW_CO2   
				    , BT_SOC               		= B.BT_SOC  
				    , OUTLET_PW_H          	= B.OUTLET_PW_H   
				    , OUTLET_PW_PRICE      	= B.OUTLET_PW_PRICE   
				    , OUTLET_PW_CO2        	= B.OUTLET_PW_CO2   
					, CREATE_DT            		= B.CREATE_DT    
					, PV_PW 						= B.PV_PW  
					, CONS_PW 					= B.CONS_PW  
					, BT_PW 						= B.BT_PW  
					, BT_SOH 					= B.BT_SOH  
					, GRID_PW 					= B.GRID_PW 	 
					, PCS_PW 					= B.PCS_PW  
					, PCS_TGT_PW 				= B.PCS_TGT_PW  
					, PCS_FD_PW_H				= B.PCS_FD_PW_H  
					, PCS_FD_PW_PRICE 		= B.PCS_FD_PW_PRICE  
					, PCS_FD_PW_CO2 			= B.PCS_FD_PW_CO2  
					, PCS_PCH_PW_H 			= B.PCS_PCH_PW_H  
					, PCS_PCH_PW_PRICE 		= B.PCS_PCH_PW_PRICE  
					, PCS_PCH_PW_CO2 		= B.PCS_PCH_PW_CO2  
					, OUTLET_PW 				= B.OUTLET_PW  
					, PV_V1 						= B.PV_V1  
					, PV_I1 						= B.PV_I1  
					, PV_PW1 					= B.PV_PW1  
					, PV_V2 						= B.PV_V2  
					, PV_I2 						= B.PV_I2  
					, PV_PW2 					= B.PV_PW2  
					, INVERTER_V 				= B.INVERTER_V  
					, INVERTER_I 				= B.INVERTER_I  
					, INVERTER_PW 			= B.INVERTER_PW  
					, DC_LINK_V 				= B.DC_LINK_V  
					, G_RLY_CNT 				= B.G_RLY_CNT  
					, BAT_RLY_CNT 				= B.BAT_RLY_CNT  
					, GRID_TO_GRID_RLY_CNT= B.GRID_TO_GRID_RLY_CNT  
					, BAT_PCHRG_RLY_CNT 	= B.BAT_PCHRG_RLY_CNT  
					, RACK_V 					= B.RACK_V  
					, RACK_I 						= B.RACK_I  
					, CELL_MAX_V 				= B.CELL_MAX_V  
					, CELL_MIN_V 				= B.CELL_MIN_V  
					, CELL_AVG_V 				= B.CELL_AVG_V  
					, CELL_MAX_T 				= B.CELL_MAX_T  
					, CELL_MIN_T 				= B.CELL_MIN_T  
					, CELL_AVG_T 				= B.CELL_AVG_T 
				FROM 	B
				WHERE 	dd.COLEC_DD 			= B.COLEC_DD
				AND	 	dd.UTC_OFFSET			= B.UTC_OFFSET
				AND		dd.DEVICE_ID			= B.DEVICE_ID
				AND		dd.DEVICE_TYPE_CD	= B.DEVICE_TYPE_CD
			    RETURNING dd.*
			)			
	       	INSERT INTO TB_STT_ESS_DD (  
				 COLEC_DD, UTC_OFFSET, DEVICE_ID, DEVICE_TYPE_CD   
				, PV_PW_H, PV_PW_PRICE, PV_PW_CO2             
				, CONS_PW_H, CONS_PW_PRICE	, CONS_PW_CO2, CONS_GRID_PW_H, CONS_GRID_PW_PRICE, CONS_GRID_PW_CO2
				, CONS_PV_PW_H, CONS_PV_PW_PRICE, CONS_PV_PW_CO2, CONS_BT_PW_H	, CONS_BT_PW_PRICE, CONS_BT_PW_CO2        
				, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2, BT_DCHRG_PW_H, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2       
				, GRID_OB_PW_H, GRID_OB_PW_PRICE	, GRID_OB_PW_CO2
				, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2
				, GRID_TR_PV_PW_H, GRID_TR_PV_PW_PRICE	, GRID_TR_PV_PW_CO2     
				, GRID_TR_BT_PW_H, GRID_TR_BT_PW_PRICE	, GRID_TR_BT_PW_CO2   
			    , BT_SOC
			    , OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2  
				, CREATE_DT    
				, PV_PW, CONS_PW, BT_PW, BT_SOH, GRID_PW
				, PCS_PW, PCS_TGT_PW, PCS_FD_PW_H, PCS_FD_PW_PRICE, PCS_FD_PW_CO2	, PCS_PCH_PW_H, PCS_PCH_PW_PRICE, PCS_PCH_PW_CO2 		 
				, OUTLET_PW
				, PV_V1, PV_I1, PV_PW1, PV_V2	, PV_I2, PV_PW2 				 
				, INVERTER_V, INVERTER_I, INVERTER_PW
				, DC_LINK_V
				, G_RLY_CNT, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT 	 
				, RACK_V, RACK_I 				 
				, CELL_MAX_V, CELL_MIN_V, CELL_AVG_V	, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T 			 
			) 
			SELECT
				 COLEC_DD, UTC_OFFSET, DEVICE_ID, DEVICE_TYPE_CD   
				, PV_PW_H, PV_PW_PRICE, PV_PW_CO2             
				, CONS_PW_H, CONS_PW_PRICE	, CONS_PW_CO2, CONS_GRID_PW_H, CONS_GRID_PW_PRICE, CONS_GRID_PW_CO2
				, CONS_PV_PW_H, CONS_PV_PW_PRICE, CONS_PV_PW_CO2, CONS_BT_PW_H	, CONS_BT_PW_PRICE, CONS_BT_PW_CO2        
				, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2, BT_DCHRG_PW_H, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2       
				, GRID_OB_PW_H, GRID_OB_PW_PRICE	, GRID_OB_PW_CO2
				, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2
				, GRID_TR_PV_PW_H, GRID_TR_PV_PW_PRICE	, GRID_TR_PV_PW_CO2     
				, GRID_TR_BT_PW_H, GRID_TR_BT_PW_PRICE	, GRID_TR_BT_PW_CO2   
			    , BT_SOC
			    , OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2  
				, CREATE_DT    
				, PV_PW, CONS_PW, BT_PW, BT_SOH, GRID_PW
				, PCS_PW, PCS_TGT_PW, PCS_FD_PW_H, PCS_FD_PW_PRICE, PCS_FD_PW_CO2	, PCS_PCH_PW_H, PCS_PCH_PW_PRICE, PCS_PCH_PW_CO2 		 
				, OUTLET_PW
				, PV_V1, PV_I1, PV_PW1, PV_V2	, PV_I2, PV_PW2 				 
				, INVERTER_V, INVERTER_I, INVERTER_PW
				, DC_LINK_V
				, G_RLY_CNT, BAT_RLY_CNT, GRID_TO_GRID_RLY_CNT, BAT_PCHRG_RLY_CNT 	 
				, RACK_V, RACK_I 				 
				, CELL_MAX_V, CELL_MIN_V, CELL_AVG_V	, CELL_MAX_T, CELL_MIN_T, CELL_AVG_T 			 
			FROM B
			WHERE NOT EXISTS (  SELECT 1 
			    FROM 	U 
			    WHERE 	U.COLEC_DD 		= B.COLEC_DD
			    AND		U.UTC_OFFSET 		= B.UTC_OFFSET
			    AND		U.DEVICE_ID 		= B.DEVICE_ID
			    AND		U.DEVICE_TYPE_CD= B.DEVICE_TYPE_CD
			 ) ;'
    		, v_tm, v_to, v_from, v_to, i_exe_base, v_row.utc_offset);
    
--	        RAISE NOTICE 'i_sp_nm[%] UTC_OFFSET[%] 시작[%] 종료[%] v_tm[%] SQL[%]', i_sp_nm, v_row.utc_offset, v_from, v_to, v_tm, v_sql;
    		EXECUTE v_sql;
	       
	        GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	
--	      	RAISE notice 'i_sp_nm[%] Merge TB_STT_ESS_DD Table [%]건', i_sp_nm, v_work_num;

	  END LOOP;

 	    ------------------------------------------------
		-- 일별 순위 통계 집계 프로시저 수행
		------------------------------------------------
--         CALL SP_RANK_DAILY(i_exe_base);

--	 RAISE notice 'i_sp_nm[%] End Loop !!! ', i_sp_nm;

	  -- 실행성공
      v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS then
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
   	return v_rslt_cd;

END;
$function$
;

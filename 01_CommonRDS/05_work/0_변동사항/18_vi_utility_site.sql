create or replace view vi_utility_site(parent_id, installer_id, site_id) as
SELECT bui.parent_id,   --상위_id, utility 계정, 서비스계정
       bui.installer_id,
       bd.site_id
FROM tb_bas_device_pms bd
         JOIN tb_bas_installer_group bui ON bd.instl_user_id::text = bui.installer_id::text;

-- alter table vi_utility_site owner to wems;
-- GRANT ALL ON TABLE vi_utility_site TO wems;

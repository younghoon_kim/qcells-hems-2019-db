CREATE OR REPLACE FUNCTION fn_check_db_state()
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
  NAME:       fn_check_db_state
  PURPOSE:

  REVISIONS:
  Ver        Date        Author           Description
  ---------  ----------  ---------------  ------------------------------------
  1.0        2020-04-27  이재형                       최초생성   
  
  NOTES:   db 상태를 확인하여 결과를 반환한다.
                   < INPUT >


                   < OUTPUT >
					'batch err' 배치문제
					'query delay' 롱쿼리문제
					'AccessExclusiveLock' lock 발생
*****************************************************************************/
-- 수동 실행 스크립트
-- SELECT fn_check_db_state();
declare
v_row record;
v_flag varchar;
v_cnt numeric :=0;
begin
	
	-- table lock (AccessExclusiveLock) 확인 , 해당 lock은 select를 제한함
	SELECT count(t.relname)
	into v_cnt
	FROM pg_locks l,
	     pg_stat_all_tables t
	WHERE l.relation = t.relid
	and mode = 'AccessExclusiveLock'
	group by l.relation
	ORDER BY relation ASC;

	--AccessExclusiveLock 해당 테이블 확인시 pid 종료 등을 위해 내용반환, AccessExclusiveLock 메시지 반환
	if v_cnt > 0 then
		for v_row in (
			SELECT t.relname as table_name,   
				pid,    
				mode
			FROM pg_locks l,
			     pg_stat_all_tables t
			WHERE l.relation = t.relid
			and mode = 'AccessExclusiveLock'
			ORDER BY relation ASC
		)
		loop
			raise notice 'table : %, pid : %, mode : %',v_row.table_name, v_row.pid, v_row.mode;
		end loop;
		return 'AccessExclusiveLock';
	end if;

	
	-- 배치 상태 확인 
	SELECT
	CASE WHEN COUNT(*) = 0 THEN 'Y' ELSE 'N' END AS JOB_STATUS
	into v_flag
	FROM
	tb_sys_sp_exe_hst
	WHERE
	err_msg is not null
	and exe_base >= to_char(now()- interval '1 hour','YYYYMMDDHH24MISS');
	
	--배치 오류 발생시 해당 오류와 batch_err 메세지 반환
	if v_flag ='N' then 
		 for v_row in (
			SELECT
			sp_nm, exe_base
			FROM
			tb_sys_sp_exe_hst
			WHERE
			err_msg is not null
			and exe_base >= to_char(now()- interval '1 hour','YYYYMMDDHH24MISS')
		)
		loop
		raise notice 'err_batch %, %' ,v_row.sp_nm,v_row.exe_base;
		end loop;
		return 'batch err';
	end if;
	
	-- 롱쿼리 확인
	select count(*)
	into v_cnt
	from pg_stat_activity
	where state = 'active'
	and current_timestamp - query_start  > '1 min';

	-- 롱쿼리 확인시 해당 쿼리와 query delay 메시지 반환
	if v_cnt > 0 then
		for v_row in (
			select current_timestamp - query_start as runtime,
      			   query
			from pg_stat_activity
			where state = 'active'
			and current_timestamp - query_start  > '1 min'
		)
		loop
			raise notice 'runtime : % , query : %', v_row.runtime, v_row.query;
		end loop;
		return 'query delay';
	end if;

	-- 세가지 해당 없을시 ok 메세지 반환
	return 'ok';

end;
$function$
;

-- Drop table
--DROP TABLE tb_opr_ess_profile;

CREATE TABLE tb_opr_ess_profile(
    profile_id INT GENERATED ALWAYS AS IDENTITY ,    --profile_ID, sequence 사용
    profile_name varchar(255), --profile_name
    info jsonb, --설정할 파라미터의 집합
    type varchar, --설정 범위 : fullset , installer
    cntry_cd varchar, --대상 국가
    owner varchar(100), --프로파일 생성자, system : qcell에서 배포함, 베이스가 되는 프로파일, installer_id, admin_id : 본인이 생성한 프로파이만 삭제 가능
    profile_desc varchar, --profile에 대한 설명
    update_dt timestamptz NOT NULL DEFAULT sys_extract_utc(now()), -- 생성일시
    base_profile_id integer,

	CONSTRAINT pk_opr_ess_profile PRIMARY KEY (profile_id)
);
create unique index tb_opr_ess_profile_profile_name_owner_uindex
    on tb_opr_ess_profile (profile_name, owner);

COMMENT ON TABLE tb_opr_ess_profile IS ' ESS 의 기본설정 정보 테이블';

-- Column comments
COMMENT ON COLUMN tb_opr_ess_profile.profile_id IS 'profile_ID, sequence 사용, squence name : sq_opr_profile';
COMMENT ON COLUMN tb_opr_ess_profile.profile_name IS 'profile_name';
COMMENT ON COLUMN tb_opr_ess_profile.info IS '설정할 파라미터의 집합';
COMMENT ON COLUMN tb_opr_ess_profile.type IS '설정 범위 : fullset (전체설정), installer(인스톨러용) ';
COMMENT ON COLUMN tb_opr_ess_profile.cntry_cd IS '대상 국가';
COMMENT ON COLUMN tb_opr_ess_profile.owner IS '프로파일 생성자';
COMMENT ON COLUMN tb_opr_ess_profile.profile_desc IS '프로파일에 대한 설명';
COMMENT ON COLUMN tb_opr_ess_profile.update_dt IS '생성일시';
comment on column tb_opr_ess_profile.base_profile_id is 'fullset profile id ';
-- Permissions

ALTER TABLE tb_opr_ess_profile OWNER TO wems;
GRANT ALL ON TABLE tb_opr_ess_profile TO wems;
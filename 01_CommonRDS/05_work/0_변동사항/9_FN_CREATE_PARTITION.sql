CREATE OR REPLACE FUNCTION FN_CREATE_PARTITION(sDate character varying, eDate character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:		FN_CREATE_PARTITION
   PURPOSE:	파티션 자동생성에 문제가 생긴경우 수동으로 해당 파티션을 생성해주기 위한 function		

   REVISIONS:
   Ver        Date        Author          Description
   ---------  ----------  --------------  ------------------------------------
   1.0        2020-04-03  김현덕       	1. Created this function.

	usage:	SELECT FN_CREATE_PARTITION('20200329','20200402')
   NOTES:	시작날짜, 마지막 날짜를 받아서 테이블에 해당기간만큼의 파티션을 생성.
				처리량이 많은 일간 테이블만을 대상으로 함.
	Input : 
	        
	Output :
******************************************************************************/	
DECLARE
	/***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
	v_rslt_cd 	TEXT := null;
	v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
	v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
	v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
	v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3
	 
	tbl 	record;
	genDate	record;
	v_sql TEXT := '';
	v_postStr TEXT :='';

BEGIN

    /***************************************************************************
      2.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';
		
		FOR tbl IN ( 
			SELECT SUBSTRING(LOWER(table_nm),4) AS nm FROM tb_plf_tbl_meta WHERE partition_type='D'
		) LOOP
			FOR genDate IN(
				select TO_CHAR( GENERATE_SERIES(sDate::date, eDate::date , '1 day'::interval ) , 'YYYYMMDD' ) AS startDay
					, TO_CHAR( GENERATE_SERIES(sDate::date+'1 day'::interval ,eDate::date+'1 day'::interval  , '1 day'::interval ) , 'YYYYMMDD' ) AS endDay
			) LOOP
				IF tbl.nm = 'stt_ess_dd' THEN v_postStr := '';
				ELSIF tbl.nm = 'opr_ess_state_hist' THEN v_postStr := '';
				ELSIF tbl.nm = 'stt_rank_dd' THEN v_postStr := '';
				ELSIF tbl.nm = 'stt_ess_tm' THEN v_postStr := '00';
				ELSE v_postStr := '000000';
				END IF;
				v_sql := v_sql || 'CREATE TABLE IF NOT EXISTS pt_' || tbl.nm || '_' || genDate.startDay || ' PARTITION OF tb_'||tbl.nm || ' FOR VALUES FROM (''' || genDate.startDay||v_postStr||''') TO ('''||genDate.endDay||v_postStr||''');' || chr(10);
			END LOOP;
	   END LOOP;
		EXECUTE v_sql;
        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   return v_err_msg;

END;
$function$

CREATE OR REPLACE FUNCTION sp_quity_wrty_dd(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       SP_QUITY_WRTY_DD
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2015-04-10   yngwie       1. Created this function.
   2.0        2019-09-20   박동환        Postgresql 변환
   2.1        2020-07-24   서정우        site_id추가로 인해 tb_bas_device -> tb_bas_device_pms 를 사용하도록 수정

   NOTES:   품질 보증을 위해 전일자의 특정 수집 항목들의 일 평균, 최소, 최대값 등을 저장한다.
            1일 주기 수행

    Input : i_exe_base VARCHAR 집계 대상 년월일

	Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   					-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     					-- 적재수
    v_work_num		INTEGER   := 0;     					-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_ems_pt_tbl 	VARCHAR   := NULL;  
    v_bms_pt_tbl 	VARCHAR   := NULL;  
   	v_sql 				VARCHAR   := NULL;  
   	v_cnt				INTEGER   := 0;  
   
BEGIN
	i_exe_base := SUBSTR(i_exe_base, 1, 8);

	/***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
		 v_rslt_cd := 'A';

		v_ems_pt_tbl := CONCAT_WS('', 'pt_raw_ems_info_', i_exe_base);
	    v_bms_pt_tbl := CONCAT_WS('', 'pt_raw_bms_info_', i_exe_base);

--	   	RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] START[%] i_exe_base[%] v_ems_pt_tbl[%] v_bms_pt_tbl[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base, v_ems_pt_tbl, v_bms_pt_tbl;
      
        -- 해당 파티션 테이블이 있는지 확인	
		SELECT 	COUNT(child.relname)	-- 파티션명
		INTO 		v_cnt
		FROM 	pg_inherits
	    JOIN 		pg_class parent ON pg_inherits.inhparent = parent.oid
	    JOIN 		pg_class child	ON pg_inherits.inhrelid   = child.oid
	    JOIN 		pg_namespace nmsp_parent  ON nmsp_parent.oid = parent.relnamespace
	    JOIN 		pg_namespace nmsp_child    ON nmsp_child.oid   = child.relnamespace
		WHERE 	child.relname IN ( LOWER(v_ems_pt_tbl), LOWER(v_bms_pt_tbl) );

--	    RAISE NOTICE 'i_sp_nm[%] i_site_seq[%] PARTITION TABLE EXISTS[%]', i_sp_nm, i_site_seq, CASE WHEN v_cnt = 2 THEN 'TRUE' ELSE 'FALSE' END;
	
        -- 해당 파티션 테이블이 있으면 수행한다.		
		IF v_cnt = 2 THEN
			v_rslt_cd := 'B';
		
			-- 기존 데이터가 있으면 삭제
			DELETE FROM TB_STT_QUITY_WRTY_DD WHERE COLEC_DD = i_exe_base;

			GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       
--	    	RAISE NOTICE 'i_sp_nm[%] delete TB_STT_QUITY_WRTY_DD Table : [%]건, i_exe_base[%]', i_sp_nm, v_work_num, i_exe_base;

     		v_rslt_cd := 'C';

	      	-- 신규 데이터 추가
	      	v_sql := FORMAT(	'
				WITH BMS AS ( 
					SELECT 	ROW_NUMBER() OVER (PARTITION BY DEVICE_ID ORDER BY COLEC_DT DESC) SEQ 
							, DEVICE_ID 
							, COL15 
							, COL17 
							, COL18 
							, COL19 
							, COL21 
							, COL22 
					FROM 	%s 
				) , EMS AS ( 
					SELECT 	DEVICE_ID 
							, TRUNC(MAX(COL281::NUMERIC), 1) AS EMSBOARD_TEMP_MAX_AVG 
							, TRUNC(MIN(COL281::NUMERIC), 1) AS EMSBOARD_TEMP_MIN_AVG
					FROM 	%s
					GROUP BY DEVICE_ID 
				) 
				INSERT INTO TB_STT_QUITY_WRTY_DD ( 
					COLEC_DD 
					, DEVICE_ID 
					, DEVICE_TYPE_CD 
					, CELL_MAX_VOL_AVG 
					, CELL_MIN_VOL_AVG 
					, RACK_CUR_AVG 
					, CELL_MAX_TEMP_AVG 
					, CELL_MIN_TEMP_AVG 
					, EMSBOARD_TEMP_MAX_AVG 
					, EMSBOARD_TEMP_MIN_AVG 
					, LAST_SOH 
				) 
				SELECT 
					 %s AS COLEC_DD
					, A.DEVICE_ID 
					, A.DEVICE_TYPE_CD 
					, TRUNC(AVG(B.COL18::NUMERIC), 3) AS CELL_MAX_VOL_AVG 
					, TRUNC(AVG(B.COL19::NUMERIC), 3) AS CELL_MIN_VOL_AVG 
					, TRUNC(AVG(B.COL17::NUMERIC), 1) AS RACK_CUR_AVG 
					, TRUNC(AVG(B.COL21::NUMERIC), 1) AS CELL_MAX_TEMP_AVG 
					, TRUNC(AVG(B.COL22::NUMERIC), 1) AS CELL_MIN_TEMP_AVG 
					, MAX(C.EMSBOARD_TEMP_MAX_AVG) AS EMSBOARD_TEMP_MAX_AVG 
					, MAX(C.EMSBOARD_TEMP_MIN_AVG) AS EMSBOARD_TEMP_MIN_AVG 
					, TRUNC(MAX((CASE WHEN D.COL15=''NA'' THEN NULL ELSE D.COL15 END)::NUMERIC), 1) AS LAST_SOH 
				FROM 	TB_BAS_DEVICE_PMS A
						, BMS B 
						, BMS D	
						, EMS C 
				WHERE 	A.DEVICE_ID = B.DEVICE_ID 
				AND 	D.SEQ = 1 
				AND 	A.DEVICE_ID = D.DEVICE_ID 
				AND 	A.DEVICE_ID = C.DEVICE_ID 
				GROUP BY A.DEVICE_ID, A.DEVICE_TYPE_CD	;',
				v_bms_pt_tbl, v_ems_pt_tbl, CHR(39)||i_exe_base||CHR(39)
			);
			
			
	       	EXECUTE v_sql;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;		        
 --	       	RAISE NOTICE 'i_sp_nm[%] insert TB_STT_QUITY_WRTY_DD Table : [%]건, SQL[%]', i_sp_nm, v_work_num, v_sql;
 	       
	    END IF;
   
--    	RAISE NOTICE 'i_sp_nm[%] End Loop !!! ', i_sp_nm;

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
                           
   	return v_rslt_cd;

END;
$function$
;

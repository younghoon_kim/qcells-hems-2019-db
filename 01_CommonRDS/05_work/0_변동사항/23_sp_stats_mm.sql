CREATE OR REPLACE FUNCTION sp_stats_mm(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
/******************************************************************************
   NAME:       SP_STATS_MM
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-26   yngwie       Created this function.
   2.0        2019-09-20   박동환        Postgresql 변환
   2.2        2020-07-24   서정우        1. site_id추가로 인해 tb_bas_device -> tb_bas_device_pms 를 사용하도록 수정
                                        2. sp_rank_monthly 주석 처리
   2.3        2020-08-23   윤상원        BT_REAL_SOC 컬럼 추가
   2.4        2020-10-26   서정우        load_main_pw_h, load_sub_pw_h 컬럼 추가 (tb_raw_ems_info의 col39, col40)

   NOTES:   시간별 통계 데이터를 가지고 월별 통계 데이터를 생성한다.
            1일 주기 갱신, 월 변경시 누적. (매일 0시 5분에 전일까지의 집계 수행)

            Source Table : TB_STT_ESS_TM
            Target Table : TB_STT_ESS_MM

    Input : i_exe_base VARCHAR 집계 대상 년월

	Output :

******************************************************************************/
DECLARE

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_from			VARCHAR   := NULL; 
    v_to				VARCHAR   := NULL; 
    v_row           	record; 
  	v_sql 				VARCHAR  	:= NULL; 
   
BEGIN
	i_exe_base := SUBSTR(i_exe_base, 1, 6);

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	  BEGIN
  
		v_rslt_cd := 'A';
--	    RAISE NOTICE 'i_sp_nm:[%] i_site_seq[%] START[%] i_exe_base[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base;

    	FOR v_row IN ( 
		    SELECT  UTC_OFFSET  
		    FROM (  
		    	SELECT	0 AS UTC_OFFSET  
		    	
		    	UNION ALL    
		    	SELECT  UTC_OFFSET  
		    	FROM   TB_BAS_CITY  
		    	WHERE  UTC_OFFSET IS NOT NULL  
		    	
		    	UNION ALL  
		    	SELECT  UTC_OFFSET + 1 
		    	FROM  TB_BAS_CITY  
		    	WHERE UTC_OFFSET IS NOT NULL  
		    )  T
		    GROUP by UTC_OFFSET 
		    
		) LOOP
            -- 추가할 데이터의 시작, 종료일시를 구한다.
            v_from := FN_TS_STR_TO_STR ( i_exe_base || '0100', v_row.utc_offset, 'YYYYMMDDHH24', 0, 'YYYYMMDDHH24' );
            v_to    := FN_TS_STR_TO_STR ( TO_CHAR(TO_TIMESTAMP(i_exe_base, 'YYYYMM')+INTERVAL '1 MONTH -1 DAY', 'YYYYMMDD23'), v_row.utc_offset, 'YYYYMMDDHH24', 0, 'YYYYMMDDHH24' );
	       
--		    RAISE NOTICE 'i_sp_nm:[%] LOOP utc_offset[%]', i_sp_nm, v_row.utc_offset;
		   
		   
    		-- 시간별 통계 데이터를 SUM 해서 월별 통계 테이블에 Merge
   		  	v_sql := FORMAT(' 
			WITH AR AS ( 
				SELECT  	* 
				FROM (  
					SELECT 	  DEVICE_ID
								, DEVICE_TYPE_CD
								, BT_SOC, BT_REAL_SOC
								, ROW_NUMBER() OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM DESC) AS RNUM
					FROM 	TB_STT_ESS_TM
					WHERE   COLEC_TM BETWEEN ''%s'' AND ''%s''
				)  Z
				WHERE RNUM = 1
			), A AS (
				SELECT
					 A.DEVICE_ID
					, A.DEVICE_TYPE_CD
					, SUM(A.PV_PW_H) AS PV_PW_H
					, TRUNC(SUM(A.PV_PW_PRICE), 2) AS PV_PW_PRICE
					, TRUNC(SUM(A.PV_PW_CO2), 2) AS PV_PW_CO2
					, SUM(A.CONS_PW_H) AS CONS_PW_H
					, TRUNC(SUM(A.CONS_PW_PRICE), 2) AS CONS_PW_PRICE
					, TRUNC(SUM(A.CONS_PW_CO2), 2) AS CONS_PW_CO2
					, SUM(A.CONS_GRID_PW_H) AS CONS_GRID_PW_H
					, TRUNC(SUM(A.CONS_GRID_PW_PRICE), 2) AS CONS_GRID_PW_PRICE
					, TRUNC(SUM(A.CONS_GRID_PW_CO2), 2) AS CONS_GRID_PW_CO2
					, SUM(A.CONS_PV_PW_H) AS CONS_PV_PW_H
					, TRUNC(SUM(A.CONS_PV_PW_PRICE), 2) AS CONS_PV_PW_PRICE
					, TRUNC(SUM(A.CONS_PV_PW_CO2), 2) AS CONS_PV_PW_CO2
					, SUM(A.CONS_BT_PW_H) AS CONS_BT_PW_H
					, TRUNC(SUM(A.CONS_BT_PW_PRICE), 2) AS CONS_BT_PW_PRICE
					, TRUNC(SUM(A.CONS_BT_PW_CO2), 2) AS CONS_BT_PW_CO2
					, SUM(A.BT_CHRG_PW_H) AS BT_CHRG_PW_H
					, TRUNC(SUM(A.BT_CHRG_PW_PRICE), 2) AS BT_CHRG_PW_PRICE
					, TRUNC(SUM(A.BT_CHRG_PW_CO2), 2) AS BT_CHRG_PW_CO2
					, SUM(A.BT_DCHRG_PW_H) AS BT_DCHRG_PW_H
					, TRUNC(SUM(A.BT_DCHRG_PW_PRICE), 2) AS BT_DCHRG_PW_PRICE
					, TRUNC(SUM(A.BT_DCHRG_PW_CO2), 2) AS BT_DCHRG_PW_CO2
					, SUM(A.GRID_OB_PW_H) AS GRID_OB_PW_H
					, TRUNC(SUM(A.GRID_OB_PW_PRICE), 2) AS GRID_OB_PW_PRICE
					, TRUNC(SUM(A.GRID_OB_PW_CO2), 2) AS GRID_OB_PW_CO2
					, SUM(A.GRID_TR_PW_H) AS GRID_TR_PW_H
					, TRUNC(SUM(A.GRID_TR_PW_PRICE), 2) AS GRID_TR_PW_PRICE
					, TRUNC(SUM(A.GRID_TR_PW_CO2), 2) AS GRID_TR_PW_CO2
					, SUM(A.GRID_TR_PV_PW_H) AS GRID_TR_PV_PW_H
					, TRUNC(SUM(A.GRID_TR_PV_PW_PRICE), 2) AS GRID_TR_PV_PW_PRICE
					, TRUNC(SUM(A.GRID_TR_PV_PW_CO2), 2) AS GRID_TR_PV_PW_CO2
					, SUM(A.GRID_TR_BT_PW_H) AS GRID_TR_BT_PW_H
					, TRUNC(SUM(A.GRID_TR_BT_PW_PRICE), 2) AS GRID_TR_BT_PW_PRICE
					, TRUNC(SUM(A.GRID_TR_BT_PW_CO2), 2) AS GRID_TR_BT_PW_CO2
					, SUM(A.OUTLET_PW_H) AS OUTLET_PW_H
					, TRUNC(SUM(A.OUTLET_PW_PRICE), 2) AS OUTLET_PW_PRICE
					, TRUNC(SUM(A.OUTLET_PW_CO2), 2) AS OUTLET_PW_CO2
					, SUM(A.PCS_FD_PW_H)			AS PCS_FD_PW_H
					, TRUNC(SUM(A.PCS_FD_PW_PRICE), 2)		AS PCS_FD_PW_PRICE
					, TRUNC(SUM(A.PCS_FD_PW_CO2), 2)			AS PCS_FD_PW_CO2
					, SUM(A.PCS_PCH_PW_H)			AS PCS_PCH_PW_H
					, TRUNC(SUM(A.PCS_PCH_PW_PRICE), 2)		AS PCS_PCH_PW_PRICE
					, TRUNC(SUM(A.PCS_PCH_PW_CO2), 2)		AS PCS_PCH_PW_CO2
	   		  	        , SUM(A.LOAD_MAIN_PW_H)			AS LOAD_MAIN_PW_H
	   		  	        , SUM(A.LOAD_SUB_PW_H)			AS LOAD_SUB_PW_H
				FROM 	TB_STT_ESS_TM A
				WHERE	A.COLEC_TM BETWEEN ''%s'' AND ''%s''
				GROUP BY A.DEVICE_ID, A.DEVICE_TYPE_CD

			), B as (
				SELECT
					 ''%s'' AS COLEC_MM
					, %s AS UTC_OFFSET
					, A.DEVICE_ID
					, A.DEVICE_TYPE_CD
					, A.PV_PW_H, A.PV_PW_PRICE, A.PV_PW_CO2, A.CONS_PW_H
					, A.CONS_PW_PRICE, A.CONS_PW_CO2, A.CONS_GRID_PW_H, A.CONS_GRID_PW_PRICE, A.CONS_GRID_PW_CO2
					, A.CONS_PV_PW_H, A.CONS_PV_PW_PRICE, A.CONS_PV_PW_CO2
					, A.CONS_BT_PW_H, A.CONS_BT_PW_PRICE, A.CONS_BT_PW_CO2
					, A.BT_CHRG_PW_H, A.BT_CHRG_PW_PRICE, A.BT_CHRG_PW_CO2
					, A.BT_DCHRG_PW_H, A.BT_DCHRG_PW_PRICE, A.BT_DCHRG_PW_CO2
					, A.GRID_OB_PW_H, A.GRID_OB_PW_PRICE, A.GRID_OB_PW_CO2
					, A.GRID_TR_PW_H, A.GRID_TR_PW_PRICE, A.GRID_TR_PW_CO2
					, A.GRID_TR_PV_PW_H, A.GRID_TR_PV_PW_PRICE, A.GRID_TR_PV_PW_CO2
					, A.GRID_TR_BT_PW_H, A.GRID_TR_BT_PW_PRICE, A.GRID_TR_BT_PW_CO2
--				    , MAX(AR.BT_SOC) AS BT_SOC
--   		  	    , MAX(AR.BT_REAL_SOC) AS BT_REAL_SOC
				    , AR.BT_SOC
   		  	        , AR.BT_REAL_SOC
					, A.OUTLET_PW_H, A.OUTLET_PW_PRICE, A.OUTLET_PW_CO2
					, SYS_EXTRACT_UTC(NOW()) AS CREATE_DT
					, A.PCS_FD_PW_H, A.PCS_FD_PW_PRICE, A.PCS_FD_PW_CO2, A.PCS_PCH_PW_H, A.PCS_PCH_PW_PRICE, A.PCS_PCH_PW_CO2
	   		  	        , A.LOAD_MAIN_PW_H
	   		  	        , A.LOAD_SUB_PW_H
				FROM  A
						, TB_BAS_DEVICE_PMS B
						, AR
				WHERE  A.DEVICE_ID 			= B.DEVICE_ID
					AND A.DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD
					AND A.DEVICE_ID 			= AR.DEVICE_ID
					AND A.DEVICE_TYPE_CD 	= AR.DEVICE_TYPE_CD
--				GROUP BY A.DEVICE_ID, A.DEVICE_TYPE_CD

	      ), U AS (
			UPDATE TB_STT_ESS_MM mm
			SET
				  PV_PW_H              = B.PV_PW_H
				, PV_PW_PRICE          = B.PV_PW_PRICE
				, PV_PW_CO2            = B.PV_PW_CO2
				, CONS_PW_H            = B.CONS_PW_H
				, CONS_PW_PRICE        = B.CONS_PW_PRICE
				, CONS_PW_CO2          = B.CONS_PW_CO2
				, CONS_GRID_PW_H       = B.CONS_GRID_PW_H
				, CONS_GRID_PW_PRICE   = B.CONS_GRID_PW_PRICE
				, CONS_GRID_PW_CO2     = B.CONS_GRID_PW_CO2
				, CONS_PV_PW_H         = B.CONS_PV_PW_H
				, CONS_PV_PW_PRICE     = B.CONS_PV_PW_PRICE
				, CONS_PV_PW_CO2       = B.CONS_PV_PW_CO2
				, CONS_BT_PW_H         = B.CONS_BT_PW_H
				, CONS_BT_PW_PRICE     = B.CONS_BT_PW_PRICE
				, CONS_BT_PW_CO2       = B.CONS_BT_PW_CO2
				, BT_CHRG_PW_H         = B.BT_CHRG_PW_H
				, BT_CHRG_PW_PRICE     = B.BT_CHRG_PW_PRICE
				, BT_CHRG_PW_CO2       = B.BT_CHRG_PW_CO2
				, BT_DCHRG_PW_H        = B.BT_DCHRG_PW_H
				, BT_DCHRG_PW_PRICE    = B.BT_DCHRG_PW_PRICE
				, BT_DCHRG_PW_CO2      = B.BT_DCHRG_PW_CO2
				, GRID_OB_PW_H         = B.GRID_OB_PW_H
				, GRID_OB_PW_PRICE     = B.GRID_OB_PW_PRICE
				, GRID_OB_PW_CO2       = B.GRID_OB_PW_CO2
				, GRID_TR_PW_H         = B.GRID_TR_PW_H
				, GRID_TR_PW_PRICE     = B.GRID_TR_PW_PRICE
				, GRID_TR_PW_CO2       = B.GRID_TR_PW_CO2
				, GRID_TR_PV_PW_H      = B.GRID_TR_PV_PW_H
				, GRID_TR_PV_PW_PRICE  = B.GRID_TR_PV_PW_PRICE
				, GRID_TR_PV_PW_CO2    = B.GRID_TR_PV_PW_CO2
				, GRID_TR_BT_PW_H      = B.GRID_TR_BT_PW_H
				, GRID_TR_BT_PW_PRICE  = B.GRID_TR_BT_PW_PRICE
				, GRID_TR_BT_PW_CO2    = B.GRID_TR_BT_PW_CO2
			    , BT_SOC               		= B.BT_SOC
   		  	    , BT_REAL_SOC               = B.BT_REAL_SOC
			    , OUTLET_PW_H          	= B.OUTLET_PW_H
			    , OUTLET_PW_PRICE      = B.OUTLET_PW_PRICE
			    , OUTLET_PW_CO2        = B.OUTLET_PW_CO2
				, CREATE_DT            		= B.CREATE_DT
				, PCS_FD_PW_H				= B.PCS_FD_PW_H
				, PCS_FD_PW_PRICE 		= B.PCS_FD_PW_PRICE
				, PCS_FD_PW_CO2 			= B.PCS_FD_PW_CO2
				, PCS_PCH_PW_H 			= B.PCS_PCH_PW_H
				, PCS_PCH_PW_PRICE 		= B.PCS_PCH_PW_PRICE
				, PCS_PCH_PW_CO2 			= B.PCS_PCH_PW_CO2
	   		  	, LOAD_MAIN_PW_H      = B.LOAD_MAIN_PW_H
	   		  	, LOAD_SUB_PW_H       = B.LOAD_SUB_PW_H
	      	FROM 	B
			WHERE	mm.COLEC_MM 		= B.COLEC_MM
				AND 	mm.UTC_OFFSET 		= B.UTC_OFFSET
				AND 	mm.DEVICE_ID			= B.DEVICE_ID
				AND 	mm.DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD
				RETURNING mm.*
	      )
		INSERT INTO TB_STT_ESS_MM (
				 COLEC_MM	, UTC_OFFSET, DEVICE_ID	, DEVICE_TYPE_CD
				, PV_PW_H, PV_PW_PRICE, PV_PW_CO2
				, CONS_PW_H, CONS_PW_PRICE	, CONS_PW_CO2, CONS_GRID_PW_H
				, CONS_GRID_PW_PRICE, CONS_GRID_PW_CO2
				, CONS_PV_PW_H, CONS_PV_PW_PRICE, CONS_PV_PW_CO2
				, CONS_BT_PW_H, CONS_BT_PW_PRICE, CONS_BT_PW_CO2
				, BT_CHRG_PW_H, BT_CHRG_PW_PRICE, BT_CHRG_PW_CO2
				, BT_DCHRG_PW_H	, BT_DCHRG_PW_PRICE, BT_DCHRG_PW_CO2
				, GRID_OB_PW_H, GRID_OB_PW_PRICE	, GRID_OB_PW_CO2
				, GRID_TR_PW_H, GRID_TR_PW_PRICE, GRID_TR_PW_CO2
				, GRID_TR_PV_PW_H, GRID_TR_PV_PW_PRICE	, GRID_TR_PV_PW_CO2
				, GRID_TR_BT_PW_H, GRID_TR_BT_PW_PRICE	, GRID_TR_BT_PW_CO2
			    , BT_SOC, BT_REAL_SOC
			    , OUTLET_PW_H, OUTLET_PW_PRICE, OUTLET_PW_CO2
				, CREATE_DT
				, PCS_FD_PW_H, PCS_FD_PW_PRICE	, PCS_FD_PW_CO2
				, PCS_PCH_PW_H, PCS_PCH_PW_PRICE, PCS_PCH_PW_CO2
		                , LOAD_MAIN_PW_H
		                , LOAD_SUB_PW_H
			)
			SELECT
				 B.COLEC_MM, B.UTC_OFFSET, B.DEVICE_ID, B.DEVICE_TYPE_CD
				, B.PV_PW_H	, B.PV_PW_PRICE, B.PV_PW_CO2
				, B.CONS_PW_H	, B.CONS_PW_PRICE, B.CONS_PW_CO2
				, B.CONS_GRID_PW_H, B.CONS_GRID_PW_PRICE, B.CONS_GRID_PW_CO2
				, B.CONS_PV_PW_H, B.CONS_PV_PW_PRICE, B.CONS_PV_PW_CO2
				, B.CONS_BT_PW_H	, B.CONS_BT_PW_PRICE, B.CONS_BT_PW_CO2
				, B.BT_CHRG_PW_H	, B.BT_CHRG_PW_PRICE, B.BT_CHRG_PW_CO2
				, B.BT_DCHRG_PW_H, B.BT_DCHRG_PW_PRICE, B.BT_DCHRG_PW_CO2
				, B.GRID_OB_PW_H	, B.GRID_OB_PW_PRICE, B.GRID_OB_PW_CO2
				, B.GRID_TR_PW_H, B.GRID_TR_PW_PRICE, B.GRID_TR_PW_CO2
				, B.GRID_TR_PV_PW_H	, B.GRID_TR_PV_PW_PRICE, B.GRID_TR_PV_PW_CO2
				, B.GRID_TR_BT_PW_H	, B.GRID_TR_BT_PW_PRICE, B.GRID_TR_BT_PW_CO2
			    , B.BT_SOC, B.BT_REAL_SOC
			    , B.OUTLET_PW_H, B.OUTLET_PW_PRICE, B.OUTLET_PW_CO2  
				, B.CREATE_DT  
				, B.PCS_FD_PW_H, B.PCS_FD_PW_PRICE, B.PCS_FD_PW_CO2 		 
				, B.PCS_PCH_PW_H	, B.PCS_PCH_PW_PRICE, B.PCS_PCH_PW_CO2
		                , B.LOAD_MAIN_PW_H
		                , B.LOAD_SUB_PW_H
			FROM B
			WHERE NOT EXISTS (  
				SELECT 	1 
			    FROM 	U 
				WHERE	COLEC_MM 			= B.COLEC_MM  
					AND 	UTC_OFFSET 		= B.UTC_OFFSET  
					AND 	DEVICE_ID			= B.DEVICE_ID  
					AND 	DEVICE_TYPE_CD 	= B.DEVICE_TYPE_CD  
			  ) ;', v_from, v_to, v_from, v_to, i_exe_base, v_row.utc_offset);
			 
--	      	RAISE NOTICE 'i_sp_nm[%] Merge TB_STT_ESS_MM Table [%]건 UTC_OFFSET[%] 시작[%] 종료[%] SQL[%]', i_sp_nm, v_work_num, v_row.utc_offset, v_from, v_to, v_sql;

			EXECUTE v_sql;
	      	GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
	        v_rslt_cnt := v_rslt_cnt + v_work_num;	       


	   END LOOP;
	   
	    ------------------------------------------------
		-- 월별 순위 통계 집계 프로시저 수행
		------------------------------------------------
--         PERFORM SP_RANK_MONTHLY('SP_RANK_MONTHLY', 0, i_exe_base, i_exe_mthd);

        -- 실행성공
        v_rslt_cd := '1';
        
    EXCEPTION WHEN OTHERS THEN
--          	RAISE notice 'i_sp_nm[%] EXCEPTION !!! ', i_sp_nm;

        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;
   
   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );
   	return v_rslt_cd;

END;
$function$
;

-- Drop Table
DROP TABLE IF EXISTS dc_cnf_manager;

-- Create Table
create table dc_cnf_manager
(
	id varchar(40) not null
		constraint dc_cnf_manager_pk
			primary key,
	ip varchar(20) not null,
	port integer default 22,
	acct_id varchar(60),
	acct_pw varchar(150),
	base_path varchar(200),
	intl_ip varchar(20),
	use_yn varchar(1) default 'Y'::character varying not null
);

COMMENT ON TABLE DC_CNF_MANAGER IS 'DAQ Worker Manager 정보';
COMMENT ON COLUMN DC_CNF_MANAGER.id IS 'Worker Manager의 아이디';
COMMENT ON COLUMN DC_CNF_MANAGER.ip IS 'Worker Server의 Public IP';
COMMENT ON COLUMN DC_CNF_MANAGER.port IS 'Worker 서버에 SSH 접속을 위한 Port';
COMMENT ON COLUMN DC_CNF_MANAGER.acct_id IS 'Worker 서버 계정 아이디';
COMMENT ON COLUMN DC_CNF_MANAGER.acct_pw IS 'Worker 서버 계정 패스워드 ( DB 암호화 )';
COMMENT ON COLUMN DC_CNF_MANAGER.base_path IS 'DAQ 프로그램 설치 기본 위치';
COMMENT ON COLUMN DC_CNF_MANAGER.intl_ip IS 'Worker 서버의 Private IP';
COMMENT ON COLUMN DC_CNF_MANAGER.use_yn IS '사용여부';

alter table dc_cnf_manager owner to wems;

-- Insert Data
INSERT INTO wems.dc_cnf_manager (id, ip, port, acct_id, acct_pw, base_path, intl_ip, use_yn) VALUES ('WKM1', '54.66.19.245', 22, 'bems', 'f3c135d8b192173f91899e2e115101a8', '/home/bems/daq', '11.0.30.102', 'Y');
-- INSERT INTO wems.dc_cnf_manager (id, ip, port, acct_id, acct_pw, base_path, intl_ip, use_yn) VALUES ('WKM2', '3.106.119.8', 22, 'bems', 'f3c135d8b192173f91899e2e115101a8', '/home/bems/daq', '11.0.47.115', 'N');
--

--email noti에 필요한 칼럼 추가
	ALTER TABLE wems.tb_opr_alarm ADD send_dt timestamp NULL;
	ALTER TABLE wems.tb_bas_model_alarm_code_map ADD repeat_flag varchar(1) NULL ,add  repeat_interval varchar(6) NULL;
	
--email noti error desc 수정 내용
	TRUNCATE TABLE wems.tb_plf_code_desc_trans;
	ALTER TABLE wems.tb_plf_code_desc_trans ADD COLUMN model_type varchar(10);
	ALTER TABLE wems.tb_plf_code_desc_trans DROP CONSTRAINT pk_plf_code_desc_trans; 
	ALTER TABLE wems.tb_plf_code_desc_trans ADD CONSTRAINT pk_plf_code_desc_trans PRIMARY KEY (grp_cd, code, lang_cd, model_type);
	--ALTER TABLE WEMS.TB_PLF_CODE_DESC_TRANS ADD CONSTRAINT PK_PLF_CODE_DESC_TRANS PRIMARY KEY(GRP_CD,CODE,LANG_CD);	--기존 pk
	--위 작업이후 csv 파일의 데이터 import

--데이터가 올라오고 있는 장비가 Off Line으로 표시되는 문제를 수정하기 위해 추가
	ALTER TABLE wems.tb_bas_device ADD connector_id varchar(30) default null -- CONNECTOR ID;

-- 활동목록 리스트 쿼리 성능 개선을 위한 인덱스 추가
    CREATE INDEX idx_tb_opr_act ON wems.tb_opr_act USING btree (create_dt);

--	tb_raw_login 테이블을 partition이 아닌 일반 테이블로 변경
	DROP INDEX pk_raw_login;
	CREATE TABLE TB_RAW_LOGIN_NEW (like TB_RAW_LOGIN including all);
	INSERT INTO TB_RAW_LOGIN_NEW ( select * from TB_RAW_LOGIN);
	
	DROP TABLE TB_RAW_LOGIN;
	ALTER TABLE tb_raw_login_new RENAME TO TB_RAW_LOGIN;
	COMMENT ON TABLE TB_RAW_LOGIN IS 'EMS가 M2M 서버로 접속한 이력관리';
	ALTER TABLE tb_raw_login ADD CONSTRAINT pk_raw_login PRIMARY KEY (device_id, create_dt, create_tm, response_cd, ems_out_ipaddress, ems_tm, data_seq);

-- tb_raw_login을 파티션 테이블 생성 대상 목록에서 제외
	UPDATE tb_plf_tbl_meta SET partition_type=null WHERE table_nm ='TB_RAW_LOGIN'
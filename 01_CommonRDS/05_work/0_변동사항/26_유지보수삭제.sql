--************************
-- ***    유지보수 페이지 삭제에 따른 변경 ***
--************************

--DROP TABLE
DROP TABLE tb_opr_mtnc;
DROP TABLE tb_opr_mtnc_file;

--유지보수관련 코드 삭제
DELETE FROM tb_plf_code_info WHERE grp_cd='MTNC_TYPE_CD';
DELETE FROM tb_plf_code_info WHERE grp_cd='MTNC_STUS_CD';
DELETE FROM tb_plf_code_grp WHERE grp_cd='MTNC_TYPE_CD';
DELETE FROM tb_plf_code_grp WHERE grp_cd='MTNC_STUS_CD';

--활동목록코드에 유지보수 관련 코드 추가
INSERT INTO tb_plf_code_info(grp_cd, code, code_nm, ref_1, ref_2, ref_3, ref_4, ref_5, cd_desc, ord_seq, ref_6)
 VALUES ('ACT_TYPE_CD','21','SW 업데이트',null,null,null,'code.acttypecd.21',null,null,2,null);
INSERT INTO tb_plf_code_info(grp_cd, code, code_nm, ref_1, ref_2, ref_3, ref_4, ref_5, cd_desc, ord_seq, ref_6)
 VALUES ('ACT_TYPE_CD','60','제품교채',null,null,null,'code.acttypecd.60',null,null,6,null);
INSERT INTO tb_plf_code_info(grp_cd, code, code_nm, ref_1, ref_2, ref_3, ref_4, ref_5, cd_desc, ord_seq, ref_6)
 VALUES ('ACT_TYPE_CD','90','삭제',null,null,null,'code.acttypecd.90',null,null,9,null);


--유지보수->활동목록 트리거 삭제
DROP FUNCTION IF EXISTS tgr_act_mtnc$tb_opr_mtnc();
DROP TRIGGER IF EXISTS tgr_act_mtnc ON tb_opr_mtnc;

SELECT grp_cd,code,code_nm,ref_1,ref_4 FROM tb_plf_code_info where grp_cd='ACT_TYPE_CD' order by code;
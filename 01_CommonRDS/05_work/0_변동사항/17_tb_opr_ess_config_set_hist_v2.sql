-- Drop table
-- DROP TABLE tb_opr_ess_config_set_hist_v2;

CREATE TABLE tb_opr_ess_config_set_hist_v2
(
    device_id          varchar(50)      NOT NULL, -- 장비ID
    device_type_cd     varchar(4)       NOT NULL, -- 장비유형코드
    pv_max_pwr1        double precision NULL,     -- 1번패널최대발전파워(W)
    pv_max_pwr2        double precision NULL,     -- 2번패널최대발전파워(W)
    feed_in_limit      varchar(4)       NULL,     -- 매전량(%)
    pem_mode           numeric(1)       NULL,     -- 3rd Party Control (0=Disable, 1=Enable)
    smeterd0id         numeric(1)       NULL,     -- Smart Meter D0 ID1 (0=None, 4=EM112, 5=EM24)
    max_inverter_pw_cd varchar(4)       NULL,     -- InverterLimit, 인버터최대출력(W)
    basicmode_cd       varchar(4),-- 우선충전모드 0:Off, 1:On, NA(변경없음)
    backup_soc         numeric,
    hysteresis_low     numeric,
    hysteresis_high    numeric,
    profile_id         integer,
    profile_info       jsonb,
    update_id          varchar(64)      NOT NULL, -- 생성자ID
    update_dt          timestamptz      NOT NULL  -- 생성일시
);
COMMENT ON TABLE tb_opr_ess_config_set_hist_v2 IS 'ESS 의 설정 정보 제어 이력 테이블';

-- Column comments
COMMENT ON COLUMN tb_opr_ess_config_set_hist_v2.device_id IS '장비ID';
COMMENT ON COLUMN tb_opr_ess_config_set_hist_v2.device_type_cd IS '장비유형코드';
COMMENT ON COLUMN tb_opr_ess_config_set_hist_v2.pv_max_pwr1 IS '1번패널최대발전파워(W)';
COMMENT ON COLUMN tb_opr_ess_config_set_hist_v2.pv_max_pwr2 IS '2번패널최대발전파워(W)';
COMMENT ON COLUMN tb_opr_ess_config_set_hist_v2.feed_in_limit IS '매전량(%)';
COMMENT ON COLUMN tb_opr_ess_config_set_hist_v2.max_inverter_pw_cd IS 'InverterLimit, 인버터최대출력(W)';
COMMENT ON COLUMN tb_opr_ess_config_set_hist_v2.basicmode_cd IS '우선충전모드 0:Off, 1:On, NA(변경없음)';
COMMENT ON COLUMN tb_opr_ess_config_set_hist_v2.pem_mode IS '3rd Party Control (0=Disable, 1=Enable)';
COMMENT ON COLUMN tb_opr_ess_config_set_hist_v2.smeterd0id IS 'Smart Meter D0 ID1 (0=None, 4=EM112, 5=EM24)';
COMMENT ON COLUMN tb_opr_ess_config_set_hist_v2.profile_id IS 'profile_id';
COMMENT ON COLUMN tb_opr_ess_config_set_hist_v2.profile_info IS 'profile_info';
COMMENT ON COLUMN tb_opr_ess_config_set_hist_v2.update_id IS '생성자ID';
COMMENT ON COLUMN tb_opr_ess_config_set_hist_v2.update_dt IS '생성일시';


-- Permissions

ALTER TABLE tb_opr_ess_config_set_hist_v2
    OWNER TO wems;
GRANT ALL ON TABLE tb_opr_ess_config_set_hist_v2 TO wems;

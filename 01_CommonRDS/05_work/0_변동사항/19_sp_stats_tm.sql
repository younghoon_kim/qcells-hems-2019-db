CREATE OR REPLACE function sp_stats_tm(i_sp_nm character varying, i_site_seq numeric, i_exe_base character varying, i_exe_mthd character varying) returns character varying
    language plpgsql
as
$$
/******************************************************************************
   NAME:       SP_STATS_TM
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2014-06-20   yngwie  	Created this function.
   2.0        2019-09-20   박동환    Postgresql 변환
   2.1        2019-10-11   이재형    성능개선을 위해 파티션테이블 조회 방법수정
   2.4        2020-06-10   윤상원    네트워크 단절 통계 복구 처리
   2.5        2020-07-24   서정우    site_id추가로 인해 tb_bas_device -> tb_bas_device_pms, tb_bas_site 를 사용하도록 수정
   2.6        2020-08-23   윤상원    BT_REAL_SOC 컬럼 추가

   NOTES:   5분주기로 수집된 데이터를 가지고 시간별 통계 데이터를 생성한다.
              매시 2분에 전시각 데이터 집계 수행.

            Source Table : TB_OPR_ESS_STATE_HIST
            Target Table : TB_STT_ESS_TM

    Input :

	Output :

		Usage : SELECT SP_STATS_TM('SP_STATS_TM', 0, '2019102519', 'M');

******************************************************************************/
DECLARE
    -- yunsang
    result RECORD;
    result_dd RECORD;
    result_mm RECORD;

    /***************************************************************************
      1.변수 및 커서 선언
    ***************************************************************************/
    -- (1) 프로시져실행이력 변수
	v_st_dt			TIMESTAMP := CLOCK_TIMESTAMP(); -- 시작일시
    v_rslt_cd			VARCHAR   := '0';   						-- 실행상태코드 (0:실행,1:성공,그외:오류)
    v_rslt_cnt			INTEGER   := 0;     						-- 적재수
    v_work_num		INTEGER   := 0;     						-- 처리건수
    v_err_msg		VARCHAR   := NULL;  					-- SQL오류메시지내용
    v_err_msg1		VARCHAR   := NULL;  					-- 메시지내용1
    v_err_msg2		VARCHAR   := NULL;  					-- 메시지내용2
    v_err_msg3		VARCHAR   := NULL;  					-- 메시지내용3

    -- (2)업무처리 변수
    v_prev_dt			VARCHAR   := NULL;
    v_next_dt			VARCHAR   := NULL;


BEGIN
	i_exe_base := SUBSTR(i_exe_base, 1, 10);

    /***************************************************************************
      2.프로시져 시작로그 등록
    ***************************************************************************/
    PERFORM FN_SP_EXE_ST_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , i_exe_mthd
                            );

    /***************************************************************************
      3.업무처리
    ***************************************************************************/
	BEGIN
		v_rslt_cd := 'A';

--		RAISE notice 'i_sp_nm[%], i_site_seq[%], START[%], i_exe_base[%]', i_sp_nm, i_site_seq, TO_CHAR(SYS_EXTRACT_UTC(now()), 'YYYY-MM-DD HH24:MI:SS'), i_exe_base;

		-- 집계대상시각(i_exe_base)의 이전시각을 구한다.
		SELECT TO_CHAR( TO_TIMESTAMP(i_exe_base, 'YYYYMMDDHH24')-interval '1 hour', 'YYYYMMDDHH24')
		into	v_prev_dt;

		SELECT TO_CHAR( TO_TIMESTAMP(i_exe_base, 'YYYYMMDDHH24') + interval '1 hour', 'YYYYMMDDHH24')
		into	v_next_dt;

--		RAISE notice 'i_sp_nm[%], i_site_seq[%], i_exe_base[%], v_prev_dt[%]'	, i_sp_nm, i_site_seq, i_exe_base, v_prev_dt;

		-- 시간별 통계 테이블 데이터 중 i_exe_base 에 해당하는 레코드 Delete
		DELETE FROM TB_STT_ESS_TM WHERE COLEC_TM = i_exe_base;

        GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;
--		RAISE notice 'i_sp_nm[%], i_site_seq[%], DELETE TB_STT_ESS_TM Table :[%], 삭제건수[%]', i_sp_nm, i_site_seq, i_exe_base, v_work_num;

   		v_rslt_cd := 'B';

        WITH T AS (
			SELECT
			    DEVICE_ID
			    , DEVICE_TYPE_CD
			    , SUBSTR(COLEC_DT, 1, 10)  AS COLEC_TM
			    , PV_PW_H
			    , PV_PW_PRICE
			    , PV_PW_CO2
			    , CONS_PW_H
			    , CONS_PW_PRICE
			    , CONS_PW_CO2
			    , BT_CHRG_PW_H
			    , BT_CHRG_PW_PRICE
			    , BT_CHRG_PW_CO2
			    , BT_DCHRG_PW_H
			    , BT_DCHRG_PW_PRICE
			    , BT_DCHRG_PW_CO2
			    , GRID_OB_PW_H
			    , GRID_OB_PW_PRICE
			    , GRID_OB_PW_CO2
			    , GRID_TR_PW_H
			    , GRID_TR_PW_PRICE
			    , GRID_TR_PW_CO2
			-- [ 2015.05.13 yngwie OUTLET_PW 추가
			    , BT_SOC
                , BT_REAL_SOC
			    , OUTLET_PW_H
			    , OUTLET_PW_PRICE
			    , OUTLET_PW_CO2
			-- ]
			-- [ 2015.07.06 yngwie 수집 컬럼 추가
			    , PV_PW
			    , CONS_PW
			    , BT_PW
			    , BT_SOH
			    , GRID_PW
			    , PCS_PW
			    , PCS_TGT_PW
			    , PCS_FD_PW_H
			    , PCS_PCH_PW_H
			    , OUTLET_PW
			    , PV_V1
			    , PV_I1
			    , PV_PW1
			    , PV_V2
			    , PV_I2
			    , PV_PW2
			    , INVERTER_V
			    , INVERTER_I
			    , INVERTER_PW
			    , DC_LINK_V
			    , G_RLY_CNT
			    , BAT_RLY_CNT
			    , GRID_TO_GRID_RLY_CNT
			    , BAT_PCHRG_RLY_CNT
			    , RACK_V
			    , RACK_I
			    , CELL_MAX_V
			    , CELL_MIN_V
			    , CELL_AVG_V
			    , CELL_MAX_T
			    , CELL_MIN_T
			    , CELL_AVG_T
			    , SMTR_TR_CNTER
			    , SMTR_OB_CNTER
			FROM TB_OPR_ESS_STATE_HIST
			WHERE (COLEC_DT, DEVICE_ID) IN (
				      SELECT 	MAX(COLEC_DT) AS COLEC_DT, DEVICE_ID
				      FROM 	TB_OPR_ESS_STATE_HIST
				      -- 2019-10-11 파티션 테이블 조회 수정, 이재형
				      WHERE COLEC_DT >= i_exe_base AND COLEC_DT < v_next_dt
				      GROUP BY DEVICE_ID

				      UNION ALL

				      SELECT 	MAX(COLEC_DT) AS COLEC_DT, DEVICE_ID
				      FROM 	TB_OPR_ESS_STATE_HIST
				      WHERE	COLEC_DT BETWEEN SUBSTR(v_prev_dt, 1, 8)  || '000000' and v_prev_dt || '5959'
				      GROUP BY  DEVICE_ID
					)
		), B AS (
			SELECT
				DEVICE_ID
				, DEVICE_TYPE_CD
				, COLEC_TM
				, PV_PW_H
				, COALESCE(LAG(PV_PW_H , 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_PV_PW_H
				, CONS_PW_H
				, COALESCE(LAG(CONS_PW_H , 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_CONS_PW_H
				, BT_CHRG_PW_H
				, COALESCE(LAG(BT_CHRG_PW_H, 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_BT_CHRG_PW_H
				, BT_DCHRG_PW_H
				, COALESCE(LAG(BT_DCHRG_PW_H, 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_BT_DCHRG_PW_H
				, GRID_OB_PW_H
				, COALESCE(LAG(GRID_OB_PW_H, 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_GRID_OB_PW_H
				, GRID_TR_PW_H
				, COALESCE(LAG(GRID_TR_PW_H, 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_GRID_TR_PW_H
		-- [ 2015.05.13 yngwie OUTLET_PW 추가
				, BT_SOC
			    , BT_REAL_SOC
				, OUTLET_PW_H
				, COALESCE(LAG(OUTLET_PW_H , 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_OUTLET_PW_H
		-- ]
		-- [ 2015.07.06 yngwie 수집 컬럼 추가
				, PV_PW
				, CONS_PW
				, BT_PW
				, BT_SOH
				, GRID_PW
				, PCS_PW
				, PCS_TGT_PW
				, PCS_FD_PW_H
				, COALESCE(LAG(PCS_FD_PW_H , 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_PCS_FD_PW_H
				, PCS_PCH_PW_H
				, COALESCE(LAG(PCS_PCH_PW_H , 1) OVER (PARTITION BY DEVICE_ID, DEVICE_TYPE_CD ORDER BY COLEC_TM), 0) AS PREV_PCS_PCH_PW_H
				, OUTLET_PW
				, PV_V1
				, PV_I1
				, PV_PW1
				, PV_V2
				, PV_I2
				, PV_PW2
				, INVERTER_V
				, INVERTER_I
				, INVERTER_PW
				, DC_LINK_V
				, G_RLY_CNT
				, BAT_RLY_CNT
				, GRID_TO_GRID_RLY_CNT
				, BAT_PCHRG_RLY_CNT
				, RACK_V
				, RACK_I
				, CELL_MAX_V
				, CELL_MIN_V
				, CELL_AVG_V
				, CELL_MAX_T
				, CELL_MIN_T
				, CELL_AVG_T
				, SMTR_TR_CNTER
				, SMTR_OB_CNTER
		-- ]
			FROM T
		), A as (
			SELECT
				COLEC_TM
				, DEVICE_ID
				, DEVICE_TYPE_CD
				, CASE WHEN PV_PW_H - PREV_PV_PW_H < 0 THEN PV_PW_H ELSE PV_PW_H - PREV_PV_PW_H END PV_PW_H
				, NULL AS PV_PW_PRICE
				, NULL AS PV_PW_CO2
				, CASE WHEN CONS_PW_H - PREV_CONS_PW_H < 0 THEN CONS_PW_H ELSE CONS_PW_H - PREV_CONS_PW_H END CONS_PW_H
				, NULL AS CONS_PW_PRICE
				, NULL AS CONS_PW_CO2
				, NULL AS CONS_GRID_PW_H
				, NULL AS CONS_GRID_PW_PRICE
				, NULL AS CONS_GRID_PW_CO2
				, NULL AS CONS_PV_PW_H
				, NULL AS CONS_PV_PW_PRICE
				, NULL AS CONS_PV_PW_CO2
				, NULL AS CONS_BT_PW_H
				, NULL AS CONS_BT_PW_PRICE
				, NULL AS CONS_BT_PW_CO2
				, CASE WHEN BT_CHRG_PW_H - PREV_BT_CHRG_PW_H < 0 THEN BT_CHRG_PW_H ELSE BT_CHRG_PW_H - PREV_BT_CHRG_PW_H END BT_CHRG_PW_H
				, NULL AS BT_CHRG_PW_PRICE
				, NULL AS BT_CHRG_PW_CO2
				, CASE WHEN BT_DCHRG_PW_H - PREV_BT_DCHRG_PW_H < 0 THEN BT_DCHRG_PW_H ELSE BT_DCHRG_PW_H - PREV_BT_DCHRG_PW_H END BT_DCHRG_PW_H
				, NULL AS BT_DCHRG_PW_PRICE
				, NULL AS BT_DCHRG_PW_CO2
				, CASE WHEN GRID_OB_PW_H - PREV_GRID_OB_PW_H < 0 THEN GRID_OB_PW_H ELSE GRID_OB_PW_H - PREV_GRID_OB_PW_H END GRID_OB_PW_H
				, NULL AS GRID_OB_PW_PRICE
				, NULL AS GRID_OB_PW_CO2
				, CASE WHEN GRID_TR_PW_H - PREV_GRID_TR_PW_H < 0 THEN GRID_TR_PW_H ELSE GRID_TR_PW_H - PREV_GRID_TR_PW_H END GRID_TR_PW_H
				, NULL AS GRID_TR_PW_PRICE
				, NULL AS GRID_TR_PW_CO2
				, NULL AS GRID_TR_PV_PW_H
				, NULL AS GRID_TR_PV_PW_PRICE
				, NULL AS GRID_TR_PV_PW_CO2
				, NULL AS GRID_TR_BT_PW_H
				, NULL AS GRID_TR_BT_PW_PRICE
				, NULL AS GRID_TR_BT_PW_CO2
			 , FN_GET_DIV_GRID_TR(
			      CASE WHEN PV_PW_H - PREV_PV_PW_H < 0 THEN PV_PW_H ELSE PV_PW_H - PREV_PV_PW_H END
			     , CASE WHEN BT_DCHRG_PW_H - PREV_BT_DCHRG_PW_H < 0 THEN BT_DCHRG_PW_H ELSE BT_DCHRG_PW_H - PREV_BT_DCHRG_PW_H END
			     , CASE WHEN GRID_TR_PW_H - PREV_GRID_TR_PW_H < 0 THEN GRID_TR_PW_H ELSE GRID_TR_PW_H - PREV_GRID_TR_PW_H END
			   ) DIV_GRID_TR
			 , FN_GET_DIV_CONS (
			     CASE WHEN PV_PW_H - PREV_PV_PW_H < 0 THEN PV_PW_H ELSE PV_PW_H - PREV_PV_PW_H END
			     , CASE WHEN BT_DCHRG_PW_H - PREV_BT_DCHRG_PW_H < 0 THEN BT_DCHRG_PW_H ELSE BT_DCHRG_PW_H - PREV_BT_DCHRG_PW_H END
			     , CASE WHEN GRID_OB_PW_H - PREV_GRID_OB_PW_H < 0 THEN GRID_OB_PW_H ELSE GRID_OB_PW_H - PREV_GRID_OB_PW_H END
			     , CASE WHEN CONS_PW_H - PREV_CONS_PW_H < 0 THEN CONS_PW_H ELSE CONS_PW_H - PREV_CONS_PW_H END
			   ) DIV_CONS
			-- [ 2015.05.13 yngwie OUTLET_PW 추가
			   , BT_SOC
			   , BT_REAL_SOC
			   , CASE WHEN OUTLET_PW_H - PREV_OUTLET_PW_H < 0 THEN OUTLET_PW_H ELSE OUTLET_PW_H - PREV_OUTLET_PW_H END AS OUTLET_PW_H
			   , NULL AS OUTLET_PW_PRICE
			   , NULL AS OUTLET_PW_CO2
			-- ]
				, SYS_EXTRACT_UTC(NOW()) AS CREATE_DT
			-- [ 2015.07.06 yngwie 수집 컬럼 추가
				, PV_PW
				, CONS_PW
				, BT_PW
				, BT_SOH
				, GRID_PW
				, PCS_PW
				, PCS_TGT_PW
			    , CASE WHEN PCS_FD_PW_H - PREV_PCS_FD_PW_H < 0 THEN PCS_FD_PW_H ELSE PCS_FD_PW_H - PREV_PCS_FD_PW_H END AS PCS_FD_PW_H
			    , NULL AS PCS_FD_PW_PRICE
			    , NULL AS PCS_FD_PW_CO2
			    , CASE WHEN PCS_PCH_PW_H - PREV_PCS_PCH_PW_H < 0 THEN PCS_PCH_PW_H ELSE PCS_PCH_PW_H - PREV_PCS_PCH_PW_H END AS PCS_PCH_PW_H
			    , NULL AS PCS_PCH_PW_PRICE
			    , NULL AS PCS_PCH_PW_CO2
				, OUTLET_PW
				, PV_V1
				, PV_I1
				, PV_PW1
				, PV_V2
				, PV_I2
				, PV_PW2
				, INVERTER_V
				, INVERTER_I
				, INVERTER_PW
				, DC_LINK_V
				, G_RLY_CNT
				, BAT_RLY_CNT
				, GRID_TO_GRID_RLY_CNT
				, BAT_PCHRG_RLY_CNT
				, RACK_V
				, RACK_I
				, CELL_MAX_V
				, CELL_MIN_V
				, CELL_AVG_V
				, CELL_MAX_T
				, CELL_MIN_T
				, CELL_AVG_T
				, SMTR_TR_CNTER
				, SMTR_OB_CNTER
			-- ]
			FROM  B
			WHERE COLEC_TM = i_exe_base
		)
        --  각 전력량의 요금, CO2 와 부하를 PV, GRID, BT 로 나눈 결과를 계산하여 시간별 통계 테이블에 Insert
        INSERT INTO TB_STT_ESS_TM (
			COLEC_TM
			, DEVICE_ID
			, DEVICE_TYPE_CD
			, PV_PW_H
			, PV_PW_PRICE
			, PV_PW_CO2
			, CONS_PW_H
			, CONS_PW_PRICE
			, CONS_PW_CO2
			, CONS_GRID_PW_H
			, CONS_GRID_PW_PRICE
			, CONS_GRID_PW_CO2
			, CONS_PV_PW_H
			, CONS_PV_PW_PRICE
			, CONS_PV_PW_CO2
			, CONS_BT_PW_H
			, CONS_BT_PW_PRICE
			, CONS_BT_PW_CO2
			, BT_CHRG_PW_H
			, BT_CHRG_PW_PRICE
			, BT_CHRG_PW_CO2
			, BT_DCHRG_PW_H
			, BT_DCHRG_PW_PRICE
			, BT_DCHRG_PW_CO2
			, GRID_OB_PW_H
			, GRID_OB_PW_PRICE
			, GRID_OB_PW_CO2
			, GRID_TR_PW_H
			, GRID_TR_PW_PRICE
			, GRID_TR_PW_CO2
			, GRID_TR_PV_PW_H
			, GRID_TR_PV_PW_PRICE
			, GRID_TR_PV_PW_CO2
			, GRID_TR_BT_PW_H
			, GRID_TR_BT_PW_PRICE
			, GRID_TR_BT_PW_CO2
			-- [ 2015.05.13 yngwie OUTLET_PW 추가
			, BT_SOC
			, BT_REAL_SOC
			, OUTLET_PW_H
			, OUTLET_PW_PRICE
			, OUTLET_PW_CO2

			, CREATE_DT
			-- [ 2015.07.06 yngwie 수집 컬럼 추가
			, PV_PW
			, CONS_PW
			, BT_PW
			, BT_SOH
			, GRID_PW
			, PCS_PW
			, PCS_TGT_PW
			, PCS_FD_PW_H
			, PCS_FD_PW_PRICE
			, PCS_FD_PW_CO2
			, PCS_PCH_PW_H
			, PCS_PCH_PW_PRICE
			, PCS_PCH_PW_CO2
			, OUTLET_PW
			, PV_V1
			, PV_I1
			, PV_PW1
			, PV_V2
			, PV_I2
			, PV_PW2
			, INVERTER_V
			, INVERTER_I
			, INVERTER_PW
			, DC_LINK_V
			, G_RLY_CNT
			, BAT_RLY_CNT
			, GRID_TO_GRID_RLY_CNT
			, BAT_PCHRG_RLY_CNT
			, RACK_V
			, RACK_I
			, CELL_MAX_V
			, CELL_MIN_V
			, CELL_AVG_V
			, CELL_MAX_T
			, CELL_MIN_T
			, CELL_AVG_T
			, SMTR_TR_CNTER
			, SMTR_OB_CNTER
			-- ]
        )
        SELECT
        	A.COLEC_TM
        	, A.DEVICE_ID
        	, A.DEVICE_TYPE_CD
        	, A.PV_PW_H
        	, TRUNC(A.PV_PW_H::NUMERIC * ET.UNIT_PRICE * .001, 2) AS PV_PW_PRICE
        	, TRUNC(A.PV_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS PV_PW_CO2
        	, A.CONS_PW_H
        	, TRUNC(A.CONS_PW_H::NUMERIC * E.UNIT_PRICE * .001, 2) AS CONS_PW_PRICE
        	, TRUNC(A.CONS_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS CONS_PW_CO2
        	, FN_GET_SPLIT(DIV_CONS, 3, '|')::NUMERIC AS CONS_GRID_PW_H
        	, TRUNC(FN_GET_SPLIT(DIV_CONS, 3, '|')::NUMERIC * E.UNIT_PRICE * .001, 2) AS CONS_GRID_PW_PRICE
        	, TRUNC(FN_GET_SPLIT(DIV_CONS, 3, '|')::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS CONS_GRID_PW_CO2
        	, FN_GET_SPLIT(DIV_CONS, 1, '|')::NUMERIC AS CONS_PV_PW_H
        	, TRUNC(FN_GET_SPLIT(DIV_CONS, 1, '|')::NUMERIC * E.UNIT_PRICE * .001, 2) AS CONS_PV_PW_PRICE
        	, TRUNC(FN_GET_SPLIT(DIV_CONS, 1, '|')::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS CONS_PV_PW_CO2
        	, FN_GET_SPLIT(DIV_CONS, 2, '|')::NUMERIC AS CONS_BT_PW_H
        	, TRUNC(FN_GET_SPLIT(DIV_CONS, 2, '|')::NUMERIC * E.UNIT_PRICE * .001, 2) AS CONS_BT_PW_PRICE
        	, TRUNC(FN_GET_SPLIT(DIV_CONS, 2, '|')::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS CONS_BT_PW_CO2
        	, A.BT_CHRG_PW_H
        	, TRUNC(A.BT_CHRG_PW_H::NUMERIC * ET.UNIT_PRICE * .001, 2) AS BT_CHRG_PW_PRICE
        	, TRUNC(A.BT_CHRG_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS BT_CHRG_PW_CO2
        	, A.BT_DCHRG_PW_H
        	, TRUNC(A.BT_DCHRG_PW_H::NUMERIC * ET.UNIT_PRICE * .001, 2) AS BT_DCHRG_PW_PRICE
        	, TRUNC(A.BT_DCHRG_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS BT_DCHRG_PW_CO2
        	, A.GRID_OB_PW_H
        	, TRUNC(A.GRID_OB_PW_H::NUMERIC * E.UNIT_PRICE * .001, 2) AS GRID_OB_PW_PRICE
        	, TRUNC(A.GRID_OB_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS GRID_OB_PW_CO2
        	, A.GRID_TR_PW_H
        	, TRUNC(A.GRID_TR_PW_H::NUMERIC * ET.UNIT_PRICE * .001, 2) AS GRID_TR_PW_PRICE
        	, TRUNC(A.GRID_TR_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS GRID_TR_PW_CO2
        	, FN_GET_SPLIT(DIV_GRID_TR, 1, '|')::NUMERIC AS GRID_TR_PV_PW_H
        	, TRUNC(FN_GET_SPLIT(DIV_GRID_TR, 1, '|')::NUMERIC * ET.UNIT_PRICE * .001, 2) AS GRID_TR_PV_PW_PRICE
        	, TRUNC(FN_GET_SPLIT(DIV_GRID_TR, 1, '|')::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS GRID_TR_PV_PW_CO2
        	, FN_GET_SPLIT(DIV_GRID_TR, 2, '|')::NUMERIC AS GRID_TR_BT_PW_H
        	, TRUNC(FN_GET_SPLIT(DIV_GRID_TR, 2, '|')::NUMERIC * ET.UNIT_PRICE * .001, 2) AS GRID_TR_BT_PW_PRICE
        	, TRUNC(FN_GET_SPLIT(DIV_GRID_TR, 2, '|')::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS GRID_TR_BT_PW_CO2
		-- [ 2015.05.13 yngwie OUTLET_PW 추가
			, A.BT_SOC
            , A.BT_REAL_SOC
        	, A.OUTLET_PW_H
        	, TRUNC(A.OUTLET_PW_H::NUMERIC * E.UNIT_PRICE * .001, 2) AS OUTLET_PW_PRICE
        	, TRUNC(A.OUTLET_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS OUTLET_PW_CO2
        -- ]
        	, SYS_EXTRACT_UTC(NOW()) AS CREATE_DT
		-- [ 2015.07.06 yngwie 수집 컬럼 추가
			, A.PV_PW
			, A.CONS_PW
			, A.BT_PW
			, A.BT_SOH
			, A.GRID_PW
			, A.PCS_PW
			, A.PCS_TGT_PW
			, A.PCS_FD_PW_H
        	, TRUNC(A.PCS_FD_PW_H::NUMERIC * ET.UNIT_PRICE * .001, 2) AS PCS_FD_PW_PRICE
        	, TRUNC(A.PCS_FD_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS PCS_FD_PW_CO2
			, A.PCS_PCH_PW_H
        	, TRUNC(A.PCS_PCH_PW_H::NUMERIC * E.UNIT_PRICE * .001, 2) AS PCS_PCH_PW_PRICE
        	, TRUNC(A.PCS_PCH_PW_H::NUMERIC * D.CARB_EMI_QUITY * .001, 2) AS PCS_PCH_PW_CO2
			, A.OUTLET_PW
			, A.PV_V1
			, A.PV_I1
			, A.PV_PW1
			, A.PV_V2
			, A.PV_I2
			, A.PV_PW2
			, A.INVERTER_V
			, A.INVERTER_I
			, A.INVERTER_PW
			, A.DC_LINK_V
			, A.G_RLY_CNT
			, A.BAT_RLY_CNT
			, A.GRID_TO_GRID_RLY_CNT
			, A.BAT_PCHRG_RLY_CNT
			, A.RACK_V
			, A.RACK_I
			, A.CELL_MAX_V
			, A.CELL_MIN_V
			, A.CELL_AVG_V
			, A.CELL_MAX_T
			, A.CELL_MIN_T
			, A.CELL_AVG_T
			, A.SMTR_TR_CNTER
			, A.SMTR_OB_CNTER
		-- ]
        FROM		  A
		        	, TB_BAS_DEVICE_PMS B
		        	, TB_PLF_CARB_EMI_QUITY D
		        	, TB_BAS_ELPW_UNIT_PRICE E
		        	, TB_BAS_ELPW_UNIT_PRICE ET
                    , TB_BAS_SITE S
        WHERE 	A.DEVICE_ID = B.DEVICE_ID
            AND     B.site_id = S.site_id
            AND     S.CNTRY_CD = D.CNTRY_CD
--         	AND 	B.CNTRY_CD = D.CNTRY_CD
        	AND 	D.USE_FLAG = 'Y'
        	AND 	B.ELPW_PROD_CD = E.ELPW_PROD_CD
        	AND 	E.IO_FLAG = 'O'
        	AND 	E.USE_FLAG = 'Y'
        -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가
			AND 	E.WKDAY_FLAG = FN_GET_WKDAY_FLAG(SUBSTR(COLEC_TM, 1, 8))
		-- 2015.07.31 요금제 년월 변경
        --	AND 	SUBSTR(COLEC_TM, 1, 6) = E.UNIT_YM
        	AND 	E.UNIT_YM ='000000'
        	AND 	SUBSTR(COLEC_TM, 9, 2) = E.UNIT_HH
        	AND 	B.ELPW_PROD_CD = ET.ELPW_PROD_CD
        	AND 	ET.IO_FLAG = 'T'
        	AND 	ET.USE_FLAG = 'Y'
        -- 2015.05.13 modified by yngwie 주중, 주말 구분 추가, T 는  1 만 있음
		-- 2015.08.06 modified by yngwie T 도 주말 0 추가
		--	AND 	ET.WKDAY_FLAG = '1'
			AND 	ET.WKDAY_FLAG = FN_GET_WKDAY_FLAG(SUBSTR(COLEC_TM, 1, 8))
		-- 2015.07.31 요금제 년월 변경
        --	AND 	SUBSTR(COLEC_TM, 1, 6) = ET.UNIT_YM
        	AND 	ET.UNIT_YM = '000000'
        	AND 	SUBSTR(COLEC_TM, 9, 2) = ET.UNIT_HH
        ;

        GET DIAGNOSTICS v_work_num = ROW_COUNT;   -- 적재수
        v_rslt_cnt := v_rslt_cnt + v_work_num;
--		RAISE notice 'i_sp_nm[%], i_site_seq[%], Insert TB_STT_ESS_TM Table 입력건수[%]'	, i_sp_nm, i_site_seq, v_work_num;

   		v_rslt_cd := 'C';


		TRUNCATE TABLE tb_opr_offline_info_tmp;
		INSERT INTO tb_opr_offline_info_tmp SELECT * FROM tb_opr_offline_info TBNET WHERE TBNET.type = 'HOUR';
		TRUNCATE TABLE tb_opr_offline_info;

		FOR result IN
            SELECT *
            FROM tb_opr_offline_info_tmp TBN
            WHERE TBN.type = 'HOUR'
        LOOP
            PERFORM sp_stats_tm_offline('SP_STATS_TM_OFFLINE', 0, SUBSTR(result.start_tm, 1, 10), i_exe_mthd, result.device_id);
            --DELETE from tb_opr_netdis_info where device_id = result.device_id and date = result.date and type = result.type;
        END LOOP;

	    -------------------------------------------------
	    --- 일별 통계 집계 프로시저 수행
	    -------------------------------------------------
--		RAISE notice 'i_sp_nm[%], i_site_seq[%], Call SP_STATS_DD 입력일자[%]', i_sp_nm, i_site_seq, substring(i_exe_base, 1, 8);
   		PERFORM sp_stats_dd('SP_STATS_DD', 0, SUBSTR(i_exe_base, 1, 8), i_exe_mthd);

        -- 이미 처리된 날짜(i_exe_base)를 제외한 다른 날짜(dd)의 통계를 처리 - 이미 SP_STATS_DD에서 처리하였으므로
		FOR result_dd IN
            SELECT device_id, SUBSTR(start_tm, 1, 8) AS dd_date FROM tb_opr_offline_info_tmp WHERE SUBSTR(start_tm, 1, 8) <> SUBSTR(i_exe_base, 1, 8)  group by dd_date, device_id
        LOOP
            PERFORM sp_stats_dd_offline('SP_STATS_DD_OFFLINE', 0, result_dd.dd_date, i_exe_mthd, result_dd.device_id);
        END LOOP;

		-- (mm)의 통계를 처리
		FOR result_mm IN
		    SELECT device_id, SUBSTR(start_tm, 1, 6) AS mm_date FROM tb_opr_offline_info_tmp GROUP BY mm_date, device_id
        LOOP
            PERFORM sp_stats_mm_offline('SP_STATS_MM_OFFLINE', 0, result_mm.mm_date, i_exe_mthd, result_mm.device_id);
        END LOOP;

        -- 실행성공
        v_rslt_cd := '1';

    EXCEPTION WHEN OTHERS THEN
        /***************************************************************************
          오류메세지 등에 대한 처리
        ***************************************************************************/
        IF v_rslt_cd = '0' THEN
            v_rslt_cd := '2';
        END IF;

        v_rslt_cnt := 0;    --적재수
        GET STACKED DIAGNOSTICS v_err_msg1 = MESSAGE_TEXT
                               ,v_err_msg2 = PG_EXCEPTION_DETAIL
                               ,v_err_msg3 = PG_EXCEPTION_HINT
                               ;
        v_err_msg = v_err_msg1||'_|~'||v_err_msg2||'_|~'||v_err_msg3; --SQL오류메시지내용
    END;

   /***************************************************************************
      4.프로시져 종료로그 등록
    ***************************************************************************/
	PERFORM FN_SP_EXE_ED_LOG( i_sp_nm
                            , i_site_seq
                            , i_exe_base
                            , v_st_dt
                            , v_rslt_cd
                            , v_rslt_cnt
                            , v_err_msg
                            );

   	return v_rslt_cd;

END;
$$;

-- alter function sp_stats_tm(varchar, numeric, varchar, varchar) owner to wems;

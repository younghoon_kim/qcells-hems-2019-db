create table tb_test_mon_gens3_temp
(
    uid                  varchar,
    time                 timestamptz,
    pv_pw                integer,
    pv_pw_h              integer,
    pv_stus_cd           integer,
    cons_pw              integer,
    cons_pw_h            integer,
    bt_pw                integer,
    bt_chrg_pw_h         integer,
    bt_dchrg_pw_h        integer,
    bt_soc               double precision,
    bt_soh               double precision,
    bt_stus_cd           integer,
    grid_pw              integer,
    grid_ob_pw_h         integer,
    grid_tr_pw_h         integer,
--     grid_stus_cd         integer,
    pcs_pw               integer,
    pcs_fd_pw_h          integer,
    pcs_pch_pw_h         integer,
    ems_opmode           integer,
    pcs_opmode1          integer,
    pcs_opmode2          integer,
    pcs_tgt_pw           integer,
    outlet_pw            integer,
    outlet_pw_h          integer,
    sys_st_cd            integer,
    bt_chrg_st_cd        integer,
    bt_dchrg_st_cd       integer,
    pv_st_cd             integer,
    grid_st_cd           integer,
    smtr_st_cd           integer,
    smtr_tr_cnter        integer,
    smtr_ob_cnter        integer,
    pcs_flag0            integer,
    pcs_flag1            integer,
    pcs_flag2            integer,
    pcs_opmode_cmd1      integer,
    pcs_opmode_cmd2      integer,
    pv_v1                integer,
    pv_i1                integer,
    pv_pw1               integer,
    pv_v2                integer,
    pv_i2                integer,
    pv_pw2               integer,
    pv_v3                integer,
    pv_i3                integer,
    pv_pw3               integer,
    inverter_v           integer,
    inverter_i           integer,
    inverter_pw          integer,
    dc_link_v            integer,
    g_rly_cnt            integer,
    bat_rly_cnt          integer,
    grid_to_grid_rly_cnt integer,
    bat_pchrg_rly_cnt    integer,
    bms_flag0            integer,
    rack_v               integer,
    rack_i               integer,
--     cell_max_v           integer,
--     cell_min_v           integer,
--     cell_avg_v           integer,
--     cell_max_t           integer,
--     cell_min_t           integer,
--     cell_avg_t           integer,
    error_info           varchar,
    create_dt            timestamptz default sys_extract_utc(now()) not null
);
comment on column tb_test_mon_gens3_temp.uid is 'Device Serial Number';
comment on column tb_test_mon_gens3_temp.time is 'UTC Time';
comment on column tb_test_mon_gens3_temp.pv_pw is 'PV현재 발전 전력 PV 파워, W';
comment on column tb_test_mon_gens3_temp.pv_pw_h is 'PV일일누적 발전 전력양. PV 발전량(1일), Wh';
comment on column tb_test_mon_gens3_temp.pv_stus_cd is 'PV상태코드. ESS상태정보.PV 상태코드 (0:정지 / 1:발전)';
comment on column tb_test_mon_gens3_temp.cons_pw is '부하현재전력. 부하 파워, W';
comment on column tb_test_mon_gens3_temp.cons_pw_h is '부하일일누적전력량. 부하 전력량(1일), Wh';
comment on column tb_test_mon_gens3_temp.bt_pw is '배터리현재전력';
comment on column tb_test_mon_gens3_temp.bt_chrg_pw_h is '배터리일일누적충전량';
comment on column tb_test_mon_gens3_temp.bt_dchrg_pw_h is '배터리일일누적방전량 ';
comment on column tb_test_mon_gens3_temp.bt_soc is '배터리충전량백분율(SOC). 배터리 SOC, %';
comment on column tb_test_mon_gens3_temp.bt_soh is '배터리잔존수명(SOH). 배터리 SOH, %';
comment on column tb_test_mon_gens3_temp.bt_stus_cd is '배터리상태코드. 
운전상태
ESS상태정보.배터리 상태코드(매전/충전/소비)
( 0:방전 / 1:충전 / 2:정지)';
comment on column tb_test_mon_gens3_temp.grid_pw is 'GRID현재전력. 그리드 파워, W';
comment on column tb_test_mon_gens3_temp.grid_ob_pw_h is 'GRID일일누적소비전력량. 그리드 전력 사용량(1일), Wh';
comment on column tb_test_mon_gens3_temp.grid_tr_pw_h is 'GRID일일누적매전량. 그리드 전력 매전량(1일), kWh';
-- comment on column tb_test_mon_gens3_temp.grid_stus_cd is 'GRID상태코드. ESS상태정보.Grid상태코드 (0:소비 / 1:매전 / 2:정지)';
comment on column tb_test_mon_gens3_temp.pcs_pw is 'PCS 인버터 파워. W';
comment on column tb_test_mon_gens3_temp.pcs_fd_pw_h is 'PCS 인버터 전력 발전량. PCS 인버터 전력 출력량(1일), Wh';
comment on column tb_test_mon_gens3_temp.pcs_pch_pw_h is 'PCS 인버터 전력 사용량. PCS 인버터 전력 사용량(1일), Wh';
comment on column tb_test_mon_gens3_temp.ems_opmode is 'EMS 운영모드 EMS상태정보. X일 경우 정전표시
1: Self consumption
2. Zero Export Mode
3. Time-based Mode
4. External Generation Mode';
comment on column tb_test_mon_gens3_temp.pcs_opmode1 is 'PCS 운전모드 아래와 같이 변경됨
0: NOP
1: Auto Mode
2: Manual Mode';
comment on column tb_test_mon_gens3_temp.pcs_opmode2 is 'PCS 운전모드2 사용안함. 하지만 PCS에서 넘어온 값을 그대로 보낼예정';
comment on column tb_test_mon_gens3_temp.pcs_tgt_pw is 'PCS 타겟파워 W';
comment on column tb_test_mon_gens3_temp.outlet_pw is 'Power Outlet 전력 ESS상태정보.POWER OUTLET
EV 파워';
comment on column tb_test_mon_gens3_temp.outlet_pw_h is 'Power Outlet 전력량 EV 전력량 (1일)';
comment on column tb_test_mon_gens3_temp.sys_st_cd is '시스템 상태 코드 Col88 
- System Flag00 PCS_ready 값이 1이면서 PCS_fault 값이 0이면 1 보냄
- 그리고 Pcs와 communication에 문제가 없어야 함
- 위의 상태가 아니면 0을 보낸다.';
comment on column tb_test_mon_gens3_temp.bt_chrg_st_cd is '배터리 충전 가능여부 0: 불가능 / 1: 가능
- System Flag11 (batt1_ch_av | batt2_ch_av | batt3_ch_av) == 1이면 1 보냄
- System Flag00의 PCS_Fault가 1이면 0으로 set';
comment on column tb_test_mon_gens3_temp.bt_dchrg_st_cd is '배터리 방전 가능여부 0: 불가능 / 1: 가능
- System Flag11 (batt1_disch_av | batt2_disch_av | batt3_disch_av) == 1이면 1 보냄
- System Flag00의 PCS_Fault가 1이면 0으로 set';
comment on column tb_test_mon_gens3_temp.pv_st_cd is '인버터 방전(PV 발전 가능) 가능여부 0: 불가능 / 1: 가능
 - System Flag12 (pv1_av | pv2_av | pv3_av) == 1이면 1 보냄';
comment on column tb_test_mon_gens3_temp.grid_st_cd is '그리드 상태 코드
- System Flag01 grid_status값이 1이고 grid fault 값이 0이면 1
- 위의 값이 아니면 0을 보냄';
comment on column tb_test_mon_gens3_temp.smtr_st_cd is '스마트 미터(에너지 미터) 상태 코드
- 스마트미터로 부터 데이터를 받으면 1';
comment on column tb_test_mon_gens3_temp.smtr_tr_cnter is '스마트미터 총 사용량 ';
comment on column tb_test_mon_gens3_temp.smtr_ob_cnter  is '스마트미터 총 구매량';
comment on column tb_test_mon_gens3_temp.pcs_flag0 is 'System Flag00';
comment on column tb_test_mon_gens3_temp.pcs_flag1 is 'System Flag01';
comment on column tb_test_mon_gens3_temp.pcs_flag2 is 'System Flag02';
comment on column tb_test_mon_gens3_temp.pcs_opmode_cmd1 is 'PCS 제어 커맨드1 (PCS CMD1)
0 : NOP, 1 : Auto Mode, 2 : Manual Mode';
comment on column tb_test_mon_gens3_temp.pcs_opmode_cmd2 is 'PCS 제어 커맨드2 (PCS CMD2)
0 : Normal, 1: Forced Battery flag ';
comment on column tb_test_mon_gens3_temp.pv_v1 is 'PV 전압1';
comment on column tb_test_mon_gens3_temp.pv_i1 is 'PV 전류1';
comment on column tb_test_mon_gens3_temp.pv_pw1 is 'PV 파워1';
comment on column tb_test_mon_gens3_temp.pv_v2 is 'PV 전압2';
comment on column tb_test_mon_gens3_temp.pv_i2 is 'PV 전류2';
comment on column tb_test_mon_gens3_temp.pv_pw2 is 'PV 파워2';
comment on column tb_test_mon_gens3_temp.pv_v3 is 'PV 전압3';
comment on column tb_test_mon_gens3_temp.pv_i3 is 'PV 전류3';
comment on column tb_test_mon_gens3_temp.pv_pw3 is 'PV 파워3';
comment on column tb_test_mon_gens3_temp.inverter_v is '인버터 전압';
comment on column tb_test_mon_gens3_temp.inverter_i is '인버터 전류';
comment on column tb_test_mon_gens3_temp.inverter_pw is '인버터 파워';
comment on column tb_test_mon_gens3_temp.dc_link_v is ' DC Link 전압';
comment on column tb_test_mon_gens3_temp.g_rly_cnt  is 'Relay Count (G-relay)';
comment on column tb_test_mon_gens3_temp.bat_rly_cnt  is 'Relay Count (BAT-relay)';
comment on column tb_test_mon_gens3_temp.grid_to_grid_rly_cnt  is 'Relay Count (Grid to Grid relay)';
comment on column tb_test_mon_gens3_temp.bat_pchrg_rly_cnt  is 'Relay Count (BAT-Precharge-relay)';
comment on column tb_test_mon_gens3_temp.bms_flag0 is 'BMS 운전상태';
comment on column tb_test_mon_gens3_temp.rack_v is '랙 전압 (BDC 전압)';
comment on column tb_test_mon_gens3_temp.rack_i is '랙 전류 (BDC 전류)';
-- comment on column tb_test_mon_gens3_temp.cell_max_v is '셀 최대 전압';
-- comment on column tb_test_mon_gens3_temp.cell_min_v is '셀 최소 전압';
-- comment on column tb_test_mon_gens3_temp.cell_avg_v is '셀 평균 전압';
-- comment on column tb_test_mon_gens3_temp.cell_max_t is '셀 최대 온도';
-- comment on column tb_test_mon_gens3_temp.cell_min_t  is '셀 최소 온도';
-- comment on column tb_test_mon_gens3_temp.cell_avg_t is '셀 평균 온도';
comment on column tb_test_mon_gens3_temp.error_info is '에러 정보';
comment on column tb_test_mon_gens3_temp.create_dt is '데이터 생성시간';
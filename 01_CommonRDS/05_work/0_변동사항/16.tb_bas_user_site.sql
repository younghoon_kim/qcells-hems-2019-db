-- auto-generated definition
create table tb_bas_user_site
(
    user_id   varchar(64)                                                not null,
    site_id   integer                                                    not null,
    create_id varchar(64)                                                not null,
    create_dt timestamp(6) with time zone default sys_extract_utc(now()) not null,
    update_id varchar(64),
    update_dt timestamp(6) with time zone,
    constraint pk_bas_user_site
        primary key (user_id, site_id)
);

comment on table tb_bas_user_site is '사용자와 site_id 정보를 관리한다.';
comment on column tb_bas_user_site.user_id is '사용자ID';
comment on column tb_bas_user_site.create_id is '등록자ID';
comment on column tb_bas_user_site.create_dt is '등록일시';
comment on column tb_bas_user_site.update_id is '수정자ID (암호화)';
comment on column tb_bas_user_site.update_dt is '수정일시';

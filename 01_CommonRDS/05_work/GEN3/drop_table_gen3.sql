--******************************************
-- ***    GEN3 적용중 DROP 되는 테이블 정리 ***
--*******************************************


--2020.12.01 younghoon 1. Drop예정 테이블은 테스트를 통한 안정화 이전까지 _drop을 접미사로 붙여 관리한다.

-- tb_opr_connection_info_pms를 새로만들며 사용되지 않는 tb_raw_login을 드랍
--DROP TABLE tb_raw_login;

--Notification 관련 테이블 TSDB로 이관
--DROP TABLE tb_opr_alarm;
--DROP TABLE tb_opr_alarm_hist;

ALTER TABLE tb_raw_login RENAME TO tb_raw_login_drop;
ALTER TABLE tb_opr_alarm RENAME TO tb_opr_alarm_drop;
ALTER TABLE tb_opr_alarm_hist RENAME TO tb_opr_alarm_hist_drop;
ALTER TABLE tb_bas_model_alarm_code_map RENAME TO tb_bas_model_alarm_code_map_drop;
-- 질문, tb_raw_login_m2m은 계속필요한것인가?
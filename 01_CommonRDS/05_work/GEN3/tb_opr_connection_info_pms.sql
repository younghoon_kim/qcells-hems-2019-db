CREATE TABLE tb_opr_connection_info_pms(
  device_id varchar(50) not null,
  colec_dt timestamp(6) with time zone,
  response_cd varchar(4), --not null인지
  ems_tm  varchar(14) not null,
  ems_ipaddress varchar(30),
  ems_out_ipaddress varchar(30),
  update_dt timestamp(6) with time zone,
  constraint pk_tb_opr_connection_info_pms
      primary key(device_id,colec_dt)
);
COMMENT ON TABLE tb_opr_connection_info_pms IS 'PMS 연결정보 저장';
COMMENT ON COLUMN tb_opr_connection_info_pms.device_id IS 'PMS 장비 ID';
COMMENT ON COLUMN tb_opr_connection_info_pms.colec_dt IS '수집시간';
COMMENT ON COLUMN tb_opr_connection_info_pms.response_cd IS
    'ESS로 전송한 결과코드
    0:Online(정상상태)'
    '1:Unknown UID'
    '2:Password Error'
    '3:UID,Password Error'
    '128:Session Closed';
COMMENT ON COLUMN tb_opr_connection_info_pms.ems_tm IS 'EMS시간';
COMMENT ON COLUMN tb_opr_connection_info_pms.ems_ipaddress IS 'EMS Local IP Address';
COMMENT ON COLUMN tb_opr_connection_info_pms.ems_out_ipaddress IS 'EMS Public IP Address';
COMMENT ON COLUMN tb_opr_connection_info_pms.update_dt IS '업데이트 시간';


--**********************************************
-- ***   장비에서 보내주는 Configuration 정보 이력 테이블 ***
--**********************************************

--drop table if exists tb_opr_ess_config_mon_gen3_hist_v2;
CREATE TABLE tb_opr_ess_config_mon_gen3_hist_v2(
   device_id varchar(50) not null,
   device_type_cd varchar(4) not null,
   --GridCode
   grid_code int,
   --UserSettings
   backup_soc numeric,
   --BasicSettings
        --EnergyPolicy
        energy_policy int,
        --Inverter
       inverter_limit int,
       inverter_grid_target_freq int,
       inverter_EGmode_enable int,
       inverter_PSmode_select_enable int,
       inverter_ESrandomdelay_enable int,
       inverter_MENsystem_enable int,
        --PV
        pv_type int,
        installed_pv_stringcount int,
        installed_pv1_stringpower int,
        installed_pv2_stringpower int,
        installed_pv3_stringpower int,
        installed_pv4_stringpower int,
        installed_pv5_stringpower int,
        pv_feed_in_limit int,
        --Meter
        meter_model int,
        meter_connection int,
        meter_tcp_ipaddress varchar(20),
        meter_tcp_port int,
        meter_rtu_baudrate int,
        meter_rtu_parity varchar(4),
        meter_rtu_dataBits int,
        meter_rtu_stopBits int,
        meter_loadfromgw int,
        --Battery
        battery_operating_rackcount int,
        battery_installed_rackcount int,
        --ExternalControl
        externalcontrol_gateway_connection int,
        --TOU
        tou_count int,
        section1_time varchar(20),
        section1_action int,
        section1_battery_ref int,
        section1_tariff int,
        section2_time varchar(20),
        section2_action int,
        section2_battery_ref int,
        section2_tariff int,
        section3_time varchar(20),
        section3_action int,
        section3_battery_ref int,
        section3_tariff int,
        section4_time varchar(20),
        section4_action int,
        section4_battery_ref int,
        section4_tariff int,
        section5_time varchar(20),
        section5_action int,
        section5_battery_ref int,
        section5_tariff int,
        section6_time varchar(20),
        section6_action int,
        section6_battery_ref int,
        section6_tariff int,
    --EngineerSettings
        --PCS_ExternalEMS
        PCS_externalEMS_pcs_connection int,
        PCS_externalEMS_debugmode_enable int,
        --Hysteresis
       hysteresis_low numeric,
       hysteresis_high numeric,
        --BatteryUserSOC
        battery_usersoc_min int,
        battery_usersoc_max int,
        --DebugInfo
        debuginfo_processmgr int,
        debuginfo_systemlog int,
        debuginfo_fota int,
        debuginfo_powercontrol int,
        debuginfo_algorithmmgr int,
        debuginfo_essmgr int,
        debuginfo_dcsourcemgr int,
        debuginfo_cloudmgr int,
        debuginfo_metermgr int,
        debuginfo_gatewaymgr int,
        debuginfo_datamgr int,
        debuginfo_dbmgr int,
        debuginfo_webengine int,
        --UpdateSettings
        PCS_updateset_autofwupdate_enable int,
    --AdvancedSettings
        --GridControl
            --OverVoltage
            gc_over_voltage_level1 numeric,
            gc_over_voltage_time1 numeric,
            gc_over_voltage_level2 numeric,
            gc_over_voltage_time2 numeric,
            gc_over_voltage_level3 numeric,
            gc_over_voltage_time3 numeric,
            gc_over_voltage_level4 numeric,
            gc_over_voltage_time4 numeric,
            gc_over_voltage_release_level numeric,
            gc_over_voltage_release_time int,
            --TenMinVoltage
            gc_10min_voltage_level numeric,
            --UnderVoltage
            gc_under_voltage_level1 numeric,
            gc_under_voltage_time1 numeric,
            gc_under_voltage_level2 numeric,
            gc_under_voltage_time2 numeric,
            gc_under_voltage_level3 numeric,
            gc_under_voltage_time3 int,
            gc_under_voltage_level4 numeric,
            gc_under_voltage_time4 int,
            gc_under_voltage_release_level numeric,
            gc_under_voltage_release_time  int,
            --OverFrequency
            gc_over_frequency_level1 numeric,
            gc_over_frequency_time1 int,
            gc_over_frequency_level2 numeric,
            gc_over_frequency_time2 int,
            gc_over_frequency_level3 numeric,
            gc_over_frequency_time3 int,
            gc_over_frequency_level4 numeric,
            gc_over_frequency_time4 int,
            gc_over_frequency_level5 numeric,
            gc_over_frequency_time5 int,
            gc_over_frequency_release_level numeric,
            gc_over_frequency_release_time  int,
            --UnderFrequency
            gc_under_frequency_level1 numeric,
            gc_under_frequency_time1 int,
            gc_under_frequency_level2 numeric,
            gc_under_frequency_time2 int,
            gc_under_frequency_level3 numeric,
            gc_under_frequency_time3 int,
            gc_under_frequency_level4 numeric,
            gc_under_frequency_time4 int,
            gc_under_frequency_level5 numeric,
            gc_under_frequency_time5 int,
            gc_under_frequency_release_level numeric,
            gc_under_frequency_release_time  int,

        --GridControlActive
            --ActivePowerSetPoint
            gc_active_power_setpoint_enable int,
            gc_active_power_setpoint_value numeric,
            --ActivePowerFrequency
            gc_active_power_frequency_enable int,
            gc_active_power_frequency_slope_select int,
            gc_active_power_frequency_sl numeric,
            gc_active_power_frequency_sh numeric,
            gc_active_power_frequency_x1 numeric,
            gc_active_power_frequency_x2 numeric,
            gc_active_power_frequency_x3 numeric,
            gc_active_power_frequency_x4 numeric,
            gc_active_power_frequency_x5 numeric,
            gc_active_power_frequency_x6 numeric,
            gc_active_power_frequency_response_time int,
            --ActivePowerHysteresis
            gc_active_power_hysteresis_select_enable int,
            gc_active_power_hysteresis_of_pref_select int,
            gc_active_power_hysteresis_of_frequency numeric,
            gc_active_power_hysteresis_of_time int,
            gc_active_power_hysteresis_uf_pref_select int,
            gc_active_power_hysteresis_uf_frequency numeric,
            gc_active_power_hysteresis_uf_time int,
            --ActivePowerVoltage
            gc_active_power_voltage_enable int,
            gc_active_power_voltage_curve_select int,
            gc_active_power_voltage_x1 numeric,
            gc_active_power_voltage_y1 numeric,
            gc_active_power_voltage_x2 numeric,
            gc_active_power_voltage_y2 numeric,
            gc_active_power_voltage_x3 numeric,
            gc_active_power_voltage_y3 numeric,
            gc_active_power_voltage_x4 numeric,
            gc_active_power_voltage_y4 numeric,
            gc_active_power_voltage_response_time int,
        --GridControlReactive
            --PT1FilterbehaviorTau

            --2020.12.29 younghoon v4.0에서 사라짐
            --gc_reactive_pt1_filter_tau numeric,
            --ReactivePowerCospi_SetPoint
            gc_reactive_power_setpoint_enable numeric,
            gc_reactive_power_setpoint_excited numeric,
            gc_reactive_power_setpoint_value numeric,
            gc_reactive_power_setpoint_response_time int,
            --ReactivePowerCospi_P
            gc_reactive_power_cospi_p_enable numeric,
            gc_reactive_power_cospi_p_excited numeric,
            gc_reactive_power_cospi_p_x1 numeric,
            gc_reactive_power_cospi_p_y1 numeric,
            gc_reactive_power_cospi_p_x2 numeric,
            gc_reactive_power_cospi_p_y2 numeric,
            gc_reactive_power_cospi_p_x3 numeric,
            gc_reactive_power_cospi_p_y3 numeric,
            gc_reactive_power_cospi_p_response_time numeric,
            --ReactivePower_Q_SetPoint
            gc_reactive_power_q_setpoint_enable numeric,
            gc_reactive_power_q_setpoint_excited numeric,
            gc_reactive_power_q_setpoint_value numeric,
            gc_reactive_power_q_setpoint_response_time int,
            --ReactivePower_Q_U
            gc_reactive_power_q_u_enable numeric,
            gc_reactive_power_q_u_curve_select int,
            gc_reactive_power_q_u_fixed_vref_select_flag int,
            gc_reactive_power_q_u_vref_voltage numeric,
            gc_reactive_power_q_u_auto_vref_avg_time int,
            gc_reactive_power_q_u_x1 numeric,
            gc_reactive_power_q_u_y1 numeric,
            gc_reactive_power_q_u_x2 numeric,
            gc_reactive_power_q_u_y2 numeric,
            gc_reactive_power_q_u_x3 numeric,
            gc_reactive_power_q_u_y3 numeric,
            gc_reactive_power_q_u_x4 numeric,
            gc_reactive_power_q_u_y4 numeric,
            gc_reactive_power_q_u_response_time int,
            --ReactivePower_Q_P
            gc_reactive_power_q_p_enable int,
            gc_reactive_power_q_p_x1gen numeric,
            gc_reactive_power_q_p_y1gen numeric,
            gc_reactive_power_q_p_x2gen numeric,
            gc_reactive_power_q_p_y2gen numeric,
            gc_reactive_power_q_p_x3gen numeric,
            gc_reactive_power_q_p_y3gen numeric,
            gc_reactive_power_q_p_x1load numeric,
            gc_reactive_power_q_p_y1load numeric,
            gc_reactive_power_q_p_x2load numeric,
            gc_reactive_power_q_p_y2load numeric,
            gc_reactive_power_q_p_x3load numeric,
            gc_reactive_power_q_p_y3load numeric,
            gc_reactive_power_q_p_response_time int,
        --DCInjectionControl
        --GridControl 하위항목이 아님. 수정해야할까? ->우선 원래 쓰던 이름 사용
        gc_dc_injection_control_enable numeric,
        gc_dc_injection_control_faultlevel1 numeric,
        gc_dc_injection_control_faulttime1 numeric,
        gc_dc_injection_control_faultlevel2 numeric,
        gc_dc_injection_control_faulttime2 numeric,
        gc_dc_injection_control_control_enable int,
        gc_dc_injection_control_control_level numeric,

        --RCMUControl
            --Sudden
            gc_rcmu_control_sudden_enable numeric,
            gc_rcmu_control_sudden_level1 numeric,
            gc_rcmu_control_sudden_time1 numeric,
            gc_rcmu_control_sudden_level2 numeric,
            gc_rcmu_control_sudden_time2 numeric,
            gc_rcmu_control_sudden_level3 numeric,
            gc_rcmu_control_sudden_time3 numeric,
            --Continuous
            gc_rcmu_control_continuous_enable numeric,
            gc_rcmu_control_continuous_level1 numeric,
            gc_rcmu_control_continuous_time1 numeric,
        --PVInsulationControl
        gc_pv_insulation_control_enable numeric,
        gc_pv_insulation_control_faultlevel numeric,
        gc_pv_insulation_control_checkcnt numeric,
        gc_pv_insulation_control_retrytime int,
        --AntilsLandingControl
        gc_antiislanding_control_active_detect_enable int,
        gc_antiislanding_control_passive_detect_enable int,
        gc_antiislanding_control_freqfeedback_enable int,
        gc_antiislanding_control_stepinjection_enable int,
        gc_antiislanding_control_reactive_suppresion_enable int,
        gc_antiislanding_control_antiislandinggain1 int,
        gc_antiislanding_control_antiislandinggain2 int,
        gc_antiislanding_control_freqfeedback_qmax numeric,
        gc_antiislanding_control_stepinjection_qmax numeric,
        --GradientControl
        gc_gradient_control_enable numeric,
        gc_gradient_control_energy_source_change_enable numeric,
        gc_gradient_control_unitrefselect int,
        gc_gradient_control_timelevel numeric,
        gc_gradient_control_activepowersetpointgradient numeric,
        gc_gradient_control_activepowergradient numeric,
        --DeratingControl
        gc_derating_control_enable numeric,
        gc_derating_control_start_temprature numeric,--기존에도 철자가 틀리게되어있어 일단 e가없는상태로 생성
        gc_derating_control_value numeric,
        --FeedinRelay
        feed_in_relay_enable int,
        feed_in_relay_relay_1_attach_level int,
        feed_in_relay_relay_1_detach_level int,
        feed_in_relay_relay_2_attach_level int,
        feed_in_relay_relay_2_detach_level int,
        feed_in_relay_relay_3_attach_level int,
        feed_in_relay_relay_3_detach_level int,
        feed_in_relay_relay_4_attach_level int,
        feed_in_relay_relay_4_detach_level int,
        --ReConnectionTime
        reconnection_time int,
        --TauCoefficient
        tau_coefficient numeric,
        --InverterControl
        invertercontrol_grid_targetvoltage numeric,
        invertercontrol_grid_targetvoltage_offset numeric,
        invertercontrol_pcs_max_powerlimit int,
        invertercontrol_VAR_action int,
        invertercontrol_pcs_varmax_q1 int,
        invertercontrol_pcs_varmax_q2 int,
        invertercontrol_pcs_varmax_q3 int,
        invertercontrol_pcs_varmax_q4 int,
        invertercontrol_pcs_pfmin_q1 numeric,
        invertercontrol_pcs_pfmin_q2 numeric,
        invertercontrol_pcs_pfmin_q3 numeric,
        invertercontrol_pcs_pfmin_q4 numeric,
        --DynamicReactiveCurrent
        dynamic_reactivecurrent_enable int,
        dynamic_reactivecurrent_argramod int,
        dynamic_reactivecurrent_argrasag numeric,
        dynamic_reactivecurrent_argraswell numeric,
        dynamic_reactivecurrent_dbv_min numeric,
        dynamic_reactivecurrent_dbv_max numeric,
        --FRT
        frt_hvrt_enable int,
        frt_lvrt_enable int,
        frt_hfrt_enable int,
        frt_lfrt_enable int,
        frt_hvrt_voltage numeric,
        frt_lvrt_voltage numeric,
        frt_hfrt_voltage numeric,
        frt_lfrt_voltage numeric,
        --VoltageRiseSuppression
        voltagerise_suppression_enable int,
        voltagerise_suppression_vref numeric,
        voltagerise_suppression_delaytime numeric,
        voltagerise_suppression_max_pf numeric,
        voltagerise_suppression_pf_responsetime numeric,
        voltagerise_suppression_minactive_power numeric,
        voltagerise_suppression_activepower_responsetime numeric,
        voltagerise_suppression_releasetime numeric,
        update_dt timestamp(6) with time zone,
        update_id varchar(64) not null,
        base_profile_cd integer,
   constraint pk_opr_ess_config_mon_gen3_hist_v2
      primary key (device_id, device_type_cd)
);

COMMENT ON TABLE tb_opr_ess_config_mon_gen3_hist_v2 IS 'ESS 의 설정 정보 모니터링 상태 테이블';

COMMENT ON COLUMN  tb_opr_ess_config_mon_gen3_hist_v2.device_id IS '장비ID';

COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.device_type_cd IS '장비유형코드';
--GridCode
COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.grid_code IS 'Grid Code';
--UserSettings
COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.backup_soc IS '0%(default),25%,50%,75%,100%' ;

--BasicSettings
    --EnergyPolicy
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.energy_policy IS '"0: Stanby
    1: Self consumption
    2. Zero Export Mode
    3. Time-based Mode
    4. External Generation Mode"' ;
    --Inverter
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.inverter_limit IS '정수값, 3600, 4600, 5000, 8000 모델별로 상이' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.inverter_grid_target_freq IS '50.00 or 60.00 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.inverter_EGmode_enable IS '0: Disable1: Enable';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.inverter_PSmode_select_enable IS '0: Not operation 1: Operation' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.inverter_ESrandomdelay_enable IS '0: Disable1: Enable';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.inverter_MENsystem_enable IS '0: Disable1: Enable';
    --PV
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.pv_type IS '"0: String Inverter
    1: Micro Inverter
    2: Power Optimizers"
    ' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.installed_pv_stringcount IS '0 ~ 5' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.installed_pv1_stringpower IS '0 ~ 5000W' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.installed_pv2_stringpower IS '0 ~ 5000W' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.installed_pv3_stringpower IS '0 ~ 5000W' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.installed_pv4_stringpower IS '0 ~ 5000W' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.installed_pv5_stringpower IS '0 ~ 5000W' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.pv_feed_in_limit IS '-10 ~ 100 %' ;
    --Meter
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.meter_model IS
    '"0:없음
    4:EM112 (CarloGavazzi)
    5:EM24 (CarloGavazzi)
    99:Custom"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.meter_connection IS
        '"0: Disable
    1: Modbus TCP
    2: Modbus RTU"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.meter_tcp_ipaddress IS 'TCP IP주소' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.meter_tcp_port IS '0 ~ 65535' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.meter_rtu_baudrate IS '4600, 9600, 19200, 38400, 57600, 115200' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.meter_rtu_parity IS 'N'' or ''E'' or ''O''' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.meter_rtu_dataBits IS '5 ~ 8' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.meter_rtu_stopBits IS '1 ~ 2' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.meter_loadfromgw IS '"0 : Meter Load 정보를 EMS 와 연결된 Meter 로부터 전달 받아 사용함. (기본)1 : Meter Load 정보를 3rd GW 로부터 전달 받아 사용함."' ;
    --Battery
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.battery_operating_rackcount IS '0~3' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.battery_installed_rackcount IS '0~3' ;
    --ExternalControl
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.externalcontrol_gateway_connection IS
        '"0: Disable
    1: Modbus TCP
    2: Modbus RTU"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.tou_count IS
        '"tou 카운트"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section1_time IS
        '"HH:MM 형태"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section1_action IS
        '"0: NOT / 1: Charge / 2: Discarhge / 3: Auto"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section1_battery_ref IS
        '"Target battery ref 값 (단위: W)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section1_tariff IS
        '"요금 정보, 통화 포함 X (Ex. 10$이면 10 내려옴)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section2_time IS
        '"HH:MM 형태"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section2_action IS
        '"0: NOT / 1: Charge / 2: Discarhge / 3: Auto"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section2_battery_ref IS
        '"Target battery ref 값 (단위: W)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section2_tariff IS
        '"요금 정보, 통화 포함 X (Ex. 10$이면 10 내려옴)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section3_time IS
        '"HH:MM 형태"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section3_action IS
        '"0: NOT / 1: Charge / 2: Discarhge / 3: Auto"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section3_battery_ref IS
        '"Target battery ref 값 (단위: W)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section3_tariff IS
        '"요금 정보, 통화 포함 X (Ex. 10$이면 10 내려옴)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section4_time IS
        '"HH:MM 형태"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section4_action IS
        '"0: NOT / 1: Charge / 2: Discarhge / 3: Auto"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section4_battery_ref IS
        '"Target battery ref 값 (단위: W)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section4_tariff IS
        '"요금 정보, 통화 포함 X (Ex. 10$이면 10 내려옴)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section5_time IS
        '"HH:MM 형태"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section5_action IS
        '"0: NOT / 1: Charge / 2: Discarhge / 3: Auto"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section5_battery_ref IS
        '"Target battery ref 값 (단위: W)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section5_tariff IS
        '"요금 정보, 통화 포함 X (Ex. 10$이면 10 내려옴)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section6_time IS
        '"HH:MM 형태"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section6_action IS
        '"0: NOT / 1: Charge / 2: Discarhge / 3: Auto"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section6_battery_ref IS
        '"Target battery ref 값 (단위: W)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.section6_tariff IS
        '"요금 정보, 통화 포함 X (Ex. 10$이면 10 내려옴)"' ;


--EngineerSettings
    --PCS_ExternalEMS
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.PCS_externalEMS_pcs_connection IS
        '"0: Disable
    1: CAN (QCELL PCS)
    2: Modbus TCP (External EMS)
    3: Modbus RTU (External EMS)"' ;

    --Hysteresis
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.hysteresis_low IS 'Battery SOC가 5% 이하일 때 설정한 Low hysteresis SOC (10%,15%(default),20%,25%) 까지 방전하는 것이 불가능하다' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.hysteresis_high IS 'Battery SOC가 95% 이상일 때 설정한 High hysteresis SOC (90%(default),85%,80%) 까지 충전하는 것이 불가능하다' ;
    --BatteryUserSoc
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.battery_usersoc_min IS '해당 값은 실제 Battery SOC값이며 설정된 값이 User SOC의 0%임' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.battery_usersoc_max IS '해당 값은 실제 Battery SOC값이며 설정된 값이 User SOC의 100%임' ;
    --DebugInfo
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.debuginfo_processmgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.debuginfo_systemlog IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.debuginfo_fota IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.debuginfo_powercontrol IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.debuginfo_algorithmmgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.debuginfo_essmgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.debuginfo_dcsourcemgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.debuginfo_cloudmgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.debuginfo_metermgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.debuginfo_gatewaymgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.debuginfo_datamgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.debuginfo_dbmgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.debuginfo_webengine IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;

--Advanced Settings
    --GridControl
        --Overvoltage
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_voltage_level1 IS '100.0 ~ 350.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_voltage_time1 IS '0 ~ 65535 [msec]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_voltage_level2 IS '100.0 ~ 350.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_voltage_time2 IS '0 ~ 65535 [msec]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_voltage_level3 IS '100.0 ~ 350.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_voltage_time3 IS '0 ~ 65535 [msec]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_voltage_level4 IS '100.0 ~ 350.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_voltage_time4 IS '0 ~ 65535 [msec]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_voltage_release_level IS '100.0 ~ 350.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_voltage_release_time IS '0 ~ 65535 [msec]' ;

        --TenminVoltage
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_10min_voltage_level IS '100.0 ~ 350.0 [V]' ;

        --UnderVoltage
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_voltage_level1 IS '100.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_voltage_time1 IS '0 ~ 65535 [msec]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_voltage_level2 IS '100.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_voltage_time2 IS '0 ~ 65535 [msec]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_voltage_level3 IS '100.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_voltage_time3 IS '0 ~ 65535 [msec]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_voltage_level4 IS '100.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_voltage_time4 IS '0 ~ 65535 [msec]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_voltage_release_level IS '100.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_voltage_release_time IS '0 ~ 65535 [msec]' ;

    --OverFrequency
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_frequency_level1 IS '50.000~65.000 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_frequency_time1 IS '0 ~ 100000 [msec]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_frequency_level2 IS '50.000~65.000 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_frequency_time2 IS '0 ~ 100000 [msec]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_frequency_level3 IS '50.000~65.000 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_frequency_time3 IS '0 ~ 100000 [msec]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_frequency_level4 IS '50.000~65.000 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_frequency_time4 IS '0 ~ 100000 [msec]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_frequency_level5 IS '50.000~65.000 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_frequency_time5 IS '0 ~ 100000 [msec]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_frequency_release_level IS '50.000~65.000 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_over_frequency_release_time IS '0 ~ 100000 [msec]' ;

    --Underfrequency
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_frequency_level1 IS '45.000~60.000 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_frequency_time1 IS '0 ~ 100000 [msec]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_frequency_level2 IS '45.000~60.000 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_frequency_time2 IS '0 ~ 100000 [msec]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_frequency_level3 IS '45.000~60.000 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_frequency_time3 IS '0 ~ 100000 [msec]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_frequency_level4 IS '45.000~60.000 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_frequency_time4 IS '0 ~ 100000 [msec]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_frequency_level5 IS '45.000~60.000 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_frequency_time5 IS '0 ~ 100000 [msec]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_frequency_release_level IS '45.000~60.000 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_under_frequency_release_time IS '0 ~ 100000 [msec]' ;



    --GridControlActive
        --ActivePowerSetPoint
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_setpoint_enable IS '0: Disable, 1: Enable' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_setpoint_value IS '0.0 ~ 100.0 [%]' ;
        --ActivePowerFrequency
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_frequency_enable IS '0: Disable, 1: Enable' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_frequency_slope_select IS '0: Curve, 1: Slope' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_frequency_sl IS '-100.0 ~ 100.0 [%]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_frequency_sh IS '-100.0 ~ 100.0 [%]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_frequency_x1 IS '45.000 ~ 60.000 [Hz]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_frequency_x2 IS '45.000 ~ 60.000 [Hz]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_frequency_x3 IS '45.000 ~ 60.000 [Hz]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_frequency_x4 IS '45.000 ~ 60.000 [Hz]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_frequency_x5 IS '45.000 ~ 60.000 [Hz]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_frequency_x6 IS '45.000 ~ 60.000 [Hz]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_frequency_response_time IS '0 ~ 60000 [msec]' ;
        --ActivePowerHysteresis
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_hysteresis_select_enable  IS '0: Pref, 1: Pmax' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_hysteresis_of_pref_select IS '0: Pref, 1: Pmax' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_hysteresis_of_frequency IS '50.000 ~ 65.000 [Hz]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_hysteresis_of_time IS '0 ~ 60000 [msec]' ;

        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_hysteresis_uf_pref_select IS '0: Pref, 1: Pmax' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_hysteresis_uf_frequency IS '50.000 ~ 65.000 [Hz]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_hysteresis_uf_time IS '0 ~ 60000 [msec]' ;

        --ActivePowerVoltage
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_voltage_enable IS '0: Disable, 1: Enable' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_voltage_curve_select IS '0: Curve, 1: Target' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_voltage_x1 IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_voltage_y2 IS '-100.0 ~ 100.0 [%]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_voltage_x2 IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_voltage_y2 IS '-100.0 ~ 100.0 [%]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_voltage_x3 IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_voltage_y3 IS '-100.0 ~ 100.0 [%]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_voltage_x4 IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_voltage_y4 IS '-100.0 ~ 100.0 [%]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_active_power_voltage_response_time IS '0 ~ 60000 [msec]' ;

    --GridControlReactive
        --PT1FilterBehaviorTau
        --COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_pt1_filter_tau IS '0 ~ 60000 [msec]' ;
        --ReactivePowerCospi_SetPoint
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_setpoint_enable IS '0: Disable, 1: Enable' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_setpoint_excited IS '0: Under, 1: Over' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_setpoint_value IS '0.00 ~ 1.00 [역률]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_setpoint_response_time IS '0 ~ 60000 [msec]' ;
        --ReactivePowerCospi_P
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_cospi_p_enable IS '0: Disable, 1: Enable' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_cospi_p_excited IS '0: Under, 1: Over' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_cospi_p_x1 IS '0.0 ~ 100.0 [%]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_cospi_p_y1 IS '0.00 ~ 1.00 [역률]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_cospi_p_x2 IS '0.0 ~ 100.0 [%]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_cospi_p_y2 IS '0.00 ~ 1.00 [역률]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_cospi_p_x3 IS '0.0 ~ 100.0 [%]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_cospi_p_y3 IS '0.00 ~ 1.00 [역률]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_cospi_p_response_time IS '0 ~ 60000 [msec]' ;
        --ReactivePower_Q_SetPoint
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_setpoint_enable IS '0: Disable, 1: Enable' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_setpoint_excited IS '0: Under, 1: Over' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_setpoint_value IS '0.00 ~ 60.00 [%(Q/S)]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_setpoint_response_time IS '0 ~ 60000 [msec]' ;
        --ReactivePower_Q_U
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_enable IS '0: Disable, 1: Enable' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_curve_select IS '0: Curve, 1: Target' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_fixed_vref_select_flag IS '0: Disable, 1: Enable' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_vref_voltage IS '0 ~ 300.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_auto_vref_avg_time IS '0 ~ 50000 [msec]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_x1 IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_y1 IS '-60.00 ~ 60.00 [%(Q/S)]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_x2 IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_y2 IS '-60.00 ~ 60.00 [%(Q/S)]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_x3 IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_y3 IS '-60.00 ~ 60.00 [%(Q/S)]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_x4 IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_y4 IS '-60.00 ~ 60.00 [%(Q/S)]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_u_response_time IS '0 ~ 60000 [msec]' ;
        --ReactivePower_Q_P
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_enable IS '0: Disable, 1: Enable' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_x1gen IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_y1gen IS '-60.00 ~ 60.00 [%(Q/S)]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_x2gen IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_y2gen IS '-60.00 ~ 60.00 [%(Q/S)]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_x3gen IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_y3gen IS '-60.00 ~ 60.00 [%(Q/S)]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_x1load IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_y1load IS '-60.00 ~ 60.00 [%(Q/S)]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_x2load IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_y2load IS '-60.00 ~ 60.00 [%(Q/S)]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_x3load IS '0.0 ~ 240.0 [V]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_y3load IS '-60.00 ~ 60.00 [%(Q/S)]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_reactive_power_q_p_response_time IS '0 ~ 60000 [msec]' ;
    --DCInjectionControl
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_dc_injection_control_enable IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_dc_injection_control_faultlevel1 IS '0.00 ~ 1.00 [A]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_dc_injection_control_faulttime1 IS '0 ~ 10000 [msec]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_dc_injection_control_faultlevel2 IS '0.00 ~ 1.00 [A]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_dc_injection_control_faulttime2 IS '0 ~ 10000 [msec]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_dc_injection_control_control_enable IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_dc_injection_control_control_level IS '0.00 ~ 1.00 [A]' ;
    --RCMUControl
        --Sudden
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_rcmu_control_sudden_enable IS '0: Disable, 1: Enable' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_rcmu_control_sudden_level1 IS '0 ~ 500 [mA]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_rcmu_control_sudden_time1 IS '0 ~ 500 [msec]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_rcmu_control_sudden_level2 IS '0 ~ 500 [mA]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_rcmu_control_sudden_time2 IS '0 ~ 500 [msec]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_rcmu_control_sudden_level3 IS '0 ~ 500 [mA]' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_rcmu_control_sudden_time3 IS '0 ~ 500 [msec]' ;
        --Continuous
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_rcmu_control_continuous_enable IS '0: Disable, 1: Enable' ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_rcmu_control_continuous_level1 IS '0 ~ 500 [mA]'  ;
        COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_rcmu_control_continuous_time1 IS '0 ~ 500 [msec]' ;
    --PVInsulationControl
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_pv_insulation_control_enable IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_pv_insulation_control_faultlevel IS '0.0 ~ 1000.0 [KOhm]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_pv_insulation_control_checkcnt IS '1 ~ 100 [Cnt]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_pv_insulation_control_retrytime IS '0 ~ 100 [Min]' ;
    --AntilslandingControl
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_antiislanding_control_active_detect_enable IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_antiislanding_control_passive_detect_enable IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_antiislanding_control_freqfeedback_enable IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_antiislanding_control_stepinjection_enable IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_antiislanding_control_reactive_suppresion_enable IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_antiislanding_control_antiislandinggain1 IS '0 ~ 100000' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_antiislanding_control_antiislandinggain2 IS '0 ~ 100000' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_antiislanding_control_freqfeedback_qmax IS '0.00 ~ 1.00 [p.u.]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_antiislanding_control_stepinjection_qmax IS '0.00 ~ 1.00 [p.u.]' ;
    --GradientControl
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_gradient_control_enable IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_gradient_control_energy_source_change_enable IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_gradient_control_unitrefselect IS '0: %Irated / 1: %Pmax';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_gradient_control_timelevel IS '60 ~ 1200 [sec]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_gradient_control_activepowersetpointgradient  IS '0.00 ~ 100.00 [Pmax/sec]' ;
    --DeratingControl
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_derating_control_enable IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_derating_control_start_temprature IS '0.0 ~ 150.0 [℃]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.gc_derating_control_value IS '0.0 ~ 60.0 [%]' ;
    --FeedinRelay
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.feed_in_relay_enable IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.feed_in_relay_relay_1_attach_level IS '0 ~ 10000 [W]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.feed_in_relay_relay_1_detach_level IS '0 ~ 10000 [W]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.feed_in_relay_relay_2_attach_level IS '0 ~ 10000 [W]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.feed_in_relay_relay_2_detach_level IS '0 ~ 10000 [W]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.feed_in_relay_relay_3_attach_level IS '0 ~ 10000 [W]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.feed_in_relay_relay_3_detach_level IS '0 ~ 10000 [W]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.feed_in_relay_relay_4_attach_level IS '0 ~ 10000 [W]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.feed_in_relay_relay_4_detach_level IS '0 ~ 10000 [W]' ;
    --ReconnectionTime
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.reconnection_time IS '0 ~ 65535 [sec]' ;
    --TauCoefficient
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.tau_coefficient IS '0.0 ~ 6.0 [Tau]' ;
    --tb_opr_ess_config_mon_gen3_hist_v2.inverterControl
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.invertercontrol_grid_targetvoltage IS '0.0 ~ 300.0 [V]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.invertercontrol_grid_targetvoltage_offset IS '-300.0 ~ 300.0 [V]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.invertercontrol_pcs_max_powerlimit IS '-10000 ~ 10000 [VA]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.invertercontrol_VAR_action  IS'VAR action on change between charging and discharging: ';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.invertercontrol_pcs_varmax_q1  IS '-10000 ~ 10000 [VAr]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.invertercontrol_pcs_varmax_q2  IS '-10000 ~ 10000 [VAr]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.invertercontrol_pcs_varmax_q3  IS '-10000 ~ 10000 [VAr]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.invertercontrol_pcs_varmax_q4  IS '-10000 ~ 10000 [VAr]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.invertercontrol_pcs_pfmin_q1  IS '-1.00 ~ 1.00 [역률]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.invertercontrol_pcs_pfmin_q2  IS '-1.00 ~ 1.00 [역률]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.invertercontrol_pcs_pfmin_q3  IS '-1.00 ~ 1.00 [역률]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.invertercontrol_pcs_pfmin_q4  IS '-1.00 ~ 1.00 [역률]';
    --dynamicReactiveCurrent
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.dynamic_reactivecurrent_enable  IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.dynamic_reactivecurrent_argramod IS  '0: Undefined / 1: Basic Behavior / 2: Alternative Behavior' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.dynamic_reactivecurrent_argrasag  IS '0.00 ~ 60.00 [%ARtg/%dV]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.dynamic_reactivecurrent_argraswell  IS '0.00 ~ 60.00 [%ARtg/%dV]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.dynamic_reactivecurrent_dbv_min  IS '0.00 ~ 60.00 [%ARtg/%dV]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.dynamic_reactivecurrent_dbv_max  IS '0.00 ~ 60.00 [%ARtg/%dV]';
    --frt
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.frt_hvrt_enable  IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.frt_lvrt_enable  IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.frt_hfrt_enable  IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.frt_lfrt_enable  IS '0: Disable, 1: Enable' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.frt_hvrt_voltage  IS '100.0 ~ 350.0 [V]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.frt_lvrt_voltage  IS '0.0 ~ 240.0 [V]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.frt_hfrt_voltage  IS '50.000 ~ 65.000 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.frt_lfrt_voltage  IS '45.000 ~ 60.000 [Hz]' ;
    --voltageriseSuppression
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.voltagerise_suppression_enable  IS  '0: Disable, 1: Enable';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.voltagerise_suppression_vref  IS '100.0 ~ 350.0 [V]' ;
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.voltagerise_suppression_delaytime  IS '0.0 ~ 5000.0 [Sec]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.voltagerise_suppression_max_pf  IS '0.00 ~ 1.00 [역률]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.voltagerise_suppression_pf_responsetime  IS '0.0 ~ 5000.0 [Sec]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.voltagerise_suppression_minactive_power  IS  '0.00 ~ 100.00 [%]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.voltagerise_suppression_activepower_responsetime  IS '0.0 ~ 5000.0 [Sec]';
    COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.voltagerise_suppression_releasetime  IS '0.0 ~ 5000.0 [Sec]';



COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.update_dt IS '최종 업데이트 시간(최초에는 삽입시간)';
COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.update_id IS '업데이트 아이디';
COMMENT ON COLUMN tb_opr_ess_config_mon_gen3_hist_v2.base_profile_cd IS 'base가 되는 profile의 country_code';



--데이터 보관주기와 파티셔닝주기를 입력한다.
--INSERT INTO tb_plf_tbl_meta(sub_area, table_nm, table_type, arcv_flag, arcv_period, fst_load_dt, load_type, partition_type, std_col_nm, std_col_type, del_flag, del_cycle,create_dt)
-- VALUES('OPR','TB_OPR_ESS_CONFIG_MON_GEN3_HIST_V2','HIST','Y','12','20201118',
--                                   'Append','M','COLEC_DT','TIMESTAMP','Y','12',sys_extract_utcnow());

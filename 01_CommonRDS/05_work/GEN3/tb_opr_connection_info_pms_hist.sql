CREATE TABLE tb_opr_connection_info_pms_hist(
  device_id varchar(50) not null,
  colec_dt timestamp(6) with time zone,
  response_cd varchar(4), --not null인지
  ems_tm  varchar(14) not null,
  ems_ipaddress varchar(30),
  ems_out_ipaddress varchar(30),
  create_dt timestamp(6) with time zone,
    constraint pk_tb_opr_connection_info_pms_hist
	primary key (device_id, colec_dt)
) partition by RANGE (colec_dt);

COMMENT ON TABLE tb_opr_connection_info_pms_hist IS 'PMS연결정보에 대한 이력 저장';
COMMENT ON COLUMN tb_opr_connection_info_pms_hist.device_id IS 'PMS 장비 ID';
COMMENT ON COLUMN tb_opr_connection_info_pms_hist.colec_dt IS '수집시간';
COMMENT ON COLUMN tb_opr_connection_info_pms_hist.response_cd IS
    'ESS로 전송한 결과코드
    0:Online(정상상태)'
    '1:Unknown UID'
    '2:Password Error'
    '3:UID,Password Error'
    '128:Session Closed';
COMMENT ON COLUMN tb_opr_connection_info_pms_hist.ems_tm IS 'EMS시간';
COMMENT ON COLUMN tb_opr_connection_info_pms_hist.ems_ipaddress IS 'EMS Local IP Address';
COMMENT ON COLUMN tb_opr_connection_info_pms_hist.ems_out_ipaddress IS 'EMS Public IP Address';
COMMENT ON COLUMN tb_opr_connection_info_pms_hist.create_dt IS '이력 기록 시간';


-- 파티션 생성 배치실행을 위해 meta테이블에 삽입
INSERT INTO tb_plf_tbl_meta(sub_area, table_nm, table_type, arcv_flag, arcv_period, fst_load_dt, load_type, partition_type, std_col_nm, std_col_type,del_flag, del_cycle,create_dt)
VALUES('OPR','TB_OPR_CONNECTION_INFO_PMS_HIST','HIST','Y','1','20201118',
                                   'Append','D','COLEC_DT','TIMESTAMP','Y','31',sys_extract_utcnow());

-- **** 파티션 생성 예시 ****
/*
create table if not exists tb_opr_connection_info_pms_hist_202011
partition of tb_opr_connection_info_pms_hist
(
	constraint tb_opr_connection_info_pms_hist_202011_pkey
		primary key (colec_dt)
)
FOR VALUES FROM ('20201100') TO ('20201200');
*/


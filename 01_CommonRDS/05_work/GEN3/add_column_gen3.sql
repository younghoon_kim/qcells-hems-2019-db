-- tb_plf_firmware_info 에 펌웨어 hash 컬럼 추가.
alter table tb_plf_firmware_info
	add column ems_fw_hash varchar;

alter table tb_plf_firmware_info
	add column pcs_fw_hash varchar;

alter table tb_plf_firmware_info
	add column bms_fw_hash varchar;

comment on column tb_plf_firmware_info.ems_fw_hash is 'ems 펌웨어 hash 정보';
comment on column tb_plf_firmware_info.pcs_fw_hash is 'pcs 펌웨어 hash 정보';
comment on column tb_plf_firmware_info.bms_fw_hash is 'bms 펌웨어 hash 정보';

-- tb_plf_firmware_info_hist 에 펌웨어 hash 컬럼 추가.
alter table tb_plf_firmware_info_hist
	add column ems_fw_hash varchar;

alter table tb_plf_firmware_info_hist
	add column pcs_fw_hash varchar;

alter table tb_plf_firmware_info_hist
	add column bms_fw_hash varchar;

comment on column tb_plf_firmware_info_hist.ems_fw_hash is 'ems 펌웨어 hash 정보';
comment on column tb_plf_firmware_info_hist.pcs_fw_hash is 'pcs 펌웨어 hash 정보';
comment on column tb_plf_firmware_info_hist.bms_fw_hash is 'bms 펌웨어 hash 정보';

-- tb_bas_device_pms 에 ExternalControlInfo gateway model name 컬럼 추가.
alter table tb_bas_device_pms
    add column ECI_model_name varchar;

comment on column tb_bas_device_pms.ECI_model_name is 'ExternalControlInfo (ECI), 3rd party Gateway의 Model Name
- Energeybase XX
- SwichDin XX
- 기타 model name ';


--2020.11.19 younghoon
--tb_plf_device_fw_hist에 pcs,bms 업데이트 시간 컬럼 추가
ALTER TABLE TB_PLF_DEVICE_FW_HIST ADD COLUMN pcs_update_dt timestamp(6) with time zone;
COMMENT ON COLUMN  tb_plf_device_fw_hist.pcs_update_dt IS '최근 PCS 업데이트 시도 일자 (최초에는 초기 설치 일자) UTC';

ALTER TABLE TB_PLF_DEVICE_FW_HIST ADD COLUMN bms_update_dt timestamp(6) with time zone;
COMMENT ON COLUMN  tb_plf_device_fw_hist.bms_update_dt IS '최근 BMS 업데이트 시도 일자 (최초에는 초기 설치 일자) UTC';



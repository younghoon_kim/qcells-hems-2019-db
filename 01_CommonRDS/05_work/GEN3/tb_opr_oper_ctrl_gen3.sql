-- auto-generated definition
create table tb_opr_oper_ctrl_gen3
(
    grp_id                   varchar(50) not null
        constraint pk_opr_oper_ctrl_gen3
            primary key,
    reboot_system            varchar,
    change_operation_mode    int,
    COM_inverter_limit_power int,
    COM_battery_target_power int,
    COM_pv_limit_power       int,
    get_system_status        int,
    reverse_ssh_ip           varchar,
    reverse_ssh_port         int,
    dr_organization varchar(64),
    dr_mode int,
    rel_BMSprotect_1 int,
    rel_BMSprotect_2 int,
    rel_BMSprotect_3 int,
    create_id        varchar(64)                                                not null,
    create_dt        timestamp(6) with time zone default sys_extract_utc(now()) not null,
    update_id        varchar(64),
    update_dt        timestamp(6) with time zone default sys_extract_utc(now()) not null
);

comment on table tb_opr_oper_ctrl_gen3 is 'ESS 제어요청정보를 관리한다.(gen3)';

comment on column tb_opr_oper_ctrl_gen3.grp_id is '제어그룹 ID';

comment on column tb_opr_oper_ctrl_gen3.reboot_system is 'system reboot';

comment on column tb_opr_oper_ctrl_gen3.change_operation_mode is
    '0: NOP (No operation mode)
    1: Auto (PCS 자동 mode)
    2: Manual (Manual mode, 아래 3개의 값이 같이 전달되어야 함)
    3: Restore (Cloud에서 바꾸기 이전 원래 mode로 돌리기 위한 command)';

comment on column tb_opr_oper_ctrl_gen3.COM_inverter_limit_power is 'change_operation_mode의 설정값 중 일부, ChangeOperationMode가 Manual일 때만 유의미한 값';

comment on column tb_opr_oper_ctrl_gen3.COM_battery_target_power is 'change_operation_mode의 설정값 중 일부, ChangeOperationMode가 Manual일 때만 유의미한 값';

comment on column tb_opr_oper_ctrl_gen3.COM_pv_limit_power is 'change_operation_mode의 설정값 중 일부, ChangeOperationMode가 Manual일 때만 유의미한 값';

comment on column tb_opr_oper_ctrl_gen3.get_system_status is '현재까지 저장하고 있던 System log를 S3에 upload,현재 System(PMS)의 상태를 요청하는 기능';

comment on column tb_opr_oper_ctrl_gen3.reverse_ssh_ip is 'Reverse ssh 접속을 위한 IP 주소';

comment on column tb_opr_oper_ctrl_gen3.reverse_ssh_port is 'Reverse ssh 접속을 위한 Port 번호 (0~65535)';

comment on column tb_opr_oper_ctrl_gen3.create_id is '등록자ID';

comment on column tb_opr_oper_ctrl_gen3.create_dt is '등록일시';

comment on column tb_opr_oper_ctrl_gen3.update_id is '수정자ID';

comment on column tb_opr_oper_ctrl_gen3.update_dt is '수정일시';

comment on column tb_opr_oper_ctrl_gen3.dr_organization is 'auto, drm, fcas, diamond_energy';
comment on column tb_opr_oper_ctrl_gen3.dr_mode is 'drm의 경우 10진수 수 형태로 전달';

--BMS Protection을 release 시키기 위함
comment on column tb_opr_oper_ctrl_gen3.rel_BMSprotect_1 is '0: None / 1: Software Reset / 2: Power Reset / 3: Permanent Reset ';
comment on column tb_opr_oper_ctrl_gen3.rel_BMSprotect_2 is '0: None / 1: Software Reset / 2: Power Reset / 3: Permanent Reset ';
comment on column tb_opr_oper_ctrl_gen3.rel_BMSprotect_3 is '0: None / 1: Software Reset / 2: Power Reset / 3: Permanent Reset ';
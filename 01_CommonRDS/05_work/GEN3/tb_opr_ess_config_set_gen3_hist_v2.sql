--**********************************************
-- ***   장비에서 보내주는 Configuration 정보 이력 테이블 ***
--**********************************************

--drop table if exists tb_opr_ess_config_set_gen3_hist_v2;

CREATE TABLE tb_opr_ess_config_set_gen3_hist_v2(
   device_id varchar(50) not null,
   device_type_cd varchar(4) not null,
	--GridCode
   grid_code int,
   --UserSettings
   backup_soc numeric,
   --BasicSettings
        --EnergyPolicy
        energy_policy int,
        --Inverter
       inverter_limit int,
       inverter_grid_target_freq int,
        inverter_EGmode_enable int,
       inverter_PSmode_select_enable int,
       inverter_ESrandomdelay_enable int,
       inverter_MENsystem_enable int,
        --PV
        pv_type int,
        installed_pv_stringcount int,
        installed_pv1_stringpower int,
        installed_pv2_stringpower int,
        installed_pv3_stringpower int,
        installed_pv4_stringpower int,
        installed_pv5_stringpower int,
        pv_feed_in_limit int,
        --Meter
        meter_model int,
        meter_connection int,
        meter_tcp_ipaddress varchar(20),
        meter_tcp_port int,
        meter_rtu_baudrate int,
        meter_rtu_parity varchar(4),
        meter_rtu_dataBits int,
        meter_rtu_stopBits int,
        meter_loadfromgw int,
        --Battery
        battery_operating_rackcount int,
        battery_installed_rackcount int,
        --ExternalControl
        externalcontrol_gateway_connection int,
        --TOU
        tou_count int,
        section1_time varchar(20),
        section1_action int,
        section1_battery_ref int,
        section1_tariff int,
        section2_time varchar(20),
        section2_action int,
        section2_battery_ref int,
        section2_tariff int,
        section3_time varchar(20),
        section3_action int,
        section3_battery_ref int,
        section3_tariff int,
        section4_time varchar(20),
        section4_action int,
        section4_battery_ref int,
        section4_tariff int,
        section5_time varchar(20),
        section5_action int,
        section5_battery_ref int,
        section5_tariff int,
        section6_time varchar(20),
        section6_action int,
        section6_battery_ref int,
        section6_tariff int,
    --EngineerSettings
        --PCS_ExternalEMS
        PCS_externalEMS_pcs_connection int,
        PCS_externalEMS_debugmode_enable int,
        --Hysteresis
       hysteresis_low numeric,
       hysteresis_high numeric,
        --BatteryUserSOC
        battery_usersoc_min int,
        battery_usersoc_max int,
        --DebugInfo
        debuginfo_processmgr int,
        debuginfo_systemlog int,
        debuginfo_fota int,
        debuginfo_powercontrol int,
        debuginfo_algorithmmgr int,
        debuginfo_essmgr int,
        debuginfo_dcsourcemgr int,
        debuginfo_cloudmgr int,
        debuginfo_metermgr int,
        debuginfo_gatewaymgr int,
        debuginfo_datamgr int,
        debuginfo_dbmgr int,
        debuginfo_webengine int,
        --UpdateSettings
        PCS_updateset_autofwupdate_enable int,
    --AdvancedSettings
    advanced_settings jsonb,
        update_dt timestamp(6) with time zone,
        update_id varchar(64) not null,
        base_profile_cd integer,
        colec_dt timestamp(6) with time zone default sys_extract_utcnow() not null,
           constraint pk_opr_ess_config_set_gen3_hist_v2
      primary key (device_id, colec_dt,device_type_cd)

);

COMMENT ON TABLE tb_opr_ess_config_set_gen3_hist_v2 IS 'ESS 의 설정 정보 모니터링 상태 테이블';

COMMENT ON COLUMN  tb_opr_ess_config_set_gen3_hist_v2.device_id IS '장비ID';

COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.device_type_cd IS '장비유형코드';
--GridCode
COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.grid_code IS 'Grid Code';
--UserSettings
COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.backup_soc IS '0%(default),25%,50%,75%,100%' ;

--BasicSettings
    --EnergyPolicy
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.energy_policy IS '"0: Stanby
    1: Self consumption
    2. Zero Export Mode
    3. Time-based Mode
    4. External Generation Mode"' ;
    --Inverter
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.inverter_limit IS '정수값, 3600, 4600, 5000, 8000 모델별로 상이' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.inverter_grid_target_freq IS '50.00 or 60.00 [Hz]' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.inverter_EGmode_enable IS '0: Disable1: Enable';
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.inverter_PSmode_select_enable IS '0: Not operation 1: Operation' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.inverter_ESrandomdelay_enable IS '0: Disable1: Enable';
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.inverter_MENsystem_enable IS '0: Disable1: Enable';

    --PV
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.pv_type IS '"0: String Inverter
    1: Micro Inverter
    2: Power Optimizers"
    ' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.installed_pv_stringcount IS '0 ~ 5' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.installed_pv1_stringpower IS '0 ~ 5000W' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.installed_pv2_stringpower IS '0 ~ 5000W' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.installed_pv3_stringpower IS '0 ~ 5000W' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.installed_pv4_stringpower IS '0 ~ 5000W' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.installed_pv5_stringpower IS '0 ~ 5000W' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.pv_feed_in_limit IS '-10 ~ 100 %' ;
    --Meter
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.meter_model IS
    '"0:없음
    4:EM112 (CarloGavazzi)
    5:EM24 (CarloGavazzi)
    99:Custom"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.meter_connection IS
        '"0: Disable
    1: Modbus TCP
    2: Modbus RTU"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.meter_tcp_ipaddress IS 'TCP IP주소' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.meter_tcp_port IS '0 ~ 65535' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.meter_rtu_baudrate IS '4600, 9600, 19200, 38400, 57600, 115200' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.meter_rtu_parity IS 'N'' or ''E'' or ''O''' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.meter_rtu_dataBits IS '5 ~ 8' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.meter_rtu_stopBits IS '1 ~ 2' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.meter_loadfromgw IS '"0 : Meter Load 정보를 EMS 와 연결된 Meter 로부터 전달 받아 사용함. (기본)1 : Meter Load 정보를 3rd GW 로부터 전달 받아 사용함."' ;
    --Battery
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.battery_operating_rackcount IS '0~3' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.battery_installed_rackcount IS '0~3' ;
    --ExternalControl
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.externalcontrol_gateway_connection IS
        '"0: Disable
    1: Modbus TCP
    2: Modbus RTU"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.tou_count IS
        '"tou 카운트"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section1_time IS
        '"HH:MM 형태"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section1_action IS
        '"0: NOT / 1: Charge / 2: Discarhge / 3: Auto"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section1_battery_ref IS
        '"Target battery ref 값 (단위: W)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section1_tariff IS
        '"요금 정보, 통화 포함 X (Ex. 10$이면 10 내려옴)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section2_time IS
        '"HH:MM 형태"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section2_action IS
        '"0: NOT / 1: Charge / 2: Discarhge / 3: Auto"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section2_battery_ref IS
        '"Target battery ref 값 (단위: W)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section2_tariff IS
        '"요금 정보, 통화 포함 X (Ex. 10$이면 10 내려옴)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section3_time IS
        '"HH:MM 형태"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section3_action IS
        '"0: NOT / 1: Charge / 2: Discarhge / 3: Auto"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section3_battery_ref IS
        '"Target battery ref 값 (단위: W)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section3_tariff IS
        '"요금 정보, 통화 포함 X (Ex. 10$이면 10 내려옴)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section4_time IS
        '"HH:MM 형태"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section4_action IS
        '"0: NOT / 1: Charge / 2: Discarhge / 3: Auto"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section4_battery_ref IS
        '"Target battery ref 값 (단위: W)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section4_tariff IS
        '"요금 정보, 통화 포함 X (Ex. 10$이면 10 내려옴)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section5_time IS
        '"HH:MM 형태"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section5_action IS
        '"0: NOT / 1: Charge / 2: Discarhge / 3: Auto"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section5_battery_ref IS
        '"Target battery ref 값 (단위: W)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section5_tariff IS
        '"요금 정보, 통화 포함 X (Ex. 10$이면 10 내려옴)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section6_time IS
        '"HH:MM 형태"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section6_action IS
        '"0: NOT / 1: Charge / 2: Discarhge / 3: Auto"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section6_battery_ref IS
        '"Target battery ref 값 (단위: W)"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.section6_tariff IS
        '"요금 정보, 통화 포함 X (Ex. 10$이면 10 내려옴)"' ;





--EngineerSettings
    --PCS_ExternalEMS
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.PCS_externalEMS_pcs_connection IS
        '"0: Disable
    1: CAN (QCELL PCS)
    2: Modbus TCP (External EMS)
    3: Modbus RTU (External EMS)"' ;

    --Hysteresis
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.hysteresis_low IS 'Battery SOC가 5% 이하일 때 설정한 Low hysteresis SOC (10%,15%(default),20%,25%) 까지 방전하는 것이 불가능하다' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.hysteresis_high IS 'Battery SOC가 95% 이상일 때 설정한 High hysteresis SOC (90%(default),85%,80%) 까지 충전하는 것이 불가능하다' ;
    --BatteryUserSoc
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.battery_usersoc_min IS '해당 값은 실제 Battery SOC값이며 설정된 값이 User SOC의 0%임' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.battery_usersoc_max IS '해당 값은 실제 Battery SOC값이며 설정된 값이 User SOC의 100%임' ;
    --DebugInfo
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.debuginfo_processmgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.debuginfo_systemlog IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.debuginfo_fota IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.debuginfo_powercontrol IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.debuginfo_algorithmmgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.debuginfo_essmgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.debuginfo_dcsourcemgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.debuginfo_cloudmgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.debuginfo_metermgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.debuginfo_gatewaymgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.debuginfo_datamgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.debuginfo_dbmgr IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;
    COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.debuginfo_webengine IS
        '"0: Debug Level
    1: Info Level
    2: Warning Level
    3: Error Level"' ;

--Advanced Settings
   COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.advanced_settings IS 'Advanced Settings의 정보를 JSON형태로 저장.';
COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.colec_dt IS '이력이 수집된 시간';
COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.update_id IS '생성자 id';
COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.update_dt IS '최종 업데이트 시간(최초에는 삽입시간)';
COMMENT ON COLUMN tb_opr_ess_config_set_gen3_hist_v2.base_profile_cd IS 'base가 되는 profile의 country_code';



--데이터 보관주기와 파티셔닝주기를 입력한다.
--INSERT INTO tb_plf_tbl_meta(sub_area, table_nm, table_type, arcv_flag, arcv_period, fst_load_dt, load_type, partition_type, std_col_nm, std_col_type, del_flag, del_cycle,create_dt)
-- VALUES('OPR','TB_OPR_ESS_CONFIG_SET_GEN3_HIST_V2','HIST','Y','12','20201118',
--                                   'Append','M','COLEC_DT','TIMESTAMP','Y','12',sys_extract_utcnow());

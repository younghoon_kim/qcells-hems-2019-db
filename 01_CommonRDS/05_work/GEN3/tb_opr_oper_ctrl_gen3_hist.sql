-- auto-generated definition
create table tb_opr_oper_ctrl_gen3_hist
(
    req_id            numeric(18)                                                not null,
    device_id         varchar(50)                                                not null,
    device_type_cd    varchar(4)                                                 not null,
    grp_id            varchar(50)                                                not null,
    grp_nm            varchar(200),
    reboot_system            varchar,
    change_operation_mode    int,
    COM_inverter_limit_power int,
    COM_battery_target_power int,
    COM_pv_limit_power       int,
    get_system_status        int,
    reverse_ssh_ip           varchar,
    reverse_ssh_port         int,
    dr_organization varchar(64),
    dr_mode int,
    rel_BMSprotect_1 int,
    rel_BMSprotect_2 int,
    rel_BMSprotect_3 int,
    create_id         varchar(64)                                                not null,
    colec_dt         timestamp(6) with time zone default sys_extract_utc(now()) not null,
    constraint pk_opr_oper_ctrl_gen3_hist
        primary key (req_id, device_id, colec_dt,device_type_cd, grp_id)
);

comment on table tb_opr_oper_ctrl_gen3_hist is 'ESS 제어요청정보를 관리한다.(gen3)';

comment on column tb_opr_oper_ctrl_gen3_hist.req_id is '제어요청 일련번호 ,sq_opr_oper_ctrl를 tb_opr_oper_ctrl_hist와 공통으로 사용함.';

comment on column tb_opr_oper_ctrl_gen3_hist.device_id is '장비ID';

comment on column tb_opr_oper_ctrl_gen3_hist.device_type_cd is '장비유형코드';

comment on column tb_opr_oper_ctrl_gen3_hist.grp_id is '제어그룹ID';

comment on column tb_opr_oper_ctrl_gen3_hist.grp_nm is '제어그룹명';

comment on column tb_opr_oper_ctrl_gen3_hist.grp_id is '제어그룹 ID';

comment on column tb_opr_oper_ctrl_gen3_hist.reboot_system is 'system reboot';

comment on column tb_opr_oper_ctrl_gen3_hist.change_operation_mode is
    '0: NOP (No operation mode)
    1: Auto (PCS 자동 mode)
    2: Manual (Manual mode, 아래 3개의 값이 같이 전달되어야 함)
    3: Restore (Cloud에서 바꾸기 이전 원래 mode로 돌리기 위한 command)';

comment on column tb_opr_oper_ctrl_gen3_hist.COM_inverter_limit_power is 'change_operation_mode의 설정값 중 일부, ChangeOperationMode가 Manual일 때만 유의미한 값';

comment on column tb_opr_oper_ctrl_gen3_hist.COM_battery_target_power is 'change_operation_mode의 설정값 중 일부, ChangeOperationMode가 Manual일 때만 유의미한 값';

comment on column tb_opr_oper_ctrl_gen3_hist.COM_pv_limit_power is 'change_operation_mode의 설정값 중 일부, ChangeOperationMode가 Manual일 때만 유의미한 값';

comment on column tb_opr_oper_ctrl_gen3_hist.get_system_status is '현재까지 저장하고 있던 System log를 S3에 upload,현재 System(PMS)의 상태를 요청하는 기능';

comment on column tb_opr_oper_ctrl_gen3_hist.reverse_ssh_ip is 'Reverse ssh 접속을 위한 IP 주소';

comment on column tb_opr_oper_ctrl_gen3_hist.reverse_ssh_port is 'Reverse ssh 접속을 위한 Port 번호 (0~65535)';
comment on column tb_opr_oper_ctrl_gen3_hist.dr_organization is 'auto, drm, fcas, diamond_energy';
comment on column tb_opr_oper_ctrl_gen3_hist.dr_mode is 'drm의 경우 10진수 수 형태로 전달';

--BMS Protection을 release 시키기 위함
comment on column tb_opr_oper_ctrl_gen3_hist.rel_BMSprotect_1 is '0: None / 1: Software Reset / 2: Power Reset / 3: Permanent Reset ';
comment on column tb_opr_oper_ctrl_gen3_hist.rel_BMSprotect_2 is '0: None / 1: Software Reset / 2: Power Reset / 3: Permanent Reset ';
comment on column tb_opr_oper_ctrl_gen3_hist.rel_BMSprotect_3 is '0: None / 1: Software Reset / 2: Power Reset / 3: Permanent Reset ';


comment on column tb_opr_oper_ctrl_gen3_hist.create_id is '등록자ID';

comment on column tb_opr_oper_ctrl_gen3_hist.colec_dt is '생성일자';


--데이터 보관주기와 파티셔닝주기를 입력한다.2020.11.24 younghoon
INSERT INTO tb_plf_tbl_meta(sub_area, table_nm, table_type, arcv_flag, arcv_period, fst_load_dt, load_type, partition_type, std_col_nm, std_col_type, del_flag, del_cycle,create_dt)
 VALUES('OPR','TB_OPR_OPER_CTRL_GEN3_HIST','HIST','Y','12','20201118',
                                   'Append','M','CREATE_DT','TIMESTAMP','Y','12',sys_extract_utcnow());

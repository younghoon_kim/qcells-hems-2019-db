create table tb_opr_mon_state_pms_gen2_hist
(
   device_id varchar(50) not null,
   device_type_cd varchar(4) not null,
   colec_dt varchar(14) not null,
   oper_stus_cd varchar(4),
   pv_pw numeric(18,2),
   pv_pw_h numeric(18,2),
   pv_pw_price numeric(22,4),
   pv_pw_co2 numeric(18,2),
   pv_stus_cd varchar(4),
   cons_pw numeric(18,2),
   cons_pw_h numeric(18,2),
   cons_pw_price numeric(22,4),
   cons_pw_co2 numeric(20,6),
   bt_pw numeric(18,2),
   bt_chrg_pw_h numeric(18,2),
   bt_chrg_pw_price numeric(22,4),
   bt_chrg_pw_co2 numeric(20,6),
   bt_dchrg_pw_h numeric(18,2),
   bt_dchrg_pw_price numeric(22,4),
   bt_dchrg_pw_co2 numeric(20,6),
   bt_soc numeric(5,1),
   bt_soh numeric(10),
   bt_stus_cd varchar(4),
   grid_pw numeric(18,2),
   grid_ob_pw_h numeric(18,2),
   grid_ob_pw_price numeric(22,4),
   grid_ob_pw_co2 numeric(20,6),
   grid_tr_pw_h numeric(18,2),
   grid_tr_pw_price numeric(22,4),
   grid_tr_pw_co2 numeric(20,6),
   grid_stus_cd varchar(4),
   pcs_pw numeric(18,2),
   pcs_fd_pw_h numeric(18,2),
   pcs_pch_pw_h numeric(18,2),
   ems_opmode varchar(4),
   pcs_opmode1 varchar(4),
   pcs_opmode2 varchar(4),
   pcs_tgt_pw numeric(18,2),
   feed_in_limit varchar(4),
   create_dt timestamp(6) with time zone default sys_extract_utc(now()) not null,
   max_inverter_pw_cd varchar(8),
   rfsh_prd double precision default 60 not null,
   outlet_pw numeric(18,2),
   outlet_pw_h numeric(18,2),
   outlet_pw_price numeric(18,2),
   outlet_pw_co2 numeric(18,2),
   pwr_range_cd_ess_grid varchar(4),
   pwr_range_cd_grid_ess varchar(4),
   pwr_range_cd_grid_load varchar(4),
   pwr_range_cd_ess_load varchar(4),
   pwr_range_cd_pv_ess varchar(4),
   pcs_comm_err_rate numeric(3),
   smtr_comm_err_rate numeric(3),
   m2m_comm_err_rate numeric(3),
   pcs_flag0 varchar(8),
   pcs_flag1 varchar(8),
   pcs_flag2 varchar(8),
   pcs_opmode_cmd1 varchar(1),
   pcs_opmode_cmd2 varchar(1),
   pv_v1 numeric(5,1),
   pv_i1 numeric(4,1),
   pv_pw1 numeric(5),
   pv_v2 numeric(5,1),
   pv_i2 numeric(4,1),
   pv_pw2 numeric(5),
   inverter_v numeric(5,1),
   inverter_i numeric(4,1),
   inverter_pw numeric(5),
   dc_link_v numeric(5,1),
   g_rly_cnt double precision,
   bat_rly_cnt double precision,
   grid_to_grid_rly_cnt double precision,
   bat_pchrg_rly_cnt double precision,
   bms_flag0 varchar(8),
   bms_diag0 varchar(8),
   rack_v numeric(5,1),
   rack_i numeric(4,1),
   cell_max_v numeric(6,3),
   cell_min_v numeric(6,3),
   cell_avg_v numeric(6,3),
   cell_max_t numeric(4,1),
   cell_min_t numeric(4,1),
   cell_avg_t numeric(4,1),
   tray_cnt double precision,
   smtr_tp_cd varchar(4),
   smtr_pulse_cnt double precision,
   smtr_modl_cd varchar(4),
   pv_max_pwr1 double precision,
   pv_max_pwr2 double precision,
   instl_rgn varchar(10),
   mmtr_slv_addr double precision,
   basicmode_cd varchar(4),
   sys_st_cd varchar(4),
   bt_chrg_st_cd varchar(4),
   bt_dchrg_st_cd varchar(4),
   pv_st_cd varchar(4),
   grid_st_cd varchar(4),
   smtr_st_cd varchar(4),
   smtr_tr_cnter double precision,
   smtr_ob_cnter double precision,
   dn_enable varchar(4),
   dc_start varchar(6),
   dc_end varchar(6),
   nc_start varchar(6),
   nc_end varchar(6),
   grid_code0 varchar(50),
   grid_code1 varchar(50),
   grid_code2 varchar(50),
   pcon_bat_targetpower varchar(50),
   grid_code3 varchar(50),
   bdc_module_code0 varchar(50),
   bdc_module_code1 varchar(50),
   bdc_module_code2 varchar(50),
   bdc_module_code3 varchar(50),
   fault5_realtime_year varchar(50),
   fault5_realtime_month varchar(50),
   fault5_day varchar(50),
   fault5_hour varchar(50),
   fault5_minute varchar(50),
   fault5_second varchar(50),
   fault5_datahigh varchar(50),
   fault5_datalow varchar(50),
   fault6_realtime_year varchar(50),
   fault6_realtime_month varchar(50),
   fault6_realtime_day varchar(50),
   fault6_realtime_hour varchar(50),
   fault6_realtime_minute varchar(50),
   fault6_realtime_second varchar(50),
   fault6_datahigh varchar(50),
   fault6_datalow varchar(50),
   fault7_realtime_year varchar(50),
   fault7_realtime_month varchar(50),
   fault7_realtime_day varchar(50),
   fault7_realtime_hour varchar(50),
   fault7_realtime_minute varchar(50),
   fault7_realtime_second varchar(50),
   fault7_datahigh varchar(50),
   fault7_datalow varchar(50),
   fault8_realtime_year varchar(50),
   fault8_realtime_month varchar(50),
   fault8_realtime_day varchar(50),
   fault8_realtime_hour varchar(50),
   fault8_realtime_minute varchar(50),
   fault8_realtime_second varchar(50),
   fault8_datahigh varchar(50),
   fault8_datalow varchar(50),
   fault9_realtime_year varchar(50),
   fault9_realtime_month varchar(50),
   fault9_realtime_day varchar(50),
   fault9_realtime_hour varchar(50),
   fault9_realtime_minute varchar(50),
   fault9_realtime_second varchar(50),
   fault9_datahigh varchar(50),
   fault9_datalow varchar(50),
   is_connect boolean default false,
   bt_real_soc numeric(5,1),
   load_main_pw numeric(18,2),
   load_sub_pw numeric(18,2),
   load_main_pw_h numeric(18,2),
   load_sub_pw_h numeric(18,2),
   	constraint pk_opr_mon_state_pms_gen2_hist
		primary key (device_id, create_dt,device_type_cd)
);

comment on table tb_opr_mon_state_pms_gen2_hist is 'ESS의 5분단위 상태정보를 관리한다.';

comment on column tb_opr_mon_state_pms_gen2_hist.device_id is '장비ID';

comment on column tb_opr_mon_state_pms_gen2_hist.device_type_cd is '장비유형코드';

comment on column tb_opr_mon_state_pms_gen2_hist.colec_dt is '수집시간';

comment on column tb_opr_mon_state_pms_gen2_hist.oper_stus_cd is '운전상태코드';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_pw is 'PV현재 발전 전력';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_pw_h is 'PV일일누적 발전 전력양';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_pw_price is 'PV일일누적 발전 전력요금';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_pw_co2 is 'PV일일누적탄소배출량';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_stus_cd is 'PV상태코드';

comment on column tb_opr_mon_state_pms_gen2_hist.cons_pw is '부하현재전력';

comment on column tb_opr_mon_state_pms_gen2_hist.cons_pw_h is '부하일일누적전력량';

comment on column tb_opr_mon_state_pms_gen2_hist.cons_pw_price is '부하일일누적전력요금';

comment on column tb_opr_mon_state_pms_gen2_hist.cons_pw_co2 is '부하일일누적탄소배출량';

comment on column tb_opr_mon_state_pms_gen2_hist.bt_pw is '배터리현재전력';

comment on column tb_opr_mon_state_pms_gen2_hist.bt_chrg_pw_h is '배터리일일누적충전량';

comment on column tb_opr_mon_state_pms_gen2_hist.bt_chrg_pw_price is '배터리일일누적충전요금';

comment on column tb_opr_mon_state_pms_gen2_hist.bt_chrg_pw_co2 is '배터리일일누적충전 탄소배출량';

comment on column tb_opr_mon_state_pms_gen2_hist.bt_dchrg_pw_h is '배터리일일누적방전량';

comment on column tb_opr_mon_state_pms_gen2_hist.bt_dchrg_pw_price is '배터리일일누적방전요금';

comment on column tb_opr_mon_state_pms_gen2_hist.bt_dchrg_pw_co2 is '배터리일일누적방전 탄소배출량';

comment on column tb_opr_mon_state_pms_gen2_hist.bt_soc is '배터리충전량백분율(SOC)';

comment on column tb_opr_mon_state_pms_gen2_hist.bt_soh is '배터리잔존수명(SOH)';

comment on column tb_opr_mon_state_pms_gen2_hist.bt_stus_cd is '배터리상태코드';

comment on column tb_opr_mon_state_pms_gen2_hist.grid_pw is 'GRID현재전력';

comment on column tb_opr_mon_state_pms_gen2_hist.grid_ob_pw_h is 'GRID일일누적소비전력량';

comment on column tb_opr_mon_state_pms_gen2_hist.grid_ob_pw_price is 'GRID일일누적소비전력요금';

comment on column tb_opr_mon_state_pms_gen2_hist.grid_ob_pw_co2 is 'GRID일일누적 소비전력 탄소배출량';

comment on column tb_opr_mon_state_pms_gen2_hist.grid_tr_pw_h is 'GRID일일누적매전량';

comment on column tb_opr_mon_state_pms_gen2_hist.grid_tr_pw_price is 'GRID일일누적매전요금';

comment on column tb_opr_mon_state_pms_gen2_hist.grid_tr_pw_co2 is 'GRID일일누적매전 탄소배출량';

comment on column tb_opr_mon_state_pms_gen2_hist.grid_stus_cd is 'GRID상태코드';

comment on column tb_opr_mon_state_pms_gen2_hist.pcs_pw is 'PCS 인버터 파워';

comment on column tb_opr_mon_state_pms_gen2_hist.pcs_fd_pw_h is 'PCS 인버터 전력 발전량';

comment on column tb_opr_mon_state_pms_gen2_hist.pcs_pch_pw_h is 'PCS 인버터 전력 사용량';

comment on column tb_opr_mon_state_pms_gen2_hist.ems_opmode is 'EMS 운영모드';

comment on column tb_opr_mon_state_pms_gen2_hist.pcs_opmode1 is 'PCS 운전모드';

comment on column tb_opr_mon_state_pms_gen2_hist.pcs_opmode2 is 'PCS 운전모드2';

comment on column tb_opr_mon_state_pms_gen2_hist.pcs_tgt_pw is 'PCS 타겟파워';

comment on column tb_opr_mon_state_pms_gen2_hist.feed_in_limit is 'Feed In Limit';

comment on column tb_opr_mon_state_pms_gen2_hist.create_dt is '등록일시';

comment on column tb_opr_mon_state_pms_gen2_hist.rfsh_prd is '갱신주기';

comment on column tb_opr_mon_state_pms_gen2_hist.outlet_pw is 'Power Outlet 전력';

comment on column tb_opr_mon_state_pms_gen2_hist.outlet_pw_h is 'Power Outlet 전력량';

comment on column tb_opr_mon_state_pms_gen2_hist.outlet_pw_price is 'Power Outlet 전력요금';

comment on column tb_opr_mon_state_pms_gen2_hist.outlet_pw_co2 is 'Power Outlet 탄소배출량';

comment on column tb_opr_mon_state_pms_gen2_hist.pwr_range_cd_ess_grid is 'Power 범위 코드 (ESS -> Grid)';

comment on column tb_opr_mon_state_pms_gen2_hist.pwr_range_cd_grid_ess is 'Power 범위 코드 (Grid -> ESS)';

comment on column tb_opr_mon_state_pms_gen2_hist.pwr_range_cd_grid_load is 'Power 범위 코드 (Grid -> Load)';

comment on column tb_opr_mon_state_pms_gen2_hist.pwr_range_cd_ess_load is 'Power 범위 코드 (ESS -> Load)';

comment on column tb_opr_mon_state_pms_gen2_hist.pwr_range_cd_pv_ess is 'Power 범위 코드 (PV -> ESS)';

comment on column tb_opr_mon_state_pms_gen2_hist.pcs_comm_err_rate is 'PCS 통신 에러율';

comment on column tb_opr_mon_state_pms_gen2_hist.smtr_comm_err_rate is '스마트미터 통신 에러율';

comment on column tb_opr_mon_state_pms_gen2_hist.m2m_comm_err_rate is 'M2M 통신 에러율';

comment on column tb_opr_mon_state_pms_gen2_hist.pcs_flag0 is 'PCS 운전상태 0';

comment on column tb_opr_mon_state_pms_gen2_hist.pcs_flag1 is 'PCS 운전상태 1';

comment on column tb_opr_mon_state_pms_gen2_hist.pcs_flag2 is 'PCS 운전상태 2';

comment on column tb_opr_mon_state_pms_gen2_hist.pcs_opmode_cmd1 is 'PCS 제어 컴맨드 1';

comment on column tb_opr_mon_state_pms_gen2_hist.pcs_opmode_cmd2 is 'PCS 제어 컴맨드 2';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_v1 is 'PV 전압1';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_i1 is 'PV 전류1';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_pw1 is 'PV 파워1';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_v2 is 'PV 전압2';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_i2 is 'PV 전류2';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_pw2 is 'PV 파워2';

comment on column tb_opr_mon_state_pms_gen2_hist.inverter_v is '인버터 전압';

comment on column tb_opr_mon_state_pms_gen2_hist.inverter_i is '인버터 전류';

comment on column tb_opr_mon_state_pms_gen2_hist.inverter_pw is '인버터 파워';

comment on column tb_opr_mon_state_pms_gen2_hist.dc_link_v is 'DC Link 전압';

comment on column tb_opr_mon_state_pms_gen2_hist.g_rly_cnt is 'Relay Count (G-relay)';

comment on column tb_opr_mon_state_pms_gen2_hist.bat_rly_cnt is 'Relay Count (BAT-relay)';

comment on column tb_opr_mon_state_pms_gen2_hist.grid_to_grid_rly_cnt is 'Relay Count (Grid to Grid relay)';

comment on column tb_opr_mon_state_pms_gen2_hist.bat_pchrg_rly_cnt is 'Relay Count (BAT-Precharge-relay)';

comment on column tb_opr_mon_state_pms_gen2_hist.bms_flag0 is 'BMS 운전상태';

comment on column tb_opr_mon_state_pms_gen2_hist.bms_diag0 is 'BMS 진단상태';

comment on column tb_opr_mon_state_pms_gen2_hist.rack_v is '랙 전압';

comment on column tb_opr_mon_state_pms_gen2_hist.rack_i is '랙 전류';

comment on column tb_opr_mon_state_pms_gen2_hist.cell_max_v is '셀 최대 전압';

comment on column tb_opr_mon_state_pms_gen2_hist.cell_min_v is '셀 최소 전압';

comment on column tb_opr_mon_state_pms_gen2_hist.cell_avg_v is '셀 평균 전압';

comment on column tb_opr_mon_state_pms_gen2_hist.cell_max_t is '셀 최대 온도';

comment on column tb_opr_mon_state_pms_gen2_hist.cell_min_t is '셀 최소 온도';

comment on column tb_opr_mon_state_pms_gen2_hist.cell_avg_t is '셀 평균 온도';

comment on column tb_opr_mon_state_pms_gen2_hist.tray_cnt is '트레이 수량';

comment on column tb_opr_mon_state_pms_gen2_hist.smtr_tp_cd is '설치 스마트미터 기종 코드';

comment on column tb_opr_mon_state_pms_gen2_hist.smtr_pulse_cnt is '설치 스마트미터 펄스카운트';

comment on column tb_opr_mon_state_pms_gen2_hist.smtr_modl_cd is '스마트미터 모델 코드';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_max_pwr1 is '설치 PV 최대 전력량1';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_max_pwr2 is '설치 PV 최대 전력량2';

comment on column tb_opr_mon_state_pms_gen2_hist.instl_rgn is '설치 지역';

comment on column tb_opr_mon_state_pms_gen2_hist.mmtr_slv_addr is 'Slave address for Modebus Meter';

comment on column tb_opr_mon_state_pms_gen2_hist.basicmode_cd is 'ELA Basic Mode 코드';

comment on column tb_opr_mon_state_pms_gen2_hist.sys_st_cd is '시스템 상태 코드';

comment on column tb_opr_mon_state_pms_gen2_hist.bt_chrg_st_cd is '배터리 충전 가능여부 코드';

comment on column tb_opr_mon_state_pms_gen2_hist.bt_dchrg_st_cd is '배터리 방전 가능여부 코드';

comment on column tb_opr_mon_state_pms_gen2_hist.pv_st_cd is '인버터 방전(PV 발전 가능) 가능여부 코드';

comment on column tb_opr_mon_state_pms_gen2_hist.grid_st_cd is '그리드 상태 코드';

comment on column tb_opr_mon_state_pms_gen2_hist.smtr_st_cd is '스마트 미터(에너지 미터) 상태 코드';

comment on column tb_opr_mon_state_pms_gen2_hist.smtr_tr_cnter is '스마트미터 총 사용량';

comment on column tb_opr_mon_state_pms_gen2_hist.smtr_ob_cnter is '스마트미터 총 구매량';

comment on column tb_opr_mon_state_pms_gen2_hist.dn_enable is '주야간충방전플래그';

comment on column tb_opr_mon_state_pms_gen2_hist.dc_start is '주간충전시작';

comment on column tb_opr_mon_state_pms_gen2_hist.dc_end is '주간충전종료';

comment on column tb_opr_mon_state_pms_gen2_hist.nc_start is '야간충전시작';

comment on column tb_opr_mon_state_pms_gen2_hist.nc_end is '야간충전종료';

comment on column tb_opr_mon_state_pms_gen2_hist.is_connect is '연결/미연결 상태 저장';

comment on column tb_opr_mon_state_pms_gen2_hist.bt_real_soc is '실제 SOC, user_soc와는 다른 개념';

comment on column tb_opr_mon_state_pms_gen2_hist.load_main_pw is 'Load 중 main_load의 power';

comment on column tb_opr_mon_state_pms_gen2_hist.load_sub_pw is 'Load 중 sub_load의 power';

comment on column tb_opr_mon_state_pms_gen2_hist.load_main_pw_h is 'Load 중 main_load의 Electrical energy';

comment on column tb_opr_mon_state_pms_gen2_hist.load_sub_pw_h is 'Load 중 sub_load의 Electrical energy';

--데이터 보관주기와 파티셔닝주기를 입력한다.
INSERT INTO tb_plf_tbl_meta(sub_area, table_nm, table_type, arcv_flag, arcv_period, fst_load_dt, load_type, partition_type, std_col_nm, std_col_type, del_flag, del_cycle,create_dt)
 VALUES('OPR','TB_OPR_MON_STATE_PMS_GEN2_HIST','HIST','Y','4','20201118',
                                   'Append','M','CREATE_DT','TIMESTAMP','Y','4',sys_extract_utcnow());
--**********************************************
-- ***  1분주기로 업데이트되는 gen3 pms장비 상태 테이블의 이력을 담는다. ***
--**********************************************
--drop table if exists tb_opr_mon_state_pms_gen3_hist;
create table tb_opr_mon_state_pms_gen3_hist
(
      device_id varchar(50) not null,
   device_type_cd varchar(4) not null,
   --EMS INFO
   pv_pw numeric(18,2),
   pv_pw_h numeric(18,2),
   pv_pw_h_delta numeric (18,2),
   pv_stus_cd varchar(4),
   cons_pw numeric(18,2),
   cons_pw_h numeric(18,2),
   cons_pw_h_delta numeric(18,2),
   bt_pw numeric(18,2),
   bt_chrg_pw_h numeric(18,2),
   bt_chrg_pw_h_delta numeric(18,2),
   bt_dchrg_pw_h numeric(18,2),
   bt_dchrg_pw_h_delta numeric(18,2),
   bt_soc numeric(5,1),
   bt_soh numeric(10),
   bt_stus_cd varchar(4),
   grid_pw numeric(18,2),
   grid_ob_pw_h numeric(18,2),
   grid_ob_pw_h_delta numeric(18,2),
   grid_tr_pw_h numeric(18,2),
   grid_tr_pw_h_delta numeric(18,2),
   pcs_fd_pw_h numeric(18,2),
   pcs_fd_pw_h_delta numeric(18,2),
   pcs_pch_pw_h numeric(18,2),
   pcs_pch_pw_h_delta numeric(18,2),
   ems_opmode varchar(4),
   pcs_opmode1 varchar(4),
   pcs_opmode2 varchar(4),
   pcs_tgt_pw numeric(18,2),
   sys_st_cd numeric,
    bt_chrg_st_cd numeric,
    bt_dchrg_st_cd numeric,
    pv_st_cd numeric,
    grid_st_cd numeric,
    smtr_st_cd numeric,
    --2020.12.29 v4.0에서 삭제
    --smtr_tr_cnter double precision,
    --smtr_ob_cnter double precision,

    --PCS INFO
   pcs_opmode_cmd1 varchar(1),
   pcs_opmode_cmd2 varchar(1),
   pv_v1 numeric(5,1),
   pv_i1 numeric(4,1),
   pv_pw1 numeric(5),
   pv_v2 numeric(5,1),
   pv_i2 numeric(4,1),
   pv_pw2 numeric(5),
   pv_v3 numeric(5,1),
   pv_i3 numeric(4,1),
   pv_pw3 numeric(5),
   inverter_v numeric(5,1),
   inverter_i numeric(4,1),
   inverter_pw numeric(5),
   dc_link_v numeric(5,1),
   bms_flag0 varchar(8),
   --Battery INFO
   rack_v numeric(5,1),
   rack_i numeric(4,1),
    --Cellinfo
    rack_id numeric,
	cell_max_v numeric(6,3),
   	cell_max_v_module_id numeric,
   	cell_max_v_cell_id numeric,
   	cell_min_v numeric(6,3),
	cell_min_v_module_id numeric,
	cell_min_v_cell_id numeric,
	cell_avg_v numeric(6,3),
	cell_max_t numeric(4,1),
	cell_max_t_module_id numeric,
	cell_max_t_cell_id numeric,
	cell_min_t numeric(4,1),
	cell_min_t_module_id numeric,
	cell_min_t_cell_id numeric,
	cell_avg_t numeric(4,1),
	--Error INFO
	ems_error_codes varchar(50),
	pcs_error_codes varchar(50),
	bms_error_codes varchar(50),
	colec_dt timestamp(6) with time zone default sys_extract_utc(now()) not null,
	create_dt timestamp(6) with time zone default sys_extract_utc(now()) not null,
   constraint pk_opr_mon_state_pms_gen3_hist
      primary key (device_id, colec_dt, device_type_cd)
);

comment on table tb_opr_mon_state_pms_gen3_hist is 'ESS의 5분단위 상태정보를 관리한다.';

comment on column tb_opr_mon_state_pms_gen3_hist.device_id is '장비ID';

comment on column tb_opr_mon_state_pms_gen3_hist.device_type_cd is '장비유형코드';

comment on column tb_opr_mon_state_pms_gen3_hist.colec_dt is '수집시간';

comment on column tb_opr_mon_state_pms_gen3_hist.pv_pw is 'PV현재 발전 전력';

comment on column tb_opr_mon_state_pms_gen3_hist.pv_pw_h is 'PV일일누적 발전 전력양';

comment on column tb_opr_mon_state_pms_gen3_hist.pv_pw_h_delta is 'PV일일누적 발전 전력양에 대한 1분 Delta 값. Wh';

comment on column tb_opr_mon_state_pms_gen3_hist.pv_stus_cd is 'PV상태코드';

comment on column tb_opr_mon_state_pms_gen3_hist.cons_pw is '부하현재전력';

comment on column tb_opr_mon_state_pms_gen3_hist.cons_pw_h is '부하일일누적전력량';

comment on column tb_opr_mon_state_pms_gen3_hist.cons_pw_h_delta is '부하일일누적전력량에 대한 1분 Delta 값. Wh';

comment on column tb_opr_mon_state_pms_gen3_hist.bt_pw is '배터리현재전력';

comment on column tb_opr_mon_state_pms_gen3_hist.bt_chrg_pw_h is '배터리일일누적충전량';

comment on column tb_opr_mon_state_pms_gen3_hist.bt_chrg_pw_h is '배터리일일누적충전량';

comment on column tb_opr_mon_state_pms_gen3_hist.bt_dchrg_pw_h_delta is '배터리일일누적충전량에 대한 1분 Delta 값';

comment on column tb_opr_mon_state_pms_gen3_hist.bt_soc is '배터리충전량백분율(SOC)';

comment on column tb_opr_mon_state_pms_gen3_hist.bt_soh is '배터리잔존수명(SOH)';

comment on column tb_opr_mon_state_pms_gen3_hist.bt_stus_cd is '배터리상태코드';

comment on column tb_opr_mon_state_pms_gen3_hist.grid_pw is 'GRID현재전력';

comment on column tb_opr_mon_state_pms_gen3_hist.grid_ob_pw_h is 'GRID일일누적소비전력량';

comment on column tb_opr_mon_state_pms_gen3_hist.grid_ob_pw_h_delta is 'GRID일일누적소비전력량에 대한 1분 Delta 값. Wh';

comment on column tb_opr_mon_state_pms_gen3_hist.grid_tr_pw_h is 'GRID일일누적매전량';

comment on column tb_opr_mon_state_pms_gen3_hist.grid_tr_pw_h_delta is 'GRID일일누적매전량. 그리드 전력 매전량에 대한 1분 Delta 값. Wh';

comment on column tb_opr_mon_state_pms_gen3_hist.pcs_fd_pw_h is 'PCS 인버터 전력 발전량';

comment on column tb_opr_mon_state_pms_gen3_hist.pcs_fd_pw_h_delta is 'PCS 인버터 전력 발전량. PCS 인버터 전력 출력량에 대한 1분 Delta 값. Wh';

comment on column tb_opr_mon_state_pms_gen3_hist.pcs_pch_pw_h is 'PCS 인버터 전력 사용량';

comment on column tb_opr_mon_state_pms_gen3_hist.pcs_pch_pw_h_delta is 'PCS 인버터 전력 사용량. PCS 인버터 전력 사용량에 대한 1분 Delta 값. Wh';

comment on column tb_opr_mon_state_pms_gen3_hist.ems_opmode is 'EMS 운영모드';

comment on column tb_opr_mon_state_pms_gen3_hist.pcs_opmode1 is 'PCS 운전모드';

comment on column tb_opr_mon_state_pms_gen3_hist.pcs_opmode2 is 'PCS 운전모드2';

comment on column tb_opr_mon_state_pms_gen3_hist.pcs_tgt_pw is 'PCS 타겟파워';

comment on column tb_opr_mon_state_pms_gen3_hist.sys_st_cd is '시스템 상태 코드 Col88 ';

comment on column tb_opr_mon_state_pms_gen3_hist.bt_chrg_st_cd is '배터리 충전 가능여부 0: 불가능 / 1: 가능 ';

comment on column tb_opr_mon_state_pms_gen3_hist.bt_dchrg_st_cd is '배터리 방전 가능여부 0: 불가능 / 1: 가능 ';

comment on column tb_opr_mon_state_pms_gen3_hist.pv_st_cd is '인버터 방전(PV 발전 가능) 가능여부 0: 불가능 / 1: 가능';

comment on column tb_opr_mon_state_pms_gen3_hist.grid_st_cd is '그리드 상태 코드 ';

comment on column tb_opr_mon_state_pms_gen3_hist.smtr_st_cd is '스마트 미터(에너지 미터) 상태 코드 ';

--comment on column tb_opr_mon_state_pms_gen3_hist.smtr_tr_cnter is '스마트미터 총 사용량  ';

--comment on column tb_opr_mon_state_pms_gen3_hist.smtr_ob_cnter is '스마트미터 총 구매량';

comment on column tb_opr_mon_state_pms_gen3_hist.pcs_opmode_cmd1 is 'PCS 제어 컴맨드 1';

comment on column tb_opr_mon_state_pms_gen3_hist.pcs_opmode_cmd2 is 'PCS 제어 컴맨드 2';

comment on column tb_opr_mon_state_pms_gen3_hist.pv_v1 is 'PV 전압1';

comment on column tb_opr_mon_state_pms_gen3_hist.pv_i1 is 'PV 전류1';

comment on column tb_opr_mon_state_pms_gen3_hist.pv_pw1 is 'PV 파워1';

comment on column tb_opr_mon_state_pms_gen3_hist.pv_v2 is 'PV 전압2';

comment on column tb_opr_mon_state_pms_gen3_hist.pv_i2 is 'PV 전류2';

comment on column tb_opr_mon_state_pms_gen3_hist.pv_pw2 is 'PV 파워2';

comment on column tb_opr_mon_state_pms_gen3_hist.pv_i3 is 'PV 전류3';

comment on column tb_opr_mon_state_pms_gen3_hist.pv_pw3 is 'PV 파워3';

comment on column tb_opr_mon_state_pms_gen3_hist.inverter_v is '인버터 전압';

comment on column tb_opr_mon_state_pms_gen3_hist.inverter_i is '인버터 전류';

comment on column tb_opr_mon_state_pms_gen3_hist.inverter_pw is '인버터 파워';

comment on column tb_opr_mon_state_pms_gen3_hist.dc_link_v is 'DC Link 전압';

comment on column tb_opr_mon_state_pms_gen3_hist.bms_flag0 is 'BMS 운전상태';

comment on column tb_opr_mon_state_pms_gen3_hist.rack_v is '랙 전압';

comment on column tb_opr_mon_state_pms_gen3_hist.rack_i is '랙 전류';

comment on column tb_opr_mon_state_pms_gen3_hist.rack_id is 'Battery Rack에 대한 ID';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_max_v is '셀 최대 전압';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_max_v_module_id is '최대 전압을 가지는 module의 ID';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_max_v_cell_id is '최대 전압을 가지는 Cell의 ID';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_min_v is '셀 최소 전압';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_min_v_module_id is '최소 전압을 가지는 module의 ID';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_min_v_cell_id is '최소 전압을 가지는 Cell의 ID';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_max_t is '셀 최대 온도';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_max_t_module_id is '최대 온도를 가지는 module의 ID';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_max_t_cell_id is '최대 온도를 가지는 Cell의 ID';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_min_t is '셀 최소 온도';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_min_t_module_id is '최소 온도를 가지는 module의 ID';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_min_t_cell_id is '최소 온도를 가지는 Cell의 ID';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_avg_v is '셀 평균 전압';

comment on column tb_opr_mon_state_pms_gen3_hist.cell_avg_t is '셀 평균 온도';

comment on column tb_opr_mon_state_pms_gen3_hist.ems_error_codes is '발생한 EMS Error code를 모두 string 형식으로 표현, 구분자는 $';

comment on column tb_opr_mon_state_pms_gen3_hist.pcs_error_codes is '발생한 PCS Error code를 모두 string 형식으로 표현, 구분자는 $';

comment on column tb_opr_mon_state_pms_gen3_hist.bms_error_codes is '발생한 BMS Error code를 모두 string 형식으로 표현, 구분자는 $';


INSERT INTO tb_plf_tbl_meta(sub_area, table_nm, table_type, arcv_flag, arcv_period, fst_load_dt, load_type, partition_type, std_col_nm, std_col_type, del_flag, del_cycle,create_dt)
 VALUES('OPR','TB_OPR_MON_STATE_PMS_GEN3_HIST','HIST','Y','4','20201118',
                                   'Append','M','CREATE_DT','TIMESTAMP','Y','4',sys_extract_utcnow());